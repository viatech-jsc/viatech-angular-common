(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@ng-bootstrap/ng-bootstrap'), require('@angular/forms'), require('jquery'), require('@angular/common'), require('rxjs/operators'), require('@angular/animations'), require('ngx-toastr'), require('@angular/router'), require('ngx-image-cropper'), require('ngx-uploader'), require('@angular/common/http')) :
    typeof define === 'function' && define.amd ? define('viatech-angular-common', ['exports', '@angular/core', '@ng-bootstrap/ng-bootstrap', '@angular/forms', 'jquery', '@angular/common', 'rxjs/operators', '@angular/animations', 'ngx-toastr', '@angular/router', 'ngx-image-cropper', 'ngx-uploader', '@angular/common/http'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['viatech-angular-common'] = {}, global.ng.core, global.i1, global.ng.forms, global.$$1, global.ng.common, global.rxjs.operators, global.ng.animations, global.i1$1, global.ng.router, global.i3$1, global.i4, global.ng.common.http));
}(this, (function (exports, i0, i1, i3, $$1, i2, operators, animations, i1$1, i1$2, i3$1, i4, i1$3) { 'use strict';

    function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

    var $__default = /*#__PURE__*/_interopDefaultLegacy($$1);

    var TimepickerComponent = /** @class */ (function () {
        function TimepickerComponent() {
            this.showSeccond = false;
            this.showMeridian = false;
            this.timepickerModelChange = new i0.EventEmitter();
            this.disabled = false;
            this.spinners = false;
        }
        TimepickerComponent.prototype.ngOnInit = function () {
        };
        TimepickerComponent.prototype.getDateSelected = function () {
            // if(this.objectUtils.isValid(this.timepickerItem)){
            //   return new Date(this.timepickerItem.year, this.timepickerItem.month - 1, this.timepickerItem.day);
            // }
            // return null;
        };
        TimepickerComponent.prototype.timeSelect = function (time) {
            this.timepickerModelChange.emit(time);
        };
        return TimepickerComponent;
    }());
    TimepickerComponent.ɵfac = function TimepickerComponent_Factory(t) { return new (t || TimepickerComponent)(); };
    TimepickerComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TimepickerComponent, selectors: [["via-timepicker"]], inputs: { timepickerModel: "timepickerModel", showSeccond: "showSeccond", showMeridian: "showMeridian", disabled: "disabled" }, outputs: { timepickerModelChange: "timepickerModelChange" }, decls: 2, vars: 5, consts: [["id", "win-timepicker"], [3, "readonlyInputs", "ngModel", "meridian", "seconds", "spinners", "ngModelChange"]], template: function TimepickerComponent_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵelementStart(0, "div", 0);
                i0.ɵɵelementStart(1, "ngb-timepicker", 1);
                i0.ɵɵlistener("ngModelChange", function TimepickerComponent_Template_ngb_timepicker_ngModelChange_1_listener($event) { return ctx.timepickerModel = $event; })("ngModelChange", function TimepickerComponent_Template_ngb_timepicker_ngModelChange_1_listener($event) { return ctx.timeSelect($event); });
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
            }
            if (rf & 2) {
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("readonlyInputs", ctx.disabled)("ngModel", ctx.timepickerModel)("meridian", ctx.showMeridian)("seconds", ctx.showSeccond)("spinners", ctx.spinners);
            }
        }, directives: [i1.NgbTimepicker, i3.NgControlStatus, i3.NgModel], styles: ["#win-datepicker[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:column;width:100%}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:5px 10px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]{align-items:center;display:flex;flex:1;justify-content:baseline}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{margin-right:5px;width:20px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{flex:8}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span.dropdown-arrow[_ngcontent-%COMP%]{flex:1;font-size:24px;text-align:right}#win-datepicker[_ngcontent-%COMP%]   .input-datepicker[_ngcontent-%COMP%]{font-size:0;height:0;line-height:0;margin:0;padding:0;visibility:hidden;width:100%}"] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(TimepickerComponent, [{
                type: i0.Component,
                args: [{
                        selector: "via-timepicker",
                        templateUrl: "./timepicker.html",
                        styleUrls: ["./timepicker.scss"]
                    }]
            }], function () { return []; }, { timepickerModel: [{
                    type: i0.Input
                }], showSeccond: [{
                    type: i0.Input
                }], showMeridian: [{
                    type: i0.Input
                }], timepickerModelChange: [{
                    type: i0.Output
                }], disabled: [{
                    type: i0.Input
                }] });
    })();

    var TimepickerModule = /** @class */ (function () {
        function TimepickerModule() {
        }
        return TimepickerModule;
    }());
    TimepickerModule.ɵmod = i0.ɵɵdefineNgModule({ type: TimepickerModule });
    TimepickerModule.ɵinj = i0.ɵɵdefineInjector({ factory: function TimepickerModule_Factory(t) { return new (t || TimepickerModule)(); }, imports: [[
                i1.NgbModule,
                i3.FormsModule,
                i3.ReactiveFormsModule
            ]] });
    (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(TimepickerModule, { declarations: [TimepickerComponent], imports: [i1.NgbModule,
                i3.FormsModule,
                i3.ReactiveFormsModule], exports: [TimepickerComponent] });
    })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(TimepickerModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [TimepickerComponent],
                        imports: [
                            i1.NgbModule,
                            i3.FormsModule,
                            i3.ReactiveFormsModule
                        ],
                        exports: [TimepickerComponent]
                    }]
            }], null, null);
    })();

    var ImageSphereComponent = /** @class */ (function () {
        function ImageSphereComponent(ngbActiveModal) {
            this.ngbActiveModal = ngbActiveModal;
        }
        ImageSphereComponent.prototype.ngOnInit = function () {
            pannellum.viewer('panorama', {
                "type": "equirectangular",
                "autoLoad": true,
                "title": "360 Photo",
                "compass": true,
                "panorama": this.imgPath
            });
        };
        ImageSphereComponent.prototype.closeModal = function () {
            this.ngbActiveModal.close();
        };
        return ImageSphereComponent;
    }());
    ImageSphereComponent.ɵfac = function ImageSphereComponent_Factory(t) { return new (t || ImageSphereComponent)(i0.ɵɵdirectiveInject(i1.NgbActiveModal)); };
    ImageSphereComponent.ɵcmp = i0.ɵɵdefineComponent({ type: ImageSphereComponent, selectors: [["via-photo-sphere"]], inputs: { imgPath: "imgPath" }, decls: 5, vars: 0, consts: [["id", "photo-sphere"], [1, "modal-360", 3, "click"], ["aria-hidden", "true"], ["id", "panorama"]], template: function ImageSphereComponent_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵelementStart(0, "div", 0);
                i0.ɵɵelementStart(1, "div", 1);
                i0.ɵɵlistener("click", function ImageSphereComponent_Template_div_click_1_listener() { return ctx.closeModal(); });
                i0.ɵɵelementStart(2, "span", 2);
                i0.ɵɵtext(3, "\u00D7");
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelement(4, "div", 3);
                i0.ɵɵelementEnd();
            }
        }, styles: ["#photo-sphere[_ngcontent-%COMP%]   #panorama[_ngcontent-%COMP%]{min-height:700px}#photo-sphere[_ngcontent-%COMP%]   .modal-360[_ngcontent-%COMP%]{background:#fff;border:1px solid rgba(0,0,0,.4);border-radius:4px;cursor:pointer;height:25px;line-height:25px;position:absolute;right:3px;text-align:center;top:3px;width:25px;z-index:1}"] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(ImageSphereComponent, [{
                type: i0.Component,
                args: [{
                        selector: "via-photo-sphere",
                        templateUrl: "./360.img.html",
                        styleUrls: ["./360.img.scss"]
                    }]
            }], function () { return [{ type: i1.NgbActiveModal }]; }, { imgPath: [{
                    type: i0.Input
                }] });
    })();

    var ImageSphereModule = /** @class */ (function () {
        function ImageSphereModule() {
        }
        return ImageSphereModule;
    }());
    ImageSphereModule.ɵmod = i0.ɵɵdefineNgModule({ type: ImageSphereModule });
    ImageSphereModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ImageSphereModule_Factory(t) { return new (t || ImageSphereModule)(); }, imports: [[]] });
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ImageSphereModule, { declarations: [ImageSphereComponent], exports: [ImageSphereComponent] }); })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(ImageSphereModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [ImageSphereComponent],
                        imports: [],
                        exports: [ImageSphereComponent]
                    }]
            }], null, null);
    })();

    var mapConfig = {
        defaultLattitude: 10.8230989,
        defaultLongtitude: 106.6296638,
        defaultTitle: "Ho CHi Minh City",
        mapEventNames: {
            search: "search",
            myLocation: "myLocation",
            mapClick: "mapClick",
            markerDrag: "markerDrag"
        },
        myLocation: "MAP_MY_LOCATION"
    };
    var DATE_FORMAT = "hh:mma dd/MM/yyyy";
    var TIME_FORMAT = "hh:mma";
    var DATE_FORMAT_WITHOUT_TIME = "dd/MM/yyyy";

    var Map = /** @class */ (function () {
        function Map(latitude, longtitude, title, eventName, zoomLevel) {
            this.latitude = latitude;
            this.longtitude = longtitude;
            this.title = title;
            this.eventName = eventName;
            this.zoomLevel = zoomLevel;
        }
        return Map;
    }());

    function MapComponent_div_1_Template(rf, ctx) {
        if (rf & 1) {
            var _r3_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 5);
            i0.ɵɵelementStart(1, "div", 6);
            i0.ɵɵelementStart(2, "div", 7);
            i0.ɵɵelement(3, "div", 8);
            i0.ɵɵelementStart(4, "input", 9);
            i0.ɵɵlistener("ngModelChange", function MapComponent_div_1_Template_input_ngModelChange_4_listener($event) { i0.ɵɵrestoreView(_r3_1); var ctx_r2 = i0.ɵɵnextContext(); return ctx_r2.cordinate.title = $event; });
            i0.ɵɵelementEnd();
            i0.ɵɵelement(5, "img", 10);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r0 = i0.ɵɵnextContext();
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("ngModel", ctx_r0.cordinate.title);
        }
    }
    function MapComponent_button_4_Template(rf, ctx) {
        if (rf & 1) {
            var _r5_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "button", 11);
            i0.ɵɵlistener("click", function MapComponent_button_4_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r5_1); var ctx_r4 = i0.ɵɵnextContext(); return ctx_r4.myCurrentLocation(); });
            i0.ɵɵelement(1, "img", 12);
            i0.ɵɵelementEnd();
        }
    }
    var MapComponent = /** @class */ (function () {
        function MapComponent() {
            this.markerDraggable = true;
            this.customIcon = false;
            this.showMyLocation = true;
            this.hideSearchTextBox = false;
            this.cordinateChange = new i0.EventEmitter();
            this.markerClick = new i0.EventEmitter();
            this.mapRadius = new i0.EventEmitter();
            this.mapDragged = new i0.EventEmitter();
            this.searchEvent = new i0.EventEmitter();
            this.getTopCordinatorEvent = new i0.EventEmitter();
            this.getCurrentLocationEvent = new i0.EventEmitter();
            this.greenPinIcon = 'assets/icons/green_pin.svg';
            this.locationIcon = 'assets/icons/red_pin.svg';
            this.markers = [];
        }
        MapComponent.prototype.ngAfterViewInit = function () {
            var self = this;
            self.placeholderText = self.searchPlaceholer;
            self.geocoder = new google.maps.Geocoder();
            var initMap = function () {
                self.myLatLng = {
                    lat: self.cordinate.latitude, lng: self.cordinate.longtitude
                };
                var mapConfigObject = {
                    zoom: 15,
                    center: self.myLatLng,
                    streetViewControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: false
                };
                self.map = new google.maps.Map(document.getElementById('googleMap'), mapConfigObject);
                //listen to map drag event
                google.maps.event.addListener(self.map, 'dragend', function () {
                    self.mapDragged.emit("");
                });
                //listen to map zoom event
                google.maps.event.addListener(self.map, 'zoom_changed', function () {
                    if (self.mapRadius) {
                        var cordinate = new Map(undefined, undefined, undefined, "zoom_changed", self.map.getZoom());
                        self.mapRadius.emit(cordinate);
                    }
                });
                if (!self.hideSearchTextBox) {
                    self.input = document.getElementById('pac-input');
                    self.searchBox = new google.maps.places.SearchBox(self.input);
                    self.map.addListener('bounds_changed', function () {
                        self.searchBox.setBounds(self.map.getBounds());
                    });
                    self.addSearchBoxEvent();
                }
                if (self.cordinate.title === mapConfig.myLocation) {
                    self.myLocation = new google.maps.Marker({
                        position: self.myLatLng,
                        icon: {
                            url: self.greenPinIcon,
                            scaledSize: new google.maps.Size(30, 30),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(0, 0) // anchor
                        },
                        zIndex: 1000,
                        map: self.map,
                        draggable: false
                    });
                }
                else {
                    self.marker = new google.maps.Marker({
                        position: self.myLatLng,
                        map: self.map,
                        draggable: self.markerDraggable,
                        animation: google.maps.Animation.DROP,
                        title: self.cordinate.title
                    });
                    self.addClickListener();
                    self.addDragendListener();
                }
                self.geocodePosition(self.myLatLng);
            };
            if (self.cordinate.latitude && self.cordinate.longtitude) {
                initMap();
            }
            else {
                // ask user for current location permission 
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (location) {
                        self.cordinate.latitude = location.coords.latitude;
                        self.cordinate.longtitude = location.coords.longitude;
                        initMap();
                    }, function () {
                        // default location: 288H1 Nam Ky Khoi Nghia
                        self.cordinate.latitude = 10.7895299;
                        self.cordinate.longtitude = 106.68493590000003;
                        initMap();
                    });
                }
            }
        };
        MapComponent.prototype.getFurthestPointFromLocation = function (lat, lng, eventName) {
            var cordinate, self = this, cornersArray = {}, corner1, corner2, corner3, corner4, farestPoint;
            var sW = this.map.getBounds().getSouthWest();
            var nE = this.map.getBounds().getNorthEast();
            cornersArray = {}, corner1, corner2, corner3, corner4;
            corner1 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), sW.lng()));
            corner2 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), nE.lng()));
            corner3 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), nE.lng()));
            corner4 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), sW.lng()));
            cornersArray[corner1] = {
                "lat": sW.lat(),
                "lng": sW.lng()
            };
            cornersArray[corner2] = {
                "lat": nE.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner3] = {
                "lat": sW.lat(),
                "lng": nE.lng()
            };
            cornersArray[corner4] = {
                "lat": nE.lat(),
                "lng": sW.lng()
            };
            farestPoint = cornersArray[Math.max(corner1, corner2, corner3, corner4)];
            cordinate = new Map(farestPoint.lat, farestPoint.lng, "", eventName);
            this.mapRadius.emit(cordinate);
        };
        MapComponent.prototype.getCenterMapLocation = function () {
            return this.map.getCenter();
        };
        MapComponent.prototype.calculateDistance = function (aLat, aLng, bLat, bLng) {
            return google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(aLat, aLng), new google.maps.LatLng(bLat, bLng));
        };
        // Deletes all markers in the array by removing references to them.
        MapComponent.prototype.deleteMarkers = function () {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
            this.markers = [];
        };
        //listLocation[0]: code
        //listLocation[1]: Latitude (pointY)
        //listLocation[2]: Longtitude (pointX)
        //listLocation[3]: index
        //listLocation[4]: label
        MapComponent.prototype.showMultitpleLocations = function (listLocation) {
            if (listLocation === void 0) { listLocation = []; }
            var self = this;
            var infowindow = new google.maps.InfoWindow;
            var marker, i;
            var bounds = new google.maps.LatLngBounds();
            this.deleteMarkers();
            for (i = 0; i < listLocation.length; i++) {
                //marker CONFIG
                var markObject = {
                    position: new google.maps.LatLng(listLocation[i][1], listLocation[i][2]),
                    //labelOrigin: new google.maps.Point(9, 9),
                    //   label: {
                    //     text: listLocation[i][4]? listLocation[i][4]: "",
                    //     fontSize: "12px",
                    //     color: "#e74c3c",
                    //     fontFamily: "montserrat"
                    //  },
                    scaledSize: new google.maps.Size(30, 30),
                    map: this.map
                };
                markObject["icon"] = {
                    url: this.locationIcon,
                    scaledSize: new google.maps.Size(30, 30),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0) // anchor
                };
                marker = new google.maps.Marker(markObject);
                this.markers.push(marker);
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        //get address
                        var latlng = new google.maps.LatLng(listLocation[i][1], listLocation[i][2]);
                        self.geocoder.geocode({ 'latLng': latlng }, function (responses) {
                            var address;
                            if (!listLocation[i][4]) {
                                if (responses && responses.length > 0) {
                                    address = responses[0].formatted_address;
                                }
                                else {
                                    address = "Cannot determine address at this location.";
                                }
                            }
                            else {
                                address = listLocation[i][4];
                            }
                            self.activeInfoWindow && self.activeInfoWindow.close();
                            self.activeInfoWindow = infowindow;
                            infowindow.setContent(address);
                            infowindow.open(self.map, marker);
                            //marker object [title, lat, long, index]
                            if (listLocation[i][3]) {
                                self.markerClick.emit(listLocation[i][3]);
                            }
                        });
                    };
                })(marker, i));
            }
            // for (let index = 0; index < this.markers.length; index++) {
            //   bounds.extend(this.markers[index].getPosition());
            // }
            // if (self.marker) {
            //   bounds.extend(self.marker.getPosition());
            // }
            // if (self.myLocation) {
            //   bounds.extend(self.myLocation.getPosition());
            // }
            //this.map.fitBounds(bounds);
        };
        MapComponent.prototype.panTo = function (lat, lng, zoomNumber) {
            if (zoomNumber === void 0) { zoomNumber = 12; }
            var myLatLng = new google.maps.LatLng(lat, lng);
            this.map.setZoom(zoomNumber);
            this.map.panTo(myLatLng);
        };
        MapComponent.prototype.geocodePosition = function (pos, eventName, func) {
            var _this = this;
            if (eventName === void 0) { eventName = undefined; }
            if (func === void 0) { func = null; }
            this.geocoder.geocode({ 'latLng': pos }, function (responses) {
                if (responses && responses.length > 0) {
                    _this.cordinate.title = responses[0].formatted_address;
                    _this.cordinate.latitude = responses[0].geometry.location.lat();
                    _this.cordinate.longtitude = responses[0].geometry.location.lng();
                }
                else {
                    _this.cordinate.title = "Cannot determine address at this location.";
                    _this.cordinate.latitude = null;
                    _this.cordinate.longtitude = null;
                }
                _this.triggerUpdateData(eventName);
                $__default['default']("#pac-input").click();
                if (func) {
                    func();
                }
            });
        };
        MapComponent.prototype.addSearchBoxEvent = function () {
            var _this = this;
            var self = this;
            self.searchBox.addListener('places_changed', function () {
                if (self.marker) {
                    self.marker.setMap(null);
                }
                //self.deleteMarkers();
                var places = self.searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    var markerObj = {
                        map: self.map,
                        draggable: _this.markerDraggable,
                        title: place.name,
                        animation: google.maps.Animation.DROP,
                        position: place.geometry.location
                    };
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    self.marker = new google.maps.Marker(markerObj);
                    // self.markers.push(self.marker);
                    self.addDragendListener();
                    self.searchEvent.emit(new Map(markerObj.position.lat(), markerObj.position.lng()));
                    self.cordinate.latitude = place.geometry.location.lat();
                    self.cordinate.longtitude = place.geometry.location.lng();
                    self.cordinate.title = place.formatted_address;
                    self.triggerUpdateData(mapConfig.mapEventNames.search);
                    if (place.geometry.viewport) {
                        bounds.union(place.geometry.viewport);
                    }
                    else {
                        bounds.extend(place.geometry.location);
                    }
                });
                self.map.fitBounds(bounds);
                self.map.setZoom(12);
                self.map.panTo(self.marker.position);
            });
        };
        MapComponent.prototype.addClickListener = function () {
            var _this = this;
            google.maps.event.addListener(this.map, 'click', function (event) {
                //event.preventDefault();
                _this.geocodePosition(event.latLng, mapConfig.mapEventNames.mapClick, function () {
                    _this.marker.setMap(null);
                    _this.marker = new google.maps.Marker({
                        position: event.latLng,
                        map: _this.map,
                        draggable: _this.markerDraggable,
                        title: _this.cordinate.title
                    });
                    _this.addDragendListener();
                });
            });
        };
        MapComponent.prototype.addDragendListener = function () {
            var _this = this;
            google.maps.event.addListener(this.marker, 'dragend', function () {
                event.preventDefault();
                _this.deleteMarkers();
                _this.geocodePosition(_this.marker.getPosition(), mapConfig.mapEventNames.markerDrag);
                _this.cordinate.latitude = _this.marker.getPosition().lat();
                _this.cordinate.longtitude = _this.marker.getPosition().lng();
            });
        };
        MapComponent.prototype.triggerUpdateData = function (eventName) {
            this.cordinate.eventName = eventName;
            this.cordinateChange.emit(this.cordinate);
        };
        MapComponent.prototype.myCurrentLocation = function () {
            var self = this;
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (location) {
                    if (self.marker) {
                        self.marker.setMap(null);
                    }
                    // self.deleteMarkers();
                    self.cordinate.latitude = location.coords.latitude;
                    self.cordinate.longtitude = location.coords.longitude;
                    self.myLatLng = {
                        lat: location.coords.latitude, lng: location.coords.longitude
                    };
                    if (self.myLocation) {
                        self.myLocation.setMap(null);
                    }
                    self.myLocation = new google.maps.Marker({
                        position: self.myLatLng,
                        icon: {
                            url: self.greenPinIcon,
                            scaledSize: new google.maps.Size(30, 30),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(15, 15) // anchor
                        },
                        zIndex: 1000,
                        map: self.map,
                        draggable: false,
                        title: self.cordinate.title
                    });
                    self.geocodePosition(self.myLocation.getPosition(), mapConfig.mapEventNames.myLocation);
                    //self.addDragendListener();
                    self.getCurrentLocationEvent.emit(new Map(location.coords.latitude, location.coords.longitude));
                    var currentZoomLevel = self.map.getZoom();
                    var bounds = new google.maps.LatLngBounds();
                    self.map.setCenter(new google.maps.LatLng(self.myLatLng.lat, self.myLatLng.lng));
                });
            }
            else {
                console.log("Geolocation is not supported by this browser.");
            }
        };
        MapComponent.prototype.getRadius = function (distance) {
            if (distance === void 0) { distance = 100; }
            if (this.myLocation) {
                var w = this.map.getBounds().getSouthWest();
                var centerLng = this.myLocation.getPosition();
                distance = google.maps.geometry.spherical.computeDistanceBetween(centerLng, w);
            }
            //this.mapRadius.emit(distance ? (distance / 1000).toFixed(1) : "CANNOT_CALCULATE_DISTANCE");
        };
        MapComponent.prototype.getTopDistanceFromMarker = function (lat, lng, eventName) {
            if (!lat || !lng) {
                return;
            }
            var myLatLng = new google.maps.LatLng(lat, lng);
            // Get map bounds
            var bounds = this.map.getBounds();
            // Find map top border & distance to center
            //let mapTop = new google.maps.LatLng(bounds.getNorthEast().lat(), myLatLng.lng());
            //let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, mapTop);
            // Find map left border & distance to center
            // let mapLeft = new google.maps.LatLng(myLatLng.lat(), bounds.getSouthWest().lng());
            // let distanceToLeft = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, mapLeft);
            // Get shortest length
            //let radius = distanceToTop < distanceToLeft ? distanceToTop : distanceToLeft;
            //let radius = distanceToTop;
            this.getTopCordinatorEvent.emit(new Map(bounds.getNorthEast().lat(), myLatLng.lng(), "", eventName));
            // let circle = new google.maps.Circle({
            //   strokeColor: '#FF0000',
            //   strokeOpacity: 0.8,
            //   strokeWeight: 2,
            //   fillColor: '#FF0000',
            //   fillOpacity: 0.35,
            //   editable: true,
            //   map: this.map,
            //   center: myLatLng,
            //   radius: radius
            // });
        };
        MapComponent.prototype.drawCircle = function (centerLat, centerLng, pointLat, pointLng, fillColor) {
            if (fillColor === void 0) { fillColor = "#FF0000"; }
            var myLatLng = new google.maps.LatLng(centerLat, centerLng);
            // Get map bounds
            var bounds = this.map.getBounds();
            var point = new google.maps.LatLng(pointLat, pointLng);
            var distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, point);
            var radius = distanceToTop;
            this.circle.setMap(null);
            this.circle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: fillColor,
                fillOpacity: 0.35,
                editable: true,
                map: this.map,
                center: myLatLng,
                radius: radius
            });
        };
        return MapComponent;
    }());
    MapComponent.ɵfac = function MapComponent_Factory(t) { return new (t || MapComponent)(); };
    MapComponent.ɵcmp = i0.ɵɵdefineComponent({ type: MapComponent, selectors: [["via-map"]], inputs: { cordinate: "cordinate", searchPlaceholer: "searchPlaceholer", markerDraggable: "markerDraggable", customIcon: "customIcon", showMyLocation: "showMyLocation", hideSearchTextBox: "hideSearchTextBox" }, outputs: { cordinateChange: "cordinateChange", markerClick: "markerClick", mapRadius: "mapRadius", mapDragged: "mapDragged", searchEvent: "searchEvent", getTopCordinatorEvent: "getTopCordinatorEvent", getCurrentLocationEvent: "getCurrentLocationEvent" }, decls: 5, vars: 2, consts: [[1, "row"], ["class", "col-sm-12 map-search-container", 4, "ngIf"], [1, "col-sm-12"], ["id", "googleMap", 1, "google-maps"], ["class", "my-location-btn", 3, "click", 4, "ngIf"], [1, "col-sm-12", "map-search-container"], [1, "form-group"], ["id", "via-map-search"], ["id", "dummy-col"], ["type", "text", "id", "pac-input", "aria-describedby", "textHelp", "placeholder", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["src", "/assets/icons/ic_search.svg", 1, "input-search-icon"], [1, "my-location-btn", 3, "click"], ["src", "assets/icons/green_pin.svg"]], template: function MapComponent_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵelementStart(0, "div", 0);
                i0.ɵɵtemplate(1, MapComponent_div_1_Template, 6, 1, "div", 1);
                i0.ɵɵelementStart(2, "div", 2);
                i0.ɵɵelement(3, "div", 3);
                i0.ɵɵtemplate(4, MapComponent_button_4_Template, 2, 0, "button", 4);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
            }
            if (rf & 2) {
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", !ctx.hideSearchTextBox);
                i0.ɵɵadvance(3);
                i0.ɵɵproperty("ngIf", ctx.showMyLocation);
            }
        }, directives: [i2.NgIf, i3.DefaultValueAccessor, i3.NgControlStatus, i3.NgModel], styles: [".google-maps[_ngcontent-%COMP%]{border-radius:3px;height:300px}.my-location-btn[_ngcontent-%COMP%]{background:#fff;border-radius:2px;border-style:none;bottom:110px;box-shadow:0 1px 4px -1px rgba(0,0,0,.3);cursor:pointer;height:41px;padding:0!important;position:absolute;right:25px;text-align:center;width:41px}.my-location-btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:20px}.map-search-container[_ngcontent-%COMP%]{margin-bottom:10px}.map-search-container[_ngcontent-%COMP%]   #pac-input[_ngcontent-%COMP%]{padding:10px}.map-search-container[_ngcontent-%COMP%]   .input-search-icon[_ngcontent-%COMP%]{position:absolute;right:21px;top:13px;width:20px}"] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(MapComponent, [{
                type: i0.Component,
                args: [{
                        selector: 'via-map',
                        templateUrl: './map.html',
                        styleUrls: ['./map.scss']
                    }]
            }], function () { return []; }, { cordinate: [{
                    type: i0.Input
                }], searchPlaceholer: [{
                    type: i0.Input
                }], markerDraggable: [{
                    type: i0.Input
                }], customIcon: [{
                    type: i0.Input
                }], showMyLocation: [{
                    type: i0.Input
                }], hideSearchTextBox: [{
                    type: i0.Input
                }], cordinateChange: [{
                    type: i0.Output
                }], markerClick: [{
                    type: i0.Output
                }], mapRadius: [{
                    type: i0.Output
                }], mapDragged: [{
                    type: i0.Output
                }], searchEvent: [{
                    type: i0.Output
                }], getTopCordinatorEvent: [{
                    type: i0.Output
                }], getCurrentLocationEvent: [{
                    type: i0.Output
                }] });
    })();

    function myLibInit(apiKey) {
        var x = function (config) { return function () { return config.load(apiKey); }; };
        return x;
    }
    var MapModule = /** @class */ (function () {
        function MapModule() {
        }
        return MapModule;
    }());
    MapModule.ɵmod = i0.ɵɵdefineNgModule({ type: MapModule });
    MapModule.ɵinj = i0.ɵɵdefineInjector({ factory: function MapModule_Factory(t) { return new (t || MapModule)(); }, imports: [[i3.FormsModule, i2.CommonModule]] });
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(MapModule, { declarations: [MapComponent], imports: [i3.FormsModule, i2.CommonModule], exports: [MapComponent] }); })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(MapModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [MapComponent],
                        imports: [i3.FormsModule, i2.CommonModule],
                        exports: [MapComponent]
                    }]
            }], null, null);
    })();

    var languageObj = {
        enter_text: "Please enter the text",
        no_data_found: "No items to select"
    };

    var TypeaheadItem = /** @class */ (function () {
        function TypeaheadItem(name, value) {
            this.name = name;
            this.value = value;
        }
        return TypeaheadItem;
    }());

    var ClickOutSiteDirective = /** @class */ (function () {
        function ClickOutSiteDirective(el) {
            this.el = el;
            this.tohClickOutSite = new i0.EventEmitter();
            this.clickOutSite = new i0.EventEmitter();
        }
        ClickOutSiteDirective.prototype.documentClick = function ($event) {
            if (this.tohClickOutSite.observers.length > 0) {
                if (this.el.nativeElement.innerHTML.indexOf($event.target.innerHTML) === -1) {
                    this.tohClickOutSite.emit(null);
                }
            }
            else if (this.clickOutSite.observers.length > 0) {
                if (!this.el.nativeElement.contains($event.target)) {
                    this.clickOutSite.emit(null);
                }
            }
        };
        return ClickOutSiteDirective;
    }());
    ClickOutSiteDirective.ɵfac = function ClickOutSiteDirective_Factory(t) { return new (t || ClickOutSiteDirective)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
    ClickOutSiteDirective.ɵdir = i0.ɵɵdefineDirective({ type: ClickOutSiteDirective, selectors: [["", "tohClickOutSite", ""]], hostBindings: function ClickOutSiteDirective_HostBindings(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵlistener("click", function ClickOutSiteDirective_click_HostBindingHandler($event) { return ctx.documentClick($event); }, false, i0.ɵɵresolveDocument);
            }
        }, outputs: { tohClickOutSite: "tohClickOutSite", clickOutSite: "clickOutSite" } });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(ClickOutSiteDirective, [{
                type: i0.Directive,
                args: [{
                        selector: "[tohClickOutSite]"
                    }]
            }], function () { return [{ type: i0.ElementRef }]; }, { tohClickOutSite: [{
                    type: i0.Output
                }], clickOutSite: [{
                    type: i0.Output
                }], documentClick: [{
                    type: i0.HostListener,
                    args: ["document:click", ["$event"]]
                }] });
    })();

    function TypeaheadComponent_span_4_li_1_span_3_Template(rf, ctx) {
        if (rf & 1) {
            var _r11_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "span", 14);
            i0.ɵɵlistener("click", function TypeaheadComponent_span_4_li_1_span_3_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r11_1); var item_r7 = i0.ɵɵnextContext().$implicit; var ctx_r9 = i0.ɵɵnextContext(2); return ctx_r9.remove(item_r7); });
            i0.ɵɵelement(1, "i", 15);
            i0.ɵɵelementEnd();
        }
    }
    function TypeaheadComponent_span_4_li_1_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "li", 12);
            i0.ɵɵelementStart(1, "span", 13);
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(3, TypeaheadComponent_span_4_li_1_span_3_Template, 2, 0, "span", 9);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var item_r7 = ctx.$implicit;
            var ctx_r6 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(1);
            i0.ɵɵpropertyInterpolate("title", item_r7.name);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(item_r7.name);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r6.disabled);
        }
    }
    function TypeaheadComponent_span_4_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span");
            i0.ɵɵtemplate(1, TypeaheadComponent_span_4_li_1_Template, 4, 3, "li", 11);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r0 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", ctx_r0.options.itemsSelected);
        }
    }
    function TypeaheadComponent_span_5_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 16);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r1 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(ctx_r1.options.itemsSelected[0].name);
        }
    }
    function TypeaheadComponent_input_7_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "input", 17);
        }
        if (rf & 2) {
            var ctx_r2 = i0.ɵɵnextContext();
            i0.ɵɵpropertyInterpolate("placeholder", ctx_r2.languageObj.enter_text);
            i0.ɵɵproperty("formControl", ctx_r2.searchBox);
        }
    }
    function TypeaheadComponent_span_8_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 18);
            i0.ɵɵelement(1, "i", 19);
            i0.ɵɵelementEnd();
        }
    }
    function TypeaheadComponent_span_9_Template(rf, ctx) {
        if (rf & 1) {
            var _r13_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "span", 14);
            i0.ɵɵlistener("click", function TypeaheadComponent_span_9_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r13_1); var ctx_r12 = i0.ɵɵnextContext(); return ctx_r12.removeSingleItem(); });
            i0.ɵɵelement(1, "i", 15);
            i0.ɵɵelementEnd();
        }
    }
    function TypeaheadComponent_ul_10_li_1_Template(rf, ctx) {
        if (rf & 1) {
            var _r18_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "li", 23);
            i0.ɵɵlistener("click", function TypeaheadComponent_ul_10_li_1_Template_li_click_0_listener() { i0.ɵɵrestoreView(_r18_1); var item_r16 = ctx.$implicit; var ctx_r17 = i0.ɵɵnextContext(2); return ctx_r17.select(item_r16); });
            i0.ɵɵelementStart(1, "span");
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var item_r16 = ctx.$implicit;
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(item_r16.name);
        }
    }
    function TypeaheadComponent_ul_10_li_2_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "li", 24);
            i0.ɵɵelementStart(1, "span");
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r15 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx_r15.languageObj.no_data_found);
        }
    }
    function TypeaheadComponent_ul_10_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "ul", 20);
            i0.ɵɵtemplate(1, TypeaheadComponent_ul_10_li_1_Template, 3, 1, "li", 21);
            i0.ɵɵtemplate(2, TypeaheadComponent_ul_10_li_2_Template, 3, 1, "li", 22);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r5 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", ctx_r5.options.items);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r5.options.noDataFound);
        }
    }
    var TypeaheadComponent = /** @class */ (function () {
        function TypeaheadComponent() {
            this.languageObj = languageObj;
            this.disabled = false;
            this.typeaheadLoadData = new i0.EventEmitter();
            this.typeaheadItemSelected = new i0.EventEmitter();
            this.searchBox = new i3.FormControl();
            //prevent duplicate search when set value for search box
            this.isSettingValue = false;
            //super();
        }
        TypeaheadComponent.prototype.hide = function () {
            this.options.items = [];
            this.options.errorMessage = "";
            this.options.isLoading = false;
            this.options.noDataFound = false;
            if (this.options.inlineSingleSearch == false) {
                //this.isSettingValue = true;
                this.searchBox.setValue("");
            }
            else if (this.isSettingValue == false && this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
                //this.isSettingValue = true;
                //this.searchBox.setValue(this.options.itemsSelected[0].name);
            }
        };
        TypeaheadComponent.prototype.isSingleInlineSearch = function () {
            return this.options.inlineSingleSearch == true && this.options.multiple == false;
        };
        TypeaheadComponent.prototype.select = function (item) {
            this.options.items = this.options.items.filter(function (filterItem) {
                return filterItem.value !== item.value;
            });
            if (!this.options.multiple) {
                this.options.itemsSelected = [];
            }
            this.options.itemsSelected.push(item);
            this.options.noDataFound = this.options.items.length === 0;
            this.typeaheadItemSelected.emit(item);
            if (this.isSingleInlineSearch() == true) {
                this.isSettingValue = true;
                this.searchBox.setValue(item.name);
            }
        };
        TypeaheadComponent.prototype.remove = function (item) {
            this.options.items.push(item);
            this.options.itemsSelected = this.options.itemsSelected.filter(function (filterItem) {
                return filterItem.value !== item.value;
            });
            this.options.noDataFound = this.options.items.length === 0;
        };
        TypeaheadComponent.prototype.removeSingleItem = function () {
            this.isSettingValue = true;
            this.searchBox.setValue("");
            this.options.itemsSelected = [];
            this.typeaheadItemSelected.emit(new TypeaheadItem(undefined, undefined));
        };
        TypeaheadComponent.prototype.ngAfterViewInit = function () {
            var _this = this;
            if (this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
                this.searchBox.setValue(this.options.itemsSelected[0].name);
                this.isSettingValue = true;
            }
            this.searchBox.valueChanges.pipe(operators.debounceTime(this.options.debounceTime), operators.distinctUntilChanged()).subscribe(function (searchText) {
                if (searchText.length >= _this.options.minLengthToLoad && _this.isSettingValue == false) {
                    _this.options.isLoading = true;
                    _this.typeaheadLoadData.emit(searchText);
                }
                _this.isSettingValue = false;
            });
        };
        TypeaheadComponent.prototype.setSearchBoxValue = function (text) {
            this.searchBox.setValue(text);
            this.isSettingValue = true;
        };
        return TypeaheadComponent;
    }());
    TypeaheadComponent.ɵfac = function TypeaheadComponent_Factory(t) { return new (t || TypeaheadComponent)(); };
    TypeaheadComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TypeaheadComponent, selectors: [["via-typeahead"]], inputs: { languageObj: "languageObj", options: "options", disabled: "disabled" }, outputs: { typeaheadLoadData: "typeaheadLoadData", typeaheadItemSelected: "typeaheadItemSelected" }, decls: 11, vars: 6, consts: [["id", "typeahead", 3, "tohClickOutSite"], [1, "typeahead-container"], [1, "typeahead"], [1, "typeahead-items-selected"], [4, "ngIf"], ["class", "inline-disabled", 4, "ngIf"], [1, "typeahead-menu-item-input"], ["autocomplete", "off", "class", "typeahead-input", 3, "placeholder", "formControl", 4, "ngIf"], ["class", "ic-spinner", 4, "ngIf"], ["class", "ic-remove", 3, "click", 4, "ngIf"], ["class", "typeahead-menu-items z-depth-1", 4, "ngIf"], ["class", "typeahead-menu-item-selected", 4, "ngFor", "ngForOf"], [1, "typeahead-menu-item-selected"], [1, "item-display", 3, "title"], [1, "ic-remove", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-times"], [1, "inline-disabled"], ["autocomplete", "off", 1, "typeahead-input", 3, "placeholder", "formControl"], [1, "ic-spinner"], ["id", "circle-o-notch-spin", "aria-hidden", "true", 1, "fa", "fa-circle-o-notch", "fa-spin"], [1, "typeahead-menu-items", "z-depth-1"], ["class", "typeahead-menu-item", 3, "click", 4, "ngFor", "ngForOf"], ["class", "typeahead-menu-item", 4, "ngIf"], [1, "typeahead-menu-item", 3, "click"], [1, "typeahead-menu-item"]], template: function TypeaheadComponent_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵelementStart(0, "div", 0);
                i0.ɵɵlistener("tohClickOutSite", function TypeaheadComponent_Template_div_tohClickOutSite_0_listener() { return ctx.hide(); });
                i0.ɵɵelementStart(1, "ul", 1);
                i0.ɵɵelementStart(2, "li", 2);
                i0.ɵɵelementStart(3, "ul", 3);
                i0.ɵɵtemplate(4, TypeaheadComponent_span_4_Template, 2, 1, "span", 4);
                i0.ɵɵtemplate(5, TypeaheadComponent_span_5_Template, 2, 1, "span", 5);
                i0.ɵɵelementStart(6, "li", 6);
                i0.ɵɵtemplate(7, TypeaheadComponent_input_7_Template, 1, 2, "input", 7);
                i0.ɵɵtemplate(8, TypeaheadComponent_span_8_Template, 2, 0, "span", 8);
                i0.ɵɵtemplate(9, TypeaheadComponent_span_9_Template, 2, 0, "span", 9);
                i0.ɵɵtemplate(10, TypeaheadComponent_ul_10_Template, 3, 2, "ul", 10);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
            }
            if (rf & 2) {
                i0.ɵɵadvance(4);
                i0.ɵɵproperty("ngIf", !ctx.options.inlineSingleSearch);
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", ctx.options.inlineSingleSearch && ctx.disabled == true && ctx.options.itemsSelected.length);
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("ngIf", ctx.disabled == false);
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", ctx.options.isLoading);
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", ctx.isSingleInlineSearch() && ctx.options.itemsSelected.length && ctx.options.isLoading == false && !ctx.disabled);
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", ctx.options.items.length > 0 || ctx.options.noDataFound);
            }
        }, directives: [ClickOutSiteDirective, i2.NgIf, i2.NgForOf, i3.DefaultValueAccessor, i3.NgControlStatus, i3.FormControlDirective], styles: ["#typeahead[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:row;width:100%}#typeahead[_ngcontent-%COMP%]   .inline-disabled[_ngcontent-%COMP%]{padding-left:5px;padding-top:5px}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:1px 5px;z-index:1}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]{margin:0;width:100%}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;list-style:none;margin:5px 0;padding:0}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]{background:#999;border:1px solid #ebebeb;border-radius:3px;border-radius:5px;color:#fff;display:flex;flex-direction:row;margin:0 10px 0 0;max-width:200px;min-width:120px;padding:5px}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]   .item-display[_ngcontent-%COMP%]{flex:9;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:100%}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]   .ic-remove[_ngcontent-%COMP%]{flex:1;font-size:14px;text-align:right}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]{align-items:center;border:0;color:#999;display:flex;flex:1;height:34px;margin:0;width:auto}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]   .typeahead-input[_ngcontent-%COMP%]{border:0;flex:9}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]   .ic-spinner[_ngcontent-%COMP%]{flex:1;text-align:right}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:flex;flex-direction:column;left:15px;list-style:none;margin:0;max-height:300px;overflow:auto;padding:0;position:absolute;top:90%;width:calc(100% - 30px)}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]   .typeahead-menu-item[_ngcontent-%COMP%]{margin:0;padding:10px}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]   .typeahead-menu-item[_ngcontent-%COMP%]:hover{background:#f7f8f9}#typeahead[_ngcontent-%COMP%]   .typeahead-arrow[_ngcontent-%COMP%]{flex:1;text-align:right}#typeahead[_ngcontent-%COMP%]   .typeahead-arrow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#999;font-size:24px}"] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(TypeaheadComponent, [{
                type: i0.Component,
                args: [{
                        selector: "via-typeahead",
                        templateUrl: "./typeahead.html",
                        styleUrls: ["./typeahead.scss"]
                    }]
            }], function () { return []; }, { languageObj: [{
                    type: i0.Input
                }], options: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], typeaheadLoadData: [{
                    type: i0.Output
                }], typeaheadItemSelected: [{
                    type: i0.Output
                }] });
    })();

    var ClickOutSiteModule = /** @class */ (function () {
        function ClickOutSiteModule() {
        }
        return ClickOutSiteModule;
    }());
    ClickOutSiteModule.ɵmod = i0.ɵɵdefineNgModule({ type: ClickOutSiteModule });
    ClickOutSiteModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ClickOutSiteModule_Factory(t) { return new (t || ClickOutSiteModule)(); }, imports: [[]] });
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ClickOutSiteModule, { declarations: [ClickOutSiteDirective], exports: [ClickOutSiteDirective] }); })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(ClickOutSiteModule, [{
                type: i0.NgModule,
                args: [{
                        imports: [],
                        declarations: [ClickOutSiteDirective],
                        exports: [ClickOutSiteDirective]
                    }]
            }], null, null);
    })();

    var TypeaheadModule = /** @class */ (function () {
        function TypeaheadModule() {
        }
        return TypeaheadModule;
    }());
    TypeaheadModule.ɵmod = i0.ɵɵdefineNgModule({ type: TypeaheadModule });
    TypeaheadModule.ɵinj = i0.ɵɵdefineInjector({ factory: function TypeaheadModule_Factory(t) { return new (t || TypeaheadModule)(); }, imports: [[
                i1.NgbModule,
                ClickOutSiteModule,
                i2.CommonModule,
                i3.FormsModule,
                i3.ReactiveFormsModule
            ]] });
    (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(TypeaheadModule, { declarations: [TypeaheadComponent], imports: [i1.NgbModule,
                ClickOutSiteModule,
                i2.CommonModule,
                i3.FormsModule,
                i3.ReactiveFormsModule], exports: [TypeaheadComponent] });
    })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(TypeaheadModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [TypeaheadComponent],
                        imports: [
                            i1.NgbModule,
                            ClickOutSiteModule,
                            i2.CommonModule,
                            i3.FormsModule,
                            i3.ReactiveFormsModule
                        ],
                        exports: [TypeaheadComponent]
                    }]
            }], null, null);
    })();

    var TypeaheadOptions = /** @class */ (function () {
        function TypeaheadOptions(url, multiple, inlineSingleSearch) {
            if (url === void 0) { url = ""; }
            if (multiple === void 0) { multiple = true; }
            if (inlineSingleSearch === void 0) { inlineSingleSearch = false; }
            this._url = url;
            this._multiple = multiple;
            this._inlineSingleSearch = inlineSingleSearch;
            this._debounceTime = 400;
            this._itemsSelected = new Array();
            this._items = new Array();
            this._errorMessage = "";
            this._isLoading = false;
            this._minLengthToLoad = 1;
            this._noDataFound = false;
        }
        Object.defineProperty(TypeaheadOptions.prototype, "url", {
            /**
             * Getter url
             * return {string}
             */
            get: function () {
                return this._url;
            },
            /**
             * Setter url
             * param {string} value
             */
            set: function (value) {
                this._url = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TypeaheadOptions.prototype, "multiple", {
            /**
             * Getter multiple
             * return {boolean}
             */
            get: function () {
                return this._multiple;
            },
            /**
             * Setter multiple
             * param {boolean} value
             */
            set: function (value) {
                this._multiple = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TypeaheadOptions.prototype, "inlineSingleSearch", {
            /**
             * Getter inlineSingleSearch
             * return {boolean}
             */
            get: function () {
                return this._inlineSingleSearch;
            },
            /**
             * Setter inlineSingleSearch
             * param {boolean} value
             */
            set: function (value) {
                this._inlineSingleSearch = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TypeaheadOptions.prototype, "debounceTime", {
            /**
             * Getter debounceTime
             * return {number}
             */
            get: function () {
                return this._debounceTime;
            },
            /**
             * Setter debounceTime
             * param {number} value
             */
            set: function (value) {
                this._debounceTime = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TypeaheadOptions.prototype, "itemsSelected", {
            /**
             * Getter itemsSelected
             * return {TypeaheadItem[]}
             */
            get: function () {
                return this._itemsSelected;
            },
            /**
             * Setter itemsSelected
             * param {TypeaheadItem[]} value
             */
            set: function (value) {
                this._itemsSelected = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TypeaheadOptions.prototype, "items", {
            /**
             * Getter items
             * return {TypeaheadItem[]}
             */
            get: function () {
                return this._items;
            },
            /**
             * Setter items
             * param {TypeaheadItem[]} value
             */
            set: function (value) {
                this._items = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TypeaheadOptions.prototype, "errorMessage", {
            /**
             * Getter errorMessage
             * return {string}
             */
            get: function () {
                return this._errorMessage;
            },
            /**
             * Setter errorMessage
             * param {string} value
             */
            set: function (value) {
                this._errorMessage = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TypeaheadOptions.prototype, "isLoading", {
            /**
             * Getter isLoading
             * return {boolean}
             */
            get: function () {
                return this._isLoading;
            },
            /**
             * Setter isLoading
             * param {boolean} value
             */
            set: function (value) {
                this._isLoading = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TypeaheadOptions.prototype, "minLengthToLoad", {
            /**
             * Getter minLengthToLoad
             * return {number}
             */
            get: function () {
                return this._minLengthToLoad;
            },
            /**
             * Setter minLengthToLoad
             * param {number} value
             */
            set: function (value) {
                this._minLengthToLoad = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TypeaheadOptions.prototype, "noDataFound", {
            /**
             * Getter noDataFound
             * return {boolean}
             */
            get: function () {
                return this._noDataFound;
            },
            /**
             * Setter noDataFound
             * param {boolean} value
             */
            set: function (value) {
                this._noDataFound = value;
            },
            enumerable: false,
            configurable: true
        });
        return TypeaheadOptions;
    }());

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (Object.prototype.hasOwnProperty.call(b, p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    var __assign = function () {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p))
                        t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function __rest(s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }
    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
            r = Reflect.decorate(decorators, target, key, desc);
        else
            for (var i = decorators.length - 1; i >= 0; i--)
                if (d = decorators[i])
                    r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); };
    }
    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
            return Reflect.metadata(metadataKey, metadataValue);
    }
    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            } }
            function rejected(value) { try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }
    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function () { if (t[0] & 1)
                throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f)
                throw new TypeError("Generator is already executing.");
            while (_)
                try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                        return t;
                    if (y = 0, t)
                        op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0:
                        case 1:
                            t = op;
                            break;
                        case 4:
                            _.label++;
                            return { value: op[1], done: false };
                        case 5:
                            _.label++;
                            y = op[1];
                            op = [0];
                            continue;
                        case 7:
                            op = _.ops.pop();
                            _.trys.pop();
                            continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                _ = 0;
                                continue;
                            }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                _.label = op[1];
                                break;
                            }
                            if (op[0] === 6 && _.label < t[1]) {
                                _.label = t[1];
                                t = op;
                                break;
                            }
                            if (t && _.label < t[2]) {
                                _.label = t[2];
                                _.ops.push(op);
                                break;
                            }
                            if (t[2])
                                _.ops.pop();
                            _.trys.pop();
                            continue;
                    }
                    op = body.call(thisArg, _);
                }
                catch (e) {
                    op = [6, e];
                    y = 0;
                }
                finally {
                    f = t = 0;
                }
            if (op[0] & 5)
                throw op[1];
            return { value: op[0] ? op[1] : void 0, done: true };
        }
    }
    var __createBinding = Object.create ? (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        Object.defineProperty(o, k2, { enumerable: true, get: function () { return m[k]; } });
    }) : (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        o[k2] = m[k];
    });
    function __exportStar(m, o) {
        for (var p in m)
            if (p !== "default" && !Object.prototype.hasOwnProperty.call(o, p))
                __createBinding(o, m, p);
    }
    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m)
            return m.call(o);
        if (o && typeof o.length === "number")
            return {
                next: function () {
                    if (o && i >= o.length)
                        o = void 0;
                    return { value: o && o[i++], done: !o };
                }
            };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }
    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++)
            s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }
    ;
    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }
    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n])
            i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try {
            step(g[n](v));
        }
        catch (e) {
            settle(q[0][3], e);
        } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length)
            resume(q[0][0], q[0][1]); }
    }
    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }
    function __asyncValues(o) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
    }
    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) {
            Object.defineProperty(cooked, "raw", { value: raw });
        }
        else {
            cooked.raw = raw;
        }
        return cooked;
    }
    ;
    var __setModuleDefault = Object.create ? (function (o, v) {
        Object.defineProperty(o, "default", { enumerable: true, value: v });
    }) : function (o, v) {
        o["default"] = v;
    };
    function __importStar(mod) {
        if (mod && mod.__esModule)
            return mod;
        var result = {};
        if (mod != null)
            for (var k in mod)
                if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k))
                    __createBinding(result, mod, k);
        __setModuleDefault(result, mod);
        return result;
    }
    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }
    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }
    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var TypeaheadItemValue = /** @class */ (function (_super) {
        __extends(TypeaheadItemValue, _super);
        function TypeaheadItemValue(name, value, data) {
            var _this = _super.call(this, name, value) || this;
            _this.data = data;
            return _this;
        }
        return TypeaheadItemValue;
    }(TypeaheadItem));

    var _c0 = ["custom-toast-component", ""];
    function CustomToast_div_6_p_1_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "p", 6);
        }
        if (rf & 2) {
            var item_r1 = i0.ɵɵnextContext().$implicit;
            i0.ɵɵproperty("innerHtml", item_r1, i0.ɵɵsanitizeHtml);
        }
    }
    function CustomToast_div_6_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div");
            i0.ɵɵtemplate(1, CustomToast_div_6_p_1_Template, 1, 1, "p", 5);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var item_r1 = ctx.$implicit;
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", item_r1 != "undefined");
        }
    }
    var CustomToast = /** @class */ (function (_super) {
        __extends(CustomToast, _super);
        function CustomToast(toastrService, toastPackage) {
            var _this = _super.call(this, toastrService, toastPackage) || this;
            _this.toastrService = toastrService;
            _this.toastPackage = toastPackage;
            _this.listMessages = _this.message.toString().split("||");
            return _this;
        }
        return CustomToast;
    }(i1$1.Toast));
    CustomToast.ɵfac = function CustomToast_Factory(t) { return new (t || CustomToast)(i0.ɵɵdirectiveInject(i1$1.ToastrService), i0.ɵɵdirectiveInject(i1$1.ToastPackage)); };
    CustomToast.ɵcmp = i0.ɵɵdefineComponent({ type: CustomToast, selectors: [["", "custom-toast-component", ""]], features: [i0.ɵɵInheritDefinitionFeature], attrs: _c0, decls: 7, vars: 2, consts: [[1, "win-alert"], [1, "fa", "fa-close", "action"], [1, "title"], [1, "detail"], [4, "ngFor", "ngForOf"], [3, "innerHtml", 4, "ngIf"], [3, "innerHtml"]], template: function CustomToast_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵelementStart(0, "div", 0);
                i0.ɵɵelement(1, "i", 1);
                i0.ɵɵelementStart(2, "div", 2);
                i0.ɵɵelementStart(3, "span");
                i0.ɵɵtext(4);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementStart(5, "div", 3);
                i0.ɵɵtemplate(6, CustomToast_div_6_Template, 2, 1, "div", 4);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
            }
            if (rf & 2) {
                i0.ɵɵadvance(4);
                i0.ɵɵtextInterpolate(ctx.title);
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("ngForOf", ctx.listMessages);
            }
        }, directives: [i2.NgForOf, i2.NgIf], encapsulation: 2, data: { animation: [
                animations.trigger('flyInOut', []),
            ] } });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(CustomToast, [{
                type: i0.Component,
                args: [{
                        selector: '[custom-toast-component]',
                        templateUrl: "toast.html",
                        animations: [
                            animations.trigger('flyInOut', []),
                        ],
                        preserveWhitespaces: false
                    }]
            }], function () { return [{ type: i1$1.ToastrService }, { type: i1$1.ToastPackage }]; }, null);
    })();

    var ToastModule = /** @class */ (function () {
        function ToastModule() {
        }
        return ToastModule;
    }());
    ToastModule.ɵmod = i0.ɵɵdefineNgModule({ type: ToastModule });
    ToastModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ToastModule_Factory(t) { return new (t || ToastModule)(); }, imports: [[
                i3.FormsModule,
                i3.ReactiveFormsModule,
                i2.CommonModule
            ]] });
    (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ToastModule, { declarations: [CustomToast], imports: [i3.FormsModule,
                i3.ReactiveFormsModule,
                i2.CommonModule], exports: [CustomToast] });
    })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(ToastModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [CustomToast],
                        imports: [
                            i3.FormsModule,
                            i3.ReactiveFormsModule,
                            i2.CommonModule
                        ],
                        exports: [CustomToast]
                    }]
            }], null, null);
    })();

    var modalConfigs = {
        alert: "ALERT",
        confirm: "CONFIRM"
    };

    function WinModalComponent_div_0_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 8);
            i0.ɵɵelement(1, "img", 9);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r0 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("src", ctx_r0.modalOptions.modalLogo, i0.ɵɵsanitizeUrl);
        }
    }
    function WinModalComponent_button_9_Template(rf, ctx) {
        if (rf & 1) {
            var _r3_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "button", 10);
            i0.ɵɵlistener("click", function WinModalComponent_button_9_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r3_1); var ctx_r2 = i0.ɵɵnextContext(); return ctx_r2.close(); });
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r1 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r1.modalOptions.noBtnTitle, " ");
        }
    }
    var WinModalComponent = /** @class */ (function () {
        function WinModalComponent(activeModal) {
            this.activeModal = activeModal;
            this.onConfirm = new i0.EventEmitter();
            this.modalTypes = modalConfigs;
            //super();
        }
        WinModalComponent.prototype.close = function () {
            this.activeModal.dismiss("close");
        };
        WinModalComponent.prototype.ok = function () {
            this.activeModal.close("ok");
        };
        WinModalComponent.prototype.handleCloseByIcon = function () {
            if (this.modalOptions.type === modalConfigs.confirm) {
                this.close();
            }
            else {
                this.ok();
            }
        };
        return WinModalComponent;
    }());
    WinModalComponent.ɵfac = function WinModalComponent_Factory(t) { return new (t || WinModalComponent)(i0.ɵɵdirectiveInject(i1.NgbActiveModal)); };
    WinModalComponent.ɵcmp = i0.ɵɵdefineComponent({ type: WinModalComponent, selectors: [["via-modal"]], inputs: { modalOptions: "modalOptions" }, outputs: { onConfirm: "onConfirm" }, decls: 10, vars: 5, consts: [["class", "center-text m-t-20", 4, "ngIf"], [1, "center-text"], [1, "modal-title"], [1, "modal-body", "modal-body-text-p"], [3, "innerHtml"], [1, "modal-footer"], ["type", "button", 1, "btn", "uppercase-text", "btn-primary", "modal-action-btn", 3, "click"], ["type", "button", "class", "btn uppercase-text btn-light modal-action-btn", 3, "click", 4, "ngIf"], [1, "center-text", "m-t-20"], [2, "margin", "0px auto", 3, "src"], ["type", "button", 1, "btn", "uppercase-text", "btn-light", "modal-action-btn", 3, "click"]], template: function WinModalComponent_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵtemplate(0, WinModalComponent_div_0_Template, 2, 1, "div", 0);
                i0.ɵɵelementStart(1, "div", 1);
                i0.ɵɵelementStart(2, "h4", 2);
                i0.ɵɵtext(3);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementStart(4, "div", 3);
                i0.ɵɵelement(5, "p", 4);
                i0.ɵɵelementEnd();
                i0.ɵɵelementStart(6, "div", 5);
                i0.ɵɵelementStart(7, "button", 6);
                i0.ɵɵlistener("click", function WinModalComponent_Template_button_click_7_listener() { return ctx.ok(); });
                i0.ɵɵtext(8);
                i0.ɵɵelementEnd();
                i0.ɵɵtemplate(9, WinModalComponent_button_9_Template, 2, 1, "button", 7);
                i0.ɵɵelementEnd();
            }
            if (rf & 2) {
                i0.ɵɵproperty("ngIf", ctx.modalOptions.modalLogo);
                i0.ɵɵadvance(3);
                i0.ɵɵtextInterpolate(ctx.modalOptions.modalTitle);
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("innerHtml", ctx.modalOptions.modalContent, i0.ɵɵsanitizeHtml);
                i0.ɵɵadvance(3);
                i0.ɵɵtextInterpolate1(" ", ctx.modalOptions.yesBtnTitle, " ");
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", ctx.modalOptions.type === ctx.modalTypes.confirm);
            }
        }, directives: [i2.NgIf], encapsulation: 2 });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(WinModalComponent, [{
                type: i0.Component,
                args: [{
                        selector: 'via-modal',
                        templateUrl: './modal.html'
                    }]
            }], function () { return [{ type: i1.NgbActiveModal }]; }, { modalOptions: [{
                    type: i0.Input
                }], onConfirm: [{
                    type: i0.Output
                }] });
    })();

    var Modal = /** @class */ (function () {
        function Modal(modalTitle, yesBtnTitle, noBtnTitle, type, translateParams, modalContent, modalLogo) {
            if (noBtnTitle === void 0) { noBtnTitle = "close"; }
            this.modalTitle = modalTitle;
            this.modalContent = modalContent;
            this.yesBtnTitle = yesBtnTitle;
            this.noBtnTitle = noBtnTitle;
            this.type = type;
            this.translateParams = translateParams;
            this.modalLogo = modalLogo;
        }
        return Modal;
    }());

    var WinModalModule = /** @class */ (function () {
        function WinModalModule() {
        }
        return WinModalModule;
    }());
    WinModalModule.ɵmod = i0.ɵɵdefineNgModule({ type: WinModalModule });
    WinModalModule.ɵinj = i0.ɵɵdefineInjector({ factory: function WinModalModule_Factory(t) { return new (t || WinModalModule)(); }, imports: [[
                i1.NgbModule,
                i3.FormsModule,
                i2.CommonModule,
                i3.ReactiveFormsModule
            ]] });
    (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(WinModalModule, { declarations: [WinModalComponent], imports: [i1.NgbModule,
                i3.FormsModule,
                i2.CommonModule,
                i3.ReactiveFormsModule], exports: [WinModalComponent] });
    })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(WinModalModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [WinModalComponent],
                        imports: [
                            i1.NgbModule,
                            i3.FormsModule,
                            i2.CommonModule,
                            i3.ReactiveFormsModule
                        ],
                        exports: [WinModalComponent]
                    }]
            }], null, null);
    })();

    var DATE_FORMAT$1 = "hh:mma dd/MM/yyyy";
    var DATE_FORMAT_WITHOUT_TIME$1 = "dd/MM/yyyy";

    var I18N_VALUES = {
        en: {
            weekdays: [
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa",
                "Su"
            ],
            months: [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
            ]
        },
        vi: {
            weekdays: [
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7",
                "CN"
            ],
            months: [
                "T1",
                "T2",
                "T3",
                "T4",
                "T5",
                "T6",
                "T7",
                "T8",
                "T9",
                "T10",
                "T11",
                "T12"
            ]
        }
    };
    // Define a service holding the language. You probably already have one if your app is i18ned.
    var I18n = /** @class */ (function () {
        function I18n() {
            this.language = "vi";
        }
        return I18n;
    }());
    I18n.ɵfac = function I18n_Factory(t) { return new (t || I18n)(); };
    I18n.ɵprov = i0.ɵɵdefineInjectable({ token: I18n, factory: I18n.ɵfac });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(I18n, [{
                type: i0.Injectable
            }], null, null);
    })();
    // Define custom service providing the months and weekdays translations
    var CustomDatepickerI18n = /** @class */ (function (_super) {
        __extends(CustomDatepickerI18n, _super);
        function CustomDatepickerI18n(_i18n) {
            var _this = _super.call(this) || this;
            _this._i18n = _i18n;
            return _this;
        }
        CustomDatepickerI18n.prototype.getWeekdayShortName = function (weekday) {
            return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
        };
        CustomDatepickerI18n.prototype.getMonthShortName = function (month) {
            return I18N_VALUES[this._i18n.language].months[month - 1];
        };
        CustomDatepickerI18n.prototype.getMonthFullName = function (month) {
            return this.getMonthShortName(month);
        };
        CustomDatepickerI18n.prototype.getDayAriaLabel = function (date) {
            return date.day + "-" + date.month + "-" + date.year;
        };
        return CustomDatepickerI18n;
    }(i1.NgbDatepickerI18n));
    CustomDatepickerI18n.ɵfac = function CustomDatepickerI18n_Factory(t) { return new (t || CustomDatepickerI18n)(i0.ɵɵinject(I18n)); };
    CustomDatepickerI18n.ɵprov = i0.ɵɵdefineInjectable({ token: CustomDatepickerI18n, factory: CustomDatepickerI18n.ɵfac });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(CustomDatepickerI18n, [{
                type: i0.Injectable
            }], function () { return [{ type: I18n }]; }, null);
    })();
    var DatepickerComponent = /** @class */ (function () {
        function DatepickerComponent(_i18n, formatter, calendar) {
            this._i18n = _i18n;
            this.formatter = formatter;
            this.calendar = calendar;
            this.today = new Date();
            this.minDate = {
                day: 1,
                month: 1,
                year: this.today.getFullYear() - 100
            };
            this.disabled = false;
            this.datepickerItemChange = new i0.EventEmitter();
            this.dateFormat = DATE_FORMAT_WITHOUT_TIME$1;
            //super();
        }
        DatepickerComponent.prototype.ngOnInit = function () {
            this._i18n.language = "en";
        };
        DatepickerComponent.prototype.setLanguage = function (language) {
            this._i18n.language = language;
            this.placeholder = this._i18n.language == "en" ? "dd/mm/yyyy" : "nn/tt/nnnn";
        };
        DatepickerComponent.prototype.selectToday = function () {
            this.datepickerItem = { year: 2021, month: 6, day: 7 };
        };
        DatepickerComponent.prototype.getDateSelected = function () {
            if (this.isValid(this.datepickerItem)) {
                return new Date(this.datepickerItem.year, this.datepickerItem.month - 1, this.datepickerItem.day);
            }
            return null;
        };
        DatepickerComponent.prototype.dateSelect = function (date) {
            this.datepickerItemChange.emit(date);
        };
        DatepickerComponent.prototype.isValid = function (obj) {
            return obj !== null && obj !== undefined;
        };
        DatepickerComponent.prototype.validateInput = function (currentValue, input) {
            var parsed = this.formatter.parse(input);
            var value;
            if ((parsed && this.calendar.isValid(i1.NgbDate.from(parsed))) || parsed == undefined || parsed == null) {
                value = i1.NgbDate.from(parsed);
            }
            else {
                value = null;
            }
            this.dateSelect(value);
            return value;
        };
        DatepickerComponent.prototype.format = function (datepickerItem) {
            return this.formatter.format(datepickerItem);
        };
        DatepickerComponent.prototype.checkError = function (date) {
            var parsed = this.formatter.parse(date);
            var isValid = false;
            if (parsed && this.calendar.isValid(i1.NgbDate.from(parsed))) {
                isValid = true;
            }
            return isValid;
        };
        return DatepickerComponent;
    }());
    DatepickerComponent.ɵfac = function DatepickerComponent_Factory(t) { return new (t || DatepickerComponent)(i0.ɵɵdirectiveInject(I18n), i0.ɵɵdirectiveInject(i1.NgbDateParserFormatter), i0.ɵɵdirectiveInject(i1.NgbCalendar)); };
    DatepickerComponent.ɵcmp = i0.ɵɵdefineComponent({ type: DatepickerComponent, selectors: [["via-datepicker"]], inputs: { datepickerItem: "datepickerItem", maxDate: "maxDate", minDate: "minDate", disabled: "disabled" }, outputs: { datepickerItemChange: "datepickerItemChange" }, decls: 5, vars: 5, consts: [["id", "win-datepicker", "tohClickOutSite", "", 3, "clickOutSite"], ["placeholder", "DD/MM/YYYY", "name", "datePickerIpt", "ngbDatepicker", "", 1, "form-control", 3, "disabled", "startDate", "value", "minDate", "maxDate", "input", "dateSelect"], ["datePickerIpt", "", "d", "ngbDatepicker"], ["src", "./assets/icons/ic_calendar.svg", 1, "date-icon", 3, "click"]], template: function DatepickerComponent_Template(rf, ctx) {
            if (rf & 1) {
                var _r2_1 = i0.ɵɵgetCurrentView();
                i0.ɵɵelementStart(0, "div", 0);
                i0.ɵɵlistener("clickOutSite", function DatepickerComponent_Template_div_clickOutSite_0_listener() { i0.ɵɵrestoreView(_r2_1); var _r1 = i0.ɵɵreference(3); return _r1.close(); });
                i0.ɵɵelementStart(1, "input", 1, 2);
                i0.ɵɵlistener("input", function DatepickerComponent_Template_input_input_1_listener() { i0.ɵɵrestoreView(_r2_1); var _r0 = i0.ɵɵreference(2); return ctx.datepickerItem = ctx.validateInput(ctx.datepickerItem, _r0.value); })("dateSelect", function DatepickerComponent_Template_input_dateSelect_1_listener($event) { return ctx.dateSelect($event); });
                i0.ɵɵelementEnd();
                i0.ɵɵelementStart(4, "img", 3);
                i0.ɵɵlistener("click", function DatepickerComponent_Template_img_click_4_listener() { i0.ɵɵrestoreView(_r2_1); var _r1 = i0.ɵɵreference(3); return _r1.toggle(); });
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
            }
            if (rf & 2) {
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("disabled", ctx.disabled)("startDate", ctx.datepickerItem)("value", ctx.format(ctx.datepickerItem))("minDate", ctx.minDate)("maxDate", ctx.maxDate);
            }
        }, directives: [i1.NgbInputDatepicker], styles: ["#win-datepicker[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:column;position:relative;width:100%}#win-datepicker[_ngcontent-%COMP%]   .date-icon[_ngcontent-%COMP%]{position:absolute;right:10px;top:13px;width:20px}#win-datepicker[_ngcontent-%COMP%]   .disabled-background[_ngcontent-%COMP%]{background:#f7f8f9}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:5px 10px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]{align-items:center;display:flex;flex:1;justify-content:baseline}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{margin-right:5px;width:20px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{flex:8}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span.dropdown-arrow[_ngcontent-%COMP%]{flex:1;font-size:24px;text-align:right}#win-datepicker[_ngcontent-%COMP%]   .input-datepicker[_ngcontent-%COMP%]{border:0;font-size:0;height:0;line-height:0;margin:0;padding:0;visibility:hidden;width:100%}"] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(DatepickerComponent, [{
                type: i0.Component,
                args: [{
                        selector: "via-datepicker",
                        templateUrl: "./datepicker.html",
                        styleUrls: ["./datepicker.scss"]
                    }]
            }], function () { return [{ type: I18n }, { type: i1.NgbDateParserFormatter }, { type: i1.NgbCalendar }]; }, { datepickerItem: [{
                    type: i0.Input
                }], maxDate: [{
                    type: i0.Input
                }], minDate: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], datepickerItemChange: [{
                    type: i0.Output
                }] });
    })();

    var DatepickerModule = /** @class */ (function () {
        function DatepickerModule() {
        }
        return DatepickerModule;
    }());
    DatepickerModule.ɵmod = i0.ɵɵdefineNgModule({ type: DatepickerModule });
    DatepickerModule.ɵinj = i0.ɵɵdefineInjector({ factory: function DatepickerModule_Factory(t) { return new (t || DatepickerModule)(); }, providers: [I18n, { provide: i1.NgbDatepickerI18n, useClass: CustomDatepickerI18n }], imports: [[
                i1.NgbModule,
                i3.FormsModule,
                i2.CommonModule,
                i3.ReactiveFormsModule
            ]] });
    (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(DatepickerModule, { declarations: [DatepickerComponent], imports: [i1.NgbModule,
                i3.FormsModule,
                i2.CommonModule,
                i3.ReactiveFormsModule], exports: [DatepickerComponent] });
    })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(DatepickerModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [DatepickerComponent],
                        imports: [
                            i1.NgbModule,
                            i3.FormsModule,
                            i2.CommonModule,
                            i3.ReactiveFormsModule
                        ],
                        exports: [DatepickerComponent],
                        providers: [I18n, { provide: i1.NgbDatepickerI18n, useClass: CustomDatepickerI18n }]
                    }]
            }], null, null);
    })();

    /**
     * This Service handles how the date is represented in scripts i.e. ngModel.
     */
    var CustomAdapter = /** @class */ (function (_super) {
        __extends(CustomAdapter, _super);
        function CustomAdapter() {
            var _this = _super.apply(this, __spread(arguments)) || this;
            _this.DELIMITER = '-';
            return _this;
        }
        CustomAdapter.prototype.fromModel = function (value) {
            if (value) {
                var date = value.split(this.DELIMITER);
                return {
                    day: parseInt(date[0], 10),
                    month: parseInt(date[1], 10),
                    year: parseInt(date[2], 10)
                };
            }
            return null;
        };
        CustomAdapter.prototype.toModel = function (date) {
            return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
        };
        return CustomAdapter;
    }(i1.NgbDateAdapter));
    CustomAdapter.ɵfac = function CustomAdapter_Factory(t) { return ɵCustomAdapter_BaseFactory(t || CustomAdapter); };
    CustomAdapter.ɵprov = i0.ɵɵdefineInjectable({ token: CustomAdapter, factory: CustomAdapter.ɵfac });
    var ɵCustomAdapter_BaseFactory = /*@__PURE__*/ i0.ɵɵgetInheritedFactory(CustomAdapter);
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(CustomAdapter, [{
                type: i0.Injectable
            }], null, null);
    })();
    /**
     * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
     */
    var CustomDateParserFormatter = /** @class */ (function (_super) {
        __extends(CustomDateParserFormatter, _super);
        function CustomDateParserFormatter() {
            var _this = _super.apply(this, __spread(arguments)) || this;
            _this.DELIMITER = '/';
            return _this;
        }
        CustomDateParserFormatter.prototype.parse = function (value) {
            if (value) {
                var date = value.split(this.DELIMITER);
                return {
                    day: parseInt(date[0], 10),
                    month: parseInt(date[1], 10),
                    year: parseInt(date[2], 10)
                };
            }
            return null;
        };
        CustomDateParserFormatter.prototype.format = function (date) {
            return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
        };
        return CustomDateParserFormatter;
    }(i1.NgbDateParserFormatter));
    CustomDateParserFormatter.ɵfac = function CustomDateParserFormatter_Factory(t) { return ɵCustomDateParserFormatter_BaseFactory(t || CustomDateParserFormatter); };
    CustomDateParserFormatter.ɵprov = i0.ɵɵdefineInjectable({ token: CustomDateParserFormatter, factory: CustomDateParserFormatter.ɵfac });
    var ɵCustomDateParserFormatter_BaseFactory = /*@__PURE__*/ i0.ɵɵgetInheritedFactory(CustomDateParserFormatter);
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(CustomDateParserFormatter, [{
                type: i0.Injectable
            }], null, null);
    })();

    var FullTextSearchPipe = /** @class */ (function () {
        function FullTextSearchPipe() {
        }
        FullTextSearchPipe.prototype.transform = function (value, keys, term) {
            if (!term)
                return value;
            return (value || []).filter(function (item) { return keys.split(',').some(function (key) { return item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key]); }); });
        };
        return FullTextSearchPipe;
    }());
    FullTextSearchPipe.ɵfac = function FullTextSearchPipe_Factory(t) { return new (t || FullTextSearchPipe)(); };
    FullTextSearchPipe.ɵpipe = i0.ɵɵdefinePipe({ name: "fullTextSearch", type: FullTextSearchPipe, pure: false });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(FullTextSearchPipe, [{
                type: i0.Pipe,
                args: [{
                        name: 'fullTextSearch',
                        pure: false
                    }]
            }], function () { return []; }, null);
    })();

    function DropdownComponent_div_4_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 12);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r0 = i0.ɵɵnextContext();
            i0.ɵɵpropertyInterpolate("title", ctx_r0.getSelectedStr(ctx_r0.itemSelected));
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r0.getSelectedStr(ctx_r0.itemSelected), " ");
        }
    }
    function DropdownComponent_span_5_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span");
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r1 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(ctx_r1.placeholder);
        }
    }
    function DropdownComponent_div_9_i_2_Template(rf, ctx) {
        if (rf & 1) {
            var _r7_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "i", 17);
            i0.ɵɵlistener("click", function DropdownComponent_div_9_i_2_Template_i_click_0_listener() { i0.ɵɵrestoreView(_r7_1); var ctx_r6 = i0.ɵɵnextContext(2); return ctx_r6.clearSearchText(); });
            i0.ɵɵelementEnd();
        }
    }
    function DropdownComponent_div_9_img_3_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "img", 18);
        }
    }
    function DropdownComponent_div_9_Template(rf, ctx) {
        if (rf & 1) {
            var _r9_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 13);
            i0.ɵɵelementStart(1, "input", 14);
            i0.ɵɵlistener("ngModelChange", function DropdownComponent_div_9_Template_input_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r9_1); var ctx_r8 = i0.ɵɵnextContext(); return ctx_r8.searchText = $event; });
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(2, DropdownComponent_div_9_i_2_Template, 1, 0, "i", 15);
            i0.ɵɵtemplate(3, DropdownComponent_div_9_img_3_Template, 1, 0, "img", 16);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r2 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngModel", ctx_r2.searchText);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r2.searchText);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r2.searchText);
        }
    }
    var _c0$1 = function (a0) { return { "selected": a0 }; };
    function DropdownComponent_li_10_Template(rf, ctx) {
        if (rf & 1) {
            var _r12_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "li", 19);
            i0.ɵɵlistener("click", function DropdownComponent_li_10_Template_li_click_0_listener() { i0.ɵɵrestoreView(_r12_1); var item_r10 = ctx.$implicit; var ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.selected(item_r10); });
            i0.ɵɵelementStart(1, "span", 20);
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "span", 21);
            i0.ɵɵelement(4, "i", 22);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var item_r10 = ctx.$implicit;
            var ctx_r3 = i0.ɵɵnextContext();
            i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(3, _c0$1, ctx_r3.hasSelected(item_r10)));
            i0.ɵɵadvance(1);
            i0.ɵɵpropertyInterpolate("title", item_r10.name);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(item_r10.name);
        }
    }
    var _c1 = function (a0) { return { "disabled-background": a0 }; };
    var _c2 = function (a0) { return { "open": a0 }; };
    var _c3 = function (a0) { return { "arrow-rotate": a0 }; };
    var DropdownComponent = /** @class */ (function () {
        function DropdownComponent() {
            this.items = new Array();
            this.itemSelected = new Array();
            this.multiple = false;
            this.disabled = false;
            this.placeholder = "";
            this.itemSelectedChange = new i0.EventEmitter();
            this.isShow = false;
            //super();
        }
        DropdownComponent.prototype.isItemSelectedValid = function () {
            return this.itemSelected !== null && this.itemSelected !== undefined && this.itemSelected.length > 0;
        };
        DropdownComponent.prototype.toggle = function () {
            if (!this.disabled) {
                this.isShow = !this.isShow;
            }
        };
        DropdownComponent.prototype.hide = function () {
            this.isShow = false;
        };
        DropdownComponent.prototype.show = function () {
            this.isShow = true;
        };
        DropdownComponent.prototype.hasSelected = function (item) {
            var itemsFound = this.itemSelected.filter(function (i) { return i.value === item.value; });
            return itemsFound.length > 0;
        };
        DropdownComponent.prototype.selected = function (item) {
            this.searchText = "";
            if (this.multiple) {
                var itemsFound = this.itemSelected.filter(function (i) { return i.value === item.value; });
                if (itemsFound.length === 0) {
                    this.itemSelected.push(item);
                }
                else {
                    this.itemSelected = this.itemSelected.filter(function (i) { return i.value !== item.value; });
                }
            }
            else {
                this.hide();
                this.itemSelected = [];
                this.itemSelected.push(item);
            }
            this.itemSelectedChange.emit(this.itemSelected);
        };
        DropdownComponent.prototype.clearSearchText = function () {
            this.searchText = "";
        };
        DropdownComponent.prototype.getSelectedStr = function (itemSelected) {
            var str = "";
            if (itemSelected) {
                var i_1 = 0;
                itemSelected.forEach(function (item) {
                    i_1 = i_1 + 1;
                    str = str + item.name + (i_1 < itemSelected.length ? ", " : "");
                });
            }
            return str;
        };
        return DropdownComponent;
    }());
    DropdownComponent.ɵfac = function DropdownComponent_Factory(t) { return new (t || DropdownComponent)(); };
    DropdownComponent.ɵcmp = i0.ɵɵdefineComponent({ type: DropdownComponent, selectors: [["via-dropdown"]], inputs: { items: "items", itemSelected: "itemSelected", multiple: "multiple", disabled: "disabled", placeholder: "placeholder" }, outputs: { itemSelectedChange: "itemSelectedChange" }, decls: 13, vars: 20, consts: [["id", "dropdown"], [1, "dropdown-container", 3, "ngClass"], [1, "dropdown", 3, "ngClass"], [1, "dropdown-selectedBox", 3, "click"], ["class", "text-box", 3, "title", 4, "ngIf"], [4, "ngIf"], [1, "dropdown-arrow"], ["aria-hidden", "true", 1, "fa", "fa-angle-down", "arrow", 3, "ngClass"], [1, "dropdown-menu-items", "z-depth-1"], ["class", "dropdown-search-bar", 4, "ngIf"], ["class", "dropdown-menu-item", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [1, "dropdown-backdrop", 3, "ngClass", "click"], [1, "text-box", 3, "title"], [1, "dropdown-search-bar"], [1, "dropdown-search-input", 3, "ngModel", "ngModelChange"], ["class", "fa fa-times clear-search-input", "aria-hidden", "true", 3, "click", 4, "ngIf"], ["class", "input-search-icon dropdown-search-icon", "src", "/assets/icons/ic_search.svg", 4, "ngIf"], ["aria-hidden", "true", 1, "fa", "fa-times", "clear-search-input", 3, "click"], ["src", "/assets/icons/ic_search.svg", 1, "input-search-icon", "dropdown-search-icon"], [1, "dropdown-menu-item", 3, "ngClass", "click"], [1, "text-no-wrap", 3, "title"], [1, "item-check"], ["aria-hidden", "true", 1, "fa", "fa-check"]], template: function DropdownComponent_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵelementStart(0, "div", 0);
                i0.ɵɵelementStart(1, "ul", 1);
                i0.ɵɵelementStart(2, "li", 2);
                i0.ɵɵelementStart(3, "div", 3);
                i0.ɵɵlistener("click", function DropdownComponent_Template_div_click_3_listener() { return ctx.toggle(); });
                i0.ɵɵtemplate(4, DropdownComponent_div_4_Template, 2, 2, "div", 4);
                i0.ɵɵtemplate(5, DropdownComponent_span_5_Template, 2, 1, "span", 5);
                i0.ɵɵelementStart(6, "span", 6);
                i0.ɵɵelement(7, "i", 7);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementStart(8, "ul", 8);
                i0.ɵɵtemplate(9, DropdownComponent_div_9_Template, 4, 3, "div", 9);
                i0.ɵɵtemplate(10, DropdownComponent_li_10_Template, 5, 5, "li", 10);
                i0.ɵɵpipe(11, "fullTextSearch");
                i0.ɵɵelementEnd();
                i0.ɵɵelementStart(12, "div", 11);
                i0.ɵɵlistener("click", function DropdownComponent_Template_div_click_12_listener() { return ctx.hide(); });
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
            }
            if (rf & 2) {
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(12, _c1, ctx.disabled));
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(14, _c2, ctx.isShow));
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("ngIf", ctx.isItemSelectedValid());
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", !ctx.isItemSelectedValid());
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(16, _c3, ctx.isShow));
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("ngIf", ctx.items.length > 15);
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngForOf", i0.ɵɵpipeBind3(11, 8, ctx.items, "name", ctx.searchText));
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(18, _c2, ctx.isShow));
            }
        }, directives: [i2.NgClass, i2.NgIf, i2.NgForOf, i3.DefaultValueAccessor, i3.NgControlStatus, i3.NgModel], pipes: [FullTextSearchPipe], styles: ["#dropdown[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:row;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]{position:-webkit-sticky;position:sticky;top:0}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]   .dropdown-search-input[_ngcontent-%COMP%]{border:.5px solid #dedede;border-left:0;border-right:0;border-top:0;font-size:13px;height:45px;padding-left:10px;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]   .clear-search-input[_ngcontent-%COMP%]{color:#d9e0e5;position:absolute;right:13px;top:13px}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]   .dropdown-search-icon[_ngcontent-%COMP%]{position:absolute;right:13px;top:13px;width:20px}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:9;list-style:none;margin:0;padding:0;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]{display:flex;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown.open[_ngcontent-%COMP%]   .dropdown-backdrop[_ngcontent-%COMP%], #dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown.open[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{display:block}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:none;list-style:none;margin:5px 0 38px;max-height:400px;overflow:auto;padding:0;position:absolute;top:100%;width:100%;z-index:4}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]{align-items:center;display:flex;flex:9;justify-content:flex-start;padding:10px}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]:hover{background:#f7f8f9}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item.selected[_ngcontent-%COMP%]   .item-check[_ngcontent-%COMP%]{color:#2a7af6;display:block}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]   .text-no-wrap[_ngcontent-%COMP%]{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]   .item-check[_ngcontent-%COMP%]{color:#bcbec0;display:none;flex:1;text-align:right}#dropdown[_ngcontent-%COMP%]   .arrow-rotate[_ngcontent-%COMP%]{transform:rotate(-180deg)}#dropdown[_ngcontent-%COMP%]   .arrow[_ngcontent-%COMP%]{transition:.2s ease}#dropdown[_ngcontent-%COMP%]   .dropdown-selectedBox[_ngcontent-%COMP%]{padding:10px;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-selectedBox[_ngcontent-%COMP%]   .text-box[_ngcontent-%COMP%]{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:90%}#dropdown[_ngcontent-%COMP%]   .dropdown-arrow[_ngcontent-%COMP%]{flex:1;height:46px;line-height:46px;position:absolute;right:10px;text-align:center;top:2px}#dropdown[_ngcontent-%COMP%]   .dropdown-arrow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#999;font-size:24px}#dropdown[_ngcontent-%COMP%]   .dropdown-backdrop[_ngcontent-%COMP%]{display:none;height:100%;left:0;position:fixed;top:0;width:100%;z-index:2}"] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(DropdownComponent, [{
                type: i0.Component,
                args: [{
                        selector: "via-dropdown",
                        templateUrl: "./dropdown.html",
                        styleUrls: ["./dropdown.scss"]
                    }]
            }], function () { return []; }, { items: [{
                    type: i0.Input
                }], itemSelected: [{
                    type: i0.Input
                }], multiple: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], placeholder: [{
                    type: i0.Input
                }], itemSelectedChange: [{
                    type: i0.Output
                }] });
    })();

    var DropdownItem = /** @class */ (function () {
        function DropdownItem(name, value) {
            this.name = name;
            this.value = value;
        }
        return DropdownItem;
    }());

    var DropdownModule = /** @class */ (function () {
        function DropdownModule() {
        }
        return DropdownModule;
    }());
    DropdownModule.ɵmod = i0.ɵɵdefineNgModule({ type: DropdownModule });
    DropdownModule.ɵinj = i0.ɵɵdefineInjector({ factory: function DropdownModule_Factory(t) { return new (t || DropdownModule)(); }, imports: [[
                i1.NgbModule,
                i3.FormsModule,
                i2.CommonModule,
                i3.ReactiveFormsModule
            ]] });
    (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(DropdownModule, { declarations: [DropdownComponent, FullTextSearchPipe], imports: [i1.NgbModule,
                i3.FormsModule,
                i2.CommonModule,
                i3.ReactiveFormsModule], exports: [DropdownComponent, FullTextSearchPipe] });
    })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(DropdownModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [DropdownComponent, FullTextSearchPipe],
                        imports: [
                            i1.NgbModule,
                            i3.FormsModule,
                            i2.CommonModule,
                            i3.ReactiveFormsModule
                        ],
                        exports: [DropdownComponent, FullTextSearchPipe]
                    }]
            }], null, null);
    })();

    var FilterPipe = /** @class */ (function () {
        function FilterPipe() {
        }
        FilterPipe.prototype.transform = function (columns) {
            return columns.filter(function (column) {
                return !column.hidden;
            });
        };
        return FilterPipe;
    }());
    FilterPipe.ɵfac = function FilterPipe_Factory(t) { return new (t || FilterPipe)(); };
    FilterPipe.ɵpipe = i0.ɵɵdefinePipe({ name: "filterColumn", type: FilterPipe, pure: true });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(FilterPipe, [{
                type: i0.Pipe,
                args: [{ name: 'filterColumn' }]
            }], null, null);
    })();

    var SORT = {
        ASC: "ASC",
        DESC: "DESC",
        NONE: ""
    };

    var TableCell = /** @class */ (function () {
        function TableCell(value, css, type, routeLink, buttonActions, buttonDropdownActions) {
            if (value === void 0) { value = ""; }
            if (css === void 0) { css = ""; }
            if (type === void 0) { type = CELL_TYPE.STRING; }
            if (routeLink === void 0) { routeLink = ""; }
            if (buttonActions === void 0) { buttonActions = []; }
            if (buttonDropdownActions === void 0) { buttonDropdownActions = []; }
            this._value = value;
            this._css = css;
            this._type = type;
            this._routeLink = routeLink;
            this._buttonActions = buttonActions;
            this._buttonDropdownActions = buttonDropdownActions;
        }
        Object.defineProperty(TableCell.prototype, "value", {
            /**
             * Getter value
             * return {string}
             */
            get: function () {
                return this._value;
            },
            /**
             * Setter value
             * param {string} value
             */
            set: function (value) {
                this._value = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableCell.prototype, "css", {
            /**
             * Getter css
             * return {string}
             */
            get: function () {
                return this._css;
            },
            /**
             * Setter css
             * param {string} value
             */
            set: function (value) {
                this._css = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableCell.prototype, "type", {
            /**
             * Getter type
             * return {string}
             */
            get: function () {
                return this._type;
            },
            /**
             * Setter type
             * param {string} value
             */
            set: function (value) {
                this._type = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableCell.prototype, "routeLink", {
            /**
             * Getter routeLink
             * return {string}
             */
            get: function () {
                return this._routeLink;
            },
            /**
             * Setter routeLink
             * param {string} value
             */
            set: function (value) {
                this._routeLink = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableCell.prototype, "buttonActions", {
            /**
             * Getter buttonActions
             * return {ButtonAction[]}
             */
            get: function () {
                return this._buttonActions;
            },
            /**
             * Setter buttonActions
             * param {ButtonAction[]} value
             */
            set: function (value) {
                this._buttonActions = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableCell.prototype, "buttonDropdownActions", {
            /**
             * Getter buttonDropdownActions
             * return {ButtonAction[]}
             */
            get: function () {
                return this._buttonDropdownActions;
            },
            /**
             * Setter buttonDropdownActions
             * param {ButtonAction[]} value
             */
            set: function (value) {
                this._buttonDropdownActions = value;
            },
            enumerable: false,
            configurable: true
        });
        return TableCell;
    }());
    var CELL_TYPE = {
        STRING: "STRING",
        DATE_TIME: "DATE_TIME",
        STATUS: "STATUS",
        ACTION: "ACTION",
        IMAGE_THUMBNAIL: "IMAGE_THUMBNAIL",
        INPUT: "INPUT"
    };

    var TABLE_LANGUAGE_CONFIG = {
        table_entries_per_page: "Entries per page",
        table_footer_showing: "Showing",
        table_page: "Page",
        filter_by: "Filter by",
        table_to: "to",
        table_of: "of",
        no_data_found: "No data found",
        table_elements: "elements",
        rts_dropdown_filter: "Filter",
        rts_reset: "Reset",
        rts_dropdown_search: "Search"
    };

    var TableFilter = /** @class */ (function () {
        function TableFilter(hasFilter) {
            if (hasFilter === void 0) { hasFilter = true; }
            this._hasFilter = hasFilter;
        }
        Object.defineProperty(TableFilter.prototype, "hasFilter", {
            get: function () {
                return this._hasFilter;
            },
            set: function (value) {
                this._hasFilter = value;
            },
            enumerable: false,
            configurable: true
        });
        return TableFilter;
    }());

    var TableDefaultSort = /** @class */ (function () {
        function TableDefaultSort(sortColumnName, sortType) {
            if (sortColumnName === void 0) { sortColumnName = ""; }
            if (sortType === void 0) { sortType = SORT.ASC; }
            this._sortColumnName = sortColumnName;
            this._sortType = sortType;
        }
        Object.defineProperty(TableDefaultSort.prototype, "sortColumnName", {
            /**
             * Getter sortColumnName
             * return {string}
             */
            get: function () {
                return this._sortColumnName;
            },
            /**
             * Setter sortColumnName
             * param {string} value
             */
            set: function (value) {
                this._sortColumnName = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableDefaultSort.prototype, "sortType", {
            /**
             * Getter sortType
             * return {string}
             */
            get: function () {
                return this._sortType;
            },
            /**
             * Setter sortType
             * param {string} value
             */
            set: function (value) {
                this._sortType = value;
            },
            enumerable: false,
            configurable: true
        });
        return TableDefaultSort;
    }());

    var TableOption = /** @class */ (function () {
        function TableOption(tableColumns, tableRows, tableType) {
            if (tableColumns === void 0) { tableColumns = []; }
            if (tableRows === void 0) { tableRows = []; }
            if (tableType === void 0) { tableType = "RTS"; }
            this._tableType = "RTS";
            this._tableColumns = tableColumns;
            this._tableRows = tableRows;
            this._totalItems = 0;
            this._itemsPerPage = 10;
            this._currentPage = 0;
            this._tableFilter = new TableFilter();
            this._tableActions = new Array();
            this._defaultSort = new TableDefaultSort();
            this._tableType = tableType;
        }
        Object.defineProperty(TableOption.prototype, "tableColumns", {
            /**
             * Getter tableColumns
             * return {TableColumn[]}
             */
            get: function () {
                return this._tableColumns;
            },
            /**
             * Setter tableColumns
             * param {TableColumn[]} value
             */
            set: function (value) {
                this._tableColumns = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableOption.prototype, "tableRows", {
            /**
             * Getter tableRows
             * return {TableRow[]}
             */
            get: function () {
                return this._tableRows;
            },
            /**
             * Setter tableRows
             * param {TableRow[]} value
             */
            set: function (value) {
                this._tableRows = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableOption.prototype, "totalItems", {
            /**
             * Getter totalItems
             * return {number}
             */
            get: function () {
                return this._totalItems;
            },
            /**
             * Setter totalItems
             * param {number} value
             */
            set: function (value) {
                this._totalItems = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableOption.prototype, "itemsPerPage", {
            /**
             * Getter itemsPerPage
             * return {number}
             */
            get: function () {
                return this._itemsPerPage;
            },
            /**
             * Setter itemsPerPage
             * param {number} value
             */
            set: function (value) {
                this._itemsPerPage = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableOption.prototype, "currentPage", {
            /**
             * Getter currentPage
             * return {number}
             */
            get: function () {
                return this._currentPage;
            },
            /**
             * Setter currentPage
             * param {number} value
             */
            set: function (value) {
                this._currentPage = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableOption.prototype, "tableFilter", {
            /**
             * Getter tableFilter
             * return {TableFilter}
             */
            get: function () {
                return this._tableFilter;
            },
            /**
             * Setter tableFilter
             * param {TableFilter} value
             */
            set: function (value) {
                this._tableFilter = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableOption.prototype, "tableActions", {
            /**
             * Getter tableActions
             * return {TableAction[]}
             */
            get: function () {
                return this._tableActions;
            },
            /**
             * Setter tableActions
             * param {TableAction[]} value
             */
            set: function (value) {
                this._tableActions = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableOption.prototype, "defaultSort", {
            /**
             * Getter defaultSort
             * return {TableDefaultSort}
             */
            get: function () {
                return this._defaultSort;
            },
            /**
             * Setter defaultSort
             * param {TableDefaultSort} value
             */
            set: function (value) {
                this._defaultSort = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableOption.prototype, "tableType", {
            /**
             * Getter tableType
             * return {string }
             */
            get: function () {
                return this._tableType;
            },
            /**
             * Setter tableType
             * param {string } value
             */
            set: function (value) {
                this._tableType = value;
            },
            enumerable: false,
            configurable: true
        });
        return TableOption;
    }());
    var EntryPerpage = /** @class */ (function () {
        function EntryPerpage(name, value, selected) {
            if (name === void 0) { name = "5"; }
            if (value === void 0) { value = 5; }
            if (selected === void 0) { selected = false; }
            this._name = name;
            this._value = value;
            this._selected = selected;
        }
        Object.defineProperty(EntryPerpage.prototype, "selected", {
            get: function () {
                return this._selected;
            },
            set: function (value) {
                this._selected = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EntryPerpage.prototype, "value", {
            get: function () {
                return this._value;
            },
            set: function (value) {
                this._value = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(EntryPerpage.prototype, "name", {
            get: function () {
                return this._name;
            },
            set: function (value) {
                this._name = value;
            },
            enumerable: false,
            configurable: true
        });
        return EntryPerpage;
    }());

    function TableComponent_div_5_Template(rf, ctx) {
        if (rf & 1) {
            var _r11_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 19);
            i0.ɵɵelementStart(1, "ul", 20);
            i0.ɵɵelementStart(2, "li", 21);
            i0.ɵɵelementStart(3, "span", 22);
            i0.ɵɵelementStart(4, "img", 23);
            i0.ɵɵlistener("click", function TableComponent_div_5_Template_img_click_4_listener() { i0.ɵɵrestoreView(_r11_1); var ctx_r10 = i0.ɵɵnextContext(); return ctx_r10.toggleFilter(); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(5, "span", 24);
            i0.ɵɵlistener("click", function TableComponent_div_5_Template_span_click_5_listener() { i0.ɵɵrestoreView(_r11_1); var ctx_r12 = i0.ɵɵnextContext(); return ctx_r12.toggleFilter(); });
            i0.ɵɵtext(6);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(7, "div", 25);
            i0.ɵɵlistener("click", function TableComponent_div_5_Template_div_click_7_listener() { i0.ɵɵrestoreView(_r11_1); var ctx_r13 = i0.ɵɵnextContext(); return ctx_r13.hideFilter(); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(8, "ul", 26);
            i0.ɵɵelementStart(9, "li");
            i0.ɵɵprojection(10);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r0 = i0.ɵɵnextContext();
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngClass", ctx_r0.isShowFilter ? "open" : "");
            i0.ɵɵadvance(4);
            i0.ɵɵtextInterpolate(ctx_r0.TablelanguageObj.filter_by);
        }
    }
    function TableComponent_div_7_span_1_button_1_Template(rf, ctx) {
        if (rf & 1) {
            var _r19_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "button", 28);
            i0.ɵɵlistener("click", function TableComponent_div_7_span_1_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r19_1); var btn_r15 = i0.ɵɵnextContext().$implicit; var ctx_r17 = i0.ɵɵnextContext(2); return ctx_r17.doAction(btn_r15); });
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var btn_r15 = i0.ɵɵnextContext().$implicit;
            i0.ɵɵproperty("ngClass", btn_r15.css);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(btn_r15.name);
        }
    }
    function TableComponent_div_7_span_1_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span");
            i0.ɵɵtemplate(1, TableComponent_div_7_span_1_button_1_Template, 2, 2, "button", 27);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var btn_r15 = ctx.$implicit;
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !btn_r15.hided);
        }
    }
    function TableComponent_div_7_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div");
            i0.ɵɵtemplate(1, TableComponent_div_7_span_1_Template, 2, 1, "span", 16);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r1 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", ctx_r1.tableOption.tableActions);
        }
    }
    function TableComponent_option_13_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "option", 29);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var entry_r21 = ctx.$implicit;
            i0.ɵɵproperty("selected", entry_r21.selected);
            i0.ɵɵattribute("value", entry_r21.value);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(entry_r21.name);
        }
    }
    function TableComponent_div_16_Template(rf, ctx) {
        if (rf & 1) {
            var _r23_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 30);
            i0.ɵɵelementStart(1, "span", 31);
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "ngb-pagination", 32);
            i0.ɵɵlistener("pageChange", function TableComponent_div_16_Template_ngb_pagination_pageChange_3_listener($event) { i0.ɵɵrestoreView(_r23_1); var ctx_r22 = i0.ɵɵnextContext(); return ctx_r22.tableOption.currentPage = $event; })("pageChange", function TableComponent_div_16_Template_ngb_pagination_pageChange_3_listener() { i0.ɵɵrestoreView(_r23_1); var ctx_r24 = i0.ɵɵnextContext(); return ctx_r24.doPageChange(); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r4 = i0.ɵɵnextContext();
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate1("", ctx_r4.TablelanguageObj.table_page, " ");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("directionLinks", false)("collectionSize", ctx_r4.tableOption.totalItems)("rotate", true)("maxSize", 5)("pageSize", ctx_r4.tableOption.itemsPerPage)("page", ctx_r4.tableOption.currentPage);
        }
    }
    function TableComponent_thead_18_tr_1_th_1_i_4_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "i", 40);
        }
    }
    function TableComponent_thead_18_tr_1_th_1_i_5_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "i", 41);
        }
    }
    var _c0$2 = function (a0, a1, a2) { return [a0, a1, a2]; };
    function TableComponent_thead_18_tr_1_th_1_Template(rf, ctx) {
        if (rf & 1) {
            var _r33_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "th", 36);
            i0.ɵɵlistener("click", function TableComponent_thead_18_tr_1_th_1_Template_th_click_0_listener() { i0.ɵɵrestoreView(_r33_1); var column_r29 = ctx.$implicit; var ctx_r32 = i0.ɵɵnextContext(3); return ctx_r32.doSort(column_r29); });
            i0.ɵɵelementStart(1, "div", 37);
            i0.ɵɵelementStart(2, "span");
            i0.ɵɵtext(3);
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(4, TableComponent_thead_18_tr_1_th_1_i_4_Template, 1, 0, "i", 38);
            i0.ɵɵtemplate(5, TableComponent_thead_18_tr_1_th_1_i_5_Template, 1, 0, "i", 39);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var column_r29 = ctx.$implicit;
            var ctx_r27 = i0.ɵɵnextContext(3);
            i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction3(4, _c0$2, column_r29.sortable ? "sortable" : "", column_r29.sort, column_r29.css));
            i0.ɵɵadvance(3);
            i0.ɵɵtextInterpolate(column_r29.value);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r27.isSortAsc(column_r29));
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r27.isSortDesc(column_r29));
        }
    }
    function TableComponent_thead_18_tr_1_th_3_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "th");
        }
    }
    function TableComponent_thead_18_tr_1_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "tr", 34);
            i0.ɵɵtemplate(1, TableComponent_thead_18_tr_1_th_1_Template, 6, 8, "th", 35);
            i0.ɵɵpipe(2, "filterColumn");
            i0.ɵɵtemplate(3, TableComponent_thead_18_tr_1_th_3_Template, 1, 0, "th", 7);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r25 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", i0.ɵɵpipeBind1(2, 2, ctx_r25.tableOption.tableColumns));
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx_r25.rowSortable);
        }
    }
    function TableComponent_thead_18_tr_2_th_1_i_4_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "i", 40);
        }
    }
    function TableComponent_thead_18_tr_2_th_1_i_5_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "i", 41);
        }
    }
    function TableComponent_thead_18_tr_2_th_1_div_6_div_3_Template(rf, ctx) {
        if (rf & 1) {
            var _r45_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 51);
            i0.ɵɵelementStart(1, "input", 52);
            i0.ɵɵlistener("ngModelChange", function TableComponent_thead_18_tr_2_th_1_div_6_div_3_Template_input_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r45_1); var header_r42 = ctx.$implicit; return header_r42.checked = $event; });
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(2, "label", 53);
            i0.ɵɵtext(3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var header_r42 = ctx.$implicit;
            var i_r43 = ctx.index;
            var columnIndex_r36 = i0.ɵɵnextContext(2).index;
            i0.ɵɵadvance(1);
            i0.ɵɵpropertyInterpolate2("id", "styled-checkbox-", i_r43, "-", columnIndex_r36, "");
            i0.ɵɵpropertyInterpolate("value", header_r42.value);
            i0.ɵɵproperty("ngModel", header_r42.checked);
            i0.ɵɵadvance(1);
            i0.ɵɵpropertyInterpolate2("for", "styled-checkbox-", i_r43, "-", columnIndex_r36, "");
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(header_r42.title);
        }
    }
    function TableComponent_thead_18_tr_2_th_1_div_6_Template(rf, ctx) {
        if (rf & 1) {
            var _r49_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 45);
            i0.ɵɵelement(1, "img", 46);
            i0.ɵɵelementStart(2, "div", 47);
            i0.ɵɵtemplate(3, TableComponent_thead_18_tr_2_th_1_div_6_div_3_Template, 4, 7, "div", 48);
            i0.ɵɵelementStart(4, "div", 49);
            i0.ɵɵelementStart(5, "button", 50);
            i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_6_Template_button_click_5_listener() { i0.ɵɵrestoreView(_r49_1); var column_r35 = i0.ɵɵnextContext().$implicit; var ctx_r47 = i0.ɵɵnextContext(3); return ctx_r47.filterOnTableColumn(column_r35.filterList, column_r35.name); });
            i0.ɵɵtext(6);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(7, "button", 50);
            i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_6_Template_button_click_7_listener() { i0.ɵɵrestoreView(_r49_1); var column_r35 = i0.ɵɵnextContext().$implicit; var ctx_r50 = i0.ɵɵnextContext(3); return ctx_r50.filterOnTableColumnReset(column_r35.filterList); });
            i0.ɵɵtext(8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var column_r35 = i0.ɵɵnextContext().$implicit;
            var ctx_r39 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("ngForOf", column_r35.filterList);
            i0.ɵɵadvance(3);
            i0.ɵɵtextInterpolate(ctx_r39.TablelanguageObj.rts_dropdown_filter);
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx_r39.TablelanguageObj.rts_reset);
        }
    }
    function TableComponent_thead_18_tr_2_th_1_div_7_Template(rf, ctx) {
        if (rf & 1) {
            var _r55_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 45);
            i0.ɵɵelement(1, "img", 54);
            i0.ɵɵelementStart(2, "div", 55);
            i0.ɵɵelementStart(3, "input", 56);
            i0.ɵɵlistener("ngModelChange", function TableComponent_thead_18_tr_2_th_1_div_7_Template_input_ngModelChange_3_listener($event) { i0.ɵɵrestoreView(_r55_1); var column_r35 = i0.ɵɵnextContext().$implicit; return column_r35.searchStr = $event; });
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(4, "div", 49);
            i0.ɵɵelementStart(5, "button", 50);
            i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_7_Template_button_click_5_listener() { i0.ɵɵrestoreView(_r55_1); var column_r35 = i0.ɵɵnextContext().$implicit; var ctx_r56 = i0.ɵɵnextContext(3); return ctx_r56.tableDropdownSearch(column_r35.searchStr, column_r35.name); });
            i0.ɵɵtext(6);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(7, "button", 50);
            i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_7_Template_button_click_7_listener() { i0.ɵɵrestoreView(_r55_1); var column_r35 = i0.ɵɵnextContext().$implicit; var ctx_r58 = i0.ɵɵnextContext(3); return ctx_r58.tableDropdownSearchReset(column_r35.searchStr); });
            i0.ɵɵtext(8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var column_r35 = i0.ɵɵnextContext().$implicit;
            var ctx_r40 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("ngModel", column_r35.searchStr);
            i0.ɵɵadvance(3);
            i0.ɵɵtextInterpolate(ctx_r40.TablelanguageObj.rts_dropdown_search);
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx_r40.TablelanguageObj.rts_reset);
        }
    }
    function TableComponent_thead_18_tr_2_th_1_Template(rf, ctx) {
        if (rf & 1) {
            var _r62_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "th", 43);
            i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_Template_th_click_0_listener() { i0.ɵɵrestoreView(_r62_1); var column_r35 = ctx.$implicit; var ctx_r61 = i0.ɵɵnextContext(3); return ctx_r61.doSort(column_r35); });
            i0.ɵɵelementStart(1, "div", 37);
            i0.ɵɵelementStart(2, "span");
            i0.ɵɵtext(3);
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(4, TableComponent_thead_18_tr_2_th_1_i_4_Template, 1, 0, "i", 38);
            i0.ɵɵtemplate(5, TableComponent_thead_18_tr_2_th_1_i_5_Template, 1, 0, "i", 39);
            i0.ɵɵtemplate(6, TableComponent_thead_18_tr_2_th_1_div_6_Template, 9, 3, "div", 44);
            i0.ɵɵtemplate(7, TableComponent_thead_18_tr_2_th_1_div_7_Template, 9, 3, "div", 44);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var column_r35 = ctx.$implicit;
            var ctx_r34 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(3);
            i0.ɵɵtextInterpolate(column_r35.value);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r34.isSortAsc(column_r35));
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r34.isSortDesc(column_r35));
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", column_r35.filterList.length);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", column_r35.searchable);
        }
    }
    function TableComponent_thead_18_tr_2_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "tr", 34);
            i0.ɵɵtemplate(1, TableComponent_thead_18_tr_2_th_1_Template, 8, 5, "th", 42);
            i0.ɵɵpipe(2, "filterColumn");
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r26 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", i0.ɵɵpipeBind1(2, 1, ctx_r26.tableOption.tableColumns));
        }
    }
    function TableComponent_thead_18_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "thead");
            i0.ɵɵtemplate(1, TableComponent_thead_18_tr_1_Template, 4, 4, "tr", 33);
            i0.ɵɵtemplate(2, TableComponent_thead_18_tr_2_Template, 3, 3, "tr", 33);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r5 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r5.tableType === "WIN");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r5.tableType === "RTS");
        }
    }
    function TableComponent_ng_container_20_td_2_span_1_Template(rf, ctx) {
        if (rf & 1) {
            var _r77_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "span", 68);
            i0.ɵɵelementStart(1, "img", 69);
            i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_1_Template_img_click_1_listener() { i0.ɵɵrestoreView(_r77_1); var row_r63 = i0.ɵɵnextContext(2).$implicit; var ctx_r75 = i0.ɵɵnextContext(); return ctx_r75.doOpenPreview(row_r63.tableCells[1].value); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var cell_r68 = i0.ɵɵnextContext().$implicit;
            var ctx_r69 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("src", ctx_r69.downloadFilePath + cell_r68.value + "_thumb", i0.ɵɵsanitizeUrl);
        }
    }
    function TableComponent_ng_container_20_td_2_span_2_button_3_Template(rf, ctx) {
        if (rf & 1) {
            var _r83_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "button", 74);
            i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_2_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r83_1); var cell_r68 = i0.ɵɵnextContext(2).$implicit; var i_r64 = i0.ɵɵnextContext().index; var ctx_r81 = i0.ɵɵnextContext(); cell_r68.enabled = true; return ctx_r81.focusInput("input-" + i_r64); });
            i0.ɵɵelement(1, "i", 75);
            i0.ɵɵelementEnd();
        }
    }
    function TableComponent_ng_container_20_td_2_span_2_div_4_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 76);
            i0.ɵɵtext(1, "maxlength ");
            i0.ɵɵelementEnd();
        }
    }
    function TableComponent_ng_container_20_td_2_span_2_Template(rf, ctx) {
        if (rf & 1) {
            var _r87_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "span", 70);
            i0.ɵɵtext(1);
            i0.ɵɵelementStart(2, "input", 71);
            i0.ɵɵlistener("ngModelChange", function TableComponent_ng_container_20_td_2_span_2_Template_input_ngModelChange_2_listener($event) { i0.ɵɵrestoreView(_r87_1); var cell_r68 = i0.ɵɵnextContext().$implicit; return cell_r68.value = $event; })("focusin", function TableComponent_ng_container_20_td_2_span_2_Template_input_focusin_2_listener() { i0.ɵɵrestoreView(_r87_1); var cell_r68 = i0.ɵɵnextContext().$implicit; return cell_r68.oldValue = cell_r68.value; })("focusout", function TableComponent_ng_container_20_td_2_span_2_Template_input_focusout_2_listener() { i0.ɵɵrestoreView(_r87_1); var cell_r68 = i0.ɵɵnextContext().$implicit; var row_r63 = i0.ɵɵnextContext().$implicit; var ctx_r90 = i0.ɵɵnextContext(); ctx_r90.doUpdateInputEvent(row_r63, cell_r68.enabled, cell_r68.value, cell_r68.oldValue); return cell_r68.enabled = false; })("keyup.enter", function TableComponent_ng_container_20_td_2_span_2_Template_input_keyup_enter_2_listener() { i0.ɵɵrestoreView(_r87_1); var cell_r68 = i0.ɵɵnextContext().$implicit; var row_r63 = i0.ɵɵnextContext().$implicit; var ctx_r93 = i0.ɵɵnextContext(); cell_r68.enabled = false; return ctx_r93.doUpdateInputEvent(row_r63, true, cell_r68, cell_r68.oldValue); });
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_2_button_3_Template, 2, 0, "button", 72);
            i0.ɵɵtemplate(4, TableComponent_ng_container_20_td_2_span_2_div_4_Template, 2, 0, "div", 73);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var cell_r68 = i0.ɵɵnextContext().$implicit;
            var i_r64 = i0.ɵɵnextContext().index;
            var ctx_r70 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", cell_r68.originalValue, " ");
            i0.ɵɵadvance(1);
            i0.ɵɵpropertyInterpolate("id", "input-" + i_r64);
            i0.ɵɵproperty("ngModel", cell_r68.value)("disabled", !cell_r68.enabled || !ctx_r70.rowSortable);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !cell_r68.enabled && ctx_r70.rowSortable);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", cell_r68.value.length > ctx_r70.inputMaxLenght);
        }
    }
    function TableComponent_ng_container_20_td_2_span_3_Template(rf, ctx) {
        if (rf & 1) {
            var _r100_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "span", 77);
            i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_3_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r100_1); var cell_r68 = i0.ɵɵnextContext().$implicit; var ctx_r98 = i0.ɵɵnextContext(2); return ctx_r98.cellClicked(cell_r68); });
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var cell_r68 = i0.ɵɵnextContext().$implicit;
            i0.ɵɵpropertyInterpolate("title", cell_r68.value);
            i0.ɵɵproperty("ngClass", cell_r68.routeLink.trim().length > 0 ? "hand" : "");
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(cell_r68.value);
        }
    }
    function TableComponent_ng_container_20_td_2_span_4_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 78);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var cell_r68 = i0.ɵɵnextContext().$implicit;
            i0.ɵɵpropertyInterpolate("title", cell_r68.value);
            i0.ɵɵproperty("ngClass", cell_r68.value);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(cell_r68.value);
        }
    }
    function TableComponent_ng_container_20_td_2_span_5_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 79);
            i0.ɵɵpipe(1, "date");
            i0.ɵɵtext(2);
            i0.ɵɵpipe(3, "date");
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var cell_r68 = i0.ɵɵnextContext().$implicit;
            var ctx_r73 = i0.ɵɵnextContext(2);
            i0.ɵɵpropertyInterpolate("title", i0.ɵɵpipeBind2(1, 2, cell_r68.value, ctx_r73.dateFormat));
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(i0.ɵɵpipeBind2(3, 5, cell_r68.value, ctx_r73.dateFormat));
        }
    }
    function TableComponent_ng_container_20_td_2_span_6_span_1_button_2_Template(rf, ctx) {
        if (rf & 1) {
            var _r112_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "button", 84);
            i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_span_1_button_2_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r112_1); var buttonAction_r107 = i0.ɵɵnextContext().$implicit; var row_r63 = i0.ɵɵnextContext(3).$implicit; return buttonAction_r107.buttonAction(row_r63); });
            i0.ɵɵelement(1, "img", 85);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var buttonAction_r107 = i0.ɵɵnextContext().$implicit;
            i0.ɵɵpropertyInterpolate("title", buttonAction_r107.name);
            i0.ɵɵproperty("disabled", buttonAction_r107.disabled);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("src", buttonAction_r107.iconUrl, i0.ɵɵsanitizeUrl);
        }
    }
    function TableComponent_ng_container_20_td_2_span_6_span_1_button_3_Template(rf, ctx) {
        if (rf & 1) {
            var _r117_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "button", 86);
            i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_span_1_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r117_1); var buttonAction_r107 = i0.ɵɵnextContext().$implicit; var row_r63 = i0.ɵɵnextContext(3).$implicit; return buttonAction_r107.buttonAction(row_r63); });
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var buttonAction_r107 = i0.ɵɵnextContext().$implicit;
            i0.ɵɵproperty("disabled", buttonAction_r107.disabled)("ngClass", "btn-" + buttonAction_r107.name);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(buttonAction_r107.name);
        }
    }
    function TableComponent_ng_container_20_td_2_span_6_span_1_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span");
            i0.ɵɵelementStart(1, "span");
            i0.ɵɵtemplate(2, TableComponent_ng_container_20_td_2_span_6_span_1_button_2_Template, 2, 3, "button", 82);
            i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_6_span_1_button_3_Template, 2, 3, "button", 83);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var buttonAction_r107 = ctx.$implicit;
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", buttonAction_r107.iconUrl);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !buttonAction_r107.iconUrl);
        }
    }
    function TableComponent_ng_container_20_td_2_span_6_button_2_Template(rf, ctx) {
        if (rf & 1) {
            var _r122_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "button", 87);
            i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_button_2_Template_button_click_0_listener($event) { i0.ɵɵrestoreView(_r122_1); var i_r64 = i0.ɵɵnextContext(3).index; var ctx_r120 = i0.ɵɵnextContext(); return ctx_r120.expand("table-row-extra" + i_r64, $event); });
            i0.ɵɵelement(1, "i", 88);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var i_r64 = i0.ɵɵnextContext(3).index;
            var ctx_r105 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngClass", "table-row-extra" + i_r64 == ctx_r105.expandedRowId ? "arrow-rotate" : "");
        }
    }
    function TableComponent_ng_container_20_td_2_span_6_div_3_span_5_Template(rf, ctx) {
        if (rf & 1) {
            var _r128_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "span");
            i0.ɵɵelementStart(1, "button", 94);
            i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_div_3_span_5_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r128_1); var buttonAction_r125 = ctx.$implicit; var row_r63 = i0.ɵɵnextContext(4).$implicit; return buttonAction_r125.disabled != true ? buttonAction_r125.buttonAction(row_r63) : ""; });
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var buttonAction_r125 = ctx.$implicit;
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("disabled", buttonAction_r125.disabled);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", buttonAction_r125.name, "");
        }
    }
    function TableComponent_ng_container_20_td_2_span_6_div_3_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 89);
            i0.ɵɵelementStart(1, "div", 90);
            i0.ɵɵelementStart(2, "button", 91);
            i0.ɵɵelement(3, "i", 92);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(4, "div", 93);
            i0.ɵɵtemplate(5, TableComponent_ng_container_20_td_2_span_6_div_3_span_5_Template, 3, 2, "span", 16);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var cell_r68 = i0.ɵɵnextContext(2).$implicit;
            i0.ɵɵadvance(5);
            i0.ɵɵproperty("ngForOf", cell_r68.buttonDropdownActions);
        }
    }
    function TableComponent_ng_container_20_td_2_span_6_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span");
            i0.ɵɵtemplate(1, TableComponent_ng_container_20_td_2_span_6_span_1_Template, 4, 2, "span", 16);
            i0.ɵɵtemplate(2, TableComponent_ng_container_20_td_2_span_6_button_2_Template, 2, 1, "button", 80);
            i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_6_div_3_Template, 6, 1, "div", 81);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var cell_r68 = i0.ɵɵnextContext().$implicit;
            var row_r63 = i0.ɵɵnextContext().$implicit;
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", cell_r68.buttonActions);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", row_r63.expandedText);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", cell_r68.buttonDropdownActions.length);
        }
    }
    function TableComponent_ng_container_20_td_2_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "td", 61);
            i0.ɵɵtemplate(1, TableComponent_ng_container_20_td_2_span_1_Template, 2, 1, "span", 62);
            i0.ɵɵtemplate(2, TableComponent_ng_container_20_td_2_span_2_Template, 5, 6, "span", 63);
            i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_3_Template, 2, 3, "span", 64);
            i0.ɵɵtemplate(4, TableComponent_ng_container_20_td_2_span_4_Template, 2, 3, "span", 65);
            i0.ɵɵtemplate(5, TableComponent_ng_container_20_td_2_span_5_Template, 4, 8, "span", 66);
            i0.ɵɵtemplate(6, TableComponent_ng_container_20_td_2_span_6_Template, 4, 3, "span", 67);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var cell_r68 = ctx.$implicit;
            var ctx_r65 = i0.ɵɵnextContext(2);
            i0.ɵɵproperty("ngClass", cell_r68.type + " " + cell_r68.css)("ngSwitch", cell_r68.type);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.IMAGE_THUMBNAIL);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.INPUT);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.STATUS);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.DATE_TIME);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.ACTION);
        }
    }
    function TableComponent_ng_container_20_td_3_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "td", 95);
            i0.ɵɵelement(1, "a", 96);
            i0.ɵɵelementEnd();
        }
    }
    function TableComponent_ng_container_20_tr_4_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "tr", 97);
            i0.ɵɵelementStart(1, "td");
            i0.ɵɵelementStart(2, "div", 98);
            i0.ɵɵelement(3, "div", 99);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r132 = i0.ɵɵnextContext();
            var row_r63 = ctx_r132.$implicit;
            var i_r64 = ctx_r132.index;
            i0.ɵɵadvance(1);
            i0.ɵɵattribute("colspan", row_r63.tableCells.length);
            i0.ɵɵadvance(1);
            i0.ɵɵpropertyInterpolate("id", "table-row-extra" + i_r64);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("innerHTML", row_r63.expandedText, i0.ɵɵsanitizeHtml);
        }
    }
    function TableComponent_ng_container_20_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementContainerStart(0);
            i0.ɵɵelementStart(1, "tr", 57);
            i0.ɵɵtemplate(2, TableComponent_ng_container_20_td_2_Template, 7, 7, "td", 58);
            i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_3_Template, 2, 0, "td", 59);
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(4, TableComponent_ng_container_20_tr_4_Template, 4, 3, "tr", 60);
            i0.ɵɵelementContainerEnd();
        }
        if (rf & 2) {
            var row_r63 = ctx.$implicit;
            var i_r64 = ctx.index;
            var ctx_r6 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵpropertyInterpolate("id", "table-row-" + i_r64);
            i0.ɵɵproperty("ngClass", ctx_r6.rowSortable ? row_r63.css + "sortable-row" : row_r63.css);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", row_r63.tableCells);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r6.rowSortable);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r6.rowSortable);
        }
    }
    function TableComponent_tfoot_21_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "tfoot");
            i0.ɵɵelementStart(1, "tr");
            i0.ɵɵelementStart(2, "td");
            i0.ɵɵtext(3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r7 = i0.ɵɵnextContext();
            i0.ɵɵadvance(2);
            i0.ɵɵattribute("colspan", ctx_r7.tableOption.tableColumns.length);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(ctx_r7.TablelanguageObj.no_data_found);
        }
    }
    function TableComponent_div_22_div_2_ng_template_2_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "img", 103);
        }
    }
    function TableComponent_div_22_div_2_ng_template_3_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelement(0, "img", 104);
        }
    }
    function TableComponent_div_22_div_2_Template(rf, ctx) {
        if (rf & 1) {
            var _r137_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 30);
            i0.ɵɵelementStart(1, "ngb-pagination", 100);
            i0.ɵɵlistener("pageChange", function TableComponent_div_22_div_2_Template_ngb_pagination_pageChange_1_listener($event) { i0.ɵɵrestoreView(_r137_1); var ctx_r136 = i0.ɵɵnextContext(2); return ctx_r136.tableOption.currentPage = $event; })("pageChange", function TableComponent_div_22_div_2_Template_ngb_pagination_pageChange_1_listener() { i0.ɵɵrestoreView(_r137_1); var ctx_r138 = i0.ɵɵnextContext(2); return ctx_r138.doPageChange(); });
            i0.ɵɵtemplate(2, TableComponent_div_22_div_2_ng_template_2_Template, 1, 0, "ng-template", 101);
            i0.ɵɵtemplate(3, TableComponent_div_22_div_2_ng_template_3_Template, 1, 0, "ng-template", 102);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r133 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("collectionSize", ctx_r133.tableOption.totalItems)("rotate", true)("maxSize", 5)("pageSize", ctx_r133.tableOption.itemsPerPage)("page", ctx_r133.tableOption.currentPage);
        }
    }
    function TableComponent_div_22_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 8);
            i0.ɵɵelementStart(1, "div", 9);
            i0.ɵɵtemplate(2, TableComponent_div_22_div_2_Template, 4, 5, "div", 14);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r8 = i0.ɵɵnextContext();
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", !ctx_r8.noDataFound());
        }
    }
    function TableComponent_div_23_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 105);
            i0.ɵɵtext(1);
            i0.ɵɵelementStart(2, "span", 106);
            i0.ɵɵtext(3);
            i0.ɵɵelementEnd();
            i0.ɵɵtext(4);
            i0.ɵɵelementStart(5, "span", 106);
            i0.ɵɵtext(6);
            i0.ɵɵelementEnd();
            i0.ɵɵtext(7);
            i0.ɵɵelementStart(8, "span", 107);
            i0.ɵɵtext(9);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r9 = i0.ɵɵnextContext();
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r9.TablelanguageObj.table_footer_showing, " ");
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx_r9.startItemNumber());
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r9.TablelanguageObj.table_to, " ");
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx_r9.endItemNumber());
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r9.TablelanguageObj.table_of, " ");
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate2("", ctx_r9.tableOption.totalItems, " ", ctx_r9.TablelanguageObj.table_elements, "");
        }
    }
    var _c1$1 = [[["", "filter", ""]]];
    var _c2$1 = ["[filter]"];
    var TableComponent = /** @class */ (function () {
        function TableComponent(router) {
            this.router = router;
            this.rowSortable = false;
            this.canShowActionButtons = true;
            this.downloadFilePath = '';
            this.actionSort = new i0.EventEmitter();
            this.pageChange = new i0.EventEmitter();
            this.entryPageChange = new i0.EventEmitter();
            this.actionButton = new i0.EventEmitter();
            //RTS
            this.dropdownSearch = new i0.EventEmitter();
            this.dropdownFilter = new i0.EventEmitter();
            // gallery
            this.openGalleryImagePreview = new i0.EventEmitter();
            this.doGalleryUpdateIndexCallback = new i0.EventEmitter();
            this.doGalleryUpdateInput = new i0.EventEmitter();
            this.cellType = CELL_TYPE;
            this.dateFormat = DATE_FORMAT;
            this.entryPerpages = new Array();
            this.isShowFilter = false;
            this.isShowActionHeaderBtn = true;
            this.inputMaxLenght = 255;
            this.TablelanguageObj = TABLE_LANGUAGE_CONFIG;
        }
        TableComponent.prototype.ngOnInit = function () {
            this.tableType = this.type ? this.type : "RTS";
            $(document).on('click', '.dropdown-menu', function (e) {
                e.stopPropagation();
            });
            this.entryPerpages.push(new EntryPerpage("5", 5));
            this.entryPerpages.push(new EntryPerpage("10", 10));
            this.entryPerpages.push(new EntryPerpage("20", 20));
            for (var i = 0; i < this.entryPerpages.length; i++) {
                var entry = this.entryPerpages[i];
                entry.selected = false;
                if (entry.value === this.tableOption.itemsPerPage) {
                    entry.selected = true;
                }
            }
            if (this.rowSortable) {
                var self_1 = this;
                setTimeout(function () {
                    $('#sortable').sortable({
                        placeholder: "sortable-placeholder",
                        helper: 'clone',
                        axis: "y",
                        cursor: "move",
                        deactivate: function (event, ui) {
                            var dragPostionObj = { fromIndex: ui.item[0].cells[1].innerText, toIndex: self_1.getSibling(ui.item[0]) };
                            self_1.doUpdateIndex(dragPostionObj);
                        }
                    });
                    $("#sortable").disableSelection();
                }, 0);
            }
        };
        TableComponent.prototype.getSibling = function (ui) {
            var text = "";
            if (ui.nextElementSibling && ui.nextElementSibling.cells) {
                text = ui.nextElementSibling.cells[1].innerText;
            }
            else if (ui.previousElementSibling && ui.previousElementSibling.cells) {
                text = ui.previousElementSibling.cells[1].innerText;
            }
            return text;
        };
        TableComponent.prototype.startItemNumber = function () {
            return (this.tableOption.currentPage * this.tableOption.itemsPerPage) - this.tableOption.itemsPerPage + 1;
        };
        TableComponent.prototype.endItemNumber = function () {
            return (this.tableOption.currentPage * this.tableOption.itemsPerPage) < this.tableOption.totalItems ? (this.tableOption.currentPage * this.tableOption.itemsPerPage) : this.tableOption.totalItems;
        };
        TableComponent.prototype.noDataFound = function () {
            return this.tableOption.tableRows.length === 0 && this.tableOption.currentPage <= 1;
        };
        TableComponent.prototype.hideFilter = function () {
            this.isShowFilter = false;
        };
        TableComponent.prototype.cellClicked = function (cell) {
            if (cell.routeLink.trim().length > 0) {
                this.router.navigate([cell.routeLink]);
            }
        };
        TableComponent.prototype.showFilter = function () {
            this.isShowFilter = true;
        };
        TableComponent.prototype.toggleFilter = function () {
            this.isShowFilter = !this.isShowFilter;
        };
        TableComponent.prototype.doAction = function (actionBtn) {
            this.actionButton.emit(actionBtn);
        };
        TableComponent.prototype.doPageChange = function () {
            var currentOffset = (this.tableOption.currentPage - 1) * this.tableOption.itemsPerPage;
            this.pageChange.emit(currentOffset);
        };
        TableComponent.prototype.doEntryPage = function (value) {
            this.tableOption.itemsPerPage = value;
            this.tableOption.currentPage = 1;
            this.entryPageChange.emit(value);
        };
        TableComponent.prototype.doSort = function (column) {
            if (!column.sortable) {
                return;
            }
            var columns = this.tableOption.tableColumns;
            this.currentSortColumnName = undefined;
            this.currentSortType = undefined;
            for (var i = 0; i < this.tableOption.tableColumns.length; i++) {
                var col = this.tableOption.tableColumns[i];
                if (col.name === column.name) {
                    if (col.sort === SORT.ASC) {
                        col.sort = SORT.DESC;
                    }
                    else if (col.sort === SORT.DESC) {
                        col.sort = SORT.NONE;
                        col.sort = this.tableOption.defaultSort.sortColumnName === col.name ? SORT.ASC : SORT.NONE;
                        this.currentSortColumnName = this.tableOption.defaultSort.sortColumnName;
                        this.currentSortType = this.tableOption.defaultSort.sortType;
                    }
                    else if (col.sort === SORT.NONE) {
                        col.sort = SORT.ASC;
                    }
                }
                else {
                    col.sort = SORT.NONE;
                }
            }
            this.actionSort.emit(column);
        };
        TableComponent.prototype.isSortAsc = function (column) {
            return column.sort === SORT.ASC || (this.currentSortColumnName && this.currentSortColumnName === column.name && this.currentSortType === SORT.ASC);
        };
        TableComponent.prototype.isSortDesc = function (column) {
            return column.sort === SORT.DESC || (this.currentSortColumnName && this.currentSortColumnName === column.name && this.currentSortType === SORT.DESC);
        };
        //RTS
        //dropdown filter
        TableComponent.prototype.filterOnTableColumn = function (data, name) {
            this.dropdownFilter.emit({ filterData: data, columnName: name });
        };
        TableComponent.prototype.filterOnTableColumnReset = function (data) {
            data.forEach(function (element) {
                element.checked = false;
            });
        };
        //dropdown search
        TableComponent.prototype.tableDropdownSearch = function (searchString, name) {
            this.dropdownSearch.emit({ searchStr: searchString, columnName: name });
        };
        TableComponent.prototype.tableDropdownSearchReset = function (searchString) {
            searchString = "";
        };
        // gallery
        TableComponent.prototype.doOpenPreview = function (imgCode) {
            this.openGalleryImagePreview.emit(imgCode);
        };
        TableComponent.prototype.doUpdateIndex = function (dragPostionObj) {
            this.doGalleryUpdateIndexCallback.emit(dragPostionObj);
        };
        TableComponent.prototype.doUpdateInputEvent = function (row, canEdit, newValue, originalValue) {
            if (canEdit && newValue.value !== originalValue) {
                if (newValue.value.length <= this.inputMaxLenght) {
                    this.doGalleryUpdateInput.emit(row);
                }
                else {
                    newValue.value = originalValue;
                }
            }
        };
        TableComponent.prototype.focusInput = function (id) {
            setTimeout(function () {
                document.getElementById(id).focus();
            }, 0);
        };
        TableComponent.prototype.expand = function (id, event) {
            if (this.expandedRowId === id) {
                $("#" + this.expandedRowId).slideUp("fast");
                this.expandedRowId = undefined;
            }
            else {
                if (this.expandedRowId && this.expandedRowId !== id) {
                    $("#" + this.expandedRowId).slideUp("fast");
                }
                $("#" + id).slideToggle("fast");
                this.expandedRowId = id;
            }
            event.preventDefault();
        };
        return TableComponent;
    }());
    TableComponent.ɵfac = function TableComponent_Factory(t) { return new (t || TableComponent)(i0.ɵɵdirectiveInject(i1$2.Router)); };
    TableComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TableComponent, selectors: [["via-table"]], inputs: { type: "type", tableOption: "tableOption", rowSortable: "rowSortable", canShowActionButtons: "canShowActionButtons", downloadFilePath: "downloadFilePath", TablelanguageObj: "TablelanguageObj" }, outputs: { actionSort: "actionSort", pageChange: "pageChange", entryPageChange: "entryPageChange", actionButton: "actionButton", dropdownSearch: "dropdownSearch", dropdownFilter: "dropdownFilter", openGalleryImagePreview: "openGalleryImagePreview", doGalleryUpdateIndexCallback: "doGalleryUpdateIndexCallback", doGalleryUpdateInput: "doGalleryUpdateInput" }, ngContentSelectors: _c2$1, decls: 24, vars: 13, consts: [["id", "win-table"], [1, "win-table-wrapper"], [1, "table-top"], [1, "action-wrapper"], [1, "action-container"], ["class", "filter", 4, "ngIf"], [1, "action-button"], [4, "ngIf"], [1, "paging"], [1, "paging-container"], [1, "number-per-page"], [3, "change"], ["entry", ""], [3, "selected", 4, "ngFor", "ngForOf"], ["class", "paging-wrapper", 4, "ngIf"], ["id", "sortable"], [4, "ngFor", "ngForOf"], ["class", "paging", 4, "ngIf"], ["class", "win-table-footer", 4, "ngIf"], [1, "filter"], [1, "filter-container"], [1, "dropdown", 3, "ngClass"], [1, "dropdown-content"], ["src", "./assets/icons/ic_filter.svg", 3, "click"], [1, "filter-text", 3, "click"], [1, "filter-mask", 3, "click"], [1, "dropdown-menu-items", "z-depth-1"], ["type", "button", "class", "btn uppercase-text m-l-10", 3, "ngClass", "click", 4, "ngIf"], ["type", "button", 1, "btn", "uppercase-text", "m-l-10", 3, "ngClass", "click"], [3, "selected"], [1, "paging-wrapper"], [1, "paging-title"], ["size", "sm", 3, "directionLinks", "collectionSize", "rotate", "maxSize", "pageSize", "page", "pageChange"], ["class", "heading", 4, "ngIf"], [1, "heading"], ["class", "handle", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [1, "handle", 3, "ngClass", "click"], [1, "header-container"], ["class", "fa fa-sort-amount-asc", "aria-hidden", "true", 4, "ngIf"], ["class", "fa fa-sort-amount-desc", "aria-hidden", "true", 4, "ngIf"], ["aria-hidden", "true", 1, "fa", "fa-sort-amount-asc"], ["aria-hidden", "true", 1, "fa", "fa-sort-amount-desc"], ["style", "border-radius: 50px;", "class", "handle", 3, "click", 4, "ngFor", "ngForOf"], [1, "handle", 2, "border-radius", "50px", 3, "click"], ["ngbDropdown", "", "display", "dynamic", 4, "ngIf"], ["ngbDropdown", "", "display", "dynamic"], ["ngbDropdownToggle", "", "src", "./assets/icons/filter.svg", 1, "filter-icon"], ["ngbDropdownMenu", "", 1, "dropdown-menu", "dropdown-menu-right"], ["class", "table-dropdown-item", 4, "ngFor", "ngForOf"], [1, "table-dropdown-btm-wrapper"], [1, "table-dropdown-btn", 3, "click"], [1, "table-dropdown-item"], ["type", "checkbox", 1, "styled-checkbox", 3, "id", "value", "ngModel", "ngModelChange"], [3, "for"], ["id", "dropdownForm1", "ngbDropdownToggle", "", "src", "./assets/icons/search-filter.svg", 1, "search-icon"], ["ngbDropdownMenu", "", "aria-labelledby", "dropdownForm1", 1, "dropdown-menu", "dropdown-menu-right"], [1, "table-dropdown-search-input", 3, "ngModel", "ngModelChange"], [3, "id", "ngClass"], ["class", "tablerow", 3, "ngClass", "ngSwitch", 4, "ngFor", "ngForOf"], ["class", "handle", 4, "ngIf"], ["class", "extra-row", 4, "ngIf"], [1, "tablerow", 3, "ngClass", "ngSwitch"], ["class", "thumbnail-wrapper", 4, "ngSwitchCase"], ["class", "cell-input-wrapper", 4, "ngSwitchCase"], [3, "title", "ngClass", "click", 4, "ngSwitchDefault"], ["class", "status", 3, "title", "ngClass", 4, "ngSwitchCase"], [3, "title", 4, "ngSwitchCase"], [4, "ngSwitchCase"], [1, "thumbnail-wrapper"], ["width", "60px", 3, "src", "click"], [1, "cell-input-wrapper"], [1, "cell-input", 3, "ngModel", "id", "disabled", "ngModelChange", "focusin", "focusout", "keyup.enter"], ["class", "btn btn-link btn-action cell-edit-icon", 3, "click", 4, "ngIf"], ["class", "danger-text", "translate", "", 4, "ngIf"], [1, "btn", "btn-link", "btn-action", "cell-edit-icon", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-pencil"], ["translate", "", 1, "danger-text"], [3, "title", "ngClass", "click"], [1, "status", 3, "title", "ngClass"], [3, "title"], ["type", "button", "class", "btn expand-btn", 3, "click", 4, "ngIf"], ["class", "btn-group mr-3", 4, "ngIf"], ["class", "btn btn-link btn-action icon-btn", 3, "disabled", "title", "click", 4, "ngIf"], ["type", "button", "class", "btn btn-light btn-action", 3, "disabled", "ngClass", "click", 4, "ngIf"], [1, "btn", "btn-link", "btn-action", "icon-btn", 3, "disabled", "title", "click"], [3, "src"], ["type", "button", 1, "btn", "btn-light", "btn-action", 3, "disabled", "ngClass", "click"], ["type", "button", 1, "btn", "expand-btn", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-caret-down", "arrow", 3, "ngClass"], [1, "btn-group", "mr-3"], ["ngbDropdown", "", "placement", "bottom-right", "role", "group", "aria-label", "Button group with nested dropdown", 1, "btn-group", "table-dropdown"], ["ngbDropdownToggle", "", 1, "btn"], ["aria-hidden", "true", 1, "fa", "fa-ellipsis-v"], ["ngbDropdownMenu", "", 1, "dropdown-menu"], ["ngbDropdownItem", "", 3, "disabled", "click"], [1, "handle"], [1, "fa", "fa-sort"], [1, "extra-row"], [1, "extra-wrapper", 2, "display", "none", 3, "id"], [3, "innerHTML"], ["size", "md", 1, "d-flex", "justify-content-end", 3, "collectionSize", "rotate", "maxSize", "pageSize", "page", "pageChange"], ["ngbPaginationPrevious", ""], ["ngbPaginationNext", ""], ["src", "./assets/icons/back.svg", 1, "handle-icon"], ["src", "./assets/icons/next.svg", 1, "handle-icon"], [1, "win-table-footer"], [1, "number"], [1, "total"]], template: function TableComponent_Template(rf, ctx) {
            if (rf & 1) {
                var _r139_1 = i0.ɵɵgetCurrentView();
                i0.ɵɵprojectionDef(_c1$1);
                i0.ɵɵelementStart(0, "div", 0);
                i0.ɵɵelementStart(1, "div", 1);
                i0.ɵɵelementStart(2, "div", 2);
                i0.ɵɵelementStart(3, "div", 3);
                i0.ɵɵelementStart(4, "div", 4);
                i0.ɵɵtemplate(5, TableComponent_div_5_Template, 11, 2, "div", 5);
                i0.ɵɵelementStart(6, "div", 6);
                i0.ɵɵtemplate(7, TableComponent_div_7_Template, 2, 1, "div", 7);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementStart(8, "div", 8);
                i0.ɵɵelementStart(9, "div", 9);
                i0.ɵɵelementStart(10, "div", 10);
                i0.ɵɵelementStart(11, "select", 11, 12);
                i0.ɵɵlistener("change", function TableComponent_Template_select_change_11_listener() { i0.ɵɵrestoreView(_r139_1); var _r2 = i0.ɵɵreference(12); return ctx.doEntryPage(_r2.value); });
                i0.ɵɵtemplate(13, TableComponent_option_13_Template, 2, 3, "option", 13);
                i0.ɵɵelementEnd();
                i0.ɵɵelementStart(14, "span");
                i0.ɵɵtext(15);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵtemplate(16, TableComponent_div_16_Template, 4, 7, "div", 14);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵelementStart(17, "table");
                i0.ɵɵtemplate(18, TableComponent_thead_18_Template, 3, 2, "thead", 7);
                i0.ɵɵelementStart(19, "tbody", 15);
                i0.ɵɵtemplate(20, TableComponent_ng_container_20_Template, 5, 5, "ng-container", 16);
                i0.ɵɵelementEnd();
                i0.ɵɵtemplate(21, TableComponent_tfoot_21_Template, 4, 2, "tfoot", 7);
                i0.ɵɵelementEnd();
                i0.ɵɵelementEnd();
                i0.ɵɵtemplate(22, TableComponent_div_22_Template, 3, 1, "div", 17);
                i0.ɵɵtemplate(23, TableComponent_div_23_Template, 10, 7, "div", 18);
                i0.ɵɵelementEnd();
            }
            if (rf & 2) {
                i0.ɵɵclassMap(ctx.tableType);
                i0.ɵɵadvance(5);
                i0.ɵɵproperty("ngIf", ctx.tableOption.tableFilter.hasFilter);
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("ngIf", ctx.tableOption.tableActions.length > 0 && ctx.canShowActionButtons);
                i0.ɵɵadvance(6);
                i0.ɵɵproperty("ngForOf", ctx.entryPerpages);
                i0.ɵɵadvance(2);
                i0.ɵɵtextInterpolate1(" ", ctx.TablelanguageObj.table_entries_per_page, "");
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", !ctx.noDataFound());
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("ngIf", !ctx.noDataFound());
                i0.ɵɵadvance(2);
                i0.ɵɵproperty("ngForOf", ctx.tableOption.tableRows);
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", ctx.noDataFound() && ctx.tableType === "WIN");
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", ctx.tableType === "RTS");
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", !ctx.noDataFound() && ctx.tableType === "WIN");
            }
        }, directives: [i2.NgIf, i2.NgForOf, i2.NgClass, i3.NgSelectOption, i3.ɵangular_packages_forms_forms_x, i1.NgbPagination, i1.NgbDropdown, i1.NgbDropdownToggle, i1.NgbDropdownMenu, i3.CheckboxControlValueAccessor, i3.NgControlStatus, i3.NgModel, i3.DefaultValueAccessor, i2.NgSwitch, i2.NgSwitchCase, i2.NgSwitchDefault, i1.NgbDropdownItem, i1.NgbPaginationPrevious, i1.NgbPaginationNext], pipes: [FilterPipe, i2.DatePipe], styles: ["#win-table[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:column}#win-table[_ngcontent-%COMP%]   .extra-row[_ngcontent-%COMP%]{border-bottom:0;height:auto;min-height:0}#win-table[_ngcontent-%COMP%]   .extra-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding-bottom:0!important;padding-top:0!important}#win-table[_ngcontent-%COMP%]   .expand-btn[_ngcontent-%COMP%]{background:none;margin-top:5px;text-align:center}#win-table[_ngcontent-%COMP%]   .expand-btn[_ngcontent-%COMP%]   .arrow-rotate[_ngcontent-%COMP%]{transform:rotate(-180deg)}#win-table[_ngcontent-%COMP%]   .expand-btn[_ngcontent-%COMP%]   .arrow[_ngcontent-%COMP%]{transition:.2s ease}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]:after{display:none}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-item[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]{background:none;margin-top:4px}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]:hover{background:#f7f8f9}#win-table[_ngcontent-%COMP%]   .ui-sortable-helper[_ngcontent-%COMP%]{display:inline-table}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]{align-items:center;background:#fff;border:1px solid #ebedf8;border-radius:3px;display:flex;flex-direction:column;font-size:14px;padding-bottom:40px;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]{align-items:center;display:flex;justify-content:center;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]{align-items:baseline;display:flex;margin-top:20px;width:95%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]{align-items:baseline;flex:3}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:9;list-style:none;margin:0;padding:0;z-index:1}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0;padding:0}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]{width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]{align-items:baseline;display:flex;flex-direction:row;justify-content:flex-start}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{cursor:pointer;margin-right:5px;width:18px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]   .filter-text[_ngcontent-%COMP%]{color:#0800d1;font-weight:700}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:none;list-style:none;padding:10px 10px 30px;position:absolute;top:100%;width:250%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{width:100%;z-index:1}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown.open[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{display:flex}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .action-button[_ngcontent-%COMP%]{align-items:baseline;display:flex;flex:7;justify-content:flex-end}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]{align-items:center;border-bottom:1px solid #f0f0f0;display:flex;justify-content:center;padding:10px 0;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]{align-items:baseline;display:flex;width:95%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .number-per-page[_ngcontent-%COMP%]{align-items:baseline;flex:1}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .number-per-page[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{background:#fff;border:1px solid #f0f0f0;border-radius:5px;max-width:120px;padding:5px;width:100px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .paging-wrapper[_ngcontent-%COMP%]{align-items:baseline;color:#aab2c0;display:flex;flex:1;justify-content:flex-end}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .paging-wrapper[_ngcontent-%COMP%]   .paging-title[_ngcontent-%COMP%]{margin-right:10px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]{width:95%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]{color:#b4bac6;font-size:12px;padding:30px 15px 10px 0;text-align:left;text-transform:uppercase}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th.sortable[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:row;justify-content:center;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{flex:9}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#000;flex:1;text-align:right}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]{min-height:40px;vertical-align:text-top}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{color:#b4bac6;font-size:14px;padding:10px 0}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td.bold[_ngcontent-%COMP%]{color:#354052;font-weight:700}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]   .hand[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]   button.btn-action[_ngcontent-%COMP%]{margin-right:5px;margin-top:5px;padding:10px 0!important;text-transform:uppercase;width:80px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tfoot[_ngcontent-%COMP%]{font-size:24px;font-weight:700;text-align:center}#win-table[_ngcontent-%COMP%]   .win-table-footer[_ngcontent-%COMP%]{color:#aab2c0;font-size:15px;margin-top:20px;width:95%}#win-table[_ngcontent-%COMP%]   .win-table-footer[_ngcontent-%COMP%]   .number[_ngcontent-%COMP%]{color:#000;font-weight:700}#win-table[_ngcontent-%COMP%]   .win-table-footer[_ngcontent-%COMP%]   .total[_ngcontent-%COMP%]{font-weight:700}#win-table[_ngcontent-%COMP%]   .ellipsis-td[_ngcontent-%COMP%]{max-width:110px;overflow:hidden;padding:0 10px!important;text-overflow:ellipsis;white-space:nowrap}#win-table[_ngcontent-%COMP%]   .wordwrap-td[_ngcontent-%COMP%]{display:block;max-width:110px}#win-table[_ngcontent-%COMP%]   .ACTION[_ngcontent-%COMP%]{text-align:center!important}#win-table[_ngcontent-%COMP%]   .icon-btn[_ngcontent-%COMP%]{width:50px!important}#win-table[_ngcontent-%COMP%]   .icon-btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:20px}#win-table[_ngcontent-%COMP%]   .text-align-right[_ngcontent-%COMP%]{padding-right:10px!important;text-align:right}#win-table[_ngcontent-%COMP%]   .header-text-align-right[_ngcontent-%COMP%]{padding-right:10px!important;text-align:right!important}#win-table[_ngcontent-%COMP%]   .filter-mask[_ngcontent-%COMP%]{bottom:0;display:none;left:0;position:fixed;right:0;top:0}#win-table[_ngcontent-%COMP%]   .open[_ngcontent-%COMP%]   .filter-mask[_ngcontent-%COMP%]{display:block}#win-table[_ngcontent-%COMP%]   .thumbnail-wrapper[_ngcontent-%COMP%]{cursor:pointer;height:30px;overflow:hidden;width:60px}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-edit-icon[_ngcontent-%COMP%]{color:#5a5a5a;cursor:pointer;margin-right:0;margin-top:0;width:35px!important}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-edit-icon[_ngcontent-%COMP%]:hover{color:#9a0000}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-input[_ngcontent-%COMP%]{background:#fff;border:0}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-input[_ngcontent-%COMP%]:focus{border-bottom:1px solid #07f}#win-table[_ngcontent-%COMP%]   .sortable-row[_ngcontent-%COMP%]{cursor:move}#win-table[_ngcontent-%COMP%]   .sortable-row[_ngcontent-%COMP%]:hover{background:hsla(0,0%,50.2%,.03)}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]{opacity:0;position:absolute}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%] + label[_ngcontent-%COMP%]{cursor:pointer;font-weight:200;padding:0;position:relative;text-transform:none}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%] + label[_ngcontent-%COMP%]:before{background:#fff;border:1px solid #a6b4bf;border-radius:2px;content:\"\";display:inline-block;height:20px;margin-right:10px;vertical-align:text-top;width:20px}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:checked + label[_ngcontent-%COMP%]:before{border:1px solid #4ca0fd}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:disabled + label[_ngcontent-%COMP%]{color:#b8b8b8;cursor:auto}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:disabled + label[_ngcontent-%COMP%]:before{background:#ddd;box-shadow:none}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:checked + label[_ngcontent-%COMP%]:after{background:#4ca0fd;box-shadow:2px 0 0 #4ca0fd,4px 0 0 #4ca0fd,4px -2px 0 #4ca0fd,4px -4px 0 #4ca0fd,4px -6px 0 #4ca0fd,4px -8px 0 #4ca0fd;content:\"\";height:2px;left:5px;position:absolute;top:10px;transform:rotate(45deg);width:2px}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]:before{border-bottom:7px solid rgba(0,0,0,.03);border-left:7px solid transparent;border-right:7px solid transparent;content:\"\";display:inline-block;position:absolute;right:2px;top:-7px}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]{top:25px!important}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]:after{border-bottom:6px solid #fff;border-left:6px solid transparent;border-right:6px solid transparent;content:\"\";display:inline-block;position:absolute;right:3px;top:-6px}.RTS[_ngcontent-%COMP%]   .dropdown-menu-right[_ngcontent-%COMP%]{left:auto!important;right:0!important}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]{border:none;box-shadow:0 3px 11px -1px #d6d6d6;padding:10px 10px 0}.RTS[_ngcontent-%COMP%]   .table-dropdown-btn[_ngcontent-%COMP%]{background:#4ca0fd;border:none;border-radius:5px;color:#fff;cursor:pointer;height:40px;margin:5px;min-width:100px;text-align:center}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]{border:1px solid #e2e6ea!important;border-radius:5px!important;padding-bottom:0!important}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]{width:100%!important}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]{border-bottom:1px solid #e2e6ea}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]{height:50px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]{background:#fdfefe;padding-top:6px!important}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]{color:#577286;padding-left:15px;text-transform:uppercase}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .filter-icon[_ngcontent-%COMP%]{cursor:pointer;width:17px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .search-icon[_ngcontent-%COMP%]{cursor:pointer;margin-top:3px;width:25px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .table-dropdown-item[_ngcontent-%COMP%]{padding-left:5px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .table-dropdown-btm-wrapper[_ngcontent-%COMP%]{display:inline-flex;text-align:center}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .table-dropdown-search-input[_ngcontent-%COMP%]{border:1px solid #d8d8d8;border-radius:5px;height:40px;margin-bottom:10px;margin-left:5px;margin-top:10px;padding-left:10px;width:96%}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]{border-bottom:1px solid #e2e6ea;height:50px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td.tablerow[_ngcontent-%COMP%]{padding-bottom:0!important;padding-left:15px!important;padding-top:0}.RTS[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]{margin-top:35px;width:100%}.RTS[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .handle-icon[_ngcontent-%COMP%]{position:relative;top:-2px}"] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(TableComponent, [{
                type: i0.Component,
                args: [{
                        selector: "via-table",
                        templateUrl: "./table.html",
                        styleUrls: ["./table.scss"]
                    }]
            }], function () { return [{ type: i1$2.Router }]; }, { type: [{
                    type: i0.Input
                }], tableOption: [{
                    type: i0.Input
                }], rowSortable: [{
                    type: i0.Input
                }], canShowActionButtons: [{
                    type: i0.Input
                }], downloadFilePath: [{
                    type: i0.Input
                }], actionSort: [{
                    type: i0.Output
                }], pageChange: [{
                    type: i0.Output
                }], entryPageChange: [{
                    type: i0.Output
                }], actionButton: [{
                    type: i0.Output
                }], dropdownSearch: [{
                    type: i0.Output
                }], dropdownFilter: [{
                    type: i0.Output
                }], openGalleryImagePreview: [{
                    type: i0.Output
                }], doGalleryUpdateIndexCallback: [{
                    type: i0.Output
                }], doGalleryUpdateInput: [{
                    type: i0.Output
                }], TablelanguageObj: [{
                    type: i0.Input
                }] });
    })();

    var TableModule = /** @class */ (function () {
        function TableModule() {
        }
        return TableModule;
    }());
    TableModule.ɵmod = i0.ɵɵdefineNgModule({ type: TableModule });
    TableModule.ɵinj = i0.ɵɵdefineInjector({ factory: function TableModule_Factory(t) { return new (t || TableModule)(); }, imports: [[
                i1$2.RouterModule.forChild([]),
                i1.NgbModule,
                i3.FormsModule,
                i2.CommonModule,
                i3.ReactiveFormsModule
            ], i1$2.RouterModule] });
    (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(TableModule, { declarations: [TableComponent, FilterPipe], imports: [i1$2.RouterModule, i1.NgbModule,
                i3.FormsModule,
                i2.CommonModule,
                i3.ReactiveFormsModule], exports: [TableComponent, FilterPipe, i1$2.RouterModule] });
    })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(TableModule, [{
                type: i0.NgModule,
                args: [{
                        imports: [
                            i1$2.RouterModule.forChild([]),
                            i1.NgbModule,
                            i3.FormsModule,
                            i2.CommonModule,
                            i3.ReactiveFormsModule
                        ],
                        declarations: [TableComponent, FilterPipe],
                        exports: [TableComponent, FilterPipe, i1$2.RouterModule]
                    }]
            }], null, null);
    })();

    var TableAction = /** @class */ (function () {
        function TableAction(name, routeLink, css, type, hided) {
            if (name === void 0) { name = ""; }
            if (routeLink === void 0) { routeLink = ""; }
            if (css === void 0) { css = "btn-primary"; }
            if (type === void 0) { type = ACTION_TYPE.BUTTON; }
            if (hided === void 0) { hided = false; }
            this._name = name;
            this._type = type;
            this._routeLink = routeLink;
            this._css = css;
            this._hided = hided;
        }
        Object.defineProperty(TableAction.prototype, "hided", {
            /**
             * Getter hided
             * return {boolean}
             */
            get: function () {
                return this._hided;
            },
            /**
             * Setter hided
             * param {boolean} value
             */
            set: function (value) {
                this._hided = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableAction.prototype, "css", {
            get: function () {
                return this._css;
            },
            set: function (value) {
                this._css = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableAction.prototype, "name", {
            get: function () {
                return this._name;
            },
            set: function (value) {
                this._name = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableAction.prototype, "type", {
            get: function () {
                return this._type;
            },
            set: function (value) {
                this._type = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableAction.prototype, "routeLink", {
            get: function () {
                return this._routeLink;
            },
            set: function (value) {
                this._routeLink = value;
            },
            enumerable: false,
            configurable: true
        });
        return TableAction;
    }());
    var ACTION_TYPE = {
        BUTTON: "BUTTON",
        LINK: "LINK",
        MODAL: "MODAL"
    };

    var ButtonAction = /** @class */ (function () {
        function ButtonAction(name, callbackFunc, disabled, iconUrl, isDropdownItem) {
            if (name === void 0) { name = ""; }
            if (disabled === void 0) { disabled = false; }
            if (iconUrl === void 0) { iconUrl = undefined; }
            if (isDropdownItem === void 0) { isDropdownItem = false; }
            this._isDropdownItem = false;
            this._name = name;
            this._buttonAction = callbackFunc;
            this._disabled = disabled;
            this._iconUrl = iconUrl;
            this._isDropdownItem = isDropdownItem;
        }
        Object.defineProperty(ButtonAction.prototype, "name", {
            /**
             * Getter name
             * return {string}
             */
            get: function () {
                return this._name;
            },
            /**
             * Setter name
             * param {string} value
             */
            set: function (value) {
                this._name = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ButtonAction.prototype, "buttonAction", {
            /**
             * Getter buttonAction
             * return {Function}
             */
            get: function () {
                return this._buttonAction;
            },
            /**
             * Setter buttonAction
             * param {Function} value
             */
            set: function (value) {
                this._buttonAction = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ButtonAction.prototype, "disabled", {
            /**
             * Getter disabled
             * return {boolean}
             */
            get: function () {
                return this._disabled;
            },
            /**
             * Setter disabled
             * param {boolean} value
             */
            set: function (value) {
                this._disabled = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ButtonAction.prototype, "iconUrl", {
            /**
             * Getter iconUrl
             * return {string}
             */
            get: function () {
                return this._iconUrl;
            },
            /**
             * Setter iconUrl
             * param {string} value
             */
            set: function (value) {
                this._iconUrl = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(ButtonAction.prototype, "isDropdownItem", {
            /**
             * Getter isDropdownItem
             * return {boolean }
             */
            get: function () {
                return this._isDropdownItem;
            },
            /**
             * Setter isDropdownItem
             * param {boolean } value
             */
            set: function (value) {
                this._isDropdownItem = value;
            },
            enumerable: false,
            configurable: true
        });
        return ButtonAction;
    }());

    var TableColumn = /** @class */ (function () {
        function TableColumn(value, name, sortable, sort, css, hidden, filterList, searchable, searchString) {
            if (value === void 0) { value = ""; }
            if (name === void 0) { name = ""; }
            if (sortable === void 0) { sortable = false; }
            if (sort === void 0) { sort = SORT.NONE; }
            if (css === void 0) { css = ""; }
            if (hidden === void 0) { hidden = false; }
            if (filterList === void 0) { filterList = []; }
            if (searchable === void 0) { searchable = false; }
            if (searchString === void 0) { searchString = ""; }
            this._name = name;
            this._value = value;
            this._sort = sort;
            this._sortable = sortable;
            this._css = css;
            this._hidden = hidden;
            this._filterList = filterList;
            this._searchStr = searchString;
            this._searchable = searchable;
        }
        Object.defineProperty(TableColumn.prototype, "name", {
            /**
             * Getter name
             * return {string}
             */
            get: function () {
                return this._name;
            },
            /**
             * Setter name
             * param {string} value
             */
            set: function (value) {
                this._name = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableColumn.prototype, "value", {
            /**
             * Getter value
             * return {string}
             */
            get: function () {
                return this._value;
            },
            /**
             * Setter value
             * param {string} value
             */
            set: function (value) {
                this._value = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableColumn.prototype, "sortable", {
            /**
             * Getter sortable
             * return {boolean}
             */
            get: function () {
                return this._sortable;
            },
            /**
             * Setter sortable
             * param {boolean} value
             */
            set: function (value) {
                this._sortable = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableColumn.prototype, "sort", {
            /**
             * Getter sort
             * return {string}
             */
            get: function () {
                return this._sort;
            },
            /**
             * Setter sort
             * param {string} value
             */
            set: function (value) {
                this._sort = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableColumn.prototype, "css", {
            /**
             * Getter css
             * return {string}
             */
            get: function () {
                return this._css;
            },
            /**
             * Setter css
             * param {string} value
             */
            set: function (value) {
                this._css = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableColumn.prototype, "hidden", {
            /**
             * Getter hidden
             * return {boolean}
             */
            get: function () {
                return this._hidden;
            },
            /**
             * Setter hidden
             * param {boolean} value
             */
            set: function (value) {
                this._hidden = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableColumn.prototype, "filterList", {
            /**
             * Getter filterList
             * return {TableFilterDropdownItem[]}
             */
            get: function () {
                return this._filterList;
            },
            /**
             * Setter filterList
             * param {TableFilterDropdownItem[]} value
             */
            set: function (value) {
                this._filterList = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableColumn.prototype, "searchStr", {
            /**
             * Getter searchStr
             * return {string}
             */
            get: function () {
                return this._searchStr;
            },
            /**
             * Setter searchStr
             * param {string} value
             */
            set: function (value) {
                this._searchStr = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableColumn.prototype, "searchable", {
            /**
             * Getter searchable
             * return {boolean}
             */
            get: function () {
                return this._searchable;
            },
            /**
             * Setter searchable
             * param {boolean} value
             */
            set: function (value) {
                this._searchable = value;
            },
            enumerable: false,
            configurable: true
        });
        return TableColumn;
    }());

    var TableFilterDropdownItem = /** @class */ (function () {
        function TableFilterDropdownItem(title, value, checked) {
            if (checked === void 0) { checked = false; }
            this._title = title;
            this._value = value;
            this._checked = checked;
        }
        Object.defineProperty(TableFilterDropdownItem.prototype, "title", {
            /**
             * Getter title
             * return {string}
             */
            get: function () {
                return this._title;
            },
            /**
             * Setter title
             * param {string} value
             */
            set: function (value) {
                this._title = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableFilterDropdownItem.prototype, "value", {
            /**
             * Getter value
             * return {string}
             */
            get: function () {
                return this._value;
            },
            /**
             * Setter value
             * param {string} value
             */
            set: function (value) {
                this._value = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableFilterDropdownItem.prototype, "checked", {
            /**
             * Getter checked
             * return {boolean}
             */
            get: function () {
                return this._checked;
            },
            /**
             * Setter checked
             * param {boolean} value
             */
            set: function (value) {
                this._checked = value;
            },
            enumerable: false,
            configurable: true
        });
        return TableFilterDropdownItem;
    }());

    var TableRow = /** @class */ (function () {
        function TableRow(tableCells, css, expandedText) {
            if (tableCells === void 0) { tableCells = []; }
            if (css === void 0) { css = ""; }
            if (expandedText === void 0) { expandedText = ""; }
            this._tableCells = tableCells;
            this._css = css;
            this._expandedText = expandedText;
            this._editAble = false;
            this._activeAble = false;
            this._deactiveAble = false;
            this._deleteAble = false;
        }
        Object.defineProperty(TableRow.prototype, "tableCells", {
            /**
             * Getter tableCells
             * return {TableCell[]}
             */
            get: function () {
                return this._tableCells;
            },
            /**
             * Setter tableCells
             * param {TableCell[]} value
             */
            set: function (value) {
                this._tableCells = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableRow.prototype, "css", {
            /**
             * Getter css
             * return {string}
             */
            get: function () {
                return this._css;
            },
            /**
             * Setter css
             * param {string} value
             */
            set: function (value) {
                this._css = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableRow.prototype, "editAble", {
            /**
             * Getter editAble
             * return {boolean}
             */
            get: function () {
                return this._editAble;
            },
            /**
             * Setter editAble
             * param {boolean} value
             */
            set: function (value) {
                this._editAble = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableRow.prototype, "deleteAble", {
            /**
             * Getter deleteAble
             * return {boolean}
             */
            get: function () {
                return this._deleteAble;
            },
            /**
             * Setter deleteAble
             * param {boolean} value
             */
            set: function (value) {
                this._deleteAble = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableRow.prototype, "deactiveAble", {
            /**
             * Getter deactiveAble
             * return {boolean}
             */
            get: function () {
                return this._deactiveAble;
            },
            /**
             * Setter deactiveAble
             * param {boolean} value
             */
            set: function (value) {
                this._deactiveAble = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableRow.prototype, "activeAble", {
            /**
             * Getter activeAble
             * return {boolean}
             */
            get: function () {
                return this._activeAble;
            },
            /**
             * Setter activeAble
             * param {boolean} value
             */
            set: function (value) {
                this._activeAble = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(TableRow.prototype, "expandedText", {
            /**
             * Getter expandedText
             * return {string}
             */
            get: function () {
                return this._expandedText;
            },
            /**
             * Setter expandedText
             * param {string} value
             */
            set: function (value) {
                this._expandedText = value;
            },
            enumerable: false,
            configurable: true
        });
        return TableRow;
    }());

    var TableRowValue = /** @class */ (function (_super) {
        __extends(TableRowValue, _super);
        function TableRowValue(tableCells, css, data) {
            if (tableCells === void 0) { tableCells = []; }
            if (css === void 0) { css = ""; }
            if (data === void 0) { data = null; }
            var _this = _super.call(this, tableCells, css) || this;
            _this._data = data;
            return _this;
        }
        Object.defineProperty(TableRowValue.prototype, "data", {
            get: function () {
                return this._data;
            },
            set: function (value) {
                this._data = value;
            },
            enumerable: false,
            configurable: true
        });
        return TableRowValue;
    }(TableRow));

    var UPLOAD_FILE_EVENT = {
        START_UPLOAD: "START_UPLOAD",
        UPLOAD_ERROR: "UPLOAD_ERROR",
        UPLOAD_SUCCESSED: "UPLOAD_SUCCESSED",
        FINISH_UPLOAD: "FINISH_UPLOAD"
    };
    var UPLOAD_FILE_LANGUAGE_CONFIG = {
        crop_image_yes: "Yes",
        crop_image_cancel: "Cancel",
        upload_file_drag_and_drop: "Drag and drop file here",
        upload_file_or: "or",
        upload_file_change: "Change",
        upload_file_browse_file: "Browser file",
        upload_file_browse_files: "Browser files"
    };
    var uploadType = {
        IMAGE: "IMAGE",
        FILE: "FILE"
    };
    var allowedContentTypesUploadTxt = ["text/plain"];
    var allowedContentTypesUploadImg = ["image/jpg", "image/jpeg", "image/png"];
    var allowedContentTypesUploadExel = [".xlsx", ".csv", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"];
    var uploadRatio = {
        SIXTEEN_NINE: {
            text: "SIXTEEN_NINE",
            ratioStr: "16:9",
            ratio: 16 / 9,
            calculateHeightRatio: 9 / 16,
            resizeToWidth: 1024
        },
        ONE_ONE: {
            text: "ONE_ONE",
            ratioStr: "1:1",
            ratio: 1 / 1,
            calculateHeightRatio: 1,
            resizeToWidth: 300
        },
        ONE_THREE: {
            text: "ONE_THREE",
            ratioStr: "1:3",
            ratio: 1 / 3,
            calculateHeightRatio: 3,
            resizeToWidth: 150
        },
        FOUR_ONE: {
            text: "FOUR_ONE",
            ratioStr: "4:1",
            ratio: 4 / 1,
            calculateHeightRatio: 1 / 4,
            resizeToWidth: 1024
        }
    };
    var ErrorObj = /** @class */ (function () {
        function ErrorObj(field, message) {
            if (field === void 0) { field = ""; }
            if (message === void 0) { message = ""; }
            this.field = field;
            this.message = message;
        }
        return ErrorObj;
    }());

    var UploadResponse = /** @class */ (function () {
        function UploadResponse(responseType, message, code) {
            if (responseType === void 0) { responseType = ""; }
            if (message === void 0) { message = ""; }
            if (code === void 0) { code = ""; }
            this._message = message;
            this._responseType = responseType;
            this._code = code;
        }
        Object.defineProperty(UploadResponse.prototype, "responseType", {
            /**
             * Getter responseType
             * return {string}
             */
            get: function () {
                return this._responseType;
            },
            /**
             * Setter responseType
             * param {string} value
             */
            set: function (value) {
                this._responseType = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(UploadResponse.prototype, "message", {
            /**
             * Getter message
             * return {string}
             */
            get: function () {
                return this._message;
            },
            /**
             * Setter message
             * param {string} value
             */
            set: function (value) {
                this._message = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(UploadResponse.prototype, "code", {
            /**
             * Getter code
             * return {string}
             */
            get: function () {
                return this._code;
            },
            /**
             * Setter code
             * param {string} value
             */
            set: function (value) {
                this._code = value;
            },
            enumerable: false,
            configurable: true
        });
        return UploadResponse;
    }());

    function UploadFileComponent_div_0_div_1_Template(rf, ctx) {
        if (rf & 1) {
            var _r6_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 6);
            i0.ɵɵelementStart(1, "image-cropper", 7);
            i0.ɵɵlistener("imageCropped", function UploadFileComponent_div_0_div_1_Template_image_cropper_imageCropped_1_listener($event) { i0.ɵɵrestoreView(_r6_1); var ctx_r5 = i0.ɵɵnextContext(2); return ctx_r5.imageCropped($event); })("imageLoaded", function UploadFileComponent_div_0_div_1_Template_image_cropper_imageLoaded_1_listener() { i0.ɵɵrestoreView(_r6_1); var ctx_r7 = i0.ɵɵnextContext(2); return ctx_r7.imageLoaded(); })("loadImageFailed", function UploadFileComponent_div_0_div_1_Template_image_cropper_loadImageFailed_1_listener() { i0.ɵɵrestoreView(_r6_1); var ctx_r8 = i0.ɵɵnextContext(2); return ctx_r8.loadImageFailed(); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(2, "div");
            i0.ɵɵelementStart(3, "button", 8);
            i0.ɵɵlistener("click", function UploadFileComponent_div_0_div_1_Template_button_click_3_listener() { i0.ɵɵrestoreView(_r6_1); var ctx_r9 = i0.ɵɵnextContext(2); return ctx_r9.uploadCroppedImage(); });
            i0.ɵɵtext(4);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(5, "button", 9);
            i0.ɵɵlistener("click", function UploadFileComponent_div_0_div_1_Template_button_click_5_listener() { i0.ɵɵrestoreView(_r6_1); var ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.clearUploadCroppedImage(); });
            i0.ɵɵtext(6);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r2 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("maintainAspectRatio", true)("imageQuality", 100)("imageChangedEvent", ctx_r2.imageChangedEvent)("autoCrop", true)("maintainAspectRatio", true)("aspectRatio", ctx_r2.getRatio());
            i0.ɵɵadvance(3);
            i0.ɵɵtextInterpolate1(" ", ctx_r2.uploadlanguageObj.crop_image_yes, " ");
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate1(" ", ctx_r2.uploadlanguageObj.crop_image_cancel, " ");
        }
    }
    function UploadFileComponent_div_0_div_2_Template(rf, ctx) {
        if (rf & 1) {
            var _r13_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 10);
            i0.ɵɵelementStart(1, "div", 11);
            i0.ɵɵelementStart(2, "div", 12);
            i0.ɵɵelementStart(3, "img", 13, 14);
            i0.ɵɵlistener("error", function UploadFileComponent_div_0_div_2_Template_img_error_3_listener() { i0.ɵɵrestoreView(_r13_1); var _r11 = i0.ɵɵreference(4); var ctx_r12 = i0.ɵɵnextContext(2); _r11.error = null; return _r11.src = ctx_r12.uploadOption.placeHolderImg; });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r3 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("src", ctx_r3.uploadOption.downloadFileApi + ctx_r3.fileName, i0.ɵɵsanitizeUrl);
        }
    }
    function UploadFileComponent_div_0_div_3_div_2_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 22);
            i0.ɵɵelement(1, "i", 23);
            i0.ɵɵelementEnd();
        }
    }
    function UploadFileComponent_div_0_div_3_Template(rf, ctx) {
        if (rf & 1) {
            var _r16_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 15);
            i0.ɵɵelementStart(1, "label", 16);
            i0.ɵɵtemplate(2, UploadFileComponent_div_0_div_3_div_2_Template, 2, 0, "div", 17);
            i0.ɵɵelementStart(3, "div", 18);
            i0.ɵɵtext(4);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(5, "div", 19);
            i0.ɵɵtext(6);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(7, "br");
            i0.ɵɵelementStart(8, "span", 20);
            i0.ɵɵtext(9);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(10, "br");
            i0.ɵɵelementStart(11, "input", 21);
            i0.ɵɵlistener("change", function UploadFileComponent_div_0_div_3_Template_input_change_11_listener($event) { i0.ɵɵrestoreView(_r16_1); var ctx_r15 = i0.ɵɵnextContext(2); return ctx_r15.fileChangeEvent($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r4 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", !ctx_r4.fileName);
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate1("", ctx_r4.uploadlanguageObj.upload_file_drag_and_drop, " ");
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx_r4.uploadlanguageObj.upload_file_or);
            i0.ɵɵadvance(3);
            i0.ɵɵtextInterpolate1(" ", ctx_r4.uploadlanguageObj.upload_file_browse_file, " ");
            i0.ɵɵadvance(2);
            i0.ɵɵpropertyInterpolate("accept", ctx_r4.options.allowedContentTypes);
        }
    }
    function UploadFileComponent_div_0_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 2);
            i0.ɵɵtemplate(1, UploadFileComponent_div_0_div_1_Template, 7, 8, "div", 3);
            i0.ɵɵtemplate(2, UploadFileComponent_div_0_div_2_Template, 5, 1, "div", 4);
            i0.ɵɵtemplate(3, UploadFileComponent_div_0_div_3_Template, 12, 5, "div", 5);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r0 = i0.ɵɵnextContext();
            i0.ɵɵclassMapInterpolate1("ratio-", ctx_r0.ratio, " cropper-upload");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r0.imageChangedEvent && ctx_r0.options);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r0.imageChangedEvent && ctx_r0.fileName);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r0.imageChangedEvent && ctx_r0.options);
        }
    }
    function UploadFileComponent_div_1_div_1_div_1_div_2_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 29);
            i0.ɵɵelement(1, "ngb-progressbar", 30);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var f_r22 = i0.ɵɵnextContext().$implicit;
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("value", f_r22 == null ? null : f_r22.progress == null ? null : f_r22.progress.data == null ? null : f_r22.progress.data.percentage);
        }
    }
    function UploadFileComponent_div_1_div_1_div_1_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 10);
            i0.ɵɵelementStart(1, "div", 11);
            i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_1_div_1_div_2_Template, 2, 1, "div", 28);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r20 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", !ctx_r20.isUploadFilesDone());
        }
    }
    function UploadFileComponent_div_1_div_1_div_2_img_3_Template(rf, ctx) {
        if (rf & 1) {
            var _r29_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "img", 13, 32);
            i0.ɵɵlistener("error", function UploadFileComponent_div_1_div_1_div_2_img_3_Template_img_error_0_listener() { i0.ɵɵrestoreView(_r29_1); var _r27 = i0.ɵɵreference(1); var ctx_r28 = i0.ɵɵnextContext(4); _r27.error = null; return _r27.src = ctx_r28.uploadOption.placeHolderImg; });
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r26 = i0.ɵɵnextContext(4);
            i0.ɵɵproperty("src", ctx_r26.uploadOption.downloadFileApi + ctx_r26.getImgUrl(), i0.ɵɵsanitizeUrl);
        }
    }
    function UploadFileComponent_div_1_div_1_div_2_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 10);
            i0.ɵɵelementStart(1, "div", 11);
            i0.ɵɵelementStart(2, "div", 12);
            i0.ɵɵtemplate(3, UploadFileComponent_div_1_div_1_div_2_img_3_Template, 2, 1, "img", 31);
            i0.ɵɵelementEnd();
            i0.ɵɵtext(4);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r21 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("ngIf", !ctx_r21.hidePreview);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r21.fileId, " ");
        }
    }
    function UploadFileComponent_div_1_div_1_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div");
            i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_1_div_1_Template, 3, 1, "div", 27);
            i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_1_div_2_Template, 5, 2, "div", 4);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r17 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", ctx_r17.files);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r17.multiple && (ctx_r17.fileId || ctx_r17.fileName));
        }
    }
    function UploadFileComponent_div_1_div_2_span_1_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 18);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r30 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1("", ctx_r30.uploadlanguageObj.upload_file_drag_and_drop, " ");
        }
    }
    function UploadFileComponent_div_1_div_2_span_2_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 19);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r31 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(ctx_r31.uploadlanguageObj.upload_file_or);
        }
    }
    function UploadFileComponent_div_1_div_2_span_5_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 38);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r32 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r32.uploadlanguageObj.upload_file_browse_files, " ");
        }
    }
    function UploadFileComponent_div_1_div_2_span_6_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 38);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r33 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r33.uploadlanguageObj.upload_file_change, " ");
        }
    }
    function UploadFileComponent_div_1_div_2_Template(rf, ctx) {
        if (rf & 1) {
            var _r35_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div");
            i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_2_span_1_Template, 2, 1, "span", 33);
            i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_2_span_2_Template, 2, 1, "span", 34);
            i0.ɵɵelementStart(3, "label", 35);
            i0.ɵɵelementStart(4, "input", 36);
            i0.ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_div_2_Template_input_uploadOutput_4_listener($event) { i0.ɵɵrestoreView(_r35_1); var ctx_r34 = i0.ɵɵnextContext(2); return ctx_r34.onUploadOutput($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(5, UploadFileComponent_div_1_div_2_span_5_Template, 2, 1, "span", 37);
            i0.ɵɵtemplate(6, UploadFileComponent_div_1_div_2_span_6_Template, 2, 1, "span", 37);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r18 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r18.isUploadFilesDone());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r18.isUploadFilesDone());
            i0.ɵɵadvance(2);
            i0.ɵɵpropertyInterpolate("accept", ctx_r18.options.allowedContentTypes);
            i0.ɵɵproperty("options", ctx_r18.options)("uploadInput", ctx_r18.uploadInput);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r18.isUploadFilesDone());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r18.isUploadFilesDone());
        }
    }
    function UploadFileComponent_div_1_div_3_div_1_span_1_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 18);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r38 = i0.ɵɵnextContext(4);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1("", ctx_r38.uploadlanguageObj.upload_file_drag_and_drop, " ");
        }
    }
    function UploadFileComponent_div_1_div_3_div_1_span_2_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 19);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r39 = i0.ɵɵnextContext(4);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(ctx_r39.uploadlanguageObj.upload_file_or);
        }
    }
    function UploadFileComponent_div_1_div_3_div_1_span_5_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 38);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r40 = i0.ɵɵnextContext(4);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r40.uploadlanguageObj.upload_file_browse_file, " ");
        }
    }
    function UploadFileComponent_div_1_div_3_div_1_span_6_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "span", 38);
            i0.ɵɵtext(1);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r41 = i0.ɵɵnextContext(4);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r41.uploadlanguageObj.upload_file_change, " ");
        }
    }
    function UploadFileComponent_div_1_div_3_div_1_Template(rf, ctx) {
        if (rf & 1) {
            var _r43_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div");
            i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_3_div_1_span_1_Template, 2, 1, "span", 33);
            i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_3_div_1_span_2_Template, 2, 1, "span", 34);
            i0.ɵɵelementStart(3, "label", 35);
            i0.ɵɵelementStart(4, "input", 40);
            i0.ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_div_3_div_1_Template_input_uploadOutput_4_listener($event) { i0.ɵɵrestoreView(_r43_1); var ctx_r42 = i0.ɵɵnextContext(3); return ctx_r42.onUploadOutput($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(5, UploadFileComponent_div_1_div_3_div_1_span_5_Template, 2, 1, "span", 37);
            i0.ɵɵtemplate(6, UploadFileComponent_div_1_div_3_div_1_span_6_Template, 2, 1, "span", 37);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r36 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r36.isUploadFilesDone());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r36.isUploadFilesDone());
            i0.ɵɵadvance(2);
            i0.ɵɵpropertyInterpolate("accept", ctx_r36.options.allowedContentTypes);
            i0.ɵɵproperty("options", ctx_r36.options)("uploadInput", ctx_r36.uploadInput);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r36.isUploadFilesDone());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r36.isUploadFilesDone());
        }
    }
    function UploadFileComponent_div_1_div_3_div_2_Template(rf, ctx) {
        if (rf & 1) {
            var _r45_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 15);
            i0.ɵɵelementStart(1, "label", 16);
            i0.ɵɵelementStart(2, "span", 20);
            i0.ɵɵtext(3);
            i0.ɵɵelementEnd();
            i0.ɵɵtext(4);
            i0.ɵɵelementStart(5, "input", 40);
            i0.ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_div_3_div_2_Template_input_uploadOutput_5_listener($event) { i0.ɵɵrestoreView(_r45_1); var ctx_r44 = i0.ɵɵnextContext(3); return ctx_r44.onUploadOutput($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r37 = i0.ɵɵnextContext(3);
            i0.ɵɵadvance(3);
            i0.ɵɵtextInterpolate1(" ", ctx_r37.uploadlanguageObj.upload_file_browse_file, " ");
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate1(" ", ctx_r37.fileId, " ");
            i0.ɵɵadvance(1);
            i0.ɵɵpropertyInterpolate("accept", ctx_r37.options.allowedContentTypes);
            i0.ɵɵproperty("options", ctx_r37.options)("uploadInput", ctx_r37.uploadInput);
        }
    }
    function UploadFileComponent_div_1_div_3_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 39);
            i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_3_div_1_Template, 7, 7, "div", 25);
            i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_3_div_2_Template, 6, 5, "div", 5);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r19 = i0.ɵɵnextContext(2);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r19.uploadType === "IMAGE");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r19.uploadType === "ALL");
        }
    }
    var _c0$3 = function (a0) { return { "is-drop-over": a0 }; };
    function UploadFileComponent_div_1_Template(rf, ctx) {
        if (rf & 1) {
            var _r47_1 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 24);
            i0.ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_Template_div_uploadOutput_0_listener($event) { i0.ɵɵrestoreView(_r47_1); var ctx_r46 = i0.ɵɵnextContext(); return ctx_r46.onUploadOutput($event); });
            i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_1_Template, 3, 2, "div", 25);
            i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_2_Template, 7, 7, "div", 25);
            i0.ɵɵtemplate(3, UploadFileComponent_div_1_div_3_Template, 3, 2, "div", 26);
            i0.ɵɵelementEnd();
        }
        if (rf & 2) {
            var ctx_r1 = i0.ɵɵnextContext();
            i0.ɵɵproperty("options", ctx_r1.options)("uploadInput", ctx_r1.uploadInput)("ngClass", i0.ɵɵpureFunction1(6, _c0$3, ctx_r1.dragOver));
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r1.uploadType === "IMAGE");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx_r1.multiple);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx_r1.multiple);
        }
    }
    var UploadFileComponent = /** @class */ (function () {
        function UploadFileComponent(http) {
            //super();
            this.http = http;
            this.uploadQueue = new Array();
            this.totalDoneUploaded = 0;
            //crop img
            this.imageChangedEvent = '';
            this.originalImage = '';
            this.uploadRatioConfig = uploadRatio;
            this.multiple = false;
            this.hidePreview = false;
            this.uploadType = uploadType.IMAGE;
            this.fileSize = 1024 * 1024 * 10; //5MB
            this.uploadlanguageObj = UPLOAD_FILE_LANGUAGE_CONFIG;
            this.response = new i0.EventEmitter();
            this.uploadFileEVent = new i0.EventEmitter();
            this.files = [];
            this.uploadInput = new i0.EventEmitter();
            this.humanizeBytes = i4.humanizeBytes;
            this.errorMessage = "";
        }
        UploadFileComponent.prototype.ngAfterViewInit = function () {
            var _this = this;
            setTimeout(function () {
                _this.options = {
                    concurrency: 1,
                    maxUploads: 9999,
                    allowedContentTypes: _this.uploadOption.acceptFileType.length ? _this.uploadOption.acceptFileType : allowedContentTypesUploadImg
                };
            }, 0);
        };
        UploadFileComponent.prototype.isValid = function (obj) {
            return obj !== null && obj !== undefined;
        };
        UploadFileComponent.prototype.parseErrorResponse = function (response) {
            var errors = new Array();
            if (this.isValid(response.error) && this.isValid(response.error.error) && response.error.error instanceof Array) {
                var arr = response.error.error;
                for (var i = 0; i < arr.length; i++) {
                    errors.push(new ErrorObj(arr[i].field, arr[i].message));
                }
            }
            else if (response.status !== 401) {
                errors.push(new ErrorObj(null, "serverError"));
            }
            return errors;
        };
        UploadFileComponent.prototype.getRatio = function () {
            return uploadRatio[this.ratio].ratio;
        };
        UploadFileComponent.prototype.getImgUrl = function () {
            return this.fileName ? this.fileName + "&" + new Date().getTime() : undefined;
        };
        UploadFileComponent.prototype.onUploadOutput = function (output) {
            var _this = this;
            if (output.type === "allAddedToQueue" && this.uploadQueue.length) {
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.START_UPLOAD));
                this.uploadQueue.forEach(function (element, index) {
                    _this.startUpload(element);
                });
            }
            else if (output.type === "addedToQueue" && typeof output.file !== "undefined") {
                if (output.file.size > this.fileSize) {
                    this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
                }
                else {
                    if (this.multiple) {
                        this.files.push(output.file);
                    }
                    else {
                        this.files = [output.file];
                    }
                    this.uploadQueue.push(output.file);
                }
            }
            else if (output.type === "uploading" && typeof output.file !== "undefined") {
                if (this.multiple) {
                    var index = this.files.findIndex(function (file) { return typeof output.file !== "undefined" && file.id === output.file.id; });
                    this.files[index] = output.file;
                }
            }
            else if (output.type === "removed") {
                if (this.multiple) {
                    this.files = this.files.filter(function (file) { return file !== output.file; });
                }
                else {
                    this.files = [];
                }
            }
            else if (output.type === "dragOver") {
                this.dragOver = true;
            }
            else if (output.type === "dragOut") {
                this.dragOver = false;
            }
            else if (output.type === "drop") {
                this.dragOver = false;
            }
            else if (output.type === "done") {
                this.checkResponse(output);
            }
            else if (output.type === "rejected") {
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.type"));
            }
        };
        UploadFileComponent.prototype.checkResponse = function (output) {
            this.totalDoneUploaded = this.totalDoneUploaded + 1;
            if (this.isValid(output.file.response.error) && output.file.response.error instanceof Array && output.file.responseStatus !== 200) {
                var arr = output.file.response.error;
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, arr[0].code, output.file.responseStatus));
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
                this.uploadQueue = [];
            }
            else if (output.file.response.result || output.file.responseStatus === 200) {
                if (!this.multiple) {
                    this.uploadQueue = [];
                    this.fileId = output.file.response.result;
                    this.fileName = output.file.response.result;
                    this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, output.file.response.result, output.file.responseStatus));
                    this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
                }
                else if (this.totalDoneUploaded === this.uploadQueue.length) {
                    this.uploadQueue = [];
                    this.totalDoneUploaded = 0;
                    this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
                }
            }
            else {
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error", output.file.responseStatus));
                this.files = [];
                this.uploadQueue = [];
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
            }
        };
        UploadFileComponent.prototype.isUploadFilesDone = function () {
            for (var i = 0; i < this.files.length; i++) {
                return this.files[i].progress.data.percentage === 100;
            }
            return false;
        };
        UploadFileComponent.prototype.getFilesUploaded = function () {
            return this.files;
        };
        UploadFileComponent.prototype.startUpload = function (file) {
            this.fileId = "";
            var event = {
                type: "uploadFile",
                url: this.uploadOption.uploadApi.replace(":fileType", this.uploadType),
                method: "POST",
                file: file,
                headers: {
                    language: this.uploadOption.language,
                    Authorization: this.uploadOption.authorization
                }
            };
            this.uploadInput.emit(event);
        };
        UploadFileComponent.prototype.cancelUpload = function (id) {
            this.uploadInput.emit({ type: "cancel", id: id });
        };
        UploadFileComponent.prototype.removeFile = function (id) {
            this.uploadInput.emit({ type: "remove", id: id });
        };
        UploadFileComponent.prototype.removeAllFiles = function () {
            this.uploadInput.emit({ type: "removeAll" });
        };
        // IMAGE CROPPER
        UploadFileComponent.prototype.fileChangeEvent = function (event) {
            if (event.srcElement.files[0].size > this.fileSize) {
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
            }
            else {
                this.imageChangedEvent = event;
                //this.originalImage = event.srcElement.files;
            }
        };
        UploadFileComponent.prototype.imageCropped = function (event) {
            //this.croppedImage = event.base64;
            this.cropImgObject = event;
            // crop image again if current cropped image's width is greater than resizeToWidth config
            if (event.width > uploadRatio[this.ratio].resizeToWidth) {
                this.imageCropper.resizeToWidth = uploadRatio[this.ratio].resizeToWidth;
                this.imageCropper.crop();
                this.imageCropper.resizeToWidth = 0;
            }
        };
        UploadFileComponent.prototype.imageLoaded = function () {
            // show cropper
        };
        UploadFileComponent.prototype.loadImageFailed = function () {
            // show message
        };
        UploadFileComponent.prototype.clearUploadCroppedImage = function () {
            //this.originalImage = new Array();
            this.cropImgObject = undefined;
            this.imageChangedEvent = undefined;
        };
        UploadFileComponent.prototype.uploadCroppedImage = function () {
            var _this = this;
            //upload cropped image
            if (this.cropImgObject) {
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.START_UPLOAD));
                var formData = new FormData();
                formData.append('file', this.cropImgObject.file);
                var headers = new i1$3.HttpHeaders();
                headers.append('language', this.uploadOption.language);
                headers.append('Authorization', this.uploadOption.authorization);
                this.http.post(this.uploadOption.uploadApi.replace(":fileType", this.uploadType), formData, { headers: headers }).subscribe(function (result) {
                    var res = result;
                    _this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, res.result));
                    _this.clearUploadCroppedImage();
                    _this.fileName = result.message;
                    _this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
                }, function (errorRes) {
                    var errors = _this.parseErrorResponse(errorRes);
                    if (errors.length > 0 && errors[0].message.trim().length > 0) {
                        _this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, errors[0].message.trim()));
                    }
                    _this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
                });
            }
        };
        return UploadFileComponent;
    }());
    UploadFileComponent.ɵfac = function UploadFileComponent_Factory(t) { return new (t || UploadFileComponent)(i0.ɵɵdirectiveInject(i1$3.HttpClient)); };
    UploadFileComponent.ɵcmp = i0.ɵɵdefineComponent({ type: UploadFileComponent, selectors: [["via-upload-file"]], viewQuery: function UploadFileComponent_Query(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵviewQuery(i3$1.ImageCropperComponent, true);
            }
            if (rf & 2) {
                var _t;
                i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.imageCropper = _t.first);
            }
        }, inputs: { multiple: "multiple", hidePreview: "hidePreview", uploadType: "uploadType", fileName: "fileName", fileSize: "fileSize", uploadOption: "uploadOption", uploadlanguageObj: "uploadlanguageObj", ratio: "ratio" }, outputs: { response: "response", uploadFileEVent: "uploadFileEVent" }, decls: 2, vars: 2, consts: [["id", "upload-file", 3, "class", 4, "ngIf"], ["id", "upload-file", "class", "drop-container", "ngFileDrop", "", 3, "options", "uploadInput", "ngClass", "uploadOutput", 4, "ngIf"], ["id", "upload-file"], ["class", "img-cropper-wrapper", "style", "width: 100%;", 4, "ngIf"], ["class", "upload-item", 4, "ngIf"], ["class", "upload-file-container", 4, "ngIf"], [1, "img-cropper-wrapper", 2, "width", "100%"], ["outputType", "file", 3, "maintainAspectRatio", "imageQuality", "imageChangedEvent", "autoCrop", "aspectRatio", "imageCropped", "imageLoaded", "loadImageFailed"], ["type", "button", 1, "btn", "uppercase-text", "btn-primary", "modal-action-btn", 3, "click"], ["type", "button", 1, "btn", "uppercase-text", "btn-light", "modal-action-btn", 3, "click"], [1, "upload-item"], [1, "upload-item-content"], [1, "image-uploaded"], [3, "src", "error"], ["img", ""], [1, "upload-file-container"], [1, "upload-button-file"], ["id", "upload-icon", 4, "ngIf"], [1, "message-drag-and-drop"], [1, "message-or"], [1, "btn-button-file"], ["onclick", "this.value=null", "type", "file", 1, "input-upload", 3, "accept", "change"], ["id", "upload-icon"], ["aria-hidden", "true", 1, "fa", "fa-cloud-upload", "fa-5x"], ["id", "upload-file", "ngFileDrop", "", 1, "drop-container", 3, "options", "uploadInput", "ngClass", "uploadOutput"], [4, "ngIf"], ["style", "width: 100%;", 4, "ngIf"], ["class", "upload-item", 4, "ngFor", "ngForOf"], ["class", "progress-content", 4, "ngIf"], [1, "progress-content"], ["showValue", "true", "type", "success", 3, "value"], [3, "src", "error", 4, "ngIf"], ["image", ""], ["class", "message-drag-and-drop", 4, "ngIf"], ["class", "message-or", 4, "ngIf"], [1, "upload-button"], ["onclick", "this.value=null", "type", "file", "ngFileSelect", "", "multiple", "", 1, "input-upload", 3, "accept", "options", "uploadInput", "uploadOutput"], ["class", "btn-button", 4, "ngIf"], [1, "btn-button"], [2, "width", "100%"], ["onclick", "this.value=null", "type", "file", "ngFileSelect", "", 1, "input-upload", 3, "accept", "options", "uploadInput", "uploadOutput"]], template: function UploadFileComponent_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵtemplate(0, UploadFileComponent_div_0_Template, 4, 6, "div", 0);
                i0.ɵɵtemplate(1, UploadFileComponent_div_1_Template, 4, 8, "div", 1);
            }
            if (rf & 2) {
                i0.ɵɵproperty("ngIf", ctx.ratio);
                i0.ɵɵadvance(1);
                i0.ɵɵproperty("ngIf", ctx.options && !ctx.ratio);
            }
        }, directives: [i2.NgIf, i3$1.ImageCropperComponent, i4.NgFileDropDirective, i2.NgClass, i2.NgForOf, i1.NgbProgressbar, i4.NgFileSelectDirective], styles: ["upload-file[_ngcontent-%COMP%]{width:100%}#upload-file[_ngcontent-%COMP%]{align-items:center;border:2px dashed #c4cdd9;border-radius:3px;display:flex;flex-direction:column;overflow:hidden;text-align:center;width:100%}#upload-file[_ngcontent-%COMP%]   .drop-container[_ngcontent-%COMP%]{padding:30px 10px}#upload-file[_ngcontent-%COMP%]   .message-drag-and-drop[_ngcontent-%COMP%]{font-weight:700}#upload-file[_ngcontent-%COMP%]   .message-or[_ngcontent-%COMP%]{color:#999;display:block;margin-top:20px}#upload-file[_ngcontent-%COMP%]   .upload-button[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:column;width:100%}#upload-file[_ngcontent-%COMP%]   .upload-button[_ngcontent-%COMP%]   .input-upload[_ngcontent-%COMP%]{height:0;visibility:hidden;width:0}#upload-file[_ngcontent-%COMP%]   .upload-button[_ngcontent-%COMP%]   .btn-button[_ngcontent-%COMP%]{background:#9a0000;border-radius:.25rem;color:#fff;cursor:pointer;font-size:12px;font-weight:700;max-width:200px;padding:10px}#upload-file[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]{display:flex;display:inline-block;flex-direction:column;margin:10px;text-align:center;width:100%}#upload-file[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]   .input-upload[_ngcontent-%COMP%]{height:0;visibility:hidden;width:0}#upload-file[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]   .btn-button-file[_ngcontent-%COMP%]{background:#9a0000;border-radius:.25rem;color:#fff;cursor:pointer;font-size:12px;font-weight:700;max-width:200px;padding:10px}#upload-file[_ngcontent-%COMP%]   .upload-file-container[_ngcontent-%COMP%]{padding-bottom:10px}#upload-file[_ngcontent-%COMP%]   .message-error[_ngcontent-%COMP%]{margin-bottom:5px;text-align:left;width:80%}#upload-file[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]{margin:20px auto;width:100%}#upload-file[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]   .upload-item-content[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]{padding:15px;text-align:center}#upload-file[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]   .upload-item-content[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:100%}#upload-file[_ngcontent-%COMP%]   .multiple-upload-thumbnail[_ngcontent-%COMP%]{display:inline-block;width:100px}#upload-file.cropper-upload[_ngcontent-%COMP%]{min-height:250px}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]{margin-bottom:0!important}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]{margin:0;margin-bottom:0!important}#upload-file.cropper-upload[_ngcontent-%COMP%]   .message-or[_ngcontent-%COMP%]{margin-top:0!important}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]{margin-top:0}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-file-container[_ngcontent-%COMP%]{padding:0}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-file-container[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]{margin-bottom:10px}#upload-file.cropper-upload[_ngcontent-%COMP%]   #upload-icon[_ngcontent-%COMP%]{color:hsla(0,0%,50.2%,.1803921568627451);margin-top:100px}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]{max-height:515px;width:100%}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]{height:100%;padding-bottom:10px}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]   image-cropper[_ngcontent-%COMP%]{max-height:440px}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-height:390px;max-width:100%;width:auto!important}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]{max-height:730px;width:100%}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]{height:100%;padding-bottom:10px}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]   image-cropper[_ngcontent-%COMP%]{max-height:550px}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-height:600px;max-width:100%;width:auto!important}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]{max-height:700px;width:100%}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]{height:100%;padding-bottom:10px}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]   image-cropper[_ngcontent-%COMP%]{max-height:650px}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-height:550px;max-width:100%;width:auto!important}"] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(UploadFileComponent, [{
                type: i0.Component,
                args: [{
                        selector: "via-upload-file",
                        templateUrl: "./upload.file.html",
                        styleUrls: ["./upload.file.scss"]
                    }]
            }], function () { return [{ type: i1$3.HttpClient }]; }, { imageCropper: [{
                    type: i0.ViewChild,
                    args: [i3$1.ImageCropperComponent]
                }], multiple: [{
                    type: i0.Input
                }], hidePreview: [{
                    type: i0.Input
                }], uploadType: [{
                    type: i0.Input
                }], fileName: [{
                    type: i0.Input
                }], fileSize: [{
                    type: i0.Input
                }], uploadOption: [{
                    type: i0.Input
                }], uploadlanguageObj: [{
                    type: i0.Input
                }], ratio: [{
                    type: i0.Input
                }], response: [{
                    type: i0.Output
                }], uploadFileEVent: [{
                    type: i0.Output
                }] });
    })();

    var UploadFileModule = /** @class */ (function () {
        function UploadFileModule() {
        }
        return UploadFileModule;
    }());
    UploadFileModule.ɵmod = i0.ɵɵdefineNgModule({ type: UploadFileModule });
    UploadFileModule.ɵinj = i0.ɵɵdefineInjector({ factory: function UploadFileModule_Factory(t) { return new (t || UploadFileModule)(); }, imports: [[
                i1$3.HttpClientModule,
                i1.NgbModule,
                i2.CommonModule,
                i3.FormsModule,
                i3.ReactiveFormsModule,
                i3$1.ImageCropperModule,
                i4.NgxUploaderModule
            ]] });
    (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(UploadFileModule, { declarations: [UploadFileComponent], imports: [i1$3.HttpClientModule,
                i1.NgbModule,
                i2.CommonModule,
                i3.FormsModule,
                i3.ReactiveFormsModule,
                i3$1.ImageCropperModule,
                i4.NgxUploaderModule], exports: [UploadFileComponent] });
    })();
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(UploadFileModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [UploadFileComponent],
                        imports: [
                            i1$3.HttpClientModule,
                            i1.NgbModule,
                            i2.CommonModule,
                            i3.FormsModule,
                            i3.ReactiveFormsModule,
                            i3$1.ImageCropperModule,
                            i4.NgxUploaderModule
                        ],
                        exports: [UploadFileComponent]
                    }]
            }], null, null);
    })();

    var UploadOption = /** @class */ (function () {
        function UploadOption(acceptFileType, uploadApi, downloadFileApi, language, authorization, placeHolderImg) {
            if (acceptFileType === void 0) { acceptFileType = []; }
            this._acceptFileType = acceptFileType;
            this._uploadApi = uploadApi;
            this._language = language;
            this._authorization = authorization;
            this._downloadFileApi = downloadFileApi;
            this._placeHolderImg = placeHolderImg;
        }
        Object.defineProperty(UploadOption.prototype, "acceptFileType", {
            /**
             * Getter acceptFileType
             * return {string[]}
             */
            get: function () {
                return this._acceptFileType;
            },
            /**
             * Setter acceptFileType
             * param {string[]} value
             */
            set: function (value) {
                this._acceptFileType = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(UploadOption.prototype, "uploadApi", {
            /**
             * Getter uploadApi
             * return {string}
             */
            get: function () {
                return this._uploadApi;
            },
            /**
             * Setter uploadApi
             * param {string} value
             */
            set: function (value) {
                this._uploadApi = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(UploadOption.prototype, "downloadFileApi", {
            /**
             * Getter downloadFileApi
             * return {string}
             */
            get: function () {
                return this._downloadFileApi;
            },
            /**
             * Setter downloadFileApi
             * param {string} value
             */
            set: function (value) {
                this._downloadFileApi = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(UploadOption.prototype, "language", {
            /**
             * Getter language
             * return {string}
             */
            get: function () {
                return this._language;
            },
            /**
             * Setter language
             * param {string} value
             */
            set: function (value) {
                this._language = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(UploadOption.prototype, "authorization", {
            /**
             * Getter authorization
             * return {string}
             */
            get: function () {
                return this._authorization;
            },
            /**
             * Setter authorization
             * param {string} value
             */
            set: function (value) {
                this._authorization = value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(UploadOption.prototype, "placeHolderImg", {
            /**
             * Getter placeHolderImg
             * return {string}
             */
            get: function () {
                return this._placeHolderImg;
            },
            /**
             * Setter placeHolderImg
             * param {string} value
             */
            set: function (value) {
                this._placeHolderImg = value;
            },
            enumerable: false,
            configurable: true
        });
        return UploadOption;
    }());

    var GoogleAnalyticsService = /** @class */ (function () {
        //************************
        // Import this to app.component.ts
        // subscribe to router events and send page views to Google Analytics
        // this.router.events.subscribe(event => {
        //     GoogleAnalyticsService.subscribeRouterEvents(event);
        //   });
        //************************
        function GoogleAnalyticsService() {
        }
        // /**
        //   * Emit google analytics event
        //   * Fire event example:
        //   * this.emitEvent("testCategory", "testAction", "testLabel", 10);
        //   * param {string} eventCategory
        //   * param {string} eventAction
        //   * param {string} eventLabel
        //   * param {number} eventValue
        //   */
        GoogleAnalyticsService.prototype.eventEmitter = function (eventCategory, eventAction, eventLabel, eventValue) {
            if (eventLabel === void 0) { eventLabel = null; }
            if (eventValue === void 0) { eventValue = null; }
            ga('send', 'event', {
                eventCategory: eventCategory,
                eventLabel: eventLabel,
                eventAction: eventAction,
                eventValue: eventValue
            });
        };
        GoogleAnalyticsService.prototype.subscribeRouterEvents = function (event) {
            if (event instanceof i1$2.NavigationEnd) {
                ga('set', 'page', event.urlAfterRedirects);
                ga('send', 'pageview');
            }
        };
        return GoogleAnalyticsService;
    }());
    GoogleAnalyticsService.ɵfac = function GoogleAnalyticsService_Factory(t) { return new (t || GoogleAnalyticsService)(); };
    GoogleAnalyticsService.ɵprov = i0.ɵɵdefineInjectable({ token: GoogleAnalyticsService, factory: GoogleAnalyticsService.ɵfac });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(GoogleAnalyticsService, [{
                type: i0.Injectable
            }], function () { return []; }, null);
    })();

    var GoogleAnalyticsServiceModule = /** @class */ (function () {
        function GoogleAnalyticsServiceModule() {
        }
        return GoogleAnalyticsServiceModule;
    }());
    GoogleAnalyticsServiceModule.ɵmod = i0.ɵɵdefineNgModule({ type: GoogleAnalyticsServiceModule });
    GoogleAnalyticsServiceModule.ɵinj = i0.ɵɵdefineInjector({ factory: function GoogleAnalyticsServiceModule_Factory(t) { return new (t || GoogleAnalyticsServiceModule)(); }, providers: [GoogleAnalyticsService], imports: [[]] });
    /*@__PURE__*/ (function () {
        i0.ɵsetClassMetadata(GoogleAnalyticsServiceModule, [{
                type: i0.NgModule,
                args: [{
                        imports: [],
                        providers: [GoogleAnalyticsService],
                        exports: []
                    }]
            }], null, null);
    })();

    /*
     * Public API Surface of viatech-angular-common
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.ACTION_TYPE = ACTION_TYPE;
    exports.ButtonAction = ButtonAction;
    exports.CELL_TYPE = CELL_TYPE;
    exports.ClickOutSiteDirective = ClickOutSiteDirective;
    exports.ClickOutSiteModule = ClickOutSiteModule;
    exports.CustomAdapter = CustomAdapter;
    exports.CustomDateParserFormatter = CustomDateParserFormatter;
    exports.CustomDatepickerI18n = CustomDatepickerI18n;
    exports.CustomToast = CustomToast;
    exports.DATE_FORMAT = DATE_FORMAT$1;
    exports.DATE_FORMAT_WITHOUT_TIME = DATE_FORMAT_WITHOUT_TIME$1;
    exports.DatepickerComponent = DatepickerComponent;
    exports.DatepickerModule = DatepickerModule;
    exports.DropdownComponent = DropdownComponent;
    exports.DropdownItem = DropdownItem;
    exports.DropdownModule = DropdownModule;
    exports.EntryPerpage = EntryPerpage;
    exports.ErrorObj = ErrorObj;
    exports.FilterPipe = FilterPipe;
    exports.FullTextSearchPipe = FullTextSearchPipe;
    exports.GoogleAnalyticsService = GoogleAnalyticsService;
    exports.GoogleAnalyticsServiceModule = GoogleAnalyticsServiceModule;
    exports.I18n = I18n;
    exports.ImageSphereComponent = ImageSphereComponent;
    exports.ImageSphereModule = ImageSphereModule;
    exports.Map = Map;
    exports.MapComponent = MapComponent;
    exports.MapModule = MapModule;
    exports.Modal = Modal;
    exports.SORT = SORT;
    exports.TABLE_LANGUAGE_CONFIG = TABLE_LANGUAGE_CONFIG;
    exports.TableAction = TableAction;
    exports.TableCell = TableCell;
    exports.TableColumn = TableColumn;
    exports.TableComponent = TableComponent;
    exports.TableDefaultSort = TableDefaultSort;
    exports.TableFilter = TableFilter;
    exports.TableFilterDropdownItem = TableFilterDropdownItem;
    exports.TableModule = TableModule;
    exports.TableOption = TableOption;
    exports.TableRow = TableRow;
    exports.TableRowValue = TableRowValue;
    exports.TimepickerComponent = TimepickerComponent;
    exports.TimepickerModule = TimepickerModule;
    exports.ToastModule = ToastModule;
    exports.TypeaheadComponent = TypeaheadComponent;
    exports.TypeaheadItem = TypeaheadItem;
    exports.TypeaheadItemValue = TypeaheadItemValue;
    exports.TypeaheadModule = TypeaheadModule;
    exports.TypeaheadOptions = TypeaheadOptions;
    exports.UPLOAD_FILE_EVENT = UPLOAD_FILE_EVENT;
    exports.UPLOAD_FILE_LANGUAGE_CONFIG = UPLOAD_FILE_LANGUAGE_CONFIG;
    exports.UploadFileComponent = UploadFileComponent;
    exports.UploadFileModule = UploadFileModule;
    exports.UploadOption = UploadOption;
    exports.UploadResponse = UploadResponse;
    exports.WinModalComponent = WinModalComponent;
    exports.WinModalModule = WinModalModule;
    exports.allowedContentTypesUploadExel = allowedContentTypesUploadExel;
    exports.allowedContentTypesUploadImg = allowedContentTypesUploadImg;
    exports.allowedContentTypesUploadTxt = allowedContentTypesUploadTxt;
    exports.languageObj = languageObj;
    exports.modalConfigs = modalConfigs;
    exports.myLibInit = myLibInit;
    exports.uploadRatio = uploadRatio;
    exports.uploadType = uploadType;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=viatech-angular-common.umd.js.map
