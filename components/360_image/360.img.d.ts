import { OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import * as i0 from "@angular/core";
export declare class ImageSphereComponent implements OnInit {
    private ngbActiveModal;
    imgPath: string;
    constructor(ngbActiveModal: NgbActiveModal);
    ngOnInit(): void;
    closeModal(): void;
    static ɵfac: i0.ɵɵFactoryDef<ImageSphereComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<ImageSphereComponent, "via-photo-sphere", never, { "imgPath": "imgPath"; }, {}, never, never>;
}
