import { ElementRef, EventEmitter } from "@angular/core";
import * as i0 from "@angular/core";
export declare class ClickOutSiteDirective {
    private el;
    tohClickOutSite: EventEmitter<any>;
    clickOutSite: EventEmitter<any>;
    constructor(el: ElementRef);
    documentClick($event: any): void;
    static ɵfac: i0.ɵɵFactoryDef<ClickOutSiteDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDefWithMeta<ClickOutSiteDirective, "[tohClickOutSite]", never, {}, { "tohClickOutSite": "tohClickOutSite"; "clickOutSite": "clickOutSite"; }, never>;
}
