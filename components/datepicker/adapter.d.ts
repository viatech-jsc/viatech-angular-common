import { NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as i0 from "@angular/core";
/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
export declare class CustomAdapter extends NgbDateAdapter<string> {
    readonly DELIMITER = "-";
    fromModel(value: string | null): NgbDateStruct | null;
    toModel(date: NgbDateStruct | null): string | null;
    static ɵfac: i0.ɵɵFactoryDef<CustomAdapter, never>;
    static ɵprov: i0.ɵɵInjectableDef<CustomAdapter>;
}
/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
export declare class CustomDateParserFormatter extends NgbDateParserFormatter {
    readonly DELIMITER = "/";
    parse(value: string): NgbDateStruct | null;
    format(date: NgbDateStruct | null): string;
    static ɵfac: i0.ɵɵFactoryDef<CustomDateParserFormatter, never>;
    static ɵprov: i0.ɵɵInjectableDef<CustomDateParserFormatter>;
}
