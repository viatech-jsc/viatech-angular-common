import { EventEmitter } from "@angular/core";
import { NgbDateStruct, NgbDatepickerI18n, NgbDateParserFormatter, NgbDate, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import * as i0 from "@angular/core";
export declare class I18n {
    language: string;
    static ɵfac: i0.ɵɵFactoryDef<I18n, never>;
    static ɵprov: i0.ɵɵInjectableDef<I18n>;
}
export declare class CustomDatepickerI18n extends NgbDatepickerI18n {
    private _i18n;
    constructor(_i18n: I18n);
    getWeekdayShortName(weekday: number): string;
    getMonthShortName(month: number): string;
    getMonthFullName(month: number): string;
    getDayAriaLabel(date: NgbDateStruct): string;
    static ɵfac: i0.ɵɵFactoryDef<CustomDatepickerI18n, never>;
    static ɵprov: i0.ɵɵInjectableDef<CustomDatepickerI18n>;
}
export declare class DatepickerComponent {
    _i18n: I18n;
    formatter: NgbDateParserFormatter;
    private calendar;
    today: Date;
    iptModel: any;
    datepickerItem: NgbDateStruct;
    maxDate: NgbDateStruct;
    minDate: NgbDateStruct;
    disabled: boolean;
    datepickerItemChange: EventEmitter<NgbDateStruct>;
    dateFormat: any;
    placeholder: string;
    constructor(_i18n: I18n, formatter: NgbDateParserFormatter, calendar: NgbCalendar);
    ngOnInit(): void;
    setLanguage(language: string): void;
    selectToday(): void;
    getDateSelected(): Date;
    dateSelect(date: NgbDateStruct): void;
    isValid(obj: any): boolean;
    validateInput(currentValue: NgbDateStruct | null, input: string): NgbDate | null;
    format(datepickerItem: any): string;
    checkError(date: any): boolean;
    static ɵfac: i0.ɵɵFactoryDef<DatepickerComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<DatepickerComponent, "via-datepicker", never, { "datepickerItem": "datepickerItem"; "maxDate": "maxDate"; "minDate": "minDate"; "disabled": "disabled"; }, { "datepickerItemChange": "datepickerItemChange"; }, never, never>;
}
