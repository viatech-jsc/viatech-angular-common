import * as i0 from "@angular/core";
import * as i1 from "./datepicker";
import * as i2 from "@ng-bootstrap/ng-bootstrap";
import * as i3 from "@angular/forms";
import * as i4 from "@angular/common";
export declare class DatepickerModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<DatepickerModule, [typeof i1.DatepickerComponent], [typeof i2.NgbModule, typeof i3.FormsModule, typeof i4.CommonModule, typeof i3.ReactiveFormsModule], [typeof i1.DatepickerComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<DatepickerModule>;
}
