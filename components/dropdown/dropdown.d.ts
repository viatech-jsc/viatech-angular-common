import { EventEmitter } from "@angular/core";
import { DropdownItem } from "./dropdown.item";
import * as i0 from "@angular/core";
export declare class DropdownComponent {
    items: DropdownItem[];
    itemSelected: DropdownItem[];
    multiple: boolean;
    disabled: boolean;
    placeholder: string;
    itemSelectedChange: EventEmitter<DropdownItem[]>;
    isShow: boolean;
    searchText: string;
    constructor();
    isItemSelectedValid(): boolean;
    toggle(): void;
    hide(): void;
    show(): void;
    hasSelected(item: DropdownItem): boolean;
    selected(item: DropdownItem): void;
    clearSearchText(): void;
    getSelectedStr(itemSelected: DropdownItem[]): string;
    static ɵfac: i0.ɵɵFactoryDef<DropdownComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<DropdownComponent, "via-dropdown", never, { "items": "items"; "itemSelected": "itemSelected"; "multiple": "multiple"; "disabled": "disabled"; "placeholder": "placeholder"; }, { "itemSelectedChange": "itemSelectedChange"; }, never, never>;
}
