export declare class DropdownItem {
    name: string;
    value: string;
    constructor(name: string, value: string);
}
