import * as i0 from "@angular/core";
import * as i1 from "./dropdown";
import * as i2 from "./dropdown.pipe";
import * as i3 from "@ng-bootstrap/ng-bootstrap";
import * as i4 from "@angular/forms";
import * as i5 from "@angular/common";
export declare class DropdownModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<DropdownModule, [typeof i1.DropdownComponent, typeof i2.FullTextSearchPipe], [typeof i3.NgbModule, typeof i4.FormsModule, typeof i5.CommonModule, typeof i4.ReactiveFormsModule], [typeof i1.DropdownComponent, typeof i2.FullTextSearchPipe]>;
    static ɵinj: i0.ɵɵInjectorDef<DropdownModule>;
}
