export declare class DropdownItemDto {
    name: string;
    value: string;
    constructor(name: string, value: string);
}
