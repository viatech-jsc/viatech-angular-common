import * as i0 from "@angular/core";
export declare class GoogleAnalyticsService {
    constructor();
    eventEmitter(eventCategory: string, eventAction: string, eventLabel?: string, eventValue?: number): void;
    subscribeRouterEvents(event: any): void;
    static ɵfac: i0.ɵɵFactoryDef<GoogleAnalyticsService, never>;
    static ɵprov: i0.ɵɵInjectableDef<GoogleAnalyticsService>;
}
