import { MatDialogRef } from "@angular/material/dialog";
import * as i0 from "@angular/core";
export declare class Image360Component {
    dialogRef: MatDialogRef<Image360Component>;
    data: any;
    imgPath: string;
    panoramaId: string;
    constructor(dialogRef: MatDialogRef<Image360Component>, data: any);
    ngOnInit(): void;
    closeModal(): void;
    static ɵfac: i0.ɵɵFactoryDef<Image360Component, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<Image360Component, "via-360-image", never, { "imgPath": "imgPath"; }, {}, never, never>;
}
