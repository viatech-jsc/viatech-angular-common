import { EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Modal } from "./modal.model";
import * as i0 from "@angular/core";
export declare class WinModalComponent {
    activeModal: NgbActiveModal;
    modalOptions: Modal;
    onConfirm: EventEmitter<any>;
    modalTypes: {
        alert: string;
        confirm: string;
    };
    params: any;
    constructor(activeModal: NgbActiveModal);
    close(): void;
    ok(): void;
    handleCloseByIcon(): void;
    static ɵfac: i0.ɵɵFactoryDef<WinModalComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<WinModalComponent, "via-modal", never, { "modalOptions": "modalOptions"; }, { "onConfirm": "onConfirm"; }, never, never>;
}
