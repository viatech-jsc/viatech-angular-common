export declare class Modal {
    modalTitle: string;
    modalContent?: string;
    yesBtnTitle: string;
    noBtnTitle: string;
    type: string;
    translateParams?: object;
    modalLogo?: string;
    constructor(modalTitle: string, yesBtnTitle: string, noBtnTitle: string, type: string, translateParams: object, modalContent?: string, modalLogo?: string);
}
