import * as i0 from "@angular/core";
import * as i1 from "./modal.component";
import * as i2 from "@ng-bootstrap/ng-bootstrap";
import * as i3 from "@angular/forms";
import * as i4 from "@angular/common";
export declare class WinModalModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<WinModalModule, [typeof i1.WinModalComponent], [typeof i2.NgbModule, typeof i3.FormsModule, typeof i4.CommonModule, typeof i3.ReactiveFormsModule], [typeof i1.WinModalComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<WinModalModule>;
}
