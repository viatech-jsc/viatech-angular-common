import { EventEmitter } from "@angular/core";
import { ColumnFilterBaseDto } from "../dto/filter/column.filter.base.dto";
import { TableColumnDto } from "../dto/table.column.dto";
import * as i0 from "@angular/core";
export declare class ColumnComp {
    dropdownFilter: EventEmitter<ColumnFilterBaseDto>;
    actionSort: EventEmitter<TableColumnDto>;
    column: TableColumnDto;
    tableLanguageConfig: any;
    constructor();
    ngOnInit(): void;
    doFilterOnColumn(req: ColumnFilterBaseDto): void;
    isSortAsc(column: TableColumnDto): boolean;
    isSortDesc(column: TableColumnDto): boolean;
    doSort(column: TableColumnDto): void;
    static ɵfac: i0.ɵɵFactoryDef<ColumnComp, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<ColumnComp, "via-table-column", never, { "column": "column"; "tableLanguageConfig": "tableLanguageConfig"; }, { "dropdownFilter": "dropdownFilter"; "actionSort": "actionSort"; }, never, never>;
}
