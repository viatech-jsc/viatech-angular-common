import { EventEmitter } from "@angular/core";
import { MatSelectionList } from "@angular/material/list";
import { MatMenuTrigger } from "@angular/material/menu";
import { ColumnFilterBaseDto } from "../../../dto/filter/column.filter.base.dto";
import { ColumnFilterMultDto } from "../../../dto/filter/column.filter.mult.dto";
import { TableColumnDto } from "../../../dto/table.column.dto";
import * as i0 from "@angular/core";
export declare class ColumnFilterMultComp {
    matMenuTrigger: MatMenuTrigger;
    matSelectionList: MatSelectionList;
    dropdownFilter: EventEmitter<ColumnFilterBaseDto>;
    column: TableColumnDto;
    tableLanguageConfig: any;
    filter: ColumnFilterMultDto;
    badgeCount: number;
    constructor();
    ngOnInit(): void;
    menuOpened(): void;
    doFilter(): void;
    doReset(): void;
    static ɵfac: i0.ɵɵFactoryDef<ColumnFilterMultComp, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<ColumnFilterMultComp, "via-table-column-filter-mult", never, { "column": "column"; "tableLanguageConfig": "tableLanguageConfig"; }, { "dropdownFilter": "dropdownFilter"; }, never, never>;
}
