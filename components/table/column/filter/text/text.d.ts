import { EventEmitter } from "@angular/core";
import { MatMenuTrigger } from "@angular/material/menu";
import { ColumnFilterBaseDto } from "../../../dto/filter/column.filter.base.dto";
import { ColumnFilterTextDto } from "../../../dto/filter/column.filter.text.dto";
import { TableColumnDto } from "../../../dto/table.column.dto";
import * as i0 from "@angular/core";
export declare class ColumnFilterTextComp {
    matMenuTrigger: MatMenuTrigger;
    dropdownFilter: EventEmitter<ColumnFilterBaseDto>;
    column: TableColumnDto;
    tableLanguageConfig: any;
    filter: ColumnFilterTextDto;
    searchStr: string;
    badgeCount: number;
    constructor();
    ngOnInit(): void;
    menuOpened(): void;
    doFilter(): void;
    doReset(): void;
    static ɵfac: i0.ɵɵFactoryDef<ColumnFilterTextComp, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<ColumnFilterTextComp, "via-table-column-filter-text", never, { "column": "column"; "tableLanguageConfig": "tableLanguageConfig"; }, { "dropdownFilter": "dropdownFilter"; }, never, never>;
}
