export declare const TABLE_LANGUAGE_CONFIG: {
    table_entries_per_page: string;
    table_footer_showing: string;
    table_page: string;
    filter_by: string;
    table_to: string;
    table_of: string;
    no_data_found: string;
    table_elements: string;
    rts_dropdown_filter: string;
    rts_reset: string;
    rts_dropdown_search: string;
};
export declare const SORT: {
    ASC: string;
    DESC: string;
    NONE: string;
};
export declare const CELL_TYPE: {
    STRING: string;
    DATE_TIME: string;
    STATUS: string;
    ACTION: string;
    IMAGE_THUMBNAIL: string;
    INPUT: string;
};
export declare const FILTER_ON_COLUMN_TYPE: {
    NONE: string;
    MULTIPLE: string;
    TEXT: string;
};
export declare const ACTION_TYPE: {
    BUTTON: string;
    LINK: string;
    MODAL: string;
};
export declare const DATE_FORMAT = "hh:mma dd/MM/yyyy";
