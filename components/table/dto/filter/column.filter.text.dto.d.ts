import { ColumnFilterBaseDto } from "./column.filter.base.dto";
export declare class ColumnFilterTextDto extends ColumnFilterBaseDto {
    searchStr: string;
    constructor();
}
