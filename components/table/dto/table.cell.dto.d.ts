import { TableBtnActionDto } from "./table.btn.action.dto";
export declare class TableCellDto {
    private _value;
    private _css;
    private _type;
    private _routeLink;
    private _buttonActions;
    private _buttonDropdownActions;
    constructor(value?: string, css?: string, type?: string, routeLink?: string, buttonActions?: any[], buttonDropdownActions?: any[]);
    /**
     * Getter value
     * return {string}
     */
    get value(): string;
    /**
     * Getter css
     * return {string}
     */
    get css(): string;
    /**
     * Getter type
     * return {string}
     */
    get type(): string;
    /**
     * Getter routeLink
     * return {string}
     */
    get routeLink(): string;
    /**
     * Getter buttonActions
     * return {ButtonAction[]}
     */
    get buttonActions(): TableBtnActionDto[];
    /**
     * Getter buttonDropdownActions
     * return {ButtonAction[]}
     */
    get buttonDropdownActions(): TableBtnActionDto[];
    /**
     * Setter value
     * param {string} value
     */
    set value(value: string);
    /**
     * Setter css
     * param {string} value
     */
    set css(value: string);
    /**
     * Setter type
     * param {string} value
     */
    set type(value: string);
    /**
     * Setter routeLink
     * param {string} value
     */
    set routeLink(value: string);
    /**
     * Setter buttonActions
     * param {ButtonAction[]} value
     */
    set buttonActions(value: TableBtnActionDto[]);
    /**
     * Setter buttonDropdownActions
     * param {ButtonAction[]} value
     */
    set buttonDropdownActions(value: TableBtnActionDto[]);
}
