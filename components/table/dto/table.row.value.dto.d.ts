import { TableRowDto } from "./table.row.dto";
export declare class TableRowValueDto<T> extends TableRowDto {
    private _data;
    constructor(tableCells?: any[], css?: string, data?: T);
    get data(): T;
    set data(value: T);
}
