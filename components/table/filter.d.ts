import { PipeTransform } from '@angular/core';
import { TableColumn } from './table.column';
import * as i0 from "@angular/core";
export declare class FilterPipe implements PipeTransform {
    transform(columns: Array<TableColumn>): TableColumn[];
    static ɵfac: i0.ɵɵFactoryDef<FilterPipe, never>;
    static ɵpipe: i0.ɵɵPipeDefWithMeta<FilterPipe, "filterColumn">;
}
