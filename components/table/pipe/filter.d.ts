import { PipeTransform } from '@angular/core';
import { TableColumnDto } from '../dto/table.column.dto';
import * as i0 from "@angular/core";
export declare class FilterPipe implements PipeTransform {
    transform(columns: Array<TableColumnDto>): TableColumnDto[];
    static ɵfac: i0.ɵɵFactoryDef<FilterPipe, never>;
    static ɵpipe: i0.ɵɵPipeDefWithMeta<FilterPipe, "filterColumn">;
}
