import { EventEmitter } from "@angular/core";
import { TableCellDto } from "../../dto/table.cell.dto";
import { TableRowDto } from "../../dto/table.row.dto";
import * as i0 from "@angular/core";
export declare class CellComp {
    openImagePreview: EventEmitter<string>;
    rowUpdated: EventEmitter<TableRowDto>;
    row: TableRowDto;
    cell: TableCellDto;
    dndSupport: boolean;
    cellType: any;
    constructor();
    ngOnInit(): void;
    doOpenPreview(imgCode: string): void;
    editRow(str: string): void;
    static ɵfac: i0.ɵɵFactoryDef<CellComp, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<CellComp, "via-table-cell", never, { "row": "row"; "cell": "cell"; "dndSupport": "dndSupport"; }, { "openImagePreview": "openImagePreview"; "rowUpdated": "rowUpdated"; }, never, never>;
}
