import { TableCellDto } from "../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
export declare class CellDateTimeComp {
    cell: TableCellDto;
    dateFormat: any;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<CellDateTimeComp, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<CellDateTimeComp, "via-table-cell-datetime", never, { "cell": "cell"; }, {}, never, never>;
}
