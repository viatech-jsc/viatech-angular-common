import { TableCellDto } from "../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
export declare class CellStatusComp {
    cell: TableCellDto;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<CellStatusComp, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<CellStatusComp, "via-table-cell-status", never, { "cell": "cell"; }, {}, never, never>;
}
