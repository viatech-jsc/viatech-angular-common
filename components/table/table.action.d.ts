export declare class TableAction {
    private _name;
    private _type;
    private _routeLink;
    private _css;
    private _hided;
    constructor(name?: string, routeLink?: string, css?: string, type?: string, hided?: boolean);
    /**
     * Getter hided
     * return {boolean}
     */
    get hided(): boolean;
    /**
     * Setter hided
     * param {boolean} value
     */
    set hided(value: boolean);
    get css(): string;
    set css(value: string);
    get name(): string;
    set name(value: string);
    get type(): string;
    set type(value: string);
    get routeLink(): string;
    set routeLink(value: string);
}
export declare const ACTION_TYPE: {
    BUTTON: string;
    LINK: string;
    MODAL: string;
};
