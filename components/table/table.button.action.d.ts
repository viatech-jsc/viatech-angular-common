import { TableRow } from "./table.row";
export declare class ButtonAction {
    private _name;
    private _buttonAction;
    private _disabled;
    private _iconUrl;
    private _isDropdownItem;
    constructor(name: string, callbackFunc: (row: TableRow) => void, disabled?: boolean, iconUrl?: string, isDropdownItem?: boolean);
    /**
     * Getter name
     * return {string}
     */
    get name(): string;
    /**
     * Getter buttonAction
     * return {Function}
     */
    get buttonAction(): Function;
    /**
     * Getter disabled
     * return {boolean}
     */
    get disabled(): boolean;
    /**
     * Getter iconUrl
     * return {string}
     */
    get iconUrl(): string;
    /**
     * Getter isDropdownItem
     * return {boolean }
     */
    get isDropdownItem(): boolean;
    /**
     * Setter name
     * param {string} value
     */
    set name(value: string);
    /**
     * Setter buttonAction
     * param {Function} value
     */
    set buttonAction(value: Function);
    /**
     * Setter disabled
     * param {boolean} value
     */
    set disabled(value: boolean);
    /**
     * Setter iconUrl
     * param {string} value
     */
    set iconUrl(value: string);
    /**
     * Setter isDropdownItem
     * param {boolean } value
     */
    set isDropdownItem(value: boolean);
}
