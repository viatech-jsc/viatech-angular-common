import { ButtonAction } from "./table.button.action";
export declare class TableCell {
    private _value;
    private _css;
    private _type;
    private _routeLink;
    private _buttonActions;
    private _buttonDropdownActions;
    constructor(value?: string, css?: string, type?: string, routeLink?: string, buttonActions?: any[], buttonDropdownActions?: any[]);
    /**
     * Getter value
     * return {string}
     */
    get value(): string;
    /**
     * Getter css
     * return {string}
     */
    get css(): string;
    /**
     * Getter type
     * return {string}
     */
    get type(): string;
    /**
     * Getter routeLink
     * return {string}
     */
    get routeLink(): string;
    /**
     * Getter buttonActions
     * return {ButtonAction[]}
     */
    get buttonActions(): ButtonAction[];
    /**
     * Getter buttonDropdownActions
     * return {ButtonAction[]}
     */
    get buttonDropdownActions(): ButtonAction[];
    /**
     * Setter value
     * param {string} value
     */
    set value(value: string);
    /**
     * Setter css
     * param {string} value
     */
    set css(value: string);
    /**
     * Setter type
     * param {string} value
     */
    set type(value: string);
    /**
     * Setter routeLink
     * param {string} value
     */
    set routeLink(value: string);
    /**
     * Setter buttonActions
     * param {ButtonAction[]} value
     */
    set buttonActions(value: ButtonAction[]);
    /**
     * Setter buttonDropdownActions
     * param {ButtonAction[]} value
     */
    set buttonDropdownActions(value: ButtonAction[]);
}
export declare const CELL_TYPE: {
    STRING: string;
    DATE_TIME: string;
    STATUS: string;
    ACTION: string;
    IMAGE_THUMBNAIL: string;
    INPUT: string;
};
