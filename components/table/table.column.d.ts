import { TableFilterDropdownItem } from './table.filter.dropdown';
export declare class TableColumn {
    private _name;
    private _value;
    private _sortable;
    private _sort;
    private _css;
    private _hidden;
    private _filterList;
    private _searchStr;
    private _searchable;
    constructor(value?: string, name?: string, sortable?: boolean, sort?: string, css?: string, hidden?: boolean, filterList?: any[], searchable?: boolean, searchString?: string);
    /**
     * Getter name
     * return {string}
     */
    get name(): string;
    /**
     * Getter value
     * return {string}
     */
    get value(): string;
    /**
     * Getter sortable
     * return {boolean}
     */
    get sortable(): boolean;
    /**
     * Getter sort
     * return {string}
     */
    get sort(): string;
    /**
     * Getter css
     * return {string}
     */
    get css(): string;
    /**
     * Getter hidden
     * return {boolean}
     */
    get hidden(): boolean;
    /**
     * Getter filterList
     * return {TableFilterDropdownItem[]}
     */
    get filterList(): TableFilterDropdownItem[];
    /**
     * Getter searchStr
     * return {string}
     */
    get searchStr(): string;
    /**
     * Getter searchable
     * return {boolean}
     */
    get searchable(): boolean;
    /**
     * Setter name
     * param {string} value
     */
    set name(value: string);
    /**
     * Setter value
     * param {string} value
     */
    set value(value: string);
    /**
     * Setter sortable
     * param {boolean} value
     */
    set sortable(value: boolean);
    /**
     * Setter sort
     * param {string} value
     */
    set sort(value: string);
    /**
     * Setter css
     * param {string} value
     */
    set css(value: string);
    /**
     * Setter hidden
     * param {boolean} value
     */
    set hidden(value: boolean);
    /**
     * Setter filterList
     * param {TableFilterDropdownItem[]} value
     */
    set filterList(value: TableFilterDropdownItem[]);
    /**
     * Setter searchStr
     * param {string} value
     */
    set searchStr(value: string);
    /**
     * Setter searchable
     * param {boolean} value
     */
    set searchable(value: boolean);
}
