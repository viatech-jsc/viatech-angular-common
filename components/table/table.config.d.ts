export declare const TABLE_LANGUAGE_CONFIG: {
    table_entries_per_page: string;
    table_footer_showing: string;
    table_page: string;
    filter_by: string;
    table_to: string;
    table_of: string;
    no_data_found: string;
    table_elements: string;
    rts_dropdown_filter: string;
    rts_reset: string;
    rts_dropdown_search: string;
};
