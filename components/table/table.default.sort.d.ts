export declare class TableDefaultSort {
    private _sortColumnName;
    private _sortType;
    constructor(sortColumnName?: string, sortType?: string);
    /**
     * Getter sortColumnName
     * return {string}
     */
    get sortColumnName(): string;
    /**
     * Getter sortType
     * return {string}
     */
    get sortType(): string;
    /**
     * Setter sortColumnName
     * param {string} value
     */
    set sortColumnName(value: string);
    /**
     * Setter sortType
     * param {string} value
     */
    set sortType(value: string);
}
