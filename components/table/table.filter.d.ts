export declare class TableFilter {
    private _hasFilter;
    constructor(hasFilter?: boolean);
    get hasFilter(): boolean;
    set hasFilter(value: boolean);
}
