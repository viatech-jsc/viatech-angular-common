export declare class TableFilterDropdownItem {
    private _title;
    private _value;
    private _checked;
    constructor(title: string, value: string, checked?: boolean);
    /**
     * Getter title
     * return {string}
     */
    get title(): string;
    /**
     * Getter value
     * return {string}
     */
    get value(): string;
    /**
     * Getter checked
     * return {boolean}
     */
    get checked(): boolean;
    /**
     * Setter title
     * param {string} value
     */
    set title(value: string);
    /**
     * Setter value
     * param {string} value
     */
    set value(value: string);
    /**
     * Setter checked
     * param {boolean} value
     */
    set checked(value: boolean);
}
