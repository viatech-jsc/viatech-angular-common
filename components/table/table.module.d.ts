import * as i0 from "@angular/core";
import * as i1 from "./table";
import * as i2 from "./filter";
import * as i3 from "@angular/router";
import * as i4 from "@ng-bootstrap/ng-bootstrap";
import * as i5 from "@angular/forms";
import * as i6 from "@angular/common";
export declare class TableModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<TableModule, [typeof i1.TableComponent, typeof i2.FilterPipe], [typeof i3.RouterModule, typeof i4.NgbModule, typeof i5.FormsModule, typeof i6.CommonModule, typeof i5.ReactiveFormsModule], [typeof i1.TableComponent, typeof i2.FilterPipe, typeof i3.RouterModule]>;
    static ɵinj: i0.ɵɵInjectorDef<TableModule>;
}
