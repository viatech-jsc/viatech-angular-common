import { TableColumn } from "./table.column";
import { TableRow } from "./table.row";
import { TableFilter } from "./table.filter";
import { TableAction } from "./table.action";
import { TableDefaultSort } from "./table.default.sort";
export declare class TableOption {
    private _tableColumns;
    private _tableRows;
    private _totalItems;
    private _itemsPerPage;
    private _currentPage;
    private _tableFilter;
    private _tableActions;
    private _defaultSort;
    private _tableType;
    constructor(tableColumns?: any[], tableRows?: any[], tableType?: string);
    /**
     * Getter tableColumns
     * return {TableColumn[]}
     */
    get tableColumns(): TableColumn[];
    /**
     * Getter tableRows
     * return {TableRow[]}
     */
    get tableRows(): TableRow[];
    /**
     * Getter totalItems
     * return {number}
     */
    get totalItems(): number;
    /**
     * Getter itemsPerPage
     * return {number}
     */
    get itemsPerPage(): number;
    /**
     * Getter currentPage
     * return {number}
     */
    get currentPage(): number;
    /**
     * Getter tableFilter
     * return {TableFilter}
     */
    get tableFilter(): TableFilter;
    /**
     * Getter tableActions
     * return {TableAction[]}
     */
    get tableActions(): TableAction[];
    /**
     * Getter defaultSort
     * return {TableDefaultSort}
     */
    get defaultSort(): TableDefaultSort;
    /**
     * Getter tableType
     * return {string }
     */
    get tableType(): string;
    /**
     * Setter tableColumns
     * param {TableColumn[]} value
     */
    set tableColumns(value: TableColumn[]);
    /**
     * Setter tableRows
     * param {TableRow[]} value
     */
    set tableRows(value: TableRow[]);
    /**
     * Setter totalItems
     * param {number} value
     */
    set totalItems(value: number);
    /**
     * Setter itemsPerPage
     * param {number} value
     */
    set itemsPerPage(value: number);
    /**
     * Setter currentPage
     * param {number} value
     */
    set currentPage(value: number);
    /**
     * Setter tableFilter
     * param {TableFilter} value
     */
    set tableFilter(value: TableFilter);
    /**
     * Setter tableActions
     * param {TableAction[]} value
     */
    set tableActions(value: TableAction[]);
    /**
     * Setter defaultSort
     * param {TableDefaultSort} value
     */
    set defaultSort(value: TableDefaultSort);
    /**
     * Setter tableType
     * param {string } value
     */
    set tableType(value: string);
}
export declare class EntryPerpage {
    private _name;
    private _value;
    private _selected;
    constructor(name?: string, value?: number, selected?: boolean);
    get selected(): boolean;
    set selected(value: boolean);
    get value(): number;
    set value(value: number);
    get name(): string;
    set name(value: string);
}
