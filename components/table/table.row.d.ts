import { TableCell } from "./table.cell";
export declare class TableRow {
    private _tableCells;
    private _css;
    private _editAble;
    private _deleteAble;
    private _deactiveAble;
    private _activeAble;
    private _expandedText;
    constructor(tableCells?: TableCell[], css?: string, expandedText?: string);
    /**
     * Getter tableCells
     * return {TableCell[]}
     */
    get tableCells(): TableCell[];
    /**
     * Getter css
     * return {string}
     */
    get css(): string;
    /**
     * Getter editAble
     * return {boolean}
     */
    get editAble(): boolean;
    /**
     * Getter deleteAble
     * return {boolean}
     */
    get deleteAble(): boolean;
    /**
     * Getter deactiveAble
     * return {boolean}
     */
    get deactiveAble(): boolean;
    /**
     * Getter activeAble
     * return {boolean}
     */
    get activeAble(): boolean;
    /**
     * Getter expandedText
     * return {string}
     */
    get expandedText(): string;
    /**
     * Setter tableCells
     * param {TableCell[]} value
     */
    set tableCells(value: TableCell[]);
    /**
     * Setter css
     * param {string} value
     */
    set css(value: string);
    /**
     * Setter editAble
     * param {boolean} value
     */
    set editAble(value: boolean);
    /**
     * Setter deleteAble
     * param {boolean} value
     */
    set deleteAble(value: boolean);
    /**
     * Setter deactiveAble
     * param {boolean} value
     */
    set deactiveAble(value: boolean);
    /**
     * Setter activeAble
     * param {boolean} value
     */
    set activeAble(value: boolean);
    /**
     * Setter expandedText
     * param {string} value
     */
    set expandedText(value: string);
}
