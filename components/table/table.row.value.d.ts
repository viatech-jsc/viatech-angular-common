import { TableRow } from "./table.row";
export declare class TableRowValue<T> extends TableRow {
    private _data;
    constructor(tableCells?: any[], css?: string, data?: T);
    get data(): T;
    set data(value: T);
}
