import { EventEmitter, OnInit } from "@angular/core";
import { NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";
import * as i0 from "@angular/core";
export declare class TimepickerComponent implements OnInit {
    timepickerModel: NgbTimeStruct;
    showSeccond: boolean;
    showMeridian: boolean;
    timepickerModelChange: EventEmitter<NgbTimeStruct>;
    disabled: boolean;
    spinners: boolean;
    constructor();
    ngOnInit(): void;
    getDateSelected(): void;
    timeSelect(time: NgbTimeStruct): void;
    static ɵfac: i0.ɵɵFactoryDef<TimepickerComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<TimepickerComponent, "via-timepicker", never, { "timepickerModel": "timepickerModel"; "showSeccond": "showSeccond"; "showMeridian": "showMeridian"; "disabled": "disabled"; }, { "timepickerModelChange": "timepickerModelChange"; }, never, never>;
}
