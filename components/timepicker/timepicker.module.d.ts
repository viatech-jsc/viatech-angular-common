import * as i0 from "@angular/core";
import * as i1 from "./timepicker";
import * as i2 from "@ng-bootstrap/ng-bootstrap";
import * as i3 from "@angular/forms";
export declare class TimepickerModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<TimepickerModule, [typeof i1.TimepickerComponent], [typeof i2.NgbModule, typeof i3.FormsModule, typeof i3.ReactiveFormsModule], [typeof i1.TimepickerComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<TimepickerModule>;
}
