import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import * as i0 from "@angular/core";
export declare class CustomToast extends Toast {
    protected toastrService: ToastrService;
    toastPackage: ToastPackage;
    listMessages: string[];
    constructor(toastrService: ToastrService, toastPackage: ToastPackage);
    static ɵfac: i0.ɵɵFactoryDef<CustomToast, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<CustomToast, "[custom-toast-component]", never, {}, {}, never, never>;
}
