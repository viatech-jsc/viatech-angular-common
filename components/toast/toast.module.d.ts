import * as i0 from "@angular/core";
import * as i1 from "./toast";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common";
export declare class ToastModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<ToastModule, [typeof i1.CustomToast], [typeof i2.FormsModule, typeof i2.ReactiveFormsModule, typeof i3.CommonModule], [typeof i1.CustomToast]>;
    static ɵinj: i0.ɵɵInjectorDef<ToastModule>;
}
