import { EventEmitter } from "@angular/core";
import { FormControl } from "@angular/forms";
import { TypeaheadOptionDto } from "../dto/typeahead.option.dto";
import { TypeaheadItemDto } from "../dto/typeahead.item.dto";
import * as i0 from "@angular/core";
export declare class TypeaheadComponent {
    languageObj: {
        enter_text: string;
        no_data_found: string;
    };
    options: TypeaheadOptionDto;
    disabled: boolean;
    typeaheadLoadData: EventEmitter<any>;
    typeaheadItemSelected: EventEmitter<any>;
    searchBox: FormControl;
    typeaheadId: string;
    private isSettingValue;
    constructor();
    hide(): void;
    isSingleInlineSearch(): boolean;
    select(item: TypeaheadItemDto): void;
    remove(item: TypeaheadItemDto): void;
    removeSingleItem(): void;
    ngAfterViewInit(): void;
    setSearchBoxValue(text: string): void;
    static ɵfac: i0.ɵɵFactoryDef<TypeaheadComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<TypeaheadComponent, "via-typeahead", never, { "languageObj": "languageObj"; "options": "options"; "disabled": "disabled"; }, { "typeaheadLoadData": "typeaheadLoadData"; "typeaheadItemSelected": "typeaheadItemSelected"; }, never, never>;
}
