import * as i0 from "@angular/core";
import * as i1 from "./typeahead";
import * as i2 from "../../click_out_site/click.out.site.module";
import * as i3 from "@angular/common";
import * as i4 from "@angular/forms";
export declare class TypeaheadModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<TypeaheadModule, [typeof i1.TypeaheadComponent], [typeof i2.ClickOutSiteModule, typeof i3.CommonModule, typeof i4.FormsModule, typeof i4.ReactiveFormsModule], [typeof i1.TypeaheadComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<TypeaheadModule>;
}
