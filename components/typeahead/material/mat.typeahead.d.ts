import { EventEmitter, ElementRef } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { TypeaheadItemDto } from "../dto/typeahead.item.dto";
import { TypeaheadOptionDto } from "../dto/typeahead.option.dto";
import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
import * as i0 from "@angular/core";
export declare class MatTypeaheadComponent {
    languageObj: {
        enter_text: string;
        no_data_found: string;
    };
    options: TypeaheadOptionDto;
    disabled: boolean;
    typeaheadLoadData: EventEmitter<any>;
    typeaheadItemSelected: EventEmitter<any>;
    typeaheadInput: ElementRef<HTMLInputElement>;
    typeaheadFormGroup: FormGroup;
    separatorKeysCodes: number[];
    private isSettingValue;
    constructor();
    hide(): void;
    isSingleInlineSearch(): boolean;
    select(event: MatAutocompleteSelectedEvent): void;
    remove(item: TypeaheadItemDto): void;
    removeSingleItem(): void;
    ngAfterViewInit(): void;
    setSearchBoxValue(text: string): void;
    static ɵfac: i0.ɵɵFactoryDef<MatTypeaheadComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<MatTypeaheadComponent, "via-mat-typeahead", never, { "languageObj": "languageObj"; "options": "options"; "disabled": "disabled"; }, { "typeaheadLoadData": "typeaheadLoadData"; "typeaheadItemSelected": "typeaheadItemSelected"; }, never, never>;
}
