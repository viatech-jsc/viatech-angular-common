import * as i0 from "@angular/core";
import * as i1 from "./mat.typeahead";
import * as i2 from "../../click_out_site/click.out.site.module";
import * as i3 from "@angular/common";
import * as i4 from "@angular/forms";
import * as i5 from "@angular/material/chips";
import * as i6 from "@angular/material/icon";
import * as i7 from "@angular/material/autocomplete";
import * as i8 from "@angular/material/form-field";
import * as i9 from "@angular/material/progress-spinner";
import * as i10 from "@angular/material/input";
export declare class MatTypeaheadModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<MatTypeaheadModule, [typeof i1.MatTypeaheadComponent], [typeof i2.ClickOutSiteModule, typeof i3.CommonModule, typeof i4.FormsModule, typeof i4.ReactiveFormsModule, typeof i5.MatChipsModule, typeof i6.MatIconModule, typeof i7.MatAutocompleteModule, typeof i8.MatFormFieldModule, typeof i9.MatProgressSpinnerModule, typeof i10.MatInputModule], [typeof i1.MatTypeaheadComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<MatTypeaheadModule>;
}
