import { EventEmitter } from "@angular/core";
import { FormControl } from "@angular/forms";
import { TypeaheadOptions } from "./typeahead.options";
import { TypeaheadItem } from "./typeahead.item";
import * as i0 from "@angular/core";
export declare class TypeaheadComponent {
    languageObj: {
        enter_text: string;
        no_data_found: string;
    };
    options: TypeaheadOptions;
    disabled: boolean;
    typeaheadLoadData: EventEmitter<any>;
    typeaheadItemSelected: EventEmitter<any>;
    searchBox: FormControl;
    private isSettingValue;
    constructor();
    hide(): void;
    isSingleInlineSearch(): boolean;
    select(item: TypeaheadItem): void;
    remove(item: TypeaheadItem): void;
    removeSingleItem(): void;
    ngAfterViewInit(): void;
    setSearchBoxValue(text: string): void;
    static ɵfac: i0.ɵɵFactoryDef<TypeaheadComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<TypeaheadComponent, "via-typeahead", never, { "languageObj": "languageObj"; "options": "options"; "disabled": "disabled"; }, { "typeaheadLoadData": "typeaheadLoadData"; "typeaheadItemSelected": "typeaheadItemSelected"; }, never, never>;
}
