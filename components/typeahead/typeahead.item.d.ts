export declare class TypeaheadItem {
    name: string;
    value: string;
    constructor(name: string, value: string);
}
