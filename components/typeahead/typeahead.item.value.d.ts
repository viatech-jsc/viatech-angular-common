import { TypeaheadItem } from "./typeahead.item";
export declare class TypeaheadItemValue<T> extends TypeaheadItem {
    name: string;
    value: string;
    data: T;
    constructor(name: string, value: string, data: T);
}
