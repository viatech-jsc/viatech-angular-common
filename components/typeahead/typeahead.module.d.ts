import * as i0 from "@angular/core";
import * as i1 from "./typeahead";
import * as i2 from "@ng-bootstrap/ng-bootstrap";
import * as i3 from "../click_out_site/click.out.site.module";
import * as i4 from "@angular/common";
import * as i5 from "@angular/forms";
export declare class TypeaheadModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<TypeaheadModule, [typeof i1.TypeaheadComponent], [typeof i2.NgbModule, typeof i3.ClickOutSiteModule, typeof i4.CommonModule, typeof i5.FormsModule, typeof i5.ReactiveFormsModule], [typeof i1.TypeaheadComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<TypeaheadModule>;
}
