import { TypeaheadItem } from "./typeahead.item";
export declare class TypeaheadOptions {
    private _url;
    private _multiple;
    private _inlineSingleSearch;
    private _debounceTime;
    private _itemsSelected;
    private _items;
    private _errorMessage;
    private _isLoading;
    private _minLengthToLoad;
    private _noDataFound;
    constructor(url?: string, multiple?: boolean, inlineSingleSearch?: boolean);
    /**
     * Getter url
     * return {string}
     */
    get url(): string;
    /**
     * Getter multiple
     * return {boolean}
     */
    get multiple(): boolean;
    /**
     * Getter inlineSingleSearch
     * return {boolean}
     */
    get inlineSingleSearch(): boolean;
    /**
     * Getter debounceTime
     * return {number}
     */
    get debounceTime(): number;
    /**
     * Getter itemsSelected
     * return {TypeaheadItem[]}
     */
    get itemsSelected(): TypeaheadItem[];
    /**
     * Getter items
     * return {TypeaheadItem[]}
     */
    get items(): TypeaheadItem[];
    /**
     * Getter errorMessage
     * return {string}
     */
    get errorMessage(): string;
    /**
     * Getter isLoading
     * return {boolean}
     */
    get isLoading(): boolean;
    /**
     * Getter minLengthToLoad
     * return {number}
     */
    get minLengthToLoad(): number;
    /**
     * Getter noDataFound
     * return {boolean}
     */
    get noDataFound(): boolean;
    /**
     * Setter url
     * param {string} value
     */
    set url(value: string);
    /**
     * Setter multiple
     * param {boolean} value
     */
    set multiple(value: boolean);
    /**
     * Setter inlineSingleSearch
     * param {boolean} value
     */
    set inlineSingleSearch(value: boolean);
    /**
     * Setter debounceTime
     * param {number} value
     */
    set debounceTime(value: number);
    /**
     * Setter itemsSelected
     * param {TypeaheadItem[]} value
     */
    set itemsSelected(value: TypeaheadItem[]);
    /**
     * Setter items
     * param {TypeaheadItem[]} value
     */
    set items(value: TypeaheadItem[]);
    /**
     * Setter errorMessage
     * param {string} value
     */
    set errorMessage(value: string);
    /**
     * Setter isLoading
     * param {boolean} value
     */
    set isLoading(value: boolean);
    /**
     * Setter minLengthToLoad
     * param {number} value
     */
    set minLengthToLoad(value: number);
    /**
     * Setter noDataFound
     * param {boolean} value
     */
    set noDataFound(value: boolean);
}
