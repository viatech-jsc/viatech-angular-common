export declare const UPLOAD_FILE_EVENT: {
    START_UPLOAD: string;
    UPLOAD_ERROR: string;
    UPLOAD_SUCCESSED: string;
    FINISH_UPLOAD: string;
};
export declare const UPLOAD_FILE_LANGUAGE_CONFIG: {
    crop_image_yes: string;
    crop_image_cancel: string;
    upload_file_drag_and_drop: string;
    upload_file_or: string;
    upload_file_change: string;
    upload_file_browse_file: string;
    upload_file_browse_files: string;
};
export declare const uploadType: {
    IMAGE: string;
    FILE: string;
};
export declare const allowedContentTypesUploadTxt: string[];
export declare const allowedContentTypesUploadImg: string[];
export declare const allowedContentTypesUploadExel: string[];
export declare const uploadRatio: {
    SIXTEEN_NINE: {
        text: string;
        ratioStr: string;
        ratio: number;
        calculateHeightRatio: number;
        resizeToWidth: number;
    };
    ONE_ONE: {
        text: string;
        ratioStr: string;
        ratio: number;
        calculateHeightRatio: number;
        resizeToWidth: number;
    };
    ONE_THREE: {
        text: string;
        ratioStr: string;
        ratio: number;
        calculateHeightRatio: number;
        resizeToWidth: number;
    };
    FOUR_ONE: {
        text: string;
        ratioStr: string;
        ratio: number;
        calculateHeightRatio: number;
        resizeToWidth: number;
    };
};
export declare class ErrorObj {
    field: string;
    message: string;
    constructor(field?: string, message?: string);
}
