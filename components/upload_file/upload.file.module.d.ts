import * as i0 from "@angular/core";
import * as i1 from "./upload.file";
import * as i2 from "@angular/common/http";
import * as i3 from "@ng-bootstrap/ng-bootstrap";
import * as i4 from "@angular/common";
import * as i5 from "@angular/forms";
import * as i6 from "ngx-image-cropper";
import * as i7 from "ngx-uploader";
export declare class UploadFileModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<UploadFileModule, [typeof i1.UploadFileComponent], [typeof i2.HttpClientModule, typeof i3.NgbModule, typeof i4.CommonModule, typeof i5.FormsModule, typeof i5.ReactiveFormsModule, typeof i6.ImageCropperModule, typeof i7.NgxUploaderModule], [typeof i1.UploadFileComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<UploadFileModule>;
}
