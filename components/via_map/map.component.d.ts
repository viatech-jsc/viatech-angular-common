import { EventEmitter } from '@angular/core';
import { Map } from "./map.model";
import * as i0 from "@angular/core";
export declare class MapComponent {
    cordinate: Map;
    searchPlaceholer: string;
    markerDraggable: boolean;
    customIcon: boolean;
    showMyLocation: boolean;
    hideSearchTextBox: boolean;
    cordinateChange: EventEmitter<Map>;
    markerClick: EventEmitter<string>;
    mapRadius: EventEmitter<Map>;
    mapDragged: EventEmitter<string>;
    searchEvent: EventEmitter<Map>;
    getTopCordinatorEvent: EventEmitter<Map>;
    getCurrentLocationEvent: EventEmitter<Map>;
    latlng: any;
    geocoder: any;
    myLatLng: any;
    map: any;
    myLocation: any;
    marker: any;
    input: any;
    searchBox: any;
    greenPinIcon: string;
    locationIcon: string;
    markers: any[];
    activeInfoWindow: any;
    placeholderText: string;
    loadAPI: Promise<any>;
    circle: any;
    constructor();
    ngAfterViewInit(): void;
    getFurthestPointFromLocation(lat: number, lng: number, eventName: string): void;
    getCenterMapLocation(): any;
    calculateDistance(aLat: any, aLng: any, bLat: any, bLng: any): any;
    deleteMarkers(): void;
    showMultitpleLocations(listLocation?: Array<any>): void;
    panTo(lat: number, lng: number, zoomNumber?: number): void;
    geocodePosition(pos: any, eventName?: string, func?: Function): void;
    addSearchBoxEvent(): void;
    addClickListener(): void;
    addDragendListener(): void;
    triggerUpdateData(eventName: string): void;
    myCurrentLocation(): void;
    getRadius(distance?: number): void;
    getTopDistanceFromMarker(lat: number, lng: number, eventName?: string): void;
    drawCircle(centerLat: number, centerLng: number, pointLat: number, pointLng: number, fillColor?: string): void;
    static ɵfac: i0.ɵɵFactoryDef<MapComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<MapComponent, "via-map", never, { "cordinate": "cordinate"; "searchPlaceholer": "searchPlaceholer"; "markerDraggable": "markerDraggable"; "customIcon": "customIcon"; "showMyLocation": "showMyLocation"; "hideSearchTextBox": "hideSearchTextBox"; }, { "cordinateChange": "cordinateChange"; "markerClick": "markerClick"; "mapRadius": "mapRadius"; "mapDragged": "mapDragged"; "searchEvent": "searchEvent"; "getTopCordinatorEvent": "getTopCordinatorEvent"; "getCurrentLocationEvent": "getCurrentLocationEvent"; }, never, never>;
}
