export declare class Map {
    latitude: number;
    longtitude: number;
    title: string;
    eventName: string;
    zoomLevel: number;
    constructor(latitude?: number, longtitude?: number, title?: string, eventName?: string, zoomLevel?: number);
}
