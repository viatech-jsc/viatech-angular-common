import { MapLoader } from './map.service';
import * as i0 from "@angular/core";
import * as i1 from "./map.component";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common";
export declare function myLibInit(apiKey: string): (config: MapLoader) => () => Promise<boolean>;
export declare class MapModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<MapModule, [typeof i1.MapComponent], [typeof i2.FormsModule, typeof i3.CommonModule], [typeof i1.MapComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<MapModule>;
}
