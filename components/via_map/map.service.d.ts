import * as i0 from "@angular/core";
export declare class MapLoader {
    apikey: string;
    constructor();
    load(apikey: string): Promise<boolean>;
    static ɵfac: i0.ɵɵFactoryDef<MapLoader, never>;
    static ɵprov: i0.ɵɵInjectableDef<MapLoader>;
}
