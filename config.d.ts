export declare const mapConfig: {
    defaultLattitude: number;
    defaultLongtitude: number;
    defaultTitle: string;
    mapEventNames: {
        search: string;
        myLocation: string;
        mapClick: string;
        markerDrag: string;
    };
    myLocation: string;
};
export declare const DATE_FORMAT = "hh:mma dd/MM/yyyy";
export declare const TIME_FORMAT = "hh:mma";
export declare const DATE_FORMAT_WITHOUT_TIME = "dd/MM/yyyy";
