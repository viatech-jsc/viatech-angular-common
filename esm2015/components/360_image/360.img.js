import { Component, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import * as i0 from "@angular/core";
import * as i1 from "@ng-bootstrap/ng-bootstrap";
export class ImageSphereComponent {
    constructor(ngbActiveModal) {
        this.ngbActiveModal = ngbActiveModal;
    }
    ngOnInit() {
        pannellum.viewer('panorama', {
            "type": "equirectangular",
            "autoLoad": true,
            "title": "360 Photo",
            "compass": true,
            "panorama": this.imgPath
        });
    }
    closeModal() {
        this.ngbActiveModal.close();
    }
}
ImageSphereComponent.ɵfac = function ImageSphereComponent_Factory(t) { return new (t || ImageSphereComponent)(i0.ɵɵdirectiveInject(i1.NgbActiveModal)); };
ImageSphereComponent.ɵcmp = i0.ɵɵdefineComponent({ type: ImageSphereComponent, selectors: [["via-photo-sphere"]], inputs: { imgPath: "imgPath" }, decls: 5, vars: 0, consts: [["id", "photo-sphere"], [1, "modal-360", 3, "click"], ["aria-hidden", "true"], ["id", "panorama"]], template: function ImageSphereComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵlistener("click", function ImageSphereComponent_Template_div_click_1_listener() { return ctx.closeModal(); });
        i0.ɵɵelementStart(2, "span", 2);
        i0.ɵɵtext(3, "\u00D7");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelement(4, "div", 3);
        i0.ɵɵelementEnd();
    } }, styles: ["#photo-sphere[_ngcontent-%COMP%]   #panorama[_ngcontent-%COMP%]{min-height:700px}#photo-sphere[_ngcontent-%COMP%]   .modal-360[_ngcontent-%COMP%]{background:#fff;border:1px solid rgba(0,0,0,.4);border-radius:4px;cursor:pointer;height:25px;line-height:25px;position:absolute;right:3px;text-align:center;top:3px;width:25px;z-index:1}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ImageSphereComponent, [{
        type: Component,
        args: [{
                selector: "via-photo-sphere",
                templateUrl: "./360.img.html",
                styleUrls: ["./360.img.scss"]
            }]
    }], function () { return [{ type: i1.NgbActiveModal }]; }, { imgPath: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMzYwLmltZy5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzLzM2MF9pbWFnZS8zNjAuaW1nLnRzIiwiY29tcG9uZW50cy8zNjBfaW1hZ2UvMzYwLmltZy5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQzs7O0FBUTVELE1BQU0sT0FBTyxvQkFBb0I7SUFHL0IsWUFBb0IsY0FBOEI7UUFBOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO0lBQUksQ0FBQztJQUV2RCxRQUFRO1FBQ04sU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUU7WUFDM0IsTUFBTSxFQUFFLGlCQUFpQjtZQUN6QixVQUFVLEVBQUUsSUFBSTtZQUNoQixPQUFPLEVBQUUsV0FBVztZQUNwQixTQUFTLEVBQUUsSUFBSTtZQUNmLFVBQVUsRUFBRSxJQUFJLENBQUMsT0FBTztTQUN6QixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsVUFBVTtRQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDOUIsQ0FBQzs7d0ZBakJVLG9CQUFvQjt5REFBcEIsb0JBQW9CO1FDVGpDLDhCQUNJO1FBQUEsOEJBQThDO1FBQXpDLDhGQUFTLGdCQUFZLElBQUM7UUFBbUIsK0JBQXlCO1FBQUEsc0JBQU87UUFBQSxpQkFBTztRQUNyRixpQkFBTTtRQUNOLHlCQUF5QjtRQUM3QixpQkFBTTs7a0RES08sb0JBQW9CO2NBTGhDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixXQUFXLEVBQUUsZ0JBQWdCO2dCQUM3QixTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQzthQUM5QjtpRUFFVSxPQUFPO2tCQUFmLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTmdiQWN0aXZlTW9kYWwgfSBmcm9tIFwiQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXBcIjtcbmRlY2xhcmUgbGV0IHBhbm5lbGx1bTogYW55O1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwidmlhLXBob3RvLXNwaGVyZVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuLzM2MC5pbWcuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vMzYwLmltZy5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIEltYWdlU3BoZXJlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgaW1nUGF0aDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbmdiQWN0aXZlTW9kYWw6IE5nYkFjdGl2ZU1vZGFsKSB7IH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICBwYW5uZWxsdW0udmlld2VyKCdwYW5vcmFtYScsIHtcbiAgICAgIFwidHlwZVwiOiBcImVxdWlyZWN0YW5ndWxhclwiLFxuICAgICAgXCJhdXRvTG9hZFwiOiB0cnVlLFxuICAgICAgXCJ0aXRsZVwiOiBcIjM2MCBQaG90b1wiLFxuICAgICAgXCJjb21wYXNzXCI6IHRydWUsXG4gICAgICBcInBhbm9yYW1hXCI6IHRoaXMuaW1nUGF0aFxuICAgIH0pO1xuICB9XG5cbiAgY2xvc2VNb2RhbCgpOiB2b2lkIHtcbiAgICB0aGlzLm5nYkFjdGl2ZU1vZGFsLmNsb3NlKCk7XG4gIH1cbn1cbiIsIjxkaXYgaWQ9XCJwaG90by1zcGhlcmVcIj5cbiAgICA8ZGl2IChjbGljayk9XCJjbG9zZU1vZGFsKClcIiBjbGFzcz1cIm1vZGFsLTM2MFwiPjxzcGFuIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPiZ0aW1lczs8L3NwYW4+XG4gICAgPC9kaXY+XG4gICAgPGRpdiBpZD1cInBhbm9yYW1hXCI+PC9kaXY+XG48L2Rpdj4iXX0=