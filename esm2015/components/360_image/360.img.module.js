import { NgModule } from '@angular/core';
import { ImageSphereComponent } from './360.img';
import * as i0 from "@angular/core";
export class ImageSphereModule {
}
ImageSphereModule.ɵmod = i0.ɵɵdefineNgModule({ type: ImageSphereModule });
ImageSphereModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ImageSphereModule_Factory(t) { return new (t || ImageSphereModule)(); }, imports: [[]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ImageSphereModule, { declarations: [ImageSphereComponent], exports: [ImageSphereComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ImageSphereModule, [{
        type: NgModule,
        args: [{
                declarations: [ImageSphereComponent],
                imports: [],
                exports: [ImageSphereComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMzYwLmltZy5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy8zNjBfaW1hZ2UvMzYwLmltZy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxXQUFXLENBQUM7O0FBT2pELE1BQU0sT0FBTyxpQkFBaUI7O3FEQUFqQixpQkFBaUI7aUhBQWpCLGlCQUFpQixrQkFIbkIsRUFBRTt3RkFHQSxpQkFBaUIsbUJBSmIsb0JBQW9CLGFBRXpCLG9CQUFvQjtrREFFbkIsaUJBQWlCO2NBTDdCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDcEMsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsT0FBTyxFQUFFLENBQUMsb0JBQW9CLENBQUM7YUFDaEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSW1hZ2VTcGhlcmVDb21wb25lbnQgfSBmcm9tICcuLzM2MC5pbWcnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtJbWFnZVNwaGVyZUNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtdLFxuICBleHBvcnRzOiBbSW1hZ2VTcGhlcmVDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIEltYWdlU3BoZXJlTW9kdWxlIHsgfVxuIl19