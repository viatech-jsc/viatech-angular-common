import { Directive, HostListener, ElementRef, Output, EventEmitter } from "@angular/core";
import * as i0 from "@angular/core";
export class ClickOutSiteDirective {
    constructor(el) {
        this.el = el;
        this.tohClickOutSite = new EventEmitter();
        this.clickOutSite = new EventEmitter();
    }
    documentClick($event) {
        if (this.tohClickOutSite.observers.length > 0) {
            if (this.el.nativeElement.innerHTML.indexOf($event.target.innerHTML) === -1) {
                this.tohClickOutSite.emit(null);
            }
        }
        else if (this.clickOutSite.observers.length > 0) {
            if (!this.el.nativeElement.contains($event.target)) {
                this.clickOutSite.emit(null);
            }
        }
    }
}
ClickOutSiteDirective.ɵfac = function ClickOutSiteDirective_Factory(t) { return new (t || ClickOutSiteDirective)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
ClickOutSiteDirective.ɵdir = i0.ɵɵdefineDirective({ type: ClickOutSiteDirective, selectors: [["", "tohClickOutSite", ""]], hostBindings: function ClickOutSiteDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("click", function ClickOutSiteDirective_click_HostBindingHandler($event) { return ctx.documentClick($event); }, false, i0.ɵɵresolveDocument);
    } }, outputs: { tohClickOutSite: "tohClickOutSite", clickOutSite: "clickOutSite" } });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ClickOutSiteDirective, [{
        type: Directive,
        args: [{
                selector: "[tohClickOutSite]"
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { tohClickOutSite: [{
            type: Output
        }], clickOutSite: [{
            type: Output
        }], documentClick: [{
            type: HostListener,
            args: ["document:click", ["$event"]]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpY2sub3V0LnNpdGUuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9jbGlja19vdXRfc2l0ZS9jbGljay5vdXQuc2l0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULFlBQVksRUFDWixVQUFVLEVBQ1YsTUFBTSxFQUNOLFlBQVksRUFDYixNQUFNLGVBQWUsQ0FBQzs7QUFLdkIsTUFBTSxPQUFPLHFCQUFxQjtJQUloQyxZQUFvQixFQUFjO1FBQWQsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUhqQixvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDckMsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBRWQsQ0FBQztJQUd0QyxhQUFhLENBQUMsTUFBVztRQUN2QixJQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUM7WUFDM0MsSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQzNFLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2pDO1NBQ0Y7YUFBTSxJQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUM7WUFDL0MsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2xELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzlCO1NBQ0Y7SUFDSCxDQUFDOzswRkFqQlUscUJBQXFCOzBEQUFyQixxQkFBcUI7d0dBQXJCLHlCQUFxQjs7a0RBQXJCLHFCQUFxQjtjQUhqQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjthQUM5Qjs2REFFa0IsZUFBZTtrQkFBL0IsTUFBTTtZQUNVLFlBQVk7a0JBQTVCLE1BQU07WUFLUCxhQUFhO2tCQURaLFlBQVk7bUJBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBEaXJlY3RpdmUsXG4gIEhvc3RMaXN0ZW5lcixcbiAgRWxlbWVudFJlZixcbiAgT3V0cHV0LFxuICBFdmVudEVtaXR0ZXJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiBcIlt0b2hDbGlja091dFNpdGVdXCJcbn0pXG5leHBvcnQgY2xhc3MgQ2xpY2tPdXRTaXRlRGlyZWN0aXZlIHtcbiAgQE91dHB1dCgpIHB1YmxpYyB0b2hDbGlja091dFNpdGUgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBwdWJsaWMgY2xpY2tPdXRTaXRlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZWw6IEVsZW1lbnRSZWYpIHt9XG5cbiAgQEhvc3RMaXN0ZW5lcihcImRvY3VtZW50OmNsaWNrXCIsIFtcIiRldmVudFwiXSlcbiAgZG9jdW1lbnRDbGljaygkZXZlbnQ6IGFueSkge1xuICAgIGlmKHRoaXMudG9oQ2xpY2tPdXRTaXRlLm9ic2VydmVycy5sZW5ndGggPiAwKXtcbiAgICAgIGlmICh0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuaW5uZXJIVE1MLmluZGV4T2YoJGV2ZW50LnRhcmdldC5pbm5lckhUTUwpID09PSAtMSkge1xuICAgICAgICB0aGlzLnRvaENsaWNrT3V0U2l0ZS5lbWl0KG51bGwpO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZih0aGlzLmNsaWNrT3V0U2l0ZS5vYnNlcnZlcnMubGVuZ3RoID4gMCl7XG4gICAgICBpZiAoIXRoaXMuZWwubmF0aXZlRWxlbWVudC5jb250YWlucygkZXZlbnQudGFyZ2V0KSkge1xuICAgICAgICB0aGlzLmNsaWNrT3V0U2l0ZS5lbWl0KG51bGwpO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIl19