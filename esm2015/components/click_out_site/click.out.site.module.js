import { ClickOutSiteDirective } from "./click.out.site";
import { NgModule } from "@angular/core";
import * as i0 from "@angular/core";
export class ClickOutSiteModule {
}
ClickOutSiteModule.ɵmod = i0.ɵɵdefineNgModule({ type: ClickOutSiteModule });
ClickOutSiteModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ClickOutSiteModule_Factory(t) { return new (t || ClickOutSiteModule)(); }, imports: [[]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ClickOutSiteModule, { declarations: [ClickOutSiteDirective], exports: [ClickOutSiteDirective] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ClickOutSiteModule, [{
        type: NgModule,
        args: [{
                imports: [],
                declarations: [ClickOutSiteDirective],
                exports: [ClickOutSiteDirective]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpY2sub3V0LnNpdGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvY2xpY2tfb3V0X3NpdGUvY2xpY2sub3V0LnNpdGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBT3pDLE1BQU0sT0FBTyxrQkFBa0I7O3NEQUFsQixrQkFBa0I7bUhBQWxCLGtCQUFrQixrQkFKcEIsRUFBRTt3RkFJQSxrQkFBa0IsbUJBSGQscUJBQXFCLGFBQzFCLHFCQUFxQjtrREFFcEIsa0JBQWtCO2NBTDlCLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUUsRUFBRTtnQkFDWCxZQUFZLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztnQkFDckMsT0FBTyxFQUFFLENBQUMscUJBQXFCLENBQUM7YUFDakMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDbGlja091dFNpdGVEaXJlY3RpdmUgfSBmcm9tIFwiLi9jbGljay5vdXQuc2l0ZVwiO1xuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXSxcbiAgZGVjbGFyYXRpb25zOiBbQ2xpY2tPdXRTaXRlRGlyZWN0aXZlXSxcbiAgZXhwb3J0czogW0NsaWNrT3V0U2l0ZURpcmVjdGl2ZV1cbn0pXG5leHBvcnQgY2xhc3MgQ2xpY2tPdXRTaXRlTW9kdWxlIHt9XG4iXX0=