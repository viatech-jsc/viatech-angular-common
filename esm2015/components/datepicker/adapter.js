import { Injectable } from '@angular/core';
import { NgbDateAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import * as i0 from "@angular/core";
/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
export class CustomAdapter extends NgbDateAdapter {
    constructor() {
        super(...arguments);
        this.DELIMITER = '-';
    }
    fromModel(value) {
        if (value) {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }
    toModel(date) {
        return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
    }
}
CustomAdapter.ɵfac = function CustomAdapter_Factory(t) { return ɵCustomAdapter_BaseFactory(t || CustomAdapter); };
CustomAdapter.ɵprov = i0.ɵɵdefineInjectable({ token: CustomAdapter, factory: CustomAdapter.ɵfac });
const ɵCustomAdapter_BaseFactory = /*@__PURE__*/ i0.ɵɵgetInheritedFactory(CustomAdapter);
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CustomAdapter, [{
        type: Injectable
    }], null, null); })();
/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
export class CustomDateParserFormatter extends NgbDateParserFormatter {
    constructor() {
        super(...arguments);
        this.DELIMITER = '/';
    }
    parse(value) {
        if (value) {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }
    format(date) {
        return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
    }
}
CustomDateParserFormatter.ɵfac = function CustomDateParserFormatter_Factory(t) { return ɵCustomDateParserFormatter_BaseFactory(t || CustomDateParserFormatter); };
CustomDateParserFormatter.ɵprov = i0.ɵɵdefineInjectable({ token: CustomDateParserFormatter, factory: CustomDateParserFormatter.ɵfac });
const ɵCustomDateParserFormatter_BaseFactory = /*@__PURE__*/ i0.ɵɵgetInheritedFactory(CustomDateParserFormatter);
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CustomDateParserFormatter, [{
        type: Injectable
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRhcHRlci5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2RhdGVwaWNrZXIvYWRhcHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQVksVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3BELE9BQU8sRUFBYyxjQUFjLEVBQUUsc0JBQXNCLEVBQWdCLE1BQU0sNEJBQTRCLENBQUM7O0FBRTlHOztHQUVHO0FBRUgsTUFBTSxPQUFPLGFBQWMsU0FBUSxjQUFzQjtJQUR6RDs7UUFHVyxjQUFTLEdBQUcsR0FBRyxDQUFDO0tBaUIxQjtJQWZDLFNBQVMsQ0FBQyxLQUFvQjtRQUM1QixJQUFJLEtBQUssRUFBRTtZQUNULElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZDLE9BQU87Z0JBQ0wsR0FBRyxFQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUMzQixLQUFLLEVBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQzdCLElBQUksRUFBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQzthQUM3QixDQUFDO1NBQ0g7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxPQUFPLENBQUMsSUFBMEI7UUFDaEMsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQzNGLENBQUM7O2dHQWxCVSxhQUFhO3FEQUFiLGFBQWEsV0FBYixhQUFhOzBFQUFiLGFBQWE7a0RBQWIsYUFBYTtjQUR6QixVQUFVOztBQXNCWDs7R0FFRztBQUVILE1BQU0sT0FBTyx5QkFBMEIsU0FBUSxzQkFBc0I7SUFEckU7O1FBR1csY0FBUyxHQUFHLEdBQUcsQ0FBQztLQWlCMUI7SUFmQyxLQUFLLENBQUMsS0FBYTtRQUNqQixJQUFJLEtBQUssRUFBRTtZQUNULElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZDLE9BQU87Z0JBQ0wsR0FBRyxFQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUMzQixLQUFLLEVBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUM7Z0JBQzdCLElBQUksRUFBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQzthQUM3QixDQUFDO1NBQ0g7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxNQUFNLENBQUMsSUFBMEI7UUFDL0IsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ3pGLENBQUM7O29JQWxCVSx5QkFBeUI7aUVBQXpCLHlCQUF5QixXQUF6Qix5QkFBeUI7c0ZBQXpCLHlCQUF5QjtrREFBekIseUJBQXlCO2NBRHJDLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge05nYkNhbGVuZGFyLCBOZ2JEYXRlQWRhcHRlciwgTmdiRGF0ZVBhcnNlckZvcm1hdHRlciwgTmdiRGF0ZVN0cnVjdH0gZnJvbSAnQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXAnO1xuXG4vKipcbiAqIFRoaXMgU2VydmljZSBoYW5kbGVzIGhvdyB0aGUgZGF0ZSBpcyByZXByZXNlbnRlZCBpbiBzY3JpcHRzIGkuZS4gbmdNb2RlbC5cbiAqL1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEN1c3RvbUFkYXB0ZXIgZXh0ZW5kcyBOZ2JEYXRlQWRhcHRlcjxzdHJpbmc+IHtcblxuICByZWFkb25seSBERUxJTUlURVIgPSAnLSc7XG5cbiAgZnJvbU1vZGVsKHZhbHVlOiBzdHJpbmcgfCBudWxsKTogTmdiRGF0ZVN0cnVjdCB8IG51bGwge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgbGV0IGRhdGUgPSB2YWx1ZS5zcGxpdCh0aGlzLkRFTElNSVRFUik7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBkYXkgOiBwYXJzZUludChkYXRlWzBdLCAxMCksXG4gICAgICAgIG1vbnRoIDogcGFyc2VJbnQoZGF0ZVsxXSwgMTApLFxuICAgICAgICB5ZWFyIDogcGFyc2VJbnQoZGF0ZVsyXSwgMTApXG4gICAgICB9O1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHRvTW9kZWwoZGF0ZTogTmdiRGF0ZVN0cnVjdCB8IG51bGwpOiBzdHJpbmcgfCBudWxsIHtcbiAgICByZXR1cm4gZGF0ZSA/IGRhdGUuZGF5ICsgdGhpcy5ERUxJTUlURVIgKyBkYXRlLm1vbnRoICsgdGhpcy5ERUxJTUlURVIgKyBkYXRlLnllYXIgOiBudWxsO1xuICB9XG59XG5cbi8qKlxuICogVGhpcyBTZXJ2aWNlIGhhbmRsZXMgaG93IHRoZSBkYXRlIGlzIHJlbmRlcmVkIGFuZCBwYXJzZWQgZnJvbSBrZXlib2FyZCBpLmUuIGluIHRoZSBib3VuZCBpbnB1dCBmaWVsZC5cbiAqL1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEN1c3RvbURhdGVQYXJzZXJGb3JtYXR0ZXIgZXh0ZW5kcyBOZ2JEYXRlUGFyc2VyRm9ybWF0dGVyIHtcblxuICByZWFkb25seSBERUxJTUlURVIgPSAnLyc7XG5cbiAgcGFyc2UodmFsdWU6IHN0cmluZyk6IE5nYkRhdGVTdHJ1Y3QgfCBudWxsIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIGxldCBkYXRlID0gdmFsdWUuc3BsaXQodGhpcy5ERUxJTUlURVIpO1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZGF5IDogcGFyc2VJbnQoZGF0ZVswXSwgMTApLFxuICAgICAgICBtb250aCA6IHBhcnNlSW50KGRhdGVbMV0sIDEwKSxcbiAgICAgICAgeWVhciA6IHBhcnNlSW50KGRhdGVbMl0sIDEwKVxuICAgICAgfTtcbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBmb3JtYXQoZGF0ZTogTmdiRGF0ZVN0cnVjdCB8IG51bGwpOiBzdHJpbmcge1xuICAgIHJldHVybiBkYXRlID8gZGF0ZS5kYXkgKyB0aGlzLkRFTElNSVRFUiArIGRhdGUubW9udGggKyB0aGlzLkRFTElNSVRFUiArIGRhdGUueWVhciA6ICcnO1xuICB9XG59Il19