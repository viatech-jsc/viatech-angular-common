import { Component, Input, Output, EventEmitter, Injectable } from "@angular/core";
import { NgbDatepickerI18n, NgbDateParserFormatter, NgbDate, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { DATE_FORMAT_WITHOUT_TIME } from "./datepicker.config";
import * as i0 from "@angular/core";
import * as i1 from "@ng-bootstrap/ng-bootstrap";
const I18N_VALUES = {
    en: {
        weekdays: [
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa",
            "Su"
        ],
        months: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        ]
    },
    vi: {
        weekdays: [
            "T2",
            "T3",
            "T4",
            "T5",
            "T6",
            "T7",
            "CN"
        ],
        months: [
            "T1",
            "T2",
            "T3",
            "T4",
            "T5",
            "T6",
            "T7",
            "T8",
            "T9",
            "T10",
            "T11",
            "T12"
        ]
    }
};
// Define a service holding the language. You probably already have one if your app is i18ned.
export class I18n {
    constructor() {
        this.language = "vi";
    }
}
I18n.ɵfac = function I18n_Factory(t) { return new (t || I18n)(); };
I18n.ɵprov = i0.ɵɵdefineInjectable({ token: I18n, factory: I18n.ɵfac });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(I18n, [{
        type: Injectable
    }], null, null); })();
// Define custom service providing the months and weekdays translations
export class CustomDatepickerI18n extends NgbDatepickerI18n {
    constructor(_i18n) {
        super();
        this._i18n = _i18n;
    }
    getWeekdayShortName(weekday) {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    getMonthShortName(month) {
        return I18N_VALUES[this._i18n.language].months[month - 1];
    }
    getMonthFullName(month) {
        return this.getMonthShortName(month);
    }
    getDayAriaLabel(date) {
        return `${date.day}-${date.month}-${date.year}`;
    }
}
CustomDatepickerI18n.ɵfac = function CustomDatepickerI18n_Factory(t) { return new (t || CustomDatepickerI18n)(i0.ɵɵinject(I18n)); };
CustomDatepickerI18n.ɵprov = i0.ɵɵdefineInjectable({ token: CustomDatepickerI18n, factory: CustomDatepickerI18n.ɵfac });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CustomDatepickerI18n, [{
        type: Injectable
    }], function () { return [{ type: I18n }]; }, null); })();
export class DatepickerComponent {
    constructor(_i18n, formatter, calendar) {
        this._i18n = _i18n;
        this.formatter = formatter;
        this.calendar = calendar;
        this.today = new Date();
        this.minDate = {
            day: 1,
            month: 1,
            year: this.today.getFullYear() - 100
        };
        this.disabled = false;
        this.datepickerItemChange = new EventEmitter();
        this.dateFormat = DATE_FORMAT_WITHOUT_TIME;
        //super();
    }
    ngOnInit() {
        this._i18n.language = "en";
    }
    setLanguage(language) {
        this._i18n.language = language;
        this.placeholder = this._i18n.language == "en" ? "dd/mm/yyyy" : "nn/tt/nnnn";
    }
    selectToday() {
        this.datepickerItem = { year: 2021, month: 6, day: 7 };
    }
    getDateSelected() {
        if (this.isValid(this.datepickerItem)) {
            return new Date(this.datepickerItem.year, this.datepickerItem.month - 1, this.datepickerItem.day);
        }
        return null;
    }
    dateSelect(date) {
        this.datepickerItemChange.emit(date);
    }
    isValid(obj) {
        return obj !== null && obj !== undefined;
    }
    validateInput(currentValue, input) {
        const parsed = this.formatter.parse(input);
        let value;
        if ((parsed && this.calendar.isValid(NgbDate.from(parsed))) || parsed == undefined || parsed == null) {
            value = NgbDate.from(parsed);
        }
        else {
            value = null;
        }
        this.dateSelect(value);
        return value;
    }
    format(datepickerItem) {
        return this.formatter.format(datepickerItem);
    }
    checkError(date) {
        const parsed = this.formatter.parse(date);
        let isValid = false;
        if (parsed && this.calendar.isValid(NgbDate.from(parsed))) {
            isValid = true;
        }
        return isValid;
    }
}
DatepickerComponent.ɵfac = function DatepickerComponent_Factory(t) { return new (t || DatepickerComponent)(i0.ɵɵdirectiveInject(I18n), i0.ɵɵdirectiveInject(i1.NgbDateParserFormatter), i0.ɵɵdirectiveInject(i1.NgbCalendar)); };
DatepickerComponent.ɵcmp = i0.ɵɵdefineComponent({ type: DatepickerComponent, selectors: [["via-datepicker"]], inputs: { datepickerItem: "datepickerItem", maxDate: "maxDate", minDate: "minDate", disabled: "disabled" }, outputs: { datepickerItemChange: "datepickerItemChange" }, decls: 5, vars: 5, consts: [["id", "win-datepicker", "tohClickOutSite", "", 3, "clickOutSite"], ["placeholder", "DD/MM/YYYY", "name", "datePickerIpt", "ngbDatepicker", "", 1, "form-control", 3, "disabled", "startDate", "value", "minDate", "maxDate", "input", "dateSelect"], ["datePickerIpt", "", "d", "ngbDatepicker"], ["src", "./assets/icons/ic_calendar.svg", 1, "date-icon", 3, "click"]], template: function DatepickerComponent_Template(rf, ctx) { if (rf & 1) {
        const _r2 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵlistener("clickOutSite", function DatepickerComponent_Template_div_clickOutSite_0_listener() { i0.ɵɵrestoreView(_r2); const _r1 = i0.ɵɵreference(3); return _r1.close(); });
        i0.ɵɵelementStart(1, "input", 1, 2);
        i0.ɵɵlistener("input", function DatepickerComponent_Template_input_input_1_listener() { i0.ɵɵrestoreView(_r2); const _r0 = i0.ɵɵreference(2); return ctx.datepickerItem = ctx.validateInput(ctx.datepickerItem, _r0.value); })("dateSelect", function DatepickerComponent_Template_input_dateSelect_1_listener($event) { return ctx.dateSelect($event); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(4, "img", 3);
        i0.ɵɵlistener("click", function DatepickerComponent_Template_img_click_4_listener() { i0.ɵɵrestoreView(_r2); const _r1 = i0.ɵɵreference(3); return _r1.toggle(); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("disabled", ctx.disabled)("startDate", ctx.datepickerItem)("value", ctx.format(ctx.datepickerItem))("minDate", ctx.minDate)("maxDate", ctx.maxDate);
    } }, directives: [i1.NgbInputDatepicker], styles: ["#win-datepicker[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:column;position:relative;width:100%}#win-datepicker[_ngcontent-%COMP%]   .date-icon[_ngcontent-%COMP%]{position:absolute;right:10px;top:13px;width:20px}#win-datepicker[_ngcontent-%COMP%]   .disabled-background[_ngcontent-%COMP%]{background:#f7f8f9}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:5px 10px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]{align-items:center;display:flex;flex:1;justify-content:baseline}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{margin-right:5px;width:20px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{flex:8}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span.dropdown-arrow[_ngcontent-%COMP%]{flex:1;font-size:24px;text-align:right}#win-datepicker[_ngcontent-%COMP%]   .input-datepicker[_ngcontent-%COMP%]{border:0;font-size:0;height:0;line-height:0;margin:0;padding:0;visibility:hidden;width:100%}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DatepickerComponent, [{
        type: Component,
        args: [{
                selector: "via-datepicker",
                templateUrl: "./datepicker.html",
                styleUrls: ["./datepicker.scss"]
            }]
    }], function () { return [{ type: I18n }, { type: i1.NgbDateParserFormatter }, { type: i1.NgbCalendar }]; }, { datepickerItem: [{
            type: Input
        }], maxDate: [{
            type: Input
        }], minDate: [{
            type: Input
        }], disabled: [{
            type: Input
        }], datepickerItemChange: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXBpY2tlci5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2RhdGVwaWNrZXIvZGF0ZXBpY2tlci50cyIsImNvbXBvbmVudHMvZGF0ZXBpY2tlci9kYXRlcGlja2VyLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkYsT0FBTyxFQUFpQixpQkFBaUIsRUFBRSxzQkFBc0IsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUgsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0scUJBQXFCLENBQUM7OztBQUUvRCxNQUFNLFdBQVcsR0FBRztJQUNsQixFQUFFLEVBQUU7UUFDRixRQUFRLEVBQUU7WUFDUixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1NBQ0w7UUFDRCxNQUFNLEVBQUU7WUFDTixLQUFLO1lBQ0wsS0FBSztZQUNMLEtBQUs7WUFDTCxLQUFLO1lBQ0wsS0FBSztZQUNMLEtBQUs7WUFDTCxLQUFLO1lBQ0wsS0FBSztZQUNMLEtBQUs7WUFDTCxLQUFLO1lBQ0wsS0FBSztZQUNMLEtBQUs7U0FDTjtLQUNGO0lBQ0QsRUFBRSxFQUFFO1FBQ0YsUUFBUSxFQUFFO1lBQ1IsSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtTQUNMO1FBQ0QsTUFBTSxFQUFFO1lBQ04sSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osSUFBSTtZQUNKLElBQUk7WUFDSixJQUFJO1lBQ0osS0FBSztZQUNMLEtBQUs7WUFDTCxLQUFLO1NBQ047S0FDRjtDQUNGLENBQUM7QUFFRiw4RkFBOEY7QUFFOUYsTUFBTSxPQUFPLElBQUk7SUFEakI7UUFFRSxhQUFRLEdBQUcsSUFBSSxDQUFDO0tBQ2pCOzt3REFGWSxJQUFJOzRDQUFKLElBQUksV0FBSixJQUFJO2tEQUFKLElBQUk7Y0FEaEIsVUFBVTs7QUFLWCx1RUFBdUU7QUFFdkUsTUFBTSxPQUFPLG9CQUFxQixTQUFRLGlCQUFpQjtJQUV6RCxZQUFvQixLQUFXO1FBQzdCLEtBQUssRUFBRSxDQUFDO1FBRFUsVUFBSyxHQUFMLEtBQUssQ0FBTTtJQUUvQixDQUFDO0lBRUQsbUJBQW1CLENBQUMsT0FBZTtRQUNqQyxPQUFPLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUNELGlCQUFpQixDQUFDLEtBQWE7UUFDN0IsT0FBTyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRCxnQkFBZ0IsQ0FBQyxLQUFhO1FBQzVCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxlQUFlLENBQUMsSUFBbUI7UUFDakMsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDbEQsQ0FBQzs7d0ZBbEJVLG9CQUFvQixjQUVKLElBQUk7NERBRnBCLG9CQUFvQixXQUFwQixvQkFBb0I7a0RBQXBCLG9CQUFvQjtjQURoQyxVQUFVO3NDQUdrQixJQUFJO0FBeUJqQyxNQUFNLE9BQU8sbUJBQW1CO0lBaUI5QixZQUFtQixLQUFXLEVBQVMsU0FBaUMsRUFBVSxRQUFxQjtRQUFwRixVQUFLLEdBQUwsS0FBSyxDQUFNO1FBQVMsY0FBUyxHQUFULFNBQVMsQ0FBd0I7UUFBVSxhQUFRLEdBQVIsUUFBUSxDQUFhO1FBaEJ2RyxVQUFLLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUtuQixZQUFPLEdBQWtCO1lBQ3ZCLEdBQUcsRUFBRSxDQUFDO1lBQ04sS0FBSyxFQUFFLENBQUM7WUFDUixJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsR0FBRyxHQUFHO1NBQ3JDLENBQUM7UUFDTyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBRW5DLHlCQUFvQixHQUFnQyxJQUFJLFlBQVksRUFBaUIsQ0FBQztRQUN0RixlQUFVLEdBQVEsd0JBQXdCLENBQUM7UUFJekMsVUFBVTtJQUNaLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQzdCLENBQUM7SUFFRCxXQUFXLENBQUMsUUFBZ0I7UUFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQy9CLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFBLENBQUMsQ0FBQyxZQUFZLENBQUEsQ0FBQyxDQUFDLFlBQVksQ0FBQztJQUM3RSxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBQyxJQUFJLEVBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFDLENBQUMsRUFBQyxDQUFDO0lBQ3JELENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRTtZQUNyQyxPQUFPLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ25HO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsVUFBVSxDQUFDLElBQW1CO1FBQzVCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELE9BQU8sQ0FBQyxHQUFRO1FBQ2QsT0FBTyxHQUFHLEtBQUssSUFBSSxJQUFJLEdBQUcsS0FBSyxTQUFTLENBQUM7SUFDM0MsQ0FBQztJQUVELGFBQWEsQ0FBQyxZQUFrQyxFQUFFLEtBQWE7UUFDN0QsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFM0MsSUFBSSxLQUFLLENBQUM7UUFDVixJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLE1BQU0sSUFBSSxTQUFTLElBQUksTUFBTSxJQUFJLElBQUksRUFBRTtZQUNwRyxLQUFLLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUM5QjthQUFNO1lBQ0wsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNkO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCxNQUFNLENBQUMsY0FBYztRQUNuQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFBO0lBQzlDLENBQUM7SUFFRCxVQUFVLENBQUMsSUFBSTtRQUNiLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFDLElBQUksT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUM7WUFDeEQsT0FBTyxHQUFHLElBQUksQ0FBQztTQUNoQjtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7O3NGQXpFVSxtQkFBbUIsdUJBaUJKLElBQUk7d0RBakJuQixtQkFBbUI7O1FDNUZoQyw4QkFDSTtRQURxQyxpS0FBZ0IsV0FBUyxJQUFDO1FBaUIvRCxtQ0FHQTtRQUg2SywwS0FBMEIsZ0RBQWtELElBQUMsa0dBQy9KLHNCQUFrQixJQUQ2STtRQUExUCxpQkFHQTtRQUFBLDhCQUVKO1FBRjJCLG1KQUFTLFlBQVUsSUFBQztRQUEzQyxpQkFFSjtRQUFBLGlCQUFNOztRQUxLLGVBQXFCO1FBQXJCLHVDQUFxQixpQ0FBQSx5Q0FBQSx3QkFBQSx3QkFBQTs7a0REMkVuQixtQkFBbUI7Y0FML0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLFdBQVcsRUFBRSxtQkFBbUI7Z0JBQ2hDLFNBQVMsRUFBRSxDQUFDLG1CQUFtQixDQUFDO2FBQ2pDO3NDQWtCMkIsSUFBSSx5RUFkckIsY0FBYztrQkFBdEIsS0FBSztZQUNHLE9BQU87a0JBQWYsS0FBSztZQUVOLE9BQU87a0JBRE4sS0FBSztZQU1HLFFBQVE7a0JBQWhCLEtBQUs7WUFFTixvQkFBb0I7a0JBRG5CLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOZ2JEYXRlU3RydWN0LCBOZ2JEYXRlcGlja2VySTE4biwgTmdiRGF0ZVBhcnNlckZvcm1hdHRlciwgTmdiRGF0ZSwgTmdiQ2FsZW5kYXIgfSBmcm9tIFwiQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXBcIjtcbmltcG9ydCB7IERBVEVfRk9STUFUX1dJVEhPVVRfVElNRSB9IGZyb20gXCIuL2RhdGVwaWNrZXIuY29uZmlnXCI7XG5cbmNvbnN0IEkxOE5fVkFMVUVTID0ge1xuICBlbjoge1xuICAgIHdlZWtkYXlzOiBbXG4gICAgICBcIk1vXCIsXG4gICAgICBcIlR1XCIsXG4gICAgICBcIldlXCIsXG4gICAgICBcIlRoXCIsXG4gICAgICBcIkZyXCIsXG4gICAgICBcIlNhXCIsXG4gICAgICBcIlN1XCJcbiAgICBdLFxuICAgIG1vbnRoczogW1xuICAgICAgXCJKYW5cIixcbiAgICAgIFwiRmViXCIsXG4gICAgICBcIk1hclwiLFxuICAgICAgXCJBcHJcIixcbiAgICAgIFwiTWF5XCIsXG4gICAgICBcIkp1blwiLFxuICAgICAgXCJKdWxcIixcbiAgICAgIFwiQXVnXCIsXG4gICAgICBcIlNlcFwiLFxuICAgICAgXCJPY3RcIixcbiAgICAgIFwiTm92XCIsXG4gICAgICBcIkRlY1wiXG4gICAgXVxuICB9LFxuICB2aToge1xuICAgIHdlZWtkYXlzOiBbXG4gICAgICBcIlQyXCIsXG4gICAgICBcIlQzXCIsXG4gICAgICBcIlQ0XCIsXG4gICAgICBcIlQ1XCIsXG4gICAgICBcIlQ2XCIsXG4gICAgICBcIlQ3XCIsXG4gICAgICBcIkNOXCJcbiAgICBdLFxuICAgIG1vbnRoczogW1xuICAgICAgXCJUMVwiLFxuICAgICAgXCJUMlwiLFxuICAgICAgXCJUM1wiLFxuICAgICAgXCJUNFwiLFxuICAgICAgXCJUNVwiLFxuICAgICAgXCJUNlwiLFxuICAgICAgXCJUN1wiLFxuICAgICAgXCJUOFwiLFxuICAgICAgXCJUOVwiLFxuICAgICAgXCJUMTBcIixcbiAgICAgIFwiVDExXCIsXG4gICAgICBcIlQxMlwiXG4gICAgXVxuICB9XG59O1xuXG4vLyBEZWZpbmUgYSBzZXJ2aWNlIGhvbGRpbmcgdGhlIGxhbmd1YWdlLiBZb3UgcHJvYmFibHkgYWxyZWFkeSBoYXZlIG9uZSBpZiB5b3VyIGFwcCBpcyBpMThuZWQuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgSTE4biB7XG4gIGxhbmd1YWdlID0gXCJ2aVwiO1xufVxuXG4vLyBEZWZpbmUgY3VzdG9tIHNlcnZpY2UgcHJvdmlkaW5nIHRoZSBtb250aHMgYW5kIHdlZWtkYXlzIHRyYW5zbGF0aW9uc1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEN1c3RvbURhdGVwaWNrZXJJMThuIGV4dGVuZHMgTmdiRGF0ZXBpY2tlckkxOG4ge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX2kxOG46IEkxOG4pIHtcbiAgICBzdXBlcigpO1xuICB9XG5cbiAgZ2V0V2Vla2RheVNob3J0TmFtZSh3ZWVrZGF5OiBudW1iZXIpOiBzdHJpbmcge1xuICAgIHJldHVybiBJMThOX1ZBTFVFU1t0aGlzLl9pMThuLmxhbmd1YWdlXS53ZWVrZGF5c1t3ZWVrZGF5IC0gMV07XG4gIH1cbiAgZ2V0TW9udGhTaG9ydE5hbWUobW9udGg6IG51bWJlcik6IHN0cmluZyB7XG4gICAgcmV0dXJuIEkxOE5fVkFMVUVTW3RoaXMuX2kxOG4ubGFuZ3VhZ2VdLm1vbnRoc1ttb250aCAtIDFdO1xuICB9XG4gIGdldE1vbnRoRnVsbE5hbWUobW9udGg6IG51bWJlcik6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0TW9udGhTaG9ydE5hbWUobW9udGgpO1xuICB9XG5cbiAgZ2V0RGF5QXJpYUxhYmVsKGRhdGU6IE5nYkRhdGVTdHJ1Y3QpOiBzdHJpbmcge1xuICAgIHJldHVybiBgJHtkYXRlLmRheX0tJHtkYXRlLm1vbnRofS0ke2RhdGUueWVhcn1gO1xuICB9XG59XG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS1kYXRlcGlja2VyXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vZGF0ZXBpY2tlci5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9kYXRlcGlja2VyLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgRGF0ZXBpY2tlckNvbXBvbmVudCB7XG4gIHRvZGF5ID0gbmV3IERhdGUoKTtcbiAgaXB0TW9kZWw7XG4gIEBJbnB1dCgpIGRhdGVwaWNrZXJJdGVtOiBOZ2JEYXRlU3RydWN0O1xuICBASW5wdXQoKSBtYXhEYXRlOiBOZ2JEYXRlU3RydWN0O1xuICBASW5wdXQoKVxuICBtaW5EYXRlOiBOZ2JEYXRlU3RydWN0ID0ge1xuICAgIGRheTogMSxcbiAgICBtb250aDogMSxcbiAgICB5ZWFyOiB0aGlzLnRvZGF5LmdldEZ1bGxZZWFyKCkgLSAxMDBcbiAgfTtcbiAgQElucHV0KCkgZGlzYWJsZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQE91dHB1dCgpXG4gIGRhdGVwaWNrZXJJdGVtQ2hhbmdlOiBFdmVudEVtaXR0ZXI8TmdiRGF0ZVN0cnVjdD4gPSBuZXcgRXZlbnRFbWl0dGVyPE5nYkRhdGVTdHJ1Y3Q+KCk7XG4gIGRhdGVGb3JtYXQ6IGFueSA9IERBVEVfRk9STUFUX1dJVEhPVVRfVElNRTtcbiAgcGxhY2Vob2xkZXI6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgX2kxOG46IEkxOG4sIHB1YmxpYyBmb3JtYXR0ZXI6IE5nYkRhdGVQYXJzZXJGb3JtYXR0ZXIsIHByaXZhdGUgY2FsZW5kYXI6IE5nYkNhbGVuZGFyKSB7XG4gICAgLy9zdXBlcigpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5faTE4bi5sYW5ndWFnZSA9IFwiZW5cIjtcbiAgfVxuXG4gIHNldExhbmd1YWdlKGxhbmd1YWdlOiBzdHJpbmcpIHtcbiAgICB0aGlzLl9pMThuLmxhbmd1YWdlID0gbGFuZ3VhZ2U7XG4gICAgdGhpcy5wbGFjZWhvbGRlciA9IHRoaXMuX2kxOG4ubGFuZ3VhZ2UgPT0gXCJlblwiPyBcImRkL21tL3l5eXlcIjogXCJubi90dC9ubm5uXCI7XG4gIH1cblxuICBzZWxlY3RUb2RheSgpIHtcbiAgICB0aGlzLmRhdGVwaWNrZXJJdGVtID0ge3llYXI6MjAyMSwgbW9udGg6IDYsIGRheTo3fTtcbiAgfVxuXG4gIGdldERhdGVTZWxlY3RlZCgpOiBEYXRlIHtcbiAgICBpZiAodGhpcy5pc1ZhbGlkKHRoaXMuZGF0ZXBpY2tlckl0ZW0pKSB7XG4gICAgICByZXR1cm4gbmV3IERhdGUodGhpcy5kYXRlcGlja2VySXRlbS55ZWFyLCB0aGlzLmRhdGVwaWNrZXJJdGVtLm1vbnRoIC0gMSwgdGhpcy5kYXRlcGlja2VySXRlbS5kYXkpO1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGRhdGVTZWxlY3QoZGF0ZTogTmdiRGF0ZVN0cnVjdCk6IHZvaWQge1xuICAgIHRoaXMuZGF0ZXBpY2tlckl0ZW1DaGFuZ2UuZW1pdChkYXRlKTtcbiAgfVxuXG4gIGlzVmFsaWQob2JqOiBhbnkpOiBib29sZWFuIHtcbiAgICByZXR1cm4gb2JqICE9PSBudWxsICYmIG9iaiAhPT0gdW5kZWZpbmVkO1xuICB9XG5cbiAgdmFsaWRhdGVJbnB1dChjdXJyZW50VmFsdWU6IE5nYkRhdGVTdHJ1Y3QgfCBudWxsLCBpbnB1dDogc3RyaW5nKTogTmdiRGF0ZSB8IG51bGwge1xuICAgIGNvbnN0IHBhcnNlZCA9IHRoaXMuZm9ybWF0dGVyLnBhcnNlKGlucHV0KTtcblxuICAgIGxldCB2YWx1ZTtcbiAgICBpZiAoKHBhcnNlZCAmJiB0aGlzLmNhbGVuZGFyLmlzVmFsaWQoTmdiRGF0ZS5mcm9tKHBhcnNlZCkpKSB8fCBwYXJzZWQgPT0gdW5kZWZpbmVkIHx8IHBhcnNlZCA9PSBudWxsKSB7XG4gICAgICB2YWx1ZSA9IE5nYkRhdGUuZnJvbShwYXJzZWQpO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YWx1ZSA9IG51bGw7XG4gICAgfVxuICAgIHRoaXMuZGF0ZVNlbGVjdCh2YWx1ZSk7XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG5cbiAgZm9ybWF0KGRhdGVwaWNrZXJJdGVtKXtcbiAgICByZXR1cm4gdGhpcy5mb3JtYXR0ZXIuZm9ybWF0KGRhdGVwaWNrZXJJdGVtKVxuICB9XG5cbiAgY2hlY2tFcnJvcihkYXRlKXtcbiAgICBjb25zdCBwYXJzZWQgPSB0aGlzLmZvcm1hdHRlci5wYXJzZShkYXRlKTtcbiAgICBsZXQgaXNWYWxpZCA9IGZhbHNlO1xuICAgIGlmIChwYXJzZWQgJiYgdGhpcy5jYWxlbmRhci5pc1ZhbGlkKE5nYkRhdGUuZnJvbShwYXJzZWQpKSl7XG4gICAgICBpc1ZhbGlkID0gdHJ1ZTtcbiAgICB9XG4gICAgcmV0dXJuIGlzVmFsaWQ7XG4gIH1cbn1cbiIsIjxkaXYgaWQ9XCJ3aW4tZGF0ZXBpY2tlclwiIHRvaENsaWNrT3V0U2l0ZSAoY2xpY2tPdXRTaXRlKT1cImQuY2xvc2UoKVwiPlxuICAgIDwhLS0gPHVsIGNsYXNzPVwiZGF0ZXBpY2tlci1jb250YWluZXJcIiAoY2xpY2spPVwiZC50b2dnbGUoKVwiIFtuZ0NsYXNzXT1cInsnZGlzYWJsZWQtYmFja2dyb3VuZCcgOiBkaXNhYmxlZH1cIj5cbiAgICAgICAgPGxpIGNsYXNzPVwiZGF0ZXBpY2tlclwiPlxuICAgICAgICAgICAgPGltZyBzcmM9XCIuL2Fzc2V0cy9pY29ucy9pY19jYWxlbmRhci5zdmdcIiAvPlxuICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCIhZ2V0RGF0ZVNlbGVjdGVkKClcIj5cbiAgICAgICAgICAgICAgICA8bmctY29udGVudCBzZWxlY3Q9W3RpdGxlXT48L25nLWNvbnRlbnQ+XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiAqbmdJZj1cImdldERhdGVTZWxlY3RlZCgpXCI+XG4gICAgICAgICAgICAgICAge3tnZXREYXRlU2VsZWN0ZWQoKSB8IGRhdGU6ZGF0ZUZvcm1hdH19XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImRyb3Bkb3duLWFycm93XCI+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1kb3duXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2xpPlxuICAgIDwvdWw+XG4gICAgPGlucHV0IFtkaXNhYmxlZF09XCJkaXNhYmxlZFwiIGNsYXNzPVwiaW5wdXQtZGF0ZXBpY2tlclwiIHR5cGU9XCJ0ZXh0XCIgWyhuZ01vZGVsKV09XCJkYXRlcGlja2VySXRlbVwiIG5nYkRhdGVwaWNrZXIgI2Q9XCJuZ2JEYXRlcGlja2VyXCIgW21pbkRhdGVdPVwibWluRGF0ZVwiIFttYXhEYXRlXT1cIm1heERhdGVcIiAoZGF0ZVNlbGVjdCk9XCJkYXRlU2VsZWN0KCRldmVudClcIj4gLS0+XG5cbiAgICA8aW5wdXQgW2Rpc2FibGVkXT1cImRpc2FibGVkXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBwbGFjZWhvbGRlcj1cIkREL01NL1lZWVlcIiBbc3RhcnREYXRlXT1cImRhdGVwaWNrZXJJdGVtXCIgI2RhdGVQaWNrZXJJcHQgbmFtZT1cImRhdGVQaWNrZXJJcHRcIiBbdmFsdWVdPVwiZm9ybWF0KGRhdGVwaWNrZXJJdGVtKVwiIChpbnB1dCk9XCJkYXRlcGlja2VySXRlbSA9IHZhbGlkYXRlSW5wdXQoZGF0ZXBpY2tlckl0ZW0sIGRhdGVQaWNrZXJJcHQudmFsdWUpXCJcbiAgICAgICAgbmdiRGF0ZXBpY2tlciAjZD1cIm5nYkRhdGVwaWNrZXJcIiBbbWluRGF0ZV09XCJtaW5EYXRlXCIgW21heERhdGVdPVwibWF4RGF0ZVwiIChkYXRlU2VsZWN0KT1cImRhdGVTZWxlY3QoJGV2ZW50KVwiPlxuXG4gICAgPGltZyBjbGFzcz1cImRhdGUtaWNvblwiIChjbGljayk9XCJkLnRvZ2dsZSgpXCIgc3JjPVwiLi9hc3NldHMvaWNvbnMvaWNfY2FsZW5kYXIuc3ZnXCIgLz5cblxuPC9kaXY+Il19