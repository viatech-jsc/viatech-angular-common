import { NgModule } from '@angular/core';
import { NgbModule, NgbDatepickerI18n } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from '@angular/common';
import { DatepickerComponent, I18n, CustomDatepickerI18n } from './datepicker';
import * as i0 from "@angular/core";
export class DatepickerModule {
}
DatepickerModule.ɵmod = i0.ɵɵdefineNgModule({ type: DatepickerModule });
DatepickerModule.ɵinj = i0.ɵɵdefineInjector({ factory: function DatepickerModule_Factory(t) { return new (t || DatepickerModule)(); }, providers: [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }], imports: [[
            NgbModule,
            FormsModule,
            CommonModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(DatepickerModule, { declarations: [DatepickerComponent], imports: [NgbModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule], exports: [DatepickerComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DatepickerModule, [{
        type: NgModule,
        args: [{
                declarations: [DatepickerComponent],
                imports: [
                    NgbModule,
                    FormsModule,
                    CommonModule,
                    ReactiveFormsModule
                ],
                exports: [DatepickerComponent],
                providers: [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXBpY2tlci5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9kYXRlcGlja2VyL2RhdGVwaWNrZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLG1CQUFtQixFQUFFLElBQUksRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGNBQWMsQ0FBQzs7QUFhL0UsTUFBTSxPQUFPLGdCQUFnQjs7b0RBQWhCLGdCQUFnQjsrR0FBaEIsZ0JBQWdCLG1CQUZoQixDQUFDLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQyxZQVB4RTtZQUNQLFNBQVM7WUFDVCxXQUFXO1lBQ1gsWUFBWTtZQUNaLG1CQUFtQjtTQUNwQjt3RkFJVSxnQkFBZ0IsbUJBVlosbUJBQW1CLGFBRWhDLFNBQVM7UUFDVCxXQUFXO1FBQ1gsWUFBWTtRQUNaLG1CQUFtQixhQUVYLG1CQUFtQjtrREFHbEIsZ0JBQWdCO2NBWDVCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztnQkFDbkMsT0FBTyxFQUFFO29CQUNQLFNBQVM7b0JBQ1QsV0FBVztvQkFDWCxZQUFZO29CQUNaLG1CQUFtQjtpQkFDcEI7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLENBQUM7Z0JBQzlCLFNBQVMsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQzthQUNsRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOZ2JNb2R1bGUsIE5nYkRhdGVwaWNrZXJJMThuIH0gZnJvbSBcIkBuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwXCI7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IERhdGVwaWNrZXJDb21wb25lbnQsIEkxOG4sIEN1c3RvbURhdGVwaWNrZXJJMThuIH0gZnJvbSAnLi9kYXRlcGlja2VyJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbRGF0ZXBpY2tlckNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBOZ2JNb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW0RhdGVwaWNrZXJDb21wb25lbnRdLFxuICBwcm92aWRlcnM6IFtJMThuLCB7IHByb3ZpZGU6IE5nYkRhdGVwaWNrZXJJMThuLCB1c2VDbGFzczogQ3VzdG9tRGF0ZXBpY2tlckkxOG4gfV1cbn0pXG5leHBvcnQgY2xhc3MgRGF0ZXBpY2tlck1vZHVsZSB7IH1cbiJdfQ==