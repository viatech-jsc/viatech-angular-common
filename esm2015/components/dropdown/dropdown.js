import { Component, Input, Output, EventEmitter } from "@angular/core";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@angular/forms";
import * as i3 from "./dropdown.pipe";
function DropdownComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 12);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵpropertyInterpolate("title", ctx_r0.getSelectedStr(ctx_r0.itemSelected));
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r0.getSelectedStr(ctx_r0.itemSelected), " ");
} }
function DropdownComponent_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r1.placeholder);
} }
function DropdownComponent_div_9_i_2_Template(rf, ctx) { if (rf & 1) {
    const _r7 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "i", 17);
    i0.ɵɵlistener("click", function DropdownComponent_div_9_i_2_Template_i_click_0_listener() { i0.ɵɵrestoreView(_r7); const ctx_r6 = i0.ɵɵnextContext(2); return ctx_r6.clearSearchText(); });
    i0.ɵɵelementEnd();
} }
function DropdownComponent_div_9_img_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "img", 18);
} }
function DropdownComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    const _r9 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 13);
    i0.ɵɵelementStart(1, "input", 14);
    i0.ɵɵlistener("ngModelChange", function DropdownComponent_div_9_Template_input_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r9); const ctx_r8 = i0.ɵɵnextContext(); return ctx_r8.searchText = $event; });
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(2, DropdownComponent_div_9_i_2_Template, 1, 0, "i", 15);
    i0.ɵɵtemplate(3, DropdownComponent_div_9_img_3_Template, 1, 0, "img", 16);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngModel", ctx_r2.searchText);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r2.searchText);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r2.searchText);
} }
const _c0 = function (a0) { return { "selected": a0 }; };
function DropdownComponent_li_10_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "li", 19);
    i0.ɵɵlistener("click", function DropdownComponent_li_10_Template_li_click_0_listener() { i0.ɵɵrestoreView(_r12); const item_r10 = ctx.$implicit; const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.selected(item_r10); });
    i0.ɵɵelementStart(1, "span", 20);
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "span", 21);
    i0.ɵɵelement(4, "i", 22);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r10 = ctx.$implicit;
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(3, _c0, ctx_r3.hasSelected(item_r10)));
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate("title", item_r10.name);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r10.name);
} }
const _c1 = function (a0) { return { "disabled-background": a0 }; };
const _c2 = function (a0) { return { "open": a0 }; };
const _c3 = function (a0) { return { "arrow-rotate": a0 }; };
export class DropdownComponent {
    constructor() {
        this.items = new Array();
        this.itemSelected = new Array();
        this.multiple = false;
        this.disabled = false;
        this.placeholder = "";
        this.itemSelectedChange = new EventEmitter();
        this.isShow = false;
        //super();
    }
    isItemSelectedValid() {
        return this.itemSelected !== null && this.itemSelected !== undefined && this.itemSelected.length > 0;
    }
    toggle() {
        if (!this.disabled) {
            this.isShow = !this.isShow;
        }
    }
    hide() {
        this.isShow = false;
    }
    show() {
        this.isShow = true;
    }
    hasSelected(item) {
        let itemsFound = this.itemSelected.filter((i) => { return i.value === item.value; });
        return itemsFound.length > 0;
    }
    selected(item) {
        this.searchText = "";
        if (this.multiple) {
            let itemsFound = this.itemSelected.filter((i) => { return i.value === item.value; });
            if (itemsFound.length === 0) {
                this.itemSelected.push(item);
            }
            else {
                this.itemSelected = this.itemSelected.filter((i) => { return i.value !== item.value; });
            }
        }
        else {
            this.hide();
            this.itemSelected = [];
            this.itemSelected.push(item);
        }
        this.itemSelectedChange.emit(this.itemSelected);
    }
    clearSearchText() {
        this.searchText = "";
    }
    getSelectedStr(itemSelected) {
        let str = "";
        if (itemSelected) {
            let i = 0;
            itemSelected.forEach(item => {
                i = i + 1;
                str = str + item.name + (i < itemSelected.length ? ", " : "");
            });
        }
        return str;
    }
}
DropdownComponent.ɵfac = function DropdownComponent_Factory(t) { return new (t || DropdownComponent)(); };
DropdownComponent.ɵcmp = i0.ɵɵdefineComponent({ type: DropdownComponent, selectors: [["via-dropdown"]], inputs: { items: "items", itemSelected: "itemSelected", multiple: "multiple", disabled: "disabled", placeholder: "placeholder" }, outputs: { itemSelectedChange: "itemSelectedChange" }, decls: 13, vars: 20, consts: [["id", "dropdown"], [1, "dropdown-container", 3, "ngClass"], [1, "dropdown", 3, "ngClass"], [1, "dropdown-selectedBox", 3, "click"], ["class", "text-box", 3, "title", 4, "ngIf"], [4, "ngIf"], [1, "dropdown-arrow"], ["aria-hidden", "true", 1, "fa", "fa-angle-down", "arrow", 3, "ngClass"], [1, "dropdown-menu-items", "z-depth-1"], ["class", "dropdown-search-bar", 4, "ngIf"], ["class", "dropdown-menu-item", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [1, "dropdown-backdrop", 3, "ngClass", "click"], [1, "text-box", 3, "title"], [1, "dropdown-search-bar"], [1, "dropdown-search-input", 3, "ngModel", "ngModelChange"], ["class", "fa fa-times clear-search-input", "aria-hidden", "true", 3, "click", 4, "ngIf"], ["class", "input-search-icon dropdown-search-icon", "src", "/assets/icons/ic_search.svg", 4, "ngIf"], ["aria-hidden", "true", 1, "fa", "fa-times", "clear-search-input", 3, "click"], ["src", "/assets/icons/ic_search.svg", 1, "input-search-icon", "dropdown-search-icon"], [1, "dropdown-menu-item", 3, "ngClass", "click"], [1, "text-no-wrap", 3, "title"], [1, "item-check"], ["aria-hidden", "true", 1, "fa", "fa-check"]], template: function DropdownComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "ul", 1);
        i0.ɵɵelementStart(2, "li", 2);
        i0.ɵɵelementStart(3, "div", 3);
        i0.ɵɵlistener("click", function DropdownComponent_Template_div_click_3_listener() { return ctx.toggle(); });
        i0.ɵɵtemplate(4, DropdownComponent_div_4_Template, 2, 2, "div", 4);
        i0.ɵɵtemplate(5, DropdownComponent_span_5_Template, 2, 1, "span", 5);
        i0.ɵɵelementStart(6, "span", 6);
        i0.ɵɵelement(7, "i", 7);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "ul", 8);
        i0.ɵɵtemplate(9, DropdownComponent_div_9_Template, 4, 3, "div", 9);
        i0.ɵɵtemplate(10, DropdownComponent_li_10_Template, 5, 5, "li", 10);
        i0.ɵɵpipe(11, "fullTextSearch");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(12, "div", 11);
        i0.ɵɵlistener("click", function DropdownComponent_Template_div_click_12_listener() { return ctx.hide(); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(12, _c1, ctx.disabled));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(14, _c2, ctx.isShow));
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.isItemSelectedValid());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.isItemSelectedValid());
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(16, _c3, ctx.isShow));
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.items.length > 15);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngForOf", i0.ɵɵpipeBind3(11, 8, ctx.items, "name", ctx.searchText));
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(18, _c2, ctx.isShow));
    } }, directives: [i1.NgClass, i1.NgIf, i1.NgForOf, i2.DefaultValueAccessor, i2.NgControlStatus, i2.NgModel], pipes: [i3.FullTextSearchPipe], styles: ["#dropdown[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:row;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]{position:-webkit-sticky;position:sticky;top:0}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]   .dropdown-search-input[_ngcontent-%COMP%]{border:.5px solid #dedede;border-left:0;border-right:0;border-top:0;font-size:13px;height:45px;padding-left:10px;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]   .clear-search-input[_ngcontent-%COMP%]{color:#d9e0e5;position:absolute;right:13px;top:13px}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]   .dropdown-search-icon[_ngcontent-%COMP%]{position:absolute;right:13px;top:13px;width:20px}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:9;list-style:none;margin:0;padding:0;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]{display:flex;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown.open[_ngcontent-%COMP%]   .dropdown-backdrop[_ngcontent-%COMP%], #dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown.open[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{display:block}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:none;list-style:none;margin:5px 0 38px;max-height:400px;overflow:auto;padding:0;position:absolute;top:100%;width:100%;z-index:4}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]{align-items:center;display:flex;flex:9;justify-content:flex-start;padding:10px}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]:hover{background:#f7f8f9}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item.selected[_ngcontent-%COMP%]   .item-check[_ngcontent-%COMP%]{color:#2a7af6;display:block}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]   .text-no-wrap[_ngcontent-%COMP%]{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]   .item-check[_ngcontent-%COMP%]{color:#bcbec0;display:none;flex:1;text-align:right}#dropdown[_ngcontent-%COMP%]   .arrow-rotate[_ngcontent-%COMP%]{transform:rotate(-180deg)}#dropdown[_ngcontent-%COMP%]   .arrow[_ngcontent-%COMP%]{transition:.2s ease}#dropdown[_ngcontent-%COMP%]   .dropdown-selectedBox[_ngcontent-%COMP%]{padding:10px;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-selectedBox[_ngcontent-%COMP%]   .text-box[_ngcontent-%COMP%]{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:90%}#dropdown[_ngcontent-%COMP%]   .dropdown-arrow[_ngcontent-%COMP%]{flex:1;height:46px;line-height:46px;position:absolute;right:10px;text-align:center;top:2px}#dropdown[_ngcontent-%COMP%]   .dropdown-arrow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#999;font-size:24px}#dropdown[_ngcontent-%COMP%]   .dropdown-backdrop[_ngcontent-%COMP%]{display:none;height:100%;left:0;position:fixed;top:0;width:100%;z-index:2}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DropdownComponent, [{
        type: Component,
        args: [{
                selector: "via-dropdown",
                templateUrl: "./dropdown.html",
                styleUrls: ["./dropdown.scss"]
            }]
    }], function () { return []; }, { items: [{
            type: Input
        }], itemSelected: [{
            type: Input
        }], multiple: [{
            type: Input
        }], disabled: [{
            type: Input
        }], placeholder: [{
            type: Input
        }], itemSelectedChange: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9kcm9wZG93bi9kcm9wZG93bi50cyIsImNvbXBvbmVudHMvZHJvcGRvd24vZHJvcGRvd24uaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7SUNJdkQsK0JBQ0k7SUFBQSxZQUNKO0lBQUEsaUJBQU07OztJQUY2Qiw2RUFBd0M7SUFDdkUsZUFDSjtJQURJLDJFQUNKOzs7SUFDQSw0QkFBcUM7SUFBQSxZQUFlO0lBQUEsaUJBQU87OztJQUF0QixlQUFlO0lBQWYsd0NBQWU7Ozs7SUFRaEQsNkJBQWdIO0lBQW5ELDBMQUEyQjtJQUFvQixpQkFBSTs7O0lBQ2hILDBCQUEwRzs7OztJQUY5RywrQkFBMEQ7SUFBQSxpQ0FDdEQ7SUFENkQsaU5BQXdCO0lBQS9CLGlCQUN0RDtJQUFBLHFFQUE0RztJQUM1Ryx5RUFBMEc7SUFBQSxpQkFBTTs7O0lBRm5ELGVBQXdCO0lBQXhCLDJDQUF3QjtJQUNsRixlQUFrQjtJQUFsQix3Q0FBa0I7SUFDaEIsZUFBbUI7SUFBbkIseUNBQW1COzs7OztJQUM1Qiw4QkFDSTtJQUR1SSwwTkFBd0I7SUFDL0osZ0NBQWlEO0lBQUEsWUFBYTtJQUFBLGlCQUFPO0lBQ3JFLGdDQUNJO0lBQUEsd0JBQThDO0lBQ2xELGlCQUFPO0lBQ1gsaUJBQUs7Ozs7SUFMd0Ysa0ZBQTZDO0lBQzNHLGVBQXFCO0lBQXJCLGdEQUFxQjtJQUFDLGVBQWE7SUFBYixtQ0FBYTs7Ozs7QURWbEYsTUFBTSxPQUFPLGlCQUFpQjtJQVM1QjtRQVJTLFVBQUssR0FBbUIsSUFBSSxLQUFLLEVBQWdCLENBQUM7UUFDbEQsaUJBQVksR0FBbUIsSUFBSSxLQUFLLEVBQWdCLENBQUM7UUFDekQsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUMxQixhQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGdCQUFXLEdBQVcsRUFBRSxDQUFDO1FBQ3hCLHVCQUFrQixHQUFpQyxJQUFJLFlBQVksRUFBa0IsQ0FBQztRQUNoRyxXQUFNLEdBQVksS0FBSyxDQUFDO1FBR3RCLFVBQVU7SUFDWixDQUFDO0lBRUQsbUJBQW1CO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFlBQVksS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZHLENBQUM7SUFFRCxNQUFNO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7U0FDNUI7SUFDSCxDQUFDO0lBRUQsSUFBSTtRQUNGLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxJQUFJO1FBQ0YsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVELFdBQVcsQ0FBQyxJQUFrQjtRQUM1QixJQUFJLFVBQVUsR0FBbUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLE9BQU8sQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEcsT0FBTyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsUUFBUSxDQUFDLElBQWtCO1FBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLFVBQVUsR0FBbUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLE9BQU8sQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEcsSUFBSSxVQUFVLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDM0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDOUI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsT0FBTyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN4RjtTQUNGO2FBQU07WUFDTCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDWixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5QjtRQUNELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELGNBQWMsQ0FBQyxZQUE0QjtRQUN6QyxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7UUFDYixJQUFJLFlBQVksRUFBRTtZQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDVixZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMxQixDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDVixHQUFHLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQSxDQUFDLENBQUMsSUFBSSxDQUFBLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUM5RCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBRUQsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDOztrRkFwRVUsaUJBQWlCO3NEQUFqQixpQkFBaUI7UUNSOUIsOEJBQ0k7UUFBQSw2QkFDSTtRQUFBLDZCQUNJO1FBQUEsOEJBQ0k7UUFEOEIsMkZBQVMsWUFBUSxJQUFDO1FBQ2hELGtFQUNJO1FBRUosb0VBQXFDO1FBQ3JDLCtCQUNJO1FBQUEsdUJBQStGO1FBQ25HLGlCQUFPO1FBQ1gsaUJBQU07UUFFTiw2QkFDSTtRQUFBLGtFQUEwRDtRQUcxRCxtRUFDSTs7UUFLUixpQkFBSztRQUNMLGdDQUFvRjtRQUEvRSw0RkFBUyxVQUFNLElBQUM7UUFBeUQsaUJBQU07UUFDeEYsaUJBQUs7UUFDVCxpQkFBSztRQUNULGlCQUFNOztRQTFCNkIsZUFBOEM7UUFBOUMsbUVBQThDO1FBQ3BELGVBQTZCO1FBQTdCLGlFQUE2QjtRQUVyQyxlQUE2QjtRQUE3QixnREFBNkI7UUFHNUIsZUFBOEI7UUFBOUIsaURBQThCO1FBRUUsZUFBcUM7UUFBckMsaUVBQXFDO1FBS3RFLGVBQXVCO1FBQXZCLDRDQUF1QjtRQUd4QixlQUE2RDtRQUE3RCxrRkFBNkQ7UUFPL0MsZUFBNkI7UUFBN0IsaUVBQTZCOztrRERoQmxELGlCQUFpQjtjQUw3QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLFdBQVcsRUFBRSxpQkFBaUI7Z0JBQzlCLFNBQVMsRUFBRSxDQUFDLGlCQUFpQixDQUFDO2FBQy9CO3NDQUVVLEtBQUs7a0JBQWIsS0FBSztZQUNHLFlBQVk7a0JBQXBCLEtBQUs7WUFDRyxRQUFRO2tCQUFoQixLQUFLO1lBQ0csUUFBUTtrQkFBaEIsS0FBSztZQUNHLFdBQVc7a0JBQW5CLEtBQUs7WUFDSSxrQkFBa0I7a0JBQTNCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBEcm9wZG93bkl0ZW0gfSBmcm9tIFwiLi9kcm9wZG93bi5pdGVtXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtZHJvcGRvd25cIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9kcm9wZG93bi5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9kcm9wZG93bi5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIERyb3Bkb3duQ29tcG9uZW50IHtcbiAgQElucHV0KCkgaXRlbXM6IERyb3Bkb3duSXRlbVtdID0gbmV3IEFycmF5PERyb3Bkb3duSXRlbT4oKTtcbiAgQElucHV0KCkgaXRlbVNlbGVjdGVkOiBEcm9wZG93bkl0ZW1bXSA9IG5ldyBBcnJheTxEcm9wZG93bkl0ZW0+KCk7XG4gIEBJbnB1dCgpIG11bHRpcGxlOiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpIGRpc2FibGVkOiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyOiBzdHJpbmcgPSBcIlwiO1xuICBAT3V0cHV0KCkgaXRlbVNlbGVjdGVkQ2hhbmdlOiBFdmVudEVtaXR0ZXI8RHJvcGRvd25JdGVtW10+ID0gbmV3IEV2ZW50RW1pdHRlcjxEcm9wZG93bkl0ZW1bXT4oKTtcbiAgaXNTaG93OiBib29sZWFuID0gZmFsc2U7XG4gIHNlYXJjaFRleHQ6IHN0cmluZztcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgLy9zdXBlcigpO1xuICB9XG5cbiAgaXNJdGVtU2VsZWN0ZWRWYWxpZCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5pdGVtU2VsZWN0ZWQgIT09IG51bGwgJiYgdGhpcy5pdGVtU2VsZWN0ZWQgIT09IHVuZGVmaW5lZCAmJiB0aGlzLml0ZW1TZWxlY3RlZC5sZW5ndGggPiAwO1xuICB9XG5cbiAgdG9nZ2xlKCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5kaXNhYmxlZCkge1xuICAgICAgdGhpcy5pc1Nob3cgPSAhdGhpcy5pc1Nob3c7XG4gICAgfVxuICB9XG5cbiAgaGlkZSgpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2hvdyA9IGZhbHNlO1xuICB9XG5cbiAgc2hvdygpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2hvdyA9IHRydWU7XG4gIH1cblxuICBoYXNTZWxlY3RlZChpdGVtOiBEcm9wZG93bkl0ZW0pOiBib29sZWFuIHtcbiAgICBsZXQgaXRlbXNGb3VuZDogRHJvcGRvd25JdGVtW10gPSB0aGlzLml0ZW1TZWxlY3RlZC5maWx0ZXIoKGkpID0+IHsgcmV0dXJuIGkudmFsdWUgPT09IGl0ZW0udmFsdWUgfSk7XG4gICAgcmV0dXJuIGl0ZW1zRm91bmQubGVuZ3RoID4gMDtcbiAgfVxuXG4gIHNlbGVjdGVkKGl0ZW06IERyb3Bkb3duSXRlbSk6IHZvaWQge1xuICAgIHRoaXMuc2VhcmNoVGV4dCA9IFwiXCI7XG4gICAgaWYgKHRoaXMubXVsdGlwbGUpIHtcbiAgICAgIGxldCBpdGVtc0ZvdW5kOiBEcm9wZG93bkl0ZW1bXSA9IHRoaXMuaXRlbVNlbGVjdGVkLmZpbHRlcigoaSkgPT4geyByZXR1cm4gaS52YWx1ZSA9PT0gaXRlbS52YWx1ZSB9KTtcbiAgICAgIGlmIChpdGVtc0ZvdW5kLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICB0aGlzLml0ZW1TZWxlY3RlZC5wdXNoKGl0ZW0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5pdGVtU2VsZWN0ZWQgPSB0aGlzLml0ZW1TZWxlY3RlZC5maWx0ZXIoKGkpID0+IHsgcmV0dXJuIGkudmFsdWUgIT09IGl0ZW0udmFsdWUgfSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuaGlkZSgpO1xuICAgICAgdGhpcy5pdGVtU2VsZWN0ZWQgPSBbXTtcbiAgICAgIHRoaXMuaXRlbVNlbGVjdGVkLnB1c2goaXRlbSk7XG4gICAgfVxuICAgIHRoaXMuaXRlbVNlbGVjdGVkQ2hhbmdlLmVtaXQodGhpcy5pdGVtU2VsZWN0ZWQpO1xuICB9XG5cbiAgY2xlYXJTZWFyY2hUZXh0KCk6IHZvaWQge1xuICAgIHRoaXMuc2VhcmNoVGV4dCA9IFwiXCI7XG4gIH1cblxuICBnZXRTZWxlY3RlZFN0cihpdGVtU2VsZWN0ZWQ6IERyb3Bkb3duSXRlbVtdKTogc3RyaW5nIHtcbiAgICBsZXQgc3RyID0gXCJcIjtcbiAgICBpZiAoaXRlbVNlbGVjdGVkKSB7XG4gICAgICBsZXQgaSA9IDA7XG4gICAgICBpdGVtU2VsZWN0ZWQuZm9yRWFjaChpdGVtID0+IHtcbiAgICAgICAgaSA9IGkgKyAxO1xuICAgICAgICBzdHIgPSBzdHIgKyBpdGVtLm5hbWUgKyAoaSA8IGl0ZW1TZWxlY3RlZC5sZW5ndGg/IFwiLCBcIjogXCJcIik7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICByZXR1cm4gc3RyO1xuICB9XG59XG4iLCI8ZGl2IGlkPVwiZHJvcGRvd25cIj5cbiAgICA8dWwgY2xhc3M9XCJkcm9wZG93bi1jb250YWluZXJcIiBbbmdDbGFzc109XCJ7J2Rpc2FibGVkLWJhY2tncm91bmQnIDogZGlzYWJsZWR9XCI+XG4gICAgICAgIDxsaSBjbGFzcz1cImRyb3Bkb3duXCIgW25nQ2xhc3NdPVwieydvcGVuJyA6IGlzU2hvd31cIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93bi1zZWxlY3RlZEJveFwiIChjbGljayk9XCJ0b2dnbGUoKVwiPlxuICAgICAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJpc0l0ZW1TZWxlY3RlZFZhbGlkKClcIiB0aXRsZT1cInt7Z2V0U2VsZWN0ZWRTdHIoaXRlbVNlbGVjdGVkKX19XCIgY2xhc3M9XCJ0ZXh0LWJveFwiPlxuICAgICAgICAgICAgICAgICAgICB7e2dldFNlbGVjdGVkU3RyKGl0ZW1TZWxlY3RlZCl9fVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiIWlzSXRlbVNlbGVjdGVkVmFsaWQoKVwiPnt7cGxhY2Vob2xkZXJ9fTwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImRyb3Bkb3duLWFycm93XCI+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtYW5nbGUtZG93biBhcnJvd1wiIFtuZ0NsYXNzXT1cInsnYXJyb3ctcm90YXRlJyA6IGlzU2hvd31cIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XG4gICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDx1bCBjbGFzcz1cImRyb3Bkb3duLW1lbnUtaXRlbXMgei1kZXB0aC0xXCI+XG4gICAgICAgICAgICAgICAgPGRpdiAqbmdJZj1cIml0ZW1zLmxlbmd0aD4xNVwiIGNsYXNzPVwiZHJvcGRvd24tc2VhcmNoLWJhclwiPiA8aW5wdXQgWyhuZ01vZGVsKV09XCJzZWFyY2hUZXh0XCIgY2xhc3M9XCJkcm9wZG93bi1zZWFyY2gtaW5wdXRcIj5cbiAgICAgICAgICAgICAgICAgICAgPGkgKm5nSWY9XCJzZWFyY2hUZXh0XCIgY2xhc3M9XCJmYSBmYS10aW1lcyBjbGVhci1zZWFyY2gtaW5wdXRcIiAoY2xpY2spPVwiY2xlYXJTZWFyY2hUZXh0KClcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgIDxpbWcgKm5nSWY9XCIhc2VhcmNoVGV4dFwiIGNsYXNzPVwiaW5wdXQtc2VhcmNoLWljb24gZHJvcGRvd24tc2VhcmNoLWljb25cIiBzcmM9XCIvYXNzZXRzL2ljb25zL2ljX3NlYXJjaC5zdmdcIj48L2Rpdj5cbiAgICAgICAgICAgICAgICA8bGkgKm5nRm9yPVwibGV0IGl0ZW0gb2YgaXRlbXMgfCBmdWxsVGV4dFNlYXJjaDonbmFtZSc6c2VhcmNoVGV4dFwiIGNsYXNzPVwiZHJvcGRvd24tbWVudS1pdGVtXCIgW25nQ2xhc3NdPVwieydzZWxlY3RlZCc6IChoYXNTZWxlY3RlZChpdGVtKSl9XCIgKGNsaWNrKT1cInNlbGVjdGVkKGl0ZW0pXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwidGV4dC1uby13cmFwXCIgdGl0bGU9XCJ7e2l0ZW0ubmFtZX19XCI+e3tpdGVtLm5hbWV9fTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpdGVtLWNoZWNrXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLWNoZWNrXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICA8ZGl2IChjbGljayk9XCJoaWRlKClcIiBbbmdDbGFzc109XCJ7J29wZW4nIDogaXNTaG93fVwiIGNsYXNzPVwiZHJvcGRvd24tYmFja2Ryb3BcIj48L2Rpdj5cbiAgICAgICAgPC9saT5cbiAgICA8L3VsPlxuPC9kaXY+Il19