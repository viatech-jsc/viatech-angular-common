import { NgModule } from '@angular/core';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from '@angular/common';
import { DropdownComponent } from './dropdown';
import { FullTextSearchPipe } from './dropdown.pipe';
import * as i0 from "@angular/core";
export class DropdownModule {
}
DropdownModule.ɵmod = i0.ɵɵdefineNgModule({ type: DropdownModule });
DropdownModule.ɵinj = i0.ɵɵdefineInjector({ factory: function DropdownModule_Factory(t) { return new (t || DropdownModule)(); }, imports: [[
            NgbModule,
            FormsModule,
            CommonModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(DropdownModule, { declarations: [DropdownComponent, FullTextSearchPipe], imports: [NgbModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule], exports: [DropdownComponent, FullTextSearchPipe] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DropdownModule, [{
        type: NgModule,
        args: [{
                declarations: [DropdownComponent, FullTextSearchPipe],
                imports: [
                    NgbModule,
                    FormsModule,
                    CommonModule,
                    ReactiveFormsModule
                ],
                exports: [DropdownComponent, FullTextSearchPipe]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZHJvcGRvd24vZHJvcGRvd24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBQy9DLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGlCQUFpQixDQUFDOztBQVlyRCxNQUFNLE9BQU8sY0FBYzs7a0RBQWQsY0FBYzsyR0FBZCxjQUFjLGtCQVJoQjtZQUNQLFNBQVM7WUFDVCxXQUFXO1lBQ1gsWUFBWTtZQUNaLG1CQUFtQjtTQUNwQjt3RkFHVSxjQUFjLG1CQVRWLGlCQUFpQixFQUFFLGtCQUFrQixhQUVsRCxTQUFTO1FBQ1QsV0FBVztRQUNYLFlBQVk7UUFDWixtQkFBbUIsYUFFWCxpQkFBaUIsRUFBRSxrQkFBa0I7a0RBRXBDLGNBQWM7Y0FWMUIsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLGlCQUFpQixFQUFFLGtCQUFrQixDQUFDO2dCQUNyRCxPQUFPLEVBQUU7b0JBQ1AsU0FBUztvQkFDVCxXQUFXO29CQUNYLFlBQVk7b0JBQ1osbUJBQW1CO2lCQUNwQjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxrQkFBa0IsQ0FBQzthQUNqRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOZ2JNb2R1bGUgfSBmcm9tIFwiQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXBcIjtcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRHJvcGRvd25Db21wb25lbnQgfSBmcm9tICcuL2Ryb3Bkb3duJztcbmltcG9ydCB7IEZ1bGxUZXh0U2VhcmNoUGlwZSB9IGZyb20gJy4vZHJvcGRvd24ucGlwZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW0Ryb3Bkb3duQ29tcG9uZW50LCBGdWxsVGV4dFNlYXJjaFBpcGVdLFxuICBpbXBvcnRzOiBbXG4gICAgTmdiTW9kdWxlLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIENvbW1vbk1vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtEcm9wZG93bkNvbXBvbmVudCwgRnVsbFRleHRTZWFyY2hQaXBlXVxufSlcbmV4cG9ydCBjbGFzcyBEcm9wZG93bk1vZHVsZSB7IH1cbiJdfQ==