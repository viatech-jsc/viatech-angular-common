import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class FullTextSearchPipe {
    constructor() { }
    transform(value, keys, term) {
        if (!term)
            return value;
        return (value || []).filter(item => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
    }
}
FullTextSearchPipe.ɵfac = function FullTextSearchPipe_Factory(t) { return new (t || FullTextSearchPipe)(); };
FullTextSearchPipe.ɵpipe = i0.ɵɵdefinePipe({ name: "fullTextSearch", type: FullTextSearchPipe, pure: false });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FullTextSearchPipe, [{
        type: Pipe,
        args: [{
                name: 'fullTextSearch',
                pure: false
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL2Ryb3Bkb3duL2Ryb3Bkb3duLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7O0FBTXBELE1BQU0sT0FBTyxrQkFBa0I7SUFFM0IsZ0JBQWdCLENBQUM7SUFFVixTQUFTLENBQUMsS0FBSyxFQUFFLElBQVksRUFBRSxJQUFZO1FBQzlDLElBQUksQ0FBQyxJQUFJO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFDeEIsT0FBTyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDekksQ0FBQzs7b0ZBUFEsa0JBQWtCOzJFQUFsQixrQkFBa0I7a0RBQWxCLGtCQUFrQjtjQUo5QixJQUFJO2VBQUM7Z0JBQ0YsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsSUFBSSxFQUFFLEtBQUs7YUFDZCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICAgIG5hbWU6ICdmdWxsVGV4dFNlYXJjaCcsXG4gICAgcHVyZTogZmFsc2Vcbn0pXG5leHBvcnQgY2xhc3MgRnVsbFRleHRTZWFyY2hQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gICAgcHVibGljIHRyYW5zZm9ybSh2YWx1ZSwga2V5czogc3RyaW5nLCB0ZXJtOiBzdHJpbmcpIHtcbiAgICAgICAgaWYgKCF0ZXJtKSByZXR1cm4gdmFsdWU7XG4gICAgICAgIHJldHVybiAodmFsdWUgfHwgW10pLmZpbHRlcihpdGVtID0+IGtleXMuc3BsaXQoJywnKS5zb21lKGtleSA9PiBpdGVtLmhhc093blByb3BlcnR5KGtleSkgJiYgbmV3IFJlZ0V4cCh0ZXJtLCAnZ2knKS50ZXN0KGl0ZW1ba2V5XSkpKTtcbiAgICB9XG59Il19