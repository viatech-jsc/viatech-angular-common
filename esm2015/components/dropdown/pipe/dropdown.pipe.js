import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class FullTextSearchPipe {
    constructor() { }
    transform(value, keys, term) {
        if (!term)
            return value;
        return (value || []).filter(item => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
    }
}
FullTextSearchPipe.ɵfac = function FullTextSearchPipe_Factory(t) { return new (t || FullTextSearchPipe)(); };
FullTextSearchPipe.ɵpipe = i0.ɵɵdefinePipe({ name: "fullTextSearch", type: FullTextSearchPipe, pure: false });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FullTextSearchPipe, [{
        type: Pipe,
        args: [{
                name: 'fullTextSearch',
                pure: false
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvZHJvcGRvd24vcGlwZS9kcm9wZG93bi5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDOztBQU1wRCxNQUFNLE9BQU8sa0JBQWtCO0lBRTNCLGdCQUFnQixDQUFDO0lBRVYsU0FBUyxDQUFDLEtBQUssRUFBRSxJQUFZLEVBQUUsSUFBWTtRQUM5QyxJQUFJLENBQUMsSUFBSTtZQUFFLE9BQU8sS0FBSyxDQUFDO1FBQ3hCLE9BQU8sQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pJLENBQUM7O29GQVBRLGtCQUFrQjsyRUFBbEIsa0JBQWtCO2tEQUFsQixrQkFBa0I7Y0FKOUIsSUFBSTtlQUFDO2dCQUNGLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLElBQUksRUFBRSxLQUFLO2FBQ2QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgICBuYW1lOiAnZnVsbFRleHRTZWFyY2gnLFxuICAgIHB1cmU6IGZhbHNlXG59KVxuZXhwb3J0IGNsYXNzIEZ1bGxUZXh0U2VhcmNoUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIHB1YmxpYyB0cmFuc2Zvcm0odmFsdWUsIGtleXM6IHN0cmluZywgdGVybTogc3RyaW5nKSB7XG4gICAgICAgIGlmICghdGVybSkgcmV0dXJuIHZhbHVlO1xuICAgICAgICByZXR1cm4gKHZhbHVlIHx8IFtdKS5maWx0ZXIoaXRlbSA9PiBrZXlzLnNwbGl0KCcsJykuc29tZShrZXkgPT4gaXRlbS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIG5ldyBSZWdFeHAodGVybSwgJ2dpJykudGVzdChpdGVtW2tleV0pKSk7XG4gICAgfVxufSJdfQ==