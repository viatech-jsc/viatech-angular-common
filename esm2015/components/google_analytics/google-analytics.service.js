import { Injectable } from '@angular/core';
import { NavigationEnd } from '@angular/router'; // import Router and NavigationEnd
import * as i0 from "@angular/core";
export class GoogleAnalyticsService {
    //************************
    // Import this to app.component.ts
    // subscribe to router events and send page views to Google Analytics
    // this.router.events.subscribe(event => {
    //     GoogleAnalyticsService.subscribeRouterEvents(event);
    //   });
    //************************
    constructor() { }
    // /**
    //   * Emit google analytics event
    //   * Fire event example:
    //   * this.emitEvent("testCategory", "testAction", "testLabel", 10);
    //   * param {string} eventCategory
    //   * param {string} eventAction
    //   * param {string} eventLabel
    //   * param {number} eventValue
    //   */
    eventEmitter(eventCategory, eventAction, eventLabel = null, eventValue = null) {
        ga('send', 'event', {
            eventCategory: eventCategory,
            eventLabel: eventLabel,
            eventAction: eventAction,
            eventValue: eventValue
        });
    }
    subscribeRouterEvents(event) {
        if (event instanceof NavigationEnd) {
            ga('set', 'page', event.urlAfterRedirects);
            ga('send', 'pageview');
        }
    }
}
GoogleAnalyticsService.ɵfac = function GoogleAnalyticsService_Factory(t) { return new (t || GoogleAnalyticsService)(); };
GoogleAnalyticsService.ɵprov = i0.ɵɵdefineInjectable({ token: GoogleAnalyticsService, factory: GoogleAnalyticsService.ɵfac });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(GoogleAnalyticsService, [{
        type: Injectable
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLWFuYWx5dGljcy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvZ29vZ2xlX2FuYWx5dGljcy9nb29nbGUtYW5hbHl0aWNzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0saUJBQWlCLENBQUMsQ0FBQyxrQ0FBa0M7O0FBSW5GLE1BQU0sT0FBTyxzQkFBc0I7SUFHL0IsMEJBQTBCO0lBQzFCLGtDQUFrQztJQUNsQyxxRUFBcUU7SUFDckUsMENBQTBDO0lBQzFDLDJEQUEyRDtJQUMzRCxRQUFRO0lBQ1IsMEJBQTBCO0lBQzFCLGdCQUFnQixDQUFDO0lBRWpCLE1BQU07SUFDTixrQ0FBa0M7SUFDbEMsMEJBQTBCO0lBQzFCLHFFQUFxRTtJQUNyRSxtQ0FBbUM7SUFDbkMsaUNBQWlDO0lBQ2pDLGdDQUFnQztJQUNoQyxnQ0FBZ0M7SUFDaEMsT0FBTztJQUNBLFlBQVksQ0FBQyxhQUFxQixFQUNyQyxXQUFtQixFQUNuQixhQUFxQixJQUFJLEVBQ3pCLGFBQXFCLElBQUk7UUFDekIsRUFBRSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUU7WUFDaEIsYUFBYSxFQUFFLGFBQWE7WUFDNUIsVUFBVSxFQUFFLFVBQVU7WUFDdEIsV0FBVyxFQUFFLFdBQVc7WUFDeEIsVUFBVSxFQUFFLFVBQVU7U0FDekIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHFCQUFxQixDQUFDLEtBQVU7UUFDNUIsSUFBSSxLQUFLLFlBQVksYUFBYSxFQUFFO1lBQ2hDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQzNDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7U0FDMUI7SUFDTCxDQUFDOzs0RkF0Q1Esc0JBQXNCOzhEQUF0QixzQkFBc0IsV0FBdEIsc0JBQXNCO2tEQUF0QixzQkFBc0I7Y0FEbEMsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5hdmlnYXRpb25FbmQgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInOyAvLyBpbXBvcnQgUm91dGVyIGFuZCBOYXZpZ2F0aW9uRW5kXG5kZWNsYXJlIGxldCBnYTogRnVuY3Rpb247IC8vIERlY2xhcmUgZ2EgYXMgYSBmdW5jdGlvblxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgR29vZ2xlQW5hbHl0aWNzU2VydmljZSB7XG5cblxuICAgIC8vKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgLy8gSW1wb3J0IHRoaXMgdG8gYXBwLmNvbXBvbmVudC50c1xuICAgIC8vIHN1YnNjcmliZSB0byByb3V0ZXIgZXZlbnRzIGFuZCBzZW5kIHBhZ2Ugdmlld3MgdG8gR29vZ2xlIEFuYWx5dGljc1xuICAgIC8vIHRoaXMucm91dGVyLmV2ZW50cy5zdWJzY3JpYmUoZXZlbnQgPT4ge1xuICAgIC8vICAgICBHb29nbGVBbmFseXRpY3NTZXJ2aWNlLnN1YnNjcmliZVJvdXRlckV2ZW50cyhldmVudCk7XG4gICAgLy8gICB9KTtcbiAgICAvLyoqKioqKioqKioqKioqKioqKioqKioqKlxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgICAvLyAvKipcbiAgICAvLyAgICogRW1pdCBnb29nbGUgYW5hbHl0aWNzIGV2ZW50XG4gICAgLy8gICAqIEZpcmUgZXZlbnQgZXhhbXBsZTpcbiAgICAvLyAgICogdGhpcy5lbWl0RXZlbnQoXCJ0ZXN0Q2F0ZWdvcnlcIiwgXCJ0ZXN0QWN0aW9uXCIsIFwidGVzdExhYmVsXCIsIDEwKTtcbiAgICAvLyAgICogcGFyYW0ge3N0cmluZ30gZXZlbnRDYXRlZ29yeVxuICAgIC8vICAgKiBwYXJhbSB7c3RyaW5nfSBldmVudEFjdGlvblxuICAgIC8vICAgKiBwYXJhbSB7c3RyaW5nfSBldmVudExhYmVsXG4gICAgLy8gICAqIHBhcmFtIHtudW1iZXJ9IGV2ZW50VmFsdWVcbiAgICAvLyAgICovXG4gICAgcHVibGljIGV2ZW50RW1pdHRlcihldmVudENhdGVnb3J5OiBzdHJpbmcsXG4gICAgICAgIGV2ZW50QWN0aW9uOiBzdHJpbmcsXG4gICAgICAgIGV2ZW50TGFiZWw6IHN0cmluZyA9IG51bGwsXG4gICAgICAgIGV2ZW50VmFsdWU6IG51bWJlciA9IG51bGwpIHtcbiAgICAgICAgZ2EoJ3NlbmQnLCAnZXZlbnQnLCB7XG4gICAgICAgICAgICBldmVudENhdGVnb3J5OiBldmVudENhdGVnb3J5LFxuICAgICAgICAgICAgZXZlbnRMYWJlbDogZXZlbnRMYWJlbCxcbiAgICAgICAgICAgIGV2ZW50QWN0aW9uOiBldmVudEFjdGlvbixcbiAgICAgICAgICAgIGV2ZW50VmFsdWU6IGV2ZW50VmFsdWVcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3Vic2NyaWJlUm91dGVyRXZlbnRzKGV2ZW50OiBhbnkpIHtcbiAgICAgICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkge1xuICAgICAgICAgICAgZ2EoJ3NldCcsICdwYWdlJywgZXZlbnQudXJsQWZ0ZXJSZWRpcmVjdHMpO1xuICAgICAgICAgICAgZ2EoJ3NlbmQnLCAncGFnZXZpZXcnKTtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==