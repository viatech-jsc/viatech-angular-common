import { NgModule } from "@angular/core";
import { GoogleAnalyticsService } from './google-analytics.service';
import * as i0 from "@angular/core";
export class GoogleAnalyticsServiceModule {
}
GoogleAnalyticsServiceModule.ɵmod = i0.ɵɵdefineNgModule({ type: GoogleAnalyticsServiceModule });
GoogleAnalyticsServiceModule.ɵinj = i0.ɵɵdefineInjector({ factory: function GoogleAnalyticsServiceModule_Factory(t) { return new (t || GoogleAnalyticsServiceModule)(); }, providers: [GoogleAnalyticsService], imports: [[]] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(GoogleAnalyticsServiceModule, [{
        type: NgModule,
        args: [{
                imports: [],
                providers: [GoogleAnalyticsService],
                exports: []
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLmFuYWx5dGljcy5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy9nb29nbGVfYW5hbHl0aWNzL2dvb2dsZS5hbmFseXRpY3MubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7O0FBT3BFLE1BQU0sT0FBTyw0QkFBNEI7O2dFQUE1Qiw0QkFBNEI7dUlBQTVCLDRCQUE0QixtQkFINUIsQ0FBQyxzQkFBc0IsQ0FBQyxZQUQxQixFQUFFO2tEQUlBLDRCQUE0QjtjQUx4QyxRQUFRO2VBQUM7Z0JBQ1IsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsU0FBUyxFQUFFLENBQUMsc0JBQXNCLENBQUM7Z0JBQ25DLE9BQU8sRUFBRSxFQUFFO2FBQ1oiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBHb29nbGVBbmFseXRpY3NTZXJ2aWNlIH0gZnJvbSAnLi9nb29nbGUtYW5hbHl0aWNzLnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXSxcbiAgcHJvdmlkZXJzOiBbR29vZ2xlQW5hbHl0aWNzU2VydmljZV0sXG4gIGV4cG9ydHM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIEdvb2dsZUFuYWx5dGljc1NlcnZpY2VNb2R1bGUge31cbiJdfQ==