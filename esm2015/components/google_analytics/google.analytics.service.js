import { Injectable } from '@angular/core';
import { NavigationEnd } from '@angular/router'; // import Router and NavigationEnd
import * as i0 from "@angular/core";
export class GoogleAnalyticsService {
    //************************
    // Import this to app.component.ts
    // subscribe to router events and send page views to Google Analytics
    // this.router.events.subscribe(event => {
    //     GoogleAnalyticsService.subscribeRouterEvents(event);
    //   });
    //************************
    constructor() { }
    // /**
    //   * Emit google analytics event
    //   * Fire event example:
    //   * this.emitEvent("testCategory", "testAction", "testLabel", 10);
    //   * param {string} eventCategory
    //   * param {string} eventAction
    //   * param {string} eventLabel
    //   * param {number} eventValue
    //   */
    eventEmitter(eventCategory, eventAction, eventLabel = null, eventValue = null) {
        ga('send', 'event', {
            eventCategory: eventCategory,
            eventLabel: eventLabel,
            eventAction: eventAction,
            eventValue: eventValue
        });
    }
    subscribeRouterEvents(event) {
        if (event instanceof NavigationEnd) {
            ga('set', 'page', event.urlAfterRedirects);
            ga('send', 'pageview');
        }
    }
}
GoogleAnalyticsService.ɵfac = function GoogleAnalyticsService_Factory(t) { return new (t || GoogleAnalyticsService)(); };
GoogleAnalyticsService.ɵprov = i0.ɵɵdefineInjectable({ token: GoogleAnalyticsService, factory: GoogleAnalyticsService.ɵfac });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(GoogleAnalyticsService, [{
        type: Injectable
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLmFuYWx5dGljcy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy9nb29nbGVfYW5hbHl0aWNzL2dvb2dsZS5hbmFseXRpY3Muc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQyxDQUFDLGtDQUFrQzs7QUFJbkYsTUFBTSxPQUFPLHNCQUFzQjtJQUcvQiwwQkFBMEI7SUFDMUIsa0NBQWtDO0lBQ2xDLHFFQUFxRTtJQUNyRSwwQ0FBMEM7SUFDMUMsMkRBQTJEO0lBQzNELFFBQVE7SUFDUiwwQkFBMEI7SUFDMUIsZ0JBQWdCLENBQUM7SUFFakIsTUFBTTtJQUNOLGtDQUFrQztJQUNsQywwQkFBMEI7SUFDMUIscUVBQXFFO0lBQ3JFLG1DQUFtQztJQUNuQyxpQ0FBaUM7SUFDakMsZ0NBQWdDO0lBQ2hDLGdDQUFnQztJQUNoQyxPQUFPO0lBQ0EsWUFBWSxDQUFDLGFBQXFCLEVBQ3JDLFdBQW1CLEVBQ25CLGFBQXFCLElBQUksRUFDekIsYUFBcUIsSUFBSTtRQUN6QixFQUFFLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRTtZQUNoQixhQUFhLEVBQUUsYUFBYTtZQUM1QixVQUFVLEVBQUUsVUFBVTtZQUN0QixXQUFXLEVBQUUsV0FBVztZQUN4QixVQUFVLEVBQUUsVUFBVTtTQUN6QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQscUJBQXFCLENBQUMsS0FBVTtRQUM1QixJQUFJLEtBQUssWUFBWSxhQUFhLEVBQUU7WUFDaEMsRUFBRSxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDM0MsRUFBRSxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQztTQUMxQjtJQUNMLENBQUM7OzRGQXRDUSxzQkFBc0I7OERBQXRCLHNCQUFzQixXQUF0QixzQkFBc0I7a0RBQXRCLHNCQUFzQjtjQURsQyxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmF2aWdhdGlvbkVuZCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7IC8vIGltcG9ydCBSb3V0ZXIgYW5kIE5hdmlnYXRpb25FbmRcbmRlY2xhcmUgbGV0IGdhOiBGdW5jdGlvbjsgLy8gRGVjbGFyZSBnYSBhcyBhIGZ1bmN0aW9uXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBHb29nbGVBbmFseXRpY3NTZXJ2aWNlIHtcblxuXG4gICAgLy8qKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAvLyBJbXBvcnQgdGhpcyB0byBhcHAuY29tcG9uZW50LnRzXG4gICAgLy8gc3Vic2NyaWJlIHRvIHJvdXRlciBldmVudHMgYW5kIHNlbmQgcGFnZSB2aWV3cyB0byBHb29nbGUgQW5hbHl0aWNzXG4gICAgLy8gdGhpcy5yb3V0ZXIuZXZlbnRzLnN1YnNjcmliZShldmVudCA9PiB7XG4gICAgLy8gICAgIEdvb2dsZUFuYWx5dGljc1NlcnZpY2Uuc3Vic2NyaWJlUm91dGVyRXZlbnRzKGV2ZW50KTtcbiAgICAvLyAgIH0pO1xuICAgIC8vKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIC8vIC8qKlxuICAgIC8vICAgKiBFbWl0IGdvb2dsZSBhbmFseXRpY3MgZXZlbnRcbiAgICAvLyAgICogRmlyZSBldmVudCBleGFtcGxlOlxuICAgIC8vICAgKiB0aGlzLmVtaXRFdmVudChcInRlc3RDYXRlZ29yeVwiLCBcInRlc3RBY3Rpb25cIiwgXCJ0ZXN0TGFiZWxcIiwgMTApO1xuICAgIC8vICAgKiBwYXJhbSB7c3RyaW5nfSBldmVudENhdGVnb3J5XG4gICAgLy8gICAqIHBhcmFtIHtzdHJpbmd9IGV2ZW50QWN0aW9uXG4gICAgLy8gICAqIHBhcmFtIHtzdHJpbmd9IGV2ZW50TGFiZWxcbiAgICAvLyAgICogcGFyYW0ge251bWJlcn0gZXZlbnRWYWx1ZVxuICAgIC8vICAgKi9cbiAgICBwdWJsaWMgZXZlbnRFbWl0dGVyKGV2ZW50Q2F0ZWdvcnk6IHN0cmluZyxcbiAgICAgICAgZXZlbnRBY3Rpb246IHN0cmluZyxcbiAgICAgICAgZXZlbnRMYWJlbDogc3RyaW5nID0gbnVsbCxcbiAgICAgICAgZXZlbnRWYWx1ZTogbnVtYmVyID0gbnVsbCkge1xuICAgICAgICBnYSgnc2VuZCcsICdldmVudCcsIHtcbiAgICAgICAgICAgIGV2ZW50Q2F0ZWdvcnk6IGV2ZW50Q2F0ZWdvcnksXG4gICAgICAgICAgICBldmVudExhYmVsOiBldmVudExhYmVsLFxuICAgICAgICAgICAgZXZlbnRBY3Rpb246IGV2ZW50QWN0aW9uLFxuICAgICAgICAgICAgZXZlbnRWYWx1ZTogZXZlbnRWYWx1ZVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzdWJzY3JpYmVSb3V0ZXJFdmVudHMoZXZlbnQ6IGFueSkge1xuICAgICAgICBpZiAoZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSB7XG4gICAgICAgICAgICBnYSgnc2V0JywgJ3BhZ2UnLCBldmVudC51cmxBZnRlclJlZGlyZWN0cyk7XG4gICAgICAgICAgICBnYSgnc2VuZCcsICdwYWdldmlldycpO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19