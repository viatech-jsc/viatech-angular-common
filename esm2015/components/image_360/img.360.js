import { Component, Input, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
export class Image360Component {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.panoramaId = "";
        this.panoramaId = "panorama";
    }
    ngOnInit() {
        pannellum.viewer(this.panoramaId, {
            "type": "equirectangular",
            "autoLoad": true,
            "title": "360 Photo",
            "compass": true,
            "panorama": this.data.imgPath
        });
    }
    closeModal() {
        // this.ngbActiveModal.close();
        this.dialogRef.close("close");
    }
}
Image360Component.ɵfac = function Image360Component_Factory(t) { return new (t || Image360Component)(i0.ɵɵdirectiveInject(i1.MatDialogRef), i0.ɵɵdirectiveInject(MAT_DIALOG_DATA)); };
Image360Component.ɵcmp = i0.ɵɵdefineComponent({ type: Image360Component, selectors: [["via-360-image"]], inputs: { imgPath: "imgPath" }, decls: 5, vars: 0, consts: [[1, "via-360-image-wrapper"], [1, "modal-360", 3, "click"], ["aria-hidden", "true"], ["id", "panorama", 1, "panorama-wrapper"]], template: function Image360Component_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵlistener("click", function Image360Component_Template_div_click_1_listener() { return ctx.closeModal(); });
        i0.ɵɵelementStart(2, "span", 2);
        i0.ɵɵtext(3, "\u00D7");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelement(4, "div", 3);
        i0.ɵɵelementEnd();
    } }, styles: [".via-360-image-wrapper[_ngcontent-%COMP%]{position:relative}.via-360-image-wrapper[_ngcontent-%COMP%]   .panorama-wrapper[_ngcontent-%COMP%]{min-height:700px}.via-360-image-wrapper[_ngcontent-%COMP%]   .modal-360[_ngcontent-%COMP%]{background:#fff;border:1px solid rgba(0,0,0,.4);border-radius:4px;cursor:pointer;height:25px;line-height:25px;position:absolute;right:3px;text-align:center;top:3px;width:25px;z-index:1}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(Image360Component, [{
        type: Component,
        args: [{
                selector: "via-360-image",
                templateUrl: "./img.360.html",
                styleUrls: ["./img.360.scss"]
            }]
    }], function () { return [{ type: i1.MatDialogRef }, { type: undefined, decorators: [{
                type: Inject,
                args: [MAT_DIALOG_DATA]
            }] }]; }, { imgPath: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1nLjM2MC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvaW1hZ2VfMzYwL2ltZy4zNjAudHMiLCIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL2ltYWdlXzM2MC9pbWcuMzYwLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7OztBQVF6RSxNQUFNLE9BQU8saUJBQWlCO0lBSTVCLFlBQ1MsU0FBMEMsRUFDakIsSUFBUztRQURsQyxjQUFTLEdBQVQsU0FBUyxDQUFpQztRQUNqQixTQUFJLEdBQUosSUFBSSxDQUFLO1FBSjNDLGVBQVUsR0FBVyxFQUFFLENBQUM7UUFNdEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7SUFDL0IsQ0FBQztJQUVELFFBQVE7UUFDTixTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDaEMsTUFBTSxFQUFFLGlCQUFpQjtZQUN6QixVQUFVLEVBQUUsSUFBSTtZQUNoQixPQUFPLEVBQUUsV0FBVztZQUNwQixTQUFTLEVBQUUsSUFBSTtZQUNmLFVBQVUsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87U0FDOUIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFVBQVU7UUFDUiwrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7a0ZBeEJVLGlCQUFpQiw4REFNbEIsZUFBZTtzREFOZCxpQkFBaUI7UUNUOUIsOEJBQ0k7UUFBQSw4QkFDSTtRQURDLDJGQUFTLGdCQUFZLElBQUM7UUFDdkIsK0JBQXlCO1FBQUEsc0JBQU87UUFBQSxpQkFBTztRQUMzQyxpQkFBTTtRQUNOLHlCQUFrRDtRQUN0RCxpQkFBTTs7a0RESU8saUJBQWlCO2NBTDdCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsV0FBVyxFQUFFLGdCQUFnQjtnQkFDN0IsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLENBQUM7YUFDOUI7O3NCQU9JLE1BQU07dUJBQUMsZUFBZTt3QkFMaEIsT0FBTztrQkFBZixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBJbmplY3QgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEgfSBmcm9tIFwiQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nXCI7XG5kZWNsYXJlIGxldCBwYW5uZWxsdW06IGFueTtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS0zNjAtaW1hZ2VcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9pbWcuMzYwLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2ltZy4zNjAuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBJbWFnZTM2MENvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGltZ1BhdGg6IHN0cmluZztcbiAgcGFub3JhbWFJZDogc3RyaW5nID0gXCJcIjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8SW1hZ2UzNjBDb21wb25lbnQ+LFxuICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogYW55XG4gICkgeyBcbiAgICB0aGlzLnBhbm9yYW1hSWQgPSBcInBhbm9yYW1hXCI7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICBwYW5uZWxsdW0udmlld2VyKHRoaXMucGFub3JhbWFJZCwge1xuICAgICAgXCJ0eXBlXCI6IFwiZXF1aXJlY3Rhbmd1bGFyXCIsXG4gICAgICBcImF1dG9Mb2FkXCI6IHRydWUsXG4gICAgICBcInRpdGxlXCI6IFwiMzYwIFBob3RvXCIsXG4gICAgICBcImNvbXBhc3NcIjogdHJ1ZSxcbiAgICAgIFwicGFub3JhbWFcIjogdGhpcy5kYXRhLmltZ1BhdGhcbiAgICB9KTtcbiAgfVxuXG4gIGNsb3NlTW9kYWwoKTogdm9pZCB7XG4gICAgLy8gdGhpcy5uZ2JBY3RpdmVNb2RhbC5jbG9zZSgpO1xuICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKFwiY2xvc2VcIik7XG4gIH1cbn1cbiIsIjxkaXYgY2xhc3M9XCJ2aWEtMzYwLWltYWdlLXdyYXBwZXJcIj5cbiAgICA8ZGl2IChjbGljayk9XCJjbG9zZU1vZGFsKClcIiBjbGFzcz1cIm1vZGFsLTM2MFwiPlxuICAgICAgICA8c3BhbiBhcmlhLWhpZGRlbj1cInRydWVcIj4mdGltZXM7PC9zcGFuPlxuICAgIDwvZGl2PlxuICAgIDxkaXYgaWQ9XCJwYW5vcmFtYVwiIGNsYXNzPVwicGFub3JhbWEtd3JhcHBlclwiPjwvZGl2PlxuPC9kaXY+XG4iXX0=