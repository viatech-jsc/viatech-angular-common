export class ModalDto {
    constructor(modalTitle, yesBtnTitle, noBtnTitle = "close", type, translateParams, modalContent, modalLogo) {
        this.modalTitle = modalTitle;
        this.modalContent = modalContent;
        this.yesBtnTitle = yesBtnTitle;
        this.noBtnTitle = noBtnTitle;
        this.type = type;
        this.translateParams = translateParams;
        this.modalLogo = modalLogo;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuZHRvLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy9tb2RhbC9kdG8vbW9kYWwuZHRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sT0FBTyxRQUFRO0lBU2pCLFlBQVksVUFBa0IsRUFBRSxXQUFtQixFQUFFLGFBQXFCLE9BQU8sRUFBRSxJQUFZLEVBQUUsZUFBdUIsRUFBRSxZQUFxQixFQUFFLFNBQWtCO1FBQy9KLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQy9CLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQzdCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0lBQy9CLENBQUM7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBNb2RhbER0byB7XG4gICAgbW9kYWxUaXRsZTogc3RyaW5nO1xuICAgIG1vZGFsQ29udGVudD86IHN0cmluZztcbiAgICB5ZXNCdG5UaXRsZTogc3RyaW5nO1xuICAgIG5vQnRuVGl0bGU6IHN0cmluZztcbiAgICB0eXBlOiBzdHJpbmc7XG4gICAgdHJhbnNsYXRlUGFyYW1zPzogb2JqZWN0O1xuICAgIG1vZGFsTG9nbz86IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKG1vZGFsVGl0bGU6IHN0cmluZywgeWVzQnRuVGl0bGU6IHN0cmluZywgbm9CdG5UaXRsZTogc3RyaW5nID0gXCJjbG9zZVwiLCB0eXBlOiBzdHJpbmcsIHRyYW5zbGF0ZVBhcmFtczogb2JqZWN0LCBtb2RhbENvbnRlbnQ/OiBzdHJpbmcsIG1vZGFsTG9nbz86IHN0cmluZyl7XG4gICAgICAgIHRoaXMubW9kYWxUaXRsZSA9IG1vZGFsVGl0bGU7XG4gICAgICAgIHRoaXMubW9kYWxDb250ZW50ID0gbW9kYWxDb250ZW50O1xuICAgICAgICB0aGlzLnllc0J0blRpdGxlID0geWVzQnRuVGl0bGU7XG4gICAgICAgIHRoaXMubm9CdG5UaXRsZSA9IG5vQnRuVGl0bGU7XG4gICAgICAgIHRoaXMudHlwZSA9IHR5cGU7XG4gICAgICAgIHRoaXMudHJhbnNsYXRlUGFyYW1zID0gdHJhbnNsYXRlUGFyYW1zO1xuICAgICAgICB0aGlzLm1vZGFsTG9nbyA9IG1vZGFsTG9nbztcbiAgICB9XG59XG4iXX0=