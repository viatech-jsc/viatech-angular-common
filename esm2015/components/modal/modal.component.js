import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Modal } from "./modal.model";
import { modalConfigs } from './modal.config';
import * as i0 from "@angular/core";
import * as i1 from "@ng-bootstrap/ng-bootstrap";
import * as i2 from "@angular/common";
function WinModalComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 8);
    i0.ɵɵelement(1, "img", 9);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("src", ctx_r0.modalOptions.modalLogo, i0.ɵɵsanitizeUrl);
} }
function WinModalComponent_button_9_Template(rf, ctx) { if (rf & 1) {
    const _r3 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 10);
    i0.ɵɵlistener("click", function WinModalComponent_button_9_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r3); const ctx_r2 = i0.ɵɵnextContext(); return ctx_r2.close(); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r1.modalOptions.noBtnTitle, " ");
} }
export class WinModalComponent {
    constructor(activeModal) {
        this.activeModal = activeModal;
        this.onConfirm = new EventEmitter();
        this.modalTypes = modalConfigs;
        //super();
    }
    close() {
        this.activeModal.dismiss("close");
    }
    ok() {
        this.activeModal.close("ok");
    }
    handleCloseByIcon() {
        if (this.modalOptions.type === modalConfigs.confirm) {
            this.close();
        }
        else {
            this.ok();
        }
    }
}
WinModalComponent.ɵfac = function WinModalComponent_Factory(t) { return new (t || WinModalComponent)(i0.ɵɵdirectiveInject(i1.NgbActiveModal)); };
WinModalComponent.ɵcmp = i0.ɵɵdefineComponent({ type: WinModalComponent, selectors: [["via-modal"]], inputs: { modalOptions: "modalOptions" }, outputs: { onConfirm: "onConfirm" }, decls: 10, vars: 5, consts: [["class", "center-text m-t-20", 4, "ngIf"], [1, "center-text"], [1, "modal-title"], [1, "modal-body", "modal-body-text-p"], [3, "innerHtml"], [1, "modal-footer"], ["type", "button", 1, "btn", "uppercase-text", "btn-primary", "modal-action-btn", 3, "click"], ["type", "button", "class", "btn uppercase-text btn-light modal-action-btn", 3, "click", 4, "ngIf"], [1, "center-text", "m-t-20"], [2, "margin", "0px auto", 3, "src"], ["type", "button", 1, "btn", "uppercase-text", "btn-light", "modal-action-btn", 3, "click"]], template: function WinModalComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, WinModalComponent_div_0_Template, 2, 1, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵelementStart(2, "h4", 2);
        i0.ɵɵtext(3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(4, "div", 3);
        i0.ɵɵelement(5, "p", 4);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(6, "div", 5);
        i0.ɵɵelementStart(7, "button", 6);
        i0.ɵɵlistener("click", function WinModalComponent_Template_button_click_7_listener() { return ctx.ok(); });
        i0.ɵɵtext(8);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(9, WinModalComponent_button_9_Template, 2, 1, "button", 7);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", ctx.modalOptions.modalLogo);
        i0.ɵɵadvance(3);
        i0.ɵɵtextInterpolate(ctx.modalOptions.modalTitle);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("innerHtml", ctx.modalOptions.modalContent, i0.ɵɵsanitizeHtml);
        i0.ɵɵadvance(3);
        i0.ɵɵtextInterpolate1(" ", ctx.modalOptions.yesBtnTitle, " ");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.modalOptions.type === ctx.modalTypes.confirm);
    } }, directives: [i2.NgIf], encapsulation: 2 });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(WinModalComponent, [{
        type: Component,
        args: [{
                selector: 'via-modal',
                templateUrl: './modal.html'
            }]
    }], function () { return [{ type: i1.NgbActiveModal }]; }, { modalOptions: [{
            type: Input
        }], onConfirm: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvbW9kYWwvbW9kYWwuY29tcG9uZW50LnRzIiwiY29tcG9uZW50cy9tb2RhbC9tb2RhbC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzVELE9BQU8sRUFBQyxLQUFLLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDcEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7OztJQ0g5Qyw4QkFDSTtJQUFBLHlCQUNKO0lBQUEsaUJBQU07OztJQUQ2QixlQUE4QjtJQUE5QixxRUFBOEI7Ozs7SUFpQjdELGtDQUNJO0lBRHdFLG1MQUFpQjtJQUN6RixZQUNKO0lBQUEsaUJBQVM7OztJQURMLGVBQ0o7SUFESSwrREFDSjs7QURYSixNQUFNLE9BQU8saUJBQWlCO0lBTTVCLFlBQW1CLFdBQTJCO1FBQTNCLGdCQUFXLEdBQVgsV0FBVyxDQUFnQjtRQUpsQyxjQUFTLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDNUQsZUFBVSxHQUFHLFlBQVksQ0FBQztRQUkxQixVQUFVO0lBQ1osQ0FBQztJQUVELEtBQUs7UUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsRUFBRTtRQUNBLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxpQkFBaUI7UUFDZixJQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxLQUFLLFlBQVksQ0FBQyxPQUFPLEVBQUM7WUFDakQsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2Q7YUFBTTtZQUNMLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQztTQUNYO0lBQ0gsQ0FBQzs7a0ZBeEJVLGlCQUFpQjtzREFBakIsaUJBQWlCO1FDVDlCLGtFQUNJO1FBRUosOEJBQ0k7UUFBQSw2QkFBd0I7UUFBQSxZQUEyQjtRQUFBLGlCQUFLO1FBQzVELGlCQUFNO1FBTU4sOEJBQ0k7UUFBQSx1QkFBK0M7UUFDbkQsaUJBQU07UUFDTiw4QkFDSTtRQUFBLGlDQUNJO1FBRDBFLDhGQUFTLFFBQUksSUFBQztRQUN4RixZQUNKO1FBQUEsaUJBQVM7UUFDVCx3RUFDSTtRQUVSLGlCQUFNOztRQXJCMEIsaURBQThCO1FBSWxDLGVBQTJCO1FBQTNCLGlEQUEyQjtRQVFoRCxlQUF1QztRQUF2Qyw0RUFBdUM7UUFJdEMsZUFDSjtRQURJLDZEQUNKO1FBQzhGLGVBQThDO1FBQTlDLHVFQUE4Qzs7a0REVG5JLGlCQUFpQjtjQUo3QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLFdBQVcsRUFBRSxjQUFjO2FBQzVCO2lFQUVZLFlBQVk7a0JBQXBCLEtBQUs7WUFDSSxTQUFTO2tCQUFsQixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5nYkFjdGl2ZU1vZGFsIH0gZnJvbSAnQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXAnO1xuaW1wb3J0IHtNb2RhbH0gZnJvbSBcIi4vbW9kYWwubW9kZWxcIjtcbmltcG9ydCB7IG1vZGFsQ29uZmlncyB9IGZyb20gJy4vbW9kYWwuY29uZmlnJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndmlhLW1vZGFsJyxcbiAgdGVtcGxhdGVVcmw6ICcuL21vZGFsLmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIFdpbk1vZGFsQ29tcG9uZW50IHtcbiAgICBASW5wdXQoKSBtb2RhbE9wdGlvbnM6IE1vZGFsO1xuICAgIEBPdXRwdXQoKSBvbkNvbmZpcm06IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIG1vZGFsVHlwZXMgPSBtb2RhbENvbmZpZ3M7XG4gICAgcGFyYW1zO1xuXG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBhY3RpdmVNb2RhbDogTmdiQWN0aXZlTW9kYWwpIHtcbiAgICAvL3N1cGVyKCk7XG4gIH1cblxuICBjbG9zZSgpOiB2b2lke1xuICAgIHRoaXMuYWN0aXZlTW9kYWwuZGlzbWlzcyhcImNsb3NlXCIpO1xuICB9XG5cbiAgb2soKTogdm9pZHtcbiAgICB0aGlzLmFjdGl2ZU1vZGFsLmNsb3NlKFwib2tcIik7XG4gIH1cblxuICBoYW5kbGVDbG9zZUJ5SWNvbigpOiB2b2lke1xuICAgIGlmKHRoaXMubW9kYWxPcHRpb25zLnR5cGUgPT09IG1vZGFsQ29uZmlncy5jb25maXJtKXtcbiAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5vaygpO1xuICAgIH1cbiAgfVxufSIsIjxkaXYgY2xhc3M9XCJjZW50ZXItdGV4dCBtLXQtMjBcIiAqbmdJZj1cIm1vZGFsT3B0aW9ucy5tb2RhbExvZ29cIiA+XG4gICAgPGltZyBzdHlsZT1cIm1hcmdpbjogMHB4IGF1dG87XCIgW3NyY109XCJtb2RhbE9wdGlvbnMubW9kYWxMb2dvXCI+XG48L2Rpdj5cbjxkaXYgY2xhc3M9XCJjZW50ZXItdGV4dFwiPlxuICAgIDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCI+e3ttb2RhbE9wdGlvbnMubW9kYWxUaXRsZX19PC9oND5cbjwvZGl2PlxuPCEtLSA8ZGl2IGNsYXNzPVwibW9kYWwtaGVhZGVyXCI+XG4gICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJjbG9zZVwiIGFyaWEtbGFiZWw9XCJDbG9zZVwiIChjbGljayk9XCJoYW5kbGVDbG9zZUJ5SWNvbigpXCI+XG4gICAgICAgIDxzcGFuIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPiZ0aW1lczs8L3NwYW4+XG4gICAgPC9idXR0b24+XG48L2Rpdj4gLS0+XG48ZGl2IGNsYXNzPVwibW9kYWwtYm9keSBtb2RhbC1ib2R5LXRleHQtcFwiPlxuICAgIDxwIFtpbm5lckh0bWxdPVwibW9kYWxPcHRpb25zLm1vZGFsQ29udGVudFwiPjwvcD5cbjwvZGl2PlxuPGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPlxuICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIHVwcGVyY2FzZS10ZXh0IGJ0bi1wcmltYXJ5IG1vZGFsLWFjdGlvbi1idG5cIiAoY2xpY2spPVwib2soKVwiPlxuICAgICAgICB7e21vZGFsT3B0aW9ucy55ZXNCdG5UaXRsZX19XG4gICAgPC9idXR0b24+XG4gICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gdXBwZXJjYXNlLXRleHQgYnRuLWxpZ2h0IG1vZGFsLWFjdGlvbi1idG5cIiAoY2xpY2spPVwiY2xvc2UoKVwiICpuZ0lmPVwibW9kYWxPcHRpb25zLnR5cGU9PT1tb2RhbFR5cGVzLmNvbmZpcm1cIj5cbiAgICAgICAge3ttb2RhbE9wdGlvbnMubm9CdG5UaXRsZX19XG4gICAgPC9idXR0b24+XG48L2Rpdj4iXX0=