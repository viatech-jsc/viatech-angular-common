import { NgModule } from '@angular/core';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { WinModalComponent } from './modal.component';
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
export class WinModalModule {
}
WinModalModule.ɵmod = i0.ɵɵdefineNgModule({ type: WinModalModule });
WinModalModule.ɵinj = i0.ɵɵdefineInjector({ factory: function WinModalModule_Factory(t) { return new (t || WinModalModule)(); }, imports: [[
            NgbModule,
            FormsModule,
            CommonModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(WinModalModule, { declarations: [WinModalComponent], imports: [NgbModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule], exports: [WinModalComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(WinModalModule, [{
        type: NgModule,
        args: [{
                declarations: [WinModalComponent],
                imports: [
                    NgbModule,
                    FormsModule,
                    CommonModule,
                    ReactiveFormsModule
                ],
                exports: [WinModalComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvbW9kYWwvbW9kYWwubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7O0FBWS9DLE1BQU0sT0FBTyxjQUFjOztrREFBZCxjQUFjOzJHQUFkLGNBQWMsa0JBUmhCO1lBQ1AsU0FBUztZQUNULFdBQVc7WUFDWCxZQUFZO1lBQ1osbUJBQW1CO1NBQ3BCO3dGQUdVLGNBQWMsbUJBVFYsaUJBQWlCLGFBRTlCLFNBQVM7UUFDVCxXQUFXO1FBQ1gsWUFBWTtRQUNaLG1CQUFtQixhQUVYLGlCQUFpQjtrREFFaEIsY0FBYztjQVYxQixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7Z0JBQ2pDLE9BQU8sRUFBRTtvQkFDUCxTQUFTO29CQUNULFdBQVc7b0JBQ1gsWUFBWTtvQkFDWixtQkFBbUI7aUJBQ3BCO2dCQUNELE9BQU8sRUFBRSxDQUFDLGlCQUFpQixDQUFDO2FBQzdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5nYk1vZHVsZSB9IGZyb20gXCJAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcFwiO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcbmltcG9ydCB7IFdpbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbV2luTW9kYWxDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgTmdiTW9kdWxlLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIENvbW1vbk1vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtXaW5Nb2RhbENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgV2luTW9kYWxNb2R1bGUgeyB9XG4iXX0=