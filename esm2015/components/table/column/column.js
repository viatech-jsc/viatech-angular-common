import { Component, EventEmitter, Input, Output } from "@angular/core";
import { SORT } from "../constant";
import { TableColumnDto } from "../dto/table.column.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "./filter/multiple/multiple";
import * as i3 from "./filter/text/text";
function ColumnComp_via_table_column_filter_mult_3_Template(rf, ctx) { if (rf & 1) {
    const _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "via-table-column-filter-mult", 5);
    i0.ɵɵlistener("dropdownFilter", function ColumnComp_via_table_column_filter_mult_3_Template_via_table_column_filter_mult_dropdownFilter_0_listener($event) { i0.ɵɵrestoreView(_r5); const ctx_r4 = i0.ɵɵnextContext(); return ctx_r4.doFilterOnColumn($event); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵproperty("column", ctx_r0.column)("tableLanguageConfig", ctx_r0.tableLanguageConfig);
} }
function ColumnComp_via_table_column_filter_text_4_Template(rf, ctx) { if (rf & 1) {
    const _r7 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "via-table-column-filter-text", 5);
    i0.ɵɵlistener("dropdownFilter", function ColumnComp_via_table_column_filter_text_4_Template_via_table_column_filter_text_dropdownFilter_0_listener($event) { i0.ɵɵrestoreView(_r7); const ctx_r6 = i0.ɵɵnextContext(); return ctx_r6.doFilterOnColumn($event); });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵproperty("column", ctx_r1.column)("tableLanguageConfig", ctx_r1.tableLanguageConfig);
} }
function ColumnComp_i_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i", 6);
} }
function ColumnComp_i_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i", 7);
} }
export class ColumnComp {
    constructor() {
        this.dropdownFilter = new EventEmitter();
        this.actionSort = new EventEmitter();
    }
    ngOnInit() {
    }
    doFilterOnColumn(req) {
        this.dropdownFilter.emit(req);
    }
    isSortAsc(column) {
        return column.sort === SORT.ASC;
    }
    isSortDesc(column) {
        return column.sort === SORT.DESC;
    }
    doSort(column) {
        if (!column.sortable) {
            return;
        }
        this.actionSort.emit(column);
    }
}
ColumnComp.ɵfac = function ColumnComp_Factory(t) { return new (t || ColumnComp)(); };
ColumnComp.ɵcmp = i0.ɵɵdefineComponent({ type: ColumnComp, selectors: [["via-table-column"]], inputs: { column: "column", tableLanguageConfig: "tableLanguageConfig" }, outputs: { dropdownFilter: "dropdownFilter", actionSort: "actionSort" }, decls: 7, vars: 5, consts: [[1, "header-container"], [3, "click"], [3, "column", "tableLanguageConfig", "dropdownFilter", 4, "ngIf"], ["class", "fa fa-sort-amount-asc", "aria-hidden", "true", 4, "ngIf"], ["class", "fa fa-sort-amount-desc", "aria-hidden", "true", 4, "ngIf"], [3, "column", "tableLanguageConfig", "dropdownFilter"], ["aria-hidden", "true", 1, "fa", "fa-sort-amount-asc"], ["aria-hidden", "true", 1, "fa", "fa-sort-amount-desc"]], template: function ColumnComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "span", 1);
        i0.ɵɵlistener("click", function ColumnComp_Template_span_click_1_listener() { return ctx.doSort(ctx.column); });
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(3, ColumnComp_via_table_column_filter_mult_3_Template, 1, 2, "via-table-column-filter-mult", 2);
        i0.ɵɵtemplate(4, ColumnComp_via_table_column_filter_text_4_Template, 1, 2, "via-table-column-filter-text", 2);
        i0.ɵɵtemplate(5, ColumnComp_i_5_Template, 1, 0, "i", 3);
        i0.ɵɵtemplate(6, ColumnComp_i_6_Template, 1, 0, "i", 4);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.column.value);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.column.filter.type === "MULTIPLE");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.column.filter.type === "TEXT");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.isSortAsc(ctx.column));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.isSortDesc(ctx.column));
    } }, directives: [i1.NgIf, i2.ColumnFilterMultComp, i3.ColumnFilterTextComp], styles: [".header-container[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:row;justify-content:flex-start;width:100%}.header-container[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#000;margin-left:10px;text-align:left}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ColumnComp, [{
        type: Component,
        args: [{
                selector: "via-table-column",
                templateUrl: "./column.html",
                styleUrls: ["./column.scss"]
            }]
    }], function () { return []; }, { dropdownFilter: [{
            type: Output
        }], actionSort: [{
            type: Output
        }], column: [{
            type: Input
        }], tableLanguageConfig: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9jb2x1bW4vY29sdW1uLnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9jb2x1bW4vY29sdW1uLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBNEMsTUFBTSxlQUFlLENBQUM7QUFDakgsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUVuQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7Ozs7Ozs7SUNBckQsdURBSytCO0lBRDNCLGlRQUEyQztJQUMvQyxpQkFBK0I7OztJQUgzQixzQ0FBaUIsbURBQUE7Ozs7SUFLckIsdURBSytCO0lBRDNCLGlRQUEyQztJQUMvQyxpQkFBK0I7OztJQUgzQixzQ0FBaUIsbURBQUE7OztJQUtyQix1QkFBa0Y7OztJQUNsRix1QkFBb0Y7O0FEUnhGLE1BQU0sT0FBTyxVQUFVO0lBU3JCO1FBUFUsbUJBQWMsR0FBc0MsSUFBSSxZQUFZLEVBQXVCLENBQUM7UUFDNUYsZUFBVSxHQUFpQyxJQUFJLFlBQVksRUFBa0IsQ0FBQztJQVF4RixDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxHQUF3QjtRQUN2QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsU0FBUyxDQUFDLE1BQXNCO1FBQzlCLE9BQU8sTUFBTSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQ2xDLENBQUM7SUFFRCxVQUFVLENBQUMsTUFBc0I7UUFDL0IsT0FBTyxNQUFNLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDbkMsQ0FBQztJQUVELE1BQU0sQ0FBQyxNQUFzQjtRQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUNwQixPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMvQixDQUFDOztvRUFqQ1UsVUFBVTsrQ0FBVixVQUFVO1FDVnZCLDhCQUNJO1FBQUEsK0JBQStCO1FBQXpCLHFGQUFTLHNCQUFjLElBQUM7UUFBQyxZQUFnQjtRQUFBLGlCQUFPO1FBRXRELDZHQUtBO1FBRUEsNkdBS0E7UUFFQSx1REFBOEU7UUFDOUUsdURBQWdGO1FBQ3BGLGlCQUFNOztRQWxCNkIsZUFBZ0I7UUFBaEIsc0NBQWdCO1FBRzNDLGVBQXVDO1FBQXZDLDREQUF1QztRQU92QyxlQUFtQztRQUFuQyx3REFBbUM7UUFNcEMsZUFBeUI7UUFBekIsZ0RBQXlCO1FBQ3pCLGVBQTBCO1FBQTFCLGlEQUEwQjs7a0REUnBCLFVBQVU7Y0FMdEIsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLFdBQVcsRUFBRSxlQUFlO2dCQUM1QixTQUFTLEVBQUUsQ0FBQyxlQUFlLENBQUM7YUFDN0I7c0NBR1csY0FBYztrQkFBdkIsTUFBTTtZQUNHLFVBQVU7a0JBQW5CLE1BQU07WUFFRSxNQUFNO2tCQUFkLEtBQUs7WUFDRyxtQkFBbUI7a0JBQTNCLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCwgVGVtcGxhdGVSZWYsIFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBTT1JUIH0gZnJvbSBcIi4uL2NvbnN0YW50XCI7XG5pbXBvcnQgeyBDb2x1bW5GaWx0ZXJCYXNlRHRvIH0gZnJvbSBcIi4uL2R0by9maWx0ZXIvY29sdW1uLmZpbHRlci5iYXNlLmR0b1wiO1xuaW1wb3J0IHsgVGFibGVDb2x1bW5EdG8gfSBmcm9tIFwiLi4vZHRvL3RhYmxlLmNvbHVtbi5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10YWJsZS1jb2x1bW5cIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9jb2x1bW4uaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vY29sdW1uLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgQ29sdW1uQ29tcHtcblxuICBAT3V0cHV0KCkgZHJvcGRvd25GaWx0ZXI6IEV2ZW50RW1pdHRlcjxDb2x1bW5GaWx0ZXJCYXNlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXI8Q29sdW1uRmlsdGVyQmFzZUR0bz4oKTtcbiAgQE91dHB1dCgpIGFjdGlvblNvcnQ6IEV2ZW50RW1pdHRlcjxUYWJsZUNvbHVtbkR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyPFRhYmxlQ29sdW1uRHRvPigpO1xuXG4gIEBJbnB1dCgpIGNvbHVtbjogVGFibGVDb2x1bW5EdG87XG4gIEBJbnB1dCgpIHRhYmxlTGFuZ3VhZ2VDb25maWc6IGFueTtcbiAgXG4gIFxuICBjb25zdHJ1Y3RvcihcbiAgKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIGRvRmlsdGVyT25Db2x1bW4ocmVxOiBDb2x1bW5GaWx0ZXJCYXNlRHRvKSB7XG4gICAgdGhpcy5kcm9wZG93bkZpbHRlci5lbWl0KHJlcSk7XG4gIH1cblxuICBpc1NvcnRBc2MoY29sdW1uOiBUYWJsZUNvbHVtbkR0byk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBjb2x1bW4uc29ydCA9PT0gU09SVC5BU0M7XG4gIH1cblxuICBpc1NvcnREZXNjKGNvbHVtbjogVGFibGVDb2x1bW5EdG8pOiBib29sZWFuIHtcbiAgICByZXR1cm4gY29sdW1uLnNvcnQgPT09IFNPUlQuREVTQztcbiAgfVxuXG4gIGRvU29ydChjb2x1bW46IFRhYmxlQ29sdW1uRHRvKTogdm9pZCB7XG4gICAgaWYgKCFjb2x1bW4uc29ydGFibGUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5hY3Rpb25Tb3J0LmVtaXQoY29sdW1uKTtcbiAgfVxuXG59XG4iLCI8ZGl2IGNsYXNzPVwiaGVhZGVyLWNvbnRhaW5lclwiPlxuICAgIDxzcGFuIChjbGljayk9XCJkb1NvcnQoY29sdW1uKVwiPnt7Y29sdW1uLnZhbHVlfX08L3NwYW4+XG5cbiAgICA8dmlhLXRhYmxlLWNvbHVtbi1maWx0ZXItbXVsdCBcbiAgICAgICAgKm5nSWY9XCJjb2x1bW4uZmlsdGVyLnR5cGU9PT0nTVVMVElQTEUnXCJcbiAgICAgICAgW2NvbHVtbl09XCJjb2x1bW5cIlxuICAgICAgICBbdGFibGVMYW5ndWFnZUNvbmZpZ109XCJ0YWJsZUxhbmd1YWdlQ29uZmlnXCJcbiAgICAgICAgKGRyb3Bkb3duRmlsdGVyKT1cImRvRmlsdGVyT25Db2x1bW4oJGV2ZW50KVwiPlxuICAgIDwvdmlhLXRhYmxlLWNvbHVtbi1maWx0ZXItbXVsdD5cbiAgICBcbiAgICA8dmlhLXRhYmxlLWNvbHVtbi1maWx0ZXItdGV4dCBcbiAgICAgICAgKm5nSWY9XCJjb2x1bW4uZmlsdGVyLnR5cGU9PT0nVEVYVCdcIlxuICAgICAgICBbY29sdW1uXT1cImNvbHVtblwiXG4gICAgICAgIFt0YWJsZUxhbmd1YWdlQ29uZmlnXT1cInRhYmxlTGFuZ3VhZ2VDb25maWdcIlxuICAgICAgICAoZHJvcGRvd25GaWx0ZXIpPVwiZG9GaWx0ZXJPbkNvbHVtbigkZXZlbnQpXCI+XG4gICAgPC92aWEtdGFibGUtY29sdW1uLWZpbHRlci10ZXh0PlxuXG4gICAgPGkgKm5nSWY9XCJpc1NvcnRBc2MoY29sdW1uKVwiIGNsYXNzPVwiZmEgZmEtc29ydC1hbW91bnQtYXNjXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxuICAgIDxpICpuZ0lmPVwiaXNTb3J0RGVzYyhjb2x1bW4pXCIgY2xhc3M9XCJmYSBmYS1zb3J0LWFtb3VudC1kZXNjXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxuPC9kaXY+Il19