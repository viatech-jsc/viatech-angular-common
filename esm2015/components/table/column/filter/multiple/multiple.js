import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { MatSelectionList } from "@angular/material/list";
import { MatMenuTrigger } from "@angular/material/menu";
import { find, findIndex } from "lodash";
import { FILTER_ON_COLUMN_TYPE } from "../../../constant";
import { ColumnFilterMultDto } from "../../../dto/filter/column.filter.mult.dto";
import { TableColumnDto } from "../../../dto/table.column.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/material/menu";
import * as i3 from "@angular/material/icon";
import * as i4 from "@angular/material/badge";
import * as i5 from "@angular/material/list";
import * as i6 from "@angular/common";
const _c0 = ["matSelectionList"];
function ColumnFilterMultComp_mat_list_option_8_Template(rf, ctx) { if (rf & 1) {
    const _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-list-option", 10);
    i0.ɵɵlistener("selectedChange", function ColumnFilterMultComp_mat_list_option_8_Template_mat_list_option_selectedChange_0_listener($event) { i0.ɵɵrestoreView(_r5); const filter_r3 = ctx.$implicit; return filter_r3.checked = $event; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const filter_r3 = ctx.$implicit;
    i0.ɵɵproperty("value", filter_r3)("selected", filter_r3.checked);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", filter_r3.title, " ");
} }
export class ColumnFilterMultComp {
    constructor() {
        this.dropdownFilter = new EventEmitter();
        this.badgeCount = null;
    }
    ngOnInit() {
        this.filter = this.column.filter;
    }
    menuOpened() {
        this.matSelectionList.options.forEach((item) => {
            let element = find(this.filter.filterData, i => i.value === item.value.value);
            item.selected = element.checked;
        });
    }
    doFilter() {
        this.matMenuTrigger.closeMenu();
        let items = this.matSelectionList.selectedOptions.selected.map(item => item.value);
        items.forEach(item => item.checked = true);
        this.badgeCount = items.length > 0 ? items.length : null;
        this.filter.filterData.forEach(element => {
            let index = findIndex(items, item => item.value === element.value);
            element.checked = (index !== -1);
        });
        let req = new ColumnFilterMultDto();
        req.columnName = this.column.name;
        req.filterData = items;
        req.type = FILTER_ON_COLUMN_TYPE.MULTIPLE;
        this.dropdownFilter.emit(req);
    }
    doReset() {
        this.matMenuTrigger.closeMenu();
        this.matSelectionList.deselectAll();
        this.badgeCount = null;
        this.filter.filterData.forEach(element => {
            element.checked = false;
        });
        let req = new ColumnFilterMultDto();
        req.columnName = this.column.name;
        req.filterData = [];
        req.type = FILTER_ON_COLUMN_TYPE.MULTIPLE;
        this.dropdownFilter.emit(req);
    }
}
ColumnFilterMultComp.ɵfac = function ColumnFilterMultComp_Factory(t) { return new (t || ColumnFilterMultComp)(); };
ColumnFilterMultComp.ɵcmp = i0.ɵɵdefineComponent({ type: ColumnFilterMultComp, selectors: [["via-table-column-filter-mult"]], viewQuery: function ColumnFilterMultComp_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(MatMenuTrigger, true);
        i0.ɵɵviewQuery(_c0, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.matMenuTrigger = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.matSelectionList = _t.first);
    } }, inputs: { column: "column", tableLanguageConfig: "tableLanguageConfig" }, outputs: { dropdownFilter: "dropdownFilter" }, decls: 14, vars: 5, consts: [["mat-icon-button", "", "color", "primary", 3, "matMenuTriggerFor", "menuOpened"], ["matBadgeColor", "warn", "matBadgeOverlap", "true", "matBadgeSize", "small", 3, "matBadge"], ["overlapTrigger", "true"], ["filterMenu", "matMenu"], [3, "click"], ["matSelectionList", ""], ["checkboxPosition", "before", "color", "warn", 3, "value", "selected", "selectedChange", 4, "ngFor", "ngForOf"], [2, "display", "flex", "margin-top", "10px", "justify-content", "space-evenly", 3, "click"], ["mat-flat-button", "", "color", "primary", 3, "click"], ["mat-flat-button", "", 3, "click"], ["checkboxPosition", "before", "color", "warn", 3, "value", "selected", "selectedChange"]], template: function ColumnFilterMultComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementContainerStart(0);
        i0.ɵɵelementStart(1, "button", 0);
        i0.ɵɵlistener("menuOpened", function ColumnFilterMultComp_Template_button_menuOpened_1_listener() { return ctx.menuOpened(); });
        i0.ɵɵelementStart(2, "mat-icon", 1);
        i0.ɵɵtext(3, "filter_alt ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(4, "mat-menu", 2, 3);
        i0.ɵɵelementStart(6, "mat-selection-list", 4, 5);
        i0.ɵɵlistener("click", function ColumnFilterMultComp_Template_mat_selection_list_click_6_listener($event) { return $event.stopPropagation(); });
        i0.ɵɵtemplate(8, ColumnFilterMultComp_mat_list_option_8_Template, 2, 3, "mat-list-option", 6);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(9, "div", 7);
        i0.ɵɵlistener("click", function ColumnFilterMultComp_Template_div_click_9_listener($event) { return $event.stopPropagation(); });
        i0.ɵɵelementStart(10, "button", 8);
        i0.ɵɵlistener("click", function ColumnFilterMultComp_Template_button_click_10_listener() { return ctx.doFilter(); });
        i0.ɵɵtext(11);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(12, "button", 9);
        i0.ɵɵlistener("click", function ColumnFilterMultComp_Template_button_click_12_listener() { return ctx.doReset(); });
        i0.ɵɵtext(13);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementContainerEnd();
    } if (rf & 2) {
        const _r0 = i0.ɵɵreference(5);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("matMenuTriggerFor", _r0);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("matBadge", ctx.badgeCount);
        i0.ɵɵadvance(6);
        i0.ɵɵproperty("ngForOf", ctx.filter.filterData);
        i0.ɵɵadvance(3);
        i0.ɵɵtextInterpolate1(" ", ctx.tableLanguageConfig.rts_dropdown_filter, " ");
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate1(" ", ctx.tableLanguageConfig.rts_reset, " ");
    } }, directives: [i1.MatButton, i2.MatMenuTrigger, i3.MatIcon, i4.MatBadge, i2._MatMenu, i5.MatSelectionList, i6.NgForOf, i5.MatListOption], styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ColumnFilterMultComp, [{
        type: Component,
        args: [{
                selector: "via-table-column-filter-mult",
                templateUrl: "./multiple.html",
                styleUrls: ["./multiple.scss"]
            }]
    }], function () { return []; }, { matMenuTrigger: [{
            type: ViewChild,
            args: [MatMenuTrigger]
        }], matSelectionList: [{
            type: ViewChild,
            args: ["matSelectionList"]
        }], dropdownFilter: [{
            type: Output
        }], column: [{
            type: Input
        }], tableLanguageConfig: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGlwbGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3RhYmxlL2NvbHVtbi9maWx0ZXIvbXVsdGlwbGUvbXVsdGlwbGUudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3RhYmxlL2NvbHVtbi9maWx0ZXIvbXVsdGlwbGUvbXVsdGlwbGUuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsRixPQUFPLEVBQWlCLGdCQUFnQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDekUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRTFELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBRWpGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7Ozs7Ozs7Ozs7SUNXbkQsMkNBTUk7SUFEQSwwT0FBNkI7SUFDN0IsWUFDSjtJQUFBLGlCQUFrQjs7O0lBSGQsaUNBQWdCLCtCQUFBO0lBRWhCLGVBQ0o7SUFESSxnREFDSjs7QURYWixNQUFNLE9BQU8sb0JBQW9CO0lBYS9CO1FBUlUsbUJBQWMsR0FBc0MsSUFBSSxZQUFZLEVBQXVCLENBQUM7UUFNdEcsZUFBVSxHQUFXLElBQUksQ0FBQztJQUcxQixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxNQUFNLEdBQXdCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ3hELENBQUM7SUFFRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUM3QyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDOUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2hDLElBQUksS0FBSyxHQUFjLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5RixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFFekQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3ZDLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxLQUFLLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuRSxPQUFPLENBQUMsT0FBTyxHQUFHLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLEdBQUcsR0FBd0IsSUFBSSxtQkFBbUIsRUFBRSxDQUFDO1FBQ3pELEdBQUcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDbEMsR0FBRyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDdkIsR0FBRyxDQUFDLElBQUksR0FBRyxxQkFBcUIsQ0FBQyxRQUFRLENBQUE7UUFDekMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVELE9BQU87UUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNwQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDdkMsT0FBTyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLEdBQUcsR0FBd0IsSUFBSSxtQkFBbUIsRUFBRSxDQUFDO1FBQ3pELEdBQUcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDbEMsR0FBRyxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDcEIsR0FBRyxDQUFDLElBQUksR0FBRyxxQkFBcUIsQ0FBQyxRQUFRLENBQUE7UUFDekMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7d0ZBekRVLG9CQUFvQjt5REFBcEIsb0JBQW9CO3VCQUVwQixjQUFjOzs7Ozs7O1FDakIzQiw2QkFDSTtRQUFBLGlDQUtJO1FBSEEsMkdBQWMsZ0JBQVksSUFBQztRQUczQixtQ0FJeUI7UUFBQSwyQkFDekI7UUFBQSxpQkFBVztRQUNmLGlCQUFTO1FBQ1Qsc0NBR0k7UUFBQSxnREFHSTtRQURBLG1IQUFXLHdCQUF3QixJQUFDO1FBQ3BDLDZGQU1JO1FBRVIsaUJBQXFCO1FBQ3JCLDhCQUdJO1FBREEsb0dBQVcsd0JBQXdCLElBQUM7UUFDcEMsa0NBSUk7UUFEQSxrR0FBUyxjQUFVLElBQUM7UUFDcEIsYUFDSjtRQUFBLGlCQUFTO1FBQ1Qsa0NBR0k7UUFEQSxrR0FBUyxhQUFTLElBQUM7UUFDbkIsYUFDSjtRQUFBLGlCQUFTO1FBQ2IsaUJBQU07UUFDVixpQkFBVztRQUNmLDBCQUFlOzs7UUExQ1AsZUFBZ0M7UUFBaEMsdUNBQWdDO1FBSzVCLGVBQXVCO1FBQXZCLHlDQUF1QjtRQWFuQixlQUF3QztRQUF4QywrQ0FBd0M7UUFleEMsZUFDSjtRQURJLDRFQUNKO1FBSUksZUFDSjtRQURJLGtFQUNKOztrREQxQkMsb0JBQW9CO2NBTGhDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsOEJBQThCO2dCQUN4QyxXQUFXLEVBQUUsaUJBQWlCO2dCQUM5QixTQUFTLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQzthQUMvQjtzQ0FHNEIsY0FBYztrQkFBeEMsU0FBUzttQkFBQyxjQUFjO1lBQ00sZ0JBQWdCO2tCQUE5QyxTQUFTO21CQUFDLGtCQUFrQjtZQUVuQixjQUFjO2tCQUF2QixNQUFNO1lBRUUsTUFBTTtrQkFBZCxLQUFLO1lBQ0csbUJBQW1CO2tCQUEzQixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBNYXRMaXN0T3B0aW9uLCBNYXRTZWxlY3Rpb25MaXN0IH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL2xpc3RcIjtcbmltcG9ydCB7IE1hdE1lbnVUcmlnZ2VyIH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL21lbnVcIjtcbmltcG9ydCB7IGZpbmQsIGZpbmRJbmRleCB9IGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IEZJTFRFUl9PTl9DT0xVTU5fVFlQRSB9IGZyb20gXCIuLi8uLi8uLi9jb25zdGFudFwiO1xuaW1wb3J0IHsgQ29sdW1uRmlsdGVyQmFzZUR0byB9IGZyb20gXCIuLi8uLi8uLi9kdG8vZmlsdGVyL2NvbHVtbi5maWx0ZXIuYmFzZS5kdG9cIjtcbmltcG9ydCB7IENvbHVtbkZpbHRlck11bHREdG8gfSBmcm9tIFwiLi4vLi4vLi4vZHRvL2ZpbHRlci9jb2x1bW4uZmlsdGVyLm11bHQuZHRvXCI7XG5pbXBvcnQgeyBJdGVtRHRvIH0gZnJvbSBcIi4uLy4uLy4uL2R0by9maWx0ZXIvaXRlbS5kdG9cIjtcbmltcG9ydCB7IFRhYmxlQ29sdW1uRHRvIH0gZnJvbSBcIi4uLy4uLy4uL2R0by90YWJsZS5jb2x1bW4uZHRvXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtdGFibGUtY29sdW1uLWZpbHRlci1tdWx0XCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vbXVsdGlwbGUuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vbXVsdGlwbGUuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBDb2x1bW5GaWx0ZXJNdWx0Q29tcCB7XG4gIFxuICBAVmlld0NoaWxkKE1hdE1lbnVUcmlnZ2VyKSBtYXRNZW51VHJpZ2dlcjogTWF0TWVudVRyaWdnZXI7XG4gIEBWaWV3Q2hpbGQoXCJtYXRTZWxlY3Rpb25MaXN0XCIpIG1hdFNlbGVjdGlvbkxpc3Q6IE1hdFNlbGVjdGlvbkxpc3Q7XG4gIFxuICBAT3V0cHV0KCkgZHJvcGRvd25GaWx0ZXI6IEV2ZW50RW1pdHRlcjxDb2x1bW5GaWx0ZXJCYXNlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXI8Q29sdW1uRmlsdGVyQmFzZUR0bz4oKTtcblxuICBASW5wdXQoKSBjb2x1bW46IFRhYmxlQ29sdW1uRHRvO1xuICBASW5wdXQoKSB0YWJsZUxhbmd1YWdlQ29uZmlnOiBhbnk7XG5cbiAgZmlsdGVyOiBDb2x1bW5GaWx0ZXJNdWx0RHRvO1xuICBiYWRnZUNvdW50OiBudW1iZXIgPSBudWxsO1xuICBcbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmZpbHRlciA9IDxDb2x1bW5GaWx0ZXJNdWx0RHRvPnRoaXMuY29sdW1uLmZpbHRlcjtcbiAgfVxuXG4gIG1lbnVPcGVuZWQoKSB7XG4gICAgdGhpcy5tYXRTZWxlY3Rpb25MaXN0Lm9wdGlvbnMuZm9yRWFjaCgoaXRlbSkgPT4ge1xuICAgICAgbGV0IGVsZW1lbnQgPSBmaW5kKHRoaXMuZmlsdGVyLmZpbHRlckRhdGEsIGkgPT4gaS52YWx1ZSA9PT0gaXRlbS52YWx1ZS52YWx1ZSk7XG4gICAgICBpdGVtLnNlbGVjdGVkID0gZWxlbWVudC5jaGVja2VkO1xuICAgIH0pO1xuICB9XG5cbiAgZG9GaWx0ZXIoKSB7XG4gICAgdGhpcy5tYXRNZW51VHJpZ2dlci5jbG9zZU1lbnUoKTtcbiAgICBsZXQgaXRlbXM6IEl0ZW1EdG9bXSA9IHRoaXMubWF0U2VsZWN0aW9uTGlzdC5zZWxlY3RlZE9wdGlvbnMuc2VsZWN0ZWQubWFwKGl0ZW0gPT4gaXRlbS52YWx1ZSk7XG4gICAgaXRlbXMuZm9yRWFjaChpdGVtID0+IGl0ZW0uY2hlY2tlZCA9IHRydWUpO1xuICAgIHRoaXMuYmFkZ2VDb3VudCA9IGl0ZW1zLmxlbmd0aCA+IDAgPyBpdGVtcy5sZW5ndGggOiBudWxsO1xuXG4gICAgdGhpcy5maWx0ZXIuZmlsdGVyRGF0YS5mb3JFYWNoKGVsZW1lbnQgPT4ge1xuICAgICAgbGV0IGluZGV4ID0gZmluZEluZGV4KGl0ZW1zLCBpdGVtID0+IGl0ZW0udmFsdWUgPT09IGVsZW1lbnQudmFsdWUpO1xuICAgICAgZWxlbWVudC5jaGVja2VkID0gKGluZGV4ICE9PSAtMSk7XG4gICAgfSk7XG5cbiAgICBsZXQgcmVxOiBDb2x1bW5GaWx0ZXJNdWx0RHRvID0gbmV3IENvbHVtbkZpbHRlck11bHREdG8oKTtcbiAgICByZXEuY29sdW1uTmFtZSA9IHRoaXMuY29sdW1uLm5hbWU7XG4gICAgcmVxLmZpbHRlckRhdGEgPSBpdGVtcztcbiAgICByZXEudHlwZSA9IEZJTFRFUl9PTl9DT0xVTU5fVFlQRS5NVUxUSVBMRVxuICAgIHRoaXMuZHJvcGRvd25GaWx0ZXIuZW1pdChyZXEpO1xuICB9XG5cbiAgZG9SZXNldCgpIHtcbiAgICB0aGlzLm1hdE1lbnVUcmlnZ2VyLmNsb3NlTWVudSgpO1xuICAgIHRoaXMubWF0U2VsZWN0aW9uTGlzdC5kZXNlbGVjdEFsbCgpO1xuICAgIHRoaXMuYmFkZ2VDb3VudCA9IG51bGw7XG4gICAgdGhpcy5maWx0ZXIuZmlsdGVyRGF0YS5mb3JFYWNoKGVsZW1lbnQgPT4ge1xuICAgICAgZWxlbWVudC5jaGVja2VkID0gZmFsc2U7XG4gICAgfSk7XG4gICAgbGV0IHJlcTogQ29sdW1uRmlsdGVyTXVsdER0byA9IG5ldyBDb2x1bW5GaWx0ZXJNdWx0RHRvKCk7XG4gICAgcmVxLmNvbHVtbk5hbWUgPSB0aGlzLmNvbHVtbi5uYW1lO1xuICAgIHJlcS5maWx0ZXJEYXRhID0gW107XG4gICAgcmVxLnR5cGUgPSBGSUxURVJfT05fQ09MVU1OX1RZUEUuTVVMVElQTEVcbiAgICB0aGlzLmRyb3Bkb3duRmlsdGVyLmVtaXQocmVxKTtcbiAgfVxuXG59XG4iLCI8bmctY29udGFpbmVyPlxuICAgIDxidXR0b25cbiAgICAgICAgW21hdE1lbnVUcmlnZ2VyRm9yXT1cImZpbHRlck1lbnVcIiBcbiAgICAgICAgKG1lbnVPcGVuZWQpPVwibWVudU9wZW5lZCgpXCJcbiAgICAgICAgbWF0LWljb24tYnV0dG9uIFxuICAgICAgICBjb2xvcj1cInByaW1hcnlcIj5cbiAgICAgICAgPG1hdC1pY29uXG4gICAgICAgICAgICBbbWF0QmFkZ2VdPVwiYmFkZ2VDb3VudFwiIFxuICAgICAgICAgICAgbWF0QmFkZ2VDb2xvcj1cIndhcm5cIiBcbiAgICAgICAgICAgIG1hdEJhZGdlT3ZlcmxhcD1cInRydWVcIiBcbiAgICAgICAgICAgIG1hdEJhZGdlU2l6ZT1cInNtYWxsXCI+ZmlsdGVyX2FsdFxuICAgICAgICA8L21hdC1pY29uPlxuICAgIDwvYnV0dG9uPlxuICAgIDxtYXQtbWVudSBcbiAgICAgICAgI2ZpbHRlck1lbnU9XCJtYXRNZW51XCIgXG4gICAgICAgIG92ZXJsYXBUcmlnZ2VyPVwidHJ1ZVwiPlxuICAgICAgICA8bWF0LXNlbGVjdGlvbi1saXN0IFxuICAgICAgICAgICAgI21hdFNlbGVjdGlvbkxpc3QgXG4gICAgICAgICAgICAoY2xpY2spID0gXCIkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKClcIj5cbiAgICAgICAgICAgIDxtYXQtbGlzdC1vcHRpb25cbiAgICAgICAgICAgICAgICAqbmdGb3I9XCJsZXQgZmlsdGVyIG9mIGZpbHRlci5maWx0ZXJEYXRhXCJcbiAgICAgICAgICAgICAgICBjaGVja2JveFBvc2l0aW9uPVwiYmVmb3JlXCJcbiAgICAgICAgICAgICAgICBjb2xvcj1cIndhcm5cIlxuICAgICAgICAgICAgICAgIFt2YWx1ZV09XCJmaWx0ZXJcIlxuICAgICAgICAgICAgICAgIFsoc2VsZWN0ZWQpXT1cImZpbHRlci5jaGVja2VkXCI+XG4gICAgICAgICAgICAgICAge3tmaWx0ZXIudGl0bGV9fVxuICAgICAgICAgICAgPC9tYXQtbGlzdC1vcHRpb24+XG4gICAgICAgIDwvbWF0LXNlbGVjdGlvbi1saXN0PlxuICAgICAgICA8ZGl2IFxuICAgICAgICAgICAgc3R5bGU9XCJkaXNwbGF5OiBmbGV4OyBtYXJnaW4tdG9wOiAxMHB4OyBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcIlxuICAgICAgICAgICAgKGNsaWNrKSA9IFwiJGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpXCI+XG4gICAgICAgICAgICA8YnV0dG9uIFxuICAgICAgICAgICAgICAgIG1hdC1mbGF0LWJ1dHRvbiBcbiAgICAgICAgICAgICAgICBjb2xvcj1cInByaW1hcnlcIiBcbiAgICAgICAgICAgICAgICAoY2xpY2spPVwiZG9GaWx0ZXIoKVwiPlxuICAgICAgICAgICAgICAgIHt7dGFibGVMYW5ndWFnZUNvbmZpZy5ydHNfZHJvcGRvd25fZmlsdGVyfX1cbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgICAgICBtYXQtZmxhdC1idXR0b24gXG4gICAgICAgICAgICAgICAgKGNsaWNrKT1cImRvUmVzZXQoKVwiPlxuICAgICAgICAgICAgICAgIHt7dGFibGVMYW5ndWFnZUNvbmZpZy5ydHNfcmVzZXR9fVxuICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvbWF0LW1lbnU+XG48L25nLWNvbnRhaW5lcj4iXX0=