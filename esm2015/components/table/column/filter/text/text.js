import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { MatMenuTrigger } from "@angular/material/menu";
import { FILTER_ON_COLUMN_TYPE } from "../../../constant";
import { ColumnFilterTextDto } from "../../../dto/filter/column.filter.text.dto";
import { TableColumnDto } from "../../../dto/table.column.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/material/menu";
import * as i3 from "@angular/material/icon";
import * as i4 from "@angular/material/badge";
import * as i5 from "@angular/material/form-field";
import * as i6 from "@angular/material/input";
import * as i7 from "@angular/forms";
export class ColumnFilterTextComp {
    constructor() {
        this.dropdownFilter = new EventEmitter();
        this.searchStr = "";
        this.badgeCount = null;
    }
    ngOnInit() {
        this.filter = this.column.filter;
        this.searchStr = this.filter.searchStr;
    }
    menuOpened() {
        this.searchStr = this.filter.searchStr;
    }
    doFilter() {
        this.matMenuTrigger.closeMenu();
        this.filter.searchStr = this.searchStr;
        this.badgeCount = this.searchStr.length > 0 ? 1 : null;
        let req = new ColumnFilterTextDto();
        req.columnName = this.column.name;
        req.searchStr = this.filter.searchStr;
        req.type = FILTER_ON_COLUMN_TYPE.TEXT;
        this.dropdownFilter.emit(req);
    }
    doReset() {
        this.matMenuTrigger.closeMenu();
        this.filter.searchStr = "";
        this.badgeCount = null;
        this.searchStr = "";
        let req = new ColumnFilterTextDto();
        req.columnName = this.column.name;
        req.searchStr = "";
        req.type = FILTER_ON_COLUMN_TYPE.TEXT;
        this.dropdownFilter.emit(req);
    }
}
ColumnFilterTextComp.ɵfac = function ColumnFilterTextComp_Factory(t) { return new (t || ColumnFilterTextComp)(); };
ColumnFilterTextComp.ɵcmp = i0.ɵɵdefineComponent({ type: ColumnFilterTextComp, selectors: [["via-table-column-filter-text"]], viewQuery: function ColumnFilterTextComp_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(MatMenuTrigger, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.matMenuTrigger = _t.first);
    } }, inputs: { column: "column", tableLanguageConfig: "tableLanguageConfig" }, outputs: { dropdownFilter: "dropdownFilter" }, decls: 13, vars: 5, consts: [["mat-icon-button", "", "color", "primary", 3, "matMenuTriggerFor", "menuOpened"], ["matBadgeColor", "warn", "matBadgeOverlap", "true", "matBadgeSize", "small", 3, "matBadge"], ["overlapTrigger", "true"], ["filterMenu", "matMenu"], ["appearance", "standard", 2, "margin", "0 10px", 3, "click"], ["matInput", "", 3, "ngModel", "ngModelChange"], [2, "display", "flex", "justify-content", "space-evenly", 3, "click"], ["mat-flat-button", "", "color", "primary", 3, "click"], ["mat-flat-button", "", 3, "click"]], template: function ColumnFilterTextComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementContainerStart(0);
        i0.ɵɵelementStart(1, "button", 0);
        i0.ɵɵlistener("menuOpened", function ColumnFilterTextComp_Template_button_menuOpened_1_listener() { return ctx.menuOpened(); });
        i0.ɵɵelementStart(2, "mat-icon", 1);
        i0.ɵɵtext(3, "search ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(4, "mat-menu", 2, 3);
        i0.ɵɵelementStart(6, "mat-form-field", 4);
        i0.ɵɵlistener("click", function ColumnFilterTextComp_Template_mat_form_field_click_6_listener($event) { return $event.stopPropagation(); });
        i0.ɵɵelementStart(7, "input", 5);
        i0.ɵɵlistener("ngModelChange", function ColumnFilterTextComp_Template_input_ngModelChange_7_listener($event) { return ctx.searchStr = $event; });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "div", 6);
        i0.ɵɵlistener("click", function ColumnFilterTextComp_Template_div_click_8_listener($event) { return $event.stopPropagation(); });
        i0.ɵɵelementStart(9, "button", 7);
        i0.ɵɵlistener("click", function ColumnFilterTextComp_Template_button_click_9_listener() { return ctx.doFilter(); });
        i0.ɵɵtext(10);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(11, "button", 8);
        i0.ɵɵlistener("click", function ColumnFilterTextComp_Template_button_click_11_listener() { return ctx.doReset(); });
        i0.ɵɵtext(12);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementContainerEnd();
    } if (rf & 2) {
        const _r0 = i0.ɵɵreference(5);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("matMenuTriggerFor", _r0);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("matBadge", ctx.badgeCount);
        i0.ɵɵadvance(5);
        i0.ɵɵproperty("ngModel", ctx.searchStr);
        i0.ɵɵadvance(3);
        i0.ɵɵtextInterpolate1(" ", ctx.tableLanguageConfig.rts_dropdown_filter, " ");
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate1(" ", ctx.tableLanguageConfig.rts_reset, " ");
    } }, directives: [i1.MatButton, i2.MatMenuTrigger, i3.MatIcon, i4.MatBadge, i2._MatMenu, i5.MatFormField, i6.MatInput, i7.DefaultValueAccessor, i7.NgControlStatus, i7.NgModel], styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ColumnFilterTextComp, [{
        type: Component,
        args: [{
                selector: "via-table-column-filter-text",
                templateUrl: "./text.html",
                styleUrls: ["./text.scss"]
            }]
    }], function () { return []; }, { matMenuTrigger: [{
            type: ViewChild,
            args: [MatMenuTrigger]
        }], dropdownFilter: [{
            type: Output
        }], column: [{
            type: Input
        }], tableLanguageConfig: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdGFibGUvY29sdW1uL2ZpbHRlci90ZXh0L3RleHQudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3RhYmxlL2NvbHVtbi9maWx0ZXIvdGV4dC90ZXh0Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbEYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRTFELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7Ozs7Ozs7O0FBTy9ELE1BQU0sT0FBTyxvQkFBb0I7SUFhL0I7UUFUVSxtQkFBYyxHQUFzQyxJQUFJLFlBQVksRUFBdUIsQ0FBQztRQU10RyxjQUFTLEdBQVcsRUFBRSxDQUFDO1FBQ3ZCLGVBQVUsR0FBVyxJQUFJLENBQUM7SUFHMUIsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsTUFBTSxHQUF3QixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUN0RCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO0lBQ3pDLENBQUM7SUFFRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN2QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDdkQsSUFBSSxHQUFHLEdBQXdCLElBQUksbUJBQW1CLEVBQUUsQ0FBQztRQUN6RCxHQUFHLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2xDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7UUFDdEMsR0FBRyxDQUFDLElBQUksR0FBRyxxQkFBcUIsQ0FBQyxJQUFJLENBQUE7UUFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVELE9BQU87UUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLEdBQUcsR0FBd0IsSUFBSSxtQkFBbUIsRUFBRSxDQUFDO1FBQ3pELEdBQUcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDbEMsR0FBRyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDbkIsR0FBRyxDQUFDLElBQUksR0FBRyxxQkFBcUIsQ0FBQyxJQUFJLENBQUE7UUFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7d0ZBOUNVLG9CQUFvQjt5REFBcEIsb0JBQW9CO3VCQUVwQixjQUFjOzs7OztRQ2QzQiw2QkFDSTtRQUFBLGlDQUtJO1FBSEEsMkdBQWMsZ0JBQVksSUFBQztRQUczQixtQ0FJeUI7UUFBQSx1QkFDekI7UUFBQSxpQkFBVztRQUNmLGlCQUFTO1FBQ1Qsc0NBR0k7UUFBQSx5Q0FHSTtRQURBLCtHQUFXLHdCQUF3QixJQUFDO1FBRXBDLGdDQUVKO1FBRFEsZ0pBQXVCO1FBRDNCLGlCQUVKO1FBQUEsaUJBQWlCO1FBQ2pCLDhCQUdJO1FBREEsb0dBQVcsd0JBQXdCLElBQUM7UUFDcEMsaUNBSUk7UUFEQSxpR0FBUyxjQUFVLElBQUM7UUFDcEIsYUFDSjtRQUFBLGlCQUFTO1FBQ1Qsa0NBR0k7UUFEQSxrR0FBUyxhQUFTLElBQUM7UUFDbkIsYUFDSjtRQUFBLGlCQUFTO1FBQ2IsaUJBQU07UUFDVixpQkFBVztRQUNmLDBCQUFlOzs7UUFyQ1AsZUFBZ0M7UUFBaEMsdUNBQWdDO1FBSzVCLGVBQXVCO1FBQXZCLHlDQUF1QjtRQWNuQixlQUF1QjtRQUF2Qix1Q0FBdUI7UUFTdkIsZUFDSjtRQURJLDRFQUNKO1FBSUksZUFDSjtRQURJLGtFQUNKOztrRER4QkMsb0JBQW9CO2NBTGhDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsOEJBQThCO2dCQUN4QyxXQUFXLEVBQUUsYUFBYTtnQkFDMUIsU0FBUyxFQUFFLENBQUMsYUFBYSxDQUFDO2FBQzNCO3NDQUc0QixjQUFjO2tCQUF4QyxTQUFTO21CQUFDLGNBQWM7WUFFZixjQUFjO2tCQUF2QixNQUFNO1lBRUUsTUFBTTtrQkFBZCxLQUFLO1lBQ0csbUJBQW1CO2tCQUEzQixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBNYXRNZW51VHJpZ2dlciB9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9tZW51XCI7XG5pbXBvcnQgeyBGSUxURVJfT05fQ09MVU1OX1RZUEUgfSBmcm9tIFwiLi4vLi4vLi4vY29uc3RhbnRcIjtcbmltcG9ydCB7IENvbHVtbkZpbHRlckJhc2VEdG8gfSBmcm9tIFwiLi4vLi4vLi4vZHRvL2ZpbHRlci9jb2x1bW4uZmlsdGVyLmJhc2UuZHRvXCI7XG5pbXBvcnQgeyBDb2x1bW5GaWx0ZXJUZXh0RHRvIH0gZnJvbSBcIi4uLy4uLy4uL2R0by9maWx0ZXIvY29sdW1uLmZpbHRlci50ZXh0LmR0b1wiO1xuaW1wb3J0IHsgVGFibGVDb2x1bW5EdG8gfSBmcm9tIFwiLi4vLi4vLi4vZHRvL3RhYmxlLmNvbHVtbi5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10YWJsZS1jb2x1bW4tZmlsdGVyLXRleHRcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi90ZXh0Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3RleHQuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBDb2x1bW5GaWx0ZXJUZXh0Q29tcCB7XG4gIFxuICBAVmlld0NoaWxkKE1hdE1lbnVUcmlnZ2VyKSBtYXRNZW51VHJpZ2dlcjogTWF0TWVudVRyaWdnZXI7XG4gIFxuICBAT3V0cHV0KCkgZHJvcGRvd25GaWx0ZXI6IEV2ZW50RW1pdHRlcjxDb2x1bW5GaWx0ZXJCYXNlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXI8Q29sdW1uRmlsdGVyQmFzZUR0bz4oKTtcblxuICBASW5wdXQoKSBjb2x1bW46IFRhYmxlQ29sdW1uRHRvO1xuICBASW5wdXQoKSB0YWJsZUxhbmd1YWdlQ29uZmlnOiBhbnk7XG4gIFxuICBmaWx0ZXI6IENvbHVtbkZpbHRlclRleHREdG87XG4gIHNlYXJjaFN0cjogc3RyaW5nID0gXCJcIjtcbiAgYmFkZ2VDb3VudDogbnVtYmVyID0gbnVsbDtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuZmlsdGVyID0gPENvbHVtbkZpbHRlclRleHREdG8+dGhpcy5jb2x1bW4uZmlsdGVyO1xuICAgIHRoaXMuc2VhcmNoU3RyID0gdGhpcy5maWx0ZXIuc2VhcmNoU3RyO1xuICB9XG5cbiAgbWVudU9wZW5lZCgpIHtcbiAgICB0aGlzLnNlYXJjaFN0ciA9IHRoaXMuZmlsdGVyLnNlYXJjaFN0cjtcbiAgfVxuXG4gIGRvRmlsdGVyKCkge1xuICAgIHRoaXMubWF0TWVudVRyaWdnZXIuY2xvc2VNZW51KCk7XG4gICAgdGhpcy5maWx0ZXIuc2VhcmNoU3RyID0gdGhpcy5zZWFyY2hTdHI7XG4gICAgdGhpcy5iYWRnZUNvdW50ID0gdGhpcy5zZWFyY2hTdHIubGVuZ3RoID4gMCA/IDEgOiBudWxsO1xuICAgIGxldCByZXE6IENvbHVtbkZpbHRlclRleHREdG8gPSBuZXcgQ29sdW1uRmlsdGVyVGV4dER0bygpO1xuICAgIHJlcS5jb2x1bW5OYW1lID0gdGhpcy5jb2x1bW4ubmFtZTtcbiAgICByZXEuc2VhcmNoU3RyID0gdGhpcy5maWx0ZXIuc2VhcmNoU3RyO1xuICAgIHJlcS50eXBlID0gRklMVEVSX09OX0NPTFVNTl9UWVBFLlRFWFRcbiAgICB0aGlzLmRyb3Bkb3duRmlsdGVyLmVtaXQocmVxKTtcbiAgfVxuXG4gIGRvUmVzZXQoKSB7XG4gICAgdGhpcy5tYXRNZW51VHJpZ2dlci5jbG9zZU1lbnUoKTtcbiAgICB0aGlzLmZpbHRlci5zZWFyY2hTdHIgPSBcIlwiO1xuICAgIHRoaXMuYmFkZ2VDb3VudCA9IG51bGw7XG4gICAgdGhpcy5zZWFyY2hTdHIgPSBcIlwiO1xuICAgIGxldCByZXE6IENvbHVtbkZpbHRlclRleHREdG8gPSBuZXcgQ29sdW1uRmlsdGVyVGV4dER0bygpO1xuICAgIHJlcS5jb2x1bW5OYW1lID0gdGhpcy5jb2x1bW4ubmFtZTtcbiAgICByZXEuc2VhcmNoU3RyID0gXCJcIjtcbiAgICByZXEudHlwZSA9IEZJTFRFUl9PTl9DT0xVTU5fVFlQRS5URVhUXG4gICAgdGhpcy5kcm9wZG93bkZpbHRlci5lbWl0KHJlcSk7XG4gIH1cbn1cbiIsIjxuZy1jb250YWluZXI+XG4gICAgPGJ1dHRvblxuICAgICAgICBbbWF0TWVudVRyaWdnZXJGb3JdPVwiZmlsdGVyTWVudVwiIFxuICAgICAgICAobWVudU9wZW5lZCk9XCJtZW51T3BlbmVkKClcIlxuICAgICAgICBtYXQtaWNvbi1idXR0b24gXG4gICAgICAgIGNvbG9yPVwicHJpbWFyeVwiPlxuICAgICAgICA8bWF0LWljb25cbiAgICAgICAgICAgIFttYXRCYWRnZV09XCJiYWRnZUNvdW50XCJcbiAgICAgICAgICAgIG1hdEJhZGdlQ29sb3I9XCJ3YXJuXCIgXG4gICAgICAgICAgICBtYXRCYWRnZU92ZXJsYXA9XCJ0cnVlXCIgXG4gICAgICAgICAgICBtYXRCYWRnZVNpemU9XCJzbWFsbFwiPnNlYXJjaFxuICAgICAgICA8L21hdC1pY29uPlxuICAgIDwvYnV0dG9uPlxuICAgIDxtYXQtbWVudSBcbiAgICAgICAgI2ZpbHRlck1lbnU9XCJtYXRNZW51XCIgXG4gICAgICAgIG92ZXJsYXBUcmlnZ2VyPVwidHJ1ZVwiPlxuICAgICAgICA8bWF0LWZvcm0tZmllbGQgYXBwZWFyYW5jZT1cInN0YW5kYXJkXCJcbiAgICAgICAgICAgIHN0eWxlPVwibWFyZ2luOiAwIDEwcHg7XCJcbiAgICAgICAgICAgIChjbGljaykgPSBcIiRldmVudC5zdG9wUHJvcGFnYXRpb24oKVwiPlxuICAgICAgICAgICAgPCEtLSA8bWF0LWxhYmVsPnt7dGFibGVMYW5ndWFnZUNvbmZpZy5jb2x1bW5fZmlsdGVyX3R5cGVfdGV4dF9sYWJsZX19PC9tYXQtbGFiZWw+IC0tPlxuICAgICAgICAgICAgPGlucHV0IG1hdElucHV0XG4gICAgICAgICAgICAgICAgWyhuZ01vZGVsKV09XCJzZWFyY2hTdHJcIj5cbiAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cbiAgICAgICAgPGRpdlxuICAgICAgICAgICAgc3R5bGU9XCJkaXNwbGF5OiBmbGV4OyBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcIiBcbiAgICAgICAgICAgIChjbGljaykgPSBcIiRldmVudC5zdG9wUHJvcGFnYXRpb24oKVwiPlxuICAgICAgICAgICAgPGJ1dHRvbiBcbiAgICAgICAgICAgICAgICBtYXQtZmxhdC1idXR0b24gXG4gICAgICAgICAgICAgICAgY29sb3I9XCJwcmltYXJ5XCIgXG4gICAgICAgICAgICAgICAgKGNsaWNrKT1cImRvRmlsdGVyKClcIj5cbiAgICAgICAgICAgICAgICB7e3RhYmxlTGFuZ3VhZ2VDb25maWcucnRzX2Ryb3Bkb3duX2ZpbHRlcn19XG4gICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgIDxidXR0b24gXG4gICAgICAgICAgICAgICAgbWF0LWZsYXQtYnV0dG9uIFxuICAgICAgICAgICAgICAgIChjbGljayk9XCJkb1Jlc2V0KClcIj5cbiAgICAgICAgICAgICAgICB7e3RhYmxlTGFuZ3VhZ2VDb25maWcucnRzX3Jlc2V0fX1cbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICA8L2Rpdj5cbiAgICA8L21hdC1tZW51PlxuPC9uZy1jb250YWluZXI+Il19