import { FILTER_ON_COLUMN_TYPE } from "../../constant";
import { ColumnFilterBaseDto } from "./column.filter.base.dto";
export class ColumnFilterMultDto extends ColumnFilterBaseDto {
    constructor() {
        super();
        this.type = FILTER_ON_COLUMN_TYPE.MULTIPLE;
        this.filterData = [];
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLmZpbHRlci5tdWx0LmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdGFibGUvZHRvL2ZpbHRlci9jb2x1bW4uZmlsdGVyLm11bHQuZHRvLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRy9ELE1BQU0sT0FBTyxtQkFBb0IsU0FBUSxtQkFBbUI7SUFHeEQ7UUFDSSxLQUFLLEVBQUUsQ0FBQztRQUNSLElBQUksQ0FBQyxJQUFJLEdBQUcscUJBQXFCLENBQUMsUUFBUSxDQUFDO1FBQzNDLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO0lBQ3pCLENBQUM7Q0FFSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZJTFRFUl9PTl9DT0xVTU5fVFlQRSB9IGZyb20gXCIuLi8uLi9jb25zdGFudFwiO1xuaW1wb3J0IHsgQ29sdW1uRmlsdGVyQmFzZUR0byB9IGZyb20gXCIuL2NvbHVtbi5maWx0ZXIuYmFzZS5kdG9cIjtcbmltcG9ydCB7IEl0ZW1EdG8gfSBmcm9tIFwiLi9pdGVtLmR0b1wiO1xuXG5leHBvcnQgY2xhc3MgQ29sdW1uRmlsdGVyTXVsdER0byBleHRlbmRzIENvbHVtbkZpbHRlckJhc2VEdG97XG4gICAgZmlsdGVyRGF0YTogSXRlbUR0b1tdO1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgICAgIHRoaXMudHlwZSA9IEZJTFRFUl9PTl9DT0xVTU5fVFlQRS5NVUxUSVBMRTtcbiAgICAgICAgdGhpcy5maWx0ZXJEYXRhID0gW107XG4gICAgfVxuXG59Il19