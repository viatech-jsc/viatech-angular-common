export class TableBtnActionDto {
    constructor(name = "", callbackFunc, disabled = false, iconUrl = undefined, isDropdownItem = false) {
        this._isDropdownItem = false;
        this._name = name;
        this._buttonAction = callbackFunc;
        this._disabled = disabled;
        this._iconUrl = iconUrl;
        this._isDropdownItem = isDropdownItem;
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter buttonAction
     * return {Function}
     */
    get buttonAction() {
        return this._buttonAction;
    }
    /**
     * Getter disabled
     * return {boolean}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * Getter iconUrl
     * return {string}
     */
    get iconUrl() {
        return this._iconUrl;
    }
    /**
     * Getter isDropdownItem
     * return {boolean }
     */
    get isDropdownItem() {
        return this._isDropdownItem;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter buttonAction
     * param {Function} value
     */
    set buttonAction(value) {
        this._buttonAction = value;
    }
    /**
     * Setter disabled
     * param {boolean} value
     */
    set disabled(value) {
        this._disabled = value;
    }
    /**
     * Setter iconUrl
     * param {string} value
     */
    set iconUrl(value) {
        this._iconUrl = value;
    }
    /**
     * Setter isDropdownItem
     * param {boolean } value
     */
    set isDropdownItem(value) {
        this._isDropdownItem = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuYnRuLmFjdGlvbi5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3RhYmxlL2R0by90YWJsZS5idG4uYWN0aW9uLmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxNQUFNLE9BQU8saUJBQWlCO0lBTzFCLFlBQVksSUFBSSxHQUFHLEVBQUUsRUFBRSxZQUF3QyxFQUFFLFdBQW9CLEtBQUssRUFBRSxVQUFrQixTQUFTLEVBQUUsaUJBQTBCLEtBQUs7UUFGaEosb0JBQWUsR0FBWSxLQUFLLENBQUM7UUFHckMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUM7UUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7UUFDeEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUM7SUFDMUMsQ0FBQztJQUdEOzs7T0FHRztJQUNOLElBQVcsSUFBSTtRQUNkLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxZQUFZO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUMzQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxRQUFRO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN2QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxPQUFPO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN0QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxjQUFjO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUM3QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJLENBQUMsS0FBYTtRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxZQUFZLENBQUMsS0FBZTtRQUN0QyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxRQUFRLENBQUMsS0FBYztRQUNqQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxPQUFPLENBQUMsS0FBYTtRQUMvQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxjQUFjLENBQUMsS0FBYztRQUN2QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUM5QixDQUFDO0NBRUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUYWJsZVJvd0R0byB9IGZyb20gXCIuL3RhYmxlLnJvdy5kdG9cIjtcblxuZXhwb3J0IGNsYXNzIFRhYmxlQnRuQWN0aW9uRHRvIHtcbiAgICBwcml2YXRlIF9uYW1lOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfYnV0dG9uQWN0aW9uOiBGdW5jdGlvbjtcbiAgICBwcml2YXRlIF9kaXNhYmxlZDogYm9vbGVhbjtcbiAgICBwcml2YXRlIF9pY29uVXJsOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfaXNEcm9wZG93bkl0ZW06IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKG5hbWUgPSBcIlwiLCBjYWxsYmFja0Z1bmM6IChyb3c6IFRhYmxlUm93RHRvKSA9PiB2b2lkLCBkaXNhYmxlZDogYm9vbGVhbiA9IGZhbHNlLCBpY29uVXJsOiBzdHJpbmcgPSB1bmRlZmluZWQsIGlzRHJvcGRvd25JdGVtOiBib29sZWFuID0gZmFsc2Upe1xuICAgICAgICB0aGlzLl9uYW1lID0gbmFtZTtcbiAgICAgICAgdGhpcy5fYnV0dG9uQWN0aW9uID0gY2FsbGJhY2tGdW5jO1xuICAgICAgICB0aGlzLl9kaXNhYmxlZCA9IGRpc2FibGVkO1xuICAgICAgICB0aGlzLl9pY29uVXJsID0gaWNvblVybDtcbiAgICAgICAgdGhpcy5faXNEcm9wZG93bkl0ZW0gPSBpc0Ryb3Bkb3duSXRlbTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBuYW1lXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgbmFtZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9uYW1lO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgYnV0dG9uQWN0aW9uXG4gICAgICogcmV0dXJuIHtGdW5jdGlvbn1cbiAgICAgKi9cblx0cHVibGljIGdldCBidXR0b25BY3Rpb24oKTogRnVuY3Rpb24ge1xuXHRcdHJldHVybiB0aGlzLl9idXR0b25BY3Rpb247XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBkaXNhYmxlZFxuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cblx0cHVibGljIGdldCBkaXNhYmxlZCgpOiBib29sZWFuIHtcblx0XHRyZXR1cm4gdGhpcy5fZGlzYWJsZWQ7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBpY29uVXJsXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgaWNvblVybCgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9pY29uVXJsO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgaXNEcm9wZG93bkl0ZW1cbiAgICAgKiByZXR1cm4ge2Jvb2xlYW4gfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGlzRHJvcGRvd25JdGVtKCk6IGJvb2xlYW4gIHtcblx0XHRyZXR1cm4gdGhpcy5faXNEcm9wZG93bkl0ZW07XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBuYW1lXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBuYW1lKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9uYW1lID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBidXR0b25BY3Rpb25cbiAgICAgKiBwYXJhbSB7RnVuY3Rpb259IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgYnV0dG9uQWN0aW9uKHZhbHVlOiBGdW5jdGlvbikge1xuXHRcdHRoaXMuX2J1dHRvbkFjdGlvbiA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgZGlzYWJsZWRcbiAgICAgKiBwYXJhbSB7Ym9vbGVhbn0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBkaXNhYmxlZCh2YWx1ZTogYm9vbGVhbikge1xuXHRcdHRoaXMuX2Rpc2FibGVkID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBpY29uVXJsXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBpY29uVXJsKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9pY29uVXJsID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBpc0Ryb3Bkb3duSXRlbVxuICAgICAqIHBhcmFtIHtib29sZWFuIH0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBpc0Ryb3Bkb3duSXRlbSh2YWx1ZTogYm9vbGVhbiApIHtcblx0XHR0aGlzLl9pc0Ryb3Bkb3duSXRlbSA9IHZhbHVlO1xuXHR9XG5cbn1cbiJdfQ==