import { CELL_TYPE } from "../constant";
export class TableCellDto {
    constructor(value = "", css = "", type = CELL_TYPE.STRING, routeLink = "", buttonActions = [], buttonDropdownActions = []) {
        this._value = value;
        this._css = css;
        this._type = type;
        this._routeLink = routeLink;
        this._buttonActions = buttonActions;
        this._buttonDropdownActions = buttonDropdownActions;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter type
     * return {string}
     */
    get type() {
        return this._type;
    }
    /**
     * Getter routeLink
     * return {string}
     */
    get routeLink() {
        return this._routeLink;
    }
    /**
     * Getter buttonActions
     * return {ButtonAction[]}
     */
    get buttonActions() {
        return this._buttonActions;
    }
    /**
     * Getter buttonDropdownActions
     * return {ButtonAction[]}
     */
    get buttonDropdownActions() {
        return this._buttonDropdownActions;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter type
     * param {string} value
     */
    set type(value) {
        this._type = value;
    }
    /**
     * Setter routeLink
     * param {string} value
     */
    set routeLink(value) {
        this._routeLink = value;
    }
    /**
     * Setter buttonActions
     * param {ButtonAction[]} value
     */
    set buttonActions(value) {
        this._buttonActions = value;
    }
    /**
     * Setter buttonDropdownActions
     * param {ButtonAction[]} value
     */
    set buttonDropdownActions(value) {
        this._buttonDropdownActions = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY2VsbC5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3RhYmxlL2R0by90YWJsZS5jZWxsLmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBR3hDLE1BQU0sT0FBTyxZQUFZO0lBUXBCLFlBQVksS0FBSyxHQUFHLEVBQUUsRUFBRSxHQUFHLEdBQUcsRUFBRSxFQUFFLElBQUksR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLFNBQVMsR0FBRyxFQUFFLEVBQUUsYUFBYSxHQUFHLEVBQUUsRUFBRSxxQkFBcUIsR0FBRyxFQUFFO1FBQ3RILElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1FBQzVCLElBQUksQ0FBQyxjQUFjLEdBQUcsYUFBYSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxxQkFBcUIsQ0FBQztJQUN4RCxDQUFDO0lBRUQ7OztPQUdHO0lBQ04sSUFBVyxLQUFLO1FBQ2YsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3BCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLEdBQUc7UUFDYixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDbEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsSUFBSTtRQUNkLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxTQUFTO1FBQ25CLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN4QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxhQUFhO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUM1QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxxQkFBcUI7UUFDL0IsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUM7SUFDcEMsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsS0FBSyxDQUFDLEtBQWE7UUFDN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsR0FBRyxDQUFDLEtBQWE7UUFDM0IsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7SUFDbkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsSUFBSSxDQUFDLEtBQWE7UUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDcEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsU0FBUyxDQUFDLEtBQWE7UUFDakMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsYUFBYSxDQUFDLEtBQTBCO1FBQ2xELElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLHFCQUFxQixDQUFDLEtBQTBCO1FBQzFELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7SUFDckMsQ0FBQztDQUdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ0VMTF9UWVBFIH0gZnJvbSBcIi4uL2NvbnN0YW50XCI7XG5pbXBvcnQgeyBUYWJsZUJ0bkFjdGlvbkR0byB9IGZyb20gXCIuL3RhYmxlLmJ0bi5hY3Rpb24uZHRvXCI7XG5cbmV4cG9ydCBjbGFzcyBUYWJsZUNlbGxEdG97XG4gICAgcHJpdmF0ZSBfdmFsdWU6IHN0cmluZztcbiAgICBwcml2YXRlIF9jc3M6IHN0cmluZztcbiAgICBwcml2YXRlIF90eXBlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfcm91dGVMaW5rOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfYnV0dG9uQWN0aW9uczogVGFibGVCdG5BY3Rpb25EdG9bXTtcbiAgICBwcml2YXRlIF9idXR0b25Ecm9wZG93bkFjdGlvbnM6IFRhYmxlQnRuQWN0aW9uRHRvW107XG5cbiAgICAgY29uc3RydWN0b3IodmFsdWUgPSBcIlwiLCBjc3MgPSBcIlwiLCB0eXBlID0gQ0VMTF9UWVBFLlNUUklORywgcm91dGVMaW5rID0gXCJcIiwgYnV0dG9uQWN0aW9ucyA9IFtdLCBidXR0b25Ecm9wZG93bkFjdGlvbnMgPSBbXSl7XG4gICAgICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7XG4gICAgICAgIHRoaXMuX2NzcyA9IGNzcztcbiAgICAgICAgdGhpcy5fdHlwZSA9IHR5cGU7XG4gICAgICAgIHRoaXMuX3JvdXRlTGluayA9IHJvdXRlTGluaztcbiAgICAgICAgdGhpcy5fYnV0dG9uQWN0aW9ucyA9IGJ1dHRvbkFjdGlvbnM7XG4gICAgICAgIHRoaXMuX2J1dHRvbkRyb3Bkb3duQWN0aW9ucyA9IGJ1dHRvbkRyb3Bkb3duQWN0aW9ucztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdmFsdWVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCB2YWx1ZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl92YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGNzc1xuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGNzcygpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9jc3M7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB0eXBlXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdHlwZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl90eXBlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgcm91dGVMaW5rXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgcm91dGVMaW5rKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3JvdXRlTGluaztcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGJ1dHRvbkFjdGlvbnNcbiAgICAgKiByZXR1cm4ge0J1dHRvbkFjdGlvbltdfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGJ1dHRvbkFjdGlvbnMoKTogVGFibGVCdG5BY3Rpb25EdG9bXSB7XG5cdFx0cmV0dXJuIHRoaXMuX2J1dHRvbkFjdGlvbnM7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBidXR0b25Ecm9wZG93bkFjdGlvbnNcbiAgICAgKiByZXR1cm4ge0J1dHRvbkFjdGlvbltdfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGJ1dHRvbkRyb3Bkb3duQWN0aW9ucygpOiBUYWJsZUJ0bkFjdGlvbkR0b1tdIHtcblx0XHRyZXR1cm4gdGhpcy5fYnV0dG9uRHJvcGRvd25BY3Rpb25zO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdmFsdWVcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHZhbHVlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl92YWx1ZSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgY3NzXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBjc3ModmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX2NzcyA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdHlwZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgdHlwZSh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fdHlwZSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgcm91dGVMaW5rXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCByb3V0ZUxpbmsodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3JvdXRlTGluayA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgYnV0dG9uQWN0aW9uc1xuICAgICAqIHBhcmFtIHtCdXR0b25BY3Rpb25bXX0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBidXR0b25BY3Rpb25zKHZhbHVlOiBUYWJsZUJ0bkFjdGlvbkR0b1tdKSB7XG5cdFx0dGhpcy5fYnV0dG9uQWN0aW9ucyA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgYnV0dG9uRHJvcGRvd25BY3Rpb25zXG4gICAgICogcGFyYW0ge0J1dHRvbkFjdGlvbltdfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGJ1dHRvbkRyb3Bkb3duQWN0aW9ucyh2YWx1ZTogVGFibGVCdG5BY3Rpb25EdG9bXSkge1xuXHRcdHRoaXMuX2J1dHRvbkRyb3Bkb3duQWN0aW9ucyA9IHZhbHVlO1xuXHR9XG5cblxufVxuIl19