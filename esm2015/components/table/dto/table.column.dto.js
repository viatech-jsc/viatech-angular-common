import { SORT } from '../constant';
import { ColumnFilterBaseDto } from './filter/column.filter.base.dto';
export class TableColumnDto {
    constructor(value = "", name = "", sortable = false, sort = SORT.NONE, css = "", hidden = false) {
        this._name = name;
        this._value = value;
        this._sort = sort;
        this._sortable = sortable;
        this._css = css;
        this._hidden = hidden;
        this.filter = new ColumnFilterBaseDto();
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter sortable
     * return {boolean}
     */
    get sortable() {
        return this._sortable;
    }
    /**
     * Getter sort
     * return {string}
     */
    get sort() {
        return this._sort;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter hidden
     * return {boolean}
     */
    get hidden() {
        return this._hidden;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter sortable
     * param {boolean} value
     */
    set sortable(value) {
        this._sortable = value;
    }
    /**
     * Setter sort
     * param {string} value
     */
    set sort(value) {
        this._sort = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter hidden
     * param {boolean} value
     */
    set hidden(value) {
        this._hidden = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY29sdW1uLmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdGFibGUvZHRvL3RhYmxlLmNvbHVtbi5kdG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUNuQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUV0RSxNQUFNLE9BQU8sY0FBYztJQVN2QixZQUFZLEtBQUssR0FBRyxFQUFFLEVBQUUsSUFBSSxHQUFHLEVBQUUsRUFBRSxRQUFRLEdBQUcsS0FBSyxFQUFFLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLEdBQUcsR0FBRyxFQUFFLEVBQUUsTUFBTSxHQUFHLEtBQUs7UUFDM0YsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7UUFDMUIsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7UUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLG1CQUFtQixFQUFFLENBQUM7SUFDNUMsQ0FBQztJQUVEOzs7T0FHRztJQUNOLElBQVcsSUFBSTtRQUNkLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxLQUFLO1FBQ2YsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3BCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFFBQVE7UUFDbEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3ZCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLElBQUk7UUFDZCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsR0FBRztRQUNiLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNsQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxNQUFNO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUNyQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJLENBQUMsS0FBYTtRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxLQUFLLENBQUMsS0FBYTtRQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUNyQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxRQUFRLENBQUMsS0FBYztRQUNqQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJLENBQUMsS0FBYTtRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxHQUFHLENBQUMsS0FBYTtRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxNQUFNLENBQUMsS0FBYztRQUMvQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDO0NBRUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTT1JUIH0gZnJvbSAnLi4vY29uc3RhbnQnO1xuaW1wb3J0IHsgQ29sdW1uRmlsdGVyQmFzZUR0byB9IGZyb20gJy4vZmlsdGVyL2NvbHVtbi5maWx0ZXIuYmFzZS5kdG8nO1xuXG5leHBvcnQgY2xhc3MgVGFibGVDb2x1bW5EdG8ge1xuICAgIHByaXZhdGUgX25hbWU6IHN0cmluZztcbiAgICBwcml2YXRlIF92YWx1ZTogc3RyaW5nO1xuICAgIHByaXZhdGUgX3NvcnRhYmxlOiBib29sZWFuO1xuICAgIHByaXZhdGUgX3NvcnQ6IHN0cmluZztcbiAgICBwcml2YXRlIF9jc3M6IHN0cmluZztcbiAgICBwcml2YXRlIF9oaWRkZW46IGJvb2xlYW47XG4gICAgZmlsdGVyOiBDb2x1bW5GaWx0ZXJCYXNlRHRvO1xuXG4gICAgY29uc3RydWN0b3IodmFsdWUgPSBcIlwiLCBuYW1lID0gXCJcIiwgc29ydGFibGUgPSBmYWxzZSwgc29ydCA9IFNPUlQuTk9ORSwgY3NzID0gXCJcIiwgaGlkZGVuID0gZmFsc2UpIHtcbiAgICAgICAgdGhpcy5fbmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7XG4gICAgICAgIHRoaXMuX3NvcnQgPSBzb3J0O1xuICAgICAgICB0aGlzLl9zb3J0YWJsZSA9IHNvcnRhYmxlO1xuICAgICAgICB0aGlzLl9jc3MgPSBjc3M7XG4gICAgICAgIHRoaXMuX2hpZGRlbiA9IGhpZGRlbjtcbiAgICAgICAgdGhpcy5maWx0ZXIgPSBuZXcgQ29sdW1uRmlsdGVyQmFzZUR0bygpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBuYW1lXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgbmFtZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9uYW1lO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdmFsdWVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCB2YWx1ZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl92YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHNvcnRhYmxlXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHNvcnRhYmxlKCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLl9zb3J0YWJsZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHNvcnRcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBzb3J0KCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3NvcnQ7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBjc3NcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBjc3MoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fY3NzO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgaGlkZGVuXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGhpZGRlbigpOiBib29sZWFuIHtcblx0XHRyZXR1cm4gdGhpcy5faGlkZGVuO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgbmFtZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgbmFtZSh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fbmFtZSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdmFsdWVcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHZhbHVlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl92YWx1ZSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgc29ydGFibGVcbiAgICAgKiBwYXJhbSB7Ym9vbGVhbn0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBzb3J0YWJsZSh2YWx1ZTogYm9vbGVhbikge1xuXHRcdHRoaXMuX3NvcnRhYmxlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBzb3J0XG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBzb3J0KHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9zb3J0ID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBjc3NcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGNzcyh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fY3NzID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBoaWRkZW5cbiAgICAgKiBwYXJhbSB7Ym9vbGVhbn0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBoaWRkZW4odmFsdWU6IGJvb2xlYW4pIHtcblx0XHR0aGlzLl9oaWRkZW4gPSB2YWx1ZTtcblx0fVxuXG59Il19