export class TableFilterDto {
    constructor(hasFilter = true) {
        this.hasFilter = hasFilter;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuZmlsdGVyLmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdGFibGUvZHRvL3RhYmxlLmZpbHRlci5kdG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsTUFBTSxPQUFPLGNBQWM7SUFHdkIsWUFBWSxTQUFTLEdBQUcsSUFBSTtRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztJQUMvQixDQUFDO0NBRUoiLCJzb3VyY2VzQ29udGVudCI6WyJcbmV4cG9ydCBjbGFzcyBUYWJsZUZpbHRlckR0b3tcbiAgICBoYXNGaWx0ZXI6IGJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3RvcihoYXNGaWx0ZXIgPSB0cnVlKXtcbiAgICAgICAgdGhpcy5oYXNGaWx0ZXIgPSBoYXNGaWx0ZXI7XG4gICAgfVxuXG59Il19