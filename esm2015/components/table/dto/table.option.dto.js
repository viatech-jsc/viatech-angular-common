import { TableSortDto } from "./table.sort.dto";
import { TableFilterDto } from "./table.filter.dto";
export class TableOptionDto {
    constructor(tableColumns = [], tableRows = []) {
        this._tableColumns = tableColumns;
        this._tableRows = tableRows;
        this._totalItems = 0;
        this._itemsPerPage = 10;
        this._currentPage = 0;
        this._tableFilter = new TableFilterDto();
        this._tableActions = new Array();
        this._defaultSort = new TableSortDto();
        this._loading = false;
    }
    get loading() {
        return this._loading;
    }
    set loading(value) {
        this._loading = value;
    }
    /**
     * Getter tableColumns
     * return {TableColumn[]}
     */
    get tableColumns() {
        return this._tableColumns;
    }
    /**
     * Getter tableRows
     * return {TableRow[]}
     */
    get tableRows() {
        return this._tableRows;
    }
    /**
     * Getter totalItems
     * return {number}
     */
    get totalItems() {
        return this._totalItems;
    }
    /**
     * Getter itemsPerPage
     * return {number}
     */
    get itemsPerPage() {
        return this._itemsPerPage;
    }
    /**
     * Getter currentPage
     * return {number}
     */
    get currentPage() {
        return this._currentPage;
    }
    /**
     * Getter tableFilter
     * return {TableFilter}
     */
    get tableFilter() {
        return this._tableFilter;
    }
    /**
     * Getter tableActions
     * return {TableAction[]}
     */
    get tableActions() {
        return this._tableActions;
    }
    /**
     * Getter defaultSort
     * return {TableDefaultSort}
     */
    get defaultSort() {
        return this._defaultSort;
    }
    /**
     * Setter tableColumns
     * param {TableColumn[]} value
     */
    set tableColumns(value) {
        this._tableColumns = value;
    }
    /**
     * Setter tableRows
     * param {TableRow[]} value
     */
    set tableRows(value) {
        this._tableRows = value;
    }
    /**
     * Setter totalItems
     * param {number} value
     */
    set totalItems(value) {
        this._totalItems = value;
    }
    /**
     * Setter itemsPerPage
     * param {number} value
     */
    set itemsPerPage(value) {
        this._itemsPerPage = value;
    }
    /**
     * Setter currentPage
     * param {number} value
     */
    set currentPage(value) {
        this._currentPage = value;
    }
    /**
     * Setter tableFilter
     * param {TableFilter} value
     */
    set tableFilter(value) {
        this._tableFilter = value;
    }
    /**
     * Setter tableActions
     * param {TableAction[]} value
     */
    set tableActions(value) {
        this._tableActions = value;
    }
    /**
     * Setter defaultSort
     * param {TableDefaultSort} value
     */
    set defaultSort(value) {
        this._defaultSort = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUub3B0aW9uLmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdGFibGUvZHRvL3RhYmxlLm9wdGlvbi5kdG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRWhELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVwRCxNQUFNLE9BQU8sY0FBYztJQVd2QixZQUFZLFlBQVksR0FBRyxFQUFFLEVBQUUsU0FBUyxHQUFHLEVBQUU7UUFDekMsSUFBSSxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUM7UUFDbEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7UUFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLGNBQWMsRUFBRSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxLQUFLLEVBQWtCLENBQUM7UUFDakQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7SUFFRCxJQUFXLE9BQU87UUFDZCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUVELElBQVcsT0FBTyxDQUFDLEtBQWM7UUFDN0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQztJQUVEOzs7T0FHRztJQUNOLElBQVcsWUFBWTtRQUN0QixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDM0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsU0FBUztRQUNuQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDeEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsVUFBVTtRQUNwQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDekIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsWUFBWTtRQUN0QixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDM0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsV0FBVztRQUNyQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDMUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsV0FBVztRQUNyQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDMUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsWUFBWTtRQUN0QixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDM0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsV0FBVztRQUNyQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDMUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsWUFBWSxDQUFDLEtBQXVCO1FBQzlDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQzVCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFNBQVMsQ0FBQyxLQUFvQjtRQUN4QyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxVQUFVLENBQUMsS0FBYTtRQUNsQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztJQUMxQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxZQUFZLENBQUMsS0FBYTtRQUNwQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxXQUFXLENBQUMsS0FBYTtRQUNuQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxXQUFXLENBQUMsS0FBcUI7UUFDM0MsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsWUFBWSxDQUFDLEtBQXVCO1FBQzlDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQzVCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFdBQVcsQ0FBQyxLQUFtQjtRQUN6QyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDO0NBRUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUYWJsZVJvd0R0byB9IGZyb20gXCIuL3RhYmxlLnJvdy5kdG9cIjtcbmltcG9ydCB7IFRhYmxlQWN0aW9uRHRvIH0gZnJvbSBcIi4vdGFibGUuYWN0aW9uLmR0b1wiO1xuaW1wb3J0IHsgVGFibGVTb3J0RHRvIH0gZnJvbSBcIi4vdGFibGUuc29ydC5kdG9cIjtcbmltcG9ydCB7IFRhYmxlQ29sdW1uRHRvIH0gZnJvbSBcIi4vdGFibGUuY29sdW1uLmR0b1wiO1xuaW1wb3J0IHsgVGFibGVGaWx0ZXJEdG8gfSBmcm9tIFwiLi90YWJsZS5maWx0ZXIuZHRvXCI7XG5cbmV4cG9ydCBjbGFzcyBUYWJsZU9wdGlvbkR0byB7XG4gICAgcHJpdmF0ZSBfdGFibGVDb2x1bW5zOiBUYWJsZUNvbHVtbkR0b1tdO1xuICAgIHByaXZhdGUgX3RhYmxlUm93czogVGFibGVSb3dEdG9bXTtcbiAgICBwcml2YXRlIF90b3RhbEl0ZW1zOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBfaXRlbXNQZXJQYWdlOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBfY3VycmVudFBhZ2U6IG51bWJlcjtcbiAgICBwcml2YXRlIF90YWJsZUZpbHRlcjogVGFibGVGaWx0ZXJEdG87XG4gICAgcHJpdmF0ZSBfdGFibGVBY3Rpb25zOiBUYWJsZUFjdGlvbkR0b1tdO1xuICAgIHByaXZhdGUgX2RlZmF1bHRTb3J0OiBUYWJsZVNvcnREdG87XG4gICAgcHJpdmF0ZSBfbG9hZGluZzogYm9vbGVhbjtcblxuICAgIGNvbnN0cnVjdG9yKHRhYmxlQ29sdW1ucyA9IFtdLCB0YWJsZVJvd3MgPSBbXSl7XG4gICAgICAgIHRoaXMuX3RhYmxlQ29sdW1ucyA9IHRhYmxlQ29sdW1ucztcbiAgICAgICAgdGhpcy5fdGFibGVSb3dzID0gdGFibGVSb3dzO1xuICAgICAgICB0aGlzLl90b3RhbEl0ZW1zID0gMDtcbiAgICAgICAgdGhpcy5faXRlbXNQZXJQYWdlID0gMTA7XG4gICAgICAgIHRoaXMuX2N1cnJlbnRQYWdlID0gMDtcbiAgICAgICAgdGhpcy5fdGFibGVGaWx0ZXIgPSBuZXcgVGFibGVGaWx0ZXJEdG8oKTtcbiAgICAgICAgdGhpcy5fdGFibGVBY3Rpb25zID0gbmV3IEFycmF5PFRhYmxlQWN0aW9uRHRvPigpO1xuICAgICAgICB0aGlzLl9kZWZhdWx0U29ydCA9IG5ldyBUYWJsZVNvcnREdG8oKTtcbiAgICAgICAgdGhpcy5fbG9hZGluZyA9IGZhbHNlO1xuICAgIH1cblxuICAgIHB1YmxpYyBnZXQgbG9hZGluZygpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xvYWRpbmc7XG4gICAgfVxuXG4gICAgcHVibGljIHNldCBsb2FkaW5nKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2xvYWRpbmcgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdGFibGVDb2x1bW5zXG4gICAgICogcmV0dXJuIHtUYWJsZUNvbHVtbltdfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHRhYmxlQ29sdW1ucygpOiBUYWJsZUNvbHVtbkR0b1tdIHtcblx0XHRyZXR1cm4gdGhpcy5fdGFibGVDb2x1bW5zO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdGFibGVSb3dzXG4gICAgICogcmV0dXJuIHtUYWJsZVJvd1tdfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHRhYmxlUm93cygpOiBUYWJsZVJvd0R0b1tdIHtcblx0XHRyZXR1cm4gdGhpcy5fdGFibGVSb3dzO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdG90YWxJdGVtc1xuICAgICAqIHJldHVybiB7bnVtYmVyfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHRvdGFsSXRlbXMoKTogbnVtYmVyIHtcblx0XHRyZXR1cm4gdGhpcy5fdG90YWxJdGVtcztcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGl0ZW1zUGVyUGFnZVxuICAgICAqIHJldHVybiB7bnVtYmVyfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGl0ZW1zUGVyUGFnZSgpOiBudW1iZXIge1xuXHRcdHJldHVybiB0aGlzLl9pdGVtc1BlclBhZ2U7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBjdXJyZW50UGFnZVxuICAgICAqIHJldHVybiB7bnVtYmVyfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGN1cnJlbnRQYWdlKCk6IG51bWJlciB7XG5cdFx0cmV0dXJuIHRoaXMuX2N1cnJlbnRQYWdlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdGFibGVGaWx0ZXJcbiAgICAgKiByZXR1cm4ge1RhYmxlRmlsdGVyfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHRhYmxlRmlsdGVyKCk6IFRhYmxlRmlsdGVyRHRvIHtcblx0XHRyZXR1cm4gdGhpcy5fdGFibGVGaWx0ZXI7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB0YWJsZUFjdGlvbnNcbiAgICAgKiByZXR1cm4ge1RhYmxlQWN0aW9uW119XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdGFibGVBY3Rpb25zKCk6IFRhYmxlQWN0aW9uRHRvW10ge1xuXHRcdHJldHVybiB0aGlzLl90YWJsZUFjdGlvbnM7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBkZWZhdWx0U29ydFxuICAgICAqIHJldHVybiB7VGFibGVEZWZhdWx0U29ydH1cbiAgICAgKi9cblx0cHVibGljIGdldCBkZWZhdWx0U29ydCgpOiBUYWJsZVNvcnREdG8ge1xuXHRcdHJldHVybiB0aGlzLl9kZWZhdWx0U29ydDtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHRhYmxlQ29sdW1uc1xuICAgICAqIHBhcmFtIHtUYWJsZUNvbHVtbltdfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHRhYmxlQ29sdW1ucyh2YWx1ZTogVGFibGVDb2x1bW5EdG9bXSkge1xuXHRcdHRoaXMuX3RhYmxlQ29sdW1ucyA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdGFibGVSb3dzXG4gICAgICogcGFyYW0ge1RhYmxlUm93W119IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgdGFibGVSb3dzKHZhbHVlOiBUYWJsZVJvd0R0b1tdKSB7XG5cdFx0dGhpcy5fdGFibGVSb3dzID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB0b3RhbEl0ZW1zXG4gICAgICogcGFyYW0ge251bWJlcn0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCB0b3RhbEl0ZW1zKHZhbHVlOiBudW1iZXIpIHtcblx0XHR0aGlzLl90b3RhbEl0ZW1zID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBpdGVtc1BlclBhZ2VcbiAgICAgKiBwYXJhbSB7bnVtYmVyfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGl0ZW1zUGVyUGFnZSh2YWx1ZTogbnVtYmVyKSB7XG5cdFx0dGhpcy5faXRlbXNQZXJQYWdlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBjdXJyZW50UGFnZVxuICAgICAqIHBhcmFtIHtudW1iZXJ9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgY3VycmVudFBhZ2UodmFsdWU6IG51bWJlcikge1xuXHRcdHRoaXMuX2N1cnJlbnRQYWdlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB0YWJsZUZpbHRlclxuICAgICAqIHBhcmFtIHtUYWJsZUZpbHRlcn0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCB0YWJsZUZpbHRlcih2YWx1ZTogVGFibGVGaWx0ZXJEdG8pIHtcblx0XHR0aGlzLl90YWJsZUZpbHRlciA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdGFibGVBY3Rpb25zXG4gICAgICogcGFyYW0ge1RhYmxlQWN0aW9uW119IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgdGFibGVBY3Rpb25zKHZhbHVlOiBUYWJsZUFjdGlvbkR0b1tdKSB7XG5cdFx0dGhpcy5fdGFibGVBY3Rpb25zID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBkZWZhdWx0U29ydFxuICAgICAqIHBhcmFtIHtUYWJsZURlZmF1bHRTb3J0fSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGRlZmF1bHRTb3J0KHZhbHVlOiBUYWJsZVNvcnREdG8pIHtcblx0XHR0aGlzLl9kZWZhdWx0U29ydCA9IHZhbHVlO1xuXHR9XG5cbn1cbiJdfQ==