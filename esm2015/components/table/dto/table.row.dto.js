export class TableRowDto {
    constructor(tableCells = [], css = "") {
        this._tableCells = tableCells;
        this._css = css;
        this._editAble = false;
        this._activeAble = false;
        this._deactiveAble = false;
        this._deleteAble = false;
    }
    /**
     * Getter tableCells
     * return {TableCell[]}
     */
    get tableCells() {
        return this._tableCells;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter editAble
     * return {boolean}
     */
    get editAble() {
        return this._editAble;
    }
    /**
     * Getter deleteAble
     * return {boolean}
     */
    get deleteAble() {
        return this._deleteAble;
    }
    /**
     * Getter deactiveAble
     * return {boolean}
     */
    get deactiveAble() {
        return this._deactiveAble;
    }
    /**
     * Getter activeAble
     * return {boolean}
     */
    get activeAble() {
        return this._activeAble;
    }
    /**
     * Setter tableCells
     * param {TableCell[]} value
     */
    set tableCells(value) {
        this._tableCells = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter editAble
     * param {boolean} value
     */
    set editAble(value) {
        this._editAble = value;
    }
    /**
     * Setter deleteAble
     * param {boolean} value
     */
    set deleteAble(value) {
        this._deleteAble = value;
    }
    /**
     * Setter deactiveAble
     * param {boolean} value
     */
    set deactiveAble(value) {
        this._deactiveAble = value;
    }
    /**
     * Setter activeAble
     * param {boolean} value
     */
    set activeAble(value) {
        this._activeAble = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUucm93LmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdGFibGUvZHRvL3RhYmxlLnJvdy5kdG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsTUFBTSxPQUFPLFdBQVc7SUFRcEIsWUFBWSxhQUE2QixFQUFFLEVBQUUsTUFBYyxFQUFFO1FBQ3pELElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFVBQVU7UUFDakIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLEdBQUc7UUFDVixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsUUFBUTtRQUNmLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxVQUFVO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxZQUFZO1FBQ25CLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxVQUFVO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxVQUFVLENBQUMsS0FBcUI7UUFDdkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsR0FBRyxDQUFDLEtBQWE7UUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsUUFBUSxDQUFDLEtBQWM7UUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsVUFBVSxDQUFDLEtBQWM7UUFDaEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsWUFBWSxDQUFDLEtBQWM7UUFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsVUFBVSxDQUFDLEtBQWM7UUFDaEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztDQUVKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVGFibGVDZWxsRHRvIH0gZnJvbSBcIi4vdGFibGUuY2VsbC5kdG9cIjtcblxuZXhwb3J0IGNsYXNzIFRhYmxlUm93RHRvIHtcbiAgICBwcml2YXRlIF90YWJsZUNlbGxzOiBUYWJsZUNlbGxEdG9bXTtcbiAgICBwcml2YXRlIF9jc3M6IHN0cmluZztcbiAgICBwcml2YXRlIF9lZGl0QWJsZTogYm9vbGVhbjtcbiAgICBwcml2YXRlIF9kZWxldGVBYmxlOiBib29sZWFuO1xuICAgIHByaXZhdGUgX2RlYWN0aXZlQWJsZTogYm9vbGVhbjtcbiAgICBwcml2YXRlIF9hY3RpdmVBYmxlOiBib29sZWFuO1xuXG4gICAgY29uc3RydWN0b3IodGFibGVDZWxsczogVGFibGVDZWxsRHRvW10gPSBbXSwgY3NzOiBzdHJpbmcgPSBcIlwiKSB7XG4gICAgICAgIHRoaXMuX3RhYmxlQ2VsbHMgPSB0YWJsZUNlbGxzO1xuICAgICAgICB0aGlzLl9jc3MgPSBjc3M7XG4gICAgICAgIHRoaXMuX2VkaXRBYmxlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2FjdGl2ZUFibGUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5fZGVhY3RpdmVBYmxlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2RlbGV0ZUFibGUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdGFibGVDZWxsc1xuICAgICAqIHJldHVybiB7VGFibGVDZWxsW119XG4gICAgICovXG4gICAgcHVibGljIGdldCB0YWJsZUNlbGxzKCk6IFRhYmxlQ2VsbER0b1tdIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RhYmxlQ2VsbHM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGNzc1xuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgY3NzKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLl9jc3M7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGVkaXRBYmxlXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgZWRpdEFibGUoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9lZGl0QWJsZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgZGVsZXRlQWJsZVxuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGRlbGV0ZUFibGUoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9kZWxldGVBYmxlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBkZWFjdGl2ZUFibGVcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIGdldCBkZWFjdGl2ZUFibGUoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9kZWFjdGl2ZUFibGU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGFjdGl2ZUFibGVcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIGdldCBhY3RpdmVBYmxlKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fYWN0aXZlQWJsZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdGFibGVDZWxsc1xuICAgICAqIHBhcmFtIHtUYWJsZUNlbGxbXX0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IHRhYmxlQ2VsbHModmFsdWU6IFRhYmxlQ2VsbER0b1tdKSB7XG4gICAgICAgIHRoaXMuX3RhYmxlQ2VsbHMgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgY3NzXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IGNzcyh2YWx1ZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuX2NzcyA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBlZGl0QWJsZVxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgZWRpdEFibGUodmFsdWU6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5fZWRpdEFibGUgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgZGVsZXRlQWJsZVxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgZGVsZXRlQWJsZSh2YWx1ZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9kZWxldGVBYmxlID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGRlYWN0aXZlQWJsZVxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgZGVhY3RpdmVBYmxlKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2RlYWN0aXZlQWJsZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBhY3RpdmVBYmxlXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBhY3RpdmVBYmxlKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2FjdGl2ZUFibGUgPSB2YWx1ZTtcbiAgICB9XG5cbn0iXX0=