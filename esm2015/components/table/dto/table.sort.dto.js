import { SORT } from "../constant";
export class TableSortDto {
    constructor(sortColumnName = "", sortType = SORT.ASC) {
        this._sortColumnName = sortColumnName;
        this._sortType = sortType;
    }
    /**
     * Getter sortColumnName
     * return {string}
     */
    get sortColumnName() {
        return this._sortColumnName;
    }
    /**
     * Getter sortType
     * return {string}
     */
    get sortType() {
        return this._sortType;
    }
    /**
     * Setter sortColumnName
     * param {string} value
     */
    set sortColumnName(value) {
        this._sortColumnName = value;
    }
    /**
     * Setter sortType
     * param {string} value
     */
    set sortType(value) {
        this._sortType = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuc29ydC5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3RhYmxlL2R0by90YWJsZS5zb3J0LmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBRW5DLE1BQU0sT0FBTyxZQUFZO0lBSXJCLFlBQVksaUJBQXdCLEVBQUUsRUFBRSxXQUFrQixJQUFJLENBQUMsR0FBRztRQUNoRSxJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQztRQUN0QyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztJQUM1QixDQUFDO0lBRUQ7OztPQUdHO0lBQ04sSUFBVyxjQUFjO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUM3QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxRQUFRO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN2QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxjQUFjLENBQUMsS0FBYTtRQUN0QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUM5QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxRQUFRLENBQUMsS0FBYTtRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0NBRUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTT1JUIH0gZnJvbSBcIi4uL2NvbnN0YW50XCI7XG5cbmV4cG9ydCBjbGFzcyBUYWJsZVNvcnREdG8ge1xuICAgIHByaXZhdGUgX3NvcnRDb2x1bW5OYW1lOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfc29ydFR5cGU6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKHNvcnRDb2x1bW5OYW1lOnN0cmluZyA9IFwiXCIsIHNvcnRUeXBlOnN0cmluZyA9IFNPUlQuQVNDKXtcbiAgICAgIHRoaXMuX3NvcnRDb2x1bW5OYW1lID0gc29ydENvbHVtbk5hbWU7XG4gICAgICB0aGlzLl9zb3J0VHlwZSA9IHNvcnRUeXBlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBzb3J0Q29sdW1uTmFtZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHNvcnRDb2x1bW5OYW1lKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3NvcnRDb2x1bW5OYW1lO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgc29ydFR5cGVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBzb3J0VHlwZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9zb3J0VHlwZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHNvcnRDb2x1bW5OYW1lXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBzb3J0Q29sdW1uTmFtZSh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fc29ydENvbHVtbk5hbWUgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHNvcnRUeXBlXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBzb3J0VHlwZSh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fc29ydFR5cGUgPSB2YWx1ZTtcblx0fVxuXG59Il19