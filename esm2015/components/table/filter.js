import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class FilterPipe {
    transform(columns) {
        return columns.filter((column) => {
            return !column.hidden;
        });
    }
}
FilterPipe.ɵfac = function FilterPipe_Factory(t) { return new (t || FilterPipe)(); };
FilterPipe.ɵpipe = i0.ɵɵdefinePipe({ name: "filterColumn", type: FilterPipe, pure: true });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FilterPipe, [{
        type: Pipe,
        args: [{ name: 'filterColumn' }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdGFibGUvZmlsdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDOztBQUlwRCxNQUFNLE9BQU8sVUFBVTtJQUNuQixTQUFTLENBQUMsT0FBMkI7UUFDakMsT0FBTyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxFQUFDLEVBQUU7WUFDNUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztvRUFMUSxVQUFVO2lFQUFWLFVBQVU7a0RBQVYsVUFBVTtjQUR0QixJQUFJO2VBQUMsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVGFibGVDb2x1bW4gfSBmcm9tICcuL3RhYmxlLmNvbHVtbic7XG5cbkBQaXBlKHsgbmFtZTogJ2ZpbHRlckNvbHVtbicgfSlcbmV4cG9ydCBjbGFzcyBGaWx0ZXJQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybXtcbiAgICB0cmFuc2Zvcm0oY29sdW1uczogQXJyYXk8VGFibGVDb2x1bW4+KSB7XG4gICAgICAgIHJldHVybiBjb2x1bW5zLmZpbHRlcigoY29sdW1uKT0+e1xuICAgICAgICAgICAgcmV0dXJuICFjb2x1bW4uaGlkZGVuO1xuICAgICAgICB9KTtcbiAgICB9XG59Il19