import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class FilterPipe {
    transform(columns) {
        return columns.filter((column) => {
            return !column.hidden;
        });
    }
}
FilterPipe.ɵfac = function FilterPipe_Factory(t) { return new (t || FilterPipe)(); };
FilterPipe.ɵpipe = i0.ɵɵdefinePipe({ name: "filterColumn", type: FilterPipe, pure: true });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FilterPipe, [{
        type: Pipe,
        args: [{ name: 'filterColumn' }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9waXBlL2ZpbHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQzs7QUFJcEQsTUFBTSxPQUFPLFVBQVU7SUFDbkIsU0FBUyxDQUFDLE9BQThCO1FBQ3BDLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sRUFBQyxFQUFFO1lBQzVCLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7b0VBTFEsVUFBVTtpRUFBVixVQUFVO2tEQUFWLFVBQVU7Y0FEdEIsSUFBSTtlQUFDLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFRhYmxlQ29sdW1uRHRvIH0gZnJvbSAnLi4vZHRvL3RhYmxlLmNvbHVtbi5kdG8nO1xuXG5AUGlwZSh7IG5hbWU6ICdmaWx0ZXJDb2x1bW4nIH0pXG5leHBvcnQgY2xhc3MgRmlsdGVyUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm17XG4gICAgdHJhbnNmb3JtKGNvbHVtbnM6IEFycmF5PFRhYmxlQ29sdW1uRHRvPikge1xuICAgICAgICByZXR1cm4gY29sdW1ucy5maWx0ZXIoKGNvbHVtbik9PntcbiAgICAgICAgICAgIHJldHVybiAhY29sdW1uLmhpZGRlbjtcbiAgICAgICAgfSk7XG4gICAgfVxufSJdfQ==