import { Component, Input } from "@angular/core";
import { TableCellDto } from "../../../dto/table.cell.dto";
import { TableRowDto } from "../../../dto/table.row.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@angular/material/button";
import * as i3 from "@angular/material/menu";
import * as i4 from "@angular/material/icon";
function CellActionComp_span_1_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r6 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 4);
    i0.ɵɵlistener("click", function CellActionComp_span_1_button_2_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r6); const buttonAction_r2 = i0.ɵɵnextContext().$implicit; const ctx_r5 = i0.ɵɵnextContext(); return buttonAction_r2.buttonAction(ctx_r5.row); });
    i0.ɵɵelement(1, "img", 5);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r2 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵpropertyInterpolate("title", buttonAction_r2.name);
    i0.ɵɵproperty("disabled", buttonAction_r2.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("src", buttonAction_r2.iconUrl, i0.ɵɵsanitizeUrl);
} }
function CellActionComp_span_1_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r10 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 6);
    i0.ɵɵlistener("click", function CellActionComp_span_1_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r10); const buttonAction_r2 = i0.ɵɵnextContext().$implicit; const ctx_r9 = i0.ɵɵnextContext(); return buttonAction_r2.buttonAction(ctx_r9.row); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r2 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("disabled", buttonAction_r2.disabled)("ngClass", "btn-" + buttonAction_r2.name);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", buttonAction_r2.name, " ");
} }
function CellActionComp_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "span");
    i0.ɵɵtemplate(2, CellActionComp_span_1_button_2_Template, 2, 3, "button", 2);
    i0.ɵɵtemplate(3, CellActionComp_span_1_button_3_Template, 2, 3, "button", 3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r2 = ctx.$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", buttonAction_r2.iconUrl);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !buttonAction_r2.iconUrl);
} }
function CellActionComp_div_2_button_7_Template(rf, ctx) { if (rf & 1) {
    const _r17 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 12);
    i0.ɵɵlistener("click", function CellActionComp_div_2_button_7_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r17); const btnAction_r15 = ctx.$implicit; const ctx_r16 = i0.ɵɵnextContext(2); return btnAction_r15.disabled != true ? btnAction_r15.buttonAction(ctx_r16.row) : ""; });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const btnAction_r15 = ctx.$implicit;
    i0.ɵɵproperty("disabled", btnAction_r15.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", btnAction_r15.name, " ");
} }
function CellActionComp_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 7);
    i0.ɵɵelementStart(1, "div", 8);
    i0.ɵɵelementStart(2, "button", 9);
    i0.ɵɵelementStart(3, "mat-icon");
    i0.ɵɵtext(4, "more_vert");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "mat-menu", null, 10);
    i0.ɵɵtemplate(7, CellActionComp_div_2_button_7_Template, 2, 2, "button", 11);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const _r13 = i0.ɵɵreference(6);
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("matMenuTriggerFor", _r13);
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("ngForOf", ctx_r1.cell.buttonDropdownActions);
} }
export class CellActionComp {
    constructor() {
    }
    ngOnInit() {
    }
}
CellActionComp.ɵfac = function CellActionComp_Factory(t) { return new (t || CellActionComp)(); };
CellActionComp.ɵcmp = i0.ɵɵdefineComponent({ type: CellActionComp, selectors: [["via-table-cell-action"]], inputs: { cell: "cell", row: "row" }, decls: 3, vars: 2, consts: [[4, "ngFor", "ngForOf"], ["class", "btn-group mr-3", 4, "ngIf"], ["class", "btn btn-link btn-action icon-btn", 3, "disabled", "title", "click", 4, "ngIf"], ["type", "button", "class", "btn btn-light btn-action", 3, "disabled", "ngClass", "click", 4, "ngIf"], [1, "btn", "btn-link", "btn-action", "icon-btn", 3, "disabled", "title", "click"], [3, "src"], ["type", "button", 1, "btn", "btn-light", "btn-action", 3, "disabled", "ngClass", "click"], [1, "btn-group", "mr-3"], [1, "btn-group"], ["mat-icon-button", "", "color", "primary", 3, "matMenuTriggerFor"], ["moreActionsMenu", "matMenu"], ["mat-menu-item", "", 3, "disabled", "click", 4, "ngFor", "ngForOf"], ["mat-menu-item", "", 3, "disabled", "click"]], template: function CellActionComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "span");
        i0.ɵɵtemplate(1, CellActionComp_span_1_Template, 4, 2, "span", 0);
        i0.ɵɵtemplate(2, CellActionComp_div_2_Template, 8, 2, "div", 1);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngForOf", ctx.cell.buttonActions);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.cell.buttonDropdownActions.length);
    } }, directives: [i1.NgForOf, i1.NgIf, i1.NgClass, i2.MatButton, i3.MatMenuTrigger, i4.MatIcon, i3._MatMenu, i3.MatMenuItem], styles: ["button.btn-action[_ngcontent-%COMP%]{margin-right:5px;margin-top:5px;padding:10px 0!important;text-transform:uppercase;width:80px}.icon-btn[_ngcontent-%COMP%]{width:50px!important}.icon-btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:20px}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CellActionComp, [{
        type: Component,
        args: [{
                selector: "via-table-cell-action",
                templateUrl: "./action.html",
                styleUrls: ["./action.scss"]
            }]
    }], function () { return []; }, { cell: [{
            type: Input
        }], row: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9yb3cvY2VsbC9hY3Rpb24vYWN0aW9uLnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9yb3cvY2VsbC9hY3Rpb24vYWN0aW9uLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzNELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQzs7Ozs7Ozs7SUNDN0MsaUNBR0k7SUFGQSwyTkFBUyx3Q0FBOEIsSUFBQztJQUV4Qyx5QkFDSjtJQUFBLGlCQUFTOzs7SUFIb0MsdURBQThCO0lBRG5FLG1EQUFrQztJQUdqQyxlQUE0QjtJQUE1QiwrREFBNEI7Ozs7SUFFckMsaUNBSUk7SUFEQSw0TkFBUyx3Q0FBOEIsSUFBQztJQUN4QyxZQUNKO0lBQUEsaUJBQVM7OztJQUw2QixtREFBa0MsMENBQUE7SUFJcEUsZUFDSjtJQURJLHFEQUNKOzs7SUFaUiw0QkFDSTtJQUFBLDRCQUNJO0lBQUEsNEVBR0k7SUFFSiw0RUFJSTtJQUVSLGlCQUFPO0lBQ1gsaUJBQU87OztJQVZLLGVBQTRCO0lBQTVCLDhDQUE0QjtJQUd4QixlQUE2QjtJQUE3QiwrQ0FBNkI7Ozs7SUFlakMsa0NBSUk7SUFEQSxzT0FBNkIsSUFBSSxHQUFDLHVDQUEyQixHQUFDLEVBQUUsSUFBQztJQUNqRSxZQUNKO0lBQUEsaUJBQVM7OztJQUpMLGlEQUErQjtJQUcvQixlQUNKO0lBREksbURBQ0o7OztJQVhaLDhCQUNJO0lBQUEsOEJBQ0k7SUFBQSxpQ0FDSTtJQUFBLGdDQUFVO0lBQUEseUJBQVM7SUFBQSxpQkFBVztJQUNsQyxpQkFBUztJQUNULDBDQUNJO0lBQUEsNEVBSUk7SUFFUixpQkFBVztJQUNmLGlCQUFNO0lBQ1YsaUJBQU07Ozs7SUFaMEMsZUFBcUM7SUFBckMsd0NBQXFDO0lBSWpFLGVBQW9EO0lBQXBELDJEQUFvRDs7QURkNUUsTUFBTSxPQUFPLGNBQWM7SUFLekI7SUFFQSxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7OzRFQVZVLGNBQWM7bURBQWQsY0FBYztRQ1QzQiw0QkFDSTtRQUFBLGlFQUNJO1FBZUosK0RBQ0k7UUFjUixpQkFBTzs7UUEvQkcsZUFBK0M7UUFBL0MsZ0RBQStDO1FBZ0J6QixlQUF5QztRQUF6Qyw0REFBeUM7O2tERFI1RCxjQUFjO2NBTDFCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsdUJBQXVCO2dCQUNqQyxXQUFXLEVBQUUsZUFBZTtnQkFDNUIsU0FBUyxFQUFFLENBQUMsZUFBZSxDQUFDO2FBQzdCO3NDQUdVLElBQUk7a0JBQVosS0FBSztZQUNHLEdBQUc7a0JBQVgsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgVGFibGVDZWxsRHRvIH0gZnJvbSBcIi4uLy4uLy4uL2R0by90YWJsZS5jZWxsLmR0b1wiO1xuaW1wb3J0IHsgVGFibGVSb3dEdG8gfSBmcm9tIFwiLi4vLi4vLi4vZHRvL3RhYmxlLnJvdy5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10YWJsZS1jZWxsLWFjdGlvblwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2FjdGlvbi5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9hY3Rpb24uc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBDZWxsQWN0aW9uQ29tcHtcbiAgXG4gIEBJbnB1dCgpIGNlbGw6IFRhYmxlQ2VsbER0bztcbiAgQElucHV0KCkgcm93OiBUYWJsZVJvd0R0bztcbiAgXG4gIGNvbnN0cnVjdG9yKFxuICApIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cblxufVxuIiwiPHNwYW4+XG4gICAgPHNwYW4gKm5nRm9yPVwibGV0IGJ1dHRvbkFjdGlvbiBvZiBjZWxsLmJ1dHRvbkFjdGlvbnNcIj5cbiAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICA8YnV0dG9uIFtkaXNhYmxlZF09XCJidXR0b25BY3Rpb24uZGlzYWJsZWRcIlxuICAgICAgICAgICAgICAgIChjbGljayk9XCJidXR0b25BY3Rpb24uYnV0dG9uQWN0aW9uKHJvdylcIiB0aXRsZT1cInt7YnV0dG9uQWN0aW9uLm5hbWUgfX1cIlxuICAgICAgICAgICAgICAgICpuZ0lmPVwiYnV0dG9uQWN0aW9uLmljb25VcmxcIiBjbGFzcz1cImJ0biBidG4tbGluayBidG4tYWN0aW9uIGljb24tYnRuXCI+XG4gICAgICAgICAgICAgICAgPGltZyBbc3JjXT1cImJ1dHRvbkFjdGlvbi5pY29uVXJsXCIgLz5cbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgPGJ1dHRvbiAqbmdJZj1cIiFidXR0b25BY3Rpb24uaWNvblVybFwiIFtkaXNhYmxlZF09XCJidXR0b25BY3Rpb24uZGlzYWJsZWRcIlxuICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tbGlnaHQgYnRuLWFjdGlvblwiXG4gICAgICAgICAgICAgICAgW25nQ2xhc3NdPSdcImJ0bi1cIiArIGJ1dHRvbkFjdGlvbi5uYW1lJ1xuICAgICAgICAgICAgICAgIChjbGljayk9XCJidXR0b25BY3Rpb24uYnV0dG9uQWN0aW9uKHJvdylcIj5cbiAgICAgICAgICAgICAgICB7e2J1dHRvbkFjdGlvbi5uYW1lfX1cbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICA8L3NwYW4+XG4gICAgPC9zcGFuPlxuXG4gICAgPGRpdiBjbGFzcz1cImJ0bi1ncm91cCBtci0zXCIgKm5nSWY9XCJjZWxsLmJ1dHRvbkRyb3Bkb3duQWN0aW9ucy5sZW5ndGhcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImJ0bi1ncm91cFwiPlxuICAgICAgICAgICAgPGJ1dHRvbiBtYXQtaWNvbi1idXR0b24gY29sb3I9XCJwcmltYXJ5XCIgW21hdE1lbnVUcmlnZ2VyRm9yXT1cIm1vcmVBY3Rpb25zTWVudVwiPlxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbj5tb3JlX3ZlcnQ8L21hdC1pY29uPlxuICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICA8bWF0LW1lbnUgI21vcmVBY3Rpb25zTWVudT1cIm1hdE1lbnVcIj5cbiAgICAgICAgICAgICAgICA8YnV0dG9uICpuZ0Zvcj1cImxldCBidG5BY3Rpb24gb2YgY2VsbC5idXR0b25Ecm9wZG93bkFjdGlvbnNcIiBcbiAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cImJ0bkFjdGlvbi5kaXNhYmxlZFwiIFxuICAgICAgICAgICAgICAgICAgICBtYXQtbWVudS1pdGVtIFxuICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwiYnRuQWN0aW9uLmRpc2FibGVkIT10cnVlP2J0bkFjdGlvbi5idXR0b25BY3Rpb24ocm93KTonJ1wiPlxuICAgICAgICAgICAgICAgICAgICB7e2J0bkFjdGlvbi5uYW1lfX1cbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgIDwvbWF0LW1lbnU+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuPC9zcGFuPiJdfQ==