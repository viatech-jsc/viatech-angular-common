import { Component, EventEmitter, Input, Output } from "@angular/core";
import { CELL_TYPE } from "../../constant";
import { TableCellDto } from "../../dto/table.cell.dto";
import { TableRowDto } from "../../dto/table.row.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "./image/image";
import * as i3 from "./input/input";
import * as i4 from "./status/status";
import * as i5 from "./datetime/datetime";
import * as i6 from "./action/action";
import * as i7 from "./default/default";
function CellComp_ng_container_1_Template(rf, ctx) { if (rf & 1) {
    const _r7 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "via-table-cell-image", 3);
    i0.ɵɵlistener("openImagePreview", function CellComp_ng_container_1_Template_via_table_cell_image_openImagePreview_1_listener($event) { i0.ɵɵrestoreView(_r7); const ctx_r6 = i0.ɵɵnextContext(); return ctx_r6.doOpenPreview($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("cell", ctx_r0.cell);
} }
function CellComp_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    const _r9 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "via-table-cell-input", 4);
    i0.ɵɵlistener("rowUpdated", function CellComp_ng_container_2_Template_via_table_cell_input_rowUpdated_1_listener($event) { i0.ɵɵrestoreView(_r9); const ctx_r8 = i0.ɵɵnextContext(); return ctx_r8.editRow($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("cell", ctx_r1.cell);
} }
function CellComp_ng_container_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelement(1, "via-table-cell-status", 5);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("cell", ctx_r2.cell);
} }
function CellComp_ng_container_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelement(1, "via-table-cell-datetime", 5);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("cell", ctx_r3.cell);
} }
function CellComp_ng_container_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelement(1, "via-table-cell-action", 5);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("cell", ctx_r4.cell);
} }
function CellComp_ng_container_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelement(1, "via-table-cell-default", 5);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("cell", ctx_r5.cell);
} }
export class CellComp {
    constructor() {
        this.openImagePreview = new EventEmitter();
        this.rowUpdated = new EventEmitter();
        this.dndSupport = false;
        this.cellType = CELL_TYPE;
    }
    ngOnInit() {
    }
    doOpenPreview(imgCode) {
        this.openImagePreview.emit(imgCode);
    }
    editRow(str) {
        this.cell.value = str;
        this.rowUpdated.emit(this.row);
    }
}
CellComp.ɵfac = function CellComp_Factory(t) { return new (t || CellComp)(); };
CellComp.ɵcmp = i0.ɵɵdefineComponent({ type: CellComp, selectors: [["via-table-cell"]], inputs: { row: "row", cell: "cell", dndSupport: "dndSupport" }, outputs: { openImagePreview: "openImagePreview", rowUpdated: "rowUpdated" }, decls: 7, vars: 6, consts: [[3, "ngSwitch"], [4, "ngSwitchCase"], [4, "ngSwitchDefault"], [3, "cell", "openImagePreview"], [3, "cell", "rowUpdated"], [3, "cell"]], template: function CellComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementContainerStart(0, 0);
        i0.ɵɵtemplate(1, CellComp_ng_container_1_Template, 2, 1, "ng-container", 1);
        i0.ɵɵtemplate(2, CellComp_ng_container_2_Template, 2, 1, "ng-container", 1);
        i0.ɵɵtemplate(3, CellComp_ng_container_3_Template, 2, 1, "ng-container", 1);
        i0.ɵɵtemplate(4, CellComp_ng_container_4_Template, 2, 1, "ng-container", 1);
        i0.ɵɵtemplate(5, CellComp_ng_container_5_Template, 2, 1, "ng-container", 1);
        i0.ɵɵtemplate(6, CellComp_ng_container_6_Template, 2, 1, "ng-container", 2);
        i0.ɵɵelementContainerEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("ngSwitch", ctx.cell.type);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngSwitchCase", ctx.cellType.IMAGE_THUMBNAIL);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngSwitchCase", ctx.cellType.INPUT);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngSwitchCase", ctx.cellType.STATUS);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngSwitchCase", ctx.cellType.DATE_TIME);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngSwitchCase", ctx.cellType.ACTION);
    } }, directives: [i1.NgSwitch, i1.NgSwitchCase, i1.NgSwitchDefault, i2.CellImageComp, i3.CellInputComp, i4.CellStatusComp, i5.CellDateTimeComp, i6.CellActionComp, i7.CellDefaultComp], styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CellComp, [{
        type: Component,
        args: [{
                selector: "via-table-cell",
                templateUrl: "./cell.html",
                styleUrls: ["./cell.scss"]
            }]
    }], function () { return []; }, { openImagePreview: [{
            type: Output
        }], rowUpdated: [{
            type: Output
        }], row: [{
            type: Input
        }], cell: [{
            type: Input
        }], dndSupport: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2VsbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdGFibGUvcm93L2NlbGwvY2VsbC50cyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdGFibGUvcm93L2NlbGwvY2VsbC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7Ozs7Ozs7Ozs7O0lDRmxELDZCQUNJO0lBQUEsK0NBR3VCO0lBRG5CLHdPQUEwQztJQUM5QyxpQkFBdUI7SUFDM0IsMEJBQWU7OztJQUhQLGVBQWE7SUFBYixrQ0FBYTs7OztJQUtyQiw2QkFDSTtJQUFBLCtDQUd1QjtJQURuQixzTkFBOEI7SUFDbEMsaUJBQXVCO0lBQzNCLDBCQUFlOzs7SUFIUCxlQUFhO0lBQWIsa0NBQWE7OztJQUtyQiw2QkFDSTtJQUFBLDJDQUE2RDtJQUNqRSwwQkFBZTs7O0lBRFksZUFBYTtJQUFiLGtDQUFhOzs7SUFHeEMsNkJBQ0k7SUFBQSw2Q0FBaUU7SUFDckUsMEJBQWU7OztJQURjLGVBQWE7SUFBYixrQ0FBYTs7O0lBRzFDLDZCQUNJO0lBQUEsMkNBQTZEO0lBQ2pFLDBCQUFlOzs7SUFEWSxlQUFhO0lBQWIsa0NBQWE7OztJQUd4Qyw2QkFDSTtJQUFBLDRDQUErRDtJQUNuRSwwQkFBZTs7O0lBRGEsZUFBYTtJQUFiLGtDQUFhOztBRGxCN0MsTUFBTSxPQUFPLFFBQVE7SUFXbkI7UUFUVSxxQkFBZ0IsR0FBeUIsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUNwRSxlQUFVLEdBQThCLElBQUksWUFBWSxFQUFlLENBQUM7UUFJekUsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUVyQyxhQUFRLEdBQVEsU0FBUyxDQUFDO0lBSTFCLENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQztJQUVELGFBQWEsQ0FBQyxPQUFlO1FBQzNCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELE9BQU8sQ0FBQyxHQUFXO1FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUN0QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Z0VBekJVLFFBQVE7NkNBQVIsUUFBUTtRQ1ZyQixnQ0FDSTtRQUFBLDJFQUNJO1FBTUosMkVBQ0k7UUFNSiwyRUFDSTtRQUdKLDJFQUNJO1FBR0osMkVBQ0k7UUFHSiwyRUFDSTtRQUVSLDBCQUFlOztRQTlCRCx3Q0FBc0I7UUFDbEIsZUFBc0M7UUFBdEMsMkRBQXNDO1FBT3RDLGVBQTRCO1FBQTVCLGlEQUE0QjtRQU81QixlQUE2QjtRQUE3QixrREFBNkI7UUFJN0IsZUFBZ0M7UUFBaEMscURBQWdDO1FBSWhDLGVBQTZCO1FBQTdCLGtEQUE2Qjs7a0REYmxDLFFBQVE7Y0FMcEIsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLFdBQVcsRUFBRSxhQUFhO2dCQUMxQixTQUFTLEVBQUUsQ0FBQyxhQUFhLENBQUM7YUFDM0I7c0NBR1csZ0JBQWdCO2tCQUF6QixNQUFNO1lBQ0csVUFBVTtrQkFBbkIsTUFBTTtZQUVFLEdBQUc7a0JBQVgsS0FBSztZQUNHLElBQUk7a0JBQVosS0FBSztZQUNHLFVBQVU7a0JBQWxCLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBDRUxMX1RZUEUgfSBmcm9tIFwiLi4vLi4vY29uc3RhbnRcIjtcbmltcG9ydCB7IFRhYmxlQ2VsbER0byB9IGZyb20gXCIuLi8uLi9kdG8vdGFibGUuY2VsbC5kdG9cIjtcbmltcG9ydCB7IFRhYmxlUm93RHRvIH0gZnJvbSBcIi4uLy4uL2R0by90YWJsZS5yb3cuZHRvXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtdGFibGUtY2VsbFwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2NlbGwuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vY2VsbC5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIENlbGxDb21we1xuICBcbiAgQE91dHB1dCgpIG9wZW5JbWFnZVByZXZpZXc6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XG4gIEBPdXRwdXQoKSByb3dVcGRhdGVkOiBFdmVudEVtaXR0ZXI8VGFibGVSb3dEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcjxUYWJsZVJvd0R0bz4oKTtcblxuICBASW5wdXQoKSByb3c6IFRhYmxlUm93RHRvO1xuICBASW5wdXQoKSBjZWxsOiBUYWJsZUNlbGxEdG87XG4gIEBJbnB1dCgpIGRuZFN1cHBvcnQ6IGJvb2xlYW4gPSBmYWxzZTtcblxuICBjZWxsVHlwZTogYW55ID0gQ0VMTF9UWVBFO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBkb09wZW5QcmV2aWV3KGltZ0NvZGU6IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMub3BlbkltYWdlUHJldmlldy5lbWl0KGltZ0NvZGUpO1xuICB9XG5cbiAgZWRpdFJvdyhzdHI6IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMuY2VsbC52YWx1ZSA9IHN0cjtcbiAgICB0aGlzLnJvd1VwZGF0ZWQuZW1pdCh0aGlzLnJvdyk7XG4gIH1cblxufVxuIiwiPG5nLWNvbnRhaW5lciBbbmdTd2l0Y2hdPVwiY2VsbC50eXBlXCI+XG4gICAgPG5nLWNvbnRhaW5lciAqbmdTd2l0Y2hDYXNlPWNlbGxUeXBlLklNQUdFX1RIVU1CTkFJTD5cbiAgICAgICAgPHZpYS10YWJsZS1jZWxsLWltYWdlXG4gICAgICAgICAgICBbY2VsbF09XCJjZWxsXCJcbiAgICAgICAgICAgIChvcGVuSW1hZ2VQcmV2aWV3KT1cImRvT3BlblByZXZpZXcoJGV2ZW50KVwiPlxuICAgICAgICA8L3ZpYS10YWJsZS1jZWxsLWltYWdlPlxuICAgIDwvbmctY29udGFpbmVyPlxuICAgIFxuICAgIDxuZy1jb250YWluZXIgKm5nU3dpdGNoQ2FzZT1jZWxsVHlwZS5JTlBVVD5cbiAgICAgICAgPHZpYS10YWJsZS1jZWxsLWlucHV0XG4gICAgICAgICAgICBbY2VsbF09XCJjZWxsXCJcbiAgICAgICAgICAgIChyb3dVcGRhdGVkKT1cImVkaXRSb3coJGV2ZW50KVwiPlxuICAgICAgICA8L3ZpYS10YWJsZS1jZWxsLWlucHV0PlxuICAgIDwvbmctY29udGFpbmVyPlxuICAgIFxuICAgIDxuZy1jb250YWluZXIgKm5nU3dpdGNoQ2FzZT1jZWxsVHlwZS5TVEFUVVM+XG4gICAgICAgIDx2aWEtdGFibGUtY2VsbC1zdGF0dXMgW2NlbGxdPVwiY2VsbFwiPjwvdmlhLXRhYmxlLWNlbGwtc3RhdHVzPlxuICAgIDwvbmctY29udGFpbmVyPlxuICAgIFxuICAgIDxuZy1jb250YWluZXIgKm5nU3dpdGNoQ2FzZT1jZWxsVHlwZS5EQVRFX1RJTUU+XG4gICAgICAgIDx2aWEtdGFibGUtY2VsbC1kYXRldGltZSBbY2VsbF09XCJjZWxsXCI+PC92aWEtdGFibGUtY2VsbC1kYXRldGltZT5cbiAgICA8L25nLWNvbnRhaW5lcj5cbiAgICBcbiAgICA8bmctY29udGFpbmVyICpuZ1N3aXRjaENhc2U9Y2VsbFR5cGUuQUNUSU9OPlxuICAgICAgICA8dmlhLXRhYmxlLWNlbGwtYWN0aW9uIFtjZWxsXT1cImNlbGxcIj48L3ZpYS10YWJsZS1jZWxsLWFjdGlvbj5cbiAgICA8L25nLWNvbnRhaW5lcj5cbiAgICBcbiAgICA8bmctY29udGFpbmVyICpuZ1N3aXRjaERlZmF1bHQ+XG4gICAgICAgIDx2aWEtdGFibGUtY2VsbC1kZWZhdWx0IFtjZWxsXT1cImNlbGxcIj48L3ZpYS10YWJsZS1jZWxsLWRlZmF1bHQ+XG4gICAgPC9uZy1jb250YWluZXI+XG48L25nLWNvbnRhaW5lcj4iXX0=