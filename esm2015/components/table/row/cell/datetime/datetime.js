import { Component, Input } from "@angular/core";
import { DATE_FORMAT } from "../../../constant";
import { TableCellDto } from "../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class CellDateTimeComp {
    constructor() {
        this.dateFormat = DATE_FORMAT;
    }
    ngOnInit() {
    }
}
CellDateTimeComp.ɵfac = function CellDateTimeComp_Factory(t) { return new (t || CellDateTimeComp)(); };
CellDateTimeComp.ɵcmp = i0.ɵɵdefineComponent({ type: CellDateTimeComp, selectors: [["via-table-cell-datetime"]], inputs: { cell: "cell" }, decls: 4, vars: 8, consts: [[3, "title"]], template: function CellDateTimeComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 0);
        i0.ɵɵpipe(1, "date");
        i0.ɵɵtext(2);
        i0.ɵɵpipe(3, "date");
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵpropertyInterpolate("title", i0.ɵɵpipeBind2(1, 2, ctx.cell.value, ctx.dateFormat));
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate1(" ", i0.ɵɵpipeBind2(3, 5, ctx.cell.value, ctx.dateFormat), "\n");
    } }, pipes: [i1.DatePipe], styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CellDateTimeComp, [{
        type: Component,
        args: [{
                selector: "via-table-cell-datetime",
                templateUrl: "./datetime.html",
                styleUrls: ["./datetime.scss"]
            }]
    }], function () { return []; }, { cell: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXRpbWUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3RhYmxlL3Jvdy9jZWxsL2RhdGV0aW1lL2RhdGV0aW1lLnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9yb3cvY2VsbC9kYXRldGltZS9kYXRldGltZS5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNoRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7OztBQU8zRCxNQUFNLE9BQU8sZ0JBQWdCO0lBTTNCO1FBRkEsZUFBVSxHQUFRLFdBQVcsQ0FBQztJQUk5QixDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7O2dGQVhVLGdCQUFnQjtxREFBaEIsZ0JBQWdCO1FDVDdCLCtCQUdJOztRQUFBLFlBQ0o7O1FBQUEsaUJBQU87O1FBSEgsdUZBQXlDO1FBRXpDLGVBQ0o7UUFESSxzRkFDSjs7a0RES2EsZ0JBQWdCO2NBTDVCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUseUJBQXlCO2dCQUNuQyxXQUFXLEVBQUUsaUJBQWlCO2dCQUM5QixTQUFTLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQzthQUMvQjtzQ0FHVSxJQUFJO2tCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IERBVEVfRk9STUFUIH0gZnJvbSBcIi4uLy4uLy4uL2NvbnN0YW50XCI7XG5pbXBvcnQgeyBUYWJsZUNlbGxEdG8gfSBmcm9tIFwiLi4vLi4vLi4vZHRvL3RhYmxlLmNlbGwuZHRvXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtdGFibGUtY2VsbC1kYXRldGltZVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2RhdGV0aW1lLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2RhdGV0aW1lLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgQ2VsbERhdGVUaW1lQ29tcHtcbiAgXG4gIEBJbnB1dCgpIGNlbGw6IFRhYmxlQ2VsbER0bztcblxuICBkYXRlRm9ybWF0OiBhbnkgPSBEQVRFX0ZPUk1BVDtcbiAgXG4gIGNvbnN0cnVjdG9yKFxuICApIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cblxufVxuIiwiPHNwYW4gXG4gICAgdGl0bGU9XCJ7e2NlbGwudmFsdWUgIHwgZGF0ZTpkYXRlRm9ybWF0fX1cIlxuICAgID5cbiAgICB7e2NlbGwudmFsdWUgfCBkYXRlOmRhdGVGb3JtYXR9fVxuPC9zcGFuPiJdfQ==