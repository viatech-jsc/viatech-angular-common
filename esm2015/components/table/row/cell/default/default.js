import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { TableCellDto } from "../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common";
export class CellDefaultComp {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    cellClicked(cell) {
        if (cell.routeLink.trim().length > 0) {
            this.router.navigate([cell.routeLink]);
        }
    }
}
CellDefaultComp.ɵfac = function CellDefaultComp_Factory(t) { return new (t || CellDefaultComp)(i0.ɵɵdirectiveInject(i1.Router)); };
CellDefaultComp.ɵcmp = i0.ɵɵdefineComponent({ type: CellDefaultComp, selectors: [["via-table-cell-default"]], inputs: { cell: "cell" }, decls: 2, vars: 3, consts: [[3, "title", "ngClass", "click"]], template: function CellDefaultComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 0);
        i0.ɵɵlistener("click", function CellDefaultComp_Template_span_click_0_listener() { return ctx.cellClicked(ctx.cell); });
        i0.ɵɵtext(1);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵpropertyInterpolate("title", ctx.cell.value);
        i0.ɵɵproperty("ngClass", ctx.cell.routeLink.trim().length > 0 ? "hand" : "");
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate1(" ", ctx.cell.value, "\n");
    } }, directives: [i2.NgClass], styles: [".hand[_ngcontent-%COMP%]{cursor:pointer}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CellDefaultComp, [{
        type: Component,
        args: [{
                selector: "via-table-cell-default",
                templateUrl: "./default.html",
                styleUrls: ["./default.scss"]
            }]
    }], function () { return [{ type: i1.Router }]; }, { cell: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmYXVsdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdGFibGUvcm93L2NlbGwvZGVmYXVsdC9kZWZhdWx0LnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9yb3cvY2VsbC9kZWZhdWx0L2RlZmF1bHQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDZCQUE2QixDQUFDOzs7O0FBTzNELE1BQU0sT0FBTyxlQUFlO0lBSTFCLFlBQ1UsTUFBYztRQUFkLFdBQU0sR0FBTixNQUFNLENBQVE7SUFFeEIsQ0FBQztJQUVELFFBQVE7SUFDUixDQUFDO0lBRUQsV0FBVyxDQUFDLElBQWtCO1FBQzVCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7U0FDeEM7SUFDSCxDQUFDOzs4RUFoQlUsZUFBZTtvREFBZixlQUFlO1FDVDVCLCtCQUtJO1FBSEEsMEZBQVMseUJBQWlCLElBQUM7UUFHM0IsWUFDSjtRQUFBLGlCQUFPOztRQUxILGlEQUFzQjtRQUV0Qiw0RUFBMEQ7UUFFMUQsZUFDSjtRQURJLGdEQUNKOztrRERHYSxlQUFlO2NBTDNCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsd0JBQXdCO2dCQUNsQyxXQUFXLEVBQUUsZ0JBQWdCO2dCQUM3QixTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQzthQUM5Qjt5REFHVSxJQUFJO2tCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFRhYmxlQ2VsbER0byB9IGZyb20gXCIuLi8uLi8uLi9kdG8vdGFibGUuY2VsbC5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10YWJsZS1jZWxsLWRlZmF1bHRcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9kZWZhdWx0Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2RlZmF1bHQuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBDZWxsRGVmYXVsdENvbXB7XG4gIFxuICBASW5wdXQoKSBjZWxsOiBUYWJsZUNlbGxEdG87XG4gIFxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBjZWxsQ2xpY2tlZChjZWxsOiBUYWJsZUNlbGxEdG8pOiB2b2lkIHtcbiAgICBpZiAoY2VsbC5yb3V0ZUxpbmsudHJpbSgpLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtjZWxsLnJvdXRlTGlua10pO1xuICAgIH1cbiAgfVxufVxuIiwiPHNwYW4gXG4gICAgdGl0bGU9XCJ7e2NlbGwudmFsdWV9fVwiIFxuICAgIChjbGljayk9XCJjZWxsQ2xpY2tlZChjZWxsKVwiIFxuICAgIFtuZ0NsYXNzXT1cImNlbGwucm91dGVMaW5rLnRyaW0oKS5sZW5ndGggPiAwID8gJ2hhbmQnIDogJydcIlxuICAgID5cbiAgICB7e2NlbGwudmFsdWV9fVxuPC9zcGFuPiJdfQ==