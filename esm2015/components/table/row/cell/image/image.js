import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TableCellDto } from "../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
export class CellImageComp {
    constructor() {
        this.openImagePreview = new EventEmitter();
    }
    ngOnInit() {
    }
    doOpenPreview(imgCode) {
        this.openImagePreview.emit(imgCode);
    }
}
CellImageComp.ɵfac = function CellImageComp_Factory(t) { return new (t || CellImageComp)(); };
CellImageComp.ɵcmp = i0.ɵɵdefineComponent({ type: CellImageComp, selectors: [["via-table-cell-image"]], inputs: { cell: "cell" }, outputs: { openImagePreview: "openImagePreview" }, decls: 2, vars: 1, consts: [[1, "thumbnail-wrapper"], ["width", "60px", 3, "src", "click"]], template: function CellImageComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 0);
        i0.ɵɵelementStart(1, "img", 1);
        i0.ɵɵlistener("click", function CellImageComp_Template_img_click_1_listener() { return ctx.doOpenPreview(ctx.cell.value); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("src", ctx.cell.value, i0.ɵɵsanitizeUrl);
    } }, styles: [".thumbnail-wrapper[_ngcontent-%COMP%]{cursor:pointer;height:30px;overflow:hidden;width:60px}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CellImageComp, [{
        type: Component,
        args: [{
                selector: "via-table-cell-image",
                templateUrl: "./image.html",
                styleUrls: ["./image.scss"]
            }]
    }], function () { return []; }, { openImagePreview: [{
            type: Output
        }], cell: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1hZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3RhYmxlL3Jvdy9jZWxsL2ltYWdlL2ltYWdlLnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9yb3cvY2VsbC9pbWFnZS9pbWFnZS5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDZCQUE2QixDQUFDOztBQU8zRCxNQUFNLE9BQU8sYUFBYTtJQU14QjtRQUpVLHFCQUFnQixHQUF5QixJQUFJLFlBQVksRUFBVSxDQUFDO0lBTTlFLENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQztJQUVELGFBQWEsQ0FBQyxPQUFlO1FBQzNCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7MEVBZlUsYUFBYTtrREFBYixhQUFhO1FDUjFCLCtCQUNJO1FBRUksOEJBRVI7UUFGYSx1RkFBUyxpQ0FBeUIsSUFBQztRQUF4QyxpQkFFUjtRQUFBLGlCQUFPOztRQURDLGVBQWtCO1FBQWxCLHNEQUFrQjs7a0RESWIsYUFBYTtjQUx6QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjtnQkFDaEMsV0FBVyxFQUFFLGNBQWM7Z0JBQzNCLFNBQVMsRUFBRSxDQUFDLGNBQWMsQ0FBQzthQUM1QjtzQ0FHVyxnQkFBZ0I7a0JBQXpCLE1BQU07WUFFRSxJQUFJO2tCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBUYWJsZUNlbGxEdG8gfSBmcm9tIFwiLi4vLi4vLi4vZHRvL3RhYmxlLmNlbGwuZHRvXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtdGFibGUtY2VsbC1pbWFnZVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2ltYWdlLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2ltYWdlLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgQ2VsbEltYWdlQ29tcHtcbiAgXG4gIEBPdXRwdXQoKSBvcGVuSW1hZ2VQcmV2aWV3OiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xuICBcbiAgQElucHV0KCkgY2VsbDogVGFibGVDZWxsRHRvO1xuICBcbiAgY29uc3RydWN0b3IoXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBkb09wZW5QcmV2aWV3KGltZ0NvZGU6IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMub3BlbkltYWdlUHJldmlldy5lbWl0KGltZ0NvZGUpO1xuICB9XG5cbn1cbiIsIjxzcGFuIGNsYXNzPVwidGh1bWJuYWlsLXdyYXBwZXJcIj5cbiAgICA8IS0tIDxpbWcgKGNsaWNrKT1cImRvT3BlblByZXZpZXcoY2VsbC52YWx1ZSlcIiB3aWR0aD1cIjYwcHhcIlxuICAgICAgICBbc3JjXT1cImRvd25sb2FkRmlsZVBhdGggKyBjZWxsLnZhbHVlICsgJ190aHVtYidcIiAvPiAtLT5cbiAgICAgICAgPGltZyAoY2xpY2spPVwiZG9PcGVuUHJldmlldyhjZWxsLnZhbHVlKVwiIHdpZHRoPVwiNjBweFwiXG4gICAgICAgIFtzcmNdPVwiY2VsbC52YWx1ZVwiIC8+XG48L3NwYW4+Il19