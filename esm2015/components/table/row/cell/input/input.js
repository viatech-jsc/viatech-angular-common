import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { clone } from "lodash";
import { TableCellDto } from "../../../dto/table.cell.dto";
import { TableRowDto } from "../../../dto/table.row.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/button";
import * as i4 from "@angular/material/icon";
const _c0 = ["inputElement"];
function CellInputComp_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 5);
    i0.ɵɵlistener("click", function CellInputComp_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r4); const ctx_r3 = i0.ɵɵnextContext(); return ctx_r3.edit(); });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2, "edit");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function CellInputComp_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 6);
    i0.ɵɵtext(1, " maxlength ");
    i0.ɵɵelementEnd();
} }
export class CellInputComp {
    constructor() {
        this.rowUpdated = new EventEmitter();
        this.inputMaxLenght = 255;
        // originalValue: string = "";
        this.enabled = false;
        this.oldValue = "";
    }
    ngOnInit() {
        // this.originalValue = clone(this.cell.value);
        this.oldValue = clone(this.cell.value);
    }
    focusInput() {
        setTimeout(() => {
            this.inputElement.nativeElement.focus();
        }, 0);
    }
    keyUpEnter() {
        this.enabled = false;
        this.doUpdateInputEvent(true, this.cell, this.oldValue);
    }
    edit() {
        this.enabled = true;
        this.focusInput();
    }
    focusin() {
        this.oldValue = clone(this.cell.value);
    }
    focusout() {
        this.doUpdateInputEvent(this.enabled, this.cell, this.oldValue);
        this.enabled = false;
    }
    doUpdateInputEvent(canEdit, newValue, originalValue) {
        if (canEdit && newValue && newValue.value !== originalValue) {
            if (newValue.value && newValue.value.length <= this.inputMaxLenght) {
                this.cell.value = newValue.value;
                this.rowUpdated.emit(this.row);
            }
            else {
                newValue.value = originalValue;
            }
        }
    }
}
CellInputComp.ɵfac = function CellInputComp_Factory(t) { return new (t || CellInputComp)(); };
CellInputComp.ɵcmp = i0.ɵɵdefineComponent({ type: CellInputComp, selectors: [["via-table-cell-input"]], viewQuery: function CellInputComp_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.inputElement = _t.first);
    } }, inputs: { row: "row", cell: "cell" }, outputs: { rowUpdated: "rowUpdated" }, decls: 5, vars: 4, consts: [[1, "cell-input-wrapper"], [1, "cell-input", 3, "ngModel", "disabled", "ngModelChange", "focusin", "focusout", "keyup.enter"], ["inputElement", ""], ["mat-icon-button", "", "color", "primary", 3, "click", 4, "ngIf"], ["class", "danger-text", "translate", "", 4, "ngIf"], ["mat-icon-button", "", "color", "primary", 3, "click"], ["translate", "", 1, "danger-text"]], template: function CellInputComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 0);
        i0.ɵɵelementStart(1, "input", 1, 2);
        i0.ɵɵlistener("ngModelChange", function CellInputComp_Template_input_ngModelChange_1_listener($event) { return ctx.cell.value = $event; })("focusin", function CellInputComp_Template_input_focusin_1_listener() { return ctx.focusin(); })("focusout", function CellInputComp_Template_input_focusout_1_listener() { return ctx.focusout(); })("keyup.enter", function CellInputComp_Template_input_keyup_enter_1_listener() { return ctx.keyUpEnter(); });
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(3, CellInputComp_button_3_Template, 3, 0, "button", 3);
        i0.ɵɵtemplate(4, CellInputComp_div_4_Template, 2, 0, "div", 4);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngModel", ctx.cell.value)("disabled", !ctx.enabled);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", !ctx.enabled);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.cell.value.length > ctx.inputMaxLenght);
    } }, directives: [i1.DefaultValueAccessor, i1.NgControlStatus, i1.NgModel, i2.NgIf, i3.MatButton, i4.MatIcon], styles: [".cell-input-wrapper[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%]{font-size:15px}.cell-input-wrapper[_ngcontent-%COMP%]   .cell-input[_ngcontent-%COMP%]{background-color:inherit;border:0}.cell-input-wrapper[_ngcontent-%COMP%]   .cell-input[_ngcontent-%COMP%]:focus{border-bottom:1px solid #07f}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CellInputComp, [{
        type: Component,
        args: [{
                selector: "via-table-cell-input",
                templateUrl: "./input.html",
                styleUrls: ["./input.scss"]
            }]
    }], function () { return []; }, { inputElement: [{
            type: ViewChild,
            args: ['inputElement']
        }], rowUpdated: [{
            type: Output
        }], row: [{
            type: Input
        }], cell: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5wdXQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3RhYmxlL3Jvdy9jZWxsL2lucHV0L2lucHV0LnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9yb3cvY2VsbC9pbnB1dC9pbnB1dC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sUUFBUSxDQUFDO0FBQy9CLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7Ozs7Ozs7OztJQ1FyRCxpQ0FLSTtJQUhBLDhLQUFnQjtJQUdoQixnQ0FBVTtJQUFBLG9CQUFJO0lBQUEsaUJBQVc7SUFDN0IsaUJBQVM7OztJQUNULDhCQUNJO0lBQUEsMkJBQ0o7SUFBQSxpQkFBTTs7QURWVixNQUFNLE9BQU8sYUFBYTtJQWF4QjtRQVZVLGVBQVUsR0FBOEIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUtyRSxtQkFBYyxHQUFXLEdBQUcsQ0FBQztRQUM3Qiw4QkFBOEI7UUFDOUIsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUN6QixhQUFRLEdBQVcsRUFBRSxDQUFDO0lBSXRCLENBQUM7SUFFRCxRQUFRO1FBQ04sK0NBQStDO1FBQy9DLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELFVBQVU7UUFDUixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDMUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQztJQUVELFVBQVU7UUFDUixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRCxJQUFJO1FBQ0YsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxPQUFPO1FBQ0wsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxPQUFZLEVBQUUsUUFBYSxFQUFFLGFBQXFCO1FBQ25FLElBQUksT0FBTyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxLQUFLLGFBQWEsRUFBRTtZQUMzRCxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDbEUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQztnQkFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2hDO2lCQUFNO2dCQUNMLFFBQVEsQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDO2FBQ2hDO1NBQ0Y7SUFDSCxDQUFDOzswRUF4RFUsYUFBYTtrREFBYixhQUFhOzs7Ozs7UUNWMUIsK0JBQ0k7UUFDQSxtQ0FTQTtRQU5JLDBJQUF3QixnRkFFYixhQUFTLElBRkksa0ZBR1osY0FBVSxJQUhFLHdGQUlULGdCQUFZLElBSkg7UUFINUIsaUJBU0E7UUFBQSxvRUFLSTtRQUVKLDhEQUNJO1FBRVIsaUJBQU87O1FBaEJDLGVBQXdCO1FBQXhCLHdDQUF3QiwwQkFBQTtRQU94QixlQUFnQjtRQUFoQixtQ0FBZ0I7UUFNSyxlQUF3QztRQUF4QyxpRUFBd0M7O2tERFJ4RCxhQUFhO2NBTHpCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsc0JBQXNCO2dCQUNoQyxXQUFXLEVBQUUsY0FBYztnQkFDM0IsU0FBUyxFQUFFLENBQUMsY0FBYyxDQUFDO2FBQzVCO3NDQUU0QixZQUFZO2tCQUF0QyxTQUFTO21CQUFDLGNBQWM7WUFFZixVQUFVO2tCQUFuQixNQUFNO1lBRUUsR0FBRztrQkFBWCxLQUFLO1lBQ0csSUFBSTtrQkFBWixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBjbG9uZSB9IGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IFRhYmxlQ2VsbER0byB9IGZyb20gXCIuLi8uLi8uLi9kdG8vdGFibGUuY2VsbC5kdG9cIjtcbmltcG9ydCB7IFRhYmxlUm93RHRvIH0gZnJvbSBcIi4uLy4uLy4uL2R0by90YWJsZS5yb3cuZHRvXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtdGFibGUtY2VsbC1pbnB1dFwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL2lucHV0Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL2lucHV0LnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgQ2VsbElucHV0Q29tcHtcbiAgQFZpZXdDaGlsZCgnaW5wdXRFbGVtZW50JykgaW5wdXRFbGVtZW50OiBFbGVtZW50UmVmO1xuICBcbiAgQE91dHB1dCgpIHJvd1VwZGF0ZWQ6IEV2ZW50RW1pdHRlcjxUYWJsZVJvd0R0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIFxuICBASW5wdXQoKSByb3c6IFRhYmxlUm93RHRvO1xuICBASW5wdXQoKSBjZWxsOiBUYWJsZUNlbGxEdG87XG5cbiAgaW5wdXRNYXhMZW5naHQ6IG51bWJlciA9IDI1NTtcbiAgLy8gb3JpZ2luYWxWYWx1ZTogc3RyaW5nID0gXCJcIjtcbiAgZW5hYmxlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBvbGRWYWx1ZTogc3RyaW5nID0gXCJcIjtcbiAgXG4gIGNvbnN0cnVjdG9yKFxuICApIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIC8vIHRoaXMub3JpZ2luYWxWYWx1ZSA9IGNsb25lKHRoaXMuY2VsbC52YWx1ZSk7XG4gICAgdGhpcy5vbGRWYWx1ZSA9IGNsb25lKHRoaXMuY2VsbC52YWx1ZSk7XG4gIH1cbiAgXG4gIGZvY3VzSW5wdXQoKSB7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB0aGlzLmlucHV0RWxlbWVudC5uYXRpdmVFbGVtZW50LmZvY3VzKCk7XG4gICAgfSwgMCk7XG4gIH1cblxuICBrZXlVcEVudGVyKCkge1xuICAgIHRoaXMuZW5hYmxlZCA9IGZhbHNlO1xuICAgIHRoaXMuZG9VcGRhdGVJbnB1dEV2ZW50KHRydWUsIHRoaXMuY2VsbCwgdGhpcy5vbGRWYWx1ZSk7XG4gIH1cblxuICBlZGl0KCkge1xuICAgIHRoaXMuZW5hYmxlZCA9IHRydWU7XG4gICAgdGhpcy5mb2N1c0lucHV0KCk7XG4gIH1cblxuICBmb2N1c2luKCkge1xuICAgIHRoaXMub2xkVmFsdWUgPSBjbG9uZSh0aGlzLmNlbGwudmFsdWUpO1xuICB9XG5cbiAgZm9jdXNvdXQoKSB7XG4gICAgdGhpcy5kb1VwZGF0ZUlucHV0RXZlbnQodGhpcy5lbmFibGVkLCB0aGlzLmNlbGwsIHRoaXMub2xkVmFsdWUpO1xuICAgIHRoaXMuZW5hYmxlZCA9IGZhbHNlO1xuICB9XG5cbiAgZG9VcGRhdGVJbnB1dEV2ZW50KGNhbkVkaXQ6IGFueSwgbmV3VmFsdWU6IGFueSwgb3JpZ2luYWxWYWx1ZTogc3RyaW5nKTogdm9pZCB7XG4gICAgaWYgKGNhbkVkaXQgJiYgbmV3VmFsdWUgJiYgbmV3VmFsdWUudmFsdWUgIT09IG9yaWdpbmFsVmFsdWUpIHtcbiAgICAgIGlmIChuZXdWYWx1ZS52YWx1ZSAmJiBuZXdWYWx1ZS52YWx1ZS5sZW5ndGggPD0gdGhpcy5pbnB1dE1heExlbmdodCkge1xuICAgICAgICB0aGlzLmNlbGwudmFsdWUgPSBuZXdWYWx1ZS52YWx1ZTtcbiAgICAgICAgdGhpcy5yb3dVcGRhdGVkLmVtaXQodGhpcy5yb3cpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgbmV3VmFsdWUudmFsdWUgPSBvcmlnaW5hbFZhbHVlO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG59XG4iLCI8c3BhbiBjbGFzcz1cImNlbGwtaW5wdXQtd3JhcHBlclwiPlxuICAgIDwhLS0ge3tjZWxsLnZhbHVlfX0gLS0+XG4gICAgPGlucHV0IFxuICAgICAgICAjaW5wdXRFbGVtZW50XG4gICAgICAgIGNsYXNzPVwiY2VsbC1pbnB1dFwiIFxuICAgICAgICBbKG5nTW9kZWwpXT1cImNlbGwudmFsdWVcIlxuICAgICAgICBbZGlzYWJsZWRdPVwiIWVuYWJsZWRcIiBcbiAgICAgICAgKGZvY3VzaW4pPVwiZm9jdXNpbigpXCJcbiAgICAgICAgKGZvY3Vzb3V0KT1cImZvY3Vzb3V0KClcIlxuICAgICAgICAoa2V5dXAuZW50ZXIpPVwia2V5VXBFbnRlcigpXCJcbiAgICAgICAgPlxuICAgIDxidXR0b24gXG4gICAgICAgICpuZ0lmPVwiIWVuYWJsZWRcIiBcbiAgICAgICAgKGNsaWNrKT1cImVkaXQoKVwiXG4gICAgICAgIG1hdC1pY29uLWJ1dHRvbiBcbiAgICAgICAgY29sb3I9XCJwcmltYXJ5XCI+XG4gICAgICAgIDxtYXQtaWNvbj5lZGl0PC9tYXQtaWNvbj5cbiAgICA8L2J1dHRvbj5cbiAgICA8ZGl2IGNsYXNzPVwiZGFuZ2VyLXRleHRcIiAqbmdJZj1cImNlbGwudmFsdWUubGVuZ3RoPmlucHV0TWF4TGVuZ2h0XCIgdHJhbnNsYXRlPlxuICAgICAgICBtYXhsZW5ndGhcbiAgICA8L2Rpdj5cbjwvc3Bhbj5cbjwhLS0gXG5cbjxzcGFuICpuZ1N3aXRjaENhc2U9Y2VsbFR5cGUuSU5QVVQgY2xhc3M9XCJjZWxsLWlucHV0LXdyYXBwZXJcIj5cbiAgICB7e2NlbGwub3JpZ2luYWxWYWx1ZX19XG4gICAgPGlucHV0IGNsYXNzPVwiY2VsbC1pbnB1dFwiIFsobmdNb2RlbCldPVwiY2VsbC52YWx1ZVwiIGlkPVwie3snaW5wdXQtJyArIGl9fVwiXG4gICAgICAgIFtkaXNhYmxlZF09XCIhY2VsbC5lbmFibGVkIHx8ICFkbmRTdXBwb3J0XCIgKGZvY3VzaW4pPVwiY2VsbC5vbGRWYWx1ZSA9IGNlbGwudmFsdWVcIlxuICAgICAgICAoZm9jdXNvdXQpPVwiZG9VcGRhdGVJbnB1dEV2ZW50KHJvdywgY2VsbC5lbmFibGVkLCBjZWxsLnZhbHVlLCBjZWxsLm9sZFZhbHVlKTsgY2VsbC5lbmFibGVkID0gZmFsc2VcIlxuICAgICAgICAoa2V5dXAuZW50ZXIpPVwiY2VsbC5lbmFibGVkID0gZmFsc2U7IGRvVXBkYXRlSW5wdXRFdmVudChyb3csIHRydWUsIGNlbGwsIGNlbGwub2xkVmFsdWUpXCI+XG4gICAgPGJ1dHRvbiAoY2xpY2spPVwiY2VsbC5lbmFibGVkID0gdHJ1ZTsgZm9jdXNJbnB1dCgnaW5wdXQtJyArIGkpO1wiXG4gICAgICAgICpuZ0lmPVwiIWNlbGwuZW5hYmxlZCAmJiBkbmRTdXBwb3J0XCIgY2xhc3M9XCJidG4gYnRuLWxpbmsgYnRuLWFjdGlvbiBjZWxsLWVkaXQtaWNvblwiPlxuICAgICAgICA8aSBjbGFzcz1cImZhIGZhLXBlbmNpbFwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICA8L2J1dHRvbj5cbiAgICA8ZGl2IGNsYXNzPVwiZGFuZ2VyLXRleHRcIiAqbmdJZj1cImNlbGwudmFsdWUubGVuZ3RoPmlucHV0TWF4TGVuZ2h0XCIgdHJhbnNsYXRlPm1heGxlbmd0aFxuICAgIDwvZGl2PlxuPC9zcGFuPlxuXG4gLS0+Il19