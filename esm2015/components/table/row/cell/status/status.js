import { Component, Input } from "@angular/core";
import { TableCellDto } from "../../../dto/table.cell.dto";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class CellStatusComp {
    constructor() {
    }
    ngOnInit() {
    }
}
CellStatusComp.ɵfac = function CellStatusComp_Factory(t) { return new (t || CellStatusComp)(); };
CellStatusComp.ɵcmp = i0.ɵɵdefineComponent({ type: CellStatusComp, selectors: [["via-table-cell-status"]], inputs: { cell: "cell" }, decls: 2, vars: 3, consts: [[1, "status", 3, "title", "ngClass"]], template: function CellStatusComp_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 0);
        i0.ɵɵtext(1);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵpropertyInterpolate("title", ctx.cell.value);
        i0.ɵɵproperty("ngClass", ctx.cell.value);
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate1(" ", ctx.cell.value, "\n");
    } }, directives: [i1.NgClass], styles: [".ACTION[_ngcontent-%COMP%]{text-align:center!important}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CellStatusComp, [{
        type: Component,
        args: [{
                selector: "via-table-cell-status",
                templateUrl: "./status.html",
                styleUrls: ["./status.scss"]
            }]
    }], function () { return []; }, { cell: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdHVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9yb3cvY2VsbC9zdGF0dXMvc3RhdHVzLnRzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90YWJsZS9yb3cvY2VsbC9zdGF0dXMvc3RhdHVzLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDZCQUE2QixDQUFDOzs7QUFPM0QsTUFBTSxPQUFPLGNBQWM7SUFJekI7SUFFQSxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7OzRFQVRVLGNBQWM7bURBQWQsY0FBYztRQ1IzQiwrQkFLSTtRQUFBLFlBQ0o7UUFBQSxpQkFBTzs7UUFMSCxpREFBdUI7UUFFdkIsd0NBQXNCO1FBRXRCLGVBQ0o7UUFESSxnREFDSjs7a0RERWEsY0FBYztjQUwxQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtnQkFDakMsV0FBVyxFQUFFLGVBQWU7Z0JBQzVCLFNBQVMsRUFBRSxDQUFDLGVBQWUsQ0FBQzthQUM3QjtzQ0FHVSxJQUFJO2tCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFRhYmxlQ2VsbER0byB9IGZyb20gXCIuLi8uLi8uLi9kdG8vdGFibGUuY2VsbC5kdG9cIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10YWJsZS1jZWxsLXN0YXR1c1wiLFxuICB0ZW1wbGF0ZVVybDogXCIuL3N0YXR1cy5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9zdGF0dXMuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBDZWxsU3RhdHVzQ29tcHtcbiAgXG4gIEBJbnB1dCgpIGNlbGw6IFRhYmxlQ2VsbER0bztcbiAgXG4gIGNvbnN0cnVjdG9yKFxuICApIHtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cblxufVxuIiwiPHNwYW4gXG4gICAgdGl0bGU9XCJ7e2NlbGwudmFsdWUgfX1cIiBcbiAgICBjbGFzcz1cInN0YXR1c1wiIFxuICAgIFtuZ0NsYXNzXT1cImNlbGwudmFsdWVcIlxuICAgID5cbiAgICB7e2NlbGwudmFsdWV9fVxuPC9zcGFuPiJdfQ==