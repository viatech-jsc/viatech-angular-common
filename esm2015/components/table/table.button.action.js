export class ButtonAction {
    constructor(name = "", callbackFunc, disabled = false, iconUrl = undefined, isDropdownItem = false) {
        this._isDropdownItem = false;
        this._name = name;
        this._buttonAction = callbackFunc;
        this._disabled = disabled;
        this._iconUrl = iconUrl;
        this._isDropdownItem = isDropdownItem;
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter buttonAction
     * return {Function}
     */
    get buttonAction() {
        return this._buttonAction;
    }
    /**
     * Getter disabled
     * return {boolean}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * Getter iconUrl
     * return {string}
     */
    get iconUrl() {
        return this._iconUrl;
    }
    /**
     * Getter isDropdownItem
     * return {boolean }
     */
    get isDropdownItem() {
        return this._isDropdownItem;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter buttonAction
     * param {Function} value
     */
    set buttonAction(value) {
        this._buttonAction = value;
    }
    /**
     * Setter disabled
     * param {boolean} value
     */
    set disabled(value) {
        this._disabled = value;
    }
    /**
     * Setter iconUrl
     * param {string} value
     */
    set iconUrl(value) {
        this._iconUrl = value;
    }
    /**
     * Setter isDropdownItem
     * param {boolean } value
     */
    set isDropdownItem(value) {
        this._isDropdownItem = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuYnV0dG9uLmFjdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL3RhYmxlL3RhYmxlLmJ1dHRvbi5hY3Rpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsTUFBTSxPQUFPLFlBQVk7SUFPckIsWUFBWSxJQUFJLEdBQUcsRUFBRSxFQUFFLFlBQXFDLEVBQUUsV0FBb0IsS0FBSyxFQUFFLFVBQWtCLFNBQVMsRUFBRSxpQkFBMEIsS0FBSztRQUY3SSxvQkFBZSxHQUFZLEtBQUssQ0FBQztRQUdyQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQztRQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztRQUN4QixJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQztJQUMxQyxDQUFDO0lBR0Q7OztPQUdHO0lBQ04sSUFBVyxJQUFJO1FBQ2QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ25CLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFlBQVk7UUFDdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzNCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFFBQVE7UUFDbEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3ZCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLE9BQU87UUFDakIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3RCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLGNBQWM7UUFDeEIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQzdCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLElBQUksQ0FBQyxLQUFhO1FBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3BCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFlBQVksQ0FBQyxLQUFlO1FBQ3RDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQzVCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFFBQVEsQ0FBQyxLQUFjO1FBQ2pDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLE9BQU8sQ0FBQyxLQUFhO1FBQy9CLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQ3ZCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLGNBQWMsQ0FBQyxLQUFjO1FBQ3ZDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQzlCLENBQUM7Q0FFRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRhYmxlUm93IH0gZnJvbSBcIi4vdGFibGUucm93XCI7XG5cbmV4cG9ydCBjbGFzcyBCdXR0b25BY3Rpb24ge1xuICAgIHByaXZhdGUgX25hbWU6IHN0cmluZztcbiAgICBwcml2YXRlIF9idXR0b25BY3Rpb246IEZ1bmN0aW9uO1xuICAgIHByaXZhdGUgX2Rpc2FibGVkOiBib29sZWFuO1xuICAgIHByaXZhdGUgX2ljb25Vcmw6IHN0cmluZztcbiAgICBwcml2YXRlIF9pc0Ryb3Bkb3duSXRlbTogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgY29uc3RydWN0b3IobmFtZSA9IFwiXCIsIGNhbGxiYWNrRnVuYzogKHJvdzogVGFibGVSb3cpID0+IHZvaWQsIGRpc2FibGVkOiBib29sZWFuID0gZmFsc2UsIGljb25Vcmw6IHN0cmluZyA9IHVuZGVmaW5lZCwgaXNEcm9wZG93bkl0ZW06IGJvb2xlYW4gPSBmYWxzZSl7XG4gICAgICAgIHRoaXMuX25hbWUgPSBuYW1lO1xuICAgICAgICB0aGlzLl9idXR0b25BY3Rpb24gPSBjYWxsYmFja0Z1bmM7XG4gICAgICAgIHRoaXMuX2Rpc2FibGVkID0gZGlzYWJsZWQ7XG4gICAgICAgIHRoaXMuX2ljb25VcmwgPSBpY29uVXJsO1xuICAgICAgICB0aGlzLl9pc0Ryb3Bkb3duSXRlbSA9IGlzRHJvcGRvd25JdGVtO1xuICAgIH1cblxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIG5hbWVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBuYW1lKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX25hbWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBidXR0b25BY3Rpb25cbiAgICAgKiByZXR1cm4ge0Z1bmN0aW9ufVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGJ1dHRvbkFjdGlvbigpOiBGdW5jdGlvbiB7XG5cdFx0cmV0dXJuIHRoaXMuX2J1dHRvbkFjdGlvbjtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGRpc2FibGVkXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGRpc2FibGVkKCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLl9kaXNhYmxlZDtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGljb25VcmxcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBpY29uVXJsKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX2ljb25Vcmw7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBpc0Ryb3Bkb3duSXRlbVxuICAgICAqIHJldHVybiB7Ym9vbGVhbiB9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgaXNEcm9wZG93bkl0ZW0oKTogYm9vbGVhbiAge1xuXHRcdHJldHVybiB0aGlzLl9pc0Ryb3Bkb3duSXRlbTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIG5hbWVcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IG5hbWUodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX25hbWUgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGJ1dHRvbkFjdGlvblxuICAgICAqIHBhcmFtIHtGdW5jdGlvbn0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBidXR0b25BY3Rpb24odmFsdWU6IEZ1bmN0aW9uKSB7XG5cdFx0dGhpcy5fYnV0dG9uQWN0aW9uID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBkaXNhYmxlZFxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGRpc2FibGVkKHZhbHVlOiBib29sZWFuKSB7XG5cdFx0dGhpcy5fZGlzYWJsZWQgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGljb25VcmxcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGljb25VcmwodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX2ljb25VcmwgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGlzRHJvcGRvd25JdGVtXG4gICAgICogcGFyYW0ge2Jvb2xlYW4gfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGlzRHJvcGRvd25JdGVtKHZhbHVlOiBib29sZWFuICkge1xuXHRcdHRoaXMuX2lzRHJvcGRvd25JdGVtID0gdmFsdWU7XG5cdH1cblxufVxuIl19