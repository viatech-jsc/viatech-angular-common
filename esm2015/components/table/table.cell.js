export class TableCell {
    constructor(value = "", css = "", type = CELL_TYPE.STRING, routeLink = "", buttonActions = [], buttonDropdownActions = []) {
        this._value = value;
        this._css = css;
        this._type = type;
        this._routeLink = routeLink;
        this._buttonActions = buttonActions;
        this._buttonDropdownActions = buttonDropdownActions;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter type
     * return {string}
     */
    get type() {
        return this._type;
    }
    /**
     * Getter routeLink
     * return {string}
     */
    get routeLink() {
        return this._routeLink;
    }
    /**
     * Getter buttonActions
     * return {ButtonAction[]}
     */
    get buttonActions() {
        return this._buttonActions;
    }
    /**
     * Getter buttonDropdownActions
     * return {ButtonAction[]}
     */
    get buttonDropdownActions() {
        return this._buttonDropdownActions;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter type
     * param {string} value
     */
    set type(value) {
        this._type = value;
    }
    /**
     * Setter routeLink
     * param {string} value
     */
    set routeLink(value) {
        this._routeLink = value;
    }
    /**
     * Setter buttonActions
     * param {ButtonAction[]} value
     */
    set buttonActions(value) {
        this._buttonActions = value;
    }
    /**
     * Setter buttonDropdownActions
     * param {ButtonAction[]} value
     */
    set buttonDropdownActions(value) {
        this._buttonDropdownActions = value;
    }
}
export const CELL_TYPE = {
    STRING: "STRING",
    DATE_TIME: "DATE_TIME",
    STATUS: "STATUS",
    ACTION: "ACTION",
    IMAGE_THUMBNAIL: "IMAGE_THUMBNAIL",
    INPUT: "INPUT"
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY2VsbC5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL3RhYmxlL3RhYmxlLmNlbGwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsTUFBTSxPQUFPLFNBQVM7SUFRakIsWUFBWSxLQUFLLEdBQUcsRUFBRSxFQUFFLEdBQUcsR0FBRyxFQUFFLEVBQUUsSUFBSSxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsU0FBUyxHQUFHLEVBQUUsRUFBRSxhQUFhLEdBQUcsRUFBRSxFQUFFLHFCQUFxQixHQUFHLEVBQUU7UUFDdEgsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7UUFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7UUFDNUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUM7UUFDcEMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLHFCQUFxQixDQUFDO0lBQ3hELENBQUM7SUFFRDs7O09BR0c7SUFDTixJQUFXLEtBQUs7UUFDZixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDcEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsR0FBRztRQUNiLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNsQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJO1FBQ2QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ25CLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFNBQVM7UUFDbkIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3hCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLGFBQWE7UUFDdkIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQzVCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLHFCQUFxQjtRQUMvQixPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQztJQUNwQyxDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxLQUFLLENBQUMsS0FBYTtRQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUNyQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxHQUFHLENBQUMsS0FBYTtRQUMzQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJLENBQUMsS0FBYTtRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxTQUFTLENBQUMsS0FBYTtRQUNqQyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxhQUFhLENBQUMsS0FBcUI7UUFDN0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcscUJBQXFCLENBQUMsS0FBcUI7UUFDckQsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztJQUNyQyxDQUFDO0NBR0Q7QUFFRCxNQUFNLENBQUMsTUFBTSxTQUFTLEdBQUc7SUFDckIsTUFBTSxFQUFFLFFBQVE7SUFDaEIsU0FBUyxFQUFFLFdBQVc7SUFDdEIsTUFBTSxFQUFFLFFBQVE7SUFDaEIsTUFBTSxFQUFFLFFBQVE7SUFDaEIsZUFBZSxFQUFFLGlCQUFpQjtJQUNsQyxLQUFLLEVBQUUsT0FBTztDQUNqQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQnV0dG9uQWN0aW9uIH0gZnJvbSBcIi4vdGFibGUuYnV0dG9uLmFjdGlvblwiO1xuXG5leHBvcnQgY2xhc3MgVGFibGVDZWxse1xuICAgIHByaXZhdGUgX3ZhbHVlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfY3NzOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfdHlwZTogc3RyaW5nO1xuICAgIHByaXZhdGUgX3JvdXRlTGluazogc3RyaW5nO1xuICAgIHByaXZhdGUgX2J1dHRvbkFjdGlvbnM6IEJ1dHRvbkFjdGlvbltdO1xuICAgIHByaXZhdGUgX2J1dHRvbkRyb3Bkb3duQWN0aW9uczogQnV0dG9uQWN0aW9uW107XG5cbiAgICAgY29uc3RydWN0b3IodmFsdWUgPSBcIlwiLCBjc3MgPSBcIlwiLCB0eXBlID0gQ0VMTF9UWVBFLlNUUklORywgcm91dGVMaW5rID0gXCJcIiwgYnV0dG9uQWN0aW9ucyA9IFtdLCBidXR0b25Ecm9wZG93bkFjdGlvbnMgPSBbXSl7XG4gICAgICAgIHRoaXMuX3ZhbHVlID0gdmFsdWU7XG4gICAgICAgIHRoaXMuX2NzcyA9IGNzcztcbiAgICAgICAgdGhpcy5fdHlwZSA9IHR5cGU7XG4gICAgICAgIHRoaXMuX3JvdXRlTGluayA9IHJvdXRlTGluaztcbiAgICAgICAgdGhpcy5fYnV0dG9uQWN0aW9ucyA9IGJ1dHRvbkFjdGlvbnM7XG4gICAgICAgIHRoaXMuX2J1dHRvbkRyb3Bkb3duQWN0aW9ucyA9IGJ1dHRvbkRyb3Bkb3duQWN0aW9ucztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdmFsdWVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCB2YWx1ZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl92YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGNzc1xuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGNzcygpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9jc3M7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB0eXBlXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdHlwZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl90eXBlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgcm91dGVMaW5rXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgcm91dGVMaW5rKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3JvdXRlTGluaztcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGJ1dHRvbkFjdGlvbnNcbiAgICAgKiByZXR1cm4ge0J1dHRvbkFjdGlvbltdfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGJ1dHRvbkFjdGlvbnMoKTogQnV0dG9uQWN0aW9uW10ge1xuXHRcdHJldHVybiB0aGlzLl9idXR0b25BY3Rpb25zO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgYnV0dG9uRHJvcGRvd25BY3Rpb25zXG4gICAgICogcmV0dXJuIHtCdXR0b25BY3Rpb25bXX1cbiAgICAgKi9cblx0cHVibGljIGdldCBidXR0b25Ecm9wZG93bkFjdGlvbnMoKTogQnV0dG9uQWN0aW9uW10ge1xuXHRcdHJldHVybiB0aGlzLl9idXR0b25Ecm9wZG93bkFjdGlvbnM7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB2YWx1ZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgdmFsdWUodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3ZhbHVlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBjc3NcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGNzcyh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fY3NzID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB0eXBlXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCB0eXBlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl90eXBlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciByb3V0ZUxpbmtcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHJvdXRlTGluayh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fcm91dGVMaW5rID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBidXR0b25BY3Rpb25zXG4gICAgICogcGFyYW0ge0J1dHRvbkFjdGlvbltdfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGJ1dHRvbkFjdGlvbnModmFsdWU6IEJ1dHRvbkFjdGlvbltdKSB7XG5cdFx0dGhpcy5fYnV0dG9uQWN0aW9ucyA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgYnV0dG9uRHJvcGRvd25BY3Rpb25zXG4gICAgICogcGFyYW0ge0J1dHRvbkFjdGlvbltdfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGJ1dHRvbkRyb3Bkb3duQWN0aW9ucyh2YWx1ZTogQnV0dG9uQWN0aW9uW10pIHtcblx0XHR0aGlzLl9idXR0b25Ecm9wZG93bkFjdGlvbnMgPSB2YWx1ZTtcblx0fVxuXG5cbn1cblxuZXhwb3J0IGNvbnN0IENFTExfVFlQRSA9IHtcbiAgICBTVFJJTkc6IFwiU1RSSU5HXCIsXG4gICAgREFURV9USU1FOiBcIkRBVEVfVElNRVwiLFxuICAgIFNUQVRVUzogXCJTVEFUVVNcIixcbiAgICBBQ1RJT046IFwiQUNUSU9OXCIsXG4gICAgSU1BR0VfVEhVTUJOQUlMOiBcIklNQUdFX1RIVU1CTkFJTFwiLFxuICAgIElOUFVUOiBcIklOUFVUXCJcbn07Il19