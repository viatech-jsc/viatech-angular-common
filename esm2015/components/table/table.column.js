import { SORT } from "./sort";
export class TableColumn {
    constructor(value = "", name = "", sortable = false, sort = SORT.NONE, css = "", hidden = false, filterList = [], searchable = false, searchString = "") {
        this._name = name;
        this._value = value;
        this._sort = sort;
        this._sortable = sortable;
        this._css = css;
        this._hidden = hidden;
        this._filterList = filterList;
        this._searchStr = searchString;
        this._searchable = searchable;
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter sortable
     * return {boolean}
     */
    get sortable() {
        return this._sortable;
    }
    /**
     * Getter sort
     * return {string}
     */
    get sort() {
        return this._sort;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter hidden
     * return {boolean}
     */
    get hidden() {
        return this._hidden;
    }
    /**
     * Getter filterList
     * return {TableFilterDropdownItem[]}
     */
    get filterList() {
        return this._filterList;
    }
    /**
     * Getter searchStr
     * return {string}
     */
    get searchStr() {
        return this._searchStr;
    }
    /**
     * Getter searchable
     * return {boolean}
     */
    get searchable() {
        return this._searchable;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter sortable
     * param {boolean} value
     */
    set sortable(value) {
        this._sortable = value;
    }
    /**
     * Setter sort
     * param {string} value
     */
    set sort(value) {
        this._sort = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter hidden
     * param {boolean} value
     */
    set hidden(value) {
        this._hidden = value;
    }
    /**
     * Setter filterList
     * param {TableFilterDropdownItem[]} value
     */
    set filterList(value) {
        this._filterList = value;
    }
    /**
     * Setter searchStr
     * param {string} value
     */
    set searchStr(value) {
        this._searchStr = value;
    }
    /**
     * Setter searchable
     * param {boolean} value
     */
    set searchable(value) {
        this._searchable = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY29sdW1uLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdGFibGUvdGFibGUuY29sdW1uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFHOUIsTUFBTSxPQUFPLFdBQVc7SUFXcEIsWUFBWSxLQUFLLEdBQUcsRUFBRSxFQUFFLElBQUksR0FBRyxFQUFFLEVBQUUsUUFBUSxHQUFHLEtBQUssRUFBRSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxHQUFHLEdBQUcsRUFBRSxFQUFFLE1BQU0sR0FBRyxLQUFLLEVBQUUsVUFBVSxHQUFHLEVBQUUsRUFBRSxhQUFzQixLQUFLLEVBQUUsZUFBdUIsRUFBRTtRQUNwSyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUNoQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztRQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLFlBQVksQ0FBQztRQUMvQixJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztJQUNsQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ04sSUFBVyxJQUFJO1FBQ2QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ25CLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLEtBQUs7UUFDZixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDcEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsUUFBUTtRQUNsQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDdkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsSUFBSTtRQUNkLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxHQUFHO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ2xCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLE1BQU07UUFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3JCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFVBQVU7UUFDcEIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ3pCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFNBQVM7UUFDbkIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3hCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFVBQVU7UUFDcEIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ3pCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLElBQUksQ0FBQyxLQUFhO1FBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3BCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLEtBQUssQ0FBQyxLQUFhO1FBQzdCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3JCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFFBQVEsQ0FBQyxLQUFjO1FBQ2pDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLElBQUksQ0FBQyxLQUFhO1FBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3BCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLEdBQUcsQ0FBQyxLQUFhO1FBQzNCLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO0lBQ25CLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLE1BQU0sQ0FBQyxLQUFjO1FBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFVBQVUsQ0FBQyxLQUFnQztRQUNyRCxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztJQUMxQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxTQUFTLENBQUMsS0FBYTtRQUNqQyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxVQUFVLENBQUMsS0FBYztRQUNuQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztJQUMxQixDQUFDO0NBRUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTT1JUIH0gZnJvbSBcIi4vc29ydFwiO1xuaW1wb3J0IHsgVGFibGVGaWx0ZXJEcm9wZG93bkl0ZW0gfSBmcm9tICcuL3RhYmxlLmZpbHRlci5kcm9wZG93bic7XG5cbmV4cG9ydCBjbGFzcyBUYWJsZUNvbHVtbiB7XG4gICAgcHJpdmF0ZSBfbmFtZTogc3RyaW5nO1xuICAgIHByaXZhdGUgX3ZhbHVlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfc29ydGFibGU6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBfc29ydDogc3RyaW5nO1xuICAgIHByaXZhdGUgX2Nzczogc3RyaW5nO1xuICAgIHByaXZhdGUgX2hpZGRlbjogYm9vbGVhbjtcbiAgICBwcml2YXRlIF9maWx0ZXJMaXN0OiBUYWJsZUZpbHRlckRyb3Bkb3duSXRlbVtdO1xuICAgIHByaXZhdGUgX3NlYXJjaFN0cjogc3RyaW5nO1xuICAgIHByaXZhdGUgX3NlYXJjaGFibGU6IGJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3Rvcih2YWx1ZSA9IFwiXCIsIG5hbWUgPSBcIlwiLCBzb3J0YWJsZSA9IGZhbHNlLCBzb3J0ID0gU09SVC5OT05FLCBjc3MgPSBcIlwiLCBoaWRkZW4gPSBmYWxzZSwgZmlsdGVyTGlzdCA9IFtdLCBzZWFyY2hhYmxlOiBib29sZWFuID0gZmFsc2UsIHNlYXJjaFN0cmluZzogc3RyaW5nID0gXCJcIikge1xuICAgICAgICB0aGlzLl9uYW1lID0gbmFtZTtcbiAgICAgICAgdGhpcy5fdmFsdWUgPSB2YWx1ZTtcbiAgICAgICAgdGhpcy5fc29ydCA9IHNvcnQ7XG4gICAgICAgIHRoaXMuX3NvcnRhYmxlID0gc29ydGFibGU7XG4gICAgICAgIHRoaXMuX2NzcyA9IGNzcztcbiAgICAgICAgdGhpcy5faGlkZGVuID0gaGlkZGVuO1xuICAgICAgICB0aGlzLl9maWx0ZXJMaXN0ID0gZmlsdGVyTGlzdDtcbiAgICAgICAgdGhpcy5fc2VhcmNoU3RyID0gc2VhcmNoU3RyaW5nO1xuICAgICAgICB0aGlzLl9zZWFyY2hhYmxlID0gc2VhcmNoYWJsZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgbmFtZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IG5hbWUoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fbmFtZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHZhbHVlXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdmFsdWUoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBzb3J0YWJsZVxuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cblx0cHVibGljIGdldCBzb3J0YWJsZSgpOiBib29sZWFuIHtcblx0XHRyZXR1cm4gdGhpcy5fc29ydGFibGU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBzb3J0XG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgc29ydCgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9zb3J0O1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgY3NzXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgY3NzKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX2Nzcztcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGhpZGRlblxuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cblx0cHVibGljIGdldCBoaWRkZW4oKTogYm9vbGVhbiB7XG5cdFx0cmV0dXJuIHRoaXMuX2hpZGRlbjtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGZpbHRlckxpc3RcbiAgICAgKiByZXR1cm4ge1RhYmxlRmlsdGVyRHJvcGRvd25JdGVtW119XG4gICAgICovXG5cdHB1YmxpYyBnZXQgZmlsdGVyTGlzdCgpOiBUYWJsZUZpbHRlckRyb3Bkb3duSXRlbVtdIHtcblx0XHRyZXR1cm4gdGhpcy5fZmlsdGVyTGlzdDtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHNlYXJjaFN0clxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHNlYXJjaFN0cigpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9zZWFyY2hTdHI7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBzZWFyY2hhYmxlXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHNlYXJjaGFibGUoKTogYm9vbGVhbiB7XG5cdFx0cmV0dXJuIHRoaXMuX3NlYXJjaGFibGU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBuYW1lXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBuYW1lKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9uYW1lID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB2YWx1ZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgdmFsdWUodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3ZhbHVlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBzb3J0YWJsZVxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHNvcnRhYmxlKHZhbHVlOiBib29sZWFuKSB7XG5cdFx0dGhpcy5fc29ydGFibGUgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHNvcnRcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHNvcnQodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3NvcnQgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGNzc1xuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgY3NzKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9jc3MgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGhpZGRlblxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGhpZGRlbih2YWx1ZTogYm9vbGVhbikge1xuXHRcdHRoaXMuX2hpZGRlbiA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgZmlsdGVyTGlzdFxuICAgICAqIHBhcmFtIHtUYWJsZUZpbHRlckRyb3Bkb3duSXRlbVtdfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGZpbHRlckxpc3QodmFsdWU6IFRhYmxlRmlsdGVyRHJvcGRvd25JdGVtW10pIHtcblx0XHR0aGlzLl9maWx0ZXJMaXN0ID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBzZWFyY2hTdHJcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHNlYXJjaFN0cih2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fc2VhcmNoU3RyID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBzZWFyY2hhYmxlXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgc2VhcmNoYWJsZSh2YWx1ZTogYm9vbGVhbikge1xuXHRcdHRoaXMuX3NlYXJjaGFibGUgPSB2YWx1ZTtcblx0fVxuXG59Il19