import { SORT } from "./sort";
export class TableDefaultSort {
    constructor(sortColumnName = "", sortType = SORT.ASC) {
        this._sortColumnName = sortColumnName;
        this._sortType = sortType;
    }
    /**
     * Getter sortColumnName
     * return {string}
     */
    get sortColumnName() {
        return this._sortColumnName;
    }
    /**
     * Getter sortType
     * return {string}
     */
    get sortType() {
        return this._sortType;
    }
    /**
     * Setter sortColumnName
     * param {string} value
     */
    set sortColumnName(value) {
        this._sortColumnName = value;
    }
    /**
     * Setter sortType
     * param {string} value
     */
    set sortType(value) {
        this._sortType = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuZGVmYXVsdC5zb3J0LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdGFibGUvdGFibGUuZGVmYXVsdC5zb3J0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFOUIsTUFBTSxPQUFPLGdCQUFnQjtJQUl6QixZQUFZLGlCQUF3QixFQUFFLEVBQUUsV0FBa0IsSUFBSSxDQUFDLEdBQUc7UUFDaEUsSUFBSSxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUM7UUFDdEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7SUFDNUIsQ0FBQztJQUVEOzs7T0FHRztJQUNOLElBQVcsY0FBYztRQUN4QixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDN0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsUUFBUTtRQUNsQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDdkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsY0FBYyxDQUFDLEtBQWE7UUFDdEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsUUFBUSxDQUFDLEtBQWE7UUFDaEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQztDQUVEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU09SVCB9IGZyb20gXCIuL3NvcnRcIjtcblxuZXhwb3J0IGNsYXNzIFRhYmxlRGVmYXVsdFNvcnQge1xuICAgIHByaXZhdGUgX3NvcnRDb2x1bW5OYW1lOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfc29ydFR5cGU6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKHNvcnRDb2x1bW5OYW1lOnN0cmluZyA9IFwiXCIsIHNvcnRUeXBlOnN0cmluZyA9IFNPUlQuQVNDKXtcbiAgICAgIHRoaXMuX3NvcnRDb2x1bW5OYW1lID0gc29ydENvbHVtbk5hbWU7XG4gICAgICB0aGlzLl9zb3J0VHlwZSA9IHNvcnRUeXBlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBzb3J0Q29sdW1uTmFtZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHNvcnRDb2x1bW5OYW1lKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3NvcnRDb2x1bW5OYW1lO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgc29ydFR5cGVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBzb3J0VHlwZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9zb3J0VHlwZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHNvcnRDb2x1bW5OYW1lXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBzb3J0Q29sdW1uTmFtZSh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fc29ydENvbHVtbk5hbWUgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHNvcnRUeXBlXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBzb3J0VHlwZSh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fc29ydFR5cGUgPSB2YWx1ZTtcblx0fVxuXG59Il19