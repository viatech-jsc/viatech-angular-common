export class TableFilterDropdownItem {
    constructor(title, value, checked = false) {
        this._title = title;
        this._value = value;
        this._checked = checked;
    }
    /**
     * Getter title
     * return {string}
     */
    get title() {
        return this._title;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter checked
     * return {boolean}
     */
    get checked() {
        return this._checked;
    }
    /**
     * Setter title
     * param {string} value
     */
    set title(value) {
        this._title = value;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter checked
     * param {boolean} value
     */
    set checked(value) {
        this._checked = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuZmlsdGVyLmRyb3Bkb3duLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdGFibGUvdGFibGUuZmlsdGVyLmRyb3Bkb3duLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sT0FBTyx1QkFBdUI7SUFLbkMsWUFBWSxLQUFhLEVBQUUsS0FBYSxFQUFFLFVBQW1CLEtBQUs7UUFDakUsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDZCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztJQUMvQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxLQUFLO1FBQ2YsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3BCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLEtBQUs7UUFDZixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDcEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsT0FBTztRQUNqQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsS0FBSyxDQUFDLEtBQWE7UUFDN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsS0FBSyxDQUFDLEtBQWE7UUFDN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsT0FBTyxDQUFDLEtBQWM7UUFDaEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQztDQUVEIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFRhYmxlRmlsdGVyRHJvcGRvd25JdGVtIHtcbiAgICBwcml2YXRlIF90aXRsZTogc3RyaW5nO1xuICAgIHByaXZhdGUgX3ZhbHVlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfY2hlY2tlZDogYm9vbGVhbjtcblxuXHRjb25zdHJ1Y3Rvcih0aXRsZTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nLCBjaGVja2VkOiBib29sZWFuID0gZmFsc2UpIHtcblx0XHR0aGlzLl90aXRsZSA9IHRpdGxlO1xuICAgICAgICB0aGlzLl92YWx1ZSA9IHZhbHVlO1xuICAgICAgICB0aGlzLl9jaGVja2VkID0gY2hlY2tlZDtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHRpdGxlXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdGl0bGUoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fdGl0bGU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB2YWx1ZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHZhbHVlKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3ZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgY2hlY2tlZFxuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cblx0cHVibGljIGdldCBjaGVja2VkKCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiB0aGlzLl9jaGVja2VkO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdGl0bGVcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHRpdGxlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl90aXRsZSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdmFsdWVcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHZhbHVlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl92YWx1ZSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgY2hlY2tlZFxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGNoZWNrZWQodmFsdWU6IGJvb2xlYW4pIHtcblx0XHR0aGlzLl9jaGVja2VkID0gdmFsdWU7XG5cdH1cblxufSJdfQ==