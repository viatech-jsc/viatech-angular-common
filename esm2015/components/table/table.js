import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Router } from "@angular/router";
import { DATE_FORMAT } from '../../config';
import { SORT } from "./sort";
import { CELL_TYPE } from "./table.cell";
import { TABLE_LANGUAGE_CONFIG } from './table.config';
import { EntryPerpage, TableOption } from "./table.option";
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
import * as i4 from "@ng-bootstrap/ng-bootstrap";
import * as i5 from "./filter";
function TableComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r11 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 19);
    i0.ɵɵelementStart(1, "ul", 20);
    i0.ɵɵelementStart(2, "li", 21);
    i0.ɵɵelementStart(3, "span", 22);
    i0.ɵɵelementStart(4, "img", 23);
    i0.ɵɵlistener("click", function TableComponent_div_5_Template_img_click_4_listener() { i0.ɵɵrestoreView(_r11); const ctx_r10 = i0.ɵɵnextContext(); return ctx_r10.toggleFilter(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "span", 24);
    i0.ɵɵlistener("click", function TableComponent_div_5_Template_span_click_5_listener() { i0.ɵɵrestoreView(_r11); const ctx_r12 = i0.ɵɵnextContext(); return ctx_r12.toggleFilter(); });
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "div", 25);
    i0.ɵɵlistener("click", function TableComponent_div_5_Template_div_click_7_listener() { i0.ɵɵrestoreView(_r11); const ctx_r13 = i0.ɵɵnextContext(); return ctx_r13.hideFilter(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(8, "ul", 26);
    i0.ɵɵelementStart(9, "li");
    i0.ɵɵprojection(10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngClass", ctx_r0.isShowFilter ? "open" : "");
    i0.ɵɵadvance(4);
    i0.ɵɵtextInterpolate(ctx_r0.TablelanguageObj.filter_by);
} }
function TableComponent_div_7_span_1_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r19 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 28);
    i0.ɵɵlistener("click", function TableComponent_div_7_span_1_button_1_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r19); const btn_r15 = i0.ɵɵnextContext().$implicit; const ctx_r17 = i0.ɵɵnextContext(2); return ctx_r17.doAction(btn_r15); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const btn_r15 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("ngClass", btn_r15.css);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(btn_r15.name);
} }
function TableComponent_div_7_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtemplate(1, TableComponent_div_7_span_1_button_1_Template, 2, 2, "button", 27);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const btn_r15 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !btn_r15.hided);
} }
function TableComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtemplate(1, TableComponent_div_7_span_1_Template, 2, 1, "span", 16);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r1.tableOption.tableActions);
} }
function TableComponent_option_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "option", 29);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const entry_r21 = ctx.$implicit;
    i0.ɵɵproperty("selected", entry_r21.selected);
    i0.ɵɵattribute("value", entry_r21.value);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(entry_r21.name);
} }
function TableComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    const _r23 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 30);
    i0.ɵɵelementStart(1, "span", 31);
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "ngb-pagination", 32);
    i0.ɵɵlistener("pageChange", function TableComponent_div_16_Template_ngb_pagination_pageChange_3_listener($event) { i0.ɵɵrestoreView(_r23); const ctx_r22 = i0.ɵɵnextContext(); return ctx_r22.tableOption.currentPage = $event; })("pageChange", function TableComponent_div_16_Template_ngb_pagination_pageChange_3_listener() { i0.ɵɵrestoreView(_r23); const ctx_r24 = i0.ɵɵnextContext(); return ctx_r24.doPageChange(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1("", ctx_r4.TablelanguageObj.table_page, " ");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("directionLinks", false)("collectionSize", ctx_r4.tableOption.totalItems)("rotate", true)("maxSize", 5)("pageSize", ctx_r4.tableOption.itemsPerPage)("page", ctx_r4.tableOption.currentPage);
} }
function TableComponent_thead_18_tr_1_th_1_i_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i", 40);
} }
function TableComponent_thead_18_tr_1_th_1_i_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i", 41);
} }
const _c0 = function (a0, a1, a2) { return [a0, a1, a2]; };
function TableComponent_thead_18_tr_1_th_1_Template(rf, ctx) { if (rf & 1) {
    const _r33 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "th", 36);
    i0.ɵɵlistener("click", function TableComponent_thead_18_tr_1_th_1_Template_th_click_0_listener() { i0.ɵɵrestoreView(_r33); const column_r29 = ctx.$implicit; const ctx_r32 = i0.ɵɵnextContext(3); return ctx_r32.doSort(column_r29); });
    i0.ɵɵelementStart(1, "div", 37);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, TableComponent_thead_18_tr_1_th_1_i_4_Template, 1, 0, "i", 38);
    i0.ɵɵtemplate(5, TableComponent_thead_18_tr_1_th_1_i_5_Template, 1, 0, "i", 39);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r29 = ctx.$implicit;
    const ctx_r27 = i0.ɵɵnextContext(3);
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction3(4, _c0, column_r29.sortable ? "sortable" : "", column_r29.sort, column_r29.css));
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(column_r29.value);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r27.isSortAsc(column_r29));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r27.isSortDesc(column_r29));
} }
function TableComponent_thead_18_tr_1_th_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "th");
} }
function TableComponent_thead_18_tr_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tr", 34);
    i0.ɵɵtemplate(1, TableComponent_thead_18_tr_1_th_1_Template, 6, 8, "th", 35);
    i0.ɵɵpipe(2, "filterColumn");
    i0.ɵɵtemplate(3, TableComponent_thead_18_tr_1_th_3_Template, 1, 0, "th", 7);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r25 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", i0.ɵɵpipeBind1(2, 2, ctx_r25.tableOption.tableColumns));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r25.rowSortable);
} }
function TableComponent_thead_18_tr_2_th_1_i_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i", 40);
} }
function TableComponent_thead_18_tr_2_th_1_i_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i", 41);
} }
function TableComponent_thead_18_tr_2_th_1_div_6_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r45 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 51);
    i0.ɵɵelementStart(1, "input", 52);
    i0.ɵɵlistener("ngModelChange", function TableComponent_thead_18_tr_2_th_1_div_6_div_3_Template_input_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r45); const header_r42 = ctx.$implicit; return header_r42.checked = $event; });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(2, "label", 53);
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const header_r42 = ctx.$implicit;
    const i_r43 = ctx.index;
    const columnIndex_r36 = i0.ɵɵnextContext(2).index;
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate2("id", "styled-checkbox-", i_r43, "-", columnIndex_r36, "");
    i0.ɵɵpropertyInterpolate("value", header_r42.value);
    i0.ɵɵproperty("ngModel", header_r42.checked);
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate2("for", "styled-checkbox-", i_r43, "-", columnIndex_r36, "");
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(header_r42.title);
} }
function TableComponent_thead_18_tr_2_th_1_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r49 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 45);
    i0.ɵɵelement(1, "img", 46);
    i0.ɵɵelementStart(2, "div", 47);
    i0.ɵɵtemplate(3, TableComponent_thead_18_tr_2_th_1_div_6_div_3_Template, 4, 7, "div", 48);
    i0.ɵɵelementStart(4, "div", 49);
    i0.ɵɵelementStart(5, "button", 50);
    i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_6_Template_button_click_5_listener() { i0.ɵɵrestoreView(_r49); const column_r35 = i0.ɵɵnextContext().$implicit; const ctx_r47 = i0.ɵɵnextContext(3); return ctx_r47.filterOnTableColumn(column_r35.filterList, column_r35.name); });
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "button", 50);
    i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_6_Template_button_click_7_listener() { i0.ɵɵrestoreView(_r49); const column_r35 = i0.ɵɵnextContext().$implicit; const ctx_r50 = i0.ɵɵnextContext(3); return ctx_r50.filterOnTableColumnReset(column_r35.filterList); });
    i0.ɵɵtext(8);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r35 = i0.ɵɵnextContext().$implicit;
    const ctx_r39 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngForOf", column_r35.filterList);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r39.TablelanguageObj.rts_dropdown_filter);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r39.TablelanguageObj.rts_reset);
} }
function TableComponent_thead_18_tr_2_th_1_div_7_Template(rf, ctx) { if (rf & 1) {
    const _r55 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 45);
    i0.ɵɵelement(1, "img", 54);
    i0.ɵɵelementStart(2, "div", 55);
    i0.ɵɵelementStart(3, "input", 56);
    i0.ɵɵlistener("ngModelChange", function TableComponent_thead_18_tr_2_th_1_div_7_Template_input_ngModelChange_3_listener($event) { i0.ɵɵrestoreView(_r55); const column_r35 = i0.ɵɵnextContext().$implicit; return column_r35.searchStr = $event; });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 49);
    i0.ɵɵelementStart(5, "button", 50);
    i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_7_Template_button_click_5_listener() { i0.ɵɵrestoreView(_r55); const column_r35 = i0.ɵɵnextContext().$implicit; const ctx_r56 = i0.ɵɵnextContext(3); return ctx_r56.tableDropdownSearch(column_r35.searchStr, column_r35.name); });
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "button", 50);
    i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_7_Template_button_click_7_listener() { i0.ɵɵrestoreView(_r55); const column_r35 = i0.ɵɵnextContext().$implicit; const ctx_r58 = i0.ɵɵnextContext(3); return ctx_r58.tableDropdownSearchReset(column_r35.searchStr); });
    i0.ɵɵtext(8);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r35 = i0.ɵɵnextContext().$implicit;
    const ctx_r40 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngModel", column_r35.searchStr);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r40.TablelanguageObj.rts_dropdown_search);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r40.TablelanguageObj.rts_reset);
} }
function TableComponent_thead_18_tr_2_th_1_Template(rf, ctx) { if (rf & 1) {
    const _r62 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "th", 43);
    i0.ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_Template_th_click_0_listener() { i0.ɵɵrestoreView(_r62); const column_r35 = ctx.$implicit; const ctx_r61 = i0.ɵɵnextContext(3); return ctx_r61.doSort(column_r35); });
    i0.ɵɵelementStart(1, "div", 37);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, TableComponent_thead_18_tr_2_th_1_i_4_Template, 1, 0, "i", 38);
    i0.ɵɵtemplate(5, TableComponent_thead_18_tr_2_th_1_i_5_Template, 1, 0, "i", 39);
    i0.ɵɵtemplate(6, TableComponent_thead_18_tr_2_th_1_div_6_Template, 9, 3, "div", 44);
    i0.ɵɵtemplate(7, TableComponent_thead_18_tr_2_th_1_div_7_Template, 9, 3, "div", 44);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const column_r35 = ctx.$implicit;
    const ctx_r34 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(column_r35.value);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r34.isSortAsc(column_r35));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r34.isSortDesc(column_r35));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", column_r35.filterList.length);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", column_r35.searchable);
} }
function TableComponent_thead_18_tr_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tr", 34);
    i0.ɵɵtemplate(1, TableComponent_thead_18_tr_2_th_1_Template, 8, 5, "th", 42);
    i0.ɵɵpipe(2, "filterColumn");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r26 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", i0.ɵɵpipeBind1(2, 1, ctx_r26.tableOption.tableColumns));
} }
function TableComponent_thead_18_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "thead");
    i0.ɵɵtemplate(1, TableComponent_thead_18_tr_1_Template, 4, 4, "tr", 33);
    i0.ɵɵtemplate(2, TableComponent_thead_18_tr_2_Template, 3, 3, "tr", 33);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r5.tableType === "WIN");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r5.tableType === "RTS");
} }
function TableComponent_ng_container_20_td_2_span_1_Template(rf, ctx) { if (rf & 1) {
    const _r77 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span", 68);
    i0.ɵɵelementStart(1, "img", 69);
    i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_1_Template_img_click_1_listener() { i0.ɵɵrestoreView(_r77); const row_r63 = i0.ɵɵnextContext(2).$implicit; const ctx_r75 = i0.ɵɵnextContext(); return ctx_r75.doOpenPreview(row_r63.tableCells[1].value); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = i0.ɵɵnextContext().$implicit;
    const ctx_r69 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("src", ctx_r69.downloadFilePath + cell_r68.value + "_thumb", i0.ɵɵsanitizeUrl);
} }
function TableComponent_ng_container_20_td_2_span_2_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r83 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 74);
    i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_2_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r83); const cell_r68 = i0.ɵɵnextContext(2).$implicit; const i_r64 = i0.ɵɵnextContext().index; const ctx_r81 = i0.ɵɵnextContext(); cell_r68.enabled = true; return ctx_r81.focusInput("input-" + i_r64); });
    i0.ɵɵelement(1, "i", 75);
    i0.ɵɵelementEnd();
} }
function TableComponent_ng_container_20_td_2_span_2_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 76);
    i0.ɵɵtext(1, "maxlength ");
    i0.ɵɵelementEnd();
} }
function TableComponent_ng_container_20_td_2_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r87 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span", 70);
    i0.ɵɵtext(1);
    i0.ɵɵelementStart(2, "input", 71);
    i0.ɵɵlistener("ngModelChange", function TableComponent_ng_container_20_td_2_span_2_Template_input_ngModelChange_2_listener($event) { i0.ɵɵrestoreView(_r87); const cell_r68 = i0.ɵɵnextContext().$implicit; return cell_r68.value = $event; })("focusin", function TableComponent_ng_container_20_td_2_span_2_Template_input_focusin_2_listener() { i0.ɵɵrestoreView(_r87); const cell_r68 = i0.ɵɵnextContext().$implicit; return cell_r68.oldValue = cell_r68.value; })("focusout", function TableComponent_ng_container_20_td_2_span_2_Template_input_focusout_2_listener() { i0.ɵɵrestoreView(_r87); const cell_r68 = i0.ɵɵnextContext().$implicit; const row_r63 = i0.ɵɵnextContext().$implicit; const ctx_r90 = i0.ɵɵnextContext(); ctx_r90.doUpdateInputEvent(row_r63, cell_r68.enabled, cell_r68.value, cell_r68.oldValue); return cell_r68.enabled = false; })("keyup.enter", function TableComponent_ng_container_20_td_2_span_2_Template_input_keyup_enter_2_listener() { i0.ɵɵrestoreView(_r87); const cell_r68 = i0.ɵɵnextContext().$implicit; const row_r63 = i0.ɵɵnextContext().$implicit; const ctx_r93 = i0.ɵɵnextContext(); cell_r68.enabled = false; return ctx_r93.doUpdateInputEvent(row_r63, true, cell_r68, cell_r68.oldValue); });
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_2_button_3_Template, 2, 0, "button", 72);
    i0.ɵɵtemplate(4, TableComponent_ng_container_20_td_2_span_2_div_4_Template, 2, 0, "div", 73);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = i0.ɵɵnextContext().$implicit;
    const i_r64 = i0.ɵɵnextContext().index;
    const ctx_r70 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", cell_r68.originalValue, " ");
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate("id", "input-" + i_r64);
    i0.ɵɵproperty("ngModel", cell_r68.value)("disabled", !cell_r68.enabled || !ctx_r70.rowSortable);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !cell_r68.enabled && ctx_r70.rowSortable);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", cell_r68.value.length > ctx_r70.inputMaxLenght);
} }
function TableComponent_ng_container_20_td_2_span_3_Template(rf, ctx) { if (rf & 1) {
    const _r100 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span", 77);
    i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_3_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r100); const cell_r68 = i0.ɵɵnextContext().$implicit; const ctx_r98 = i0.ɵɵnextContext(2); return ctx_r98.cellClicked(cell_r68); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵpropertyInterpolate("title", cell_r68.value);
    i0.ɵɵproperty("ngClass", cell_r68.routeLink.trim().length > 0 ? "hand" : "");
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(cell_r68.value);
} }
function TableComponent_ng_container_20_td_2_span_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 78);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵpropertyInterpolate("title", cell_r68.value);
    i0.ɵɵproperty("ngClass", cell_r68.value);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(cell_r68.value);
} }
function TableComponent_ng_container_20_td_2_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 79);
    i0.ɵɵpipe(1, "date");
    i0.ɵɵtext(2);
    i0.ɵɵpipe(3, "date");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = i0.ɵɵnextContext().$implicit;
    const ctx_r73 = i0.ɵɵnextContext(2);
    i0.ɵɵpropertyInterpolate("title", i0.ɵɵpipeBind2(1, 2, cell_r68.value, ctx_r73.dateFormat));
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(i0.ɵɵpipeBind2(3, 5, cell_r68.value, ctx_r73.dateFormat));
} }
function TableComponent_ng_container_20_td_2_span_6_span_1_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r112 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 84);
    i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_span_1_button_2_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r112); const buttonAction_r107 = i0.ɵɵnextContext().$implicit; const row_r63 = i0.ɵɵnextContext(3).$implicit; return buttonAction_r107.buttonAction(row_r63); });
    i0.ɵɵelement(1, "img", 85);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r107 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵpropertyInterpolate("title", buttonAction_r107.name);
    i0.ɵɵproperty("disabled", buttonAction_r107.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("src", buttonAction_r107.iconUrl, i0.ɵɵsanitizeUrl);
} }
function TableComponent_ng_container_20_td_2_span_6_span_1_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r117 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 86);
    i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_span_1_button_3_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r117); const buttonAction_r107 = i0.ɵɵnextContext().$implicit; const row_r63 = i0.ɵɵnextContext(3).$implicit; return buttonAction_r107.buttonAction(row_r63); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r107 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("disabled", buttonAction_r107.disabled)("ngClass", "btn-" + buttonAction_r107.name);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(buttonAction_r107.name);
} }
function TableComponent_ng_container_20_td_2_span_6_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "span");
    i0.ɵɵtemplate(2, TableComponent_ng_container_20_td_2_span_6_span_1_button_2_Template, 2, 3, "button", 82);
    i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_6_span_1_button_3_Template, 2, 3, "button", 83);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r107 = ctx.$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", buttonAction_r107.iconUrl);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !buttonAction_r107.iconUrl);
} }
function TableComponent_ng_container_20_td_2_span_6_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r122 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 87);
    i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_button_2_Template_button_click_0_listener($event) { i0.ɵɵrestoreView(_r122); const i_r64 = i0.ɵɵnextContext(3).index; const ctx_r120 = i0.ɵɵnextContext(); return ctx_r120.expand("table-row-extra" + i_r64, $event); });
    i0.ɵɵelement(1, "i", 88);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const i_r64 = i0.ɵɵnextContext(3).index;
    const ctx_r105 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngClass", "table-row-extra" + i_r64 == ctx_r105.expandedRowId ? "arrow-rotate" : "");
} }
function TableComponent_ng_container_20_td_2_span_6_div_3_span_5_Template(rf, ctx) { if (rf & 1) {
    const _r128 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "button", 94);
    i0.ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_div_3_span_5_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r128); const buttonAction_r125 = ctx.$implicit; const row_r63 = i0.ɵɵnextContext(4).$implicit; return buttonAction_r125.disabled != true ? buttonAction_r125.buttonAction(row_r63) : ""; });
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r125 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("disabled", buttonAction_r125.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", buttonAction_r125.name, "");
} }
function TableComponent_ng_container_20_td_2_span_6_div_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 89);
    i0.ɵɵelementStart(1, "div", 90);
    i0.ɵɵelementStart(2, "button", 91);
    i0.ɵɵelement(3, "i", 92);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 93);
    i0.ɵɵtemplate(5, TableComponent_ng_container_20_td_2_span_6_div_3_span_5_Template, 3, 2, "span", 16);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = i0.ɵɵnextContext(2).$implicit;
    i0.ɵɵadvance(5);
    i0.ɵɵproperty("ngForOf", cell_r68.buttonDropdownActions);
} }
function TableComponent_ng_container_20_td_2_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtemplate(1, TableComponent_ng_container_20_td_2_span_6_span_1_Template, 4, 2, "span", 16);
    i0.ɵɵtemplate(2, TableComponent_ng_container_20_td_2_span_6_button_2_Template, 2, 1, "button", 80);
    i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_6_div_3_Template, 6, 1, "div", 81);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = i0.ɵɵnextContext().$implicit;
    const row_r63 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", cell_r68.buttonActions);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", row_r63.expandedText);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", cell_r68.buttonDropdownActions.length);
} }
function TableComponent_ng_container_20_td_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 61);
    i0.ɵɵtemplate(1, TableComponent_ng_container_20_td_2_span_1_Template, 2, 1, "span", 62);
    i0.ɵɵtemplate(2, TableComponent_ng_container_20_td_2_span_2_Template, 5, 6, "span", 63);
    i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_3_Template, 2, 3, "span", 64);
    i0.ɵɵtemplate(4, TableComponent_ng_container_20_td_2_span_4_Template, 2, 3, "span", 65);
    i0.ɵɵtemplate(5, TableComponent_ng_container_20_td_2_span_5_Template, 4, 8, "span", 66);
    i0.ɵɵtemplate(6, TableComponent_ng_container_20_td_2_span_6_Template, 4, 3, "span", 67);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = ctx.$implicit;
    const ctx_r65 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("ngClass", cell_r68.type + " " + cell_r68.css)("ngSwitch", cell_r68.type);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.IMAGE_THUMBNAIL);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.INPUT);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.STATUS);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.DATE_TIME);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngSwitchCase", ctx_r65.cellType.ACTION);
} }
function TableComponent_ng_container_20_td_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 95);
    i0.ɵɵelement(1, "a", 96);
    i0.ɵɵelementEnd();
} }
function TableComponent_ng_container_20_tr_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tr", 97);
    i0.ɵɵelementStart(1, "td");
    i0.ɵɵelementStart(2, "div", 98);
    i0.ɵɵelement(3, "div", 99);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r132 = i0.ɵɵnextContext();
    const row_r63 = ctx_r132.$implicit;
    const i_r64 = ctx_r132.index;
    i0.ɵɵadvance(1);
    i0.ɵɵattribute("colspan", row_r63.tableCells.length);
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate("id", "table-row-extra" + i_r64);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("innerHTML", row_r63.expandedText, i0.ɵɵsanitizeHtml);
} }
function TableComponent_ng_container_20_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵelementStart(1, "tr", 57);
    i0.ɵɵtemplate(2, TableComponent_ng_container_20_td_2_Template, 7, 7, "td", 58);
    i0.ɵɵtemplate(3, TableComponent_ng_container_20_td_3_Template, 2, 0, "td", 59);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, TableComponent_ng_container_20_tr_4_Template, 4, 3, "tr", 60);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const row_r63 = ctx.$implicit;
    const i_r64 = ctx.index;
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate("id", "table-row-" + i_r64);
    i0.ɵɵproperty("ngClass", ctx_r6.rowSortable ? row_r63.css + "sortable-row" : row_r63.css);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", row_r63.tableCells);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r6.rowSortable);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r6.rowSortable);
} }
function TableComponent_tfoot_21_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "tfoot");
    i0.ɵɵelementStart(1, "tr");
    i0.ɵɵelementStart(2, "td");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵattribute("colspan", ctx_r7.tableOption.tableColumns.length);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r7.TablelanguageObj.no_data_found);
} }
function TableComponent_div_22_div_2_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "img", 103);
} }
function TableComponent_div_22_div_2_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "img", 104);
} }
function TableComponent_div_22_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r137 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 30);
    i0.ɵɵelementStart(1, "ngb-pagination", 100);
    i0.ɵɵlistener("pageChange", function TableComponent_div_22_div_2_Template_ngb_pagination_pageChange_1_listener($event) { i0.ɵɵrestoreView(_r137); const ctx_r136 = i0.ɵɵnextContext(2); return ctx_r136.tableOption.currentPage = $event; })("pageChange", function TableComponent_div_22_div_2_Template_ngb_pagination_pageChange_1_listener() { i0.ɵɵrestoreView(_r137); const ctx_r138 = i0.ɵɵnextContext(2); return ctx_r138.doPageChange(); });
    i0.ɵɵtemplate(2, TableComponent_div_22_div_2_ng_template_2_Template, 1, 0, "ng-template", 101);
    i0.ɵɵtemplate(3, TableComponent_div_22_div_2_ng_template_3_Template, 1, 0, "ng-template", 102);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r133 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("collectionSize", ctx_r133.tableOption.totalItems)("rotate", true)("maxSize", 5)("pageSize", ctx_r133.tableOption.itemsPerPage)("page", ctx_r133.tableOption.currentPage);
} }
function TableComponent_div_22_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 8);
    i0.ɵɵelementStart(1, "div", 9);
    i0.ɵɵtemplate(2, TableComponent_div_22_div_2_Template, 4, 5, "div", 14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r8.noDataFound());
} }
function TableComponent_div_23_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 105);
    i0.ɵɵtext(1);
    i0.ɵɵelementStart(2, "span", 106);
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵtext(4);
    i0.ɵɵelementStart(5, "span", 106);
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵtext(7);
    i0.ɵɵelementStart(8, "span", 107);
    i0.ɵɵtext(9);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r9 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r9.TablelanguageObj.table_footer_showing, " ");
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r9.startItemNumber());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r9.TablelanguageObj.table_to, " ");
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r9.endItemNumber());
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r9.TablelanguageObj.table_of, " ");
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate2("", ctx_r9.tableOption.totalItems, " ", ctx_r9.TablelanguageObj.table_elements, "");
} }
const _c1 = [[["", "filter", ""]]];
const _c2 = ["[filter]"];
export class TableComponent {
    constructor(router) {
        this.router = router;
        this.rowSortable = false;
        this.canShowActionButtons = true;
        this.downloadFilePath = '';
        this.actionSort = new EventEmitter();
        this.pageChange = new EventEmitter();
        this.entryPageChange = new EventEmitter();
        this.actionButton = new EventEmitter();
        //RTS
        this.dropdownSearch = new EventEmitter();
        this.dropdownFilter = new EventEmitter();
        // gallery
        this.openGalleryImagePreview = new EventEmitter();
        this.doGalleryUpdateIndexCallback = new EventEmitter();
        this.doGalleryUpdateInput = new EventEmitter();
        this.cellType = CELL_TYPE;
        this.dateFormat = DATE_FORMAT;
        this.entryPerpages = new Array();
        this.isShowFilter = false;
        this.isShowActionHeaderBtn = true;
        this.inputMaxLenght = 255;
        this.TablelanguageObj = TABLE_LANGUAGE_CONFIG;
    }
    ngOnInit() {
        this.tableType = this.type ? this.type : "RTS";
        $(document).on('click', '.dropdown-menu', function (e) {
            e.stopPropagation();
        });
        this.entryPerpages.push(new EntryPerpage("5", 5));
        this.entryPerpages.push(new EntryPerpage("10", 10));
        this.entryPerpages.push(new EntryPerpage("20", 20));
        for (let i = 0; i < this.entryPerpages.length; i++) {
            let entry = this.entryPerpages[i];
            entry.selected = false;
            if (entry.value === this.tableOption.itemsPerPage) {
                entry.selected = true;
            }
        }
        if (this.rowSortable) {
            let self = this;
            setTimeout(() => {
                $('#sortable').sortable({
                    placeholder: "sortable-placeholder",
                    helper: 'clone',
                    axis: "y",
                    cursor: "move",
                    deactivate: function (event, ui) {
                        let dragPostionObj = { fromIndex: ui.item[0].cells[1].innerText, toIndex: self.getSibling(ui.item[0]) };
                        self.doUpdateIndex(dragPostionObj);
                    }
                });
                $("#sortable").disableSelection();
            }, 0);
        }
    }
    getSibling(ui) {
        let text = "";
        if (ui.nextElementSibling && ui.nextElementSibling.cells) {
            text = ui.nextElementSibling.cells[1].innerText;
        }
        else if (ui.previousElementSibling && ui.previousElementSibling.cells) {
            text = ui.previousElementSibling.cells[1].innerText;
        }
        return text;
    }
    startItemNumber() {
        return (this.tableOption.currentPage * this.tableOption.itemsPerPage) - this.tableOption.itemsPerPage + 1;
    }
    endItemNumber() {
        return (this.tableOption.currentPage * this.tableOption.itemsPerPage) < this.tableOption.totalItems ? (this.tableOption.currentPage * this.tableOption.itemsPerPage) : this.tableOption.totalItems;
    }
    noDataFound() {
        return this.tableOption.tableRows.length === 0 && this.tableOption.currentPage <= 1;
    }
    hideFilter() {
        this.isShowFilter = false;
    }
    cellClicked(cell) {
        if (cell.routeLink.trim().length > 0) {
            this.router.navigate([cell.routeLink]);
        }
    }
    showFilter() {
        this.isShowFilter = true;
    }
    toggleFilter() {
        this.isShowFilter = !this.isShowFilter;
    }
    doAction(actionBtn) {
        this.actionButton.emit(actionBtn);
    }
    doPageChange() {
        let currentOffset = (this.tableOption.currentPage - 1) * this.tableOption.itemsPerPage;
        this.pageChange.emit(currentOffset);
    }
    doEntryPage(value) {
        this.tableOption.itemsPerPage = value;
        this.tableOption.currentPage = 1;
        this.entryPageChange.emit(value);
    }
    doSort(column) {
        if (!column.sortable) {
            return;
        }
        let columns = this.tableOption.tableColumns;
        this.currentSortColumnName = undefined;
        this.currentSortType = undefined;
        for (let i = 0; i < this.tableOption.tableColumns.length; i++) {
            let col = this.tableOption.tableColumns[i];
            if (col.name === column.name) {
                if (col.sort === SORT.ASC) {
                    col.sort = SORT.DESC;
                }
                else if (col.sort === SORT.DESC) {
                    col.sort = SORT.NONE;
                    col.sort = this.tableOption.defaultSort.sortColumnName === col.name ? SORT.ASC : SORT.NONE;
                    this.currentSortColumnName = this.tableOption.defaultSort.sortColumnName;
                    this.currentSortType = this.tableOption.defaultSort.sortType;
                }
                else if (col.sort === SORT.NONE) {
                    col.sort = SORT.ASC;
                }
            }
            else {
                col.sort = SORT.NONE;
            }
        }
        this.actionSort.emit(column);
    }
    isSortAsc(column) {
        return column.sort === SORT.ASC || (this.currentSortColumnName && this.currentSortColumnName === column.name && this.currentSortType === SORT.ASC);
    }
    isSortDesc(column) {
        return column.sort === SORT.DESC || (this.currentSortColumnName && this.currentSortColumnName === column.name && this.currentSortType === SORT.DESC);
    }
    //RTS
    //dropdown filter
    filterOnTableColumn(data, name) {
        this.dropdownFilter.emit({ filterData: data, columnName: name });
    }
    filterOnTableColumnReset(data) {
        data.forEach(element => {
            element.checked = false;
        });
    }
    //dropdown search
    tableDropdownSearch(searchString, name) {
        this.dropdownSearch.emit({ searchStr: searchString, columnName: name });
    }
    tableDropdownSearchReset(searchString) {
        searchString = "";
    }
    // gallery
    doOpenPreview(imgCode) {
        this.openGalleryImagePreview.emit(imgCode);
    }
    doUpdateIndex(dragPostionObj) {
        this.doGalleryUpdateIndexCallback.emit(dragPostionObj);
    }
    doUpdateInputEvent(row, canEdit, newValue, originalValue) {
        if (canEdit && newValue.value !== originalValue) {
            if (newValue.value.length <= this.inputMaxLenght) {
                this.doGalleryUpdateInput.emit(row);
            }
            else {
                newValue.value = originalValue;
            }
        }
    }
    focusInput(id) {
        setTimeout(() => {
            document.getElementById(id).focus();
        }, 0);
    }
    expand(id, event) {
        if (this.expandedRowId === id) {
            $("#" + this.expandedRowId).slideUp("fast");
            this.expandedRowId = undefined;
        }
        else {
            if (this.expandedRowId && this.expandedRowId !== id) {
                $("#" + this.expandedRowId).slideUp("fast");
            }
            $("#" + id).slideToggle("fast");
            this.expandedRowId = id;
        }
        event.preventDefault();
    }
}
TableComponent.ɵfac = function TableComponent_Factory(t) { return new (t || TableComponent)(i0.ɵɵdirectiveInject(i1.Router)); };
TableComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TableComponent, selectors: [["via-table"]], inputs: { type: "type", tableOption: "tableOption", rowSortable: "rowSortable", canShowActionButtons: "canShowActionButtons", downloadFilePath: "downloadFilePath", TablelanguageObj: "TablelanguageObj" }, outputs: { actionSort: "actionSort", pageChange: "pageChange", entryPageChange: "entryPageChange", actionButton: "actionButton", dropdownSearch: "dropdownSearch", dropdownFilter: "dropdownFilter", openGalleryImagePreview: "openGalleryImagePreview", doGalleryUpdateIndexCallback: "doGalleryUpdateIndexCallback", doGalleryUpdateInput: "doGalleryUpdateInput" }, ngContentSelectors: _c2, decls: 24, vars: 13, consts: [["id", "win-table"], [1, "win-table-wrapper"], [1, "table-top"], [1, "action-wrapper"], [1, "action-container"], ["class", "filter", 4, "ngIf"], [1, "action-button"], [4, "ngIf"], [1, "paging"], [1, "paging-container"], [1, "number-per-page"], [3, "change"], ["entry", ""], [3, "selected", 4, "ngFor", "ngForOf"], ["class", "paging-wrapper", 4, "ngIf"], ["id", "sortable"], [4, "ngFor", "ngForOf"], ["class", "paging", 4, "ngIf"], ["class", "win-table-footer", 4, "ngIf"], [1, "filter"], [1, "filter-container"], [1, "dropdown", 3, "ngClass"], [1, "dropdown-content"], ["src", "./assets/icons/ic_filter.svg", 3, "click"], [1, "filter-text", 3, "click"], [1, "filter-mask", 3, "click"], [1, "dropdown-menu-items", "z-depth-1"], ["type", "button", "class", "btn uppercase-text m-l-10", 3, "ngClass", "click", 4, "ngIf"], ["type", "button", 1, "btn", "uppercase-text", "m-l-10", 3, "ngClass", "click"], [3, "selected"], [1, "paging-wrapper"], [1, "paging-title"], ["size", "sm", 3, "directionLinks", "collectionSize", "rotate", "maxSize", "pageSize", "page", "pageChange"], ["class", "heading", 4, "ngIf"], [1, "heading"], ["class", "handle", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [1, "handle", 3, "ngClass", "click"], [1, "header-container"], ["class", "fa fa-sort-amount-asc", "aria-hidden", "true", 4, "ngIf"], ["class", "fa fa-sort-amount-desc", "aria-hidden", "true", 4, "ngIf"], ["aria-hidden", "true", 1, "fa", "fa-sort-amount-asc"], ["aria-hidden", "true", 1, "fa", "fa-sort-amount-desc"], ["style", "border-radius: 50px;", "class", "handle", 3, "click", 4, "ngFor", "ngForOf"], [1, "handle", 2, "border-radius", "50px", 3, "click"], ["ngbDropdown", "", "display", "dynamic", 4, "ngIf"], ["ngbDropdown", "", "display", "dynamic"], ["ngbDropdownToggle", "", "src", "./assets/icons/filter.svg", 1, "filter-icon"], ["ngbDropdownMenu", "", 1, "dropdown-menu", "dropdown-menu-right"], ["class", "table-dropdown-item", 4, "ngFor", "ngForOf"], [1, "table-dropdown-btm-wrapper"], [1, "table-dropdown-btn", 3, "click"], [1, "table-dropdown-item"], ["type", "checkbox", 1, "styled-checkbox", 3, "id", "value", "ngModel", "ngModelChange"], [3, "for"], ["id", "dropdownForm1", "ngbDropdownToggle", "", "src", "./assets/icons/search-filter.svg", 1, "search-icon"], ["ngbDropdownMenu", "", "aria-labelledby", "dropdownForm1", 1, "dropdown-menu", "dropdown-menu-right"], [1, "table-dropdown-search-input", 3, "ngModel", "ngModelChange"], [3, "id", "ngClass"], ["class", "tablerow", 3, "ngClass", "ngSwitch", 4, "ngFor", "ngForOf"], ["class", "handle", 4, "ngIf"], ["class", "extra-row", 4, "ngIf"], [1, "tablerow", 3, "ngClass", "ngSwitch"], ["class", "thumbnail-wrapper", 4, "ngSwitchCase"], ["class", "cell-input-wrapper", 4, "ngSwitchCase"], [3, "title", "ngClass", "click", 4, "ngSwitchDefault"], ["class", "status", 3, "title", "ngClass", 4, "ngSwitchCase"], [3, "title", 4, "ngSwitchCase"], [4, "ngSwitchCase"], [1, "thumbnail-wrapper"], ["width", "60px", 3, "src", "click"], [1, "cell-input-wrapper"], [1, "cell-input", 3, "ngModel", "id", "disabled", "ngModelChange", "focusin", "focusout", "keyup.enter"], ["class", "btn btn-link btn-action cell-edit-icon", 3, "click", 4, "ngIf"], ["class", "danger-text", "translate", "", 4, "ngIf"], [1, "btn", "btn-link", "btn-action", "cell-edit-icon", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-pencil"], ["translate", "", 1, "danger-text"], [3, "title", "ngClass", "click"], [1, "status", 3, "title", "ngClass"], [3, "title"], ["type", "button", "class", "btn expand-btn", 3, "click", 4, "ngIf"], ["class", "btn-group mr-3", 4, "ngIf"], ["class", "btn btn-link btn-action icon-btn", 3, "disabled", "title", "click", 4, "ngIf"], ["type", "button", "class", "btn btn-light btn-action", 3, "disabled", "ngClass", "click", 4, "ngIf"], [1, "btn", "btn-link", "btn-action", "icon-btn", 3, "disabled", "title", "click"], [3, "src"], ["type", "button", 1, "btn", "btn-light", "btn-action", 3, "disabled", "ngClass", "click"], ["type", "button", 1, "btn", "expand-btn", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-caret-down", "arrow", 3, "ngClass"], [1, "btn-group", "mr-3"], ["ngbDropdown", "", "placement", "bottom-right", "role", "group", "aria-label", "Button group with nested dropdown", 1, "btn-group", "table-dropdown"], ["ngbDropdownToggle", "", 1, "btn"], ["aria-hidden", "true", 1, "fa", "fa-ellipsis-v"], ["ngbDropdownMenu", "", 1, "dropdown-menu"], ["ngbDropdownItem", "", 3, "disabled", "click"], [1, "handle"], [1, "fa", "fa-sort"], [1, "extra-row"], [1, "extra-wrapper", 2, "display", "none", 3, "id"], [3, "innerHTML"], ["size", "md", 1, "d-flex", "justify-content-end", 3, "collectionSize", "rotate", "maxSize", "pageSize", "page", "pageChange"], ["ngbPaginationPrevious", ""], ["ngbPaginationNext", ""], ["src", "./assets/icons/back.svg", 1, "handle-icon"], ["src", "./assets/icons/next.svg", 1, "handle-icon"], [1, "win-table-footer"], [1, "number"], [1, "total"]], template: function TableComponent_Template(rf, ctx) { if (rf & 1) {
        const _r139 = i0.ɵɵgetCurrentView();
        i0.ɵɵprojectionDef(_c1);
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelementStart(3, "div", 3);
        i0.ɵɵelementStart(4, "div", 4);
        i0.ɵɵtemplate(5, TableComponent_div_5_Template, 11, 2, "div", 5);
        i0.ɵɵelementStart(6, "div", 6);
        i0.ɵɵtemplate(7, TableComponent_div_7_Template, 2, 1, "div", 7);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "div", 8);
        i0.ɵɵelementStart(9, "div", 9);
        i0.ɵɵelementStart(10, "div", 10);
        i0.ɵɵelementStart(11, "select", 11, 12);
        i0.ɵɵlistener("change", function TableComponent_Template_select_change_11_listener() { i0.ɵɵrestoreView(_r139); const _r2 = i0.ɵɵreference(12); return ctx.doEntryPage(_r2.value); });
        i0.ɵɵtemplate(13, TableComponent_option_13_Template, 2, 3, "option", 13);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(14, "span");
        i0.ɵɵtext(15);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(16, TableComponent_div_16_Template, 4, 7, "div", 14);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(17, "table");
        i0.ɵɵtemplate(18, TableComponent_thead_18_Template, 3, 2, "thead", 7);
        i0.ɵɵelementStart(19, "tbody", 15);
        i0.ɵɵtemplate(20, TableComponent_ng_container_20_Template, 5, 5, "ng-container", 16);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(21, TableComponent_tfoot_21_Template, 4, 2, "tfoot", 7);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(22, TableComponent_div_22_Template, 3, 1, "div", 17);
        i0.ɵɵtemplate(23, TableComponent_div_23_Template, 10, 7, "div", 18);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵclassMap(ctx.tableType);
        i0.ɵɵadvance(5);
        i0.ɵɵproperty("ngIf", ctx.tableOption.tableFilter.hasFilter);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.tableOption.tableActions.length > 0 && ctx.canShowActionButtons);
        i0.ɵɵadvance(6);
        i0.ɵɵproperty("ngForOf", ctx.entryPerpages);
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate1(" ", ctx.TablelanguageObj.table_entries_per_page, "");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.noDataFound());
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", !ctx.noDataFound());
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngForOf", ctx.tableOption.tableRows);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.noDataFound() && ctx.tableType === "WIN");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.tableType === "RTS");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.noDataFound() && ctx.tableType === "WIN");
    } }, directives: [i2.NgIf, i2.NgForOf, i2.NgClass, i3.NgSelectOption, i3.ɵangular_packages_forms_forms_x, i4.NgbPagination, i4.NgbDropdown, i4.NgbDropdownToggle, i4.NgbDropdownMenu, i3.CheckboxControlValueAccessor, i3.NgControlStatus, i3.NgModel, i3.DefaultValueAccessor, i2.NgSwitch, i2.NgSwitchCase, i2.NgSwitchDefault, i4.NgbDropdownItem, i4.NgbPaginationPrevious, i4.NgbPaginationNext], pipes: [i5.FilterPipe, i2.DatePipe], styles: ["#win-table[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:column}#win-table[_ngcontent-%COMP%]   .extra-row[_ngcontent-%COMP%]{border-bottom:0;height:auto;min-height:0}#win-table[_ngcontent-%COMP%]   .extra-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding-bottom:0!important;padding-top:0!important}#win-table[_ngcontent-%COMP%]   .expand-btn[_ngcontent-%COMP%]{background:none;margin-top:5px;text-align:center}#win-table[_ngcontent-%COMP%]   .expand-btn[_ngcontent-%COMP%]   .arrow-rotate[_ngcontent-%COMP%]{transform:rotate(-180deg)}#win-table[_ngcontent-%COMP%]   .expand-btn[_ngcontent-%COMP%]   .arrow[_ngcontent-%COMP%]{transition:.2s ease}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]:after{display:none}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-item[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]{background:none;margin-top:4px}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]:hover{background:#f7f8f9}#win-table[_ngcontent-%COMP%]   .ui-sortable-helper[_ngcontent-%COMP%]{display:inline-table}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]{align-items:center;background:#fff;border:1px solid #ebedf8;border-radius:3px;display:flex;flex-direction:column;font-size:14px;padding-bottom:40px;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]{align-items:center;display:flex;justify-content:center;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]{align-items:baseline;display:flex;margin-top:20px;width:95%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]{align-items:baseline;flex:3}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:9;list-style:none;margin:0;padding:0;z-index:1}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0;padding:0}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]{width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]{align-items:baseline;display:flex;flex-direction:row;justify-content:flex-start}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{cursor:pointer;margin-right:5px;width:18px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]   .filter-text[_ngcontent-%COMP%]{color:#0800d1;font-weight:700}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:none;list-style:none;padding:10px 10px 30px;position:absolute;top:100%;width:250%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{width:100%;z-index:1}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown.open[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{display:flex}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .action-button[_ngcontent-%COMP%]{align-items:baseline;display:flex;flex:7;justify-content:flex-end}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]{align-items:center;border-bottom:1px solid #f0f0f0;display:flex;justify-content:center;padding:10px 0;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]{align-items:baseline;display:flex;width:95%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .number-per-page[_ngcontent-%COMP%]{align-items:baseline;flex:1}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .number-per-page[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{background:#fff;border:1px solid #f0f0f0;border-radius:5px;max-width:120px;padding:5px;width:100px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .paging-wrapper[_ngcontent-%COMP%]{align-items:baseline;color:#aab2c0;display:flex;flex:1;justify-content:flex-end}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .paging-wrapper[_ngcontent-%COMP%]   .paging-title[_ngcontent-%COMP%]{margin-right:10px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]{width:95%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]{color:#b4bac6;font-size:12px;padding:30px 15px 10px 0;text-align:left;text-transform:uppercase}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th.sortable[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:row;justify-content:center;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{flex:9}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#000;flex:1;text-align:right}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]{min-height:40px;vertical-align:text-top}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{color:#b4bac6;font-size:14px;padding:10px 0}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td.bold[_ngcontent-%COMP%]{color:#354052;font-weight:700}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]   .hand[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]   button.btn-action[_ngcontent-%COMP%]{margin-right:5px;margin-top:5px;padding:10px 0!important;text-transform:uppercase;width:80px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tfoot[_ngcontent-%COMP%]{font-size:24px;font-weight:700;text-align:center}#win-table[_ngcontent-%COMP%]   .win-table-footer[_ngcontent-%COMP%]{color:#aab2c0;font-size:15px;margin-top:20px;width:95%}#win-table[_ngcontent-%COMP%]   .win-table-footer[_ngcontent-%COMP%]   .number[_ngcontent-%COMP%]{color:#000;font-weight:700}#win-table[_ngcontent-%COMP%]   .win-table-footer[_ngcontent-%COMP%]   .total[_ngcontent-%COMP%]{font-weight:700}#win-table[_ngcontent-%COMP%]   .ellipsis-td[_ngcontent-%COMP%]{max-width:110px;overflow:hidden;padding:0 10px!important;text-overflow:ellipsis;white-space:nowrap}#win-table[_ngcontent-%COMP%]   .wordwrap-td[_ngcontent-%COMP%]{display:block;max-width:110px}#win-table[_ngcontent-%COMP%]   .ACTION[_ngcontent-%COMP%]{text-align:center!important}#win-table[_ngcontent-%COMP%]   .icon-btn[_ngcontent-%COMP%]{width:50px!important}#win-table[_ngcontent-%COMP%]   .icon-btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:20px}#win-table[_ngcontent-%COMP%]   .text-align-right[_ngcontent-%COMP%]{padding-right:10px!important;text-align:right}#win-table[_ngcontent-%COMP%]   .header-text-align-right[_ngcontent-%COMP%]{padding-right:10px!important;text-align:right!important}#win-table[_ngcontent-%COMP%]   .filter-mask[_ngcontent-%COMP%]{bottom:0;display:none;left:0;position:fixed;right:0;top:0}#win-table[_ngcontent-%COMP%]   .open[_ngcontent-%COMP%]   .filter-mask[_ngcontent-%COMP%]{display:block}#win-table[_ngcontent-%COMP%]   .thumbnail-wrapper[_ngcontent-%COMP%]{cursor:pointer;height:30px;overflow:hidden;width:60px}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-edit-icon[_ngcontent-%COMP%]{color:#5a5a5a;cursor:pointer;margin-right:0;margin-top:0;width:35px!important}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-edit-icon[_ngcontent-%COMP%]:hover{color:#9a0000}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-input[_ngcontent-%COMP%]{background:#fff;border:0}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-input[_ngcontent-%COMP%]:focus{border-bottom:1px solid #07f}#win-table[_ngcontent-%COMP%]   .sortable-row[_ngcontent-%COMP%]{cursor:move}#win-table[_ngcontent-%COMP%]   .sortable-row[_ngcontent-%COMP%]:hover{background:hsla(0,0%,50.2%,.03)}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]{opacity:0;position:absolute}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%] + label[_ngcontent-%COMP%]{cursor:pointer;font-weight:200;padding:0;position:relative;text-transform:none}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%] + label[_ngcontent-%COMP%]:before{background:#fff;border:1px solid #a6b4bf;border-radius:2px;content:\"\";display:inline-block;height:20px;margin-right:10px;vertical-align:text-top;width:20px}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:checked + label[_ngcontent-%COMP%]:before{border:1px solid #4ca0fd}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:disabled + label[_ngcontent-%COMP%]{color:#b8b8b8;cursor:auto}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:disabled + label[_ngcontent-%COMP%]:before{background:#ddd;box-shadow:none}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:checked + label[_ngcontent-%COMP%]:after{background:#4ca0fd;box-shadow:2px 0 0 #4ca0fd,4px 0 0 #4ca0fd,4px -2px 0 #4ca0fd,4px -4px 0 #4ca0fd,4px -6px 0 #4ca0fd,4px -8px 0 #4ca0fd;content:\"\";height:2px;left:5px;position:absolute;top:10px;transform:rotate(45deg);width:2px}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]:before{border-bottom:7px solid rgba(0,0,0,.03);border-left:7px solid transparent;border-right:7px solid transparent;content:\"\";display:inline-block;position:absolute;right:2px;top:-7px}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]{top:25px!important}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]:after{border-bottom:6px solid #fff;border-left:6px solid transparent;border-right:6px solid transparent;content:\"\";display:inline-block;position:absolute;right:3px;top:-6px}.RTS[_ngcontent-%COMP%]   .dropdown-menu-right[_ngcontent-%COMP%]{left:auto!important;right:0!important}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]{border:none;box-shadow:0 3px 11px -1px #d6d6d6;padding:10px 10px 0}.RTS[_ngcontent-%COMP%]   .table-dropdown-btn[_ngcontent-%COMP%]{background:#4ca0fd;border:none;border-radius:5px;color:#fff;cursor:pointer;height:40px;margin:5px;min-width:100px;text-align:center}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]{border:1px solid #e2e6ea!important;border-radius:5px!important;padding-bottom:0!important}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]{width:100%!important}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]{border-bottom:1px solid #e2e6ea}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]{height:50px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]{background:#fdfefe;padding-top:6px!important}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]{color:#577286;padding-left:15px;text-transform:uppercase}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .filter-icon[_ngcontent-%COMP%]{cursor:pointer;width:17px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .search-icon[_ngcontent-%COMP%]{cursor:pointer;margin-top:3px;width:25px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .table-dropdown-item[_ngcontent-%COMP%]{padding-left:5px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .table-dropdown-btm-wrapper[_ngcontent-%COMP%]{display:inline-flex;text-align:center}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .table-dropdown-search-input[_ngcontent-%COMP%]{border:1px solid #d8d8d8;border-radius:5px;height:40px;margin-bottom:10px;margin-left:5px;margin-top:10px;padding-left:10px;width:96%}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]{border-bottom:1px solid #e2e6ea;height:50px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td.tablerow[_ngcontent-%COMP%]{padding-bottom:0!important;padding-left:15px!important;padding-top:0}.RTS[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]{margin-top:35px;width:100%}.RTS[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .handle-icon[_ngcontent-%COMP%]{position:relative;top:-2px}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TableComponent, [{
        type: Component,
        args: [{
                selector: "via-table",
                templateUrl: "./table.html",
                styleUrls: ["./table.scss"]
            }]
    }], function () { return [{ type: i1.Router }]; }, { type: [{
            type: Input
        }], tableOption: [{
            type: Input
        }], rowSortable: [{
            type: Input
        }], canShowActionButtons: [{
            type: Input
        }], downloadFilePath: [{
            type: Input
        }], actionSort: [{
            type: Output
        }], pageChange: [{
            type: Output
        }], entryPageChange: [{
            type: Output
        }], actionButton: [{
            type: Output
        }], dropdownSearch: [{
            type: Output
        }], dropdownFilter: [{
            type: Output
        }], openGalleryImagePreview: [{
            type: Output
        }], doGalleryUpdateIndexCallback: [{
            type: Output
        }], doGalleryUpdateInput: [{
            type: Output
        }], TablelanguageObj: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy90YWJsZS90YWJsZS50cyIsImNvbXBvbmVudHMvdGFibGUvdGFibGUuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzNDLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxRQUFRLENBQUM7QUFFOUIsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGNBQWMsQ0FBQztBQUVwRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUV2RCxPQUFPLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7Ozs7SUNKdkMsK0JBQ0k7SUFBQSw4QkFDSTtJQUFBLDhCQUNJO0lBQUEsZ0NBQ0k7SUFBQSwrQkFDQTtJQURLLG9MQUF3QjtJQUE3QixpQkFDQTtJQUFBLGdDQUN3QjtJQURsQixxTEFBd0I7SUFDTixZQUE4QjtJQUFBLGlCQUFPO0lBQ2pFLGlCQUFPO0lBQ1AsK0JBQXNEO0lBQWpELGtMQUFzQjtJQUFxQixpQkFBTTtJQUN0RCw4QkFDSTtJQUFBLDBCQUNJO0lBQUEsbUJBQThCO0lBQ2xDLGlCQUFLO0lBQ1QsaUJBQUs7SUFDVCxpQkFBSztJQUNULGlCQUFLO0lBQ1QsaUJBQU07OztJQWR1QixlQUFzQztJQUF0QywyREFBc0M7SUFJM0IsZUFBOEI7SUFBOUIsdURBQThCOzs7O0lBYzFELGtDQUNnRDtJQUF4Qix5UEFBdUI7SUFBQyxZQUMxQztJQUFBLGlCQUFTOzs7SUFEWCxxQ0FBbUI7SUFBeUIsZUFDMUM7SUFEMEMsa0NBQzFDOzs7SUFIViw0QkFDSTtJQUFBLG1GQUNnRDtJQUVwRCxpQkFBTzs7O0lBSEssZUFBa0I7SUFBbEIscUNBQWtCOzs7SUFGbEMsMkJBQ0k7SUFBQSx3RUFDSTtJQUlSLGlCQUFNOzs7SUFMSSxlQUE0QztJQUE1Qyx5REFBNEM7OztJQWNsRCxrQ0FDZ0M7SUFBQSxZQUFjO0lBQUEsaUJBQVM7OztJQUFuRCw2Q0FBMkI7SUFEYSx3Q0FBMEI7SUFDdEMsZUFBYztJQUFkLG9DQUFjOzs7O0lBSXRELCtCQUNJO0lBQUEsZ0NBQTJCO0lBQUEsWUFBaUM7SUFBQSxpQkFBTztJQUNuRSwwQ0FBbVA7SUFBbEYsa09BQWtDLDZMQUFBO0lBQStCLGlCQUFpQjtJQUN2UCxpQkFBTTs7O0lBRnlCLGVBQWlDO0lBQWpDLGtFQUFpQztJQUNsQyxlQUF3QjtJQUF4QixzQ0FBd0IsaURBQUEsZ0JBQUEsY0FBQSw2Q0FBQSx3Q0FBQTs7O0lBVzlDLHdCQUFrRjs7O0lBQ2xGLHdCQUFvRjs7Ozs7SUFKNUYsOEJBQ0k7SUFEdUosdU9BQXdCO0lBQy9LLCtCQUNJO0lBQUEsNEJBQU07SUFBQSxZQUFpQjtJQUFBLGlCQUFPO0lBQzlCLCtFQUE4RTtJQUM5RSwrRUFBZ0Y7SUFDcEYsaUJBQU07SUFDVixpQkFBSzs7OztJQU42RSw0SEFBd0U7SUFFNUksZUFBaUI7SUFBakIsc0NBQWlCO0lBQ3BCLGVBQXlCO0lBQXpCLG9EQUF5QjtJQUN6QixlQUEwQjtJQUExQixxREFBMEI7OztJQUdyQyxxQkFBNkI7OztJQVJqQyw4QkFDSTtJQUFBLDRFQUNJOztJQU1KLDJFQUF3QjtJQUM1QixpQkFBSzs7O0lBUmtCLGVBQThEO0lBQTlELGdGQUE4RDtJQU83RSxlQUFtQjtJQUFuQiwwQ0FBbUI7OztJQU1mLHdCQUFrRjs7O0lBQ2xGLHdCQUFvRjs7OztJQUk1RSwrQkFDSTtJQUFBLGlDQUNBO0lBRG1ILHdPQUE0QjtJQUEvSSxpQkFDQTtJQUFBLGlDQUFtRDtJQUFBLFlBQWdCO0lBQUEsaUJBQVE7SUFDL0UsaUJBQU07Ozs7O0lBRjZCLGVBQTBDO0lBQTFDLG9GQUEwQztJQUFpQixtREFBd0I7SUFBQyw0Q0FBNEI7SUFDeEksZUFBMkM7SUFBM0MscUZBQTJDO0lBQUMsZUFBZ0I7SUFBaEIsc0NBQWdCOzs7O0lBTC9FLCtCQUNJO0lBQUEsMEJBQ0E7SUFBQSwrQkFDSTtJQUFBLHlGQUNJO0lBR0osK0JBQ0k7SUFBQSxrQ0FBaUc7SUFBOUQseVNBQTZEO0lBQUMsWUFBd0M7SUFBQSxpQkFBUztJQUNsSixrQ0FBeUY7SUFBdEQsNlJBQXFEO0lBQUMsWUFBOEI7SUFBQSxpQkFBUztJQUNwSSxpQkFBTTtJQUNWLGlCQUFNO0lBQ1YsaUJBQU07Ozs7SUFUbUMsZUFBdUQ7SUFBdkQsK0NBQXVEO0lBS2EsZUFBd0M7SUFBeEMsa0VBQXdDO0lBQ2hELGVBQThCO0lBQTlCLHdEQUE4Qjs7OztJQUluSSwrQkFDSTtJQUFBLDBCQUVBO0lBQUEsK0JBQ0k7SUFBQSxpQ0FDQTtJQUQyQyxtUEFBOEI7SUFBekUsaUJBQ0E7SUFBQSwrQkFDSTtJQUFBLGtDQUFnRztJQUE3RCx3U0FBNEQ7SUFBQyxZQUF3QztJQUFBLGlCQUFTO0lBQ2pKLGtDQUF3RjtJQUFyRCw0UkFBb0Q7SUFBQyxZQUE4QjtJQUFBLGlCQUFTO0lBQ25JLGlCQUFNO0lBQ1YsaUJBQU07SUFDVixpQkFBTTs7OztJQU42QyxlQUE4QjtJQUE5Qiw4Q0FBOEI7SUFFMkIsZUFBd0M7SUFBeEMsa0VBQXdDO0lBQ2hELGVBQThCO0lBQTlCLHdEQUE4Qjs7OztJQXpCMUksOEJBQ0k7SUFEcUksdU9BQXdCO0lBQzdKLCtCQUNJO0lBQUEsNEJBQU07SUFBQSxZQUFnQjtJQUFBLGlCQUFPO0lBQzdCLCtFQUE4RTtJQUM5RSwrRUFBZ0Y7SUFDaEYsbUZBQ0k7SUFZSixtRkFDSTtJQVVSLGlCQUFNO0lBQ1YsaUJBQUs7Ozs7SUE1QlMsZUFBZ0I7SUFBaEIsc0NBQWdCO0lBQ25CLGVBQXlCO0lBQXpCLG9EQUF5QjtJQUN6QixlQUEwQjtJQUExQixxREFBMEI7SUFDTSxlQUFnQztJQUFoQyxtREFBZ0M7SUFhaEMsZUFBeUI7SUFBekIsNENBQXlCOzs7SUFuQnhFLDhCQUNJO0lBQUEsNEVBQ0k7O0lBOEJSLGlCQUFLOzs7SUEvQitDLGVBQXdGO0lBQXhGLGdGQUF3Rjs7O0lBWmhKLDZCQUNJO0lBQUEsdUVBQ0k7SUFTSix1RUFDSTtJQWdDUixpQkFBUTs7O0lBM0NBLGVBQTJCO0lBQTNCLGlEQUEyQjtJQVUzQixlQUEyQjtJQUEzQixpREFBMkI7Ozs7SUFzQ25CLGdDQUNJO0lBQUEsK0JBRUo7SUFGUyx3UUFBc0MsQ0FBQyxZQUFTO0lBQXJELGlCQUVKO0lBQUEsaUJBQU87Ozs7SUFEQyxlQUFnRDtJQUFoRCw0RkFBZ0Q7Ozs7SUFRcEQsa0NBRUk7SUFGSSxnU0FBd0IsSUFBSSw0QkFBYSxRQUFRLGFBQU87SUFFNUQsd0JBQStDO0lBQ25ELGlCQUFTOzs7SUFDVCwrQkFBNEU7SUFBQSwwQkFDNUU7SUFBQSxpQkFBTTs7OztJQVhWLGdDQUNJO0lBQUEsWUFDQTtJQUFBLGlDQUlBO0lBSjBCLDhPQUF3QiwwTkFBQSxxWEFFK0MsS0FBSyxJQUZwRCwwUkFHaEIsS0FBSyw2Q0FBMEIsSUFBSSxrQ0FIbkI7SUFBbEQsaUJBSUE7SUFBQSxrR0FFSTtJQUVKLDRGQUE0RTtJQUVoRixpQkFBTzs7Ozs7SUFYSCxlQUNBO0lBREEsdURBQ0E7SUFBbUQsZUFBcUI7SUFBckIsZ0RBQXFCO0lBQTlDLHdDQUF3Qix1REFBQTtJQUs5QyxlQUFvQztJQUFwQywrREFBb0M7SUFHZixlQUF3QztJQUF4QyxxRUFBd0M7Ozs7SUFHckUsZ0NBQXFJO0lBQXZGLG1RQUEyQjtJQUE0RCxZQUFjO0lBQUEsaUJBQU87OztJQUFwSixpREFBc0I7SUFBOEMsNEVBQTBEO0lBQUMsZUFBYztJQUFkLG9DQUFjOzs7SUFDbkosZ0NBQWtHO0lBQUEsWUFDNUY7SUFBQSxpQkFBTzs7O0lBRFAsaURBQXVCO0lBQThDLHdDQUFzQjtJQUFDLGVBQzVGO0lBRDRGLG9DQUM1Rjs7O0lBQ04sZ0NBQWlGOztJQUFBLFlBQzVEOztJQUFBLGlCQUFPOzs7O0lBRHRCLDJGQUF5QztJQUFrQyxlQUM1RDtJQUQ0RCw4RUFDNUQ7Ozs7SUFJVCxrQ0FHSTtJQUZBLHVRQUFTLHVDQUE4QixJQUFDO0lBRXhDLDBCQUNKO0lBQUEsaUJBQVM7OztJQUhvQyx5REFBOEI7SUFEbkUscURBQWtDO0lBR2pDLGVBQTRCO0lBQTVCLGlFQUE0Qjs7OztJQUVyQyxrQ0FHNkM7SUFBekMsdVFBQVMsdUNBQThCLElBQUM7SUFBQyxZQUMzQztJQUFBLGlCQUFTOzs7SUFKMkIscURBQWtDLDRDQUFBO0lBRzNCLGVBQzNDO0lBRDJDLDRDQUMzQzs7O0lBWFYsNEJBQ0k7SUFBQSw0QkFDSTtJQUFBLHlHQUdJO0lBRUoseUdBRzZDO0lBQ2xDLGlCQUFPO0lBQzlCLGlCQUFPOzs7SUFSUyxlQUE0QjtJQUE1QixnREFBNEI7SUFHeEIsZUFBNkI7SUFBN0IsaURBQTZCOzs7O0lBTWpELGtDQUFzSDtJQUE5Ryw2UEFBZ0IsaUJBQWlCLHFCQUFjO0lBQStELHdCQUVuRjtJQUFBLGlCQUFTOzs7O0lBRGhDLGVBQXlFO0lBQXpFLG1HQUF5RTs7OztJQVF6RSw0QkFDUTtJQUFBLGtDQUM2RTtJQUF6RSxtUkFBZ0MsSUFBSSxHQUFDLHVDQUE4QixHQUFDLEVBQUUsSUFBQztJQUFFLFlBQzNFO0lBQUEsaUJBQVM7SUFDZixpQkFBTzs7O0lBSHFCLGVBQWtDO0lBQWxDLHFEQUFrQztJQUNtQixlQUMzRTtJQUQyRSxzREFDM0U7OztJQVJ0QiwrQkFDSTtJQUFBLCtCQUNJO0lBQUEsa0NBQXNDO0lBQUEsd0JBQ0g7SUFBQSxpQkFBUztJQUM1QywrQkFDSTtJQUFBLG9HQUNRO0lBSVosaUJBQU07SUFDVixpQkFBTTtJQUNWLGlCQUFNOzs7SUFQWSxlQUF1RDtJQUF2RCx3REFBdUQ7OztJQXZCekUsNEJBQ0k7SUFBQSw4RkFDSTtJQVlSLGtHQUFzSDtJQUl0SCw0RkFDSTtJQVlKLGlCQUFPOzs7O0lBOUJHLGVBQStDO0lBQS9DLGdEQUErQztJQWFELGVBQXdCO0lBQXhCLDJDQUF3QjtJQUlwRCxlQUF5QztJQUF6Qyw0REFBeUM7OztJQXpDekUsOEJBQ0k7SUFBQSx1RkFDSTtJQUdKLHVGQUNJO0lBWUosdUZBQXFJO0lBQ3JJLHVGQUFrRztJQUVsRyx1RkFBaUY7SUFFakYsdUZBQ0k7SUErQlIsaUJBQUs7Ozs7SUF2RG9ELDREQUF1QywyQkFBQTtJQUN0RixlQUFzQztJQUF0QywrREFBc0M7SUFJdEMsZUFBNEI7SUFBNUIscURBQTRCO0lBY0osZUFBNkI7SUFBN0Isc0RBQTZCO0lBRVgsZUFBZ0M7SUFBaEMseURBQWdDO0lBRTFFLGVBQTZCO0lBQTdCLHNEQUE2Qjs7O0lBaUN2Qyw4QkFDSTtJQUFBLHdCQUEwQjtJQUM5QixpQkFBSzs7O0lBRVQsOEJBQ0k7SUFBQSwwQkFDSTtJQUFBLCtCQUNJO0lBQUEsMEJBQTBDO0lBQzlDLGlCQUFNO0lBQ1YsaUJBQUs7SUFDVCxpQkFBSzs7Ozs7SUFMRyxlQUFzQztJQUF0QyxvREFBc0M7SUFDakMsZUFBOEI7SUFBOUIseURBQThCO0lBQzFCLGVBQThCO0lBQTlCLG1FQUE4Qjs7O0lBakVuRCw2QkFDSTtJQUFBLDhCQUNJO0lBQUEsOEVBQ0k7SUF1REosOEVBQ0k7SUFFUixpQkFBSztJQUNMLDhFQUNJO0lBTVIsMEJBQWU7Ozs7O0lBcEVQLGVBQXlCO0lBQXpCLG9EQUF5QjtJQUFDLHlGQUEwRDtJQUMvRCxlQUFtQztJQUFuQyw0Q0FBbUM7SUF3RHJDLGVBQW1CO0lBQW5CLHlDQUFtQjtJQUlwQixlQUFvQjtJQUFwQiwwQ0FBb0I7OztJQVVsRCw2QkFDSTtJQUFBLDBCQUNJO0lBQUEsMEJBQXFEO0lBQUEsWUFBa0M7SUFBQSxpQkFBSztJQUNoRyxpQkFBSztJQUNULGlCQUFROzs7SUFGSSxlQUFnRDtJQUFoRCxpRUFBZ0Q7SUFBQyxlQUFrQztJQUFsQywyREFBa0M7OztJQVVwRCwyQkFDbkM7OztJQUMrQiwyQkFDL0I7Ozs7SUFMUiwrQkFDSTtJQUFBLDJDQUNJO0lBRHVLLDRPQUFrQyx1TUFBQTtJQUN6TSw4RkFBbUM7SUFFbkMsOEZBQStCO0lBRW5DLGlCQUFpQjtJQUNyQixpQkFBTTs7O0lBTjJELGVBQXlDO0lBQXpDLGdFQUF5QyxnQkFBQSxjQUFBLCtDQUFBLDBDQUFBOzs7SUFIbEgsOEJBQ0k7SUFBQSw4QkFDSTtJQUFBLHVFQUNJO0lBT1IsaUJBQU07SUFDVixpQkFBTTs7O0lBVDhCLGVBQXNCO0lBQXRCLDRDQUFzQjs7O0lBVzFELGdDQUNJO0lBQUEsWUFDQTtJQUFBLGlDQUFxQjtJQUFBLFlBQXFCO0lBQUEsaUJBQU87SUFBQyxZQUNsRDtJQUFBLGlDQUFxQjtJQUFBLFlBQW1CO0lBQUEsaUJBQU87SUFBQyxZQUNoRDtJQUFBLGlDQUFvQjtJQUFBLFlBQStEO0lBQUEsaUJBQU87SUFDOUYsaUJBQU07OztJQUpGLGVBQ0E7SUFEQSw2RUFDQTtJQUFxQixlQUFxQjtJQUFyQiw4Q0FBcUI7SUFBUSxlQUNsRDtJQURrRCxpRUFDbEQ7SUFBcUIsZUFBbUI7SUFBbkIsNENBQW1CO0lBQVEsZUFDaEQ7SUFEZ0QsaUVBQ2hEO0lBQW9CLGVBQStEO0lBQS9ELHlHQUErRDs7OztBRGpMM0YsTUFBTSxPQUFPLGNBQWM7SUE4QnpCLFlBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBM0J6QixnQkFBVyxHQUFZLEtBQUssQ0FBQztRQUM3Qix5QkFBb0IsR0FBWSxJQUFJLENBQUM7UUFDckMscUJBQWdCLEdBQVcsRUFBRSxDQUFDO1FBQzdCLGVBQVUsR0FBOEIsSUFBSSxZQUFZLEVBQWUsQ0FBQztRQUN4RSxlQUFVLEdBQXlCLElBQUksWUFBWSxFQUFVLENBQUM7UUFDOUQsb0JBQWUsR0FBeUIsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUNuRSxpQkFBWSxHQUE4QixJQUFJLFlBQVksRUFBZSxDQUFDO1FBQ3BGLEtBQUs7UUFDSyxtQkFBYyxHQUE0RCxJQUFJLFlBQVksRUFBNkMsQ0FBQztRQUN4SSxtQkFBYyxHQUFnRixJQUFJLFlBQVksRUFBaUUsQ0FBQztRQUMxTCxVQUFVO1FBQ0EsNEJBQXVCLEdBQXlCLElBQUksWUFBWSxFQUFVLENBQUM7UUFDM0UsaUNBQTRCLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7UUFDMUUseUJBQW9CLEdBQTJCLElBQUksWUFBWSxFQUFZLENBQUM7UUFFdEYsYUFBUSxHQUFRLFNBQVMsQ0FBQztRQUMxQixlQUFVLEdBQVEsV0FBVyxDQUFDO1FBQzlCLGtCQUFhLEdBQW1CLElBQUksS0FBSyxFQUFnQixDQUFDO1FBQzFELGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBQzlCLDBCQUFxQixHQUFZLElBQUksQ0FBQztRQUd0QyxtQkFBYyxHQUFXLEdBQUcsQ0FBQztRQUVwQixxQkFBZ0IsR0FBRyxxQkFBcUIsQ0FBQztJQUlsRCxDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQy9DLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFVBQVUsQ0FBQztZQUNuRCxDQUFDLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLFlBQVksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDbEQsSUFBSSxLQUFLLEdBQWlCLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEQsS0FBSyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxLQUFLLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFO2dCQUNqRCxLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN2QjtTQUNGO1FBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3BCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixVQUFVLENBQUMsR0FBRyxFQUFFO2dCQUNkLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxRQUFRLENBQUM7b0JBQ3RCLFdBQVcsRUFBRSxzQkFBc0I7b0JBQ25DLE1BQU0sRUFBRSxPQUFPO29CQUNmLElBQUksRUFBRSxHQUFHO29CQUNULE1BQU0sRUFBRSxNQUFNO29CQUNkLFVBQVUsRUFBRSxVQUFVLEtBQUssRUFBRSxFQUFPO3dCQUNsQyxJQUFJLGNBQWMsR0FBRyxFQUFFLFNBQVMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUE7d0JBQ3RHLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQ3JDLENBQUM7aUJBQ0YsQ0FBQyxDQUFDO2dCQUNILENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQ3BDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNQO0lBQ0gsQ0FBQztJQUVELFVBQVUsQ0FBQyxFQUFPO1FBQ2hCLElBQUksSUFBSSxHQUFDLEVBQUUsQ0FBQztRQUNaLElBQUksRUFBRSxDQUFDLGtCQUFrQixJQUFJLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUU7WUFDeEQsSUFBSSxHQUFHLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1NBQ2pEO2FBQU0sSUFBRyxFQUFFLENBQUMsc0JBQXNCLElBQUksRUFBRSxDQUFDLHNCQUFzQixDQUFDLEtBQUssRUFBQztZQUNyRSxJQUFJLEdBQUcsRUFBRSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7U0FDckQ7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxlQUFlO1FBQ2IsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO0lBQzVHLENBQUM7SUFFRCxhQUFhO1FBQ1gsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUM7SUFDck0sQ0FBQztJQUVELFdBQVc7UUFDVCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLElBQUksQ0FBQyxDQUFDO0lBQ3RGLENBQUM7SUFFRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUVELFdBQVcsQ0FBQyxJQUFlO1FBQ3pCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7U0FDeEM7SUFDSCxDQUFDO0lBRUQsVUFBVTtRQUNSLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO0lBQzNCLENBQUM7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDekMsQ0FBQztJQUVELFFBQVEsQ0FBQyxTQUFzQjtRQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsWUFBWTtRQUNWLElBQUksYUFBYSxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7UUFDdkYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFhO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELE1BQU0sQ0FBQyxNQUFtQjtRQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUNwQixPQUFPO1NBQ1I7UUFDRCxJQUFJLE9BQU8sR0FBa0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7UUFDM0QsSUFBSSxDQUFDLHFCQUFxQixHQUFHLFNBQVMsQ0FBQztRQUN2QyxJQUFJLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQztRQUNqQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzdELElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksR0FBRyxDQUFDLElBQUksS0FBSyxNQUFNLENBQUMsSUFBSSxFQUFFO2dCQUM1QixJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLEdBQUcsRUFBRTtvQkFDekIsR0FBRyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFBO2lCQUNyQjtxQkFBTSxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDakMsR0FBRyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNyQixHQUFHLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLGNBQWMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUMzRixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDO29CQUN6RSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztpQkFDOUQ7cUJBQU0sSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQ2pDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztpQkFDckI7YUFDRjtpQkFBTTtnQkFDTCxHQUFHLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDdEI7U0FDRjtRQUVELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxTQUFTLENBQUMsTUFBbUI7UUFDM0IsT0FBTyxNQUFNLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLHFCQUFxQixLQUFLLE1BQU0sQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLGVBQWUsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDckosQ0FBQztJQUVELFVBQVUsQ0FBQyxNQUFtQjtRQUM1QixPQUFPLE1BQU0sQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMscUJBQXFCLEtBQUssTUFBTSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN2SixDQUFDO0lBRUQsS0FBSztJQUNMLGlCQUFpQjtJQUNqQixtQkFBbUIsQ0FBQyxJQUErQixFQUFFLElBQVk7UUFDL0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ25FLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxJQUErQjtRQUN0RCxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3JCLE9BQU8sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELGlCQUFpQjtJQUNqQixtQkFBbUIsQ0FBQyxZQUFvQixFQUFFLElBQVk7UUFDcEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzFFLENBQUM7SUFDRCx3QkFBd0IsQ0FBQyxZQUFvQjtRQUMzQyxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxVQUFVO0lBQ1YsYUFBYSxDQUFDLE9BQWU7UUFDM0IsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsYUFBYSxDQUFDLGNBQW1CO1FBQy9CLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVELGtCQUFrQixDQUFDLEdBQWEsRUFBRSxPQUFZLEVBQUUsUUFBYSxFQUFFLGFBQXFCO1FBQ2xGLElBQUksT0FBTyxJQUFJLFFBQVEsQ0FBQyxLQUFLLEtBQUssYUFBYSxFQUFFO1lBQy9DLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNyQztpQkFBTTtnQkFDTCxRQUFRLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzthQUNoQztTQUNGO0lBQ0gsQ0FBQztJQUVELFVBQVUsQ0FBQyxFQUFFO1FBQ1gsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLFFBQVEsQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdEMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQztJQUVELE1BQU0sQ0FBQyxFQUFVLEVBQUUsS0FBSztRQUN0QixJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssRUFBRSxFQUFFO1lBQzdCLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQztTQUNoQzthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssRUFBRSxFQUFFO2dCQUNuRCxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDN0M7WUFDRCxDQUFDLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztTQUN6QjtRQUVELEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs0RUF4TlUsY0FBYzttREFBZCxjQUFjOzs7UUNqQjNCLDhCQUNJO1FBQUEsOEJBQ0k7UUFBQSw4QkFDSTtRQUFBLDhCQUNJO1FBQUEsOEJBQ0k7UUFBQSxnRUFDSTtRQWdCSiw4QkFDSTtRQUFBLCtEQUNJO1FBT1IsaUJBQU07UUFDVixpQkFBTTtRQUNWLGlCQUFNO1FBQ04sOEJBQ0k7UUFBQSw4QkFDSTtRQUFBLGdDQUNJO1FBQUEsdUNBQ0k7UUFEVyx1SkFBVSwwQkFBd0IsSUFBQztRQUM5Qyx3RUFDZ0M7UUFDcEMsaUJBQVM7UUFDVCw2QkFBTztRQUFBLGFBQTRDO1FBQUEsaUJBQU87UUFDOUQsaUJBQU07UUFDTixrRUFDSTtRQUdSLGlCQUFNO1FBQ1YsaUJBQU07UUFDVixpQkFBTTtRQUNOLDhCQUNJO1FBQUEscUVBQ0k7UUE0Q0osa0NBQ0k7UUFBQSxvRkFDSTtRQXNFUixpQkFBUTtRQUNSLHFFQUNJO1FBSVIsaUJBQVE7UUFDWixpQkFBTTtRQUVOLGtFQUNJO1FBWUosbUVBQ0k7UUFLUixpQkFBTTs7UUFwTWMsNEJBQXFCO1FBS0QsZUFBeUM7UUFBekMsNERBQXlDO1FBa0JwRCxlQUFtRTtRQUFuRSwwRkFBbUU7UUFlNUQsZUFBbUM7UUFBbkMsMkNBQW1DO1FBR3hDLGVBQTRDO1FBQTVDLDJFQUE0QztRQUUzQixlQUFzQjtRQUF0Qix5Q0FBc0I7UUFRbkQsZUFBc0I7UUFBdEIseUNBQXNCO1FBOENYLGVBQXdEO1FBQXhELG1EQUF3RDtRQXdFbkUsZUFBNEM7UUFBNUMsbUVBQTRDO1FBUXZDLGVBQTJCO1FBQTNCLDhDQUEyQjtRQWFqQixlQUE2QztRQUE3QyxvRUFBNkM7O2tERDdLbEUsY0FBYztjQUwxQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLFdBQVcsRUFBRSxjQUFjO2dCQUMzQixTQUFTLEVBQUUsQ0FBQyxjQUFjLENBQUM7YUFDNUI7eURBRVUsSUFBSTtrQkFBWixLQUFLO1lBQ0csV0FBVztrQkFBbkIsS0FBSztZQUNHLFdBQVc7a0JBQW5CLEtBQUs7WUFDRyxvQkFBb0I7a0JBQTVCLEtBQUs7WUFDRyxnQkFBZ0I7a0JBQXhCLEtBQUs7WUFDSSxVQUFVO2tCQUFuQixNQUFNO1lBQ0csVUFBVTtrQkFBbkIsTUFBTTtZQUNHLGVBQWU7a0JBQXhCLE1BQU07WUFDRyxZQUFZO2tCQUFyQixNQUFNO1lBRUcsY0FBYztrQkFBdkIsTUFBTTtZQUNHLGNBQWM7a0JBQXZCLE1BQU07WUFFRyx1QkFBdUI7a0JBQWhDLE1BQU07WUFDRyw0QkFBNEI7a0JBQXJDLE1BQU07WUFDRyxvQkFBb0I7a0JBQTdCLE1BQU07WUFXRSxnQkFBZ0I7a0JBQXhCLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBEQVRFX0ZPUk1BVCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5pbXBvcnQgeyBTT1JUIH0gZnJvbSBcIi4vc29ydFwiO1xuaW1wb3J0IHsgVGFibGVBY3Rpb24gfSBmcm9tIFwiLi90YWJsZS5hY3Rpb25cIjtcbmltcG9ydCB7IENFTExfVFlQRSwgVGFibGVDZWxsIH0gZnJvbSBcIi4vdGFibGUuY2VsbFwiO1xuaW1wb3J0IHsgVGFibGVDb2x1bW4gfSBmcm9tIFwiLi90YWJsZS5jb2x1bW5cIjtcbmltcG9ydCB7IFRBQkxFX0xBTkdVQUdFX0NPTkZJRyB9IGZyb20gJy4vdGFibGUuY29uZmlnJztcbmltcG9ydCB7IFRhYmxlRmlsdGVyRHJvcGRvd25JdGVtIH0gZnJvbSAnLi90YWJsZS5maWx0ZXIuZHJvcGRvd24nO1xuaW1wb3J0IHsgRW50cnlQZXJwYWdlLCBUYWJsZU9wdGlvbiB9IGZyb20gXCIuL3RhYmxlLm9wdGlvblwiO1xuaW1wb3J0IHsgVGFibGVSb3cgfSBmcm9tIFwiLi90YWJsZS5yb3dcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10YWJsZVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL3RhYmxlLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3RhYmxlLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgVGFibGVDb21wb25lbnQge1xuICBASW5wdXQoKSB0eXBlOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHRhYmxlT3B0aW9uOiBUYWJsZU9wdGlvbjtcbiAgQElucHV0KCkgcm93U29ydGFibGU6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQElucHV0KCkgY2FuU2hvd0FjdGlvbkJ1dHRvbnM6IGJvb2xlYW4gPSB0cnVlO1xuICBASW5wdXQoKSBkb3dubG9hZEZpbGVQYXRoOiBzdHJpbmcgPSAnJztcbiAgQE91dHB1dCgpIGFjdGlvblNvcnQ6IEV2ZW50RW1pdHRlcjxUYWJsZUNvbHVtbj4gPSBuZXcgRXZlbnRFbWl0dGVyPFRhYmxlQ29sdW1uPigpO1xuICBAT3V0cHV0KCkgcGFnZUNoYW5nZTogRXZlbnRFbWl0dGVyPG51bWJlcj4gPSBuZXcgRXZlbnRFbWl0dGVyPG51bWJlcj4oKTtcbiAgQE91dHB1dCgpIGVudHJ5UGFnZUNoYW5nZTogRXZlbnRFbWl0dGVyPG51bWJlcj4gPSBuZXcgRXZlbnRFbWl0dGVyPG51bWJlcj4oKTtcbiAgQE91dHB1dCgpIGFjdGlvbkJ1dHRvbjogRXZlbnRFbWl0dGVyPFRhYmxlQWN0aW9uPiA9IG5ldyBFdmVudEVtaXR0ZXI8VGFibGVBY3Rpb24+KCk7XG4gIC8vUlRTXG4gIEBPdXRwdXQoKSBkcm9wZG93blNlYXJjaDogRXZlbnRFbWl0dGVyPHsgc2VhcmNoU3RyOiBzdHJpbmcsIGNvbHVtbk5hbWU6IHN0cmluZyB9PiA9IG5ldyBFdmVudEVtaXR0ZXI8eyBzZWFyY2hTdHI6IHN0cmluZywgY29sdW1uTmFtZTogc3RyaW5nIH0+KCk7XG4gIEBPdXRwdXQoKSBkcm9wZG93bkZpbHRlcjogRXZlbnRFbWl0dGVyPHsgZmlsdGVyRGF0YTogVGFibGVGaWx0ZXJEcm9wZG93bkl0ZW1bXSwgY29sdW1uTmFtZTogc3RyaW5nIH0+ID0gbmV3IEV2ZW50RW1pdHRlcjx7IGZpbHRlckRhdGE6IFRhYmxlRmlsdGVyRHJvcGRvd25JdGVtW10sIGNvbHVtbk5hbWU6IHN0cmluZyB9PigpO1xuICAvLyBnYWxsZXJ5XG4gIEBPdXRwdXQoKSBvcGVuR2FsbGVyeUltYWdlUHJldmlldzogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcbiAgQE91dHB1dCgpIGRvR2FsbGVyeVVwZGF0ZUluZGV4Q2FsbGJhY2s6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG4gIEBPdXRwdXQoKSBkb0dhbGxlcnlVcGRhdGVJbnB1dDogRXZlbnRFbWl0dGVyPFRhYmxlUm93PiA9IG5ldyBFdmVudEVtaXR0ZXI8VGFibGVSb3c+KCk7XG5cbiAgY2VsbFR5cGU6IGFueSA9IENFTExfVFlQRTtcbiAgZGF0ZUZvcm1hdDogYW55ID0gREFURV9GT1JNQVQ7XG4gIGVudHJ5UGVycGFnZXM6IEVudHJ5UGVycGFnZVtdID0gbmV3IEFycmF5PEVudHJ5UGVycGFnZT4oKTtcbiAgaXNTaG93RmlsdGVyOiBib29sZWFuID0gZmFsc2U7XG4gIGlzU2hvd0FjdGlvbkhlYWRlckJ0bjogYm9vbGVhbiA9IHRydWU7XG4gIGN1cnJlbnRTb3J0Q29sdW1uTmFtZTogc3RyaW5nO1xuICBjdXJyZW50U29ydFR5cGU6IHN0cmluZztcbiAgaW5wdXRNYXhMZW5naHQ6IG51bWJlciA9IDI1NTtcbiAgdGFibGVUeXBlOiBzdHJpbmc7XG4gIEBJbnB1dCgpIFRhYmxlbGFuZ3VhZ2VPYmogPSBUQUJMRV9MQU5HVUFHRV9DT05GSUc7XG5cbiAgZXhwYW5kZWRSb3dJZDogc3RyaW5nO1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnRhYmxlVHlwZSA9IHRoaXMudHlwZSA/IHRoaXMudHlwZSA6IFwiUlRTXCI7XG4gICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJy5kcm9wZG93bi1tZW51JywgZnVuY3Rpb24gKGUpIHtcbiAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgfSk7XG5cbiAgICB0aGlzLmVudHJ5UGVycGFnZXMucHVzaChuZXcgRW50cnlQZXJwYWdlKFwiNVwiLCA1KSk7XG4gICAgdGhpcy5lbnRyeVBlcnBhZ2VzLnB1c2gobmV3IEVudHJ5UGVycGFnZShcIjEwXCIsIDEwKSk7XG4gICAgdGhpcy5lbnRyeVBlcnBhZ2VzLnB1c2gobmV3IEVudHJ5UGVycGFnZShcIjIwXCIsIDIwKSk7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmVudHJ5UGVycGFnZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGxldCBlbnRyeTogRW50cnlQZXJwYWdlID0gdGhpcy5lbnRyeVBlcnBhZ2VzW2ldO1xuICAgICAgZW50cnkuc2VsZWN0ZWQgPSBmYWxzZTtcbiAgICAgIGlmIChlbnRyeS52YWx1ZSA9PT0gdGhpcy50YWJsZU9wdGlvbi5pdGVtc1BlclBhZ2UpIHtcbiAgICAgICAgZW50cnkuc2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0aGlzLnJvd1NvcnRhYmxlKSB7XG4gICAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgJCgnI3NvcnRhYmxlJykuc29ydGFibGUoe1xuICAgICAgICAgIHBsYWNlaG9sZGVyOiBcInNvcnRhYmxlLXBsYWNlaG9sZGVyXCIsXG4gICAgICAgICAgaGVscGVyOiAnY2xvbmUnLFxuICAgICAgICAgIGF4aXM6IFwieVwiLFxuICAgICAgICAgIGN1cnNvcjogXCJtb3ZlXCIsXG4gICAgICAgICAgZGVhY3RpdmF0ZTogZnVuY3Rpb24gKGV2ZW50LCB1aTogYW55KSB7XG4gICAgICAgICAgICBsZXQgZHJhZ1Bvc3Rpb25PYmogPSB7IGZyb21JbmRleDogdWkuaXRlbVswXS5jZWxsc1sxXS5pbm5lclRleHQsIHRvSW5kZXg6IHNlbGYuZ2V0U2libGluZyh1aS5pdGVtWzBdKX1cbiAgICAgICAgICAgIHNlbGYuZG9VcGRhdGVJbmRleChkcmFnUG9zdGlvbk9iaik7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgJChcIiNzb3J0YWJsZVwiKS5kaXNhYmxlU2VsZWN0aW9uKCk7XG4gICAgICB9LCAwKTtcbiAgICB9XG4gIH1cblxuICBnZXRTaWJsaW5nKHVpOiBhbnkpIHtcbiAgICBsZXQgdGV4dD1cIlwiO1xuICAgIGlmICh1aS5uZXh0RWxlbWVudFNpYmxpbmcgJiYgdWkubmV4dEVsZW1lbnRTaWJsaW5nLmNlbGxzKSB7XG4gICAgICB0ZXh0ID0gdWkubmV4dEVsZW1lbnRTaWJsaW5nLmNlbGxzWzFdLmlubmVyVGV4dDtcbiAgICB9IGVsc2UgaWYodWkucHJldmlvdXNFbGVtZW50U2libGluZyAmJiB1aS5wcmV2aW91c0VsZW1lbnRTaWJsaW5nLmNlbGxzKXtcbiAgICAgIHRleHQgPSB1aS5wcmV2aW91c0VsZW1lbnRTaWJsaW5nLmNlbGxzWzFdLmlubmVyVGV4dDtcbiAgICB9XG4gICAgcmV0dXJuIHRleHQ7XG4gIH1cblxuICBzdGFydEl0ZW1OdW1iZXIoKTogbnVtYmVyIHtcbiAgICByZXR1cm4gKHRoaXMudGFibGVPcHRpb24uY3VycmVudFBhZ2UgKiB0aGlzLnRhYmxlT3B0aW9uLml0ZW1zUGVyUGFnZSkgLSB0aGlzLnRhYmxlT3B0aW9uLml0ZW1zUGVyUGFnZSArIDE7XG4gIH1cblxuICBlbmRJdGVtTnVtYmVyKCk6IG51bWJlciB7XG4gICAgcmV0dXJuICh0aGlzLnRhYmxlT3B0aW9uLmN1cnJlbnRQYWdlICogdGhpcy50YWJsZU9wdGlvbi5pdGVtc1BlclBhZ2UpIDwgdGhpcy50YWJsZU9wdGlvbi50b3RhbEl0ZW1zID8gKHRoaXMudGFibGVPcHRpb24uY3VycmVudFBhZ2UgKiB0aGlzLnRhYmxlT3B0aW9uLml0ZW1zUGVyUGFnZSkgOiB0aGlzLnRhYmxlT3B0aW9uLnRvdGFsSXRlbXM7XG4gIH1cblxuICBub0RhdGFGb3VuZCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy50YWJsZU9wdGlvbi50YWJsZVJvd3MubGVuZ3RoID09PSAwICYmIHRoaXMudGFibGVPcHRpb24uY3VycmVudFBhZ2UgPD0gMTtcbiAgfVxuXG4gIGhpZGVGaWx0ZXIoKTogdm9pZCB7XG4gICAgdGhpcy5pc1Nob3dGaWx0ZXIgPSBmYWxzZTtcbiAgfVxuXG4gIGNlbGxDbGlja2VkKGNlbGw6IFRhYmxlQ2VsbCk6IHZvaWQge1xuICAgIGlmIChjZWxsLnJvdXRlTGluay50cmltKCkubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW2NlbGwucm91dGVMaW5rXSk7XG4gICAgfVxuICB9XG5cbiAgc2hvd0ZpbHRlcigpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2hvd0ZpbHRlciA9IHRydWU7XG4gIH1cblxuICB0b2dnbGVGaWx0ZXIoKTogdm9pZCB7XG4gICAgdGhpcy5pc1Nob3dGaWx0ZXIgPSAhdGhpcy5pc1Nob3dGaWx0ZXI7XG4gIH1cblxuICBkb0FjdGlvbihhY3Rpb25CdG46IFRhYmxlQWN0aW9uKTogdm9pZCB7XG4gICAgdGhpcy5hY3Rpb25CdXR0b24uZW1pdChhY3Rpb25CdG4pO1xuICB9XG5cbiAgZG9QYWdlQ2hhbmdlKCk6IHZvaWQge1xuICAgIGxldCBjdXJyZW50T2Zmc2V0ID0gKHRoaXMudGFibGVPcHRpb24uY3VycmVudFBhZ2UgLSAxKSAqIHRoaXMudGFibGVPcHRpb24uaXRlbXNQZXJQYWdlO1xuICAgIHRoaXMucGFnZUNoYW5nZS5lbWl0KGN1cnJlbnRPZmZzZXQpO1xuICB9XG5cbiAgZG9FbnRyeVBhZ2UodmFsdWU6IG51bWJlcik6IHZvaWQge1xuICAgIHRoaXMudGFibGVPcHRpb24uaXRlbXNQZXJQYWdlID0gdmFsdWU7XG4gICAgdGhpcy50YWJsZU9wdGlvbi5jdXJyZW50UGFnZSA9IDE7XG4gICAgdGhpcy5lbnRyeVBhZ2VDaGFuZ2UuZW1pdCh2YWx1ZSk7XG4gIH1cblxuICBkb1NvcnQoY29sdW1uOiBUYWJsZUNvbHVtbik6IHZvaWQge1xuICAgIGlmICghY29sdW1uLnNvcnRhYmxlKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGxldCBjb2x1bW5zOiBUYWJsZUNvbHVtbltdID0gdGhpcy50YWJsZU9wdGlvbi50YWJsZUNvbHVtbnM7XG4gICAgdGhpcy5jdXJyZW50U29ydENvbHVtbk5hbWUgPSB1bmRlZmluZWQ7XG4gICAgdGhpcy5jdXJyZW50U29ydFR5cGUgPSB1bmRlZmluZWQ7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnRhYmxlT3B0aW9uLnRhYmxlQ29sdW1ucy5sZW5ndGg7IGkrKykge1xuICAgICAgbGV0IGNvbCA9IHRoaXMudGFibGVPcHRpb24udGFibGVDb2x1bW5zW2ldO1xuICAgICAgaWYgKGNvbC5uYW1lID09PSBjb2x1bW4ubmFtZSkge1xuICAgICAgICBpZiAoY29sLnNvcnQgPT09IFNPUlQuQVNDKSB7XG4gICAgICAgICAgY29sLnNvcnQgPSBTT1JULkRFU0NcbiAgICAgICAgfSBlbHNlIGlmIChjb2wuc29ydCA9PT0gU09SVC5ERVNDKSB7XG4gICAgICAgICAgY29sLnNvcnQgPSBTT1JULk5PTkU7XG4gICAgICAgICAgY29sLnNvcnQgPSB0aGlzLnRhYmxlT3B0aW9uLmRlZmF1bHRTb3J0LnNvcnRDb2x1bW5OYW1lID09PSBjb2wubmFtZSA/IFNPUlQuQVNDIDogU09SVC5OT05FO1xuICAgICAgICAgIHRoaXMuY3VycmVudFNvcnRDb2x1bW5OYW1lID0gdGhpcy50YWJsZU9wdGlvbi5kZWZhdWx0U29ydC5zb3J0Q29sdW1uTmFtZTtcbiAgICAgICAgICB0aGlzLmN1cnJlbnRTb3J0VHlwZSA9IHRoaXMudGFibGVPcHRpb24uZGVmYXVsdFNvcnQuc29ydFR5cGU7XG4gICAgICAgIH0gZWxzZSBpZiAoY29sLnNvcnQgPT09IFNPUlQuTk9ORSkge1xuICAgICAgICAgIGNvbC5zb3J0ID0gU09SVC5BU0M7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbC5zb3J0ID0gU09SVC5OT05FO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuYWN0aW9uU29ydC5lbWl0KGNvbHVtbik7XG4gIH1cblxuICBpc1NvcnRBc2MoY29sdW1uOiBUYWJsZUNvbHVtbik6IGJvb2xlYW4ge1xuICAgIHJldHVybiBjb2x1bW4uc29ydCA9PT0gU09SVC5BU0MgfHwgKHRoaXMuY3VycmVudFNvcnRDb2x1bW5OYW1lICYmIHRoaXMuY3VycmVudFNvcnRDb2x1bW5OYW1lID09PSBjb2x1bW4ubmFtZSAmJiB0aGlzLmN1cnJlbnRTb3J0VHlwZSA9PT0gU09SVC5BU0MpO1xuICB9XG5cbiAgaXNTb3J0RGVzYyhjb2x1bW46IFRhYmxlQ29sdW1uKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGNvbHVtbi5zb3J0ID09PSBTT1JULkRFU0MgfHwgKHRoaXMuY3VycmVudFNvcnRDb2x1bW5OYW1lICYmIHRoaXMuY3VycmVudFNvcnRDb2x1bW5OYW1lID09PSBjb2x1bW4ubmFtZSAmJiB0aGlzLmN1cnJlbnRTb3J0VHlwZSA9PT0gU09SVC5ERVNDKTtcbiAgfVxuXG4gIC8vUlRTXG4gIC8vZHJvcGRvd24gZmlsdGVyXG4gIGZpbHRlck9uVGFibGVDb2x1bW4oZGF0YTogVGFibGVGaWx0ZXJEcm9wZG93bkl0ZW1bXSwgbmFtZTogc3RyaW5nKSB7XG4gICAgdGhpcy5kcm9wZG93bkZpbHRlci5lbWl0KHsgZmlsdGVyRGF0YTogZGF0YSwgY29sdW1uTmFtZTogbmFtZSB9KTtcbiAgfVxuXG4gIGZpbHRlck9uVGFibGVDb2x1bW5SZXNldChkYXRhOiBUYWJsZUZpbHRlckRyb3Bkb3duSXRlbVtdKSB7XG4gICAgZGF0YS5mb3JFYWNoKGVsZW1lbnQgPT4ge1xuICAgICAgZWxlbWVudC5jaGVja2VkID0gZmFsc2U7XG4gICAgfSk7XG4gIH1cbiAgLy9kcm9wZG93biBzZWFyY2hcbiAgdGFibGVEcm9wZG93blNlYXJjaChzZWFyY2hTdHJpbmc6IHN0cmluZywgbmFtZTogc3RyaW5nKSB7XG4gICAgdGhpcy5kcm9wZG93blNlYXJjaC5lbWl0KHsgc2VhcmNoU3RyOiBzZWFyY2hTdHJpbmcsIGNvbHVtbk5hbWU6IG5hbWUgfSk7XG4gIH1cbiAgdGFibGVEcm9wZG93blNlYXJjaFJlc2V0KHNlYXJjaFN0cmluZzogc3RyaW5nKSB7XG4gICAgc2VhcmNoU3RyaW5nID0gXCJcIjtcbiAgfVxuXG4gIC8vIGdhbGxlcnlcbiAgZG9PcGVuUHJldmlldyhpbWdDb2RlOiBzdHJpbmcpOiB2b2lkIHtcbiAgICB0aGlzLm9wZW5HYWxsZXJ5SW1hZ2VQcmV2aWV3LmVtaXQoaW1nQ29kZSk7XG4gIH1cblxuICBkb1VwZGF0ZUluZGV4KGRyYWdQb3N0aW9uT2JqOiBhbnkpOiB2b2lkIHtcbiAgICB0aGlzLmRvR2FsbGVyeVVwZGF0ZUluZGV4Q2FsbGJhY2suZW1pdChkcmFnUG9zdGlvbk9iaik7XG4gIH1cblxuICBkb1VwZGF0ZUlucHV0RXZlbnQocm93OiBUYWJsZVJvdywgY2FuRWRpdDogYW55LCBuZXdWYWx1ZTogYW55LCBvcmlnaW5hbFZhbHVlOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBpZiAoY2FuRWRpdCAmJiBuZXdWYWx1ZS52YWx1ZSAhPT0gb3JpZ2luYWxWYWx1ZSkge1xuICAgICAgaWYgKG5ld1ZhbHVlLnZhbHVlLmxlbmd0aCA8PSB0aGlzLmlucHV0TWF4TGVuZ2h0KSB7XG4gICAgICAgIHRoaXMuZG9HYWxsZXJ5VXBkYXRlSW5wdXQuZW1pdChyb3cpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgbmV3VmFsdWUudmFsdWUgPSBvcmlnaW5hbFZhbHVlO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGZvY3VzSW5wdXQoaWQpIHtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlkKS5mb2N1cygpO1xuICAgIH0sIDApO1xuICB9XG5cbiAgZXhwYW5kKGlkOiBzdHJpbmcsIGV2ZW50KSB7XG4gICAgaWYgKHRoaXMuZXhwYW5kZWRSb3dJZCA9PT0gaWQpIHtcbiAgICAgICQoXCIjXCIgKyB0aGlzLmV4cGFuZGVkUm93SWQpLnNsaWRlVXAoXCJmYXN0XCIpO1xuICAgICAgdGhpcy5leHBhbmRlZFJvd0lkID0gdW5kZWZpbmVkO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAodGhpcy5leHBhbmRlZFJvd0lkICYmIHRoaXMuZXhwYW5kZWRSb3dJZCAhPT0gaWQpIHtcbiAgICAgICAgJChcIiNcIiArIHRoaXMuZXhwYW5kZWRSb3dJZCkuc2xpZGVVcChcImZhc3RcIik7XG4gICAgICB9XG4gICAgICAkKFwiI1wiICsgaWQpLnNsaWRlVG9nZ2xlKFwiZmFzdFwiKTtcbiAgICAgIHRoaXMuZXhwYW5kZWRSb3dJZCA9IGlkO1xuICAgIH1cblxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gIH1cbn1cbiIsIjxkaXYgaWQ9XCJ3aW4tdGFibGVcIiBjbGFzcz1cInt7dGFibGVUeXBlfX1cIj5cbiAgICA8ZGl2IGNsYXNzPVwid2luLXRhYmxlLXdyYXBwZXJcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cInRhYmxlLXRvcFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImFjdGlvbi13cmFwcGVyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImFjdGlvbi1jb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpbHRlclwiICpuZ0lmPVwidGFibGVPcHRpb24udGFibGVGaWx0ZXIuaGFzRmlsdGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJmaWx0ZXItY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwiZHJvcGRvd25cIiBbbmdDbGFzc109XCJpc1Nob3dGaWx0ZXIgPyAnb3BlbicgOiAnJ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImRyb3Bkb3duLWNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgKGNsaWNrKT1cInRvZ2dsZUZpbHRlcigpXCIgc3JjPVwiLi9hc3NldHMvaWNvbnMvaWNfZmlsdGVyLnN2Z1wiIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAoY2xpY2spPVwidG9nZ2xlRmlsdGVyKClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZmlsdGVyLXRleHRcIj57e1RhYmxlbGFuZ3VhZ2VPYmouZmlsdGVyX2J5fX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiAoY2xpY2spPVwiaGlkZUZpbHRlcigpXCIgY2xhc3M9XCJmaWx0ZXItbWFza1wiPjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJkcm9wZG93bi1tZW51LWl0ZW1zIHotZGVwdGgtMVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxuZy1jb250ZW50IHNlbGVjdD1cIltmaWx0ZXJdXCI+PC9uZy1jb250ZW50PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhY3Rpb24tYnV0dG9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2ICpuZ0lmPVwidGFibGVPcHRpb24udGFibGVBY3Rpb25zLmxlbmd0aCA+IDAgJiYgY2FuU2hvd0FjdGlvbkJ1dHRvbnNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAqbmdGb3I9XCJsZXQgYnRuIG9mIHRhYmxlT3B0aW9uLnRhYmxlQWN0aW9uc1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uICpuZ0lmPVwiIWJ0bi5oaWRlZFwiIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biB1cHBlcmNhc2UtdGV4dCBtLWwtMTBcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW25nQ2xhc3NdPVwiYnRuLmNzc1wiIChjbGljayk9XCJkb0FjdGlvbihidG4pXCI+e3tidG4ubmFtZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX08L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhZ2luZ1wiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwYWdpbmctY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJudW1iZXItcGVyLXBhZ2VcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzZWxlY3QgI2VudHJ5IChjaGFuZ2UpPVwiZG9FbnRyeVBhZ2UoZW50cnkudmFsdWUpXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiAqbmdGb3I9XCJsZXQgZW50cnkgb2YgZW50cnlQZXJwYWdlc1wiIFthdHRyLnZhbHVlXT1cImVudHJ5LnZhbHVlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW3NlbGVjdGVkXT1cImVudHJ5LnNlbGVjdGVkXCI+e3tlbnRyeS5uYW1lfX08L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+IHt7VGFibGVsYW5ndWFnZU9iai50YWJsZV9lbnRyaWVzX3Blcl9wYWdlIH19PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInBhZ2luZy13cmFwcGVyXCIgKm5nSWY9XCIhbm9EYXRhRm91bmQoKVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJwYWdpbmctdGl0bGVcIj57e1RhYmxlbGFuZ3VhZ2VPYmoudGFibGVfcGFnZSB9fSA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICA8bmdiLXBhZ2luYXRpb24gc2l6ZT1cInNtXCIgW2RpcmVjdGlvbkxpbmtzXT1cImZhbHNlXCIgW2NvbGxlY3Rpb25TaXplXT1cInRhYmxlT3B0aW9uLnRvdGFsSXRlbXNcIiBbcm90YXRlXT1cInRydWVcIiBbbWF4U2l6ZV09XCI1XCIgW3BhZ2VTaXplXT1cInRhYmxlT3B0aW9uLml0ZW1zUGVyUGFnZVwiIFsocGFnZSldPVwidGFibGVPcHRpb24uY3VycmVudFBhZ2VcIiAocGFnZUNoYW5nZSk9XCJkb1BhZ2VDaGFuZ2UoKVwiPjwvbmdiLXBhZ2luYXRpb24+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8dGFibGU+XG4gICAgICAgICAgICA8dGhlYWQgKm5nSWY9XCIhbm9EYXRhRm91bmQoKVwiPlxuICAgICAgICAgICAgICAgIDx0ciAqbmdJZj1cInRhYmxlVHlwZSA9PT0gJ1dJTidcIiBjbGFzcz1cImhlYWRpbmdcIj5cbiAgICAgICAgICAgICAgICAgICAgPHRoIGNsYXNzPSdoYW5kbGUnICpuZ0Zvcj1cImxldCBjb2x1bW4gb2YgdGFibGVPcHRpb24udGFibGVDb2x1bW5zIHwgZmlsdGVyQ29sdW1uXCIgW25nQ2xhc3NdPVwiW2NvbHVtbi5zb3J0YWJsZSA/ICdzb3J0YWJsZScgOiAnJywgY29sdW1uLnNvcnQsIGNvbHVtbi5jc3NdXCIgKGNsaWNrKT1cImRvU29ydChjb2x1bW4pXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaGVhZGVyLWNvbnRhaW5lclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPnt7Y29sdW1uLnZhbHVlIH19PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpICpuZ0lmPVwiaXNTb3J0QXNjKGNvbHVtbilcIiBjbGFzcz1cImZhIGZhLXNvcnQtYW1vdW50LWFzY1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSAqbmdJZj1cImlzU29ydERlc2MoY29sdW1uKVwiIGNsYXNzPVwiZmEgZmEtc29ydC1hbW91bnQtZGVzY1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L3RoPlxuICAgICAgICAgICAgICAgICAgICA8dGggKm5nSWY9XCJyb3dTb3J0YWJsZVwiPjwvdGg+XG4gICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICA8dHIgKm5nSWY9XCJ0YWJsZVR5cGUgPT09ICdSVFMnXCIgY2xhc3M9XCJoZWFkaW5nXCI+XG4gICAgICAgICAgICAgICAgICAgIDx0aCBzdHlsZT1cImJvcmRlci1yYWRpdXM6IDUwcHg7XCIgY2xhc3M9J2hhbmRsZScgKm5nRm9yPVwibGV0IGNvbHVtbiBvZiB0YWJsZU9wdGlvbi50YWJsZUNvbHVtbnN8IGZpbHRlckNvbHVtbiA7IGxldCBjb2x1bW5JbmRleCA9IGluZGV4IFwiIChjbGljayk9XCJkb1NvcnQoY29sdW1uKVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImhlYWRlci1jb250YWluZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57e2NvbHVtbi52YWx1ZX19PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpICpuZ0lmPVwiaXNTb3J0QXNjKGNvbHVtbilcIiBjbGFzcz1cImZhIGZhLXNvcnQtYW1vdW50LWFzY1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSAqbmdJZj1cImlzU29ydERlc2MoY29sdW1uKVwiIGNsYXNzPVwiZmEgZmEtc29ydC1hbW91bnQtZGVzY1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IG5nYkRyb3Bkb3duIGRpc3BsYXk9XCJkeW5hbWljXCIgKm5nSWY9XCJjb2x1bW4uZmlsdGVyTGlzdC5sZW5ndGhcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBuZ2JEcm9wZG93blRvZ2dsZSBjbGFzcz1cImZpbHRlci1pY29uXCIgc3JjPVwiLi9hc3NldHMvaWNvbnMvZmlsdGVyLnN2Z1wiIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgbmdiRHJvcGRvd25NZW51IGNsYXNzPVwiZHJvcGRvd24tbWVudSBkcm9wZG93bi1tZW51LXJpZ2h0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGFibGUtZHJvcGRvd24taXRlbVwiICpuZ0Zvcj1cImxldCBoZWFkZXIgb2YgY29sdW1uLmZpbHRlckxpc3Q7IGxldCBpID0gaW5kZXhcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJzdHlsZWQtY2hlY2tib3hcIiBpZD1cInN0eWxlZC1jaGVja2JveC17e2l9fS17e2NvbHVtbkluZGV4fX1cIiB0eXBlPVwiY2hlY2tib3hcIiB2YWx1ZT1cInt7aGVhZGVyLnZhbHVlfX1cIiBbKG5nTW9kZWwpXT1cImhlYWRlci5jaGVja2VkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cInN0eWxlZC1jaGVja2JveC17e2l9fS17e2NvbHVtbkluZGV4fX1cIj57e2hlYWRlci50aXRsZX19PC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRhYmxlLWRyb3Bkb3duLWJ0bS13cmFwcGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cInRhYmxlLWRyb3Bkb3duLWJ0blwiIChjbGljayk9XCJmaWx0ZXJPblRhYmxlQ29sdW1uKGNvbHVtbi5maWx0ZXJMaXN0LCBjb2x1bW4ubmFtZSlcIj57e1RhYmxlbGFuZ3VhZ2VPYmoucnRzX2Ryb3Bkb3duX2ZpbHRlcn19PC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cInRhYmxlLWRyb3Bkb3duLWJ0blwiIChjbGljayk9XCJmaWx0ZXJPblRhYmxlQ29sdW1uUmVzZXQoY29sdW1uLmZpbHRlckxpc3QpXCI+e3tUYWJsZWxhbmd1YWdlT2JqLnJ0c19yZXNldH19PC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBuZ2JEcm9wZG93biBkaXNwbGF5PVwiZHluYW1pY1wiICpuZ0lmPVwiY29sdW1uLnNlYXJjaGFibGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBpZD1cImRyb3Bkb3duRm9ybTFcIiBuZ2JEcm9wZG93blRvZ2dsZSBjbGFzcz1cInNlYXJjaC1pY29uXCIgc3JjPVwiLi9hc3NldHMvaWNvbnMvc2VhcmNoLWZpbHRlci5zdmdcIiAvPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgbmdiRHJvcGRvd25NZW51IGNsYXNzPVwiZHJvcGRvd24tbWVudSBkcm9wZG93bi1tZW51LXJpZ2h0XCIgYXJpYS1sYWJlbGxlZGJ5PVwiZHJvcGRvd25Gb3JtMVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzPVwidGFibGUtZHJvcGRvd24tc2VhcmNoLWlucHV0XCIgWyhuZ01vZGVsKV09XCJjb2x1bW4uc2VhcmNoU3RyXCIgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0YWJsZS1kcm9wZG93bi1idG0td3JhcHBlclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJ0YWJsZS1kcm9wZG93bi1idG5cIiAoY2xpY2spPVwidGFibGVEcm9wZG93blNlYXJjaChjb2x1bW4uc2VhcmNoU3RyLCBjb2x1bW4ubmFtZSlcIj57e1RhYmxlbGFuZ3VhZ2VPYmoucnRzX2Ryb3Bkb3duX3NlYXJjaH19PC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cInRhYmxlLWRyb3Bkb3duLWJ0blwiIChjbGljayk9XCJ0YWJsZURyb3Bkb3duU2VhcmNoUmVzZXQoY29sdW1uLnNlYXJjaFN0cilcIj57e1RhYmxlbGFuZ3VhZ2VPYmoucnRzX3Jlc2V0fX08L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L3RoPlxuICAgICAgICAgICAgICAgIDwvdHI+XG4gICAgICAgICAgICA8L3RoZWFkPlxuICAgICAgICAgICAgPHRib2R5IGlkPVwic29ydGFibGVcIj5cbiAgICAgICAgICAgICAgICA8bmctY29udGFpbmVyICpuZ0Zvcj1cImxldCByb3cgb2YgdGFibGVPcHRpb24udGFibGVSb3dzOyBsZXQgaSA9IGluZGV4XCI+XG4gICAgICAgICAgICAgICAgICAgIDx0ciBpZD1cInt7J3RhYmxlLXJvdy0nICsgaX19XCIgW25nQ2xhc3NdPVwicm93U29ydGFibGU/IHJvdy5jc3MgKyAnc29ydGFibGUtcm93Jzogcm93LmNzc1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHRkIGNsYXNzPVwidGFibGVyb3dcIiAqbmdGb3I9XCJsZXQgY2VsbCBvZiByb3cudGFibGVDZWxsc1wiIFtuZ0NsYXNzXT1cImNlbGwudHlwZSArICcgJyArIGNlbGwuY3NzIFwiIFtuZ1N3aXRjaF09XCJjZWxsLnR5cGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAqbmdTd2l0Y2hDYXNlPWNlbGxUeXBlLklNQUdFX1RIVU1CTkFJTCBjbGFzcz1cInRodW1ibmFpbC13cmFwcGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgKGNsaWNrKT1cImRvT3BlblByZXZpZXcocm93LnRhYmxlQ2VsbHNbMV0udmFsdWUpXCIgd2lkdGg9XCI2MHB4XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtzcmNdPVwiZG93bmxvYWRGaWxlUGF0aCArIGNlbGwudmFsdWUgKyAnX3RodW1iJ1wiIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuICpuZ1N3aXRjaENhc2U9Y2VsbFR5cGUuSU5QVVQgY2xhc3M9XCJjZWxsLWlucHV0LXdyYXBwZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3tjZWxsLm9yaWdpbmFsVmFsdWV9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJjZWxsLWlucHV0XCIgWyhuZ01vZGVsKV09XCJjZWxsLnZhbHVlXCIgaWQ9XCJ7eydpbnB1dC0nICsgaX19XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtkaXNhYmxlZF09XCIhY2VsbC5lbmFibGVkIHx8ICFyb3dTb3J0YWJsZVwiIChmb2N1c2luKT1cImNlbGwub2xkVmFsdWUgPSBjZWxsLnZhbHVlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChmb2N1c291dCk9XCJkb1VwZGF0ZUlucHV0RXZlbnQocm93LCBjZWxsLmVuYWJsZWQsIGNlbGwudmFsdWUsIGNlbGwub2xkVmFsdWUpOyBjZWxsLmVuYWJsZWQgPSBmYWxzZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoa2V5dXAuZW50ZXIpPVwiY2VsbC5lbmFibGVkID0gZmFsc2U7IGRvVXBkYXRlSW5wdXRFdmVudChyb3csIHRydWUsIGNlbGwsIGNlbGwub2xkVmFsdWUpXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gKGNsaWNrKT1cImNlbGwuZW5hYmxlZCA9IHRydWU7IGZvY3VzSW5wdXQoJ2lucHV0LScgKyBpKTtcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCIhY2VsbC5lbmFibGVkICYmIHJvd1NvcnRhYmxlXCIgY2xhc3M9XCJidG4gYnRuLWxpbmsgYnRuLWFjdGlvbiBjZWxsLWVkaXQtaWNvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1wZW5jaWxcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZGFuZ2VyLXRleHRcIiAqbmdJZj1cImNlbGwudmFsdWUubGVuZ3RoPmlucHV0TWF4TGVuZ2h0XCIgdHJhbnNsYXRlPm1heGxlbmd0aFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gdGl0bGU9XCJ7e2NlbGwudmFsdWV9fVwiICpuZ1N3aXRjaERlZmF1bHQgKGNsaWNrKT1cImNlbGxDbGlja2VkKGNlbGwpXCIgW25nQ2xhc3NdPVwiY2VsbC5yb3V0ZUxpbmsudHJpbSgpLmxlbmd0aCA+IDAgPyAnaGFuZCcgOiAnJ1wiPnt7Y2VsbC52YWx1ZX19PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIHRpdGxlPVwie3tjZWxsLnZhbHVlIH19XCIgKm5nU3dpdGNoQ2FzZT1jZWxsVHlwZS5TVEFUVVMgY2xhc3M9XCJzdGF0dXNcIiBbbmdDbGFzc109XCJjZWxsLnZhbHVlXCI+e3tjZWxsLnZhbHVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIHRpdGxlPVwie3tjZWxsLnZhbHVlICB8IGRhdGU6ZGF0ZUZvcm1hdH19XCIgKm5nU3dpdGNoQ2FzZT1jZWxsVHlwZS5EQVRFX1RJTUU+e3tjZWxsLnZhbHVlIHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZTpkYXRlRm9ybWF0fX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gKm5nU3dpdGNoQ2FzZT1jZWxsVHlwZS5BQ1RJT04+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuICpuZ0Zvcj1cImxldCBidXR0b25BY3Rpb24gb2YgY2VsbC5idXR0b25BY3Rpb25zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIFtkaXNhYmxlZF09XCJidXR0b25BY3Rpb24uZGlzYWJsZWRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwiYnV0dG9uQWN0aW9uLmJ1dHRvbkFjdGlvbihyb3cpXCIgdGl0bGU9XCJ7e2J1dHRvbkFjdGlvbi5uYW1lIH19XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCJidXR0b25BY3Rpb24uaWNvblVybFwiIGNsYXNzPVwiYnRuIGJ0bi1saW5rIGJ0bi1hY3Rpb24gaWNvbi1idG5cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBbc3JjXT1cImJ1dHRvbkFjdGlvbi5pY29uVXJsXCIgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uICpuZ0lmPVwiIWJ1dHRvbkFjdGlvbi5pY29uVXJsXCIgW2Rpc2FibGVkXT1cImJ1dHRvbkFjdGlvbi5kaXNhYmxlZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBidG4tbGlnaHQgYnRuLWFjdGlvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtuZ0NsYXNzXT0nXCJidG4tXCIgKyBidXR0b25BY3Rpb24ubmFtZSdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cImJ1dHRvbkFjdGlvbi5idXR0b25BY3Rpb24ocm93KVwiPnt7YnV0dG9uQWN0aW9uLm5hbWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fTwvYnV0dG9uPjwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiAoY2xpY2spPVwiZXhwYW5kKCd0YWJsZS1yb3ctZXh0cmEnICsgaSwgJGV2ZW50KVwiICpuZ0lmPVwicm93LmV4cGFuZGVkVGV4dFwiIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0biBleHBhbmQtYnRuXCI+PGkgY2xhc3M9XCJmYSBmYS1jYXJldC1kb3duIGFycm93XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbbmdDbGFzc109XCIoJ3RhYmxlLXJvdy1leHRyYScgKyBpID09IGV4cGFuZGVkUm93SWQpPyAnYXJyb3ctcm90YXRlJzogJycgXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+PC9idXR0b24+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYnRuLWdyb3VwIG1yLTNcIiAqbmdJZj1cImNlbGwuYnV0dG9uRHJvcGRvd25BY3Rpb25zLmxlbmd0aFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYnRuLWdyb3VwIHRhYmxlLWRyb3Bkb3duXCIgbmdiRHJvcGRvd24gcGxhY2VtZW50PVwiYm90dG9tLXJpZ2h0XCIgcm9sZT1cImdyb3VwXCIgYXJpYS1sYWJlbD1cIkJ1dHRvbiBncm91cCB3aXRoIG5lc3RlZCBkcm9wZG93blwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0blwiIG5nYkRyb3Bkb3duVG9nZ2xlPjxpIGNsYXNzPVwiZmEgZmEtZWxsaXBzaXMtdlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+PC9idXR0b24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tbWVudVwiIG5nYkRyb3Bkb3duTWVudT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiAqbmdGb3I9XCJsZXQgYnV0dG9uQWN0aW9uIG9mIGNlbGwuYnV0dG9uRHJvcGRvd25BY3Rpb25zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIG5nYkRyb3Bkb3duSXRlbSBbZGlzYWJsZWRdPVwiYnV0dG9uQWN0aW9uLmRpc2FibGVkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwiYnV0dG9uQWN0aW9uLmRpc2FibGVkIT10cnVlP2J1dHRvbkFjdGlvbi5idXR0b25BY3Rpb24ocm93KTonJ1wiPiB7e2J1dHRvbkFjdGlvbi5uYW1lXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fTwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDx0ZCBjbGFzcz1cImhhbmRsZVwiICpuZ0lmPVwicm93U29ydGFibGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cImZhIGZhLXNvcnRcIj48L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L3RkPlxuICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgICAgICA8dHIgY2xhc3M9XCJleHRyYS1yb3dcIiAqbmdJZj1cIiFyb3dTb3J0YWJsZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHRkIFthdHRyLmNvbHNwYW5dPVwicm93LnRhYmxlQ2VsbHMubGVuZ3RoXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cInt7J3RhYmxlLXJvdy1leHRyYScgKyBpfX1cIiBjbGFzcz1cImV4dHJhLXdyYXBwZXJcIiBzdHlsZT1cImRpc3BsYXk6IG5vbmU7XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgW2lubmVySFRNTF09XCJyb3cuZXhwYW5kZWRUZXh0XCI+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L3RkPlxuICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxuXG4gICAgICAgICAgICA8L3Rib2R5PlxuICAgICAgICAgICAgPHRmb290ICpuZ0lmPVwibm9EYXRhRm91bmQoKSAmJiB0YWJsZVR5cGUgPT09ICdXSU4nXCI+XG4gICAgICAgICAgICAgICAgPHRyPlxuICAgICAgICAgICAgICAgICAgICA8dGQgW2F0dHIuY29sc3Bhbl09XCJ0YWJsZU9wdGlvbi50YWJsZUNvbHVtbnMubGVuZ3RoXCI+e3tUYWJsZWxhbmd1YWdlT2JqLm5vX2RhdGFfZm91bmR9fTwvdGQ+XG4gICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgIDwvdGZvb3Q+XG4gICAgICAgIDwvdGFibGU+XG4gICAgPC9kaXY+XG5cbiAgICA8ZGl2IGNsYXNzPVwicGFnaW5nXCIgKm5nSWY9XCJ0YWJsZVR5cGUgPT09ICdSVFMnXCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJwYWdpbmctY29udGFpbmVyXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicGFnaW5nLXdyYXBwZXJcIiAqbmdJZj1cIiFub0RhdGFGb3VuZCgpXCI+XG4gICAgICAgICAgICAgICAgPG5nYi1wYWdpbmF0aW9uIGNsYXNzPVwiZC1mbGV4IGp1c3RpZnktY29udGVudC1lbmRcIiBzaXplPVwibWRcIiBbY29sbGVjdGlvblNpemVdPVwidGFibGVPcHRpb24udG90YWxJdGVtc1wiIFtyb3RhdGVdPVwidHJ1ZVwiIFttYXhTaXplXT1cIjVcIiBbcGFnZVNpemVdPVwidGFibGVPcHRpb24uaXRlbXNQZXJQYWdlXCIgWyhwYWdlKV09XCJ0YWJsZU9wdGlvbi5jdXJyZW50UGFnZVwiIChwYWdlQ2hhbmdlKT1cImRvUGFnZUNoYW5nZSgpXCI+XG4gICAgICAgICAgICAgICAgICAgIDxuZy10ZW1wbGF0ZSBuZ2JQYWdpbmF0aW9uUHJldmlvdXM+PGltZyBjbGFzcz1cImhhbmRsZS1pY29uXCIgc3JjPVwiLi9hc3NldHMvaWNvbnMvYmFjay5zdmdcIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxuICAgICAgICAgICAgICAgICAgICA8bmctdGVtcGxhdGUgbmdiUGFnaW5hdGlvbk5leHQ+PGltZyBjbGFzcz1cImhhbmRsZS1pY29uXCIgc3JjPVwiLi9hc3NldHMvaWNvbnMvbmV4dC5zdmdcIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L25nLXRlbXBsYXRlPlxuICAgICAgICAgICAgICAgIDwvbmdiLXBhZ2luYXRpb24+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG5cbiAgICA8ZGl2IGNsYXNzPVwid2luLXRhYmxlLWZvb3RlclwiICpuZ0lmPVwiIW5vRGF0YUZvdW5kKCkgJiYgdGFibGVUeXBlID09PSAnV0lOJ1wiPlxuICAgICAgICB7e1RhYmxlbGFuZ3VhZ2VPYmoudGFibGVfZm9vdGVyX3Nob3dpbmcgfX1cbiAgICAgICAgPHNwYW4gY2xhc3M9XCJudW1iZXJcIj57e3N0YXJ0SXRlbU51bWJlcigpfX08L3NwYW4+IHt7VGFibGVsYW5ndWFnZU9iai50YWJsZV90b319XG4gICAgICAgIDxzcGFuIGNsYXNzPVwibnVtYmVyXCI+e3tlbmRJdGVtTnVtYmVyKCl9fTwvc3Bhbj4ge3tUYWJsZWxhbmd1YWdlT2JqLnRhYmxlX29mfX1cbiAgICAgICAgPHNwYW4gY2xhc3M9XCJ0b3RhbFwiPnt7dGFibGVPcHRpb24udG90YWxJdGVtc319IHt7VGFibGVsYW5ndWFnZU9iai50YWJsZV9lbGVtZW50cyB9fTwvc3Bhbj5cbiAgICA8L2Rpdj5cbjwvZGl2PiJdfQ==