export class TableRow {
    constructor(tableCells = [], css = "", expandedText = "") {
        this._tableCells = tableCells;
        this._css = css;
        this._expandedText = expandedText;
        this._editAble = false;
        this._activeAble = false;
        this._deactiveAble = false;
        this._deleteAble = false;
    }
    /**
     * Getter tableCells
     * return {TableCell[]}
     */
    get tableCells() {
        return this._tableCells;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter editAble
     * return {boolean}
     */
    get editAble() {
        return this._editAble;
    }
    /**
     * Getter deleteAble
     * return {boolean}
     */
    get deleteAble() {
        return this._deleteAble;
    }
    /**
     * Getter deactiveAble
     * return {boolean}
     */
    get deactiveAble() {
        return this._deactiveAble;
    }
    /**
     * Getter activeAble
     * return {boolean}
     */
    get activeAble() {
        return this._activeAble;
    }
    /**
     * Getter expandedText
     * return {string}
     */
    get expandedText() {
        return this._expandedText;
    }
    /**
     * Setter tableCells
     * param {TableCell[]} value
     */
    set tableCells(value) {
        this._tableCells = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter editAble
     * param {boolean} value
     */
    set editAble(value) {
        this._editAble = value;
    }
    /**
     * Setter deleteAble
     * param {boolean} value
     */
    set deleteAble(value) {
        this._deleteAble = value;
    }
    /**
     * Setter deactiveAble
     * param {boolean} value
     */
    set deactiveAble(value) {
        this._deactiveAble = value;
    }
    /**
     * Setter activeAble
     * param {boolean} value
     */
    set activeAble(value) {
        this._activeAble = value;
    }
    /**
     * Setter expandedText
     * param {string} value
     */
    set expandedText(value) {
        this._expandedText = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUucm93LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdGFibGUvdGFibGUucm93LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sT0FBTyxRQUFRO0lBU2pCLFlBQVksYUFBMEIsRUFBRSxFQUFFLE1BQWMsRUFBRSxFQUFFLGVBQXVCLEVBQUU7UUFDakYsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUM7UUFDOUIsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7UUFDaEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUM7UUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsVUFBVTtRQUNqQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsR0FBRztRQUNWLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxRQUFRO1FBQ2YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFVBQVU7UUFDakIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFlBQVk7UUFDbkIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFVBQVU7UUFDakIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFlBQVk7UUFDbkIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFVBQVUsQ0FBQyxLQUFrQjtRQUNwQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxHQUFHLENBQUMsS0FBYTtRQUN4QixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxRQUFRLENBQUMsS0FBYztRQUM5QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxVQUFVLENBQUMsS0FBYztRQUNoQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxZQUFZLENBQUMsS0FBYztRQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxVQUFVLENBQUMsS0FBYztRQUNoQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxZQUFZLENBQUMsS0FBYTtRQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0NBRUoiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUYWJsZUNlbGwgfSBmcm9tIFwiLi90YWJsZS5jZWxsXCI7XG5cbmV4cG9ydCBjbGFzcyBUYWJsZVJvdyB7XG4gICAgcHJpdmF0ZSBfdGFibGVDZWxsczogVGFibGVDZWxsW107XG4gICAgcHJpdmF0ZSBfY3NzOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfZWRpdEFibGU6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBfZGVsZXRlQWJsZTogYm9vbGVhbjtcbiAgICBwcml2YXRlIF9kZWFjdGl2ZUFibGU6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBfYWN0aXZlQWJsZTogYm9vbGVhbjtcbiAgICBwcml2YXRlIF9leHBhbmRlZFRleHQ6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKHRhYmxlQ2VsbHM6IFRhYmxlQ2VsbFtdID0gW10sIGNzczogc3RyaW5nID0gXCJcIiwgZXhwYW5kZWRUZXh0OiBzdHJpbmcgPSBcIlwiKSB7XG4gICAgICAgIHRoaXMuX3RhYmxlQ2VsbHMgPSB0YWJsZUNlbGxzO1xuICAgICAgICB0aGlzLl9jc3MgPSBjc3M7XG4gICAgICAgIHRoaXMuX2V4cGFuZGVkVGV4dCA9IGV4cGFuZGVkVGV4dDtcbiAgICAgICAgdGhpcy5fZWRpdEFibGUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5fYWN0aXZlQWJsZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9kZWFjdGl2ZUFibGUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5fZGVsZXRlQWJsZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciB0YWJsZUNlbGxzXG4gICAgICogcmV0dXJuIHtUYWJsZUNlbGxbXX1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IHRhYmxlQ2VsbHMoKTogVGFibGVDZWxsW10ge1xuICAgICAgICByZXR1cm4gdGhpcy5fdGFibGVDZWxscztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgY3NzXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgcHVibGljIGdldCBjc3MoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NzcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgZWRpdEFibGVcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIGdldCBlZGl0QWJsZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2VkaXRBYmxlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBkZWxldGVBYmxlXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgZGVsZXRlQWJsZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RlbGV0ZUFibGU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGRlYWN0aXZlQWJsZVxuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGRlYWN0aXZlQWJsZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RlYWN0aXZlQWJsZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgYWN0aXZlQWJsZVxuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGFjdGl2ZUFibGUoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9hY3RpdmVBYmxlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBleHBhbmRlZFRleHRcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGV4cGFuZGVkVGV4dCgpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gdGhpcy5fZXhwYW5kZWRUZXh0O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB0YWJsZUNlbGxzXG4gICAgICogcGFyYW0ge1RhYmxlQ2VsbFtdfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgdGFibGVDZWxscyh2YWx1ZTogVGFibGVDZWxsW10pIHtcbiAgICAgICAgdGhpcy5fdGFibGVDZWxscyA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBjc3NcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgY3NzKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fY3NzID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGVkaXRBYmxlXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBlZGl0QWJsZSh2YWx1ZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9lZGl0QWJsZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBkZWxldGVBYmxlXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBkZWxldGVBYmxlKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2RlbGV0ZUFibGUgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgZGVhY3RpdmVBYmxlXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBkZWFjdGl2ZUFibGUodmFsdWU6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5fZGVhY3RpdmVBYmxlID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGFjdGl2ZUFibGVcbiAgICAgKiBwYXJhbSB7Ym9vbGVhbn0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IGFjdGl2ZUFibGUodmFsdWU6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5fYWN0aXZlQWJsZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBleHBhbmRlZFRleHRcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgZXhwYW5kZWRUZXh0KHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fZXhwYW5kZWRUZXh0ID0gdmFsdWU7XG4gICAgfVxuXG59Il19