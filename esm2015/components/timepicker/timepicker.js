import { Component, Input, Output, EventEmitter } from "@angular/core";
import * as i0 from "@angular/core";
import * as i1 from "@ng-bootstrap/ng-bootstrap";
import * as i2 from "@angular/forms";
export class TimepickerComponent {
    constructor() {
        this.showSeccond = false;
        this.showMeridian = false;
        this.timepickerModelChange = new EventEmitter();
        this.disabled = false;
        this.spinners = false;
    }
    ngOnInit() {
    }
    getDateSelected() {
        // if(this.objectUtils.isValid(this.timepickerItem)){
        //   return new Date(this.timepickerItem.year, this.timepickerItem.month - 1, this.timepickerItem.day);
        // }
        // return null;
    }
    timeSelect(time) {
        this.timepickerModelChange.emit(time);
    }
}
TimepickerComponent.ɵfac = function TimepickerComponent_Factory(t) { return new (t || TimepickerComponent)(); };
TimepickerComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TimepickerComponent, selectors: [["via-timepicker"]], inputs: { timepickerModel: "timepickerModel", showSeccond: "showSeccond", showMeridian: "showMeridian", disabled: "disabled" }, outputs: { timepickerModelChange: "timepickerModelChange" }, decls: 2, vars: 5, consts: [["id", "win-timepicker"], [3, "readonlyInputs", "ngModel", "meridian", "seconds", "spinners", "ngModelChange"]], template: function TimepickerComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "ngb-timepicker", 1);
        i0.ɵɵlistener("ngModelChange", function TimepickerComponent_Template_ngb_timepicker_ngModelChange_1_listener($event) { return ctx.timepickerModel = $event; })("ngModelChange", function TimepickerComponent_Template_ngb_timepicker_ngModelChange_1_listener($event) { return ctx.timeSelect($event); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("readonlyInputs", ctx.disabled)("ngModel", ctx.timepickerModel)("meridian", ctx.showMeridian)("seconds", ctx.showSeccond)("spinners", ctx.spinners);
    } }, directives: [i1.NgbTimepicker, i2.NgControlStatus, i2.NgModel], styles: ["#win-datepicker[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:column;width:100%}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:5px 10px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]{align-items:center;display:flex;flex:1;justify-content:baseline}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{margin-right:5px;width:20px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{flex:8}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span.dropdown-arrow[_ngcontent-%COMP%]{flex:1;font-size:24px;text-align:right}#win-datepicker[_ngcontent-%COMP%]   .input-datepicker[_ngcontent-%COMP%]{font-size:0;height:0;line-height:0;margin:0;padding:0;visibility:hidden;width:100%}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TimepickerComponent, [{
        type: Component,
        args: [{
                selector: "via-timepicker",
                templateUrl: "./timepicker.html",
                styleUrls: ["./timepicker.scss"]
            }]
    }], function () { return []; }, { timepickerModel: [{
            type: Input
        }], showSeccond: [{
            type: Input
        }], showMeridian: [{
            type: Input
        }], timepickerModelChange: [{
            type: Output
        }], disabled: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXBpY2tlci5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL3RpbWVwaWNrZXIvdGltZXBpY2tlci50cyIsImNvbXBvbmVudHMvdGltZXBpY2tlci90aW1lcGlja2VyLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxNQUFNLGVBQWUsQ0FBQzs7OztBQVEvRSxNQUFNLE9BQU8sbUJBQW1CO0lBUTlCO1FBTlMsZ0JBQVcsR0FBWSxLQUFLLENBQUM7UUFDN0IsaUJBQVksR0FBWSxLQUFLLENBQUM7UUFDN0IsMEJBQXFCLEdBQWdDLElBQUksWUFBWSxFQUFpQixDQUFDO1FBQ3pGLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFDbEMsYUFBUSxHQUFHLEtBQUssQ0FBQztJQUdqQixDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7SUFFRCxlQUFlO1FBQ2IscURBQXFEO1FBQ3JELHVHQUF1RztRQUN2RyxJQUFJO1FBQ0osZUFBZTtJQUNqQixDQUFDO0lBRUQsVUFBVSxDQUFDLElBQW1CO1FBQzVCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7c0ZBdkJVLG1CQUFtQjt3REFBbkIsbUJBQW1CO1FDUmhDLDhCQUNJO1FBQUEseUNBQXdNO1FBQTVKLDhKQUE2QixpSEFBNEMsc0JBQWtCLElBQTlEO1FBQThHLGlCQUFpQjtRQUM1TSxpQkFBTTs7UUFEYyxlQUEyQjtRQUEzQiw2Q0FBMkIsZ0NBQUEsOEJBQUEsNEJBQUEsMEJBQUE7O2tERE9sQyxtQkFBbUI7Y0FML0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLFdBQVcsRUFBRSxtQkFBbUI7Z0JBQ2hDLFNBQVMsRUFBRSxDQUFDLG1CQUFtQixDQUFDO2FBQ2pDO3NDQUVVLGVBQWU7a0JBQXZCLEtBQUs7WUFDRyxXQUFXO2tCQUFuQixLQUFLO1lBQ0csWUFBWTtrQkFBcEIsS0FBSztZQUNJLHFCQUFxQjtrQkFBOUIsTUFBTTtZQUNDLFFBQVE7a0JBQWYsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTmdiVGltZVN0cnVjdCB9IGZyb20gXCJAbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcFwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwidmlhLXRpbWVwaWNrZXJcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi90aW1lcGlja2VyLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3RpbWVwaWNrZXIuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBUaW1lcGlja2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgdGltZXBpY2tlck1vZGVsOiBOZ2JUaW1lU3RydWN0O1xuICBASW5wdXQoKSBzaG93U2VjY29uZDogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSBzaG93TWVyaWRpYW46IGJvb2xlYW4gPSBmYWxzZTtcbiAgQE91dHB1dCgpIHRpbWVwaWNrZXJNb2RlbENoYW5nZTogRXZlbnRFbWl0dGVyPE5nYlRpbWVTdHJ1Y3Q+ID0gbmV3IEV2ZW50RW1pdHRlcjxOZ2JUaW1lU3RydWN0PigpO1xuICBASW5wdXQoKWRpc2FibGVkOiBib29sZWFuID0gZmFsc2U7XG4gIHNwaW5uZXJzID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHsgXG4gIH1cblxuICBnZXREYXRlU2VsZWN0ZWQoKTogdm9pZHtcbiAgICAvLyBpZih0aGlzLm9iamVjdFV0aWxzLmlzVmFsaWQodGhpcy50aW1lcGlja2VySXRlbSkpe1xuICAgIC8vICAgcmV0dXJuIG5ldyBEYXRlKHRoaXMudGltZXBpY2tlckl0ZW0ueWVhciwgdGhpcy50aW1lcGlja2VySXRlbS5tb250aCAtIDEsIHRoaXMudGltZXBpY2tlckl0ZW0uZGF5KTtcbiAgICAvLyB9XG4gICAgLy8gcmV0dXJuIG51bGw7XG4gIH1cblxuICB0aW1lU2VsZWN0KHRpbWU6IE5nYlRpbWVTdHJ1Y3QpOiB2b2lke1xuICAgIHRoaXMudGltZXBpY2tlck1vZGVsQ2hhbmdlLmVtaXQodGltZSk7XG4gIH1cbn1cbiIsIjxkaXYgaWQ9XCJ3aW4tdGltZXBpY2tlclwiPlxuICAgIDxuZ2ItdGltZXBpY2tlciBbcmVhZG9ubHlJbnB1dHNdPVwiZGlzYWJsZWRcIiBbKG5nTW9kZWwpXT1cInRpbWVwaWNrZXJNb2RlbFwiIFttZXJpZGlhbl09XCJzaG93TWVyaWRpYW5cIiAobmdNb2RlbENoYW5nZSk9XCJ0aW1lU2VsZWN0KCRldmVudClcIiBbc2Vjb25kc109XCJzaG93U2VjY29uZFwiIFtzcGlubmVyc109XCJzcGlubmVyc1wiPjwvbmdiLXRpbWVwaWNrZXI+XG48L2Rpdj4iXX0=