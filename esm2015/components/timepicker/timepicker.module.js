import { NgModule } from '@angular/core';
import { TimepickerComponent } from './timepicker';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import * as i0 from "@angular/core";
export class TimepickerModule {
}
TimepickerModule.ɵmod = i0.ɵɵdefineNgModule({ type: TimepickerModule });
TimepickerModule.ɵinj = i0.ɵɵdefineInjector({ factory: function TimepickerModule_Factory(t) { return new (t || TimepickerModule)(); }, imports: [[
            NgbModule,
            FormsModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(TimepickerModule, { declarations: [TimepickerComponent], imports: [NgbModule,
        FormsModule,
        ReactiveFormsModule], exports: [TimepickerComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TimepickerModule, [{
        type: NgModule,
        args: [{
                declarations: [TimepickerComponent],
                imports: [
                    NgbModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                exports: [TimepickerComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXBpY2tlci5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy90aW1lcGlja2VyL3RpbWVwaWNrZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ25ELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7O0FBV2xFLE1BQU0sT0FBTyxnQkFBZ0I7O29EQUFoQixnQkFBZ0I7K0dBQWhCLGdCQUFnQixrQkFQbEI7WUFDUCxTQUFTO1lBQ1QsV0FBVztZQUNYLG1CQUFtQjtTQUNwQjt3RkFHVSxnQkFBZ0IsbUJBUlosbUJBQW1CLGFBRWhDLFNBQVM7UUFDVCxXQUFXO1FBQ1gsbUJBQW1CLGFBRVgsbUJBQW1CO2tEQUVsQixnQkFBZ0I7Y0FUNUIsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLG1CQUFtQixDQUFDO2dCQUNuQyxPQUFPLEVBQUU7b0JBQ1AsU0FBUztvQkFDVCxXQUFXO29CQUNYLG1CQUFtQjtpQkFDcEI7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLENBQUM7YUFDL0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVGltZXBpY2tlckNvbXBvbmVudCB9IGZyb20gJy4vdGltZXBpY2tlcic7XG5pbXBvcnQgeyBOZ2JNb2R1bGUgfSBmcm9tIFwiQG5nLWJvb3RzdHJhcC9uZy1ib290c3RyYXBcIjtcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1RpbWVwaWNrZXJDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgTmdiTW9kdWxlLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW1RpbWVwaWNrZXJDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFRpbWVwaWNrZXJNb2R1bGUgeyB9XG4iXX0=