import { trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import * as i0 from "@angular/core";
import * as i1 from "ngx-toastr";
import * as i2 from "@angular/common";
const _c0 = ["custom-toast-component", ""];
function CustomToast_div_6_p_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "p", 6);
} if (rf & 2) {
    const item_r1 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵproperty("innerHtml", item_r1, i0.ɵɵsanitizeHtml);
} }
function CustomToast_div_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtemplate(1, CustomToast_div_6_p_1_Template, 1, 1, "p", 5);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r1 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", item_r1 != "undefined");
} }
export class CustomToast extends Toast {
    constructor(toastrService, toastPackage) {
        super(toastrService, toastPackage);
        this.toastrService = toastrService;
        this.toastPackage = toastPackage;
        this.listMessages = this.message.toString().split("||");
    }
}
CustomToast.ɵfac = function CustomToast_Factory(t) { return new (t || CustomToast)(i0.ɵɵdirectiveInject(i1.ToastrService), i0.ɵɵdirectiveInject(i1.ToastPackage)); };
CustomToast.ɵcmp = i0.ɵɵdefineComponent({ type: CustomToast, selectors: [["", "custom-toast-component", ""]], features: [i0.ɵɵInheritDefinitionFeature], attrs: _c0, decls: 7, vars: 2, consts: [[1, "win-alert"], [1, "fa", "fa-close", "action"], [1, "title"], [1, "detail"], [4, "ngFor", "ngForOf"], [3, "innerHtml", 4, "ngIf"], [3, "innerHtml"]], template: function CustomToast_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelement(1, "i", 1);
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelementStart(3, "span");
        i0.ɵɵtext(4);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "div", 3);
        i0.ɵɵtemplate(6, CustomToast_div_6_Template, 2, 1, "div", 4);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(4);
        i0.ɵɵtextInterpolate(ctx.title);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngForOf", ctx.listMessages);
    } }, directives: [i2.NgForOf, i2.NgIf], encapsulation: 2, data: { animation: [
            trigger('flyInOut', []),
        ] } });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(CustomToast, [{
        type: Component,
        args: [{
                selector: '[custom-toast-component]',
                templateUrl: "toast.html",
                animations: [
                    trigger('flyInOut', []),
                ],
                preserveWhitespaces: false
            }]
    }], function () { return [{ type: i1.ToastrService }, { type: i1.ToastPackage }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy90b2FzdC90b2FzdC50cyIsImNvbXBvbmVudHMvdG9hc3QvdG9hc3QuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsT0FBTyxFQUNSLE1BQU0scUJBQXFCLENBQUM7QUFDN0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxQyxPQUFPLEVBQUUsS0FBSyxFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUUsTUFBTSxZQUFZLENBQUM7Ozs7OztJQ0dwRCx1QkFBcUQ7OztJQUF2QixzREFBa0I7OztJQURwRCwyQkFDSTtJQUFBLDhEQUFpRDtJQUNyRCxpQkFBTTs7O0lBREMsZUFBeUI7SUFBekIsNkNBQXlCOztBRFF4QyxNQUFNLE9BQU8sV0FBWSxTQUFRLEtBQUs7SUFHcEMsWUFDWSxhQUE0QixFQUMvQixZQUEwQjtRQUVqQyxLQUFLLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBSHpCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQy9CLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBR2pDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUQsQ0FBQzs7c0VBVFUsV0FBVztnREFBWCxXQUFXO1FDZnhCLDhCQUNJO1FBQUEsdUJBQWtDO1FBQ2xDLDhCQUNJO1FBQUEsNEJBQU07UUFBQSxZQUFTO1FBQUEsaUJBQU87UUFDMUIsaUJBQU07UUFDTiw4QkFDSTtRQUFBLDREQUNJO1FBRVIsaUJBQU07UUFDVixpQkFBTTs7UUFQUSxlQUFTO1FBQVQsK0JBQVM7UUFHVixlQUFpQztRQUFqQywwQ0FBaUM7aUZESWhDO1lBQ1YsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7U0FDeEI7a0RBR1UsV0FBVztjQVJ2QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLDBCQUEwQjtnQkFDcEMsV0FBVyxFQUFFLFlBQVk7Z0JBQ3pCLFVBQVUsRUFBRTtvQkFDVixPQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQztpQkFDeEI7Z0JBQ0QsbUJBQW1CLEVBQUUsS0FBSzthQUMzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIHRyaWdnZXJcbn0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFRvYXN0LCBUb2FzdHJTZXJ2aWNlLCBUb2FzdFBhY2thZ2UgfSBmcm9tICduZ3gtdG9hc3RyJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdbY3VzdG9tLXRvYXN0LWNvbXBvbmVudF0nLFxuICB0ZW1wbGF0ZVVybDogXCJ0b2FzdC5odG1sXCIsXG4gIGFuaW1hdGlvbnM6IFtcbiAgICB0cmlnZ2VyKCdmbHlJbk91dCcsIFtdKSxcbiAgXSxcbiAgcHJlc2VydmVXaGl0ZXNwYWNlczogZmFsc2Vcbn0pXG5leHBvcnQgY2xhc3MgQ3VzdG9tVG9hc3QgZXh0ZW5kcyBUb2FzdCB7XG4gIC8vIGNvbnN0cnVjdG9yIGlzIG9ubHkgbmVjZXNzYXJ5IHdoZW4gbm90IHVzaW5nIEFvVFxuICBsaXN0TWVzc2FnZXM6IHN0cmluZ1tdO1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcm90ZWN0ZWQgdG9hc3RyU2VydmljZTogVG9hc3RyU2VydmljZSxcbiAgICBwdWJsaWMgdG9hc3RQYWNrYWdlOiBUb2FzdFBhY2thZ2UsXG4gICkge1xuICAgIHN1cGVyKHRvYXN0clNlcnZpY2UsIHRvYXN0UGFja2FnZSk7XG4gICAgdGhpcy5saXN0TWVzc2FnZXMgPSB0aGlzLm1lc3NhZ2UudG9TdHJpbmcoKS5zcGxpdChcInx8XCIpO1xuICB9XG59XG4iLCI8ZGl2IGNsYXNzPVwid2luLWFsZXJ0XCI+XG4gICAgPGkgY2xhc3M9XCJmYSBmYS1jbG9zZSBhY3Rpb25cIj48L2k+XG4gICAgPGRpdiBjbGFzcz1cInRpdGxlXCI+XG4gICAgICAgIDxzcGFuPnt7dGl0bGV9fTwvc3Bhbj5cbiAgICA8L2Rpdj5cbiAgICA8ZGl2IGNsYXNzPVwiZGV0YWlsXCI+XG4gICAgICAgIDxkaXYgKm5nRm9yPVwibGV0IGl0ZW0gb2YgbGlzdE1lc3NhZ2VzXCI+XG4gICAgICAgICAgICA8cCAqbmdJZj1cIml0ZW0hPSd1bmRlZmluZWQnXCIgIFtpbm5lckh0bWxdPVwiaXRlbVwiPjwvcD5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG48L2Rpdj4iXX0=