import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CustomToast } from './toast';
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
export class ToastModule {
}
ToastModule.ɵmod = i0.ɵɵdefineNgModule({ type: ToastModule });
ToastModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ToastModule_Factory(t) { return new (t || ToastModule)(); }, imports: [[
            FormsModule,
            ReactiveFormsModule,
            CommonModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ToastModule, { declarations: [CustomToast], imports: [FormsModule,
        ReactiveFormsModule,
        CommonModule], exports: [CustomToast] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ToastModule, [{
        type: NgModule,
        args: [{
                declarations: [CustomToast],
                imports: [
                    FormsModule,
                    ReactiveFormsModule,
                    CommonModule
                ],
                exports: [CustomToast]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9hc3QubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdG9hc3QvdG9hc3QubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFdBQVcsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDdEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDOztBQVcvQyxNQUFNLE9BQU8sV0FBVzs7K0NBQVgsV0FBVztxR0FBWCxXQUFXLGtCQVBiO1lBQ1AsV0FBVztZQUNYLG1CQUFtQjtZQUNuQixZQUFZO1NBQ2I7d0ZBR1UsV0FBVyxtQkFSUCxXQUFXLGFBRXhCLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsWUFBWSxhQUVKLFdBQVc7a0RBRVYsV0FBVztjQVR2QixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsV0FBVyxDQUFDO2dCQUMzQixPQUFPLEVBQUU7b0JBQ1AsV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLFlBQVk7aUJBQ2I7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsV0FBVyxDQUFDO2FBQ3ZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5pbXBvcnQgeyBDdXN0b21Ub2FzdCB9IGZyb20gJy4vdG9hc3QnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbQ3VzdG9tVG9hc3RdLFxuICBpbXBvcnRzOiBbXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICBDb21tb25Nb2R1bGUgXG4gIF0sXG4gIGV4cG9ydHM6IFtDdXN0b21Ub2FzdF1cbn0pXG5leHBvcnQgY2xhc3MgVG9hc3RNb2R1bGUgeyB9XG4iXX0=