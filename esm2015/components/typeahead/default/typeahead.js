import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormControl } from "@angular/forms";
import { TypeaheadOptionDto } from "../dto/typeahead.option.dto";
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { TypeAheadLanguageConfig } from "../constant";
import { TypeaheadItemDto } from "../dto/typeahead.item.dto";
import { random } from "lodash";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "../../click_out_site/click.out.site";
import * as i3 from "@angular/forms";
function TypeaheadComponent_label_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵattribute("for", ctx_r0.typeaheadId);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.options.lable);
} }
function TypeaheadComponent_span_6_li_1_span_3_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span", 14);
    i0.ɵɵlistener("click", function TypeaheadComponent_span_6_li_1_span_3_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r12); const item_r8 = i0.ɵɵnextContext().$implicit; const ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.remove(item_r8); });
    i0.ɵɵelement(1, "i", 15);
    i0.ɵɵelementEnd();
} }
function TypeaheadComponent_span_6_li_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 12);
    i0.ɵɵelementStart(1, "span", 13);
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(3, TypeaheadComponent_span_6_li_1_span_3_Template, 2, 0, "span", 9);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r8 = ctx.$implicit;
    const ctx_r7 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate("title", item_r8.name);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r8.name);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r7.disabled);
} }
function TypeaheadComponent_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtemplate(1, TypeaheadComponent_span_6_li_1_Template, 4, 3, "li", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r1.options.itemsSelected);
} }
function TypeaheadComponent_span_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.options.itemsSelected[0].name);
} }
function TypeaheadComponent_input_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "input", 17);
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵpropertyInterpolate("placeholder", ctx_r3.languageObj.enter_text);
    i0.ɵɵproperty("formControl", ctx_r3.searchBox);
    i0.ɵɵattribute("id", ctx_r3.typeaheadId);
} }
function TypeaheadComponent_span_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 18);
    i0.ɵɵelement(1, "i", 19);
    i0.ɵɵelementEnd();
} }
function TypeaheadComponent_span_11_Template(rf, ctx) { if (rf & 1) {
    const _r14 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span", 14);
    i0.ɵɵlistener("click", function TypeaheadComponent_span_11_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r14); const ctx_r13 = i0.ɵɵnextContext(); return ctx_r13.removeSingleItem(); });
    i0.ɵɵelement(1, "i", 15);
    i0.ɵɵelementEnd();
} }
function TypeaheadComponent_ul_12_li_1_Template(rf, ctx) { if (rf & 1) {
    const _r19 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "li", 23);
    i0.ɵɵlistener("click", function TypeaheadComponent_ul_12_li_1_Template_li_click_0_listener() { i0.ɵɵrestoreView(_r19); const item_r17 = ctx.$implicit; const ctx_r18 = i0.ɵɵnextContext(2); return ctx_r18.select(item_r17); });
    i0.ɵɵelementStart(1, "span");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r17 = ctx.$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(item_r17.name);
} }
function TypeaheadComponent_ul_12_li_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 24);
    i0.ɵɵelementStart(1, "span");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r16 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r16.languageObj.no_data_found);
} }
function TypeaheadComponent_ul_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "ul", 20);
    i0.ɵɵtemplate(1, TypeaheadComponent_ul_12_li_1_Template, 3, 1, "li", 21);
    i0.ɵɵtemplate(2, TypeaheadComponent_ul_12_li_2_Template, 3, 1, "li", 22);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r6.options.items);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r6.options.noDataFound);
} }
export class TypeaheadComponent {
    constructor() {
        this.languageObj = TypeAheadLanguageConfig;
        this.disabled = false;
        this.typeaheadLoadData = new EventEmitter();
        this.typeaheadItemSelected = new EventEmitter();
        this.searchBox = new FormControl();
        this.typeaheadId = "";
        //prevent duplicate search when set value for search box
        this.isSettingValue = false;
        //super();
        this.typeaheadId = "viaTypeahead" + random();
    }
    hide() {
        if (!this.options.keepHistory) {
            this.options.items = [];
        }
        this.options.errorMessage = "";
        this.options.isLoading = false;
        this.options.noDataFound = false;
        if (this.options.inlineSingleSearch == false) {
            //this.isSettingValue = true;
            this.searchBox.setValue("");
        }
        else if (this.isSettingValue == false && this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            //this.isSettingValue = true;
            //this.searchBox.setValue(this.options.itemsSelected[0].name);
        }
    }
    isSingleInlineSearch() {
        return this.options.inlineSingleSearch == true && this.options.multiple == false;
    }
    select(item) {
        this.options.items = this.options.items.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        if (!this.options.multiple) {
            this.options.itemsSelected = [];
        }
        this.options.itemsSelected.push(item);
        this.options.noDataFound = this.options.items.length === 0;
        this.typeaheadItemSelected.emit(item);
        if (this.isSingleInlineSearch() == true) {
            this.isSettingValue = true;
            this.searchBox.setValue(item.name);
        }
    }
    remove(item) {
        this.options.items.push(item);
        this.options.itemsSelected = this.options.itemsSelected.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        this.options.noDataFound = this.options.items.length === 0;
    }
    removeSingleItem() {
        var _a;
        this.isSettingValue = true;
        this.searchBox.setValue("");
        this.options.itemsSelected = [];
        (_a = this.typeaheadItemSelected) === null || _a === void 0 ? void 0 : _a.emit(new TypeaheadItemDto(undefined, undefined));
    }
    ngAfterViewInit() {
        if (this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            this.searchBox.setValue(this.options.itemsSelected[0].name);
            this.isSettingValue = true;
        }
        this.searchBox.valueChanges.pipe(debounceTime(this.options.debounceTime), distinctUntilChanged()).subscribe((searchText) => {
            var _a;
            if (searchText.length >= this.options.minLengthToLoad && this.isSettingValue == false) {
                this.options.isLoading = true;
                (_a = this.typeaheadLoadData) === null || _a === void 0 ? void 0 : _a.emit(searchText);
            }
            this.isSettingValue = false;
        });
    }
    setSearchBoxValue(text) {
        this.searchBox.setValue(text);
        this.isSettingValue = true;
    }
}
TypeaheadComponent.ɵfac = function TypeaheadComponent_Factory(t) { return new (t || TypeaheadComponent)(); };
TypeaheadComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TypeaheadComponent, selectors: [["via-typeahead"]], inputs: { languageObj: "languageObj", options: "options", disabled: "disabled" }, outputs: { typeaheadLoadData: "typeaheadLoadData", typeaheadItemSelected: "typeaheadItemSelected" }, decls: 13, vars: 7, consts: [[4, "ngIf"], [1, "via-typeahead", 3, "tohClickOutSite"], [1, "typeahead-container"], [1, "typeahead"], [1, "typeahead-items-selected"], ["class", "inline-disabled", 4, "ngIf"], [1, "typeahead-menu-item-input"], ["autocomplete", "off", "class", "typeahead-input", 3, "placeholder", "formControl", 4, "ngIf"], ["class", "ic-spinner", 4, "ngIf"], ["class", "ic-remove", 3, "click", 4, "ngIf"], ["class", "typeahead-menu-items z-depth-1", 4, "ngIf"], ["class", "typeahead-menu-item-selected", 4, "ngFor", "ngForOf"], [1, "typeahead-menu-item-selected"], [1, "item-display", 3, "title"], [1, "ic-remove", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-times"], [1, "inline-disabled"], ["autocomplete", "off", 1, "typeahead-input", 3, "placeholder", "formControl"], [1, "ic-spinner"], ["id", "circle-o-notch-spin", "aria-hidden", "true", 1, "fa", "fa-circle-o-notch", "fa-spin"], [1, "typeahead-menu-items", "z-depth-1"], ["class", "typeahead-menu-item", 3, "click", 4, "ngFor", "ngForOf"], ["class", "typeahead-menu-item", 4, "ngIf"], [1, "typeahead-menu-item", 3, "click"], [1, "typeahead-menu-item"]], template: function TypeaheadComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div");
        i0.ɵɵtemplate(1, TypeaheadComponent_label_1_Template, 2, 2, "label", 0);
        i0.ɵɵelementStart(2, "div", 1);
        i0.ɵɵlistener("tohClickOutSite", function TypeaheadComponent_Template_div_tohClickOutSite_2_listener() { return ctx.hide(); });
        i0.ɵɵelementStart(3, "ul", 2);
        i0.ɵɵelementStart(4, "li", 3);
        i0.ɵɵelementStart(5, "ul", 4);
        i0.ɵɵtemplate(6, TypeaheadComponent_span_6_Template, 2, 1, "span", 0);
        i0.ɵɵtemplate(7, TypeaheadComponent_span_7_Template, 2, 1, "span", 5);
        i0.ɵɵelementStart(8, "li", 6);
        i0.ɵɵtemplate(9, TypeaheadComponent_input_9_Template, 1, 3, "input", 7);
        i0.ɵɵtemplate(10, TypeaheadComponent_span_10_Template, 2, 0, "span", 8);
        i0.ɵɵtemplate(11, TypeaheadComponent_span_11_Template, 2, 0, "span", 9);
        i0.ɵɵtemplate(12, TypeaheadComponent_ul_12_Template, 3, 2, "ul", 10);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.lable && ctx.options.lable.length > 0);
        i0.ɵɵadvance(5);
        i0.ɵɵproperty("ngIf", !ctx.options.inlineSingleSearch);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.inlineSingleSearch && ctx.disabled == true && ctx.options.itemsSelected.length);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.disabled == false);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.isLoading);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.isSingleInlineSearch() && ctx.options.itemsSelected.length && ctx.options.isLoading == false && !ctx.disabled);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.items.length > 0 || ctx.options.noDataFound);
    } }, directives: [i1.NgIf, i2.ClickOutSiteDirective, i1.NgForOf, i3.DefaultValueAccessor, i3.NgControlStatus, i3.FormControlDirective], styles: [".via-typeahead[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:row;width:100%}.via-typeahead[_ngcontent-%COMP%]   .inline-disabled[_ngcontent-%COMP%]{padding-left:5px;padding-top:5px}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:1px 5px;z-index:1}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]{margin:0;width:100%}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;list-style:none;margin:5px 0;padding:0}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]{background:#999;border:1px solid #ebebeb;border-radius:3px;border-radius:5px;color:#fff;display:flex;flex-direction:row;margin:0 10px 0 0;max-width:200px;min-width:120px;padding:5px}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]   .item-display[_ngcontent-%COMP%]{flex:9;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:100%}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]   .ic-remove[_ngcontent-%COMP%]{flex:1;font-size:14px;text-align:right}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]{align-items:center;border:0;color:#999;display:flex;flex:1;height:34px;margin:0;width:auto}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]   .typeahead-input[_ngcontent-%COMP%]{border:0;flex:9}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]   .ic-spinner[_ngcontent-%COMP%]{flex:1;text-align:right}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:flex;flex-direction:column;left:15px;list-style:none;margin:0;max-height:300px;overflow:auto;padding:0;position:absolute;top:90%;width:calc(100% - 30px)}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]   .typeahead-menu-item[_ngcontent-%COMP%]{margin:0;padding:10px}.via-typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]   .typeahead-menu-item[_ngcontent-%COMP%]:hover{background:#f7f8f9}.via-typeahead[_ngcontent-%COMP%]   .typeahead-arrow[_ngcontent-%COMP%]{flex:1;text-align:right}.via-typeahead[_ngcontent-%COMP%]   .typeahead-arrow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#999;font-size:24px}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TypeaheadComponent, [{
        type: Component,
        args: [{
                selector: "via-typeahead",
                templateUrl: "./typeahead.html",
                styleUrls: ["./typeahead.scss"]
            }]
    }], function () { return []; }, { languageObj: [{
            type: Input
        }], options: [{
            type: Input
        }], disabled: [{
            type: Input
        }], typeaheadLoadData: [{
            type: Output
        }], typeaheadItemSelected: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy90eXBlYWhlYWQvZGVmYXVsdC90eXBlYWhlYWQudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3R5cGVhaGVhZC9kZWZhdWx0L3R5cGVhaGVhZC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxZQUFZLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDdEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDN0QsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLFFBQVEsQ0FBQzs7Ozs7O0lDSjlCLDZCQUFrRjtJQUFBLFlBQWlCO0lBQUEsaUJBQVE7OztJQUFwRyx5Q0FBd0I7SUFBbUQsZUFBaUI7SUFBakIsMENBQWlCOzs7O0lBUXZGLGdDQUNJO0lBRHNDLHNQQUFzQjtJQUM1RCx3QkFBOEM7SUFDbEQsaUJBQU87OztJQUpULDhCQUNFO0lBQUEsZ0NBQWlEO0lBQUEsWUFBYTtJQUFBLGlCQUFPO0lBQ3JFLGlGQUNJO0lBRU4saUJBQUs7Ozs7SUFKd0IsZUFBcUI7SUFBckIsK0NBQXFCO0lBQUMsZUFBYTtJQUFiLGtDQUFhO0lBQ3RDLGVBQWlCO0lBQWpCLHVDQUFpQjs7O0lBSDdDLDRCQUNFO0lBQUEseUVBQ0U7SUFLSixpQkFBTzs7O0lBTkQsZUFBMEM7SUFBMUMsc0RBQTBDOzs7SUFPaEQsZ0NBQW1IO0lBQUEsWUFBaUM7SUFBQSxpQkFBTzs7O0lBQXhDLGVBQWlDO0lBQWpDLDBEQUFpQzs7O0lBRWxKLDRCQUNBOzs7SUFEMEUsc0VBQXdDO0lBQUMsOENBQXlCO0lBQTdHLHdDQUF1Qjs7O0lBQ3RELGdDQUNJO0lBQUEsd0JBQXdGO0lBQzVGLGlCQUFPOzs7O0lBQ1AsZ0NBQ0k7SUFEaUgsK0xBQTRCO0lBQzdJLHdCQUE4QztJQUNsRCxpQkFBTzs7OztJQUVMLDhCQUNJO0lBRCtELCtOQUFzQjtJQUNyRiw0QkFBTTtJQUFBLFlBQWE7SUFBQSxpQkFBTztJQUM5QixpQkFBSzs7O0lBREssZUFBYTtJQUFiLG1DQUFhOzs7SUFFdkIsOEJBQ0k7SUFBQSw0QkFBTTtJQUFBLFlBQTZCO0lBQUEsaUJBQU87SUFDOUMsaUJBQUs7OztJQURLLGVBQTZCO0lBQTdCLHVEQUE2Qjs7O0lBTHpDLDhCQUNFO0lBQUEsd0VBQ0k7SUFFSix3RUFDSTtJQUVOLGlCQUFLOzs7SUFOQyxlQUFrQztJQUFsQyw4Q0FBa0M7SUFHbEMsZUFBMkI7SUFBM0IsaURBQTJCOztBRGY3QyxNQUFNLE9BQU8sa0JBQWtCO0lBWTdCO1FBWFMsZ0JBQVcsR0FBRyx1QkFBdUIsQ0FBQztRQUV0QyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBQ3pCLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVyRCxjQUFTLEdBQWdCLElBQUksV0FBVyxFQUFFLENBQUM7UUFDM0MsZ0JBQVcsR0FBVyxFQUFFLENBQUM7UUFFekIsd0RBQXdEO1FBQ2hELG1CQUFjLEdBQVksS0FBSyxDQUFDO1FBRXRDLFVBQVU7UUFDVixJQUFJLENBQUMsV0FBVyxHQUFHLGNBQWMsR0FBRyxNQUFNLEVBQUUsQ0FBQztJQUMvQyxDQUFDO0lBRUQsSUFBSTtRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRTtZQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7U0FDekI7UUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUNqQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLElBQUksS0FBSyxFQUFFO1lBQzVDLDZCQUE2QjtZQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUM3QjthQUFNLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRTtZQUNuSCw2QkFBNkI7WUFDN0IsOERBQThEO1NBQy9EO0lBQ0gsQ0FBQztJQUVELG9CQUFvQjtRQUNsQixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQztJQUNuRixDQUFDO0lBRUQsTUFBTSxDQUFDLElBQXNCO1FBQzNCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFO1lBQzVELE9BQU8sVUFBVSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFO1lBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztTQUNqQztRQUNELElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxJQUFJLEVBQUU7WUFDdkMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxJQUFzQjtRQUMzQixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDNUUsT0FBTyxVQUFVLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFFRCxnQkFBZ0I7O1FBQ2QsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ2hDLE1BQUEsSUFBSSxDQUFDLHFCQUFxQiwwQ0FBRSxJQUFJLENBQUMsSUFBSSxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLEVBQUU7SUFDL0UsQ0FBQztJQUVELGVBQWU7UUFDYixJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7U0FDNUI7UUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFOztZQUN6SCxJQUFJLFVBQVUsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxLQUFLLEVBQUU7Z0JBQ3JGLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDOUIsTUFBQSxJQUFJLENBQUMsaUJBQWlCLDBDQUFFLElBQUksQ0FBQyxVQUFVLEVBQUU7YUFDMUM7WUFDRCxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxJQUFZO1FBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO0lBQzdCLENBQUM7O29GQXRGVSxrQkFBa0I7dURBQWxCLGtCQUFrQjtRQ1ovQiwyQkFDRTtRQUFBLHVFQUFrRjtRQUNsRiw4QkFDRTtRQUR5QixnSEFBbUIsVUFBTSxJQUFDO1FBQ25ELDZCQUNFO1FBQUEsNkJBQ0U7UUFBQSw2QkFDRTtRQUFBLHFFQUNFO1FBT0YscUVBQW1IO1FBQ25ILDZCQUNFO1FBQUEsdUVBQ0E7UUFBQSx1RUFDSTtRQUVKLHVFQUNJO1FBRUosb0VBQ0U7UUFPSixpQkFBSztRQUNQLGlCQUFLO1FBQ1AsaUJBQUs7UUFDUCxpQkFBSztRQUNQLGlCQUFNO1FBQ1IsaUJBQU07O1FBbkM0QixlQUFpRDtRQUFqRCx3RUFBaUQ7UUFLbkUsZUFBbUM7UUFBbkMsc0RBQW1DO1FBUVgsZUFBb0Y7UUFBcEYsaUhBQW9GO1FBRXpHLGVBQXVCO1FBQXZCLDRDQUF1QjtRQUNMLGVBQXlCO1FBQXpCLDRDQUF5QjtRQUc1QyxlQUE4RztRQUE5Ryx3SUFBOEc7UUFHaEgsZUFBdUQ7UUFBdkQsOEVBQXVEOztrRERYMUQsa0JBQWtCO2NBTDlCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsV0FBVyxFQUFFLGtCQUFrQjtnQkFDL0IsU0FBUyxFQUFFLENBQUMsa0JBQWtCLENBQUM7YUFDaEM7c0NBRVUsV0FBVztrQkFBbkIsS0FBSztZQUNHLE9BQU87a0JBQWYsS0FBSztZQUNHLFFBQVE7a0JBQWhCLEtBQUs7WUFDSSxpQkFBaUI7a0JBQTFCLE1BQU07WUFDRyxxQkFBcUI7a0JBQTlCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgVHlwZWFoZWFkT3B0aW9uRHRvIH0gZnJvbSBcIi4uL2R0by90eXBlYWhlYWQub3B0aW9uLmR0b1wiO1xuaW1wb3J0IHsgZGVib3VuY2VUaW1lLCBkaXN0aW5jdFVudGlsQ2hhbmdlZCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IFR5cGVBaGVhZExhbmd1YWdlQ29uZmlnIH0gZnJvbSBcIi4uL2NvbnN0YW50XCI7XG5pbXBvcnQgeyBUeXBlYWhlYWRJdGVtRHRvIH0gZnJvbSBcIi4uL2R0by90eXBlYWhlYWQuaXRlbS5kdG9cIjtcbmltcG9ydCB7IHJhbmRvbSB9IGZyb20gXCJsb2Rhc2hcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10eXBlYWhlYWRcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi90eXBlYWhlYWQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vdHlwZWFoZWFkLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgVHlwZWFoZWFkQ29tcG9uZW50IHtcbiAgQElucHV0KCkgbGFuZ3VhZ2VPYmogPSBUeXBlQWhlYWRMYW5ndWFnZUNvbmZpZztcbiAgQElucHV0KCkgb3B0aW9uczogVHlwZWFoZWFkT3B0aW9uRHRvO1xuICBASW5wdXQoKSBkaXNhYmxlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBAT3V0cHV0KCkgdHlwZWFoZWFkTG9hZERhdGEgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSB0eXBlYWhlYWRJdGVtU2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgc2VhcmNoQm94OiBGb3JtQ29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgpO1xuICB0eXBlYWhlYWRJZDogc3RyaW5nID0gXCJcIjtcblxuICAvL3ByZXZlbnQgZHVwbGljYXRlIHNlYXJjaCB3aGVuIHNldCB2YWx1ZSBmb3Igc2VhcmNoIGJveFxuICBwcml2YXRlIGlzU2V0dGluZ1ZhbHVlOiBib29sZWFuID0gZmFsc2U7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIC8vc3VwZXIoKTtcbiAgICB0aGlzLnR5cGVhaGVhZElkID0gXCJ2aWFUeXBlYWhlYWRcIiArIHJhbmRvbSgpO1xuICB9XG4gIFxuICBoaWRlKCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5vcHRpb25zLmtlZXBIaXN0b3J5KSB7XG4gICAgICB0aGlzLm9wdGlvbnMuaXRlbXMgPSBbXTtcbiAgICB9XG4gICAgdGhpcy5vcHRpb25zLmVycm9yTWVzc2FnZSA9IFwiXCI7XG4gICAgdGhpcy5vcHRpb25zLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgIHRoaXMub3B0aW9ucy5ub0RhdGFGb3VuZCA9IGZhbHNlO1xuICAgIGlmICh0aGlzLm9wdGlvbnMuaW5saW5lU2luZ2xlU2VhcmNoID09IGZhbHNlKSB7XG4gICAgICAvL3RoaXMuaXNTZXR0aW5nVmFsdWUgPSB0cnVlO1xuICAgICAgdGhpcy5zZWFyY2hCb3guc2V0VmFsdWUoXCJcIik7XG4gICAgfSBlbHNlIGlmICh0aGlzLmlzU2V0dGluZ1ZhbHVlID09IGZhbHNlICYmIHRoaXMuaXNTaW5nbGVJbmxpbmVTZWFyY2goKSA9PSB0cnVlICYmIHRoaXMub3B0aW9ucy5pdGVtc1NlbGVjdGVkLmxlbmd0aCkge1xuICAgICAgLy90aGlzLmlzU2V0dGluZ1ZhbHVlID0gdHJ1ZTtcbiAgICAgIC8vdGhpcy5zZWFyY2hCb3guc2V0VmFsdWUodGhpcy5vcHRpb25zLml0ZW1zU2VsZWN0ZWRbMF0ubmFtZSk7XG4gICAgfVxuICB9XG5cbiAgaXNTaW5nbGVJbmxpbmVTZWFyY2goKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMub3B0aW9ucy5pbmxpbmVTaW5nbGVTZWFyY2ggPT0gdHJ1ZSAmJiB0aGlzLm9wdGlvbnMubXVsdGlwbGUgPT0gZmFsc2U7XG4gIH1cblxuICBzZWxlY3QoaXRlbTogVHlwZWFoZWFkSXRlbUR0byk6IHZvaWQge1xuICAgIHRoaXMub3B0aW9ucy5pdGVtcyA9IHRoaXMub3B0aW9ucy5pdGVtcy5maWx0ZXIoKGZpbHRlckl0ZW0pID0+IHtcbiAgICAgIHJldHVybiBmaWx0ZXJJdGVtLnZhbHVlICE9PSBpdGVtLnZhbHVlO1xuICAgIH0pO1xuICAgIGlmICghdGhpcy5vcHRpb25zLm11bHRpcGxlKSB7XG4gICAgICB0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZCA9IFtdO1xuICAgIH1cbiAgICB0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZC5wdXNoKGl0ZW0pO1xuICAgIHRoaXMub3B0aW9ucy5ub0RhdGFGb3VuZCA9IHRoaXMub3B0aW9ucy5pdGVtcy5sZW5ndGggPT09IDA7XG4gICAgdGhpcy50eXBlYWhlYWRJdGVtU2VsZWN0ZWQuZW1pdChpdGVtKTtcbiAgICBpZiAodGhpcy5pc1NpbmdsZUlubGluZVNlYXJjaCgpID09IHRydWUpIHtcbiAgICAgIHRoaXMuaXNTZXR0aW5nVmFsdWUgPSB0cnVlO1xuICAgICAgdGhpcy5zZWFyY2hCb3guc2V0VmFsdWUoaXRlbS5uYW1lKTtcbiAgICB9XG4gIH1cblxuICByZW1vdmUoaXRlbTogVHlwZWFoZWFkSXRlbUR0byk6IHZvaWQge1xuICAgIHRoaXMub3B0aW9ucy5pdGVtcy5wdXNoKGl0ZW0pO1xuICAgIHRoaXMub3B0aW9ucy5pdGVtc1NlbGVjdGVkID0gdGhpcy5vcHRpb25zLml0ZW1zU2VsZWN0ZWQuZmlsdGVyKChmaWx0ZXJJdGVtKSA9PiB7XG4gICAgICByZXR1cm4gZmlsdGVySXRlbS52YWx1ZSAhPT0gaXRlbS52YWx1ZTtcbiAgICB9KTtcbiAgICB0aGlzLm9wdGlvbnMubm9EYXRhRm91bmQgPSB0aGlzLm9wdGlvbnMuaXRlbXMubGVuZ3RoID09PSAwO1xuICB9XG5cbiAgcmVtb3ZlU2luZ2xlSXRlbSgpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2V0dGluZ1ZhbHVlID0gdHJ1ZTtcbiAgICB0aGlzLnNlYXJjaEJveC5zZXRWYWx1ZShcIlwiKTtcbiAgICB0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZCA9IFtdO1xuICAgIHRoaXMudHlwZWFoZWFkSXRlbVNlbGVjdGVkPy5lbWl0KG5ldyBUeXBlYWhlYWRJdGVtRHRvKHVuZGVmaW5lZCwgdW5kZWZpbmVkKSk7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgaWYgKHRoaXMuaXNTaW5nbGVJbmxpbmVTZWFyY2goKSA9PSB0cnVlICYmIHRoaXMub3B0aW9ucy5pdGVtc1NlbGVjdGVkLmxlbmd0aCkge1xuICAgICAgdGhpcy5zZWFyY2hCb3guc2V0VmFsdWUodGhpcy5vcHRpb25zLml0ZW1zU2VsZWN0ZWRbMF0ubmFtZSk7XG4gICAgICB0aGlzLmlzU2V0dGluZ1ZhbHVlID0gdHJ1ZTtcbiAgICB9XG5cbiAgICB0aGlzLnNlYXJjaEJveC52YWx1ZUNoYW5nZXMucGlwZShkZWJvdW5jZVRpbWUodGhpcy5vcHRpb25zLmRlYm91bmNlVGltZSksIGRpc3RpbmN0VW50aWxDaGFuZ2VkKCkpLnN1YnNjcmliZSgoc2VhcmNoVGV4dCkgPT4ge1xuICAgICAgaWYgKHNlYXJjaFRleHQubGVuZ3RoID49IHRoaXMub3B0aW9ucy5taW5MZW5ndGhUb0xvYWQgJiYgdGhpcy5pc1NldHRpbmdWYWx1ZSA9PSBmYWxzZSkge1xuICAgICAgICB0aGlzLm9wdGlvbnMuaXNMb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgdGhpcy50eXBlYWhlYWRMb2FkRGF0YT8uZW1pdChzZWFyY2hUZXh0KTtcbiAgICAgIH1cbiAgICAgIHRoaXMuaXNTZXR0aW5nVmFsdWUgPSBmYWxzZTtcbiAgICB9KTtcbiAgfVxuXG4gIHNldFNlYXJjaEJveFZhbHVlKHRleHQ6IHN0cmluZyl7XG4gICAgdGhpcy5zZWFyY2hCb3guc2V0VmFsdWUodGV4dCk7XG4gICAgdGhpcy5pc1NldHRpbmdWYWx1ZSA9IHRydWU7XG4gIH1cbn1cbiIsIlxuPGRpdj5cbiAgPGxhYmVsIFthdHRyLmZvcl09XCJ0eXBlYWhlYWRJZFwiICpuZ0lmPVwib3B0aW9ucy5sYWJsZSAmJiBvcHRpb25zLmxhYmxlLmxlbmd0aCA+IDBcIj57e29wdGlvbnMubGFibGV9fTwvbGFiZWw+XG4gIDxkaXYgY2xhc3M9XCJ2aWEtdHlwZWFoZWFkXCIgKHRvaENsaWNrT3V0U2l0ZSk9XCJoaWRlKClcIj5cbiAgICA8dWwgY2xhc3M9XCJ0eXBlYWhlYWQtY29udGFpbmVyXCI+XG4gICAgICA8bGkgY2xhc3M9XCJ0eXBlYWhlYWRcIj5cbiAgICAgICAgPHVsIGNsYXNzPVwidHlwZWFoZWFkLWl0ZW1zLXNlbGVjdGVkXCI+XG4gICAgICAgICAgPHNwYW4gKm5nSWY9XCIhb3B0aW9ucy5pbmxpbmVTaW5nbGVTZWFyY2hcIj5cbiAgICAgICAgICAgIDxsaSAqbmdGb3I9XCJsZXQgaXRlbSBvZiBvcHRpb25zLml0ZW1zU2VsZWN0ZWRcIiBjbGFzcz1cInR5cGVhaGVhZC1tZW51LWl0ZW0tc2VsZWN0ZWRcIj5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpdGVtLWRpc3BsYXlcIiB0aXRsZT1cInt7aXRlbS5uYW1lfX1cIj57e2l0ZW0ubmFtZX19PC9zcGFuPlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImljLXJlbW92ZVwiICpuZ0lmPVwiIWRpc2FibGVkXCIgKGNsaWNrKT1cInJlbW92ZShpdGVtKVwiPlxuICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS10aW1lc1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4gY2xhc3M9XCJpbmxpbmUtZGlzYWJsZWRcIiAqbmdJZj1cIm9wdGlvbnMuaW5saW5lU2luZ2xlU2VhcmNoICYmIGRpc2FibGVkPT10cnVlICYmIG9wdGlvbnMuaXRlbXNTZWxlY3RlZC5sZW5ndGhcIj57e29wdGlvbnMuaXRlbXNTZWxlY3RlZFswXS5uYW1lfX08L3NwYW4+XG4gICAgICAgICAgPGxpIGNsYXNzPVwidHlwZWFoZWFkLW1lbnUtaXRlbS1pbnB1dFwiPlxuICAgICAgICAgICAgPGlucHV0ICpuZ0lmPVwiZGlzYWJsZWQ9PWZhbHNlXCIgW2F0dHIuaWRdPVwidHlwZWFoZWFkSWRcIiBhdXRvY29tcGxldGU9XCJvZmZcIiBwbGFjZWhvbGRlcj1cInt7bGFuZ3VhZ2VPYmouZW50ZXJfdGV4dH19XCIgW2Zvcm1Db250cm9sXT1cInNlYXJjaEJveFwiIGNsYXNzPVwidHlwZWFoZWFkLWlucHV0XCI+XG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImljLXNwaW5uZXJcIiAqbmdJZj1cIm9wdGlvbnMuaXNMb2FkaW5nXCI+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1jaXJjbGUtby1ub3RjaCBmYS1zcGluXCIgaWQ9XCJjaXJjbGUtby1ub3RjaC1zcGluXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJ0aGlzLmlzU2luZ2xlSW5saW5lU2VhcmNoKCkgJiYgb3B0aW9ucy5pdGVtc1NlbGVjdGVkLmxlbmd0aCAmJiBvcHRpb25zLmlzTG9hZGluZyA9PSBmYWxzZSAmJiAhZGlzYWJsZWRcIiAoY2xpY2spPVwicmVtb3ZlU2luZ2xlSXRlbSgpXCIgY2xhc3M9XCJpYy1yZW1vdmVcIj5cbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLXRpbWVzXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPHVsICpuZ0lmPVwib3B0aW9ucy5pdGVtcy5sZW5ndGggPiAwIHx8IG9wdGlvbnMubm9EYXRhRm91bmRcIiBjbGFzcz1cInR5cGVhaGVhZC1tZW51LWl0ZW1zIHotZGVwdGgtMVwiPlxuICAgICAgICAgICAgICA8bGkgKm5nRm9yPVwibGV0IGl0ZW0gb2Ygb3B0aW9ucy5pdGVtc1wiIGNsYXNzPVwidHlwZWFoZWFkLW1lbnUtaXRlbVwiIChjbGljayk9XCJzZWxlY3QoaXRlbSlcIj5cbiAgICAgICAgICAgICAgICAgIDxzcGFuPnt7aXRlbS5uYW1lfX08L3NwYW4+XG4gICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDxsaSAqbmdJZj1cIm9wdGlvbnMubm9EYXRhRm91bmRcIiBjbGFzcz1cInR5cGVhaGVhZC1tZW51LWl0ZW1cIj5cbiAgICAgICAgICAgICAgICAgIDxzcGFuPnt7bGFuZ3VhZ2VPYmoubm9fZGF0YV9mb3VuZH19PC9zcGFuPlxuICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICA8L2xpPlxuICAgICAgICA8L3VsPlxuICAgICAgPC9saT5cbiAgICA8L3VsPlxuICA8L2Rpdj5cbjwvZGl2PiJdfQ==