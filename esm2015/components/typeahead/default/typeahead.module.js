import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TypeaheadComponent } from './typeahead';
import { CommonModule } from '@angular/common';
import { ClickOutSiteModule } from '../../click_out_site/click.out.site.module';
import * as i0 from "@angular/core";
export class TypeaheadModule {
}
TypeaheadModule.ɵmod = i0.ɵɵdefineNgModule({ type: TypeaheadModule });
TypeaheadModule.ɵinj = i0.ɵɵdefineInjector({ factory: function TypeaheadModule_Factory(t) { return new (t || TypeaheadModule)(); }, imports: [[
            ClickOutSiteModule,
            CommonModule,
            FormsModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(TypeaheadModule, { declarations: [TypeaheadComponent], imports: [ClickOutSiteModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule], exports: [TypeaheadComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TypeaheadModule, [{
        type: NgModule,
        args: [{
                declarations: [TypeaheadComponent],
                imports: [
                    ClickOutSiteModule,
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                exports: [TypeaheadComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdHlwZWFoZWFkL2RlZmF1bHQvdHlwZWFoZWFkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDakQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDOztBQVloRixNQUFNLE9BQU8sZUFBZTs7bURBQWYsZUFBZTs2R0FBZixlQUFlLGtCQVJqQjtZQUNQLGtCQUFrQjtZQUNsQixZQUFZO1lBQ1osV0FBVztZQUNYLG1CQUFtQjtTQUNwQjt3RkFHVSxlQUFlLG1CQVRYLGtCQUFrQixhQUUvQixrQkFBa0I7UUFDbEIsWUFBWTtRQUNaLFdBQVc7UUFDWCxtQkFBbUIsYUFFWCxrQkFBa0I7a0RBRWpCLGVBQWU7Y0FWM0IsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLGtCQUFrQixDQUFDO2dCQUNsQyxPQUFPLEVBQUU7b0JBQ1Asa0JBQWtCO29CQUNsQixZQUFZO29CQUNaLFdBQVc7b0JBQ1gsbUJBQW1CO2lCQUNwQjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQzthQUM5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgVHlwZWFoZWFkQ29tcG9uZW50IH0gZnJvbSAnLi90eXBlYWhlYWQnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IENsaWNrT3V0U2l0ZU1vZHVsZSB9IGZyb20gJy4uLy4uL2NsaWNrX291dF9zaXRlL2NsaWNrLm91dC5zaXRlLm1vZHVsZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1R5cGVhaGVhZENvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBDbGlja091dFNpdGVNb2R1bGUsXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW1R5cGVhaGVhZENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgVHlwZWFoZWFkTW9kdWxlIHsgfVxuIl19