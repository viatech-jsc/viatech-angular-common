export class TypeaheadOptionDto {
    constructor(url = "", multiple = true, inlineSingleSearch = false) {
        this._url = url;
        this._multiple = multiple;
        this._inlineSingleSearch = inlineSingleSearch;
        this._debounceTime = 400;
        this._itemsSelected = new Array();
        this._items = new Array();
        this._errorMessage = "";
        this._isLoading = false;
        this._minLengthToLoad = 1;
        this._noDataFound = false;
        this._lable = "";
    }
    /**
     * Getter url
     * return {string}
     */
    get url() {
        return this._url;
    }
    /**
     * Getter multiple
     * return {boolean}
     */
    get multiple() {
        return this._multiple;
    }
    /**
     * Getter inlineSingleSearch
     * return {boolean}
     */
    get inlineSingleSearch() {
        return this._inlineSingleSearch;
    }
    /**
     * Getter debounceTime
     * return {number}
     */
    get debounceTime() {
        return this._debounceTime;
    }
    /**
     * Getter itemsSelected
     * return {TypeaheadItem[]}
     */
    get itemsSelected() {
        return this._itemsSelected;
    }
    /**
     * Getter items
     * return {TypeaheadItem[]}
     */
    get items() {
        return this._items;
    }
    /**
     * Getter errorMessage
     * return {string}
     */
    get errorMessage() {
        return this._errorMessage;
    }
    /**
     * Getter isLoading
     * return {boolean}
     */
    get isLoading() {
        return this._isLoading;
    }
    /**
     * Getter minLengthToLoad
     * return {number}
     */
    get minLengthToLoad() {
        return this._minLengthToLoad;
    }
    /**
     * Getter noDataFound
     * return {boolean}
     */
    get noDataFound() {
        return this._noDataFound;
    }
    get lable() {
        return this._lable;
    }
    get keepHistory() {
        return this._keepHistory;
    }
    /**
     * Setter url
     * param {string} value
     */
    set url(value) {
        this._url = value;
    }
    /**
     * Setter multiple
     * param {boolean} value
     */
    set multiple(value) {
        this._multiple = value;
    }
    /**
     * Setter inlineSingleSearch
     * param {boolean} value
     */
    set inlineSingleSearch(value) {
        this._inlineSingleSearch = value;
    }
    /**
     * Setter debounceTime
     * param {number} value
     */
    set debounceTime(value) {
        this._debounceTime = value;
    }
    /**
     * Setter itemsSelected
     * param {TypeaheadItem[]} value
     */
    set itemsSelected(value) {
        this._itemsSelected = value;
    }
    /**
     * Setter items
     * param {TypeaheadItem[]} value
     */
    set items(value) {
        this._items = value;
    }
    /**
     * Setter errorMessage
     * param {string} value
     */
    set errorMessage(value) {
        this._errorMessage = value;
    }
    /**
     * Setter isLoading
     * param {boolean} value
     */
    set isLoading(value) {
        this._isLoading = value;
    }
    /**
     * Setter minLengthToLoad
     * param {number} value
     */
    set minLengthToLoad(value) {
        this._minLengthToLoad = value;
    }
    /**
     * Setter noDataFound
     * param {boolean} value
     */
    set noDataFound(value) {
        this._noDataFound = value;
    }
    set lable(lable) {
        this._lable = lable;
    }
    set keepHistory(_keepHistory) {
        this._keepHistory = _keepHistory;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLm9wdGlvbi5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3R5cGVhaGVhZC9kdG8vdHlwZWFoZWFkLm9wdGlvbi5kdG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsTUFBTSxPQUFPLGtCQUFrQjtJQWUzQixZQUFZLE1BQWMsRUFBRSxFQUFFLFdBQW9CLElBQUksRUFBRSxxQkFBOEIsS0FBSztRQUN2RixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUNoQixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsa0JBQWtCLENBQUM7UUFDOUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLEtBQUssRUFBb0IsQ0FBQztRQUNwRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksS0FBSyxFQUFvQixDQUFDO1FBQzVDLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsR0FBRztRQUNWLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxRQUFRO1FBQ2YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLGtCQUFrQjtRQUN6QixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztJQUNwQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxZQUFZO1FBQ25CLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxhQUFhO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxLQUFLO1FBQ1osT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFlBQVk7UUFDbkIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFNBQVM7UUFDaEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQzNCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLGVBQWU7UUFDdEIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDakMsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsV0FBVztRQUNsQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDN0IsQ0FBQztJQUVELElBQVcsS0FBSztRQUNaLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRUQsSUFBVyxXQUFXO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxHQUFHLENBQUMsS0FBYTtRQUN4QixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxRQUFRLENBQUMsS0FBYztRQUM5QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxrQkFBa0IsQ0FBQyxLQUFjO1FBQ3hDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7SUFDckMsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsWUFBWSxDQUFDLEtBQWE7UUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsYUFBYSxDQUFDLEtBQXlCO1FBQzlDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO0lBQ2hDLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLEtBQUssQ0FBQyxLQUF5QjtRQUN0QyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxZQUFZLENBQUMsS0FBYTtRQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxTQUFTLENBQUMsS0FBYztRQUMvQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxlQUFlLENBQUMsS0FBYTtRQUNwQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO0lBQ2xDLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFdBQVcsQ0FBQyxLQUFjO1FBQ2pDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO0lBQzlCLENBQUM7SUFFRCxJQUFXLEtBQUssQ0FBQyxLQUFhO1FBQzFCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxJQUFXLFdBQVcsQ0FBQyxZQUFxQjtRQUN4QyxJQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztJQUNyQyxDQUFDO0NBRUoiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUeXBlYWhlYWRJdGVtRHRvIH0gZnJvbSBcIi4vdHlwZWFoZWFkLml0ZW0uZHRvXCI7XG5cbmV4cG9ydCBjbGFzcyBUeXBlYWhlYWRPcHRpb25EdG8ge1xuICAgIHByaXZhdGUgX3VybDogc3RyaW5nO1xuICAgIHByaXZhdGUgX211bHRpcGxlOiBib29sZWFuO1xuICAgIC8vaW5saW5lIHNlYXJjaCB3aXRob3V0IGNhcmQgb24gc2VhcmNoIGJveFxuICAgIHByaXZhdGUgX2lubGluZVNpbmdsZVNlYXJjaDogYm9vbGVhbjtcbiAgICBwcml2YXRlIF9kZWJvdW5jZVRpbWU6IG51bWJlcjtcbiAgICBwcml2YXRlIF9pdGVtc1NlbGVjdGVkOiBUeXBlYWhlYWRJdGVtRHRvW107XG4gICAgcHJpdmF0ZSBfaXRlbXM6IFR5cGVhaGVhZEl0ZW1EdG9bXTtcbiAgICBwcml2YXRlIF9lcnJvck1lc3NhZ2U6IHN0cmluZztcbiAgICBwcml2YXRlIF9pc0xvYWRpbmc6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBfbWluTGVuZ3RoVG9Mb2FkOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBfbm9EYXRhRm91bmQ6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBfbGFibGU6IHN0cmluZztcbiAgICBwcml2YXRlIF9rZWVwSGlzdG9yeTogYm9vbGVhbjtcblxuICAgIGNvbnN0cnVjdG9yKHVybDogc3RyaW5nID0gXCJcIiwgbXVsdGlwbGU6IGJvb2xlYW4gPSB0cnVlLCBpbmxpbmVTaW5nbGVTZWFyY2g6IGJvb2xlYW4gPSBmYWxzZSkge1xuICAgICAgICB0aGlzLl91cmwgPSB1cmw7XG4gICAgICAgIHRoaXMuX211bHRpcGxlID0gbXVsdGlwbGU7XG4gICAgICAgIHRoaXMuX2lubGluZVNpbmdsZVNlYXJjaCA9IGlubGluZVNpbmdsZVNlYXJjaDtcbiAgICAgICAgdGhpcy5fZGVib3VuY2VUaW1lID0gNDAwO1xuICAgICAgICB0aGlzLl9pdGVtc1NlbGVjdGVkID0gbmV3IEFycmF5PFR5cGVhaGVhZEl0ZW1EdG8+KCk7XG4gICAgICAgIHRoaXMuX2l0ZW1zID0gbmV3IEFycmF5PFR5cGVhaGVhZEl0ZW1EdG8+KCk7XG4gICAgICAgIHRoaXMuX2Vycm9yTWVzc2FnZSA9IFwiXCI7XG4gICAgICAgIHRoaXMuX2lzTG9hZGluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9taW5MZW5ndGhUb0xvYWQgPSAxO1xuICAgICAgICB0aGlzLl9ub0RhdGFGb3VuZCA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9sYWJsZSA9IFwiXCI7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHVybFxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgdXJsKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLl91cmw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIG11bHRpcGxlXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgbXVsdGlwbGUoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9tdWx0aXBsZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgaW5saW5lU2luZ2xlU2VhcmNoXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgaW5saW5lU2luZ2xlU2VhcmNoKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5faW5saW5lU2luZ2xlU2VhcmNoO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBkZWJvdW5jZVRpbWVcbiAgICAgKiByZXR1cm4ge251bWJlcn1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGRlYm91bmNlVGltZSgpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5fZGVib3VuY2VUaW1lO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBpdGVtc1NlbGVjdGVkXG4gICAgICogcmV0dXJuIHtUeXBlYWhlYWRJdGVtW119XG4gICAgICovXG4gICAgcHVibGljIGdldCBpdGVtc1NlbGVjdGVkKCk6IFR5cGVhaGVhZEl0ZW1EdG9bXSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pdGVtc1NlbGVjdGVkO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBpdGVtc1xuICAgICAqIHJldHVybiB7VHlwZWFoZWFkSXRlbVtdfVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgaXRlbXMoKTogVHlwZWFoZWFkSXRlbUR0b1tdIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2l0ZW1zO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBlcnJvck1lc3NhZ2VcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGVycm9yTWVzc2FnZSgpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gdGhpcy5fZXJyb3JNZXNzYWdlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBpc0xvYWRpbmdcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIGdldCBpc0xvYWRpbmcoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc0xvYWRpbmc7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIG1pbkxlbmd0aFRvTG9hZFxuICAgICAqIHJldHVybiB7bnVtYmVyfVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgbWluTGVuZ3RoVG9Mb2FkKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiB0aGlzLl9taW5MZW5ndGhUb0xvYWQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIG5vRGF0YUZvdW5kXG4gICAgICogcmV0dXJuIHtib29sZWFufVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgbm9EYXRhRm91bmQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9ub0RhdGFGb3VuZDtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IGxhYmxlKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLl9sYWJsZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IGtlZXBIaXN0b3J5KCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5fa2VlcEhpc3Rvcnk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHVybFxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCB1cmwodmFsdWU6IHN0cmluZykge1xuICAgICAgICB0aGlzLl91cmwgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgbXVsdGlwbGVcbiAgICAgKiBwYXJhbSB7Ym9vbGVhbn0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IG11bHRpcGxlKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX211bHRpcGxlID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGlubGluZVNpbmdsZVNlYXJjaFxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgaW5saW5lU2luZ2xlU2VhcmNoKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2lubGluZVNpbmdsZVNlYXJjaCA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBkZWJvdW5jZVRpbWVcbiAgICAgKiBwYXJhbSB7bnVtYmVyfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgZGVib3VuY2VUaW1lKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fZGVib3VuY2VUaW1lID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGl0ZW1zU2VsZWN0ZWRcbiAgICAgKiBwYXJhbSB7VHlwZWFoZWFkSXRlbVtdfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgaXRlbXNTZWxlY3RlZCh2YWx1ZTogVHlwZWFoZWFkSXRlbUR0b1tdKSB7XG4gICAgICAgIHRoaXMuX2l0ZW1zU2VsZWN0ZWQgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgaXRlbXNcbiAgICAgKiBwYXJhbSB7VHlwZWFoZWFkSXRlbVtdfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgaXRlbXModmFsdWU6IFR5cGVhaGVhZEl0ZW1EdG9bXSkge1xuICAgICAgICB0aGlzLl9pdGVtcyA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBlcnJvck1lc3NhZ2VcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgZXJyb3JNZXNzYWdlKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fZXJyb3JNZXNzYWdlID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGlzTG9hZGluZ1xuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgaXNMb2FkaW5nKHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2lzTG9hZGluZyA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBtaW5MZW5ndGhUb0xvYWRcbiAgICAgKiBwYXJhbSB7bnVtYmVyfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgbWluTGVuZ3RoVG9Mb2FkKHZhbHVlOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5fbWluTGVuZ3RoVG9Mb2FkID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIG5vRGF0YUZvdW5kXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBub0RhdGFGb3VuZCh2YWx1ZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9ub0RhdGFGb3VuZCA9IHZhbHVlO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZXQgbGFibGUobGFibGU6IHN0cmluZykge1xuICAgICAgICB0aGlzLl9sYWJsZSA9IGxhYmxlO1xuICAgIH1cblxuICAgIHB1YmxpYyBzZXQga2VlcEhpc3RvcnkoX2tlZXBIaXN0b3J5OiBib29sZWFuKSB7XG4gICAgICAgIHRoaXMuX2tlZXBIaXN0b3J5ID0gX2tlZXBIaXN0b3J5O1xuICAgIH1cblxufSJdfQ==