import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { TypeaheadItemDto } from "../dto/typeahead.item.dto";
import { TypeAheadLanguageConfig } from "../constant";
import { TypeaheadOptionDto } from "../dto/typeahead.option.dto";
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/chips";
import * as i5 from "@angular/material/autocomplete";
import * as i6 from "@angular/material/icon";
import * as i7 from "@angular/material/progress-spinner";
import * as i8 from "@angular/material/core";
const _c0 = ["typeaheadInput"];
function MatTypeaheadComponent_mat_label_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-label");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r0.options.lable);
} }
function MatTypeaheadComponent_mat_chip_5_mat_icon_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-icon", 14);
    i0.ɵɵtext(1, "cancel");
    i0.ɵɵelementEnd();
} }
function MatTypeaheadComponent_mat_chip_5_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-chip", 12);
    i0.ɵɵlistener("removed", function MatTypeaheadComponent_mat_chip_5_Template_mat_chip_removed_0_listener() { i0.ɵɵrestoreView(_r12); const item_r9 = ctx.$implicit; const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.remove(item_r9); });
    i0.ɵɵtext(1);
    i0.ɵɵtemplate(2, MatTypeaheadComponent_mat_chip_5_mat_icon_2_Template, 2, 0, "mat-icon", 13);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r9 = ctx.$implicit;
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵproperty("selectable", false)("removable", !ctx_r2.disabled);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", item_r9.name, " ");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r2.disabled);
} }
function MatTypeaheadComponent_input_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "input", 15, 16);
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    const _r6 = i0.ɵɵreference(10);
    const _r1 = i0.ɵɵreference(4);
    i0.ɵɵpropertyInterpolate("placeholder", ctx_r3.languageObj.enter_text);
    i0.ɵɵproperty("matAutocomplete", _r6)("matChipInputFor", _r1)("matChipInputSeparatorKeyCodes", ctx_r3.separatorKeysCodes);
} }
function MatTypeaheadComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    const _r15 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 17);
    i0.ɵɵlistener("click", function MatTypeaheadComponent_div_7_Template_div_click_0_listener() { i0.ɵɵrestoreView(_r15); const ctx_r14 = i0.ɵɵnextContext(); return ctx_r14.removeSingleItem(); });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2, "clear");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function MatTypeaheadComponent_div_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 18);
    i0.ɵɵelement(1, "mat-spinner", 19);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("diameter", 15);
} }
function MatTypeaheadComponent_mat_option_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-option", 20);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r16 = ctx.$implicit;
    i0.ɵɵproperty("value", item_r16);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", item_r16.name, " ");
} }
function MatTypeaheadComponent_mat_option_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-option", 20);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = i0.ɵɵnextContext();
    i0.ɵɵproperty("value", null);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r8.languageObj.no_data_found, " ");
} }
export class MatTypeaheadComponent {
    constructor() {
        this.languageObj = TypeAheadLanguageConfig;
        this.disabled = false;
        this.typeaheadLoadData = new EventEmitter();
        this.typeaheadItemSelected = new EventEmitter();
        this.typeaheadFormGroup = new FormGroup({
            searchBox: new FormControl('', [])
        });
        this.separatorKeysCodes = [ENTER, COMMA];
        //prevent duplicate search when set value for search box
        this.isSettingValue = false;
        //super();
    }
    hide() {
        if (!this.options.keepHistory) {
            this.options.items = [];
        }
        this.options.errorMessage = "";
        this.options.isLoading = false;
        this.options.noDataFound = false;
        this.typeaheadInput.nativeElement.value = '';
        this.typeaheadFormGroup.controls.searchBox.setValue(null);
    }
    isSingleInlineSearch() {
        return this.options.inlineSingleSearch == true && this.options.multiple == false;
    }
    select(event) {
        let item = event.option.value;
        if (item) {
            this.options.items = this.options.items.filter((filterItem) => {
                return filterItem.value !== item.value;
            });
            if (!this.options.multiple) {
                this.options.itemsSelected = [];
            }
            this.options.itemsSelected.push(item);
            this.options.noDataFound = this.options.items.length === 0;
            this.typeaheadItemSelected.emit(item);
        }
        if (this.isSingleInlineSearch() == true) {
            this.isSettingValue = true;
            this.typeaheadInput.nativeElement.value = item.name;
            this.typeaheadFormGroup.controls.searchBox.setValue(item.name);
        }
        this.hide();
    }
    remove(item) {
        this.options.items.push(item);
        this.options.itemsSelected = this.options.itemsSelected.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        this.options.noDataFound = this.options.items.length === 0;
    }
    removeSingleItem() {
        var _a;
        this.isSettingValue = true;
        this.typeaheadFormGroup.controls.searchBox.setValue("");
        this.options.itemsSelected = [];
        (_a = this.typeaheadItemSelected) === null || _a === void 0 ? void 0 : _a.emit(new TypeaheadItemDto(undefined, undefined));
    }
    ngAfterViewInit() {
        if (this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            this.typeaheadFormGroup.controls.searchBox.setValue(this.options.itemsSelected[0].name);
            this.isSettingValue = true;
        }
        this.typeaheadFormGroup.controls.searchBox.valueChanges.pipe(debounceTime(this.options.debounceTime), distinctUntilChanged()).subscribe((searchText) => {
            var _a;
            if (searchText && searchText.length >= this.options.minLengthToLoad && this.isSettingValue == false) {
                this.options.isLoading = true;
                (_a = this.typeaheadLoadData) === null || _a === void 0 ? void 0 : _a.emit(searchText);
            }
            this.isSettingValue = false;
        });
    }
    setSearchBoxValue(text) {
        this.typeaheadFormGroup.controls.searchBox.setValue(text);
        this.isSettingValue = true;
    }
}
MatTypeaheadComponent.ɵfac = function MatTypeaheadComponent_Factory(t) { return new (t || MatTypeaheadComponent)(); };
MatTypeaheadComponent.ɵcmp = i0.ɵɵdefineComponent({ type: MatTypeaheadComponent, selectors: [["via-mat-typeahead"]], viewQuery: function MatTypeaheadComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.typeaheadInput = _t.first);
    } }, inputs: { languageObj: "languageObj", options: "options", disabled: "disabled" }, outputs: { typeaheadLoadData: "typeaheadLoadData", typeaheadItemSelected: "typeaheadItemSelected" }, decls: 13, vars: 9, consts: [[3, "formGroup"], ["appearance", "fill", 1, "typeahead-wrapper"], [4, "ngIf"], ["typeaheadChips", ""], [3, "selectable", "removable", "removed", 4, "ngFor", "ngForOf"], ["formControlName", "searchBox", 3, "placeholder", "matAutocomplete", "matChipInputFor", "matChipInputSeparatorKeyCodes", 4, "ngIf"], ["matSuffix", "", "class", "typeahead-suffix-wrapper", 3, "click", 4, "ngIf"], ["matSuffix", "", 4, "ngIf"], ["showPanel", "true", 3, "autoActiveFirstOption", "optionSelected"], ["auto", "matAutocomplete"], ["class", "typeahead-menu-item", 3, "value", 4, "ngFor", "ngForOf"], ["class", "typeahead-menu-item", 3, "value", 4, "ngIf"], [3, "selectable", "removable", "removed"], ["matChipRemove", "", 4, "ngIf"], ["matChipRemove", ""], ["formControlName", "searchBox", 3, "placeholder", "matAutocomplete", "matChipInputFor", "matChipInputSeparatorKeyCodes"], ["typeaheadInput", ""], ["matSuffix", "", 1, "typeahead-suffix-wrapper", 3, "click"], ["matSuffix", ""], [3, "diameter"], [1, "typeahead-menu-item", 3, "value"]], template: function MatTypeaheadComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "form", 0);
        i0.ɵɵelementStart(1, "mat-form-field", 1);
        i0.ɵɵtemplate(2, MatTypeaheadComponent_mat_label_2_Template, 2, 1, "mat-label", 2);
        i0.ɵɵelementStart(3, "mat-chip-list", null, 3);
        i0.ɵɵtemplate(5, MatTypeaheadComponent_mat_chip_5_Template, 3, 4, "mat-chip", 4);
        i0.ɵɵtemplate(6, MatTypeaheadComponent_input_6_Template, 2, 4, "input", 5);
        i0.ɵɵtemplate(7, MatTypeaheadComponent_div_7_Template, 3, 0, "div", 6);
        i0.ɵɵtemplate(8, MatTypeaheadComponent_div_8_Template, 2, 1, "div", 7);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(9, "mat-autocomplete", 8, 9);
        i0.ɵɵlistener("optionSelected", function MatTypeaheadComponent_Template_mat_autocomplete_optionSelected_9_listener($event) { return ctx.select($event); });
        i0.ɵɵtemplate(11, MatTypeaheadComponent_mat_option_11_Template, 2, 2, "mat-option", 10);
        i0.ɵɵtemplate(12, MatTypeaheadComponent_mat_option_12_Template, 2, 2, "mat-option", 11);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("formGroup", ctx.typeaheadFormGroup);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.options.lable && ctx.options.lable.length > 0);
        i0.ɵɵadvance(3);
        i0.ɵɵproperty("ngForOf", ctx.options.itemsSelected);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.disabled === false);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.isSingleInlineSearch() && ctx.options.itemsSelected.length && ctx.options.isLoading == false && !ctx.disabled);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.isLoading);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("autoActiveFirstOption", true);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngForOf", ctx.options.items);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.noDataFound);
    } }, directives: [i1.ɵangular_packages_forms_forms_y, i1.NgControlStatusGroup, i1.FormGroupDirective, i2.MatFormField, i3.NgIf, i4.MatChipList, i3.NgForOf, i5.MatAutocomplete, i2.MatLabel, i4.MatChip, i6.MatIcon, i4.MatChipRemove, i1.DefaultValueAccessor, i5.MatAutocompleteTrigger, i4.MatChipInput, i1.NgControlStatus, i1.FormControlName, i2.MatSuffix, i7.MatSpinner, i8.MatOption], styles: [".typeahead-wrapper[_ngcontent-%COMP%]{width:100%}.typeahead-wrapper[_ngcontent-%COMP%]   .typeahead-suffix-wrapper[_ngcontent-%COMP%]{cursor:pointer}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MatTypeaheadComponent, [{
        type: Component,
        args: [{
                selector: "via-mat-typeahead",
                templateUrl: "./mat.typeahead.html",
                styleUrls: ["./mat.typeahead.scss"]
            }]
    }], function () { return []; }, { languageObj: [{
            type: Input
        }], options: [{
            type: Input
        }], disabled: [{
            type: Input
        }], typeaheadLoadData: [{
            type: Output
        }], typeaheadItemSelected: [{
            type: Output
        }], typeaheadInput: [{
            type: ViewChild,
            args: ["typeaheadInput"]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0LnR5cGVhaGVhZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdHlwZWFoZWFkL21hdGVyaWFsL21hdC50eXBlYWhlYWQudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3R5cGVhaGVhZC9tYXRlcmlhbC9tYXQudHlwZWFoZWFkLmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlGLE9BQU8sRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFlBQVksRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzdELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUN0RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUNqRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxNQUFNLHVCQUF1QixDQUFDOzs7Ozs7Ozs7Ozs7SUNKakQsaUNBQTZEO0lBQUEsWUFBaUI7SUFBQSxpQkFBWTs7O0lBQTdCLGVBQWlCO0lBQWpCLDBDQUFpQjs7O0lBUTFFLG9DQUEwQztJQUFBLHNCQUFNO0lBQUEsaUJBQVc7Ozs7SUFON0Qsb0NBS0k7SUFERix5T0FBd0I7SUFDdEIsWUFDRjtJQUFBLDRGQUEwQztJQUM1QyxpQkFBVzs7OztJQUxULGtDQUFvQiwrQkFBQTtJQUdsQixlQUNGO0lBREUsNkNBQ0Y7SUFBd0IsZUFBaUI7SUFBakIsdUNBQWlCOzs7SUFFM0MsZ0NBVUE7Ozs7O0lBUEUsc0VBQXdDO0lBRXhDLHFDQUF3Qix3QkFBQSw0REFBQTs7OztJQUsxQiwrQkFDRTtJQUQ2SiwrTEFBNEI7SUFDekwsZ0NBQVU7SUFBQSxxQkFBSztJQUFBLGlCQUFXO0lBQzVCLGlCQUFNOzs7SUFDTiwrQkFDRTtJQUFBLGtDQUEyQztJQUM3QyxpQkFBTTs7SUFEUyxlQUFlO0lBQWYsNkJBQWU7OztJQVM5QixzQ0FDRTtJQUFBLFlBQ0Y7SUFBQSxpQkFBYTs7O0lBRmtDLGdDQUFjO0lBQzNELGVBQ0Y7SUFERSw4Q0FDRjs7O0lBQ0Esc0NBQ0U7SUFBQSxZQUNGO0lBQUEsaUJBQWE7OztJQUYyQiw0QkFBYztJQUNwRCxlQUNGO0lBREUsaUVBQ0Y7O0FEMUJOLE1BQU0sT0FBTyxxQkFBcUI7SUFnQmhDO1FBZlMsZ0JBQVcsR0FBRyx1QkFBdUIsQ0FBQztRQUV0QyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBQ3pCLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUlyRCx1QkFBa0IsR0FBRyxJQUFJLFNBQVMsQ0FBQztZQUNqQyxTQUFTLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQztTQUNuQyxDQUFDLENBQUM7UUFDSCx1QkFBa0IsR0FBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUU5Qyx3REFBd0Q7UUFDaEQsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFFdEMsVUFBVTtJQUNaLENBQUM7SUFFRCxJQUFJO1FBQ0YsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFO1lBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztTQUN6QjtRQUNELElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDN0MsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCxvQkFBb0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUM7SUFDbkYsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFtQztRQUN4QyxJQUFJLElBQUksR0FBcUIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDaEQsSUFBSSxJQUFJLEVBQUU7WUFDUixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRTtnQkFDNUQsT0FBTyxVQUFVLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDekMsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQzthQUNqQztZQUNELElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1lBQzNELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkM7UUFDRCxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLElBQUksRUFBRTtZQUN2QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQixJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztZQUNwRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2hFO1FBQ0QsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVELE1BQU0sQ0FBQyxJQUFzQjtRQUMzQixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDNUUsT0FBTyxVQUFVLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFFRCxnQkFBZ0I7O1FBQ2QsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUNoQyxNQUFBLElBQUksQ0FBQyxxQkFBcUIsMENBQUUsSUFBSSxDQUFDLElBQUksZ0JBQWdCLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxFQUFFO0lBQy9FLENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQzVFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4RixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM1QjtRQUVELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFOztZQUNySixJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksS0FBSyxFQUFFO2dCQUNuRyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7Z0JBQzlCLE1BQUEsSUFBSSxDQUFDLGlCQUFpQiwwQ0FBRSxJQUFJLENBQUMsVUFBVSxFQUFFO2FBQzFDO1lBQ0QsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDOUIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsSUFBWTtRQUM1QixJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7SUFDN0IsQ0FBQzs7MEZBekZVLHFCQUFxQjswREFBckIscUJBQXFCOzs7Ozs7UUNkbEMsK0JBQ0U7UUFBQSx5Q0FDRTtRQUFBLGtGQUE2RDtRQUM3RCw4Q0FDRTtRQUFBLGdGQUtJO1FBR0osMEVBVUE7UUFBQSxzRUFDRTtRQUVGLHNFQUNFO1FBRUosaUJBQWdCO1FBQ2hCLDhDQU1FO1FBSEEsb0lBQWtCLGtCQUFjLElBQUM7UUFHakMsdUZBQ0U7UUFFRix1RkFDRTtRQUVKLGlCQUFtQjtRQUNyQixpQkFBaUI7UUFDbkIsaUJBQU87O1FBM0NELGtEQUFnQztRQUV2QixlQUFpRDtRQUFqRCx3RUFBaUQ7UUFHeEQsZUFBMEM7UUFBMUMsbURBQTBDO1FBUzFDLGVBQTBCO1FBQTFCLDZDQUEwQjtRQVFvQixlQUE4RztRQUE5Ryx3SUFBOEc7UUFHL0ksZUFBeUI7UUFBekIsNENBQXlCO1FBTXhDLGVBQThCO1FBQTlCLDRDQUE4QjtRQUlsQixlQUFrQztRQUFsQywyQ0FBa0M7UUFHbEMsZUFBMkI7UUFBM0IsOENBQTJCOztrRER4QmhDLHFCQUFxQjtjQUxqQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IsV0FBVyxFQUFFLHNCQUFzQjtnQkFDbkMsU0FBUyxFQUFFLENBQUMsc0JBQXNCLENBQUM7YUFDcEM7c0NBRVUsV0FBVztrQkFBbkIsS0FBSztZQUNHLE9BQU87a0JBQWYsS0FBSztZQUNHLFFBQVE7a0JBQWhCLEtBQUs7WUFDSSxpQkFBaUI7a0JBQTFCLE1BQU07WUFDRyxxQkFBcUI7a0JBQTlCLE1BQU07WUFHUCxjQUFjO2tCQURiLFNBQVM7bUJBQUMsZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgRm9ybUdyb3VwIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5pbXBvcnQgeyBkZWJvdW5jZVRpbWUsIGRpc3RpbmN0VW50aWxDaGFuZ2VkIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgVHlwZWFoZWFkSXRlbUR0byB9IGZyb20gXCIuLi9kdG8vdHlwZWFoZWFkLml0ZW0uZHRvXCI7XG5pbXBvcnQgeyBUeXBlQWhlYWRMYW5ndWFnZUNvbmZpZyB9IGZyb20gXCIuLi9jb25zdGFudFwiO1xuaW1wb3J0IHsgVHlwZWFoZWFkT3B0aW9uRHRvIH0gZnJvbSBcIi4uL2R0by90eXBlYWhlYWQub3B0aW9uLmR0b1wiO1xuaW1wb3J0IHsgQ09NTUEsIEVOVEVSIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2tleWNvZGVzJztcbmltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZVNlbGVjdGVkRXZlbnQgfSBmcm9tIFwiQGFuZ3VsYXIvbWF0ZXJpYWwvYXV0b2NvbXBsZXRlXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJ2aWEtbWF0LXR5cGVhaGVhZFwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL21hdC50eXBlYWhlYWQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vbWF0LnR5cGVhaGVhZC5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIE1hdFR5cGVhaGVhZENvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGxhbmd1YWdlT2JqID0gVHlwZUFoZWFkTGFuZ3VhZ2VDb25maWc7XG4gIEBJbnB1dCgpIG9wdGlvbnM6IFR5cGVhaGVhZE9wdGlvbkR0bztcbiAgQElucHV0KCkgZGlzYWJsZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQE91dHB1dCgpIHR5cGVhaGVhZExvYWREYXRhID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgdHlwZWFoZWFkSXRlbVNlbGVjdGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIEBWaWV3Q2hpbGQoXCJ0eXBlYWhlYWRJbnB1dFwiKVxuICB0eXBlYWhlYWRJbnB1dDogRWxlbWVudFJlZjxIVE1MSW5wdXRFbGVtZW50PjtcbiAgdHlwZWFoZWFkRm9ybUdyb3VwID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgc2VhcmNoQm94OiBuZXcgRm9ybUNvbnRyb2woJycsIFtdKVxuICB9KTtcbiAgc2VwYXJhdG9yS2V5c0NvZGVzOiBudW1iZXJbXSA9IFtFTlRFUiwgQ09NTUFdO1xuXG4gIC8vcHJldmVudCBkdXBsaWNhdGUgc2VhcmNoIHdoZW4gc2V0IHZhbHVlIGZvciBzZWFyY2ggYm94XG4gIHByaXZhdGUgaXNTZXR0aW5nVmFsdWU6IGJvb2xlYW4gPSBmYWxzZTtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgLy9zdXBlcigpO1xuICB9XG4gIFxuICBoaWRlKCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5vcHRpb25zLmtlZXBIaXN0b3J5KSB7XG4gICAgICB0aGlzLm9wdGlvbnMuaXRlbXMgPSBbXTtcbiAgICB9XG4gICAgdGhpcy5vcHRpb25zLmVycm9yTWVzc2FnZSA9IFwiXCI7XG4gICAgdGhpcy5vcHRpb25zLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgIHRoaXMub3B0aW9ucy5ub0RhdGFGb3VuZCA9IGZhbHNlO1xuICAgIHRoaXMudHlwZWFoZWFkSW5wdXQubmF0aXZlRWxlbWVudC52YWx1ZSA9ICcnO1xuICAgIHRoaXMudHlwZWFoZWFkRm9ybUdyb3VwLmNvbnRyb2xzLnNlYXJjaEJveC5zZXRWYWx1ZShudWxsKTtcbiAgfVxuXG4gIGlzU2luZ2xlSW5saW5lU2VhcmNoKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLm9wdGlvbnMuaW5saW5lU2luZ2xlU2VhcmNoID09IHRydWUgJiYgdGhpcy5vcHRpb25zLm11bHRpcGxlID09IGZhbHNlO1xuICB9XG5cbiAgc2VsZWN0KGV2ZW50OiBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50KTogdm9pZCB7XG4gICAgbGV0IGl0ZW06IFR5cGVhaGVhZEl0ZW1EdG8gPSBldmVudC5vcHRpb24udmFsdWU7XG4gICAgaWYgKGl0ZW0pIHtcbiAgICAgIHRoaXMub3B0aW9ucy5pdGVtcyA9IHRoaXMub3B0aW9ucy5pdGVtcy5maWx0ZXIoKGZpbHRlckl0ZW0pID0+IHtcbiAgICAgICAgcmV0dXJuIGZpbHRlckl0ZW0udmFsdWUgIT09IGl0ZW0udmFsdWU7XG4gICAgICB9KTtcbiAgICAgIGlmICghdGhpcy5vcHRpb25zLm11bHRpcGxlKSB7XG4gICAgICAgIHRoaXMub3B0aW9ucy5pdGVtc1NlbGVjdGVkID0gW107XG4gICAgICB9XG4gICAgICB0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZC5wdXNoKGl0ZW0pO1xuICAgICAgdGhpcy5vcHRpb25zLm5vRGF0YUZvdW5kID0gdGhpcy5vcHRpb25zLml0ZW1zLmxlbmd0aCA9PT0gMDtcbiAgICAgIHRoaXMudHlwZWFoZWFkSXRlbVNlbGVjdGVkLmVtaXQoaXRlbSk7XG4gICAgfVxuICAgIGlmICh0aGlzLmlzU2luZ2xlSW5saW5lU2VhcmNoKCkgPT0gdHJ1ZSkge1xuICAgICAgdGhpcy5pc1NldHRpbmdWYWx1ZSA9IHRydWU7XG4gICAgICB0aGlzLnR5cGVhaGVhZElucHV0Lm5hdGl2ZUVsZW1lbnQudmFsdWUgPSBpdGVtLm5hbWU7XG4gICAgICB0aGlzLnR5cGVhaGVhZEZvcm1Hcm91cC5jb250cm9scy5zZWFyY2hCb3guc2V0VmFsdWUoaXRlbS5uYW1lKTtcbiAgICB9XG4gICAgdGhpcy5oaWRlKCk7XG4gIH1cblxuICByZW1vdmUoaXRlbTogVHlwZWFoZWFkSXRlbUR0byk6IHZvaWQge1xuICAgIHRoaXMub3B0aW9ucy5pdGVtcy5wdXNoKGl0ZW0pO1xuICAgIHRoaXMub3B0aW9ucy5pdGVtc1NlbGVjdGVkID0gdGhpcy5vcHRpb25zLml0ZW1zU2VsZWN0ZWQuZmlsdGVyKChmaWx0ZXJJdGVtKSA9PiB7XG4gICAgICByZXR1cm4gZmlsdGVySXRlbS52YWx1ZSAhPT0gaXRlbS52YWx1ZTtcbiAgICB9KTtcbiAgICB0aGlzLm9wdGlvbnMubm9EYXRhRm91bmQgPSB0aGlzLm9wdGlvbnMuaXRlbXMubGVuZ3RoID09PSAwO1xuICB9XG5cbiAgcmVtb3ZlU2luZ2xlSXRlbSgpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2V0dGluZ1ZhbHVlID0gdHJ1ZTtcbiAgICB0aGlzLnR5cGVhaGVhZEZvcm1Hcm91cC5jb250cm9scy5zZWFyY2hCb3guc2V0VmFsdWUoXCJcIik7XG4gICAgdGhpcy5vcHRpb25zLml0ZW1zU2VsZWN0ZWQgPSBbXTtcbiAgICB0aGlzLnR5cGVhaGVhZEl0ZW1TZWxlY3RlZD8uZW1pdChuZXcgVHlwZWFoZWFkSXRlbUR0byh1bmRlZmluZWQsIHVuZGVmaW5lZCkpO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIGlmICh0aGlzLmlzU2luZ2xlSW5saW5lU2VhcmNoKCkgPT0gdHJ1ZSAmJiB0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZC5sZW5ndGgpIHtcbiAgICAgIHRoaXMudHlwZWFoZWFkRm9ybUdyb3VwLmNvbnRyb2xzLnNlYXJjaEJveC5zZXRWYWx1ZSh0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZFswXS5uYW1lKTtcbiAgICAgIHRoaXMuaXNTZXR0aW5nVmFsdWUgPSB0cnVlO1xuICAgIH1cblxuICAgIHRoaXMudHlwZWFoZWFkRm9ybUdyb3VwLmNvbnRyb2xzLnNlYXJjaEJveC52YWx1ZUNoYW5nZXMucGlwZShkZWJvdW5jZVRpbWUodGhpcy5vcHRpb25zLmRlYm91bmNlVGltZSksIGRpc3RpbmN0VW50aWxDaGFuZ2VkKCkpLnN1YnNjcmliZSgoc2VhcmNoVGV4dCkgPT4ge1xuICAgICAgaWYgKHNlYXJjaFRleHQgJiYgc2VhcmNoVGV4dC5sZW5ndGggPj0gdGhpcy5vcHRpb25zLm1pbkxlbmd0aFRvTG9hZCAmJiB0aGlzLmlzU2V0dGluZ1ZhbHVlID09IGZhbHNlKSB7XG4gICAgICAgIHRoaXMub3B0aW9ucy5pc0xvYWRpbmcgPSB0cnVlO1xuICAgICAgICB0aGlzLnR5cGVhaGVhZExvYWREYXRhPy5lbWl0KHNlYXJjaFRleHQpO1xuICAgICAgfVxuICAgICAgdGhpcy5pc1NldHRpbmdWYWx1ZSA9IGZhbHNlO1xuICAgIH0pO1xuICB9XG5cbiAgc2V0U2VhcmNoQm94VmFsdWUodGV4dDogc3RyaW5nKXtcbiAgICB0aGlzLnR5cGVhaGVhZEZvcm1Hcm91cC5jb250cm9scy5zZWFyY2hCb3guc2V0VmFsdWUodGV4dCk7XG4gICAgdGhpcy5pc1NldHRpbmdWYWx1ZSA9IHRydWU7XG4gIH1cbn1cbiIsIjxmb3JtIFtmb3JtR3JvdXBdPVwidHlwZWFoZWFkRm9ybUdyb3VwXCI+XG4gIDxtYXQtZm9ybS1maWVsZCBjbGFzcz1cInR5cGVhaGVhZC13cmFwcGVyXCIgYXBwZWFyYW5jZT1cImZpbGxcIj5cbiAgICA8bWF0LWxhYmVsICpuZ0lmPVwib3B0aW9ucy5sYWJsZSAmJiBvcHRpb25zLmxhYmxlLmxlbmd0aCA+IDBcIj57e29wdGlvbnMubGFibGV9fTwvbWF0LWxhYmVsPlxuICAgIDxtYXQtY2hpcC1saXN0ICN0eXBlYWhlYWRDaGlwcz5cbiAgICAgIDxtYXQtY2hpcFxuICAgICAgICAqbmdGb3I9XCJsZXQgaXRlbSBvZiBvcHRpb25zLml0ZW1zU2VsZWN0ZWRcIlxuICAgICAgICBbc2VsZWN0YWJsZV09XCJmYWxzZVwiXG4gICAgICAgIFtyZW1vdmFibGVdPVwiIWRpc2FibGVkXCJcbiAgICAgICAgKHJlbW92ZWQpPVwicmVtb3ZlKGl0ZW0pXCI+XG4gICAgICAgICAge3tpdGVtLm5hbWV9fVxuICAgICAgICA8bWF0LWljb24gbWF0Q2hpcFJlbW92ZSAqbmdJZj1cIiFkaXNhYmxlZFwiPmNhbmNlbDwvbWF0LWljb24+XG4gICAgICA8L21hdC1jaGlwPlxuICAgICAgPGlucHV0XG4gICAgICAgICN0eXBlYWhlYWRJbnB1dFxuICAgICAgICAqbmdJZj1cImRpc2FibGVkID09PSBmYWxzZVwiIFxuICAgICAgICBwbGFjZWhvbGRlcj1cInt7bGFuZ3VhZ2VPYmouZW50ZXJfdGV4dH19XCJcbiAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwic2VhcmNoQm94XCJcbiAgICAgICAgW21hdEF1dG9jb21wbGV0ZV09XCJhdXRvXCJcbiAgICAgICAgW21hdENoaXBJbnB1dEZvcl09XCJ0eXBlYWhlYWRDaGlwc1wiXG4gICAgICAgIFttYXRDaGlwSW5wdXRTZXBhcmF0b3JLZXlDb2Rlc109XCJzZXBhcmF0b3JLZXlzQ29kZXNcIlxuICAgICAgPlxuICAgICAgXG4gICAgICA8ZGl2IG1hdFN1ZmZpeCBjbGFzcz1cInR5cGVhaGVhZC1zdWZmaXgtd3JhcHBlclwiICpuZ0lmPVwidGhpcy5pc1NpbmdsZUlubGluZVNlYXJjaCgpICYmIG9wdGlvbnMuaXRlbXNTZWxlY3RlZC5sZW5ndGggJiYgb3B0aW9ucy5pc0xvYWRpbmcgPT0gZmFsc2UgJiYgIWRpc2FibGVkXCIgKGNsaWNrKT1cInJlbW92ZVNpbmdsZUl0ZW0oKVwiPlxuICAgICAgICA8bWF0LWljb24+Y2xlYXI8L21hdC1pY29uPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IG1hdFN1ZmZpeCAqbmdJZj1cIm9wdGlvbnMuaXNMb2FkaW5nXCI+XG4gICAgICAgIDxtYXQtc3Bpbm5lciBbZGlhbWV0ZXJdPVwiMTVcIj48L21hdC1zcGlubmVyPlxuICAgICAgPC9kaXY+XG4gICAgPC9tYXQtY2hpcC1saXN0PlxuICAgIDxtYXQtYXV0b2NvbXBsZXRlIFxuICAgICAgI2F1dG89XCJtYXRBdXRvY29tcGxldGVcIiBcbiAgICAgIFthdXRvQWN0aXZlRmlyc3RPcHRpb25dPVwidHJ1ZVwiIFxuICAgICAgKG9wdGlvblNlbGVjdGVkKT1cInNlbGVjdCgkZXZlbnQpXCJcbiAgICAgIHNob3dQYW5lbCA9IFwidHJ1ZVwiXG4gICAgPlxuICAgICAgPG1hdC1vcHRpb24gKm5nRm9yPVwibGV0IGl0ZW0gb2Ygb3B0aW9ucy5pdGVtc1wiIFt2YWx1ZV09XCJpdGVtXCIgY2xhc3M9XCJ0eXBlYWhlYWQtbWVudS1pdGVtXCI+XG4gICAgICAgIHt7aXRlbS5uYW1lfX1cbiAgICAgIDwvbWF0LW9wdGlvbj5cbiAgICAgIDxtYXQtb3B0aW9uICpuZ0lmPVwib3B0aW9ucy5ub0RhdGFGb3VuZFwiIFt2YWx1ZV09XCJudWxsXCIgY2xhc3M9XCJ0eXBlYWhlYWQtbWVudS1pdGVtXCI+XG4gICAgICAgIHt7bGFuZ3VhZ2VPYmoubm9fZGF0YV9mb3VuZH19XG4gICAgICA8L21hdC1vcHRpb24+XG4gICAgPC9tYXQtYXV0b2NvbXBsZXRlPlxuICA8L21hdC1mb3JtLWZpZWxkPlxuPC9mb3JtPiJdfQ==