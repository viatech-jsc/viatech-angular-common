import { TypeaheadItem } from "./typeahead.item";
export class TypeaheadItemValue extends TypeaheadItem {
    constructor(name, value, data) {
        super(name, value);
        this.data = data;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLml0ZW0udmFsdWUuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy90eXBlYWhlYWQvdHlwZWFoZWFkLml0ZW0udmFsdWUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRWpELE1BQU0sT0FBTyxrQkFBdUIsU0FBUSxhQUFhO0lBS3JELFlBQVksSUFBWSxFQUFFLEtBQWEsRUFBRSxJQUFPO1FBQzVDLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbkIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7SUFDckIsQ0FBQztDQUNKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVHlwZWFoZWFkSXRlbSB9IGZyb20gXCIuL3R5cGVhaGVhZC5pdGVtXCI7XG5cbmV4cG9ydCBjbGFzcyBUeXBlYWhlYWRJdGVtVmFsdWU8VD4gIGV4dGVuZHMgVHlwZWFoZWFkSXRlbSB7XG4gICAgcHVibGljIG5hbWU6IHN0cmluZztcbiAgICBwdWJsaWMgdmFsdWU6IHN0cmluZztcbiAgICBwdWJsaWMgZGF0YTogVDtcblxuICAgIGNvbnN0cnVjdG9yKG5hbWU6IHN0cmluZywgdmFsdWU6IHN0cmluZywgZGF0YTogVCl7XG4gICAgICAgIHN1cGVyKG5hbWUsIHZhbHVlKTtcbiAgICAgICAgdGhpcy5kYXRhID0gZGF0YTtcbiAgICB9XG59Il19