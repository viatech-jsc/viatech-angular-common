import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormControl } from "@angular/forms";
import { TypeaheadOptions } from "./typeahead.options";
import { languageObj } from "./typeahead.language.config";
import { TypeaheadItem } from "./typeahead.item";
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../click_out_site/click.out.site";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
function TypeaheadComponent_span_4_li_1_span_3_Template(rf, ctx) { if (rf & 1) {
    const _r11 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span", 14);
    i0.ɵɵlistener("click", function TypeaheadComponent_span_4_li_1_span_3_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r11); const item_r7 = i0.ɵɵnextContext().$implicit; const ctx_r9 = i0.ɵɵnextContext(2); return ctx_r9.remove(item_r7); });
    i0.ɵɵelement(1, "i", 15);
    i0.ɵɵelementEnd();
} }
function TypeaheadComponent_span_4_li_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 12);
    i0.ɵɵelementStart(1, "span", 13);
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(3, TypeaheadComponent_span_4_li_1_span_3_Template, 2, 0, "span", 9);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r7 = ctx.$implicit;
    const ctx_r6 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate("title", item_r7.name);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r7.name);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r6.disabled);
} }
function TypeaheadComponent_span_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtemplate(1, TypeaheadComponent_span_4_li_1_Template, 4, 3, "li", 11);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r0.options.itemsSelected);
} }
function TypeaheadComponent_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r1.options.itemsSelected[0].name);
} }
function TypeaheadComponent_input_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "input", 17);
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵpropertyInterpolate("placeholder", ctx_r2.languageObj.enter_text);
    i0.ɵɵproperty("formControl", ctx_r2.searchBox);
} }
function TypeaheadComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 18);
    i0.ɵɵelement(1, "i", 19);
    i0.ɵɵelementEnd();
} }
function TypeaheadComponent_span_9_Template(rf, ctx) { if (rf & 1) {
    const _r13 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span", 14);
    i0.ɵɵlistener("click", function TypeaheadComponent_span_9_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r13); const ctx_r12 = i0.ɵɵnextContext(); return ctx_r12.removeSingleItem(); });
    i0.ɵɵelement(1, "i", 15);
    i0.ɵɵelementEnd();
} }
function TypeaheadComponent_ul_10_li_1_Template(rf, ctx) { if (rf & 1) {
    const _r18 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "li", 23);
    i0.ɵɵlistener("click", function TypeaheadComponent_ul_10_li_1_Template_li_click_0_listener() { i0.ɵɵrestoreView(_r18); const item_r16 = ctx.$implicit; const ctx_r17 = i0.ɵɵnextContext(2); return ctx_r17.select(item_r16); });
    i0.ɵɵelementStart(1, "span");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r16 = ctx.$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(item_r16.name);
} }
function TypeaheadComponent_ul_10_li_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li", 24);
    i0.ɵɵelementStart(1, "span");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r15 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r15.languageObj.no_data_found);
} }
function TypeaheadComponent_ul_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "ul", 20);
    i0.ɵɵtemplate(1, TypeaheadComponent_ul_10_li_1_Template, 3, 1, "li", 21);
    i0.ɵɵtemplate(2, TypeaheadComponent_ul_10_li_2_Template, 3, 1, "li", 22);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r5.options.items);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r5.options.noDataFound);
} }
export class TypeaheadComponent {
    constructor() {
        this.languageObj = languageObj;
        this.disabled = false;
        this.typeaheadLoadData = new EventEmitter();
        this.typeaheadItemSelected = new EventEmitter();
        this.searchBox = new FormControl();
        //prevent duplicate search when set value for search box
        this.isSettingValue = false;
        //super();
    }
    hide() {
        this.options.items = [];
        this.options.errorMessage = "";
        this.options.isLoading = false;
        this.options.noDataFound = false;
        if (this.options.inlineSingleSearch == false) {
            //this.isSettingValue = true;
            this.searchBox.setValue("");
        }
        else if (this.isSettingValue == false && this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            //this.isSettingValue = true;
            //this.searchBox.setValue(this.options.itemsSelected[0].name);
        }
    }
    isSingleInlineSearch() {
        return this.options.inlineSingleSearch == true && this.options.multiple == false;
    }
    select(item) {
        this.options.items = this.options.items.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        if (!this.options.multiple) {
            this.options.itemsSelected = [];
        }
        this.options.itemsSelected.push(item);
        this.options.noDataFound = this.options.items.length === 0;
        this.typeaheadItemSelected.emit(item);
        if (this.isSingleInlineSearch() == true) {
            this.isSettingValue = true;
            this.searchBox.setValue(item.name);
        }
    }
    remove(item) {
        this.options.items.push(item);
        this.options.itemsSelected = this.options.itemsSelected.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        this.options.noDataFound = this.options.items.length === 0;
    }
    removeSingleItem() {
        this.isSettingValue = true;
        this.searchBox.setValue("");
        this.options.itemsSelected = [];
        this.typeaheadItemSelected.emit(new TypeaheadItem(undefined, undefined));
    }
    ngAfterViewInit() {
        if (this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            this.searchBox.setValue(this.options.itemsSelected[0].name);
            this.isSettingValue = true;
        }
        this.searchBox.valueChanges.pipe(debounceTime(this.options.debounceTime), distinctUntilChanged()).subscribe((searchText) => {
            if (searchText.length >= this.options.minLengthToLoad && this.isSettingValue == false) {
                this.options.isLoading = true;
                this.typeaheadLoadData.emit(searchText);
            }
            this.isSettingValue = false;
        });
    }
    setSearchBoxValue(text) {
        this.searchBox.setValue(text);
        this.isSettingValue = true;
    }
}
TypeaheadComponent.ɵfac = function TypeaheadComponent_Factory(t) { return new (t || TypeaheadComponent)(); };
TypeaheadComponent.ɵcmp = i0.ɵɵdefineComponent({ type: TypeaheadComponent, selectors: [["via-typeahead"]], inputs: { languageObj: "languageObj", options: "options", disabled: "disabled" }, outputs: { typeaheadLoadData: "typeaheadLoadData", typeaheadItemSelected: "typeaheadItemSelected" }, decls: 11, vars: 6, consts: [["id", "typeahead", 3, "tohClickOutSite"], [1, "typeahead-container"], [1, "typeahead"], [1, "typeahead-items-selected"], [4, "ngIf"], ["class", "inline-disabled", 4, "ngIf"], [1, "typeahead-menu-item-input"], ["autocomplete", "off", "class", "typeahead-input", 3, "placeholder", "formControl", 4, "ngIf"], ["class", "ic-spinner", 4, "ngIf"], ["class", "ic-remove", 3, "click", 4, "ngIf"], ["class", "typeahead-menu-items z-depth-1", 4, "ngIf"], ["class", "typeahead-menu-item-selected", 4, "ngFor", "ngForOf"], [1, "typeahead-menu-item-selected"], [1, "item-display", 3, "title"], [1, "ic-remove", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-times"], [1, "inline-disabled"], ["autocomplete", "off", 1, "typeahead-input", 3, "placeholder", "formControl"], [1, "ic-spinner"], ["id", "circle-o-notch-spin", "aria-hidden", "true", 1, "fa", "fa-circle-o-notch", "fa-spin"], [1, "typeahead-menu-items", "z-depth-1"], ["class", "typeahead-menu-item", 3, "click", 4, "ngFor", "ngForOf"], ["class", "typeahead-menu-item", 4, "ngIf"], [1, "typeahead-menu-item", 3, "click"], [1, "typeahead-menu-item"]], template: function TypeaheadComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵlistener("tohClickOutSite", function TypeaheadComponent_Template_div_tohClickOutSite_0_listener() { return ctx.hide(); });
        i0.ɵɵelementStart(1, "ul", 1);
        i0.ɵɵelementStart(2, "li", 2);
        i0.ɵɵelementStart(3, "ul", 3);
        i0.ɵɵtemplate(4, TypeaheadComponent_span_4_Template, 2, 1, "span", 4);
        i0.ɵɵtemplate(5, TypeaheadComponent_span_5_Template, 2, 1, "span", 5);
        i0.ɵɵelementStart(6, "li", 6);
        i0.ɵɵtemplate(7, TypeaheadComponent_input_7_Template, 1, 2, "input", 7);
        i0.ɵɵtemplate(8, TypeaheadComponent_span_8_Template, 2, 0, "span", 8);
        i0.ɵɵtemplate(9, TypeaheadComponent_span_9_Template, 2, 0, "span", 9);
        i0.ɵɵtemplate(10, TypeaheadComponent_ul_10_Template, 3, 2, "ul", 10);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("ngIf", !ctx.options.inlineSingleSearch);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.inlineSingleSearch && ctx.disabled == true && ctx.options.itemsSelected.length);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.disabled == false);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.isLoading);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.isSingleInlineSearch() && ctx.options.itemsSelected.length && ctx.options.isLoading == false && !ctx.disabled);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.items.length > 0 || ctx.options.noDataFound);
    } }, directives: [i1.ClickOutSiteDirective, i2.NgIf, i2.NgForOf, i3.DefaultValueAccessor, i3.NgControlStatus, i3.FormControlDirective], styles: ["#typeahead[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:row;width:100%}#typeahead[_ngcontent-%COMP%]   .inline-disabled[_ngcontent-%COMP%]{padding-left:5px;padding-top:5px}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:1px 5px;z-index:1}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]{margin:0;width:100%}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;list-style:none;margin:5px 0;padding:0}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]{background:#999;border:1px solid #ebebeb;border-radius:3px;border-radius:5px;color:#fff;display:flex;flex-direction:row;margin:0 10px 0 0;max-width:200px;min-width:120px;padding:5px}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]   .item-display[_ngcontent-%COMP%]{flex:9;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:100%}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]   .ic-remove[_ngcontent-%COMP%]{flex:1;font-size:14px;text-align:right}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]{align-items:center;border:0;color:#999;display:flex;flex:1;height:34px;margin:0;width:auto}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]   .typeahead-input[_ngcontent-%COMP%]{border:0;flex:9}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]   .ic-spinner[_ngcontent-%COMP%]{flex:1;text-align:right}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:flex;flex-direction:column;left:15px;list-style:none;margin:0;max-height:300px;overflow:auto;padding:0;position:absolute;top:90%;width:calc(100% - 30px)}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]   .typeahead-menu-item[_ngcontent-%COMP%]{margin:0;padding:10px}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]   .typeahead-menu-item[_ngcontent-%COMP%]:hover{background:#f7f8f9}#typeahead[_ngcontent-%COMP%]   .typeahead-arrow[_ngcontent-%COMP%]{flex:1;text-align:right}#typeahead[_ngcontent-%COMP%]   .typeahead-arrow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#999;font-size:24px}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TypeaheadComponent, [{
        type: Component,
        args: [{
                selector: "via-typeahead",
                templateUrl: "./typeahead.html",
                styleUrls: ["./typeahead.scss"]
            }]
    }], function () { return []; }, { languageObj: [{
            type: Input
        }], options: [{
            type: Input
        }], disabled: [{
            type: Input
        }], typeaheadLoadData: [{
            type: Output
        }], typeaheadItemSelected: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdHlwZWFoZWFkL3R5cGVhaGVhZC50cyIsImNvbXBvbmVudHMvdHlwZWFoZWFkL3R5cGVhaGVhZC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDakQsT0FBTyxFQUFFLFlBQVksRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7O0lDRXBELGdDQUNnQjtJQUQwQixvUEFBc0I7SUFDaEQsd0JBQThDO0lBQ2xELGlCQUFPOzs7SUFKWCw4QkFDUTtJQUFBLGdDQUFpRDtJQUFBLFlBQWE7SUFBQSxpQkFBTztJQUNyRixpRkFDZ0I7SUFFeEIsaUJBQUs7Ozs7SUFKOEMsZUFBcUI7SUFBckIsK0NBQXFCO0lBQUMsZUFBYTtJQUFiLGtDQUFhO0lBQ3RELGVBQWlCO0lBQWpCLHVDQUFpQjs7O0lBSHpDLDRCQUNRO0lBQUEseUVBQ1E7SUFLeEIsaUJBQU87OztJQU5hLGVBQTBDO0lBQTFDLHNEQUEwQzs7O0lBTzlELGdDQUFtSDtJQUFBLFlBQWlDO0lBQUEsaUJBQU87OztJQUF4QyxlQUFpQztJQUFqQywwREFBaUM7OztJQUVoSiw0QkFDQTs7O0lBRGtELHNFQUF3QztJQUFDLDhDQUF5Qjs7O0lBQ3BILGdDQUNJO0lBQUEsd0JBQXdGO0lBQ3hGLGlCQUFPOzs7O0lBQ1gsZ0NBQ0k7SUFEaUgsOExBQTRCO0lBQzdJLHdCQUE4QztJQUM5QyxpQkFBTzs7OztJQUVQLDhCQUNJO0lBRCtELCtOQUFzQjtJQUNyRiw0QkFBTTtJQUFBLFlBQWE7SUFBQSxpQkFBTztJQUM5QixpQkFBSzs7O0lBREssZUFBYTtJQUFiLG1DQUFhOzs7SUFFdkIsOEJBQ0k7SUFBQSw0QkFBTTtJQUFBLFlBQTZCO0lBQUEsaUJBQU87SUFDOUMsaUJBQUs7OztJQURLLGVBQTZCO0lBQTdCLHVEQUE2Qjs7O0lBTDNDLDhCQUNJO0lBQUEsd0VBQ0k7SUFFSix3RUFDSTtJQUVSLGlCQUFLOzs7SUFORyxlQUFrQztJQUFsQyw4Q0FBa0M7SUFHbEMsZUFBMkI7SUFBM0IsaURBQTJCOztBRFovQyxNQUFNLE9BQU8sa0JBQWtCO0lBVzdCO1FBVlMsZ0JBQVcsR0FBRyxXQUFXLENBQUM7UUFFMUIsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUN6QixzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZDLDBCQUFxQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFFckQsY0FBUyxHQUFnQixJQUFJLFdBQVcsRUFBRSxDQUFDO1FBRTNDLHdEQUF3RDtRQUNoRCxtQkFBYyxHQUFZLEtBQUssQ0FBQztRQUV0QyxVQUFVO0lBQ1osQ0FBQztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDakMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixJQUFJLEtBQUssRUFBRTtZQUM1Qyw2QkFBNkI7WUFDN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDN0I7YUFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDbkgsNkJBQTZCO1lBQzdCLDhEQUE4RDtTQUMvRDtJQUNILENBQUM7SUFFRCxvQkFBb0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUM7SUFDbkYsQ0FBQztJQUVELE1BQU0sQ0FBQyxJQUFtQjtRQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUM1RCxPQUFPLFVBQVUsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRTtZQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7U0FDakM7UUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksSUFBSSxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNwQztJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsSUFBbUI7UUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFO1lBQzVFLE9BQU8sVUFBVSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRUQsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxhQUFhLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUVELGVBQWU7UUFDYixJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7U0FDNUI7UUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsb0JBQW9CLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFO1lBQ3pILElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLEtBQUssRUFBRTtnQkFDckYsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2dCQUM5QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3pDO1lBQ0QsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDOUIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsSUFBWTtRQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztJQUM3QixDQUFDOztvRkFsRlUsa0JBQWtCO3VEQUFsQixrQkFBa0I7UUNiL0IsOEJBQ0k7UUFEZ0IsZ0hBQW1CLFVBQU0sSUFBQztRQUMxQyw2QkFDSTtRQUFBLDZCQUNJO1FBQUEsNkJBQ0k7UUFBQSxxRUFDUTtRQU9oQixxRUFBbUg7UUFDbkgsNkJBQ0k7UUFBQSx1RUFDQTtRQUFBLHFFQUNJO1FBRUoscUVBQ0k7UUFFSixvRUFDSTtRQU9SLGlCQUFLO1FBQ0wsaUJBQUs7UUFDTCxpQkFBSztRQUNULGlCQUFLO1FBQ1QsaUJBQU07O1FBN0JnQixlQUFtQztRQUFuQyxzREFBbUM7UUFRbkIsZUFBb0Y7UUFBcEYsaUhBQW9GO1FBRXZHLGVBQXVCO1FBQXZCLDRDQUF1QjtRQUNMLGVBQXlCO1FBQXpCLDRDQUF5QjtRQUc1QyxlQUE4RztRQUE5Ryx3SUFBOEc7UUFHaEgsZUFBdUQ7UUFBdkQsOEVBQXVEOztrRERSMUQsa0JBQWtCO2NBTDlCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsV0FBVyxFQUFFLGtCQUFrQjtnQkFDL0IsU0FBUyxFQUFFLENBQUMsa0JBQWtCLENBQUM7YUFDaEM7c0NBRVUsV0FBVztrQkFBbkIsS0FBSztZQUNHLE9BQU87a0JBQWYsS0FBSztZQUNHLFFBQVE7a0JBQWhCLEtBQUs7WUFDSSxpQkFBaUI7a0JBQTFCLE1BQU07WUFDRyxxQkFBcUI7a0JBQTlCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgVHlwZWFoZWFkT3B0aW9ucyB9IGZyb20gXCIuL3R5cGVhaGVhZC5vcHRpb25zXCI7XG5pbXBvcnQgeyBsYW5ndWFnZU9iaiB9IGZyb20gXCIuL3R5cGVhaGVhZC5sYW5ndWFnZS5jb25maWdcIjtcbmltcG9ydCB7IFR5cGVhaGVhZEl0ZW0gfSBmcm9tIFwiLi90eXBlYWhlYWQuaXRlbVwiO1xuaW1wb3J0IHsgZGVib3VuY2VUaW1lLCBkaXN0aW5jdFVudGlsQ2hhbmdlZCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IGlzTmdUZW1wbGF0ZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbXBpbGVyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS10eXBlYWhlYWRcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi90eXBlYWhlYWQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vdHlwZWFoZWFkLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgVHlwZWFoZWFkQ29tcG9uZW50IHtcbiAgQElucHV0KCkgbGFuZ3VhZ2VPYmogPSBsYW5ndWFnZU9iajtcbiAgQElucHV0KCkgb3B0aW9uczogVHlwZWFoZWFkT3B0aW9ucztcbiAgQElucHV0KCkgZGlzYWJsZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQE91dHB1dCgpIHR5cGVhaGVhZExvYWREYXRhID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgdHlwZWFoZWFkSXRlbVNlbGVjdGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIHNlYXJjaEJveDogRm9ybUNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woKTtcblxuICAvL3ByZXZlbnQgZHVwbGljYXRlIHNlYXJjaCB3aGVuIHNldCB2YWx1ZSBmb3Igc2VhcmNoIGJveFxuICBwcml2YXRlIGlzU2V0dGluZ1ZhbHVlOiBib29sZWFuID0gZmFsc2U7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIC8vc3VwZXIoKTtcbiAgfVxuXG4gIGhpZGUoKTogdm9pZCB7XG4gICAgdGhpcy5vcHRpb25zLml0ZW1zID0gW107XG4gICAgdGhpcy5vcHRpb25zLmVycm9yTWVzc2FnZSA9IFwiXCI7XG4gICAgdGhpcy5vcHRpb25zLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgIHRoaXMub3B0aW9ucy5ub0RhdGFGb3VuZCA9IGZhbHNlO1xuICAgIGlmICh0aGlzLm9wdGlvbnMuaW5saW5lU2luZ2xlU2VhcmNoID09IGZhbHNlKSB7XG4gICAgICAvL3RoaXMuaXNTZXR0aW5nVmFsdWUgPSB0cnVlO1xuICAgICAgdGhpcy5zZWFyY2hCb3guc2V0VmFsdWUoXCJcIik7XG4gICAgfSBlbHNlIGlmICh0aGlzLmlzU2V0dGluZ1ZhbHVlID09IGZhbHNlICYmIHRoaXMuaXNTaW5nbGVJbmxpbmVTZWFyY2goKSA9PSB0cnVlICYmIHRoaXMub3B0aW9ucy5pdGVtc1NlbGVjdGVkLmxlbmd0aCkge1xuICAgICAgLy90aGlzLmlzU2V0dGluZ1ZhbHVlID0gdHJ1ZTtcbiAgICAgIC8vdGhpcy5zZWFyY2hCb3guc2V0VmFsdWUodGhpcy5vcHRpb25zLml0ZW1zU2VsZWN0ZWRbMF0ubmFtZSk7XG4gICAgfVxuICB9XG5cbiAgaXNTaW5nbGVJbmxpbmVTZWFyY2goKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMub3B0aW9ucy5pbmxpbmVTaW5nbGVTZWFyY2ggPT0gdHJ1ZSAmJiB0aGlzLm9wdGlvbnMubXVsdGlwbGUgPT0gZmFsc2U7XG4gIH1cblxuICBzZWxlY3QoaXRlbTogVHlwZWFoZWFkSXRlbSk6IHZvaWQge1xuICAgIHRoaXMub3B0aW9ucy5pdGVtcyA9IHRoaXMub3B0aW9ucy5pdGVtcy5maWx0ZXIoKGZpbHRlckl0ZW0pID0+IHtcbiAgICAgIHJldHVybiBmaWx0ZXJJdGVtLnZhbHVlICE9PSBpdGVtLnZhbHVlO1xuICAgIH0pO1xuICAgIGlmICghdGhpcy5vcHRpb25zLm11bHRpcGxlKSB7XG4gICAgICB0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZCA9IFtdO1xuICAgIH1cbiAgICB0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZC5wdXNoKGl0ZW0pO1xuICAgIHRoaXMub3B0aW9ucy5ub0RhdGFGb3VuZCA9IHRoaXMub3B0aW9ucy5pdGVtcy5sZW5ndGggPT09IDA7XG4gICAgdGhpcy50eXBlYWhlYWRJdGVtU2VsZWN0ZWQuZW1pdChpdGVtKTtcbiAgICBpZiAodGhpcy5pc1NpbmdsZUlubGluZVNlYXJjaCgpID09IHRydWUpIHtcbiAgICAgIHRoaXMuaXNTZXR0aW5nVmFsdWUgPSB0cnVlO1xuICAgICAgdGhpcy5zZWFyY2hCb3guc2V0VmFsdWUoaXRlbS5uYW1lKTtcbiAgICB9XG4gIH1cblxuICByZW1vdmUoaXRlbTogVHlwZWFoZWFkSXRlbSk6IHZvaWQge1xuICAgIHRoaXMub3B0aW9ucy5pdGVtcy5wdXNoKGl0ZW0pO1xuICAgIHRoaXMub3B0aW9ucy5pdGVtc1NlbGVjdGVkID0gdGhpcy5vcHRpb25zLml0ZW1zU2VsZWN0ZWQuZmlsdGVyKChmaWx0ZXJJdGVtKSA9PiB7XG4gICAgICByZXR1cm4gZmlsdGVySXRlbS52YWx1ZSAhPT0gaXRlbS52YWx1ZTtcbiAgICB9KTtcbiAgICB0aGlzLm9wdGlvbnMubm9EYXRhRm91bmQgPSB0aGlzLm9wdGlvbnMuaXRlbXMubGVuZ3RoID09PSAwO1xuICB9XG5cbiAgcmVtb3ZlU2luZ2xlSXRlbSgpOiB2b2lkIHtcbiAgICB0aGlzLmlzU2V0dGluZ1ZhbHVlID0gdHJ1ZTtcbiAgICB0aGlzLnNlYXJjaEJveC5zZXRWYWx1ZShcIlwiKTtcbiAgICB0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZCA9IFtdO1xuICAgIHRoaXMudHlwZWFoZWFkSXRlbVNlbGVjdGVkLmVtaXQobmV3IFR5cGVhaGVhZEl0ZW0odW5kZWZpbmVkLCB1bmRlZmluZWQpKTtcbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICBpZiAodGhpcy5pc1NpbmdsZUlubGluZVNlYXJjaCgpID09IHRydWUgJiYgdGhpcy5vcHRpb25zLml0ZW1zU2VsZWN0ZWQubGVuZ3RoKSB7XG4gICAgICB0aGlzLnNlYXJjaEJveC5zZXRWYWx1ZSh0aGlzLm9wdGlvbnMuaXRlbXNTZWxlY3RlZFswXS5uYW1lKTtcbiAgICAgIHRoaXMuaXNTZXR0aW5nVmFsdWUgPSB0cnVlO1xuICAgIH1cblxuICAgIHRoaXMuc2VhcmNoQm94LnZhbHVlQ2hhbmdlcy5waXBlKGRlYm91bmNlVGltZSh0aGlzLm9wdGlvbnMuZGVib3VuY2VUaW1lKSwgZGlzdGluY3RVbnRpbENoYW5nZWQoKSkuc3Vic2NyaWJlKChzZWFyY2hUZXh0KSA9PiB7XG4gICAgICBpZiAoc2VhcmNoVGV4dC5sZW5ndGggPj0gdGhpcy5vcHRpb25zLm1pbkxlbmd0aFRvTG9hZCAmJiB0aGlzLmlzU2V0dGluZ1ZhbHVlID09IGZhbHNlKSB7XG4gICAgICAgIHRoaXMub3B0aW9ucy5pc0xvYWRpbmcgPSB0cnVlO1xuICAgICAgICB0aGlzLnR5cGVhaGVhZExvYWREYXRhLmVtaXQoc2VhcmNoVGV4dCk7XG4gICAgICB9XG4gICAgICB0aGlzLmlzU2V0dGluZ1ZhbHVlID0gZmFsc2U7XG4gICAgfSk7XG4gIH1cblxuICBzZXRTZWFyY2hCb3hWYWx1ZSh0ZXh0OiBzdHJpbmcpe1xuICAgIHRoaXMuc2VhcmNoQm94LnNldFZhbHVlKHRleHQpO1xuICAgIHRoaXMuaXNTZXR0aW5nVmFsdWUgPSB0cnVlO1xuICB9XG59XG4iLCI8ZGl2IGlkPVwidHlwZWFoZWFkXCIgKHRvaENsaWNrT3V0U2l0ZSk9XCJoaWRlKClcIj5cbiAgICA8dWwgY2xhc3M9XCJ0eXBlYWhlYWQtY29udGFpbmVyXCI+XG4gICAgICAgIDxsaSBjbGFzcz1cInR5cGVhaGVhZFwiPlxuICAgICAgICAgICAgPHVsIGNsYXNzPVwidHlwZWFoZWFkLWl0ZW1zLXNlbGVjdGVkXCI+XG4gICAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCIhb3B0aW9ucy5pbmxpbmVTaW5nbGVTZWFyY2hcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaSAqbmdGb3I9XCJsZXQgaXRlbSBvZiBvcHRpb25zLml0ZW1zU2VsZWN0ZWRcIiBjbGFzcz1cInR5cGVhaGVhZC1tZW51LWl0ZW0tc2VsZWN0ZWRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJpdGVtLWRpc3BsYXlcIiB0aXRsZT1cInt7aXRlbS5uYW1lfX1cIj57e2l0ZW0ubmFtZX19PC9zcGFuPlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaWMtcmVtb3ZlXCIgKm5nSWY9XCIhZGlzYWJsZWRcIiAoY2xpY2spPVwicmVtb3ZlKGl0ZW0pXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtdGltZXNcIiBhcmlhLWhpZGRlbj1cInRydWVcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2xpPlxuICAgICAgICA8L3NwYW4+XG4gICAgICAgIDxzcGFuIGNsYXNzPVwiaW5saW5lLWRpc2FibGVkXCIgKm5nSWY9XCJvcHRpb25zLmlubGluZVNpbmdsZVNlYXJjaCAmJiBkaXNhYmxlZD09dHJ1ZSAmJiBvcHRpb25zLml0ZW1zU2VsZWN0ZWQubGVuZ3RoXCI+e3tvcHRpb25zLml0ZW1zU2VsZWN0ZWRbMF0ubmFtZX19PC9zcGFuPlxuICAgICAgICA8bGkgY2xhc3M9XCJ0eXBlYWhlYWQtbWVudS1pdGVtLWlucHV0XCI+XG4gICAgICAgICAgICA8aW5wdXQgKm5nSWY9XCJkaXNhYmxlZD09ZmFsc2VcIiBhdXRvY29tcGxldGU9XCJvZmZcIiBwbGFjZWhvbGRlcj1cInt7bGFuZ3VhZ2VPYmouZW50ZXJfdGV4dH19XCIgW2Zvcm1Db250cm9sXT1cInNlYXJjaEJveFwiIGNsYXNzPVwidHlwZWFoZWFkLWlucHV0XCI+XG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImljLXNwaW5uZXJcIiAqbmdJZj1cIm9wdGlvbnMuaXNMb2FkaW5nXCI+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1jaXJjbGUtby1ub3RjaCBmYS1zcGluXCIgaWQ9XCJjaXJjbGUtby1ub3RjaC1zcGluXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPlxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwidGhpcy5pc1NpbmdsZUlubGluZVNlYXJjaCgpICYmIG9wdGlvbnMuaXRlbXNTZWxlY3RlZC5sZW5ndGggJiYgb3B0aW9ucy5pc0xvYWRpbmcgPT0gZmFsc2UgJiYgIWRpc2FibGVkXCIgKGNsaWNrKT1cInJlbW92ZVNpbmdsZUl0ZW0oKVwiIGNsYXNzPVwiaWMtcmVtb3ZlXCI+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS10aW1lc1wiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvaT5cbiAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8dWwgKm5nSWY9XCJvcHRpb25zLml0ZW1zLmxlbmd0aCA+IDAgfHwgb3B0aW9ucy5ub0RhdGFGb3VuZFwiIGNsYXNzPVwidHlwZWFoZWFkLW1lbnUtaXRlbXMgei1kZXB0aC0xXCI+XG4gICAgICAgICAgICAgICAgPGxpICpuZ0Zvcj1cImxldCBpdGVtIG9mIG9wdGlvbnMuaXRlbXNcIiBjbGFzcz1cInR5cGVhaGVhZC1tZW51LWl0ZW1cIiAoY2xpY2spPVwic2VsZWN0KGl0ZW0pXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPnt7aXRlbS5uYW1lfX08L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGkgKm5nSWY9XCJvcHRpb25zLm5vRGF0YUZvdW5kXCIgY2xhc3M9XCJ0eXBlYWhlYWQtbWVudS1pdGVtXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPnt7bGFuZ3VhZ2VPYmoubm9fZGF0YV9mb3VuZH19PC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICA8L3VsPlxuICAgICAgICA8L2xpPlxuICAgICAgICA8L3VsPlxuICAgICAgICA8L2xpPlxuICAgIDwvdWw+XG48L2Rpdj4iXX0=