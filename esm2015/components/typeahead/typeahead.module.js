import { NgModule } from '@angular/core';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TypeaheadComponent } from './typeahead';
import { CommonModule } from '@angular/common';
import { ClickOutSiteModule } from '../click_out_site/click.out.site.module';
import * as i0 from "@angular/core";
export class TypeaheadModule {
}
TypeaheadModule.ɵmod = i0.ɵɵdefineNgModule({ type: TypeaheadModule });
TypeaheadModule.ɵinj = i0.ɵɵdefineInjector({ factory: function TypeaheadModule_Factory(t) { return new (t || TypeaheadModule)(); }, imports: [[
            NgbModule,
            ClickOutSiteModule,
            CommonModule,
            FormsModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(TypeaheadModule, { declarations: [TypeaheadComponent], imports: [NgbModule,
        ClickOutSiteModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule], exports: [TypeaheadComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(TypeaheadModule, [{
        type: NgModule,
        args: [{
                declarations: [TypeaheadComponent],
                imports: [
                    NgbModule,
                    ClickOutSiteModule,
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                exports: [TypeaheadComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL3R5cGVhaGVhZC90eXBlYWhlYWQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDakQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDOztBQWE3RSxNQUFNLE9BQU8sZUFBZTs7bURBQWYsZUFBZTs2R0FBZixlQUFlLGtCQVRqQjtZQUNQLFNBQVM7WUFDVCxrQkFBa0I7WUFDbEIsWUFBWTtZQUNaLFdBQVc7WUFDWCxtQkFBbUI7U0FDcEI7d0ZBR1UsZUFBZSxtQkFWWCxrQkFBa0IsYUFFL0IsU0FBUztRQUNULGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osV0FBVztRQUNYLG1CQUFtQixhQUVYLGtCQUFrQjtrREFFakIsZUFBZTtjQVgzQixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsa0JBQWtCLENBQUM7Z0JBQ2xDLE9BQU8sRUFBRTtvQkFDUCxTQUFTO29CQUNULGtCQUFrQjtvQkFDbEIsWUFBWTtvQkFDWixXQUFXO29CQUNYLG1CQUFtQjtpQkFDcEI7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsa0JBQWtCLENBQUM7YUFDOUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmdiTW9kdWxlIH0gZnJvbSBcIkBuZy1ib290c3RyYXAvbmctYm9vdHN0cmFwXCI7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHsgVHlwZWFoZWFkQ29tcG9uZW50IH0gZnJvbSAnLi90eXBlYWhlYWQnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IENsaWNrT3V0U2l0ZU1vZHVsZSB9IGZyb20gJy4uL2NsaWNrX291dF9zaXRlL2NsaWNrLm91dC5zaXRlLm1vZHVsZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1R5cGVhaGVhZENvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBOZ2JNb2R1bGUsXG4gICAgQ2xpY2tPdXRTaXRlTW9kdWxlLFxuICAgIENvbW1vbk1vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtUeXBlYWhlYWRDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFR5cGVhaGVhZE1vZHVsZSB7IH1cbiJdfQ==