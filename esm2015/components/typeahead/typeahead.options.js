export class TypeaheadOptions {
    constructor(url = "", multiple = true, inlineSingleSearch = false) {
        this._url = url;
        this._multiple = multiple;
        this._inlineSingleSearch = inlineSingleSearch;
        this._debounceTime = 400;
        this._itemsSelected = new Array();
        this._items = new Array();
        this._errorMessage = "";
        this._isLoading = false;
        this._minLengthToLoad = 1;
        this._noDataFound = false;
    }
    /**
     * Getter url
     * return {string}
     */
    get url() {
        return this._url;
    }
    /**
     * Getter multiple
     * return {boolean}
     */
    get multiple() {
        return this._multiple;
    }
    /**
     * Getter inlineSingleSearch
     * return {boolean}
     */
    get inlineSingleSearch() {
        return this._inlineSingleSearch;
    }
    /**
     * Getter debounceTime
     * return {number}
     */
    get debounceTime() {
        return this._debounceTime;
    }
    /**
     * Getter itemsSelected
     * return {TypeaheadItem[]}
     */
    get itemsSelected() {
        return this._itemsSelected;
    }
    /**
     * Getter items
     * return {TypeaheadItem[]}
     */
    get items() {
        return this._items;
    }
    /**
     * Getter errorMessage
     * return {string}
     */
    get errorMessage() {
        return this._errorMessage;
    }
    /**
     * Getter isLoading
     * return {boolean}
     */
    get isLoading() {
        return this._isLoading;
    }
    /**
     * Getter minLengthToLoad
     * return {number}
     */
    get minLengthToLoad() {
        return this._minLengthToLoad;
    }
    /**
     * Getter noDataFound
     * return {boolean}
     */
    get noDataFound() {
        return this._noDataFound;
    }
    /**
     * Setter url
     * param {string} value
     */
    set url(value) {
        this._url = value;
    }
    /**
     * Setter multiple
     * param {boolean} value
     */
    set multiple(value) {
        this._multiple = value;
    }
    /**
     * Setter inlineSingleSearch
     * param {boolean} value
     */
    set inlineSingleSearch(value) {
        this._inlineSingleSearch = value;
    }
    /**
     * Setter debounceTime
     * param {number} value
     */
    set debounceTime(value) {
        this._debounceTime = value;
    }
    /**
     * Setter itemsSelected
     * param {TypeaheadItem[]} value
     */
    set itemsSelected(value) {
        this._itemsSelected = value;
    }
    /**
     * Setter items
     * param {TypeaheadItem[]} value
     */
    set items(value) {
        this._items = value;
    }
    /**
     * Setter errorMessage
     * param {string} value
     */
    set errorMessage(value) {
        this._errorMessage = value;
    }
    /**
     * Setter isLoading
     * param {boolean} value
     */
    set isLoading(value) {
        this._isLoading = value;
    }
    /**
     * Setter minLengthToLoad
     * param {number} value
     */
    set minLengthToLoad(value) {
        this._minLengthToLoad = value;
    }
    /**
     * Setter noDataFound
     * param {boolean} value
     */
    set noDataFound(value) {
        this._noDataFound = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLm9wdGlvbnMuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy90eXBlYWhlYWQvdHlwZWFoZWFkLm9wdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsTUFBTSxPQUFPLGdCQUFnQjtJQWF6QixZQUFZLE1BQWMsRUFBRSxFQUFFLFdBQW9CLElBQUksRUFBRSxxQkFBOEIsS0FBSztRQUN2RixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUNoQixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsa0JBQWtCLENBQUM7UUFDOUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLEtBQUssRUFBaUIsQ0FBQztRQUNqRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksS0FBSyxFQUFpQixDQUFDO1FBQ3pDLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsR0FBRztRQUNWLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNyQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxRQUFRO1FBQ2YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLGtCQUFrQjtRQUN6QixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztJQUNwQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxZQUFZO1FBQ25CLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxhQUFhO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxLQUFLO1FBQ1osT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFlBQVk7UUFDbkIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFNBQVM7UUFDaEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQzNCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLGVBQWU7UUFDdEIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDakMsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsV0FBVztRQUNsQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsR0FBRyxDQUFDLEtBQWE7UUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsUUFBUSxDQUFDLEtBQWM7UUFDOUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsa0JBQWtCLENBQUMsS0FBYztRQUN4QyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO0lBQ3JDLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLFlBQVksQ0FBQyxLQUFhO1FBQ2pDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQy9CLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFXLGFBQWEsQ0FBQyxLQUFzQjtRQUMzQyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztJQUNoQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxLQUFLLENBQUMsS0FBc0I7UUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsWUFBWSxDQUFDLEtBQWE7UUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsU0FBUyxDQUFDLEtBQWM7UUFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUVEOzs7T0FHRztJQUNILElBQVcsZUFBZSxDQUFDLEtBQWE7UUFDcEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztJQUNsQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxXQUFXLENBQUMsS0FBYztRQUNqQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztJQUM5QixDQUFDO0NBRUoiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBUeXBlYWhlYWRJdGVtIH0gZnJvbSBcIi4vdHlwZWFoZWFkLml0ZW1cIjtcblxuZXhwb3J0IGNsYXNzIFR5cGVhaGVhZE9wdGlvbnMge1xuICAgIHByaXZhdGUgX3VybDogc3RyaW5nO1xuICAgIHByaXZhdGUgX211bHRpcGxlOiBib29sZWFuO1xuICAgIC8vaW5saW5lIHNlYXJjaCB3aXRob3V0IGNhcmQgb24gc2VhcmNoIGJveFxuICAgIHByaXZhdGUgX2lubGluZVNpbmdsZVNlYXJjaDogYm9vbGVhbjtcbiAgICBwcml2YXRlIF9kZWJvdW5jZVRpbWU6IG51bWJlcjtcbiAgICBwcml2YXRlIF9pdGVtc1NlbGVjdGVkOiBUeXBlYWhlYWRJdGVtW107XG4gICAgcHJpdmF0ZSBfaXRlbXM6IFR5cGVhaGVhZEl0ZW1bXTtcbiAgICBwcml2YXRlIF9lcnJvck1lc3NhZ2U6IHN0cmluZztcbiAgICBwcml2YXRlIF9pc0xvYWRpbmc6IGJvb2xlYW47XG4gICAgcHJpdmF0ZSBfbWluTGVuZ3RoVG9Mb2FkOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBfbm9EYXRhRm91bmQ6IGJvb2xlYW47XG5cbiAgICBjb25zdHJ1Y3Rvcih1cmw6IHN0cmluZyA9IFwiXCIsIG11bHRpcGxlOiBib29sZWFuID0gdHJ1ZSwgaW5saW5lU2luZ2xlU2VhcmNoOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICAgICAgdGhpcy5fdXJsID0gdXJsO1xuICAgICAgICB0aGlzLl9tdWx0aXBsZSA9IG11bHRpcGxlO1xuICAgICAgICB0aGlzLl9pbmxpbmVTaW5nbGVTZWFyY2ggPSBpbmxpbmVTaW5nbGVTZWFyY2g7XG4gICAgICAgIHRoaXMuX2RlYm91bmNlVGltZSA9IDQwMDtcbiAgICAgICAgdGhpcy5faXRlbXNTZWxlY3RlZCA9IG5ldyBBcnJheTxUeXBlYWhlYWRJdGVtPigpO1xuICAgICAgICB0aGlzLl9pdGVtcyA9IG5ldyBBcnJheTxUeXBlYWhlYWRJdGVtPigpO1xuICAgICAgICB0aGlzLl9lcnJvck1lc3NhZ2UgPSBcIlwiO1xuICAgICAgICB0aGlzLl9pc0xvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5fbWluTGVuZ3RoVG9Mb2FkID0gMTtcbiAgICAgICAgdGhpcy5fbm9EYXRhRm91bmQgPSBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdXJsXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG4gICAgcHVibGljIGdldCB1cmwoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX3VybDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgbXVsdGlwbGVcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIGdldCBtdWx0aXBsZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX211bHRpcGxlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBpbmxpbmVTaW5nbGVTZWFyY2hcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIGdldCBpbmxpbmVTaW5nbGVTZWFyY2goKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pbmxpbmVTaW5nbGVTZWFyY2g7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGRlYm91bmNlVGltZVxuICAgICAqIHJldHVybiB7bnVtYmVyfVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgZGVib3VuY2VUaW1lKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiB0aGlzLl9kZWJvdW5jZVRpbWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGl0ZW1zU2VsZWN0ZWRcbiAgICAgKiByZXR1cm4ge1R5cGVhaGVhZEl0ZW1bXX1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGl0ZW1zU2VsZWN0ZWQoKTogVHlwZWFoZWFkSXRlbVtdIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2l0ZW1zU2VsZWN0ZWQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGl0ZW1zXG4gICAgICogcmV0dXJuIHtUeXBlYWhlYWRJdGVtW119XG4gICAgICovXG4gICAgcHVibGljIGdldCBpdGVtcygpOiBUeXBlYWhlYWRJdGVtW10ge1xuICAgICAgICByZXR1cm4gdGhpcy5faXRlbXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGVycm9yTWVzc2FnZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuICAgIHB1YmxpYyBnZXQgZXJyb3JNZXNzYWdlKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLl9lcnJvck1lc3NhZ2U7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGlzTG9hZGluZ1xuICAgICAqIHJldHVybiB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0IGlzTG9hZGluZygpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzTG9hZGluZztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgbWluTGVuZ3RoVG9Mb2FkXG4gICAgICogcmV0dXJuIHtudW1iZXJ9XG4gICAgICovXG4gICAgcHVibGljIGdldCBtaW5MZW5ndGhUb0xvYWQoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX21pbkxlbmd0aFRvTG9hZDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgbm9EYXRhRm91bmRcbiAgICAgKiByZXR1cm4ge2Jvb2xlYW59XG4gICAgICovXG4gICAgcHVibGljIGdldCBub0RhdGFGb3VuZCgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX25vRGF0YUZvdW5kO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciB1cmxcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgdXJsKHZhbHVlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5fdXJsID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIG11bHRpcGxlXG4gICAgICogcGFyYW0ge2Jvb2xlYW59IHZhbHVlXG4gICAgICovXG4gICAgcHVibGljIHNldCBtdWx0aXBsZSh2YWx1ZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9tdWx0aXBsZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBpbmxpbmVTaW5nbGVTZWFyY2hcbiAgICAgKiBwYXJhbSB7Ym9vbGVhbn0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IGlubGluZVNpbmdsZVNlYXJjaCh2YWx1ZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9pbmxpbmVTaW5nbGVTZWFyY2ggPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgZGVib3VuY2VUaW1lXG4gICAgICogcGFyYW0ge251bWJlcn0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IGRlYm91bmNlVGltZSh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX2RlYm91bmNlVGltZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBpdGVtc1NlbGVjdGVkXG4gICAgICogcGFyYW0ge1R5cGVhaGVhZEl0ZW1bXX0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IGl0ZW1zU2VsZWN0ZWQodmFsdWU6IFR5cGVhaGVhZEl0ZW1bXSkge1xuICAgICAgICB0aGlzLl9pdGVtc1NlbGVjdGVkID0gdmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGl0ZW1zXG4gICAgICogcGFyYW0ge1R5cGVhaGVhZEl0ZW1bXX0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IGl0ZW1zKHZhbHVlOiBUeXBlYWhlYWRJdGVtW10pIHtcbiAgICAgICAgdGhpcy5faXRlbXMgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgZXJyb3JNZXNzYWdlXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IGVycm9yTWVzc2FnZSh2YWx1ZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuX2Vycm9yTWVzc2FnZSA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBpc0xvYWRpbmdcbiAgICAgKiBwYXJhbSB7Ym9vbGVhbn0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IGlzTG9hZGluZyh2YWx1ZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9pc0xvYWRpbmcgPSB2YWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgbWluTGVuZ3RoVG9Mb2FkXG4gICAgICogcGFyYW0ge251bWJlcn0gdmFsdWVcbiAgICAgKi9cbiAgICBwdWJsaWMgc2V0IG1pbkxlbmd0aFRvTG9hZCh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX21pbkxlbmd0aFRvTG9hZCA9IHZhbHVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBub0RhdGFGb3VuZFxuICAgICAqIHBhcmFtIHtib29sZWFufSB2YWx1ZVxuICAgICAqL1xuICAgIHB1YmxpYyBzZXQgbm9EYXRhRm91bmQodmFsdWU6IGJvb2xlYW4pIHtcbiAgICAgICAgdGhpcy5fbm9EYXRhRm91bmQgPSB2YWx1ZTtcbiAgICB9XG5cbn0iXX0=