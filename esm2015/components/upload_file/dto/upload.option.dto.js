export class UploadOptionDto {
    constructor(acceptFileType = [], uploadApi, downloadFileApi, language, authorization, placeHolderImg) {
        this._acceptFileType = acceptFileType;
        this._uploadApi = uploadApi;
        this._language = language;
        this._authorization = authorization;
        this._downloadFileApi = downloadFileApi;
        this._placeHolderImg = placeHolderImg;
    }
    /**
     * Getter acceptFileType
     * return {string[]}
     */
    get acceptFileType() {
        return this._acceptFileType;
    }
    /**
     * Getter uploadApi
     * return {string}
     */
    get uploadApi() {
        return this._uploadApi;
    }
    /**
     * Getter downloadFileApi
     * return {string}
     */
    get downloadFileApi() {
        return this._downloadFileApi;
    }
    /**
     * Getter language
     * return {string}
     */
    get language() {
        return this._language;
    }
    /**
     * Getter authorization
     * return {string}
     */
    get authorization() {
        return this._authorization;
    }
    /**
     * Getter placeHolderImg
     * return {string}
     */
    get placeHolderImg() {
        return this._placeHolderImg;
    }
    /**
     * Setter acceptFileType
     * param {string[]} value
     */
    set acceptFileType(value) {
        this._acceptFileType = value;
    }
    /**
     * Setter uploadApi
     * param {string} value
     */
    set uploadApi(value) {
        this._uploadApi = value;
    }
    /**
     * Setter downloadFileApi
     * param {string} value
     */
    set downloadFileApi(value) {
        this._downloadFileApi = value;
    }
    /**
     * Setter language
     * param {string} value
     */
    set language(value) {
        this._language = value;
    }
    /**
     * Setter authorization
     * param {string} value
     */
    set authorization(value) {
        this._authorization = value;
    }
    /**
     * Setter placeHolderImg
     * param {string} value
     */
    set placeHolderImg(value) {
        this._placeHolderImg = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLm9wdGlvbi5kdG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy9jb21wb25lbnRzL3VwbG9hZF9maWxlL2R0by91cGxvYWQub3B0aW9uLmR0by50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxNQUFNLE9BQU8sZUFBZTtJQVF4QixZQUFZLGlCQUEyQixFQUFFLEVBQUUsU0FBaUIsRUFBRSxlQUF1QixFQUFFLFFBQWdCLEVBQUUsYUFBcUIsRUFBRSxjQUFzQjtRQUNsSixJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQztRQUN0QyxJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztRQUM1QixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUMxQixJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQztRQUNwQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZUFBZSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxlQUFlLEdBQUcsY0FBYyxDQUFDO0lBRTFDLENBQUM7SUFFRDs7O09BR0c7SUFDTixJQUFXLGNBQWM7UUFDeEIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQzdCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFNBQVM7UUFDbkIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3hCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLGVBQWU7UUFDekIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDOUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsUUFBUTtRQUNsQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDdkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsYUFBYTtRQUN2QixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDNUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsY0FBYztRQUN4QixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDN0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsY0FBYyxDQUFDLEtBQWU7UUFDeEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsU0FBUyxDQUFDLEtBQWE7UUFDakMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsZUFBZSxDQUFDLEtBQWE7UUFDdkMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxRQUFRLENBQUMsS0FBYTtRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxhQUFhLENBQUMsS0FBYTtRQUNyQyxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxjQUFjLENBQUMsS0FBYTtRQUN0QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUM5QixDQUFDO0NBRUQiLCJzb3VyY2VzQ29udGVudCI6WyJcbmV4cG9ydCBjbGFzcyBVcGxvYWRPcHRpb25EdG8ge1xuICAgIHByaXZhdGUgX2FjY2VwdEZpbGVUeXBlOiBzdHJpbmdbXTtcbiAgICBwcml2YXRlIF91cGxvYWRBcGk6IHN0cmluZztcbiAgICBwcml2YXRlIF9kb3dubG9hZEZpbGVBcGk6IHN0cmluZztcbiAgICBwcml2YXRlIF9sYW5ndWFnZTogc3RyaW5nO1xuICAgIHByaXZhdGUgX2F1dGhvcml6YXRpb246IHN0cmluZztcbiAgICBwcml2YXRlIF9wbGFjZUhvbGRlckltZzogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IoYWNjZXB0RmlsZVR5cGU6IHN0cmluZ1tdID0gW10sIHVwbG9hZEFwaTogc3RyaW5nLCBkb3dubG9hZEZpbGVBcGk6IHN0cmluZywgbGFuZ3VhZ2U6IHN0cmluZywgYXV0aG9yaXphdGlvbjogc3RyaW5nLCBwbGFjZUhvbGRlckltZzogc3RyaW5nKXtcbiAgICAgICAgdGhpcy5fYWNjZXB0RmlsZVR5cGUgPSBhY2NlcHRGaWxlVHlwZTtcbiAgICAgICAgdGhpcy5fdXBsb2FkQXBpID0gdXBsb2FkQXBpO1xuICAgICAgICB0aGlzLl9sYW5ndWFnZSA9IGxhbmd1YWdlO1xuICAgICAgICB0aGlzLl9hdXRob3JpemF0aW9uID0gYXV0aG9yaXphdGlvbjtcbiAgICAgICAgdGhpcy5fZG93bmxvYWRGaWxlQXBpID0gZG93bmxvYWRGaWxlQXBpO1xuICAgICAgICB0aGlzLl9wbGFjZUhvbGRlckltZyA9IHBsYWNlSG9sZGVySW1nO1xuXG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGFjY2VwdEZpbGVUeXBlXG4gICAgICogcmV0dXJuIHtzdHJpbmdbXX1cbiAgICAgKi9cblx0cHVibGljIGdldCBhY2NlcHRGaWxlVHlwZSgpOiBzdHJpbmdbXSB7XG5cdFx0cmV0dXJuIHRoaXMuX2FjY2VwdEZpbGVUeXBlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdXBsb2FkQXBpXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgdXBsb2FkQXBpKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3VwbG9hZEFwaTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGRvd25sb2FkRmlsZUFwaVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGRvd25sb2FkRmlsZUFwaSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9kb3dubG9hZEZpbGVBcGk7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBsYW5ndWFnZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGxhbmd1YWdlKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX2xhbmd1YWdlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgYXV0aG9yaXphdGlvblxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGF1dGhvcml6YXRpb24oKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fYXV0aG9yaXphdGlvbjtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHBsYWNlSG9sZGVySW1nXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgcGxhY2VIb2xkZXJJbWcoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fcGxhY2VIb2xkZXJJbWc7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBhY2NlcHRGaWxlVHlwZVxuICAgICAqIHBhcmFtIHtzdHJpbmdbXX0gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBhY2NlcHRGaWxlVHlwZSh2YWx1ZTogc3RyaW5nW10pIHtcblx0XHR0aGlzLl9hY2NlcHRGaWxlVHlwZSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgdXBsb2FkQXBpXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCB1cGxvYWRBcGkodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX3VwbG9hZEFwaSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgZG93bmxvYWRGaWxlQXBpXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBkb3dubG9hZEZpbGVBcGkodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX2Rvd25sb2FkRmlsZUFwaSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgbGFuZ3VhZ2VcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGxhbmd1YWdlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9sYW5ndWFnZSA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgYXV0aG9yaXphdGlvblxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgYXV0aG9yaXphdGlvbih2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fYXV0aG9yaXphdGlvbiA9IHZhbHVlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgcGxhY2VIb2xkZXJJbWdcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHBsYWNlSG9sZGVySW1nKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9wbGFjZUhvbGRlckltZyA9IHZhbHVlO1xuXHR9XG5cbn0iXX0=