export class UploadResponseDto {
    constructor(responseType = "", message = "", code = "") {
        this._message = message;
        this._responseType = responseType;
        this._code = code;
    }
    /**
     * Getter responseType
     * return {string}
     */
    get responseType() {
        return this._responseType;
    }
    /**
     * Getter message
     * return {string}
     */
    get message() {
        return this._message;
    }
    /**
     * Getter code
     * return {string}
     */
    get code() {
        return this._code;
    }
    /**
     * Setter responseType
     * param {string} value
     */
    set responseType(value) {
        this._responseType = value;
    }
    /**
     * Setter message
     * param {string} value
     */
    set message(value) {
        this._message = value;
    }
    /**
     * Setter code
     * param {string} value
     */
    set code(value) {
        this._code = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLnJlc3BvbnNlLmR0by5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdXBsb2FkX2ZpbGUvZHRvL3VwbG9hZC5yZXNwb25zZS5kdG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxPQUFPLGlCQUFpQjtJQUsxQixZQUFZLGVBQXVCLEVBQUUsRUFBRSxVQUFrQixFQUFFLEVBQUUsT0FBZSxFQUFFO1FBQzFFLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ3RCLENBQUM7SUFFRDs7O09BR0c7SUFDTixJQUFXLFlBQVk7UUFDdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzNCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLE9BQU87UUFDakIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3RCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLElBQUk7UUFDZCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsWUFBWSxDQUFDLEtBQWE7UUFDcEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsT0FBTyxDQUFDLEtBQWE7UUFDL0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsSUFBSSxDQUFDLEtBQWE7UUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDcEIsQ0FBQztDQUdEIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFVwbG9hZFJlc3BvbnNlRHRvIHtcbiAgICBwcml2YXRlIF9yZXNwb25zZVR5cGU6IHN0cmluZztcbiAgICBwcml2YXRlIF9tZXNzYWdlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfY29kZTogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IocmVzcG9uc2VUeXBlOiBzdHJpbmcgPSBcIlwiLCBtZXNzYWdlOiBzdHJpbmcgPSBcIlwiLCBjb2RlOiBzdHJpbmcgPSBcIlwiKSB7XG4gICAgICAgIHRoaXMuX21lc3NhZ2UgPSBtZXNzYWdlO1xuICAgICAgICB0aGlzLl9yZXNwb25zZVR5cGUgPSByZXNwb25zZVR5cGU7XG4gICAgICAgIHRoaXMuX2NvZGUgPSBjb2RlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciByZXNwb25zZVR5cGVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCByZXNwb25zZVR5cGUoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fcmVzcG9uc2VUeXBlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgbWVzc2FnZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IG1lc3NhZ2UoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fbWVzc2FnZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGNvZGVcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBjb2RlKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX2NvZGU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciByZXNwb25zZVR5cGVcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IHJlc3BvbnNlVHlwZSh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fcmVzcG9uc2VUeXBlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBtZXNzYWdlXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBtZXNzYWdlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9tZXNzYWdlID0gdmFsdWU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIFNldHRlciBjb2RlXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBjb2RlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9jb2RlID0gdmFsdWU7XG5cdH1cblxuXG59Il19