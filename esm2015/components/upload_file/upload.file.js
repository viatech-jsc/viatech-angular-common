import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { ImageCropperComponent } from "ngx-image-cropper";
import { humanizeBytes } from "ngx-uploader";
import { UploadOption } from "./upload.option";
import { UPLOAD_FILE_EVENT, ErrorObj, uploadRatio, uploadType, allowedContentTypesUploadImg, UPLOAD_FILE_LANGUAGE_CONFIG } from './upload.file.config';
import { UploadResponse } from './upload.response';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "@angular/common";
import * as i3 from "ngx-image-cropper";
import * as i4 from "ngx-uploader";
import * as i5 from "@ng-bootstrap/ng-bootstrap";
function UploadFileComponent_div_0_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r6 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 6);
    i0.ɵɵelementStart(1, "image-cropper", 7);
    i0.ɵɵlistener("imageCropped", function UploadFileComponent_div_0_div_1_Template_image_cropper_imageCropped_1_listener($event) { i0.ɵɵrestoreView(_r6); const ctx_r5 = i0.ɵɵnextContext(2); return ctx_r5.imageCropped($event); })("imageLoaded", function UploadFileComponent_div_0_div_1_Template_image_cropper_imageLoaded_1_listener() { i0.ɵɵrestoreView(_r6); const ctx_r7 = i0.ɵɵnextContext(2); return ctx_r7.imageLoaded(); })("loadImageFailed", function UploadFileComponent_div_0_div_1_Template_image_cropper_loadImageFailed_1_listener() { i0.ɵɵrestoreView(_r6); const ctx_r8 = i0.ɵɵnextContext(2); return ctx_r8.loadImageFailed(); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(2, "div");
    i0.ɵɵelementStart(3, "button", 8);
    i0.ɵɵlistener("click", function UploadFileComponent_div_0_div_1_Template_button_click_3_listener() { i0.ɵɵrestoreView(_r6); const ctx_r9 = i0.ɵɵnextContext(2); return ctx_r9.uploadCroppedImage(); });
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "button", 9);
    i0.ɵɵlistener("click", function UploadFileComponent_div_0_div_1_Template_button_click_5_listener() { i0.ɵɵrestoreView(_r6); const ctx_r10 = i0.ɵɵnextContext(2); return ctx_r10.clearUploadCroppedImage(); });
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("maintainAspectRatio", true)("imageQuality", 100)("imageChangedEvent", ctx_r2.imageChangedEvent)("autoCrop", true)("maintainAspectRatio", true)("aspectRatio", ctx_r2.getRatio());
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate1(" ", ctx_r2.uploadlanguageObj.crop_image_yes, " ");
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1(" ", ctx_r2.uploadlanguageObj.crop_image_cancel, " ");
} }
function UploadFileComponent_div_0_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r13 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 10);
    i0.ɵɵelementStart(1, "div", 11);
    i0.ɵɵelementStart(2, "div", 12);
    i0.ɵɵelementStart(3, "img", 13, 14);
    i0.ɵɵlistener("error", function UploadFileComponent_div_0_div_2_Template_img_error_3_listener() { i0.ɵɵrestoreView(_r13); const _r11 = i0.ɵɵreference(4); const ctx_r12 = i0.ɵɵnextContext(2); _r11.error = null; return _r11.src = ctx_r12.uploadOption.placeHolderImg; });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("src", ctx_r3.uploadOption.downloadFileApi + ctx_r3.fileName, i0.ɵɵsanitizeUrl);
} }
function UploadFileComponent_div_0_div_3_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 22);
    i0.ɵɵelement(1, "i", 23);
    i0.ɵɵelementEnd();
} }
function UploadFileComponent_div_0_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r16 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 15);
    i0.ɵɵelementStart(1, "label", 16);
    i0.ɵɵtemplate(2, UploadFileComponent_div_0_div_3_div_2_Template, 2, 0, "div", 17);
    i0.ɵɵelementStart(3, "div", 18);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "div", 19);
    i0.ɵɵtext(6);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(7, "br");
    i0.ɵɵelementStart(8, "span", 20);
    i0.ɵɵtext(9);
    i0.ɵɵelementEnd();
    i0.ɵɵelement(10, "br");
    i0.ɵɵelementStart(11, "input", 21);
    i0.ɵɵlistener("change", function UploadFileComponent_div_0_div_3_Template_input_change_11_listener($event) { i0.ɵɵrestoreView(_r16); const ctx_r15 = i0.ɵɵnextContext(2); return ctx_r15.fileChangeEvent($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r4.fileName);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate1("", ctx_r4.uploadlanguageObj.upload_file_drag_and_drop, " ");
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(ctx_r4.uploadlanguageObj.upload_file_or);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate1(" ", ctx_r4.uploadlanguageObj.upload_file_browse_file, " ");
    i0.ɵɵadvance(2);
    i0.ɵɵpropertyInterpolate("accept", ctx_r4.options.allowedContentTypes);
} }
function UploadFileComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 2);
    i0.ɵɵtemplate(1, UploadFileComponent_div_0_div_1_Template, 7, 8, "div", 3);
    i0.ɵɵtemplate(2, UploadFileComponent_div_0_div_2_Template, 5, 1, "div", 4);
    i0.ɵɵtemplate(3, UploadFileComponent_div_0_div_3_Template, 12, 5, "div", 5);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵclassMapInterpolate1("ratio-", ctx_r0.ratio, " cropper-upload");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r0.imageChangedEvent && ctx_r0.options);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r0.imageChangedEvent && ctx_r0.fileName);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r0.imageChangedEvent && ctx_r0.options);
} }
function UploadFileComponent_div_1_div_1_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 29);
    i0.ɵɵelement(1, "ngb-progressbar", 30);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const f_r22 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("value", f_r22 == null ? null : f_r22.progress == null ? null : f_r22.progress.data == null ? null : f_r22.progress.data.percentage);
} }
function UploadFileComponent_div_1_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 10);
    i0.ɵɵelementStart(1, "div", 11);
    i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_1_div_1_div_2_Template, 2, 1, "div", 28);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r20 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r20.isUploadFilesDone());
} }
function UploadFileComponent_div_1_div_1_div_2_img_3_Template(rf, ctx) { if (rf & 1) {
    const _r29 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "img", 13, 32);
    i0.ɵɵlistener("error", function UploadFileComponent_div_1_div_1_div_2_img_3_Template_img_error_0_listener() { i0.ɵɵrestoreView(_r29); const _r27 = i0.ɵɵreference(1); const ctx_r28 = i0.ɵɵnextContext(4); _r27.error = null; return _r27.src = ctx_r28.uploadOption.placeHolderImg; });
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r26 = i0.ɵɵnextContext(4);
    i0.ɵɵproperty("src", ctx_r26.uploadOption.downloadFileApi + ctx_r26.getImgUrl(), i0.ɵɵsanitizeUrl);
} }
function UploadFileComponent_div_1_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 10);
    i0.ɵɵelementStart(1, "div", 11);
    i0.ɵɵelementStart(2, "div", 12);
    i0.ɵɵtemplate(3, UploadFileComponent_div_1_div_1_div_2_img_3_Template, 2, 1, "img", 31);
    i0.ɵɵelementEnd();
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r21 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", !ctx_r21.hidePreview);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r21.fileId, " ");
} }
function UploadFileComponent_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_1_div_1_Template, 3, 1, "div", 27);
    i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_1_div_2_Template, 5, 2, "div", 4);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r17 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r17.files);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r17.multiple && (ctx_r17.fileId || ctx_r17.fileName));
} }
function UploadFileComponent_div_1_div_2_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r30 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r30.uploadlanguageObj.upload_file_drag_and_drop, " ");
} }
function UploadFileComponent_div_1_div_2_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 19);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r31 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r31.uploadlanguageObj.upload_file_or);
} }
function UploadFileComponent_div_1_div_2_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 38);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r32 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r32.uploadlanguageObj.upload_file_browse_files, " ");
} }
function UploadFileComponent_div_1_div_2_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 38);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r33 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r33.uploadlanguageObj.upload_file_change, " ");
} }
function UploadFileComponent_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r35 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_2_span_1_Template, 2, 1, "span", 33);
    i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_2_span_2_Template, 2, 1, "span", 34);
    i0.ɵɵelementStart(3, "label", 35);
    i0.ɵɵelementStart(4, "input", 36);
    i0.ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_div_2_Template_input_uploadOutput_4_listener($event) { i0.ɵɵrestoreView(_r35); const ctx_r34 = i0.ɵɵnextContext(2); return ctx_r34.onUploadOutput($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(5, UploadFileComponent_div_1_div_2_span_5_Template, 2, 1, "span", 37);
    i0.ɵɵtemplate(6, UploadFileComponent_div_1_div_2_span_6_Template, 2, 1, "span", 37);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r18 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r18.isUploadFilesDone());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r18.isUploadFilesDone());
    i0.ɵɵadvance(2);
    i0.ɵɵpropertyInterpolate("accept", ctx_r18.options.allowedContentTypes);
    i0.ɵɵproperty("options", ctx_r18.options)("uploadInput", ctx_r18.uploadInput);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r18.isUploadFilesDone());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r18.isUploadFilesDone());
} }
function UploadFileComponent_div_1_div_3_div_1_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r38 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r38.uploadlanguageObj.upload_file_drag_and_drop, " ");
} }
function UploadFileComponent_div_1_div_3_div_1_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 19);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r39 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r39.uploadlanguageObj.upload_file_or);
} }
function UploadFileComponent_div_1_div_3_div_1_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 38);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r40 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r40.uploadlanguageObj.upload_file_browse_file, " ");
} }
function UploadFileComponent_div_1_div_3_div_1_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 38);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r41 = i0.ɵɵnextContext(4);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r41.uploadlanguageObj.upload_file_change, " ");
} }
function UploadFileComponent_div_1_div_3_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r43 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_3_div_1_span_1_Template, 2, 1, "span", 33);
    i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_3_div_1_span_2_Template, 2, 1, "span", 34);
    i0.ɵɵelementStart(3, "label", 35);
    i0.ɵɵelementStart(4, "input", 40);
    i0.ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_div_3_div_1_Template_input_uploadOutput_4_listener($event) { i0.ɵɵrestoreView(_r43); const ctx_r42 = i0.ɵɵnextContext(3); return ctx_r42.onUploadOutput($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(5, UploadFileComponent_div_1_div_3_div_1_span_5_Template, 2, 1, "span", 37);
    i0.ɵɵtemplate(6, UploadFileComponent_div_1_div_3_div_1_span_6_Template, 2, 1, "span", 37);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r36 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r36.isUploadFilesDone());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r36.isUploadFilesDone());
    i0.ɵɵadvance(2);
    i0.ɵɵpropertyInterpolate("accept", ctx_r36.options.allowedContentTypes);
    i0.ɵɵproperty("options", ctx_r36.options)("uploadInput", ctx_r36.uploadInput);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r36.isUploadFilesDone());
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r36.isUploadFilesDone());
} }
function UploadFileComponent_div_1_div_3_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r45 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 15);
    i0.ɵɵelementStart(1, "label", 16);
    i0.ɵɵelementStart(2, "span", 20);
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵtext(4);
    i0.ɵɵelementStart(5, "input", 40);
    i0.ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_div_3_div_2_Template_input_uploadOutput_5_listener($event) { i0.ɵɵrestoreView(_r45); const ctx_r44 = i0.ɵɵnextContext(3); return ctx_r44.onUploadOutput($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r37 = i0.ɵɵnextContext(3);
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate1(" ", ctx_r37.uploadlanguageObj.upload_file_browse_file, " ");
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", ctx_r37.fileId, " ");
    i0.ɵɵadvance(1);
    i0.ɵɵpropertyInterpolate("accept", ctx_r37.options.allowedContentTypes);
    i0.ɵɵproperty("options", ctx_r37.options)("uploadInput", ctx_r37.uploadInput);
} }
function UploadFileComponent_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 39);
    i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_3_div_1_Template, 7, 7, "div", 25);
    i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_3_div_2_Template, 6, 5, "div", 5);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r19 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r19.uploadType === "IMAGE");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r19.uploadType === "ALL");
} }
const _c0 = function (a0) { return { "is-drop-over": a0 }; };
function UploadFileComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r47 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 24);
    i0.ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_Template_div_uploadOutput_0_listener($event) { i0.ɵɵrestoreView(_r47); const ctx_r46 = i0.ɵɵnextContext(); return ctx_r46.onUploadOutput($event); });
    i0.ɵɵtemplate(1, UploadFileComponent_div_1_div_1_Template, 3, 2, "div", 25);
    i0.ɵɵtemplate(2, UploadFileComponent_div_1_div_2_Template, 7, 7, "div", 25);
    i0.ɵɵtemplate(3, UploadFileComponent_div_1_div_3_Template, 3, 2, "div", 26);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵproperty("options", ctx_r1.options)("uploadInput", ctx_r1.uploadInput)("ngClass", i0.ɵɵpureFunction1(6, _c0, ctx_r1.dragOver));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.uploadType === "IMAGE");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r1.multiple);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !ctx_r1.multiple);
} }
export class UploadFileComponent {
    constructor(http) {
        //super();
        this.http = http;
        this.uploadQueue = new Array();
        this.totalDoneUploaded = 0;
        //crop img
        this.imageChangedEvent = '';
        this.originalImage = '';
        this.uploadRatioConfig = uploadRatio;
        this.multiple = false;
        this.hidePreview = false;
        this.uploadType = uploadType.IMAGE;
        this.fileSize = 1024 * 1024 * 10; //5MB
        this.uploadlanguageObj = UPLOAD_FILE_LANGUAGE_CONFIG;
        this.response = new EventEmitter();
        this.uploadFileEVent = new EventEmitter();
        this.files = [];
        this.uploadInput = new EventEmitter();
        this.humanizeBytes = humanizeBytes;
        this.errorMessage = "";
    }
    ngAfterViewInit() {
        setTimeout(() => {
            this.options = {
                concurrency: 1,
                maxUploads: 9999,
                allowedContentTypes: this.uploadOption.acceptFileType.length ? this.uploadOption.acceptFileType : allowedContentTypesUploadImg
            };
        }, 0);
    }
    isValid(obj) {
        return obj !== null && obj !== undefined;
    }
    parseErrorResponse(response) {
        let errors = new Array();
        if (this.isValid(response.error) && this.isValid(response.error.error) && response.error.error instanceof Array) {
            let arr = response.error.error;
            for (let i = 0; i < arr.length; i++) {
                errors.push(new ErrorObj(arr[i].field, arr[i].message));
            }
        }
        else if (response.status !== 401) {
            errors.push(new ErrorObj(null, "serverError"));
        }
        return errors;
    }
    getRatio() {
        return uploadRatio[this.ratio].ratio;
    }
    getImgUrl() {
        return this.fileName ? this.fileName + "&" + new Date().getTime() : undefined;
    }
    onUploadOutput(output) {
        if (output.type === "allAddedToQueue" && this.uploadQueue.length) {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.START_UPLOAD));
            this.uploadQueue.forEach((element, index) => {
                this.startUpload(element);
            });
        }
        else if (output.type === "addedToQueue" && typeof output.file !== "undefined") {
            if (output.file.size > this.fileSize) {
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
            }
            else {
                if (this.multiple) {
                    this.files.push(output.file);
                }
                else {
                    this.files = [output.file];
                }
                this.uploadQueue.push(output.file);
            }
        }
        else if (output.type === "uploading" && typeof output.file !== "undefined") {
            if (this.multiple) {
                const index = this.files.findIndex(file => typeof output.file !== "undefined" && file.id === output.file.id);
                this.files[index] = output.file;
            }
        }
        else if (output.type === "removed") {
            if (this.multiple) {
                this.files = this.files.filter((file) => file !== output.file);
            }
            else {
                this.files = [];
            }
        }
        else if (output.type === "dragOver") {
            this.dragOver = true;
        }
        else if (output.type === "dragOut") {
            this.dragOver = false;
        }
        else if (output.type === "drop") {
            this.dragOver = false;
        }
        else if (output.type === "done") {
            this.checkResponse(output);
        }
        else if (output.type === "rejected") {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.type"));
        }
    }
    checkResponse(output) {
        this.totalDoneUploaded = this.totalDoneUploaded + 1;
        if (this.isValid(output.file.response.error) && output.file.response.error instanceof Array && output.file.responseStatus !== 200) {
            let arr = output.file.response.error;
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, arr[0].code, output.file.responseStatus));
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
            this.uploadQueue = [];
        }
        else if (output.file.response.result || output.file.responseStatus === 200) {
            if (!this.multiple) {
                this.uploadQueue = [];
                this.fileId = output.file.response.result;
                this.fileName = output.file.response.result;
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, output.file.response.result, output.file.responseStatus));
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
            }
            else if (this.totalDoneUploaded === this.uploadQueue.length) {
                this.uploadQueue = [];
                this.totalDoneUploaded = 0;
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
            }
        }
        else {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error", output.file.responseStatus));
            this.files = [];
            this.uploadQueue = [];
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
        }
    }
    isUploadFilesDone() {
        for (let i = 0; i < this.files.length; i++) {
            return this.files[i].progress.data.percentage === 100;
        }
        return false;
    }
    getFilesUploaded() {
        return this.files;
    }
    startUpload(file) {
        this.fileId = "";
        const event = {
            type: "uploadFile",
            url: this.uploadOption.uploadApi.replace(":fileType", this.uploadType),
            method: "POST",
            file: file,
            headers: {
                language: this.uploadOption.language,
                Authorization: this.uploadOption.authorization
            }
        };
        this.uploadInput.emit(event);
    }
    cancelUpload(id) {
        this.uploadInput.emit({ type: "cancel", id: id });
    }
    removeFile(id) {
        this.uploadInput.emit({ type: "remove", id: id });
    }
    removeAllFiles() {
        this.uploadInput.emit({ type: "removeAll" });
    }
    // IMAGE CROPPER
    fileChangeEvent(event) {
        if (event.srcElement.files[0].size > this.fileSize) {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
        }
        else {
            this.imageChangedEvent = event;
            //this.originalImage = event.srcElement.files;
        }
    }
    imageCropped(event) {
        //this.croppedImage = event.base64;
        this.cropImgObject = event;
        // crop image again if current cropped image's width is greater than resizeToWidth config
        if (event.width > uploadRatio[this.ratio].resizeToWidth) {
            this.imageCropper.resizeToWidth = uploadRatio[this.ratio].resizeToWidth;
            this.imageCropper.crop();
            this.imageCropper.resizeToWidth = 0;
        }
    }
    imageLoaded() {
        // show cropper
    }
    loadImageFailed() {
        // show message
    }
    clearUploadCroppedImage() {
        //this.originalImage = new Array();
        this.cropImgObject = undefined;
        this.imageChangedEvent = undefined;
    }
    uploadCroppedImage() {
        //upload cropped image
        if (this.cropImgObject) {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.START_UPLOAD));
            let formData = new FormData();
            formData.append('file', this.cropImgObject.file);
            let headers = new HttpHeaders();
            headers.append('language', this.uploadOption.language);
            headers.append('Authorization', this.uploadOption.authorization);
            this.http.post(this.uploadOption.uploadApi.replace(":fileType", this.uploadType), formData, { headers: headers }).subscribe((result) => {
                let res = result;
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, res.result));
                this.clearUploadCroppedImage();
                this.fileName = result.message;
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
            }, (errorRes) => {
                let errors = this.parseErrorResponse(errorRes);
                if (errors.length > 0 && errors[0].message.trim().length > 0) {
                    this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, errors[0].message.trim()));
                }
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
            });
        }
    }
}
UploadFileComponent.ɵfac = function UploadFileComponent_Factory(t) { return new (t || UploadFileComponent)(i0.ɵɵdirectiveInject(i1.HttpClient)); };
UploadFileComponent.ɵcmp = i0.ɵɵdefineComponent({ type: UploadFileComponent, selectors: [["via-upload-file"]], viewQuery: function UploadFileComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(ImageCropperComponent, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.imageCropper = _t.first);
    } }, inputs: { multiple: "multiple", hidePreview: "hidePreview", uploadType: "uploadType", fileName: "fileName", fileSize: "fileSize", uploadOption: "uploadOption", uploadlanguageObj: "uploadlanguageObj", ratio: "ratio" }, outputs: { response: "response", uploadFileEVent: "uploadFileEVent" }, decls: 2, vars: 2, consts: [["id", "upload-file", 3, "class", 4, "ngIf"], ["id", "upload-file", "class", "drop-container", "ngFileDrop", "", 3, "options", "uploadInput", "ngClass", "uploadOutput", 4, "ngIf"], ["id", "upload-file"], ["class", "img-cropper-wrapper", "style", "width: 100%;", 4, "ngIf"], ["class", "upload-item", 4, "ngIf"], ["class", "upload-file-container", 4, "ngIf"], [1, "img-cropper-wrapper", 2, "width", "100%"], ["outputType", "file", 3, "maintainAspectRatio", "imageQuality", "imageChangedEvent", "autoCrop", "aspectRatio", "imageCropped", "imageLoaded", "loadImageFailed"], ["type", "button", 1, "btn", "uppercase-text", "btn-primary", "modal-action-btn", 3, "click"], ["type", "button", 1, "btn", "uppercase-text", "btn-light", "modal-action-btn", 3, "click"], [1, "upload-item"], [1, "upload-item-content"], [1, "image-uploaded"], [3, "src", "error"], ["img", ""], [1, "upload-file-container"], [1, "upload-button-file"], ["id", "upload-icon", 4, "ngIf"], [1, "message-drag-and-drop"], [1, "message-or"], [1, "btn-button-file"], ["onclick", "this.value=null", "type", "file", 1, "input-upload", 3, "accept", "change"], ["id", "upload-icon"], ["aria-hidden", "true", 1, "fa", "fa-cloud-upload", "fa-5x"], ["id", "upload-file", "ngFileDrop", "", 1, "drop-container", 3, "options", "uploadInput", "ngClass", "uploadOutput"], [4, "ngIf"], ["style", "width: 100%;", 4, "ngIf"], ["class", "upload-item", 4, "ngFor", "ngForOf"], ["class", "progress-content", 4, "ngIf"], [1, "progress-content"], ["showValue", "true", "type", "success", 3, "value"], [3, "src", "error", 4, "ngIf"], ["image", ""], ["class", "message-drag-and-drop", 4, "ngIf"], ["class", "message-or", 4, "ngIf"], [1, "upload-button"], ["onclick", "this.value=null", "type", "file", "ngFileSelect", "", "multiple", "", 1, "input-upload", 3, "accept", "options", "uploadInput", "uploadOutput"], ["class", "btn-button", 4, "ngIf"], [1, "btn-button"], [2, "width", "100%"], ["onclick", "this.value=null", "type", "file", "ngFileSelect", "", 1, "input-upload", 3, "accept", "options", "uploadInput", "uploadOutput"]], template: function UploadFileComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, UploadFileComponent_div_0_Template, 4, 6, "div", 0);
        i0.ɵɵtemplate(1, UploadFileComponent_div_1_Template, 4, 8, "div", 1);
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", ctx.ratio);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options && !ctx.ratio);
    } }, directives: [i2.NgIf, i3.ImageCropperComponent, i4.NgFileDropDirective, i2.NgClass, i2.NgForOf, i5.NgbProgressbar, i4.NgFileSelectDirective], styles: ["upload-file[_ngcontent-%COMP%]{width:100%}#upload-file[_ngcontent-%COMP%]{align-items:center;border:2px dashed #c4cdd9;border-radius:3px;display:flex;flex-direction:column;overflow:hidden;text-align:center;width:100%}#upload-file[_ngcontent-%COMP%]   .drop-container[_ngcontent-%COMP%]{padding:30px 10px}#upload-file[_ngcontent-%COMP%]   .message-drag-and-drop[_ngcontent-%COMP%]{font-weight:700}#upload-file[_ngcontent-%COMP%]   .message-or[_ngcontent-%COMP%]{color:#999;display:block;margin-top:20px}#upload-file[_ngcontent-%COMP%]   .upload-button[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:column;width:100%}#upload-file[_ngcontent-%COMP%]   .upload-button[_ngcontent-%COMP%]   .input-upload[_ngcontent-%COMP%]{height:0;visibility:hidden;width:0}#upload-file[_ngcontent-%COMP%]   .upload-button[_ngcontent-%COMP%]   .btn-button[_ngcontent-%COMP%]{background:#9a0000;border-radius:.25rem;color:#fff;cursor:pointer;font-size:12px;font-weight:700;max-width:200px;padding:10px}#upload-file[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]{display:flex;display:inline-block;flex-direction:column;margin:10px;text-align:center;width:100%}#upload-file[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]   .input-upload[_ngcontent-%COMP%]{height:0;visibility:hidden;width:0}#upload-file[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]   .btn-button-file[_ngcontent-%COMP%]{background:#9a0000;border-radius:.25rem;color:#fff;cursor:pointer;font-size:12px;font-weight:700;max-width:200px;padding:10px}#upload-file[_ngcontent-%COMP%]   .upload-file-container[_ngcontent-%COMP%]{padding-bottom:10px}#upload-file[_ngcontent-%COMP%]   .message-error[_ngcontent-%COMP%]{margin-bottom:5px;text-align:left;width:80%}#upload-file[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]{margin:20px auto;width:100%}#upload-file[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]   .upload-item-content[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]{padding:15px;text-align:center}#upload-file[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]   .upload-item-content[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:100%}#upload-file[_ngcontent-%COMP%]   .multiple-upload-thumbnail[_ngcontent-%COMP%]{display:inline-block;width:100px}#upload-file.cropper-upload[_ngcontent-%COMP%]{min-height:250px}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]{margin-bottom:0!important}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]{margin:0;margin-bottom:0!important}#upload-file.cropper-upload[_ngcontent-%COMP%]   .message-or[_ngcontent-%COMP%]{margin-top:0!important}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]{margin-top:0}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-file-container[_ngcontent-%COMP%]{padding:0}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-file-container[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]{margin-bottom:10px}#upload-file.cropper-upload[_ngcontent-%COMP%]   #upload-icon[_ngcontent-%COMP%]{color:hsla(0,0%,50.2%,.1803921568627451);margin-top:100px}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]{max-height:515px;width:100%}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]{height:100%;padding-bottom:10px}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]   image-cropper[_ngcontent-%COMP%]{max-height:440px}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-height:390px;max-width:100%;width:auto!important}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]{max-height:730px;width:100%}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]{height:100%;padding-bottom:10px}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]   image-cropper[_ngcontent-%COMP%]{max-height:550px}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-height:600px;max-width:100%;width:auto!important}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]{max-height:700px;width:100%}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]{height:100%;padding-bottom:10px}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]   image-cropper[_ngcontent-%COMP%]{max-height:650px}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-height:550px;max-width:100%;width:auto!important}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UploadFileComponent, [{
        type: Component,
        args: [{
                selector: "via-upload-file",
                templateUrl: "./upload.file.html",
                styleUrls: ["./upload.file.scss"]
            }]
    }], function () { return [{ type: i1.HttpClient }]; }, { imageCropper: [{
            type: ViewChild,
            args: [ImageCropperComponent]
        }], multiple: [{
            type: Input
        }], hidePreview: [{
            type: Input
        }], uploadType: [{
            type: Input
        }], fileName: [{
            type: Input
        }], fileSize: [{
            type: Input
        }], uploadOption: [{
            type: Input
        }], uploadlanguageObj: [{
            type: Input
        }], ratio: [{
            type: Input
        }], response: [{
            type: Output
        }], uploadFileEVent: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLmZpbGUuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy91cGxvYWRfZmlsZS91cGxvYWQuZmlsZS50cyIsImNvbXBvbmVudHMvdXBsb2FkX2ZpbGUvdXBsb2FkLmZpbGUuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsRixPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUUxRCxPQUFPLEVBQUUsYUFBYSxFQUEwRCxNQUFNLGNBQWMsQ0FBQztBQUNyRyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLDRCQUE0QixFQUFFLDJCQUEyQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDdkosT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBcUIsVUFBVSxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDOzs7Ozs7Ozs7SUNMOUUsOEJBQ0k7SUFBQSx3Q0FFZ0I7SUFGcUwsaU9BQXFDLHFNQUFBLGlOQUFBO0lBRTFPLGlCQUFnQjtJQUNoQiwyQkFBSztJQUFBLGlDQUNHO0lBRDJFLHNNQUE4QjtJQUN6RyxZQUNKO0lBQUEsaUJBQVM7SUFDVCxpQ0FDSTtJQUR3RSw2TUFBbUM7SUFDM0csWUFDSjtJQUFBLGlCQUFTO0lBQUEsaUJBQU07SUFDdkIsaUJBQU07OztJQVRhLGVBQTRCO0lBQTVCLDBDQUE0QixxQkFBQSwrQ0FBQSxrQkFBQSw2QkFBQSxrQ0FBQTtJQUluQyxlQUNKO0lBREksd0VBQ0o7SUFFSSxlQUNKO0lBREksMkVBQ0o7Ozs7SUFHUiwrQkFDSTtJQUFBLCtCQUNJO0lBQUEsK0JBQ0k7SUFBQSxtQ0FDSjtJQUR5RCw0TUFBbUIsSUFBSSwyREFBdUM7SUFBbkgsaUJBQ0o7SUFBQSxpQkFBTTtJQUNWLGlCQUFNO0lBQ1YsaUJBQU07OztJQUhXLGVBQStDO0lBQS9DLDZGQUErQzs7O0lBT3hELCtCQUF3QztJQUFBLHdCQUEyRDtJQUFBLGlCQUFNOzs7O0lBRmpILCtCQUNJO0lBQUEsaUNBQ0k7SUFBQSxpRkFBd0M7SUFDeEMsK0JBQW1DO0lBQUEsWUFDbkM7SUFBQSxpQkFBTTtJQUNOLCtCQUF3QjtJQUFBLFlBQXFDO0lBQUEsaUJBQU07SUFDbkUscUJBQ0E7SUFBQSxnQ0FBK0I7SUFBQSxZQUFnRDtJQUFBLGlCQUFPO0lBQ3RGLHNCQUNBO0lBQUEsa0NBRUo7SUFEUSxvTkFBa0M7SUFEdEMsaUJBRUo7SUFBQSxpQkFBUTtJQUNaLGlCQUFNOzs7SUFWTyxlQUFpQjtJQUFqQix1Q0FBaUI7SUFDYSxlQUNuQztJQURtQyxrRkFDbkM7SUFDd0IsZUFBcUM7SUFBckMsNkRBQXFDO0lBRTlCLGVBQWdEO0lBQWhELGlGQUFnRDtJQUViLGVBQXdDO0lBQXhDLHNFQUF3Qzs7O0lBOUJ0SCw4QkFDSTtJQUFBLDBFQUNJO0lBV0osMEVBQ0k7SUFPSiwyRUFDSTtJQVlSLGlCQUFNOzs7SUFsQ2dCLG9FQUFzQztJQUNuRCxlQUFvQztJQUFwQyxpRUFBb0M7SUFZaEIsZUFBc0M7SUFBdEMsbUVBQXNDO0lBUTFELGVBQXFDO0lBQXJDLGtFQUFxQzs7O0lBb0I5QiwrQkFDSTtJQUFBLHNDQUNrQjtJQUN0QixpQkFBTTs7O0lBRitDLGVBQXVDO0lBQXZDLGtKQUF1Qzs7O0lBSHBHLCtCQUNJO0lBQUEsK0JBQ0k7SUFBQSx1RkFDSTtJQUdSLGlCQUFNO0lBQ1YsaUJBQU07OztJQUxnQyxlQUE0QjtJQUE1QixtREFBNEI7Ozs7SUFTdEQsbUNBQ0o7SUFEaUYsd05BQXFCLElBQUksMkRBQXlDO0lBQS9JLGlCQUNKOzs7SUFEOEIsa0dBQWtEOzs7SUFIeEYsK0JBQ0k7SUFBQSwrQkFDSTtJQUFBLCtCQUNJO0lBQUEsdUZBQ0o7SUFBQSxpQkFBTTtJQUNOLFlBQ0o7SUFBQSxpQkFBTTtJQUNWLGlCQUFNOzs7SUFKVyxlQUFvQjtJQUFwQiwyQ0FBb0I7SUFFN0IsZUFDSjtJQURJLCtDQUNKOzs7SUFmUiwyQkFDSTtJQUFBLGlGQUNJO0lBT0osZ0ZBQ0k7SUFPUixpQkFBTTs7O0lBaEJ1QixlQUF1QztJQUF2Qyx1Q0FBdUM7SUFRdkMsZUFBeUM7SUFBekMsZ0ZBQXlDOzs7SUFXbEUsZ0NBQWlFO0lBQUEsWUFDakU7SUFBQSxpQkFBTzs7O0lBRDBELGVBQ2pFO0lBRGlFLG1GQUNqRTs7O0lBRUEsZ0NBQXNEO0lBQUEsWUFBcUM7SUFBQSxpQkFBTzs7O0lBQTVDLGVBQXFDO0lBQXJDLDhEQUFxQzs7O0lBS3ZGLGdDQUF1RDtJQUFBLFlBQ3ZEO0lBQUEsaUJBQU87OztJQURnRCxlQUN2RDtJQUR1RCxtRkFDdkQ7OztJQUVBLGdDQUFzRDtJQUFBLFlBQTJDO0lBQUEsaUJBQU87OztJQUFsRCxlQUEyQztJQUEzQyw2RUFBMkM7Ozs7SUFaekcsMkJBQ0k7SUFBQSxtRkFBaUU7SUFHakUsbUZBQXNEO0lBRXRELGlDQUNJO0lBQUEsaUNBRUE7SUFEd0IsOE5BQXVDO0lBRC9ELGlCQUVBO0lBQUEsbUZBQXVEO0lBR3ZELG1GQUFzRDtJQUMxRCxpQkFBUTtJQUNaLGlCQUFNOzs7SUFiSSxlQUE0QjtJQUE1QixtREFBNEI7SUFHNUIsZUFBNEI7SUFBNUIsbURBQTRCO0lBR3dCLGVBQXdDO0lBQXhDLHVFQUF3QztJQUMxRix5Q0FBbUIsb0NBQUE7SUFDakIsZUFBNEI7SUFBNUIsbURBQTRCO0lBRzVCLGVBQTJCO0lBQTNCLGtEQUEyQjs7O0lBS2pDLGdDQUFpRTtJQUFBLFlBRWpFO0lBQUEsaUJBQU87OztJQUYwRCxlQUVqRTtJQUZpRSxtRkFFakU7OztJQUVBLGdDQUFzRDtJQUFBLFlBQXFDO0lBQUEsaUJBQU87OztJQUE1QyxlQUFxQztJQUFyQyw4REFBcUM7OztJQUl2RixnQ0FBdUQ7SUFBQSxZQUN2RDtJQUFBLGlCQUFPOzs7SUFEZ0QsZUFDdkQ7SUFEdUQsa0ZBQ3ZEOzs7SUFFQSxnQ0FBc0Q7SUFBQSxZQUEyQztJQUFBLGlCQUFPOzs7SUFBbEQsZUFBMkM7SUFBM0MsNkVBQTJDOzs7O0lBWnpHLDJCQUNJO0lBQUEseUZBQWlFO0lBSWpFLHlGQUFzRDtJQUN0RCxpQ0FDSTtJQUFBLGlDQUVBO0lBRHdCLG9PQUF1QztJQUQvRCxpQkFFQTtJQUFBLHlGQUF1RDtJQUd2RCx5RkFBc0Q7SUFDMUQsaUJBQVE7SUFDWixpQkFBTTs7O0lBYkksZUFBNEI7SUFBNUIsbURBQTRCO0lBSTVCLGVBQTRCO0lBQTVCLG1EQUE0QjtJQUV3QixlQUF3QztJQUF4Qyx1RUFBd0M7SUFDMUYseUNBQW1CLG9DQUFBO0lBQ2pCLGVBQTRCO0lBQTVCLG1EQUE0QjtJQUc1QixlQUEyQjtJQUEzQixrREFBMkI7Ozs7SUFHekMsK0JBQ0k7SUFBQSxpQ0FDSTtJQUFBLGdDQUErQjtJQUFBLFlBQWdEO0lBQUEsaUJBQU87SUFDdEYsWUFDQTtJQUFBLGlDQUVKO0lBRDRCLG9PQUF1QztJQUQvRCxpQkFFSjtJQUFBLGlCQUFRO0lBQ1osaUJBQU07OztJQUxpQyxlQUFnRDtJQUFoRCxrRkFBZ0Q7SUFDL0UsZUFDQTtJQURBLCtDQUNBO0lBQXNELGVBQXdDO0lBQXhDLHVFQUF3QztJQUMxRix5Q0FBbUIsb0NBQUE7OztJQXJCbkMsK0JBQ0k7SUFBQSxpRkFDSTtJQWNKLGdGQUNJO0lBT1IsaUJBQU07OztJQXZCRyxlQUE4QjtJQUE5QixxREFBOEI7SUFlOUIsZUFBNEI7SUFBNUIsbURBQTRCOzs7OztJQW5EekMsK0JBQ0k7SUFEa0cscU5BQXVDO0lBQ3pJLDJFQUNJO0lBa0JKLDJFQUNJO0lBY0osMkVBQ0k7SUF3QlIsaUJBQU07OztJQTVENEUsd0NBQW1CLG1DQUFBLHdEQUFBO0lBQzVGLGVBQThCO0lBQTlCLG9EQUE4QjtJQW1COUIsZUFBZ0I7SUFBaEIsc0NBQWdCO0lBZWhCLGVBQWlCO0lBQWpCLHVDQUFpQjs7QUQzRDFCLE1BQU0sT0FBTyxtQkFBbUI7SUFnQzlCLFlBQW9CLElBQWdCO1FBQ2xDLFVBQVU7UUFEUSxTQUFJLEdBQUosSUFBSSxDQUFZO1FBckI1QixnQkFBVyxHQUFpQixJQUFJLEtBQUssRUFBYyxDQUFDO1FBQ3BELHNCQUFpQixHQUFXLENBQUMsQ0FBQztRQUV0QyxVQUFVO1FBQ1Ysc0JBQWlCLEdBQVEsRUFBRSxDQUFDO1FBQzVCLGtCQUFhLEdBQVEsRUFBRSxDQUFDO1FBSXhCLHNCQUFpQixHQUFHLFdBQVcsQ0FBQztRQUN2QixhQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGVBQVUsR0FBVyxVQUFVLENBQUMsS0FBSyxDQUFDO1FBRXRDLGFBQVEsR0FBVyxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQyxDQUFBLEtBQUs7UUFFekMsc0JBQWlCLEdBQUcsMkJBQTJCLENBQUM7UUFFL0MsYUFBUSxHQUFpQyxJQUFJLFlBQVksRUFBa0IsQ0FBQztRQUM1RSxvQkFBZSxHQUF5QixJQUFJLFlBQVksRUFBVSxDQUFDO1FBSzNFLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxZQUFZLEVBQWUsQ0FBQztRQUNuRCxJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztRQUNuQyxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRUQsZUFBZTtRQUNiLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLENBQUMsT0FBTyxHQUFHO2dCQUNiLFdBQVcsRUFBRSxDQUFDO2dCQUNkLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixtQkFBbUIsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyw0QkFBNEI7YUFDL0gsQ0FBQztRQUNKLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFFRCxPQUFPLENBQUMsR0FBUTtRQUNkLE9BQU8sR0FBRyxLQUFLLElBQUksSUFBSSxHQUFHLEtBQUssU0FBUyxDQUFDO0lBQzNDLENBQUM7SUFFTSxrQkFBa0IsQ0FBQyxRQUEyQjtRQUNuRCxJQUFJLE1BQU0sR0FBZSxJQUFJLEtBQUssRUFBWSxDQUFDO1FBQy9DLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxZQUFZLEtBQUssRUFBRTtZQUMvRyxJQUFJLEdBQUcsR0FBZSxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUMzQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbkMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2FBQ3pEO1NBQ0Y7YUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO1lBQ2xDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxRQUFRLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUM7U0FDaEQ7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRUQsUUFBUTtRQUNOLE9BQU8sV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdkMsQ0FBQztJQUVELFNBQVM7UUFDUCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztJQUNoRixDQUFDO0lBRUQsY0FBYyxDQUFDLE1BQW9CO1FBQ2pDLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRTtZQUNoRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUMxQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1NBQ0o7YUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssY0FBYyxJQUFJLE9BQU8sTUFBTSxDQUFDLElBQUksS0FBSyxXQUFXLEVBQUU7WUFDL0UsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsd0NBQXdDLENBQUMsQ0FBQyxDQUFDO2FBQ2xIO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtvQkFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUM5QjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUM1QjtnQkFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDcEM7U0FDRjthQUFNLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxXQUFXLElBQUksT0FBTyxNQUFNLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTtZQUM1RSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxNQUFNLENBQUMsSUFBSSxLQUFLLFdBQVcsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzdHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQzthQUNqQztTQUNGO2FBQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtZQUNwQyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFnQixFQUFFLEVBQUUsQ0FBQyxJQUFJLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzVFO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2FBQ2pCO1NBQ0Y7YUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUFFO1lBQ3JDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO2FBQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtZQUNwQyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztTQUN2QjthQUFNLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUU7WUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7U0FDdkI7YUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssTUFBTSxFQUFFO1lBQ2pDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDNUI7YUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUFFO1lBQ3JDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUFDLGlCQUFpQixDQUFDLFlBQVksRUFBRSxxQ0FBcUMsQ0FBQyxDQUFDLENBQUM7U0FDL0c7SUFFSCxDQUFDO0lBRUQsYUFBYSxDQUFDLE1BQU07UUFDbEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLENBQUM7UUFDcEQsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssWUFBWSxLQUFLLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLEtBQUssR0FBRyxFQUFFO1lBQ2pJLElBQUksR0FBRyxHQUFlLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztZQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDaEgsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxjQUFjLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFLEVBQUUsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDeEcsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7U0FDdkI7YUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsS0FBSyxHQUFHLEVBQUU7WUFDNUUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2dCQUN0QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztnQkFDMUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BJLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxFQUFFLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2FBQ3pHO2lCQUFNLElBQUksSUFBSSxDQUFDLGlCQUFpQixLQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFO2dCQUM3RCxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxjQUFjLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFLEVBQUUsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7YUFDekc7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxjQUFjLENBQUMsaUJBQWlCLENBQUMsWUFBWSxFQUFFLG1CQUFtQixFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUN4SCxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUNoQixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztTQUN6RztJQUNILENBQUM7SUFFRCxpQkFBaUI7UUFDZixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDMUMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLEdBQUcsQ0FBQztTQUN2RDtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELGdCQUFnQjtRQUNkLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNwQixDQUFDO0lBRUQsV0FBVyxDQUFDLElBQWdCO1FBQzFCLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLE1BQU0sS0FBSyxHQUFnQjtZQUN6QixJQUFJLEVBQUUsWUFBWTtZQUNsQixHQUFHLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3RFLE1BQU0sRUFBRSxNQUFNO1lBQ2QsSUFBSSxFQUFFLElBQUk7WUFDVixPQUFPLEVBQUU7Z0JBQ1AsUUFBUSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUTtnQkFDcEMsYUFBYSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYTthQUMvQztTQUNGLENBQUM7UUFFRixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsWUFBWSxDQUFDLEVBQVU7UUFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxVQUFVLENBQUMsRUFBVTtRQUNuQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELGNBQWM7UUFDWixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCxnQkFBZ0I7SUFDaEIsZUFBZSxDQUFDLEtBQVU7UUFDeEIsSUFBSSxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNsRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsd0NBQXdDLENBQUMsQ0FBQyxDQUFDO1NBQ2xIO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1lBQy9CLDhDQUE4QztTQUMvQztJQUNILENBQUM7SUFFRCxZQUFZLENBQUMsS0FBd0I7UUFDbkMsbUNBQW1DO1FBQ25DLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBRTNCLHlGQUF5RjtRQUN6RixJQUFJLEtBQUssQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxhQUFhLEVBQUU7WUFDdkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxhQUFhLENBQUM7WUFDeEUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7U0FDckM7SUFDSCxDQUFDO0lBRUQsV0FBVztRQUNULGVBQWU7SUFDakIsQ0FBQztJQUVELGVBQWU7UUFDYixlQUFlO0lBQ2pCLENBQUM7SUFFRCx1QkFBdUI7UUFDckIsbUNBQW1DO1FBQ25DLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLENBQUM7SUFDckMsQ0FBQztJQUVELGtCQUFrQjtRQUNoQixzQkFBc0I7UUFDdEIsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDdkUsSUFBSSxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztZQUM5QixRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRWpELElBQUksT0FBTyxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7WUFDaEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN2RCxPQUFPLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBRWpFLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQVcsRUFBRSxFQUFFO2dCQUMxSSxJQUFJLEdBQUcsR0FBRyxNQUFNLENBQUM7Z0JBQ2pCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUN2RixJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDO2dCQUMvQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQzFFLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUNkLElBQUksTUFBTSxHQUFvQixJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2hFLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUM1RCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ2xHO2dCQUNELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDMUUsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7O3NGQXJQVSxtQkFBbUI7d0RBQW5CLG1CQUFtQjt1QkFDbkIscUJBQXFCOzs7OztRQ2RsQyxvRUFDSTtRQW9DSixvRUFDSTs7UUF0Q3lELGdDQUFhO1FBcUNyRSxlQUF5QjtRQUF6QixnREFBeUI7O2tERHhCakIsbUJBQW1CO2NBTC9CLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQixXQUFXLEVBQUUsb0JBQW9CO2dCQUNqQyxTQUFTLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQzthQUNsQzs2REFFbUMsWUFBWTtrQkFBN0MsU0FBUzttQkFBQyxxQkFBcUI7WUFvQnZCLFFBQVE7a0JBQWhCLEtBQUs7WUFDRyxXQUFXO2tCQUFuQixLQUFLO1lBQ0csVUFBVTtrQkFBbEIsS0FBSztZQUNHLFFBQVE7a0JBQWhCLEtBQUs7WUFDRyxRQUFRO2tCQUFoQixLQUFLO1lBQ0csWUFBWTtrQkFBcEIsS0FBSztZQUNHLGlCQUFpQjtrQkFBekIsS0FBSztZQUNHLEtBQUs7a0JBQWIsS0FBSztZQUNJLFFBQVE7a0JBQWpCLE1BQU07WUFDRyxlQUFlO2tCQUF4QixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBJbWFnZUNyb3BwZXJDb21wb25lbnQgfSBmcm9tIFwibmd4LWltYWdlLWNyb3BwZXJcIjtcbmltcG9ydCB7IEltYWdlQ3JvcHBlZEV2ZW50IH0gZnJvbSBcIm5neC1pbWFnZS1jcm9wcGVyL3NyYy9pbWFnZS1jcm9wcGVyLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgaHVtYW5pemVCeXRlcywgVXBsb2FkZXJPcHRpb25zLCBVcGxvYWRGaWxlLCBVcGxvYWRJbnB1dCwgVXBsb2FkT3V0cHV0IH0gZnJvbSBcIm5neC11cGxvYWRlclwiO1xuaW1wb3J0IHsgVXBsb2FkT3B0aW9uIH0gZnJvbSBcIi4vdXBsb2FkLm9wdGlvblwiO1xuaW1wb3J0IHsgVVBMT0FEX0ZJTEVfRVZFTlQsIEVycm9yT2JqLCB1cGxvYWRSYXRpbywgdXBsb2FkVHlwZSwgYWxsb3dlZENvbnRlbnRUeXBlc1VwbG9hZEltZywgVVBMT0FEX0ZJTEVfTEFOR1VBR0VfQ09ORklHIH0gZnJvbSAnLi91cGxvYWQuZmlsZS5jb25maWcnO1xuaW1wb3J0IHsgVXBsb2FkUmVzcG9uc2UgfSBmcm9tICcuL3VwbG9hZC5yZXNwb25zZSc7XG5pbXBvcnQgeyBIdHRwRXJyb3JSZXNwb25zZSwgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcInZpYS11cGxvYWQtZmlsZVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL3VwbG9hZC5maWxlLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL3VwbG9hZC5maWxlLnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgVXBsb2FkRmlsZUNvbXBvbmVudCB7XG4gIEBWaWV3Q2hpbGQoSW1hZ2VDcm9wcGVyQ29tcG9uZW50KSBpbWFnZUNyb3BwZXI6IEltYWdlQ3JvcHBlckNvbXBvbmVudDtcbiAgb3B0aW9uczogVXBsb2FkZXJPcHRpb25zO1xuICBmb3JtRGF0YTogRm9ybURhdGE7XG4gIGZpbGVzOiBVcGxvYWRGaWxlW107XG4gIHVwbG9hZElucHV0OiBFdmVudEVtaXR0ZXI8VXBsb2FkSW5wdXQ+O1xuICBodW1hbml6ZUJ5dGVzOiBGdW5jdGlvbjtcbiAgZHJhZ092ZXI6IGJvb2xlYW47XG4gIGVycm9yTWVzc2FnZTogc3RyaW5nO1xuICBmaWxlSWQ6IHN0cmluZztcblxuICBwcml2YXRlIHVwbG9hZFF1ZXVlOiBVcGxvYWRGaWxlW10gPSBuZXcgQXJyYXk8VXBsb2FkRmlsZT4oKTtcbiAgcHJpdmF0ZSB0b3RhbERvbmVVcGxvYWRlZDogbnVtYmVyID0gMDtcblxuICAvL2Nyb3AgaW1nXG4gIGltYWdlQ2hhbmdlZEV2ZW50OiBhbnkgPSAnJztcbiAgb3JpZ2luYWxJbWFnZTogYW55ID0gJyc7XG4gIC8vY3JvcHBlZEltYWdlOiBhbnkgPSAnJztcbiAgY3JvcEltZ09iamVjdDogSW1hZ2VDcm9wcGVkRXZlbnQ7XG4gIHJhdGlvU3RyOiBzdHJpbmc7XG4gIHVwbG9hZFJhdGlvQ29uZmlnID0gdXBsb2FkUmF0aW87XG4gIEBJbnB1dCgpIG11bHRpcGxlOiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpIGhpZGVQcmV2aWV3OiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpIHVwbG9hZFR5cGU6IHN0cmluZyA9IHVwbG9hZFR5cGUuSU1BR0U7XG4gIEBJbnB1dCgpIGZpbGVOYW1lO1xuICBASW5wdXQoKSBmaWxlU2l6ZTogbnVtYmVyID0gMTAyNCAqIDEwMjQgKiAxMDsvLzVNQlxuICBASW5wdXQoKSB1cGxvYWRPcHRpb246IFVwbG9hZE9wdGlvbjtcbiAgQElucHV0KCkgdXBsb2FkbGFuZ3VhZ2VPYmogPSBVUExPQURfRklMRV9MQU5HVUFHRV9DT05GSUc7XG4gIEBJbnB1dCgpIHJhdGlvOiBzdHJpbmc7XG4gIEBPdXRwdXQoKSByZXNwb25zZTogRXZlbnRFbWl0dGVyPFVwbG9hZFJlc3BvbnNlPiA9IG5ldyBFdmVudEVtaXR0ZXI8VXBsb2FkUmVzcG9uc2U+KCk7XG4gIEBPdXRwdXQoKSB1cGxvYWRGaWxlRVZlbnQ6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7XG4gICAgLy9zdXBlcigpO1xuXG4gICAgdGhpcy5maWxlcyA9IFtdO1xuICAgIHRoaXMudXBsb2FkSW5wdXQgPSBuZXcgRXZlbnRFbWl0dGVyPFVwbG9hZElucHV0PigpO1xuICAgIHRoaXMuaHVtYW5pemVCeXRlcyA9IGh1bWFuaXplQnl0ZXM7XG4gICAgdGhpcy5lcnJvck1lc3NhZ2UgPSBcIlwiO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgdGhpcy5vcHRpb25zID0ge1xuICAgICAgICBjb25jdXJyZW5jeTogMSxcbiAgICAgICAgbWF4VXBsb2FkczogOTk5OSxcbiAgICAgICAgYWxsb3dlZENvbnRlbnRUeXBlczogdGhpcy51cGxvYWRPcHRpb24uYWNjZXB0RmlsZVR5cGUubGVuZ3RoID8gdGhpcy51cGxvYWRPcHRpb24uYWNjZXB0RmlsZVR5cGUgOiBhbGxvd2VkQ29udGVudFR5cGVzVXBsb2FkSW1nXG4gICAgICB9O1xuICAgIH0sIDApO1xuICB9XG5cbiAgaXNWYWxpZChvYmo6IGFueSk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBvYmogIT09IG51bGwgJiYgb2JqICE9PSB1bmRlZmluZWQ7XG4gIH1cblxuICBwdWJsaWMgcGFyc2VFcnJvclJlc3BvbnNlKHJlc3BvbnNlOiBIdHRwRXJyb3JSZXNwb25zZSk6IEFycmF5PEVycm9yT2JqPiB7XG4gICAgbGV0IGVycm9yczogRXJyb3JPYmpbXSA9IG5ldyBBcnJheTxFcnJvck9iaj4oKTtcbiAgICBpZiAodGhpcy5pc1ZhbGlkKHJlc3BvbnNlLmVycm9yKSAmJiB0aGlzLmlzVmFsaWQocmVzcG9uc2UuZXJyb3IuZXJyb3IpICYmIHJlc3BvbnNlLmVycm9yLmVycm9yIGluc3RhbmNlb2YgQXJyYXkpIHtcbiAgICAgIGxldCBhcnI6IEFycmF5PGFueT4gPSByZXNwb25zZS5lcnJvci5lcnJvcjtcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYXJyLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGVycm9ycy5wdXNoKG5ldyBFcnJvck9iaihhcnJbaV0uZmllbGQsIGFycltpXS5tZXNzYWdlKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChyZXNwb25zZS5zdGF0dXMgIT09IDQwMSkge1xuICAgICAgZXJyb3JzLnB1c2gobmV3IEVycm9yT2JqKG51bGwsIFwic2VydmVyRXJyb3JcIikpO1xuICAgIH1cbiAgICByZXR1cm4gZXJyb3JzO1xuICB9XG5cbiAgZ2V0UmF0aW8oKSB7XG4gICAgcmV0dXJuIHVwbG9hZFJhdGlvW3RoaXMucmF0aW9dLnJhdGlvO1xuICB9XG5cbiAgZ2V0SW1nVXJsKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuZmlsZU5hbWUgPyB0aGlzLmZpbGVOYW1lICsgXCImXCIgKyBuZXcgRGF0ZSgpLmdldFRpbWUoKSA6IHVuZGVmaW5lZDtcbiAgfVxuXG4gIG9uVXBsb2FkT3V0cHV0KG91dHB1dDogVXBsb2FkT3V0cHV0KTogdm9pZCB7XG4gICAgaWYgKG91dHB1dC50eXBlID09PSBcImFsbEFkZGVkVG9RdWV1ZVwiICYmIHRoaXMudXBsb2FkUXVldWUubGVuZ3RoKSB7XG4gICAgICB0aGlzLnJlc3BvbnNlLmVtaXQobmV3IFVwbG9hZFJlc3BvbnNlKFVQTE9BRF9GSUxFX0VWRU5ULlNUQVJUX1VQTE9BRCkpO1xuICAgICAgdGhpcy51cGxvYWRRdWV1ZS5mb3JFYWNoKChlbGVtZW50LCBpbmRleCkgPT4ge1xuICAgICAgICB0aGlzLnN0YXJ0VXBsb2FkKGVsZW1lbnQpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIGlmIChvdXRwdXQudHlwZSA9PT0gXCJhZGRlZFRvUXVldWVcIiAmJiB0eXBlb2Ygb3V0cHV0LmZpbGUgIT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgIGlmIChvdXRwdXQuZmlsZS5zaXplID4gdGhpcy5maWxlU2l6ZSkge1xuICAgICAgICB0aGlzLnJlc3BvbnNlLmVtaXQobmV3IFVwbG9hZFJlc3BvbnNlKFVQTE9BRF9GSUxFX0VWRU5ULlVQTE9BRF9FUlJPUiwgXCJ1cGxvYWQuZmlsZS5lcnJvci5tZXNzYWdlLmZpbGUubWF4U2l6ZVwiKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAodGhpcy5tdWx0aXBsZSkge1xuICAgICAgICAgIHRoaXMuZmlsZXMucHVzaChvdXRwdXQuZmlsZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5maWxlcyA9IFtvdXRwdXQuZmlsZV07XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy51cGxvYWRRdWV1ZS5wdXNoKG91dHB1dC5maWxlKTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKG91dHB1dC50eXBlID09PSBcInVwbG9hZGluZ1wiICYmIHR5cGVvZiBvdXRwdXQuZmlsZSAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgaWYgKHRoaXMubXVsdGlwbGUpIHtcbiAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLmZpbGVzLmZpbmRJbmRleChmaWxlID0+IHR5cGVvZiBvdXRwdXQuZmlsZSAhPT0gXCJ1bmRlZmluZWRcIiAmJiBmaWxlLmlkID09PSBvdXRwdXQuZmlsZS5pZCk7XG4gICAgICAgIHRoaXMuZmlsZXNbaW5kZXhdID0gb3V0cHV0LmZpbGU7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChvdXRwdXQudHlwZSA9PT0gXCJyZW1vdmVkXCIpIHtcbiAgICAgIGlmICh0aGlzLm11bHRpcGxlKSB7XG4gICAgICAgIHRoaXMuZmlsZXMgPSB0aGlzLmZpbGVzLmZpbHRlcigoZmlsZTogVXBsb2FkRmlsZSkgPT4gZmlsZSAhPT0gb3V0cHV0LmZpbGUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5maWxlcyA9IFtdO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAob3V0cHV0LnR5cGUgPT09IFwiZHJhZ092ZXJcIikge1xuICAgICAgdGhpcy5kcmFnT3ZlciA9IHRydWU7XG4gICAgfSBlbHNlIGlmIChvdXRwdXQudHlwZSA9PT0gXCJkcmFnT3V0XCIpIHtcbiAgICAgIHRoaXMuZHJhZ092ZXIgPSBmYWxzZTtcbiAgICB9IGVsc2UgaWYgKG91dHB1dC50eXBlID09PSBcImRyb3BcIikge1xuICAgICAgdGhpcy5kcmFnT3ZlciA9IGZhbHNlO1xuICAgIH0gZWxzZSBpZiAob3V0cHV0LnR5cGUgPT09IFwiZG9uZVwiKSB7XG4gICAgICB0aGlzLmNoZWNrUmVzcG9uc2Uob3V0cHV0KTtcbiAgICB9IGVsc2UgaWYgKG91dHB1dC50eXBlID09PSBcInJlamVjdGVkXCIpIHtcbiAgICAgIHRoaXMucmVzcG9uc2UuZW1pdChuZXcgVXBsb2FkUmVzcG9uc2UoVVBMT0FEX0ZJTEVfRVZFTlQuVVBMT0FEX0VSUk9SLCBcInVwbG9hZC5maWxlLmVycm9yLm1lc3NhZ2UuZmlsZS50eXBlXCIpKTtcbiAgICB9XG5cbiAgfVxuXG4gIGNoZWNrUmVzcG9uc2Uob3V0cHV0KSB7XG4gICAgdGhpcy50b3RhbERvbmVVcGxvYWRlZCA9IHRoaXMudG90YWxEb25lVXBsb2FkZWQgKyAxO1xuICAgIGlmICh0aGlzLmlzVmFsaWQob3V0cHV0LmZpbGUucmVzcG9uc2UuZXJyb3IpICYmIG91dHB1dC5maWxlLnJlc3BvbnNlLmVycm9yIGluc3RhbmNlb2YgQXJyYXkgJiYgb3V0cHV0LmZpbGUucmVzcG9uc2VTdGF0dXMgIT09IDIwMCkge1xuICAgICAgbGV0IGFycjogQXJyYXk8YW55PiA9IG91dHB1dC5maWxlLnJlc3BvbnNlLmVycm9yO1xuICAgICAgdGhpcy5yZXNwb25zZS5lbWl0KG5ldyBVcGxvYWRSZXNwb25zZShVUExPQURfRklMRV9FVkVOVC5VUExPQURfRVJST1IsIGFyclswXS5jb2RlLCBvdXRwdXQuZmlsZS5yZXNwb25zZVN0YXR1cykpO1xuICAgICAgdGhpcy5yZXNwb25zZS5lbWl0KG5ldyBVcGxvYWRSZXNwb25zZShVUExPQURfRklMRV9FVkVOVC5GSU5JU0hfVVBMT0FELCBcIlwiLCBvdXRwdXQuZmlsZS5yZXNwb25zZVN0YXR1cykpO1xuICAgICAgdGhpcy51cGxvYWRRdWV1ZSA9IFtdO1xuICAgIH0gZWxzZSBpZiAob3V0cHV0LmZpbGUucmVzcG9uc2UucmVzdWx0IHx8IG91dHB1dC5maWxlLnJlc3BvbnNlU3RhdHVzID09PSAyMDApIHtcbiAgICAgIGlmICghdGhpcy5tdWx0aXBsZSkge1xuICAgICAgICB0aGlzLnVwbG9hZFF1ZXVlID0gW107XG4gICAgICAgIHRoaXMuZmlsZUlkID0gb3V0cHV0LmZpbGUucmVzcG9uc2UucmVzdWx0O1xuICAgICAgICB0aGlzLmZpbGVOYW1lID0gb3V0cHV0LmZpbGUucmVzcG9uc2UucmVzdWx0O1xuICAgICAgICB0aGlzLnJlc3BvbnNlLmVtaXQobmV3IFVwbG9hZFJlc3BvbnNlKFVQTE9BRF9GSUxFX0VWRU5ULlVQTE9BRF9TVUNDRVNTRUQsIG91dHB1dC5maWxlLnJlc3BvbnNlLnJlc3VsdCwgb3V0cHV0LmZpbGUucmVzcG9uc2VTdGF0dXMpKTtcbiAgICAgICAgdGhpcy5yZXNwb25zZS5lbWl0KG5ldyBVcGxvYWRSZXNwb25zZShVUExPQURfRklMRV9FVkVOVC5GSU5JU0hfVVBMT0FELCBcIlwiLCBvdXRwdXQuZmlsZS5yZXNwb25zZVN0YXR1cykpO1xuICAgICAgfSBlbHNlIGlmICh0aGlzLnRvdGFsRG9uZVVwbG9hZGVkID09PSB0aGlzLnVwbG9hZFF1ZXVlLmxlbmd0aCkge1xuICAgICAgICB0aGlzLnVwbG9hZFF1ZXVlID0gW107XG4gICAgICAgIHRoaXMudG90YWxEb25lVXBsb2FkZWQgPSAwO1xuICAgICAgICB0aGlzLnJlc3BvbnNlLmVtaXQobmV3IFVwbG9hZFJlc3BvbnNlKFVQTE9BRF9GSUxFX0VWRU5ULkZJTklTSF9VUExPQUQsIFwiXCIsIG91dHB1dC5maWxlLnJlc3BvbnNlU3RhdHVzKSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMucmVzcG9uc2UuZW1pdChuZXcgVXBsb2FkUmVzcG9uc2UoVVBMT0FEX0ZJTEVfRVZFTlQuVVBMT0FEX0VSUk9SLCBcInVwbG9hZC5maWxlLmVycm9yXCIsIG91dHB1dC5maWxlLnJlc3BvbnNlU3RhdHVzKSk7XG4gICAgICB0aGlzLmZpbGVzID0gW107XG4gICAgICB0aGlzLnVwbG9hZFF1ZXVlID0gW107XG4gICAgICB0aGlzLnJlc3BvbnNlLmVtaXQobmV3IFVwbG9hZFJlc3BvbnNlKFVQTE9BRF9GSUxFX0VWRU5ULkZJTklTSF9VUExPQUQsIFwiXCIsIG91dHB1dC5maWxlLnJlc3BvbnNlU3RhdHVzKSk7XG4gICAgfVxuICB9XG5cbiAgaXNVcGxvYWRGaWxlc0RvbmUoKTogYm9vbGVhbiB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmZpbGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICByZXR1cm4gdGhpcy5maWxlc1tpXS5wcm9ncmVzcy5kYXRhLnBlcmNlbnRhZ2UgPT09IDEwMDtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgZ2V0RmlsZXNVcGxvYWRlZCgpOiBVcGxvYWRGaWxlW10ge1xuICAgIHJldHVybiB0aGlzLmZpbGVzO1xuICB9XG5cbiAgc3RhcnRVcGxvYWQoZmlsZTogVXBsb2FkRmlsZSk6IHZvaWQge1xuICAgIHRoaXMuZmlsZUlkID0gXCJcIjtcbiAgICBjb25zdCBldmVudDogVXBsb2FkSW5wdXQgPSB7XG4gICAgICB0eXBlOiBcInVwbG9hZEZpbGVcIixcbiAgICAgIHVybDogdGhpcy51cGxvYWRPcHRpb24udXBsb2FkQXBpLnJlcGxhY2UoXCI6ZmlsZVR5cGVcIiwgdGhpcy51cGxvYWRUeXBlKSxcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICBmaWxlOiBmaWxlLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBsYW5ndWFnZTogdGhpcy51cGxvYWRPcHRpb24ubGFuZ3VhZ2UsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRoaXMudXBsb2FkT3B0aW9uLmF1dGhvcml6YXRpb25cbiAgICAgIH1cbiAgICB9O1xuXG4gICAgdGhpcy51cGxvYWRJbnB1dC5lbWl0KGV2ZW50KTtcbiAgfVxuXG4gIGNhbmNlbFVwbG9hZChpZDogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy51cGxvYWRJbnB1dC5lbWl0KHsgdHlwZTogXCJjYW5jZWxcIiwgaWQ6IGlkIH0pO1xuICB9XG5cbiAgcmVtb3ZlRmlsZShpZDogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy51cGxvYWRJbnB1dC5lbWl0KHsgdHlwZTogXCJyZW1vdmVcIiwgaWQ6IGlkIH0pO1xuICB9XG5cbiAgcmVtb3ZlQWxsRmlsZXMoKTogdm9pZCB7XG4gICAgdGhpcy51cGxvYWRJbnB1dC5lbWl0KHsgdHlwZTogXCJyZW1vdmVBbGxcIiB9KTtcbiAgfVxuXG4gIC8vIElNQUdFIENST1BQRVJcbiAgZmlsZUNoYW5nZUV2ZW50KGV2ZW50OiBhbnkpOiB2b2lkIHtcbiAgICBpZiAoZXZlbnQuc3JjRWxlbWVudC5maWxlc1swXS5zaXplID4gdGhpcy5maWxlU2l6ZSkge1xuICAgICAgdGhpcy5yZXNwb25zZS5lbWl0KG5ldyBVcGxvYWRSZXNwb25zZShVUExPQURfRklMRV9FVkVOVC5VUExPQURfRVJST1IsIFwidXBsb2FkLmZpbGUuZXJyb3IubWVzc2FnZS5maWxlLm1heFNpemVcIikpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmltYWdlQ2hhbmdlZEV2ZW50ID0gZXZlbnQ7XG4gICAgICAvL3RoaXMub3JpZ2luYWxJbWFnZSA9IGV2ZW50LnNyY0VsZW1lbnQuZmlsZXM7XG4gICAgfVxuICB9XG5cbiAgaW1hZ2VDcm9wcGVkKGV2ZW50OiBJbWFnZUNyb3BwZWRFdmVudCkge1xuICAgIC8vdGhpcy5jcm9wcGVkSW1hZ2UgPSBldmVudC5iYXNlNjQ7XG4gICAgdGhpcy5jcm9wSW1nT2JqZWN0ID0gZXZlbnQ7XG5cbiAgICAvLyBjcm9wIGltYWdlIGFnYWluIGlmIGN1cnJlbnQgY3JvcHBlZCBpbWFnZSdzIHdpZHRoIGlzIGdyZWF0ZXIgdGhhbiByZXNpemVUb1dpZHRoIGNvbmZpZ1xuICAgIGlmIChldmVudC53aWR0aCA+IHVwbG9hZFJhdGlvW3RoaXMucmF0aW9dLnJlc2l6ZVRvV2lkdGgpIHtcbiAgICAgIHRoaXMuaW1hZ2VDcm9wcGVyLnJlc2l6ZVRvV2lkdGggPSB1cGxvYWRSYXRpb1t0aGlzLnJhdGlvXS5yZXNpemVUb1dpZHRoO1xuICAgICAgdGhpcy5pbWFnZUNyb3BwZXIuY3JvcCgpO1xuICAgICAgdGhpcy5pbWFnZUNyb3BwZXIucmVzaXplVG9XaWR0aCA9IDA7XG4gICAgfVxuICB9XG5cbiAgaW1hZ2VMb2FkZWQoKSB7XG4gICAgLy8gc2hvdyBjcm9wcGVyXG4gIH1cblxuICBsb2FkSW1hZ2VGYWlsZWQoKSB7XG4gICAgLy8gc2hvdyBtZXNzYWdlXG4gIH1cblxuICBjbGVhclVwbG9hZENyb3BwZWRJbWFnZSgpIHtcbiAgICAvL3RoaXMub3JpZ2luYWxJbWFnZSA9IG5ldyBBcnJheSgpO1xuICAgIHRoaXMuY3JvcEltZ09iamVjdCA9IHVuZGVmaW5lZDtcbiAgICB0aGlzLmltYWdlQ2hhbmdlZEV2ZW50ID0gdW5kZWZpbmVkO1xuICB9XG5cbiAgdXBsb2FkQ3JvcHBlZEltYWdlKCkge1xuICAgIC8vdXBsb2FkIGNyb3BwZWQgaW1hZ2VcbiAgICBpZiAodGhpcy5jcm9wSW1nT2JqZWN0KSB7XG4gICAgICB0aGlzLnJlc3BvbnNlLmVtaXQobmV3IFVwbG9hZFJlc3BvbnNlKFVQTE9BRF9GSUxFX0VWRU5ULlNUQVJUX1VQTE9BRCkpO1xuICAgICAgbGV0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgICBmb3JtRGF0YS5hcHBlbmQoJ2ZpbGUnLCB0aGlzLmNyb3BJbWdPYmplY3QuZmlsZSk7XG5cbiAgICAgIGxldCBoZWFkZXJzID0gbmV3IEh0dHBIZWFkZXJzKCk7XG4gICAgICBoZWFkZXJzLmFwcGVuZCgnbGFuZ3VhZ2UnLCB0aGlzLnVwbG9hZE9wdGlvbi5sYW5ndWFnZSk7XG4gICAgICBoZWFkZXJzLmFwcGVuZCgnQXV0aG9yaXphdGlvbicsIHRoaXMudXBsb2FkT3B0aW9uLmF1dGhvcml6YXRpb24pO1xuXG4gICAgICB0aGlzLmh0dHAucG9zdCh0aGlzLnVwbG9hZE9wdGlvbi51cGxvYWRBcGkucmVwbGFjZShcIjpmaWxlVHlwZVwiLCB0aGlzLnVwbG9hZFR5cGUpLCBmb3JtRGF0YSwgeyBoZWFkZXJzOiBoZWFkZXJzIH0pLnN1YnNjcmliZSgocmVzdWx0OiBhbnkpID0+IHtcbiAgICAgICAgbGV0IHJlcyA9IHJlc3VsdDtcbiAgICAgICAgdGhpcy5yZXNwb25zZS5lbWl0KG5ldyBVcGxvYWRSZXNwb25zZShVUExPQURfRklMRV9FVkVOVC5VUExPQURfU1VDQ0VTU0VELCByZXMucmVzdWx0KSk7XG4gICAgICAgIHRoaXMuY2xlYXJVcGxvYWRDcm9wcGVkSW1hZ2UoKTtcbiAgICAgICAgdGhpcy5maWxlTmFtZSA9IHJlc3VsdC5tZXNzYWdlO1xuICAgICAgICB0aGlzLnJlc3BvbnNlLmVtaXQobmV3IFVwbG9hZFJlc3BvbnNlKFVQTE9BRF9GSUxFX0VWRU5ULkZJTklTSF9VUExPQUQpKTtcbiAgICAgIH0sIChlcnJvclJlcykgPT4ge1xuICAgICAgICBsZXQgZXJyb3JzOiBBcnJheTxFcnJvck9iaj4gPSB0aGlzLnBhcnNlRXJyb3JSZXNwb25zZShlcnJvclJlcyk7XG4gICAgICAgIGlmIChlcnJvcnMubGVuZ3RoID4gMCAmJiBlcnJvcnNbMF0ubWVzc2FnZS50cmltKCkubGVuZ3RoID4gMCkge1xuICAgICAgICAgIHRoaXMucmVzcG9uc2UuZW1pdChuZXcgVXBsb2FkUmVzcG9uc2UoVVBMT0FEX0ZJTEVfRVZFTlQuVVBMT0FEX0VSUk9SLCBlcnJvcnNbMF0ubWVzc2FnZS50cmltKCkpKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnJlc3BvbnNlLmVtaXQobmV3IFVwbG9hZFJlc3BvbnNlKFVQTE9BRF9GSUxFX0VWRU5ULkZJTklTSF9VUExPQUQpKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxufVxuIiwiPCEtLSBTVVBQT1JUIENST1BQRVIgSU1BR0UgLS0+XG48ZGl2IGlkPVwidXBsb2FkLWZpbGVcIiBjbGFzcz1cInJhdGlvLXt7cmF0aW99fSBjcm9wcGVyLXVwbG9hZFwiICpuZ0lmPVwicmF0aW9cIj5cbiAgICA8ZGl2ICpuZ0lmPVwiaW1hZ2VDaGFuZ2VkRXZlbnQgJiYgb3B0aW9uc1wiIGNsYXNzPVwiaW1nLWNyb3BwZXItd3JhcHBlclwiIHN0eWxlPVwid2lkdGg6IDEwMCU7XCI+XG4gICAgICAgIDxpbWFnZS1jcm9wcGVyIFttYWludGFpbkFzcGVjdFJhdGlvXT1cInRydWVcIiBbaW1hZ2VRdWFsaXR5XT1cIjEwMFwiIFtpbWFnZUNoYW5nZWRFdmVudF09XCJpbWFnZUNoYW5nZWRFdmVudFwiIFthdXRvQ3JvcF09XCJ0cnVlXCIgW21haW50YWluQXNwZWN0UmF0aW9dPVwidHJ1ZVwiIFthc3BlY3RSYXRpb109XCJnZXRSYXRpbygpXCIgb3V0cHV0VHlwZT1cImZpbGVcIiAoaW1hZ2VDcm9wcGVkKT1cImltYWdlQ3JvcHBlZCgkZXZlbnQpXCIgKGltYWdlTG9hZGVkKT1cImltYWdlTG9hZGVkKClcIlxuICAgICAgICAgICAgKGxvYWRJbWFnZUZhaWxlZCk9XCJsb2FkSW1hZ2VGYWlsZWQoKVwiPlxuICAgICAgICA8L2ltYWdlLWNyb3BwZXI+XG4gICAgICAgIDxkaXY+PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gdXBwZXJjYXNlLXRleHQgYnRuLXByaW1hcnkgbW9kYWwtYWN0aW9uLWJ0blwiIChjbGljayk9XCJ1cGxvYWRDcm9wcGVkSW1hZ2UoKVwiPlxuICAgICAgICAgICAgICAgIHt7dXBsb2FkbGFuZ3VhZ2VPYmouY3JvcF9pbWFnZV95ZXMgfX1cbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gdXBwZXJjYXNlLXRleHQgYnRuLWxpZ2h0IG1vZGFsLWFjdGlvbi1idG5cIiAoY2xpY2spPVwiY2xlYXJVcGxvYWRDcm9wcGVkSW1hZ2UoKVwiPlxuICAgICAgICAgICAgICAgIHt7dXBsb2FkbGFuZ3VhZ2VPYmouY3JvcF9pbWFnZV9jYW5jZWwgfX1cbiAgICAgICAgICAgIDwvYnV0dG9uPjwvZGl2PlxuICAgIDwvZGl2PlxuXG4gICAgPGRpdiBjbGFzcz1cInVwbG9hZC1pdGVtXCIgKm5nSWY9XCIhaW1hZ2VDaGFuZ2VkRXZlbnQgJiYgZmlsZU5hbWVcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cInVwbG9hZC1pdGVtLWNvbnRlbnRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbWFnZS11cGxvYWRlZFwiPlxuICAgICAgICAgICAgICAgIDxpbWcgW3NyY109XCJ1cGxvYWRPcHRpb24uZG93bmxvYWRGaWxlQXBpICsgZmlsZU5hbWVcIiAoZXJyb3IpPVwiaW1nLmVycm9yPW51bGw7aW1nLnNyYyA9IHVwbG9hZE9wdGlvbi5wbGFjZUhvbGRlckltZ1wiICNpbWcvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuXG4gICAgPGRpdiAqbmdJZj1cIiFpbWFnZUNoYW5nZWRFdmVudCAmJiBvcHRpb25zXCIgY2xhc3M9XCJ1cGxvYWQtZmlsZS1jb250YWluZXJcIj5cbiAgICAgICAgPGxhYmVsIGNsYXNzPVwidXBsb2FkLWJ1dHRvbi1maWxlXCI+XG4gICAgICAgICAgICA8ZGl2ICpuZ0lmPVwiIWZpbGVOYW1lXCIgaWQ9XCJ1cGxvYWQtaWNvblwiPjxpIGNsYXNzPVwiZmEgZmEtY2xvdWQtdXBsb2FkIGZhLTV4XCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPjwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1lc3NhZ2UtZHJhZy1hbmQtZHJvcFwiPnt7IHVwbG9hZGxhbmd1YWdlT2JqLnVwbG9hZF9maWxlX2RyYWdfYW5kX2Ryb3AgfX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1lc3NhZ2Utb3JcIj57e3VwbG9hZGxhbmd1YWdlT2JqLnVwbG9hZF9maWxlX29yIH19PC9kaXY+XG4gICAgICAgICAgICA8YnI+XG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImJ0bi1idXR0b24tZmlsZVwiPiB7eyB1cGxvYWRsYW5ndWFnZU9iai51cGxvYWRfZmlsZV9icm93c2VfZmlsZSB9fSA8L3NwYW4+XG4gICAgICAgICAgICA8YnI+XG4gICAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJpbnB1dC11cGxvYWRcIiBvbmNsaWNrPVwidGhpcy52YWx1ZT1udWxsXCIgdHlwZT1cImZpbGVcIiBhY2NlcHQ9XCJ7e29wdGlvbnMuYWxsb3dlZENvbnRlbnRUeXBlc319XCJcbiAgICAgICAgICAgICAgICAoY2hhbmdlKT1cImZpbGVDaGFuZ2VFdmVudCgkZXZlbnQpXCI+XG4gICAgICAgIDwvbGFiZWw+XG4gICAgPC9kaXY+XG48L2Rpdj5cblxuPCEtLSBOT1JNQUwgVVBMT0FEIC0tPlxuPGRpdiAqbmdJZj1cIm9wdGlvbnMgJiYgIXJhdGlvXCIgaWQ9XCJ1cGxvYWQtZmlsZVwiIGNsYXNzPVwiZHJvcC1jb250YWluZXJcIiBuZ0ZpbGVEcm9wIFtvcHRpb25zXT1cIm9wdGlvbnNcIiAodXBsb2FkT3V0cHV0KT1cIm9uVXBsb2FkT3V0cHV0KCRldmVudClcIiBbdXBsb2FkSW5wdXRdPVwidXBsb2FkSW5wdXRcIiBbbmdDbGFzc109XCJ7ICdpcy1kcm9wLW92ZXInOiBkcmFnT3ZlciB9XCI+XG4gICAgPGRpdiAqbmdJZj1cInVwbG9hZFR5cGUgPT09ICdJTUFHRSdcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cInVwbG9hZC1pdGVtXCIgKm5nRm9yPVwibGV0IGYgb2YgZmlsZXM7IGxldCBpID0gaW5kZXg7XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwidXBsb2FkLWl0ZW0tY29udGVudFwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwcm9ncmVzcy1jb250ZW50XCIgKm5nSWY9XCIhaXNVcGxvYWRGaWxlc0RvbmUoKVwiPlxuICAgICAgICAgICAgICAgICAgICA8bmdiLXByb2dyZXNzYmFyIHNob3dWYWx1ZT1cInRydWVcIiB0eXBlPVwic3VjY2Vzc1wiIFt2YWx1ZV09XCJmPy5wcm9ncmVzcz8uZGF0YT8ucGVyY2VudGFnZVwiPlxuICAgICAgICAgICAgICAgICAgICA8L25nYi1wcm9ncmVzc2Jhcj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzcz1cInVwbG9hZC1pdGVtXCIgKm5nSWY9XCIhbXVsdGlwbGUgJiYgKGZpbGVJZCB8fCBmaWxlTmFtZSlcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ1cGxvYWQtaXRlbS1jb250ZW50XCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImltYWdlLXVwbG9hZGVkXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpbWcgKm5nSWY9XCIhaGlkZVByZXZpZXdcIiBbc3JjXT1cInVwbG9hZE9wdGlvbi5kb3dubG9hZEZpbGVBcGkgKyBnZXRJbWdVcmwoKVwiIChlcnJvcik9XCJpbWFnZS5lcnJvcj1udWxsO2ltYWdlLnNyYyA9IHVwbG9hZE9wdGlvbi5wbGFjZUhvbGRlckltZ1wiICNpbWFnZS8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAge3tmaWxlSWR9fVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuXG4gICAgPGRpdiAqbmdJZj1cIm11bHRpcGxlXCI+XG4gICAgICAgIDxzcGFuICpuZ0lmPVwiIWlzVXBsb2FkRmlsZXNEb25lKClcIiBjbGFzcz1cIm1lc3NhZ2UtZHJhZy1hbmQtZHJvcFwiPnt7IHVwbG9hZGxhbmd1YWdlT2JqLnVwbG9hZF9maWxlX2RyYWdfYW5kX2Ryb3AgfX1cbiAgICAgICAgPC9zcGFuPlxuXG4gICAgICAgIDxzcGFuICpuZ0lmPVwiIWlzVXBsb2FkRmlsZXNEb25lKClcIiBjbGFzcz1cIm1lc3NhZ2Utb3JcIj57e3VwbG9hZGxhbmd1YWdlT2JqLnVwbG9hZF9maWxlX29yIH19PC9zcGFuPlxuXG4gICAgICAgIDxsYWJlbCBjbGFzcz1cInVwbG9hZC1idXR0b25cIj5cbiAgICAgICAgICAgIDxpbnB1dCBjbGFzcz1cImlucHV0LXVwbG9hZFwiIG9uY2xpY2s9XCJ0aGlzLnZhbHVlPW51bGxcIiBhY2NlcHQ9XCJ7e29wdGlvbnMuYWxsb3dlZENvbnRlbnRUeXBlc319XCIgdHlwZT1cImZpbGVcIiBuZ0ZpbGVTZWxlY3RcbiAgICAgICAgICAgICAgICBbb3B0aW9uc109XCJvcHRpb25zXCIgKHVwbG9hZE91dHB1dCk9XCJvblVwbG9hZE91dHB1dCgkZXZlbnQpXCIgW3VwbG9hZElucHV0XT1cInVwbG9hZElucHV0XCIgbXVsdGlwbGU+XG4gICAgICAgICAgICA8c3BhbiAqbmdJZj1cIiFpc1VwbG9hZEZpbGVzRG9uZSgpXCIgY2xhc3M9XCJidG4tYnV0dG9uXCI+IHt7IHVwbG9hZGxhbmd1YWdlT2JqLnVwbG9hZF9maWxlX2Jyb3dzZV9maWxlcyB9fVxuICAgICAgICAgICAgPC9zcGFuPlxuXG4gICAgICAgICAgICA8c3BhbiAqbmdJZj1cImlzVXBsb2FkRmlsZXNEb25lKClcIiBjbGFzcz1cImJ0bi1idXR0b25cIj4ge3sgdXBsb2FkbGFuZ3VhZ2VPYmoudXBsb2FkX2ZpbGVfY2hhbmdlIH19IDwvc3Bhbj5cbiAgICAgICAgPC9sYWJlbD5cbiAgICA8L2Rpdj5cbiAgICA8ZGl2ICpuZ0lmPVwiIW11bHRpcGxlXCIgc3R5bGU9XCJ3aWR0aDogMTAwJTtcIj5cbiAgICAgICAgPGRpdiAqbmdJZj1cInVwbG9hZFR5cGUgPT09ICdJTUFHRSdcIj5cbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiIWlzVXBsb2FkRmlsZXNEb25lKClcIiBjbGFzcz1cIm1lc3NhZ2UtZHJhZy1hbmQtZHJvcFwiPnt7IHVwbG9hZGxhbmd1YWdlT2JqLnVwbG9hZF9maWxlX2RyYWdfYW5kX2Ryb3BcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgPC9zcGFuPlxuXG4gICAgICAgICAgICA8c3BhbiAqbmdJZj1cIiFpc1VwbG9hZEZpbGVzRG9uZSgpXCIgY2xhc3M9XCJtZXNzYWdlLW9yXCI+e3t1cGxvYWRsYW5ndWFnZU9iai51cGxvYWRfZmlsZV9vciB9fTwvc3Bhbj5cbiAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cInVwbG9hZC1idXR0b25cIj5cbiAgICAgICAgICAgICAgICA8aW5wdXQgY2xhc3M9XCJpbnB1dC11cGxvYWRcIiBvbmNsaWNrPVwidGhpcy52YWx1ZT1udWxsXCIgYWNjZXB0PVwie3tvcHRpb25zLmFsbG93ZWRDb250ZW50VHlwZXN9fVwiIHR5cGU9XCJmaWxlXCIgbmdGaWxlU2VsZWN0XG4gICAgICAgICAgICAgICAgICAgIFtvcHRpb25zXT1cIm9wdGlvbnNcIiAodXBsb2FkT3V0cHV0KT1cIm9uVXBsb2FkT3V0cHV0KCRldmVudClcIiBbdXBsb2FkSW5wdXRdPVwidXBsb2FkSW5wdXRcIj5cbiAgICAgICAgICAgICAgICA8c3BhbiAqbmdJZj1cIiFpc1VwbG9hZEZpbGVzRG9uZSgpXCIgY2xhc3M9XCJidG4tYnV0dG9uXCI+IHt7IHVwbG9hZGxhbmd1YWdlT2JqLnVwbG9hZF9maWxlX2Jyb3dzZV9maWxlIH19XG4gICAgICAgICAgICAgICAgPC9zcGFuPlxuXG4gICAgICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJpc1VwbG9hZEZpbGVzRG9uZSgpXCIgY2xhc3M9XCJidG4tYnV0dG9uXCI+IHt7IHVwbG9hZGxhbmd1YWdlT2JqLnVwbG9hZF9maWxlX2NoYW5nZSB9fSA8L3NwYW4+XG4gICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiAqbmdJZj1cInVwbG9hZFR5cGUgPT09ICdBTEwnXCIgY2xhc3M9XCJ1cGxvYWQtZmlsZS1jb250YWluZXJcIj5cbiAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cInVwbG9hZC1idXR0b24tZmlsZVwiPlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYnRuLWJ1dHRvbi1maWxlXCI+IHt7IHVwbG9hZGxhbmd1YWdlT2JqLnVwbG9hZF9maWxlX2Jyb3dzZV9maWxlIH19IDwvc3Bhbj5cbiAgICAgICAgICAgICAgICB7e2ZpbGVJZH19XG4gICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzPVwiaW5wdXQtdXBsb2FkXCIgb25jbGljaz1cInRoaXMudmFsdWU9bnVsbFwiIGFjY2VwdD1cInt7b3B0aW9ucy5hbGxvd2VkQ29udGVudFR5cGVzfX1cIiB0eXBlPVwiZmlsZVwiIG5nRmlsZVNlbGVjdFxuICAgICAgICAgICAgICAgICAgICBbb3B0aW9uc109XCJvcHRpb25zXCIgKHVwbG9hZE91dHB1dCk9XCJvblVwbG9hZE91dHB1dCgkZXZlbnQpXCIgW3VwbG9hZElucHV0XT1cInVwbG9hZElucHV0XCI+XG4gICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbjwvZGl2PiJdfQ==