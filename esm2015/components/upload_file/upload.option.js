export class UploadOption {
    constructor(acceptFileType = [], uploadApi, downloadFileApi, language, authorization, placeHolderImg) {
        this._acceptFileType = acceptFileType;
        this._uploadApi = uploadApi;
        this._language = language;
        this._authorization = authorization;
        this._downloadFileApi = downloadFileApi;
        this._placeHolderImg = placeHolderImg;
    }
    /**
     * Getter acceptFileType
     * return {string[]}
     */
    get acceptFileType() {
        return this._acceptFileType;
    }
    /**
     * Getter uploadApi
     * return {string}
     */
    get uploadApi() {
        return this._uploadApi;
    }
    /**
     * Getter downloadFileApi
     * return {string}
     */
    get downloadFileApi() {
        return this._downloadFileApi;
    }
    /**
     * Getter language
     * return {string}
     */
    get language() {
        return this._language;
    }
    /**
     * Getter authorization
     * return {string}
     */
    get authorization() {
        return this._authorization;
    }
    /**
     * Getter placeHolderImg
     * return {string}
     */
    get placeHolderImg() {
        return this._placeHolderImg;
    }
    /**
     * Setter acceptFileType
     * param {string[]} value
     */
    set acceptFileType(value) {
        this._acceptFileType = value;
    }
    /**
     * Setter uploadApi
     * param {string} value
     */
    set uploadApi(value) {
        this._uploadApi = value;
    }
    /**
     * Setter downloadFileApi
     * param {string} value
     */
    set downloadFileApi(value) {
        this._downloadFileApi = value;
    }
    /**
     * Setter language
     * param {string} value
     */
    set language(value) {
        this._language = value;
    }
    /**
     * Setter authorization
     * param {string} value
     */
    set authorization(value) {
        this._authorization = value;
    }
    /**
     * Setter placeHolderImg
     * param {string} value
     */
    set placeHolderImg(value) {
        this._placeHolderImg = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLm9wdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL3VwbG9hZF9maWxlL3VwbG9hZC5vcHRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsTUFBTSxPQUFPLFlBQVk7SUFRckIsWUFBWSxpQkFBMkIsRUFBRSxFQUFFLFNBQWlCLEVBQUUsZUFBdUIsRUFBRSxRQUFnQixFQUFFLGFBQXFCLEVBQUUsY0FBc0I7UUFDbEosSUFBSSxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUM7UUFDdEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7UUFDNUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7UUFDMUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUM7UUFDcEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGVBQWUsQ0FBQztRQUN4QyxJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQztJQUUxQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ04sSUFBVyxjQUFjO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUM3QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxTQUFTO1FBQ25CLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN4QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxlQUFlO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQzlCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFFBQVE7UUFDbEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3ZCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLGFBQWE7UUFDdkIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQzVCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLGNBQWM7UUFDeEIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQzdCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLGNBQWMsQ0FBQyxLQUFlO1FBQ3hDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQzlCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLFNBQVMsQ0FBQyxLQUFhO1FBQ2pDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO0lBQ3pCLENBQUM7SUFFRTs7O09BR0c7SUFDTixJQUFXLGVBQWUsQ0FBQyxLQUFhO1FBQ3ZDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsUUFBUSxDQUFDLEtBQWE7UUFDaEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsYUFBYSxDQUFDLEtBQWE7UUFDckMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsY0FBYyxDQUFDLEtBQWE7UUFDdEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztDQUVEIiwic291cmNlc0NvbnRlbnQiOlsiXG5leHBvcnQgY2xhc3MgVXBsb2FkT3B0aW9uIHtcbiAgICBwcml2YXRlIF9hY2NlcHRGaWxlVHlwZTogc3RyaW5nW107XG4gICAgcHJpdmF0ZSBfdXBsb2FkQXBpOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfZG93bmxvYWRGaWxlQXBpOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfbGFuZ3VhZ2U6IHN0cmluZztcbiAgICBwcml2YXRlIF9hdXRob3JpemF0aW9uOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBfcGxhY2VIb2xkZXJJbWc6IHN0cmluZztcblxuICAgIGNvbnN0cnVjdG9yKGFjY2VwdEZpbGVUeXBlOiBzdHJpbmdbXSA9IFtdLCB1cGxvYWRBcGk6IHN0cmluZywgZG93bmxvYWRGaWxlQXBpOiBzdHJpbmcsIGxhbmd1YWdlOiBzdHJpbmcsIGF1dGhvcml6YXRpb246IHN0cmluZywgcGxhY2VIb2xkZXJJbWc6IHN0cmluZyl7XG4gICAgICAgIHRoaXMuX2FjY2VwdEZpbGVUeXBlID0gYWNjZXB0RmlsZVR5cGU7XG4gICAgICAgIHRoaXMuX3VwbG9hZEFwaSA9IHVwbG9hZEFwaTtcbiAgICAgICAgdGhpcy5fbGFuZ3VhZ2UgPSBsYW5ndWFnZTtcbiAgICAgICAgdGhpcy5fYXV0aG9yaXphdGlvbiA9IGF1dGhvcml6YXRpb247XG4gICAgICAgIHRoaXMuX2Rvd25sb2FkRmlsZUFwaSA9IGRvd25sb2FkRmlsZUFwaTtcbiAgICAgICAgdGhpcy5fcGxhY2VIb2xkZXJJbWcgPSBwbGFjZUhvbGRlckltZztcblxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBhY2NlcHRGaWxlVHlwZVxuICAgICAqIHJldHVybiB7c3RyaW5nW119XG4gICAgICovXG5cdHB1YmxpYyBnZXQgYWNjZXB0RmlsZVR5cGUoKTogc3RyaW5nW10ge1xuXHRcdHJldHVybiB0aGlzLl9hY2NlcHRGaWxlVHlwZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHVwbG9hZEFwaVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHVwbG9hZEFwaSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl91cGxvYWRBcGk7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBkb3dubG9hZEZpbGVBcGlcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBkb3dubG9hZEZpbGVBcGkoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fZG93bmxvYWRGaWxlQXBpO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgbGFuZ3VhZ2VcbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBsYW5ndWFnZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9sYW5ndWFnZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIGF1dGhvcml6YXRpb25cbiAgICAgKiByZXR1cm4ge3N0cmluZ31cbiAgICAgKi9cblx0cHVibGljIGdldCBhdXRob3JpemF0aW9uKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX2F1dGhvcml6YXRpb247XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBwbGFjZUhvbGRlckltZ1xuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHBsYWNlSG9sZGVySW1nKCk6IHN0cmluZyB7XG5cdFx0cmV0dXJuIHRoaXMuX3BsYWNlSG9sZGVySW1nO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgYWNjZXB0RmlsZVR5cGVcbiAgICAgKiBwYXJhbSB7c3RyaW5nW119IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgYWNjZXB0RmlsZVR5cGUodmFsdWU6IHN0cmluZ1tdKSB7XG5cdFx0dGhpcy5fYWNjZXB0RmlsZVR5cGUgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHVwbG9hZEFwaVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgdXBsb2FkQXBpKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl91cGxvYWRBcGkgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGRvd25sb2FkRmlsZUFwaVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgZG93bmxvYWRGaWxlQXBpKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9kb3dubG9hZEZpbGVBcGkgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGxhbmd1YWdlXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBsYW5ndWFnZSh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fbGFuZ3VhZ2UgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGF1dGhvcml6YXRpb25cbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGF1dGhvcml6YXRpb24odmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX2F1dGhvcml6YXRpb24gPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHBsYWNlSG9sZGVySW1nXG4gICAgICogcGFyYW0ge3N0cmluZ30gdmFsdWVcbiAgICAgKi9cblx0cHVibGljIHNldCBwbGFjZUhvbGRlckltZyh2YWx1ZTogc3RyaW5nKSB7XG5cdFx0dGhpcy5fcGxhY2VIb2xkZXJJbWcgPSB2YWx1ZTtcblx0fVxuXG59Il19