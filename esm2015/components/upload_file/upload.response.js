export class UploadResponse {
    constructor(responseType = "", message = "", code = "") {
        this._message = message;
        this._responseType = responseType;
        this._code = code;
    }
    /**
     * Getter responseType
     * return {string}
     */
    get responseType() {
        return this._responseType;
    }
    /**
     * Getter message
     * return {string}
     */
    get message() {
        return this._message;
    }
    /**
     * Getter code
     * return {string}
     */
    get code() {
        return this._code;
    }
    /**
     * Setter responseType
     * param {string} value
     */
    set responseType(value) {
        this._responseType = value;
    }
    /**
     * Setter message
     * param {string} value
     */
    set message(value) {
        this._message = value;
    }
    /**
     * Setter code
     * param {string} value
     */
    set code(value) {
        this._code = value;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLnJlc3BvbnNlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9hY3JhY3kubmd1eWVuL1dvcmtzL1JlcG9zaXRvcmllcy92aWF0ZWNoL2NvbW1vbi92aWF0ZWNoLWFuZ3VsYXItY29tbW9uLXNvdXJjZS9wcm9qZWN0cy92aWF0ZWNoLWFuZ3VsYXItY29tbW9uL3NyYy8iLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvdXBsb2FkX2ZpbGUvdXBsb2FkLnJlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sT0FBTyxjQUFjO0lBS3ZCLFlBQVksZUFBdUIsRUFBRSxFQUFFLFVBQWtCLEVBQUUsRUFBRSxPQUFlLEVBQUU7UUFDMUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7UUFDeEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxZQUFZLENBQUM7UUFDbEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7SUFDdEIsQ0FBQztJQUVEOzs7T0FHRztJQUNOLElBQVcsWUFBWTtRQUN0QixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDM0IsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsT0FBTztRQUNqQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdEIsQ0FBQztJQUVFOzs7T0FHRztJQUNOLElBQVcsSUFBSTtRQUNkLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNuQixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxZQUFZLENBQUMsS0FBYTtRQUNwQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxPQUFPLENBQUMsS0FBYTtRQUMvQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBRUU7OztPQUdHO0lBQ04sSUFBVyxJQUFJLENBQUMsS0FBYTtRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUNwQixDQUFDO0NBR0QiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgVXBsb2FkUmVzcG9uc2Uge1xuICAgIHByaXZhdGUgX3Jlc3BvbnNlVHlwZTogc3RyaW5nO1xuICAgIHByaXZhdGUgX21lc3NhZ2U6IHN0cmluZztcbiAgICBwcml2YXRlIF9jb2RlOiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcihyZXNwb25zZVR5cGU6IHN0cmluZyA9IFwiXCIsIG1lc3NhZ2U6IHN0cmluZyA9IFwiXCIsIGNvZGU6IHN0cmluZyA9IFwiXCIpIHtcbiAgICAgICAgdGhpcy5fbWVzc2FnZSA9IG1lc3NhZ2U7XG4gICAgICAgIHRoaXMuX3Jlc3BvbnNlVHlwZSA9IHJlc3BvbnNlVHlwZTtcbiAgICAgICAgdGhpcy5fY29kZSA9IGNvZGU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0dGVyIHJlc3BvbnNlVHlwZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IHJlc3BvbnNlVHlwZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9yZXNwb25zZVR5cGU7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBtZXNzYWdlXG4gICAgICogcmV0dXJuIHtzdHJpbmd9XG4gICAgICovXG5cdHB1YmxpYyBnZXQgbWVzc2FnZSgpOiBzdHJpbmcge1xuXHRcdHJldHVybiB0aGlzLl9tZXNzYWdlO1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgY29kZVxuICAgICAqIHJldHVybiB7c3RyaW5nfVxuICAgICAqL1xuXHRwdWJsaWMgZ2V0IGNvZGUoKTogc3RyaW5nIHtcblx0XHRyZXR1cm4gdGhpcy5fY29kZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIHJlc3BvbnNlVHlwZVxuICAgICAqIHBhcmFtIHtzdHJpbmd9IHZhbHVlXG4gICAgICovXG5cdHB1YmxpYyBzZXQgcmVzcG9uc2VUeXBlKHZhbHVlOiBzdHJpbmcpIHtcblx0XHR0aGlzLl9yZXNwb25zZVR5cGUgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIG1lc3NhZ2VcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IG1lc3NhZ2UodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX21lc3NhZ2UgPSB2YWx1ZTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogU2V0dGVyIGNvZGVcbiAgICAgKiBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxuICAgICAqL1xuXHRwdWJsaWMgc2V0IGNvZGUodmFsdWU6IHN0cmluZykge1xuXHRcdHRoaXMuX2NvZGUgPSB2YWx1ZTtcblx0fVxuXG5cbn0iXX0=