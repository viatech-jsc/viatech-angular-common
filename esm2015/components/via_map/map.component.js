import { Component, EventEmitter, Input, Output } from '@angular/core';
import $ from 'jquery';
import { mapConfig } from '../../config';
import { Map } from "./map.model";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@angular/forms";
function MapComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r3 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 5);
    i0.ɵɵelementStart(1, "div", 6);
    i0.ɵɵelementStart(2, "div", 7);
    i0.ɵɵelement(3, "div", 8);
    i0.ɵɵelementStart(4, "input", 9);
    i0.ɵɵlistener("ngModelChange", function MapComponent_div_1_Template_input_ngModelChange_4_listener($event) { i0.ɵɵrestoreView(_r3); const ctx_r2 = i0.ɵɵnextContext(); return ctx_r2.cordinate.title = $event; });
    i0.ɵɵelementEnd();
    i0.ɵɵelement(5, "img", 10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("ngModel", ctx_r0.cordinate.title);
} }
function MapComponent_button_4_Template(rf, ctx) { if (rf & 1) {
    const _r5 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 11);
    i0.ɵɵlistener("click", function MapComponent_button_4_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r5); const ctx_r4 = i0.ɵɵnextContext(); return ctx_r4.myCurrentLocation(); });
    i0.ɵɵelement(1, "img", 12);
    i0.ɵɵelementEnd();
} }
export class MapComponent {
    constructor() {
        this.markerDraggable = true;
        this.customIcon = false;
        this.showMyLocation = true;
        this.hideSearchTextBox = false;
        this.cordinateChange = new EventEmitter();
        this.markerClick = new EventEmitter();
        this.mapRadius = new EventEmitter();
        this.mapDragged = new EventEmitter();
        this.searchEvent = new EventEmitter();
        this.getTopCordinatorEvent = new EventEmitter();
        this.getCurrentLocationEvent = new EventEmitter();
        this.greenPinIcon = 'assets/icons/green_pin.svg';
        this.locationIcon = 'assets/icons/red_pin.svg';
        this.markers = [];
    }
    ngAfterViewInit() {
        let self = this;
        self.placeholderText = self.searchPlaceholer;
        self.geocoder = new google.maps.Geocoder();
        let initMap = () => {
            self.myLatLng = {
                lat: self.cordinate.latitude, lng: self.cordinate.longtitude
            };
            let mapConfigObject = {
                zoom: 15,
                center: self.myLatLng,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            self.map = new google.maps.Map(document.getElementById('googleMap'), mapConfigObject);
            //listen to map drag event
            google.maps.event.addListener(self.map, 'dragend', function () {
                self.mapDragged.emit("");
            });
            //listen to map zoom event
            google.maps.event.addListener(self.map, 'zoom_changed', function () {
                if (self.mapRadius) {
                    let cordinate = new Map(undefined, undefined, undefined, "zoom_changed", self.map.getZoom());
                    self.mapRadius.emit(cordinate);
                }
            });
            if (!self.hideSearchTextBox) {
                self.input = document.getElementById('pac-input');
                self.searchBox = new google.maps.places.SearchBox(self.input);
                self.map.addListener('bounds_changed', () => {
                    self.searchBox.setBounds(self.map.getBounds());
                });
                self.addSearchBoxEvent();
            }
            if (self.cordinate.title === mapConfig.myLocation) {
                self.myLocation = new google.maps.Marker({
                    position: self.myLatLng,
                    icon: {
                        url: self.greenPinIcon,
                        scaledSize: new google.maps.Size(30, 30),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(0, 0) // anchor
                    },
                    zIndex: 1000,
                    map: self.map,
                    draggable: false
                });
            }
            else {
                self.marker = new google.maps.Marker({
                    position: self.myLatLng,
                    map: self.map,
                    draggable: self.markerDraggable,
                    animation: google.maps.Animation.DROP,
                    title: self.cordinate.title
                });
                self.addClickListener();
                self.addDragendListener();
            }
            self.geocodePosition(self.myLatLng);
        };
        if (self.cordinate.latitude && self.cordinate.longtitude) {
            initMap();
        }
        else {
            // ask user for current location permission 
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((location) => {
                    self.cordinate.latitude = location.coords.latitude;
                    self.cordinate.longtitude = location.coords.longitude;
                    initMap();
                }, () => {
                    // default location: 288H1 Nam Ky Khoi Nghia
                    self.cordinate.latitude = 10.7895299;
                    self.cordinate.longtitude = 106.68493590000003;
                    initMap();
                });
            }
        }
    }
    getFurthestPointFromLocation(lat, lng, eventName) {
        let cordinate, self = this, cornersArray = {}, corner1, corner2, corner3, corner4, farestPoint;
        let sW = this.map.getBounds().getSouthWest();
        let nE = this.map.getBounds().getNorthEast();
        cornersArray = {}, corner1, corner2, corner3, corner4;
        corner1 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), sW.lng()));
        corner2 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), nE.lng()));
        corner3 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), nE.lng()));
        corner4 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), sW.lng()));
        cornersArray[corner1] = {
            "lat": sW.lat(),
            "lng": sW.lng()
        };
        cornersArray[corner2] = {
            "lat": nE.lat(),
            "lng": nE.lng()
        };
        cornersArray[corner3] = {
            "lat": sW.lat(),
            "lng": nE.lng()
        };
        cornersArray[corner4] = {
            "lat": nE.lat(),
            "lng": sW.lng()
        };
        farestPoint = cornersArray[Math.max(corner1, corner2, corner3, corner4)];
        cordinate = new Map(farestPoint.lat, farestPoint.lng, "", eventName);
        this.mapRadius.emit(cordinate);
    }
    getCenterMapLocation() {
        return this.map.getCenter();
    }
    calculateDistance(aLat, aLng, bLat, bLng) {
        return google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(aLat, aLng), new google.maps.LatLng(bLat, bLng));
    }
    // Deletes all markers in the array by removing references to them.
    deleteMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers = [];
    }
    //listLocation[0]: code
    //listLocation[1]: Latitude (pointY)
    //listLocation[2]: Longtitude (pointX)
    //listLocation[3]: index
    //listLocation[4]: label
    showMultitpleLocations(listLocation = []) {
        let self = this;
        let infowindow = new google.maps.InfoWindow;
        let marker, i;
        let bounds = new google.maps.LatLngBounds();
        this.deleteMarkers();
        for (i = 0; i < listLocation.length; i++) {
            //marker CONFIG
            let markObject = {
                position: new google.maps.LatLng(listLocation[i][1], listLocation[i][2]),
                //labelOrigin: new google.maps.Point(9, 9),
                //   label: {
                //     text: listLocation[i][4]? listLocation[i][4]: "",
                //     fontSize: "12px",
                //     color: "#e74c3c",
                //     fontFamily: "montserrat"
                //  },
                scaledSize: new google.maps.Size(30, 30),
                map: this.map
            };
            markObject["icon"] = {
                url: this.locationIcon,
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0) // anchor
            };
            marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    //get address
                    let latlng = new google.maps.LatLng(listLocation[i][1], listLocation[i][2]);
                    self.geocoder.geocode({ 'latLng': latlng }, (responses) => {
                        let address;
                        if (!listLocation[i][4]) {
                            if (responses && responses.length > 0) {
                                address = responses[0].formatted_address;
                            }
                            else {
                                address = "Cannot determine address at this location.";
                            }
                        }
                        else {
                            address = listLocation[i][4];
                        }
                        self.activeInfoWindow && self.activeInfoWindow.close();
                        self.activeInfoWindow = infowindow;
                        infowindow.setContent(address);
                        infowindow.open(self.map, marker);
                        //marker object [title, lat, long, index]
                        if (listLocation[i][3]) {
                            self.markerClick.emit(listLocation[i][3]);
                        }
                    });
                };
            })(marker, i));
        }
        // for (let index = 0; index < this.markers.length; index++) {
        //   bounds.extend(this.markers[index].getPosition());
        // }
        // if (self.marker) {
        //   bounds.extend(self.marker.getPosition());
        // }
        // if (self.myLocation) {
        //   bounds.extend(self.myLocation.getPosition());
        // }
        //this.map.fitBounds(bounds);
    }
    panTo(lat, lng, zoomNumber = 12) {
        let myLatLng = new google.maps.LatLng(lat, lng);
        this.map.setZoom(zoomNumber);
        this.map.panTo(myLatLng);
    }
    geocodePosition(pos, eventName = undefined, func = null) {
        this.geocoder.geocode({ 'latLng': pos }, (responses) => {
            if (responses && responses.length > 0) {
                this.cordinate.title = responses[0].formatted_address;
                this.cordinate.latitude = responses[0].geometry.location.lat();
                this.cordinate.longtitude = responses[0].geometry.location.lng();
            }
            else {
                this.cordinate.title = "Cannot determine address at this location.";
                this.cordinate.latitude = null;
                this.cordinate.longtitude = null;
            }
            this.triggerUpdateData(eventName);
            $("#pac-input").click();
            if (func) {
                func();
            }
        });
    }
    addSearchBoxEvent() {
        let self = this;
        self.searchBox.addListener('places_changed', () => {
            if (self.marker) {
                self.marker.setMap(null);
            }
            //self.deleteMarkers();
            let places = self.searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            let bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                let markerObj = {
                    map: self.map,
                    draggable: this.markerDraggable,
                    title: place.name,
                    animation: google.maps.Animation.DROP,
                    position: place.geometry.location
                };
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                self.marker = new google.maps.Marker(markerObj);
                // self.markers.push(self.marker);
                self.addDragendListener();
                self.searchEvent.emit(new Map(markerObj.position.lat(), markerObj.position.lng()));
                self.cordinate.latitude = place.geometry.location.lat();
                self.cordinate.longtitude = place.geometry.location.lng();
                self.cordinate.title = place.formatted_address;
                self.triggerUpdateData(mapConfig.mapEventNames.search);
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                }
                else {
                    bounds.extend(place.geometry.location);
                }
            });
            self.map.fitBounds(bounds);
            self.map.setZoom(12);
            self.map.panTo(self.marker.position);
        });
    }
    addClickListener() {
        google.maps.event.addListener(this.map, 'click', (event) => {
            //event.preventDefault();
            this.geocodePosition(event.latLng, mapConfig.mapEventNames.mapClick, () => {
                this.marker.setMap(null);
                this.marker = new google.maps.Marker({
                    position: event.latLng,
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: this.cordinate.title
                });
                this.addDragendListener();
            });
        });
    }
    addDragendListener() {
        google.maps.event.addListener(this.marker, 'dragend', () => {
            event.preventDefault();
            this.deleteMarkers();
            this.geocodePosition(this.marker.getPosition(), mapConfig.mapEventNames.markerDrag);
            this.cordinate.latitude = this.marker.getPosition().lat();
            this.cordinate.longtitude = this.marker.getPosition().lng();
        });
    }
    triggerUpdateData(eventName) {
        this.cordinate.eventName = eventName;
        this.cordinateChange.emit(this.cordinate);
    }
    myCurrentLocation() {
        let self = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (location) {
                if (self.marker) {
                    self.marker.setMap(null);
                }
                // self.deleteMarkers();
                self.cordinate.latitude = location.coords.latitude;
                self.cordinate.longtitude = location.coords.longitude;
                self.myLatLng = {
                    lat: location.coords.latitude, lng: location.coords.longitude
                };
                if (self.myLocation) {
                    self.myLocation.setMap(null);
                }
                self.myLocation = new google.maps.Marker({
                    position: self.myLatLng,
                    icon: {
                        url: self.greenPinIcon,
                        scaledSize: new google.maps.Size(30, 30),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(15, 15) // anchor
                    },
                    zIndex: 1000,
                    map: self.map,
                    draggable: false,
                    title: self.cordinate.title
                });
                self.geocodePosition(self.myLocation.getPosition(), mapConfig.mapEventNames.myLocation);
                //self.addDragendListener();
                self.getCurrentLocationEvent.emit(new Map(location.coords.latitude, location.coords.longitude));
                let currentZoomLevel = self.map.getZoom();
                let bounds = new google.maps.LatLngBounds();
                self.map.setCenter(new google.maps.LatLng(self.myLatLng.lat, self.myLatLng.lng));
            });
        }
        else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    getRadius(distance = 100) {
        if (this.myLocation) {
            let w = this.map.getBounds().getSouthWest();
            let centerLng = this.myLocation.getPosition();
            distance = google.maps.geometry.spherical.computeDistanceBetween(centerLng, w);
        }
        //this.mapRadius.emit(distance ? (distance / 1000).toFixed(1) : "CANNOT_CALCULATE_DISTANCE");
    }
    getTopDistanceFromMarker(lat, lng, eventName) {
        if (!lat || !lng) {
            return;
        }
        let myLatLng = new google.maps.LatLng(lat, lng);
        // Get map bounds
        let bounds = this.map.getBounds();
        // Find map top border & distance to center
        //let mapTop = new google.maps.LatLng(bounds.getNorthEast().lat(), myLatLng.lng());
        //let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, mapTop);
        // Find map left border & distance to center
        // let mapLeft = new google.maps.LatLng(myLatLng.lat(), bounds.getSouthWest().lng());
        // let distanceToLeft = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, mapLeft);
        // Get shortest length
        //let radius = distanceToTop < distanceToLeft ? distanceToTop : distanceToLeft;
        //let radius = distanceToTop;
        this.getTopCordinatorEvent.emit(new Map(bounds.getNorthEast().lat(), myLatLng.lng(), "", eventName));
        // let circle = new google.maps.Circle({
        //   strokeColor: '#FF0000',
        //   strokeOpacity: 0.8,
        //   strokeWeight: 2,
        //   fillColor: '#FF0000',
        //   fillOpacity: 0.35,
        //   editable: true,
        //   map: this.map,
        //   center: myLatLng,
        //   radius: radius
        // });
    }
    drawCircle(centerLat, centerLng, pointLat, pointLng, fillColor = "#FF0000") {
        let myLatLng = new google.maps.LatLng(centerLat, centerLng);
        // Get map bounds
        let bounds = this.map.getBounds();
        let point = new google.maps.LatLng(pointLat, pointLng);
        let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, point);
        let radius = distanceToTop;
        this.circle.setMap(null);
        this.circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: fillColor,
            fillOpacity: 0.35,
            editable: true,
            map: this.map,
            center: myLatLng,
            radius: radius
        });
    }
}
MapComponent.ɵfac = function MapComponent_Factory(t) { return new (t || MapComponent)(); };
MapComponent.ɵcmp = i0.ɵɵdefineComponent({ type: MapComponent, selectors: [["via-map"]], inputs: { cordinate: "cordinate", searchPlaceholer: "searchPlaceholer", markerDraggable: "markerDraggable", customIcon: "customIcon", showMyLocation: "showMyLocation", hideSearchTextBox: "hideSearchTextBox" }, outputs: { cordinateChange: "cordinateChange", markerClick: "markerClick", mapRadius: "mapRadius", mapDragged: "mapDragged", searchEvent: "searchEvent", getTopCordinatorEvent: "getTopCordinatorEvent", getCurrentLocationEvent: "getCurrentLocationEvent" }, decls: 5, vars: 2, consts: [[1, "row"], ["class", "col-sm-12 map-search-container", 4, "ngIf"], [1, "col-sm-12"], ["id", "googleMap", 1, "google-maps"], ["class", "my-location-btn", 3, "click", 4, "ngIf"], [1, "col-sm-12", "map-search-container"], [1, "form-group"], ["id", "via-map-search"], ["id", "dummy-col"], ["type", "text", "id", "pac-input", "aria-describedby", "textHelp", "placeholder", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["src", "/assets/icons/ic_search.svg", 1, "input-search-icon"], [1, "my-location-btn", 3, "click"], ["src", "assets/icons/green_pin.svg"]], template: function MapComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵtemplate(1, MapComponent_div_1_Template, 6, 1, "div", 1);
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelement(3, "div", 3);
        i0.ɵɵtemplate(4, MapComponent_button_4_Template, 2, 0, "button", 4);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.hideSearchTextBox);
        i0.ɵɵadvance(3);
        i0.ɵɵproperty("ngIf", ctx.showMyLocation);
    } }, directives: [i1.NgIf, i2.DefaultValueAccessor, i2.NgControlStatus, i2.NgModel], styles: [".google-maps[_ngcontent-%COMP%]{border-radius:3px;height:300px}.my-location-btn[_ngcontent-%COMP%]{background:#fff;border-radius:2px;border-style:none;bottom:110px;box-shadow:0 1px 4px -1px rgba(0,0,0,.3);cursor:pointer;height:41px;padding:0!important;position:absolute;right:25px;text-align:center;width:41px}.my-location-btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:20px}.map-search-container[_ngcontent-%COMP%]{margin-bottom:10px}.map-search-container[_ngcontent-%COMP%]   #pac-input[_ngcontent-%COMP%]{padding:10px}.map-search-container[_ngcontent-%COMP%]   .input-search-icon[_ngcontent-%COMP%]{position:absolute;right:21px;top:13px;width:20px}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MapComponent, [{
        type: Component,
        args: [{
                selector: 'via-map',
                templateUrl: './map.html',
                styleUrls: ['./map.scss']
            }]
    }], function () { return []; }, { cordinate: [{
            type: Input
        }], searchPlaceholer: [{
            type: Input
        }], markerDraggable: [{
            type: Input
        }], customIcon: [{
            type: Input
        }], showMyLocation: [{
            type: Input
        }], hideSearchTextBox: [{
            type: Input
        }], cordinateChange: [{
            type: Output
        }], markerClick: [{
            type: Output
        }], mapRadius: [{
            type: Output
        }], mapDragged: [{
            type: Output
        }], searchEvent: [{
            type: Output
        }], getTopCordinatorEvent: [{
            type: Output
        }], getCurrentLocationEvent: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL3ZpYV9tYXAvbWFwLmNvbXBvbmVudC50cyIsImNvbXBvbmVudHMvdmlhX21hcC9tYXAuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3ZFLE9BQU8sQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUN2QixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxhQUFhLENBQUM7Ozs7OztJQ0Y5Qiw4QkFDSTtJQUFBLDhCQUNJO0lBQUEsOEJBQ0k7SUFBQSx5QkFBMEI7SUFDMUIsZ0NBRUE7SUFESSxpTkFBNkI7SUFEakMsaUJBRUE7SUFBQSwwQkFDSjtJQUFBLGlCQUFNO0lBQ1YsaUJBQU07SUFDVixpQkFBTTs7O0lBSlUsZUFBNkI7SUFBN0IsZ0RBQTZCOzs7O0lBT3pDLGtDQUFzRjtJQUF0RCwwTEFBNkI7SUFBeUIsMEJBQXNDO0lBQUEsaUJBQVM7O0FERjdJLE1BQU0sT0FBTyxZQUFZO0lBK0J2QjtRQTVCUyxvQkFBZSxHQUFZLElBQUksQ0FBQztRQUNoQyxlQUFVLEdBQVksS0FBSyxDQUFDO1FBQzVCLG1CQUFjLEdBQVksSUFBSSxDQUFDO1FBQy9CLHNCQUFpQixHQUFZLEtBQUssQ0FBQztRQUNsQyxvQkFBZSxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzdELGdCQUFXLEdBQXlCLElBQUksWUFBWSxFQUFVLENBQUM7UUFDL0QsY0FBUyxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3ZELGVBQVUsR0FBeUIsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUM5RCxnQkFBVyxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3pELDBCQUFxQixHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25FLDRCQUF1QixHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBVS9FLGlCQUFZLEdBQUcsNEJBQTRCLENBQUM7UUFDNUMsaUJBQVksR0FBRywwQkFBMEIsQ0FBQztRQUMxQyxZQUFPLEdBQUcsRUFBRSxDQUFDO0lBT2IsQ0FBQztJQUVELGVBQWU7UUFDYixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFDN0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFM0MsSUFBSSxPQUFPLEdBQUcsR0FBRyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUc7Z0JBQ2QsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVU7YUFDN0QsQ0FBQztZQUVGLElBQUksZUFBZSxHQUFHO2dCQUNwQixJQUFJLEVBQUUsRUFBRTtnQkFDUixNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVE7Z0JBQ3JCLGlCQUFpQixFQUFFLEtBQUs7Z0JBQ3hCLFNBQVMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPO2dCQUN4QyxjQUFjLEVBQUUsS0FBSzthQUN0QixDQUFDO1lBRUYsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFFdEYsMEJBQTBCO1lBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFNBQVMsRUFBRTtnQkFDakQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDM0IsQ0FBQyxDQUFDLENBQUM7WUFDSCwwQkFBMEI7WUFDMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsY0FBYyxFQUFFO2dCQUN0RCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7b0JBQ2xCLElBQUksU0FBUyxHQUFRLElBQUksR0FBRyxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLGNBQWMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7b0JBQ2xHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUNoQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDM0IsSUFBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNsRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDOUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEVBQUUsR0FBRyxFQUFFO29CQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7Z0JBQ2pELENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2FBQzFCO1lBRUQsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssS0FBSyxTQUFTLENBQUMsVUFBVSxFQUFFO2dCQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ3ZDLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtvQkFDdkIsSUFBSSxFQUFFO3dCQUNKLEdBQUcsRUFBRSxJQUFJLENBQUMsWUFBWTt3QkFDdEIsVUFBVSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQzt3QkFDeEMsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDbkMsTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVM7cUJBQzlDO29CQUNELE1BQU0sRUFBRSxJQUFJO29CQUNaLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztvQkFDYixTQUFTLEVBQUUsS0FBSztpQkFDakIsQ0FBQyxDQUFDO2FBRUo7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO29CQUNuQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7b0JBQ3ZCLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztvQkFDYixTQUFTLEVBQUUsSUFBSSxDQUFDLGVBQWU7b0JBQy9CLFNBQVMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJO29CQUNyQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLO2lCQUM1QixDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2FBQzNCO1lBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFBO1FBRUQsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRTtZQUN4RCxPQUFPLEVBQUUsQ0FBQztTQUNYO2FBQU07WUFDTCw0Q0FBNEM7WUFDNUMsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFO2dCQUN6QixTQUFTLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7b0JBQ3BELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO29CQUNuRCxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztvQkFDdEQsT0FBTyxFQUFFLENBQUM7Z0JBQ1osQ0FBQyxFQUFFLEdBQUcsRUFBRTtvQkFDTiw0Q0FBNEM7b0JBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztvQkFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsa0JBQWtCLENBQUM7b0JBQy9DLE9BQU8sRUFBRSxDQUFDO2dCQUNaLENBQUMsQ0FBQyxDQUFBO2FBQ0g7U0FDRjtJQUdILENBQUM7SUFFRCw0QkFBNEIsQ0FBQyxHQUFXLEVBQUUsR0FBVyxFQUFFLFNBQWlCO1FBQ3RFLElBQUksU0FBYyxFQUFFLElBQUksR0FBRyxJQUFJLEVBQUUsWUFBWSxHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxDQUFDO1FBRXBHLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDN0MsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUM3QyxZQUFZLEdBQUcsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQztRQUV0RCxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDOUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlJLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5SSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFOUksWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHO1lBQ3RCLEtBQUssRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFO1lBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7U0FDaEIsQ0FBQTtRQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztZQUN0QixLQUFLLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRTtZQUNmLEtBQUssRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFO1NBQ2hCLENBQUE7UUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUc7WUFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7WUFDZixLQUFLLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRTtTQUNoQixDQUFBO1FBQ0QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHO1lBQ3RCLEtBQUssRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFO1lBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7U0FDaEIsQ0FBQTtRQUdELFdBQVcsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ3pFLFNBQVMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxvQkFBb0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJO1FBQ3RDLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDdkksQ0FBQztJQUVELG1FQUFtRTtJQUNuRSxhQUFhO1FBQ1gsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzlCO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELHVCQUF1QjtJQUN2QixvQ0FBb0M7SUFDcEMsc0NBQXNDO0lBQ3RDLHdCQUF3QjtJQUN4Qix3QkFBd0I7SUFFeEIsc0JBQXNCLENBQUMsZUFBMkIsRUFBRTtRQUNsRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxVQUFVLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUM1QyxJQUFJLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDZCxJQUFJLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDNUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRXJCLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QyxlQUFlO1lBQ2YsSUFBSSxVQUFVLEdBQUc7Z0JBQ2YsUUFBUSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEUsMkNBQTJDO2dCQUMzQyxhQUFhO2dCQUNiLHdEQUF3RDtnQkFDeEQsd0JBQXdCO2dCQUN4Qix3QkFBd0I7Z0JBQ3hCLCtCQUErQjtnQkFDL0IsTUFBTTtnQkFDTixVQUFVLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDO2dCQUN4QyxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7YUFDZCxDQUFDO1lBRUYsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHO2dCQUNuQixHQUFHLEVBQUUsSUFBSSxDQUFDLFlBQVk7Z0JBQ3RCLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUM7Z0JBQ3hDLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ25DLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTO2FBQzlDLENBQUM7WUFDRixNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMxQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxDQUFDLFVBQVUsTUFBTSxFQUFFLENBQUM7Z0JBQ2pFLE9BQU87b0JBQ0wsYUFBYTtvQkFDYixJQUFJLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDNUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsRUFBRTt3QkFDeEQsSUFBSSxPQUFPLENBQUM7d0JBQ1osSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTs0QkFDdkIsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0NBQ3JDLE9BQU8sR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUM7NkJBQzFDO2lDQUFNO2dDQUNMLE9BQU8sR0FBRyw0Q0FBNEMsQ0FBQzs2QkFDeEQ7eUJBQ0Y7NkJBQU07NEJBQ0wsT0FBTyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt5QkFDOUI7d0JBRUQsSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDdkQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQzt3QkFDbkMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDL0IsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO3dCQUVsQyx5Q0FBeUM7d0JBQ3pDLElBQUksWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFOzRCQUN0QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt5QkFDM0M7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFBO1lBQ0gsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDaEI7UUFFRCw4REFBOEQ7UUFDOUQsc0RBQXNEO1FBQ3RELElBQUk7UUFDSixxQkFBcUI7UUFDckIsOENBQThDO1FBQzlDLElBQUk7UUFDSix5QkFBeUI7UUFDekIsa0RBQWtEO1FBQ2xELElBQUk7UUFFSiw2QkFBNkI7SUFDL0IsQ0FBQztJQUVELEtBQUssQ0FBQyxHQUFXLEVBQUUsR0FBVyxFQUFFLGFBQXFCLEVBQUU7UUFDckQsSUFBSSxRQUFRLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVELGVBQWUsQ0FBQyxHQUFRLEVBQUUsWUFBb0IsU0FBUyxFQUFFLE9BQWlCLElBQUk7UUFDNUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxTQUFTLEVBQUUsRUFBRTtZQUNyRCxJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO2dCQUN0RCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDL0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDbEU7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsNENBQTRDLENBQUM7Z0JBQ3BFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQ2xDO1lBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2xDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN4QixJQUFJLElBQUksRUFBRTtnQkFDUixJQUFJLEVBQUUsQ0FBQzthQUNSO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUJBQWlCO1FBQ2YsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUFFLEdBQUcsRUFBRTtZQUVoRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDMUI7WUFFRCx1QkFBdUI7WUFDdkIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN4QyxJQUFJLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN0QixPQUFPO2FBQ1I7WUFFRCxJQUFJLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDNUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUN2QixJQUFJLFNBQVMsR0FBRztvQkFDZCxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7b0JBQ2IsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlO29CQUMvQixLQUFLLEVBQUUsS0FBSyxDQUFDLElBQUk7b0JBQ2pCLFNBQVMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJO29CQUNyQyxRQUFRLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRO2lCQUNsQyxDQUFDO2dCQUVGLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO29CQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLHFDQUFxQyxDQUFDLENBQUM7b0JBQ25ELE9BQU87aUJBQ1I7Z0JBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUVoRCxrQ0FBa0M7Z0JBQ2xDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUUxQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUVuRixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDeEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQzFELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztnQkFFL0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBRXZELElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUU7b0JBQzNCLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDdkM7cUJBQU07b0JBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUN4QztZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN2QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxnQkFBZ0I7UUFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUN6RCx5QkFBeUI7WUFDekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRTtnQkFDeEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDbkMsUUFBUSxFQUFFLEtBQUssQ0FBQyxNQUFNO29CQUN0QixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7b0JBQ2IsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlO29CQUUvQixLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLO2lCQUM1QixDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxrQkFBa0I7UUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRTtZQUN6RCxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsRUFBRSxTQUFTLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BGLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDMUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxTQUFpQjtRQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDckMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxpQkFBaUI7UUFDZixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFO1lBQ3pCLFNBQVMsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsVUFBVSxRQUFRO2dCQUN6RCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7b0JBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzFCO2dCQUNELHdCQUF3QjtnQkFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO2dCQUV0RCxJQUFJLENBQUMsUUFBUSxHQUFHO29CQUNkLEdBQUcsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTO2lCQUM5RCxDQUFDO2dCQUVGLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzlCO2dCQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDdkMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO29CQUN2QixJQUFJLEVBQUU7d0JBQ0osR0FBRyxFQUFFLElBQUksQ0FBQyxZQUFZO3dCQUN0QixVQUFVLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDO3dCQUN4QyxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUNuQyxNQUFNLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsU0FBUztxQkFDaEQ7b0JBQ0QsTUFBTSxFQUFFLElBQUk7b0JBQ1osR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHO29CQUNiLFNBQVMsRUFBRSxLQUFLO29CQUNoQixLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLO2lCQUM1QixDQUFDLENBQUM7Z0JBRUgsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3hGLDRCQUE0QjtnQkFDNUIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUE7Z0JBQy9GLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDMUMsSUFBSSxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUM1QyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNuRixDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLCtDQUErQyxDQUFDLENBQUM7U0FDOUQ7SUFDSCxDQUFDO0lBRUQsU0FBUyxDQUFDLFdBQW1CLEdBQUc7UUFDOUIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDNUMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQTtTQUMvRTtRQUNELDZGQUE2RjtJQUMvRixDQUFDO0lBRUQsd0JBQXdCLENBQUMsR0FBVyxFQUFFLEdBQVcsRUFBRSxTQUFrQjtRQUNuRSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ2hCLE9BQU87U0FDUjtRQUNELElBQUksUUFBUSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2hELGlCQUFpQjtRQUNqQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBRWxDLDJDQUEyQztRQUMzQyxtRkFBbUY7UUFDbkYsOEZBQThGO1FBRTlGLDRDQUE0QztRQUM1QyxxRkFBcUY7UUFDckYsaUdBQWlHO1FBRWpHLHNCQUFzQjtRQUN0QiwrRUFBK0U7UUFDL0UsNkJBQTZCO1FBRTdCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLFFBQVEsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNyRyx3Q0FBd0M7UUFDeEMsNEJBQTRCO1FBQzVCLHdCQUF3QjtRQUN4QixxQkFBcUI7UUFDckIsMEJBQTBCO1FBQzFCLHVCQUF1QjtRQUN2QixvQkFBb0I7UUFDcEIsbUJBQW1CO1FBQ25CLHNCQUFzQjtRQUN0QixtQkFBbUI7UUFDbkIsTUFBTTtJQUNSLENBQUM7SUFFRCxVQUFVLENBQUMsU0FBaUIsRUFBRSxTQUFpQixFQUFFLFFBQWdCLEVBQUUsUUFBZ0IsRUFBRSxZQUFvQixTQUFTO1FBQ2hILElBQUksUUFBUSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzVELGlCQUFpQjtRQUNqQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2xDLElBQUksS0FBSyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELElBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDM0YsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDO1FBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNuQyxXQUFXLEVBQUUsU0FBUztZQUN0QixhQUFhLEVBQUUsR0FBRztZQUNsQixZQUFZLEVBQUUsQ0FBQztZQUNmLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHO1lBQ2IsTUFBTSxFQUFFLFFBQVE7WUFDaEIsTUFBTSxFQUFFLE1BQU07U0FDZixDQUFDLENBQUM7SUFDTCxDQUFDOzt3RUF2ZFUsWUFBWTtpREFBWixZQUFZO1FDWHpCLDhCQUNJO1FBQUEsNkRBQ0k7UUFTSiw4QkFDSTtRQUFBLHlCQUE4QztRQUM5QyxtRUFBc0Y7UUFDMUYsaUJBQU07UUFDVixpQkFBTTs7UUFkRyxlQUEwQjtRQUExQiw2Q0FBMEI7UUFZbkIsZUFBc0I7UUFBdEIseUNBQXNCOztrRERGekIsWUFBWTtjQUx4QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFNBQVM7Z0JBQ25CLFdBQVcsRUFBRSxZQUFZO2dCQUN6QixTQUFTLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDMUI7c0NBRVUsU0FBUztrQkFBakIsS0FBSztZQUNHLGdCQUFnQjtrQkFBeEIsS0FBSztZQUNHLGVBQWU7a0JBQXZCLEtBQUs7WUFDRyxVQUFVO2tCQUFsQixLQUFLO1lBQ0csY0FBYztrQkFBdEIsS0FBSztZQUNHLGlCQUFpQjtrQkFBekIsS0FBSztZQUNJLGVBQWU7a0JBQXhCLE1BQU07WUFDRyxXQUFXO2tCQUFwQixNQUFNO1lBQ0csU0FBUztrQkFBbEIsTUFBTTtZQUNHLFVBQVU7a0JBQW5CLE1BQU07WUFDRyxXQUFXO2tCQUFwQixNQUFNO1lBQ0cscUJBQXFCO2tCQUE5QixNQUFNO1lBQ0csdUJBQXVCO2tCQUFoQyxNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAkIGZyb20gJ2pxdWVyeSc7XG5pbXBvcnQgeyBtYXBDb25maWcgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuaW1wb3J0IHsgTWFwIH0gZnJvbSBcIi4vbWFwLm1vZGVsXCI7XG5kZWNsYXJlIGxldCBnb29nbGU6IGFueTtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAndmlhLW1hcCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9tYXAuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL21hcC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTWFwQ29tcG9uZW50IHtcbiAgQElucHV0KCkgY29yZGluYXRlOiBNYXA7XG4gIEBJbnB1dCgpIHNlYXJjaFBsYWNlaG9sZXI6IHN0cmluZztcbiAgQElucHV0KCkgbWFya2VyRHJhZ2dhYmxlOiBib29sZWFuID0gdHJ1ZTtcbiAgQElucHV0KCkgY3VzdG9tSWNvbjogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSBzaG93TXlMb2NhdGlvbjogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpIGhpZGVTZWFyY2hUZXh0Qm94OiBib29sZWFuID0gZmFsc2U7XG4gIEBPdXRwdXQoKSBjb3JkaW5hdGVDaGFuZ2U6IEV2ZW50RW1pdHRlcjxNYXA+ID0gbmV3IEV2ZW50RW1pdHRlcjxNYXA+KCk7XG4gIEBPdXRwdXQoKSBtYXJrZXJDbGljazogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcbiAgQE91dHB1dCgpIG1hcFJhZGl1czogRXZlbnRFbWl0dGVyPE1hcD4gPSBuZXcgRXZlbnRFbWl0dGVyPE1hcD4oKTtcbiAgQE91dHB1dCgpIG1hcERyYWdnZWQ6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XG4gIEBPdXRwdXQoKSBzZWFyY2hFdmVudDogRXZlbnRFbWl0dGVyPE1hcD4gPSBuZXcgRXZlbnRFbWl0dGVyPE1hcD4oKTtcbiAgQE91dHB1dCgpIGdldFRvcENvcmRpbmF0b3JFdmVudDogRXZlbnRFbWl0dGVyPE1hcD4gPSBuZXcgRXZlbnRFbWl0dGVyPE1hcD4oKTtcbiAgQE91dHB1dCgpIGdldEN1cnJlbnRMb2NhdGlvbkV2ZW50OiBFdmVudEVtaXR0ZXI8TWFwPiA9IG5ldyBFdmVudEVtaXR0ZXI8TWFwPigpO1xuXG4gIGxhdGxuZzogYW55O1xuICBnZW9jb2RlcjogYW55O1xuICBteUxhdExuZztcbiAgbWFwO1xuICBteUxvY2F0aW9uO1xuICBtYXJrZXI7XG4gIGlucHV0O1xuICBzZWFyY2hCb3g7XG4gIGdyZWVuUGluSWNvbiA9ICdhc3NldHMvaWNvbnMvZ3JlZW5fcGluLnN2Zyc7XG4gIGxvY2F0aW9uSWNvbiA9ICdhc3NldHMvaWNvbnMvcmVkX3Bpbi5zdmcnO1xuICBtYXJrZXJzID0gW107XG4gIGFjdGl2ZUluZm9XaW5kb3c7XG4gIHBsYWNlaG9sZGVyVGV4dDogc3RyaW5nO1xuICBsb2FkQVBJOiBQcm9taXNlPGFueT47XG4gIGNpcmNsZTogYW55O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIGxldCBzZWxmID0gdGhpcztcbiAgICBzZWxmLnBsYWNlaG9sZGVyVGV4dCA9IHNlbGYuc2VhcmNoUGxhY2Vob2xlcjtcbiAgICBzZWxmLmdlb2NvZGVyID0gbmV3IGdvb2dsZS5tYXBzLkdlb2NvZGVyKCk7XG5cbiAgICBsZXQgaW5pdE1hcCA9ICgpID0+IHtcbiAgICAgIHNlbGYubXlMYXRMbmcgPSB7XG4gICAgICAgIGxhdDogc2VsZi5jb3JkaW5hdGUubGF0aXR1ZGUsIGxuZzogc2VsZi5jb3JkaW5hdGUubG9uZ3RpdHVkZVxuICAgICAgfTtcblxuICAgICAgbGV0IG1hcENvbmZpZ09iamVjdCA9IHtcbiAgICAgICAgem9vbTogMTUsXG4gICAgICAgIGNlbnRlcjogc2VsZi5teUxhdExuZyxcbiAgICAgICAgc3RyZWV0Vmlld0NvbnRyb2w6IGZhbHNlLFxuICAgICAgICBtYXBUeXBlSWQ6IGdvb2dsZS5tYXBzLk1hcFR5cGVJZC5ST0FETUFQLFxuICAgICAgICBtYXBUeXBlQ29udHJvbDogZmFsc2VcbiAgICAgIH07XG5cbiAgICAgIHNlbGYubWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcChkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZ29vZ2xlTWFwJyksIG1hcENvbmZpZ09iamVjdCk7XG5cbiAgICAgIC8vbGlzdGVuIHRvIG1hcCBkcmFnIGV2ZW50XG4gICAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcihzZWxmLm1hcCwgJ2RyYWdlbmQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHNlbGYubWFwRHJhZ2dlZC5lbWl0KFwiXCIpO1xuICAgICAgfSk7XG4gICAgICAvL2xpc3RlbiB0byBtYXAgem9vbSBldmVudFxuICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIoc2VsZi5tYXAsICd6b29tX2NoYW5nZWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChzZWxmLm1hcFJhZGl1cykge1xuICAgICAgICAgIGxldCBjb3JkaW5hdGU6IE1hcCA9IG5ldyBNYXAodW5kZWZpbmVkLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgXCJ6b29tX2NoYW5nZWRcIiwgc2VsZi5tYXAuZ2V0Wm9vbSgpKTtcbiAgICAgICAgICBzZWxmLm1hcFJhZGl1cy5lbWl0KGNvcmRpbmF0ZSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgICBpZiAoIXNlbGYuaGlkZVNlYXJjaFRleHRCb3gpIHtcbiAgICAgICAgc2VsZi5pbnB1dCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwYWMtaW5wdXQnKTtcbiAgICAgICAgc2VsZi5zZWFyY2hCb3ggPSBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLlNlYXJjaEJveChzZWxmLmlucHV0KTtcbiAgICAgICAgc2VsZi5tYXAuYWRkTGlzdGVuZXIoJ2JvdW5kc19jaGFuZ2VkJywgKCkgPT4ge1xuICAgICAgICAgIHNlbGYuc2VhcmNoQm94LnNldEJvdW5kcyhzZWxmLm1hcC5nZXRCb3VuZHMoKSk7XG4gICAgICAgIH0pO1xuICAgICAgICBzZWxmLmFkZFNlYXJjaEJveEV2ZW50KCk7XG4gICAgICB9XG5cbiAgICAgIGlmIChzZWxmLmNvcmRpbmF0ZS50aXRsZSA9PT0gbWFwQ29uZmlnLm15TG9jYXRpb24pIHtcbiAgICAgICAgc2VsZi5teUxvY2F0aW9uID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgcG9zaXRpb246IHNlbGYubXlMYXRMbmcsXG4gICAgICAgICAgaWNvbjoge1xuICAgICAgICAgICAgdXJsOiBzZWxmLmdyZWVuUGluSWNvbiwgLy8gdXJsXG4gICAgICAgICAgICBzY2FsZWRTaXplOiBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSgzMCwgMzApLCAvLyBzY2FsZWQgc2l6ZVxuICAgICAgICAgICAgb3JpZ2luOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCksIC8vIG9yaWdpblxuICAgICAgICAgICAgYW5jaG9yOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCkgLy8gYW5jaG9yXG4gICAgICAgICAgfSxcbiAgICAgICAgICB6SW5kZXg6IDEwMDAsXG4gICAgICAgICAgbWFwOiBzZWxmLm1hcCxcbiAgICAgICAgICBkcmFnZ2FibGU6IGZhbHNlXG4gICAgICAgIH0pO1xuXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzZWxmLm1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgIHBvc2l0aW9uOiBzZWxmLm15TGF0TG5nLFxuICAgICAgICAgIG1hcDogc2VsZi5tYXAsXG4gICAgICAgICAgZHJhZ2dhYmxlOiBzZWxmLm1hcmtlckRyYWdnYWJsZSxcbiAgICAgICAgICBhbmltYXRpb246IGdvb2dsZS5tYXBzLkFuaW1hdGlvbi5EUk9QLFxuICAgICAgICAgIHRpdGxlOiBzZWxmLmNvcmRpbmF0ZS50aXRsZVxuICAgICAgICB9KTtcbiAgICAgICAgc2VsZi5hZGRDbGlja0xpc3RlbmVyKCk7XG4gICAgICAgIHNlbGYuYWRkRHJhZ2VuZExpc3RlbmVyKCk7XG4gICAgICB9XG4gICAgICBzZWxmLmdlb2NvZGVQb3NpdGlvbihzZWxmLm15TGF0TG5nKTtcbiAgICB9XG5cbiAgICBpZiAoc2VsZi5jb3JkaW5hdGUubGF0aXR1ZGUgJiYgc2VsZi5jb3JkaW5hdGUubG9uZ3RpdHVkZSkge1xuICAgICAgaW5pdE1hcCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBhc2sgdXNlciBmb3IgY3VycmVudCBsb2NhdGlvbiBwZXJtaXNzaW9uIFxuICAgICAgaWYgKG5hdmlnYXRvci5nZW9sb2NhdGlvbikge1xuICAgICAgICBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKChsb2NhdGlvbikgPT4ge1xuICAgICAgICAgIHNlbGYuY29yZGluYXRlLmxhdGl0dWRlID0gbG9jYXRpb24uY29vcmRzLmxhdGl0dWRlO1xuICAgICAgICAgIHNlbGYuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSBsb2NhdGlvbi5jb29yZHMubG9uZ2l0dWRlO1xuICAgICAgICAgIGluaXRNYXAoKTtcbiAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgIC8vIGRlZmF1bHQgbG9jYXRpb246IDI4OEgxIE5hbSBLeSBLaG9pIE5naGlhXG4gICAgICAgICAgc2VsZi5jb3JkaW5hdGUubGF0aXR1ZGUgPSAxMC43ODk1Mjk5O1xuICAgICAgICAgIHNlbGYuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSAxMDYuNjg0OTM1OTAwMDAwMDM7XG4gICAgICAgICAgaW5pdE1hcCgpO1xuICAgICAgICB9KVxuICAgICAgfVxuICAgIH1cblxuXG4gIH1cblxuICBnZXRGdXJ0aGVzdFBvaW50RnJvbUxvY2F0aW9uKGxhdDogbnVtYmVyLCBsbmc6IG51bWJlciwgZXZlbnROYW1lOiBzdHJpbmcpIHtcbiAgICBsZXQgY29yZGluYXRlOiBNYXAsIHNlbGYgPSB0aGlzLCBjb3JuZXJzQXJyYXkgPSB7fSwgY29ybmVyMSwgY29ybmVyMiwgY29ybmVyMywgY29ybmVyNCwgZmFyZXN0UG9pbnQ7XG5cbiAgICBsZXQgc1cgPSB0aGlzLm1hcC5nZXRCb3VuZHMoKS5nZXRTb3V0aFdlc3QoKTtcbiAgICBsZXQgbkUgPSB0aGlzLm1hcC5nZXRCb3VuZHMoKS5nZXROb3J0aEVhc3QoKTtcbiAgICBjb3JuZXJzQXJyYXkgPSB7fSwgY29ybmVyMSwgY29ybmVyMiwgY29ybmVyMywgY29ybmVyNDtcblxuICAgIGNvcm5lcjEgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhzVy5sYXQoKSwgc1cubG5nKCkpKTtcbiAgICBjb3JuZXIyID0gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXQsIGxuZyksIG5ldyBnb29nbGUubWFwcy5MYXRMbmcobkUubGF0KCksIG5FLmxuZygpKSk7XG4gICAgY29ybmVyMyA9IGdvb2dsZS5tYXBzLmdlb21ldHJ5LnNwaGVyaWNhbC5jb21wdXRlRGlzdGFuY2VCZXR3ZWVuKG5ldyBnb29nbGUubWFwcy5MYXRMbmcobGF0LCBsbmcpLCBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHNXLmxhdCgpLCBuRS5sbmcoKSkpO1xuICAgIGNvcm5lcjQgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhuRS5sYXQoKSwgc1cubG5nKCkpKTtcblxuICAgIGNvcm5lcnNBcnJheVtjb3JuZXIxXSA9IHtcbiAgICAgIFwibGF0XCI6IHNXLmxhdCgpLFxuICAgICAgXCJsbmdcIjogc1cubG5nKClcbiAgICB9XG4gICAgY29ybmVyc0FycmF5W2Nvcm5lcjJdID0ge1xuICAgICAgXCJsYXRcIjogbkUubGF0KCksXG4gICAgICBcImxuZ1wiOiBuRS5sbmcoKVxuICAgIH1cbiAgICBjb3JuZXJzQXJyYXlbY29ybmVyM10gPSB7XG4gICAgICBcImxhdFwiOiBzVy5sYXQoKSxcbiAgICAgIFwibG5nXCI6IG5FLmxuZygpXG4gICAgfVxuICAgIGNvcm5lcnNBcnJheVtjb3JuZXI0XSA9IHtcbiAgICAgIFwibGF0XCI6IG5FLmxhdCgpLFxuICAgICAgXCJsbmdcIjogc1cubG5nKClcbiAgICB9XG5cblxuICAgIGZhcmVzdFBvaW50ID0gY29ybmVyc0FycmF5W01hdGgubWF4KGNvcm5lcjEsIGNvcm5lcjIsIGNvcm5lcjMsIGNvcm5lcjQpXTtcbiAgICBjb3JkaW5hdGUgPSBuZXcgTWFwKGZhcmVzdFBvaW50LmxhdCwgZmFyZXN0UG9pbnQubG5nLCBcIlwiLCBldmVudE5hbWUpO1xuICAgIHRoaXMubWFwUmFkaXVzLmVtaXQoY29yZGluYXRlKTtcbiAgfVxuXG4gIGdldENlbnRlck1hcExvY2F0aW9uKCkge1xuICAgIHJldHVybiB0aGlzLm1hcC5nZXRDZW50ZXIoKTtcbiAgfVxuXG4gIGNhbGN1bGF0ZURpc3RhbmNlKGFMYXQsIGFMbmcsIGJMYXQsIGJMbmcpIHtcbiAgICByZXR1cm4gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhhTGF0LCBhTG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhiTGF0LCBiTG5nKSk7XG4gIH1cblxuICAvLyBEZWxldGVzIGFsbCBtYXJrZXJzIGluIHRoZSBhcnJheSBieSByZW1vdmluZyByZWZlcmVuY2VzIHRvIHRoZW0uXG4gIGRlbGV0ZU1hcmtlcnMoKSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm1hcmtlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMubWFya2Vyc1tpXS5zZXRNYXAobnVsbCk7XG4gICAgfVxuICAgIHRoaXMubWFya2VycyA9IFtdO1xuICB9XG5cbiAgLy9saXN0TG9jYXRpb25bMF06IGNvZGVcbiAgLy9saXN0TG9jYXRpb25bMV06IExhdGl0dWRlIChwb2ludFkpXG4gIC8vbGlzdExvY2F0aW9uWzJdOiBMb25ndGl0dWRlIChwb2ludFgpXG4gIC8vbGlzdExvY2F0aW9uWzNdOiBpbmRleFxuICAvL2xpc3RMb2NhdGlvbls0XTogbGFiZWxcblxuICBzaG93TXVsdGl0cGxlTG9jYXRpb25zKGxpc3RMb2NhdGlvbjogQXJyYXk8YW55PiA9IFtdKSB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIGxldCBpbmZvd2luZG93ID0gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3c7XG4gICAgbGV0IG1hcmtlciwgaTtcbiAgICBsZXQgYm91bmRzID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZ0JvdW5kcygpO1xuICAgIHRoaXMuZGVsZXRlTWFya2VycygpO1xuXG4gICAgZm9yIChpID0gMDsgaSA8IGxpc3RMb2NhdGlvbi5sZW5ndGg7IGkrKykge1xuICAgICAgLy9tYXJrZXIgQ09ORklHXG4gICAgICBsZXQgbWFya09iamVjdCA9IHtcbiAgICAgICAgcG9zaXRpb246IG5ldyBnb29nbGUubWFwcy5MYXRMbmcobGlzdExvY2F0aW9uW2ldWzFdLCBsaXN0TG9jYXRpb25baV1bMl0pLFxuICAgICAgICAvL2xhYmVsT3JpZ2luOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoOSwgOSksXG4gICAgICAgIC8vICAgbGFiZWw6IHtcbiAgICAgICAgLy8gICAgIHRleHQ6IGxpc3RMb2NhdGlvbltpXVs0XT8gbGlzdExvY2F0aW9uW2ldWzRdOiBcIlwiLFxuICAgICAgICAvLyAgICAgZm9udFNpemU6IFwiMTJweFwiLFxuICAgICAgICAvLyAgICAgY29sb3I6IFwiI2U3NGMzY1wiLFxuICAgICAgICAvLyAgICAgZm9udEZhbWlseTogXCJtb250c2VycmF0XCJcbiAgICAgICAgLy8gIH0sXG4gICAgICAgIHNjYWxlZFNpemU6IG5ldyBnb29nbGUubWFwcy5TaXplKDMwLCAzMCksXG4gICAgICAgIG1hcDogdGhpcy5tYXBcbiAgICAgIH07XG5cbiAgICAgIG1hcmtPYmplY3RbXCJpY29uXCJdID0ge1xuICAgICAgICB1cmw6IHRoaXMubG9jYXRpb25JY29uLCAvLyB1cmxcbiAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSwgLy8gc2NhbGVkIHNpemVcbiAgICAgICAgb3JpZ2luOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCksIC8vIG9yaWdpblxuICAgICAgICBhbmNob3I6IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSAvLyBhbmNob3JcbiAgICAgIH07XG4gICAgICBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKG1hcmtPYmplY3QpO1xuICAgICAgdGhpcy5tYXJrZXJzLnB1c2gobWFya2VyKTtcbiAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKG1hcmtlciwgJ2NsaWNrJywgKGZ1bmN0aW9uIChtYXJrZXIsIGkpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAvL2dldCBhZGRyZXNzXG4gICAgICAgICAgbGV0IGxhdGxuZyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcobGlzdExvY2F0aW9uW2ldWzFdLCBsaXN0TG9jYXRpb25baV1bMl0pO1xuICAgICAgICAgIHNlbGYuZ2VvY29kZXIuZ2VvY29kZSh7ICdsYXRMbmcnOiBsYXRsbmcgfSwgKHJlc3BvbnNlcykgPT4ge1xuICAgICAgICAgICAgbGV0IGFkZHJlc3M7XG4gICAgICAgICAgICBpZiAoIWxpc3RMb2NhdGlvbltpXVs0XSkge1xuICAgICAgICAgICAgICBpZiAocmVzcG9uc2VzICYmIHJlc3BvbnNlcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgYWRkcmVzcyA9IHJlc3BvbnNlc1swXS5mb3JtYXR0ZWRfYWRkcmVzcztcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBhZGRyZXNzID0gXCJDYW5ub3QgZGV0ZXJtaW5lIGFkZHJlc3MgYXQgdGhpcyBsb2NhdGlvbi5cIjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgYWRkcmVzcyA9IGxpc3RMb2NhdGlvbltpXVs0XTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgc2VsZi5hY3RpdmVJbmZvV2luZG93ICYmIHNlbGYuYWN0aXZlSW5mb1dpbmRvdy5jbG9zZSgpO1xuICAgICAgICAgICAgc2VsZi5hY3RpdmVJbmZvV2luZG93ID0gaW5mb3dpbmRvdztcbiAgICAgICAgICAgIGluZm93aW5kb3cuc2V0Q29udGVudChhZGRyZXNzKTtcbiAgICAgICAgICAgIGluZm93aW5kb3cub3BlbihzZWxmLm1hcCwgbWFya2VyKTtcblxuICAgICAgICAgICAgLy9tYXJrZXIgb2JqZWN0IFt0aXRsZSwgbGF0LCBsb25nLCBpbmRleF1cbiAgICAgICAgICAgIGlmIChsaXN0TG9jYXRpb25baV1bM10pIHtcbiAgICAgICAgICAgICAgc2VsZi5tYXJrZXJDbGljay5lbWl0KGxpc3RMb2NhdGlvbltpXVszXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH0pKG1hcmtlciwgaSkpO1xuICAgIH1cblxuICAgIC8vIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCB0aGlzLm1hcmtlcnMubGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgLy8gICBib3VuZHMuZXh0ZW5kKHRoaXMubWFya2Vyc1tpbmRleF0uZ2V0UG9zaXRpb24oKSk7XG4gICAgLy8gfVxuICAgIC8vIGlmIChzZWxmLm1hcmtlcikge1xuICAgIC8vICAgYm91bmRzLmV4dGVuZChzZWxmLm1hcmtlci5nZXRQb3NpdGlvbigpKTtcbiAgICAvLyB9XG4gICAgLy8gaWYgKHNlbGYubXlMb2NhdGlvbikge1xuICAgIC8vICAgYm91bmRzLmV4dGVuZChzZWxmLm15TG9jYXRpb24uZ2V0UG9zaXRpb24oKSk7XG4gICAgLy8gfVxuXG4gICAgLy90aGlzLm1hcC5maXRCb3VuZHMoYm91bmRzKTtcbiAgfVxuXG4gIHBhblRvKGxhdDogbnVtYmVyLCBsbmc6IG51bWJlciwgem9vbU51bWJlcjogbnVtYmVyID0gMTIpIHtcbiAgICBsZXQgbXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKTtcbiAgICB0aGlzLm1hcC5zZXRab29tKHpvb21OdW1iZXIpO1xuICAgIHRoaXMubWFwLnBhblRvKG15TGF0TG5nKTtcbiAgfVxuXG4gIGdlb2NvZGVQb3NpdGlvbihwb3M6IGFueSwgZXZlbnROYW1lOiBzdHJpbmcgPSB1bmRlZmluZWQsIGZ1bmM6IEZ1bmN0aW9uID0gbnVsbCk6IHZvaWQge1xuICAgIHRoaXMuZ2VvY29kZXIuZ2VvY29kZSh7ICdsYXRMbmcnOiBwb3MgfSwgKHJlc3BvbnNlcykgPT4ge1xuICAgICAgaWYgKHJlc3BvbnNlcyAmJiByZXNwb25zZXMubGVuZ3RoID4gMCkge1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS50aXRsZSA9IHJlc3BvbnNlc1swXS5mb3JtYXR0ZWRfYWRkcmVzcztcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSByZXNwb25zZXNbMF0uZ2VvbWV0cnkubG9jYXRpb24ubGF0KCk7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSByZXNwb25zZXNbMF0uZ2VvbWV0cnkubG9jYXRpb24ubG5nKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS50aXRsZSA9IFwiQ2Fubm90IGRldGVybWluZSBhZGRyZXNzIGF0IHRoaXMgbG9jYXRpb24uXCI7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLmxhdGl0dWRlID0gbnVsbDtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IG51bGw7XG4gICAgICB9XG4gICAgICB0aGlzLnRyaWdnZXJVcGRhdGVEYXRhKGV2ZW50TmFtZSk7XG4gICAgICAkKFwiI3BhYy1pbnB1dFwiKS5jbGljaygpO1xuICAgICAgaWYgKGZ1bmMpIHtcbiAgICAgICAgZnVuYygpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgYWRkU2VhcmNoQm94RXZlbnQoKTogdm9pZCB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIHNlbGYuc2VhcmNoQm94LmFkZExpc3RlbmVyKCdwbGFjZXNfY2hhbmdlZCcsICgpID0+IHtcblxuICAgICAgaWYgKHNlbGYubWFya2VyKSB7XG4gICAgICAgIHNlbGYubWFya2VyLnNldE1hcChudWxsKTtcbiAgICAgIH1cblxuICAgICAgLy9zZWxmLmRlbGV0ZU1hcmtlcnMoKTtcbiAgICAgIGxldCBwbGFjZXMgPSBzZWxmLnNlYXJjaEJveC5nZXRQbGFjZXMoKTtcbiAgICAgIGlmIChwbGFjZXMubGVuZ3RoID09IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBsZXQgYm91bmRzID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZ0JvdW5kcygpO1xuICAgICAgcGxhY2VzLmZvckVhY2goKHBsYWNlKSA9PiB7XG4gICAgICAgIGxldCBtYXJrZXJPYmogPSB7XG4gICAgICAgICAgbWFwOiBzZWxmLm1hcCxcbiAgICAgICAgICBkcmFnZ2FibGU6IHRoaXMubWFya2VyRHJhZ2dhYmxlLFxuICAgICAgICAgIHRpdGxlOiBwbGFjZS5uYW1lLFxuICAgICAgICAgIGFuaW1hdGlvbjogZ29vZ2xlLm1hcHMuQW5pbWF0aW9uLkRST1AsXG4gICAgICAgICAgcG9zaXRpb246IHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uXG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKCFwbGFjZS5nZW9tZXRyeSkge1xuICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmV0dXJuZWQgcGxhY2UgY29udGFpbnMgbm8gZ2VvbWV0cnlcIik7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgc2VsZi5tYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKG1hcmtlck9iaik7XG5cbiAgICAgICAgLy8gc2VsZi5tYXJrZXJzLnB1c2goc2VsZi5tYXJrZXIpO1xuICAgICAgICBzZWxmLmFkZERyYWdlbmRMaXN0ZW5lcigpO1xuXG4gICAgICAgIHNlbGYuc2VhcmNoRXZlbnQuZW1pdChuZXcgTWFwKG1hcmtlck9iai5wb3NpdGlvbi5sYXQoKSwgbWFya2VyT2JqLnBvc2l0aW9uLmxuZygpKSk7XG5cbiAgICAgICAgc2VsZi5jb3JkaW5hdGUubGF0aXR1ZGUgPSBwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbi5sYXQoKTtcbiAgICAgICAgc2VsZi5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpO1xuICAgICAgICBzZWxmLmNvcmRpbmF0ZS50aXRsZSA9IHBsYWNlLmZvcm1hdHRlZF9hZGRyZXNzO1xuXG4gICAgICAgIHNlbGYudHJpZ2dlclVwZGF0ZURhdGEobWFwQ29uZmlnLm1hcEV2ZW50TmFtZXMuc2VhcmNoKTtcblxuICAgICAgICBpZiAocGxhY2UuZ2VvbWV0cnkudmlld3BvcnQpIHtcbiAgICAgICAgICBib3VuZHMudW5pb24ocGxhY2UuZ2VvbWV0cnkudmlld3BvcnQpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGJvdW5kcy5leHRlbmQocGxhY2UuZ2VvbWV0cnkubG9jYXRpb24pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHNlbGYubWFwLmZpdEJvdW5kcyhib3VuZHMpO1xuICAgICAgc2VsZi5tYXAuc2V0Wm9vbSgxMik7XG4gICAgICBzZWxmLm1hcC5wYW5UbyhzZWxmLm1hcmtlci5wb3NpdGlvbik7XG4gICAgfSk7XG4gIH1cblxuICBhZGRDbGlja0xpc3RlbmVyKCk6IHZvaWQge1xuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwLCAnY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICAgIC8vZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIHRoaXMuZ2VvY29kZVBvc2l0aW9uKGV2ZW50LmxhdExuZywgbWFwQ29uZmlnLm1hcEV2ZW50TmFtZXMubWFwQ2xpY2ssICgpID0+IHtcbiAgICAgICAgdGhpcy5tYXJrZXIuc2V0TWFwKG51bGwpO1xuICAgICAgICB0aGlzLm1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgIHBvc2l0aW9uOiBldmVudC5sYXRMbmcsXG4gICAgICAgICAgbWFwOiB0aGlzLm1hcCxcbiAgICAgICAgICBkcmFnZ2FibGU6IHRoaXMubWFya2VyRHJhZ2dhYmxlLFxuXG4gICAgICAgICAgdGl0bGU6IHRoaXMuY29yZGluYXRlLnRpdGxlXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLmFkZERyYWdlbmRMaXN0ZW5lcigpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBhZGREcmFnZW5kTGlzdGVuZXIoKTogdm9pZCB7XG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXJrZXIsICdkcmFnZW5kJywgKCkgPT4ge1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIHRoaXMuZGVsZXRlTWFya2VycygpO1xuICAgICAgdGhpcy5nZW9jb2RlUG9zaXRpb24odGhpcy5tYXJrZXIuZ2V0UG9zaXRpb24oKSwgbWFwQ29uZmlnLm1hcEV2ZW50TmFtZXMubWFya2VyRHJhZyk7XG4gICAgICB0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSA9IHRoaXMubWFya2VyLmdldFBvc2l0aW9uKCkubGF0KCk7XG4gICAgICB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlID0gdGhpcy5tYXJrZXIuZ2V0UG9zaXRpb24oKS5sbmcoKTtcbiAgICB9KTtcbiAgfVxuXG4gIHRyaWdnZXJVcGRhdGVEYXRhKGV2ZW50TmFtZTogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy5jb3JkaW5hdGUuZXZlbnROYW1lID0gZXZlbnROYW1lO1xuICAgIHRoaXMuY29yZGluYXRlQ2hhbmdlLmVtaXQodGhpcy5jb3JkaW5hdGUpO1xuICB9XG5cbiAgbXlDdXJyZW50TG9jYXRpb24oKSB7XG4gICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgIGlmIChuYXZpZ2F0b3IuZ2VvbG9jYXRpb24pIHtcbiAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oZnVuY3Rpb24gKGxvY2F0aW9uKSB7XG4gICAgICAgIGlmIChzZWxmLm1hcmtlcikge1xuICAgICAgICAgIHNlbGYubWFya2VyLnNldE1hcChudWxsKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBzZWxmLmRlbGV0ZU1hcmtlcnMoKTtcbiAgICAgICAgc2VsZi5jb3JkaW5hdGUubGF0aXR1ZGUgPSBsb2NhdGlvbi5jb29yZHMubGF0aXR1ZGU7XG4gICAgICAgIHNlbGYuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSBsb2NhdGlvbi5jb29yZHMubG9uZ2l0dWRlO1xuXG4gICAgICAgIHNlbGYubXlMYXRMbmcgPSB7XG4gICAgICAgICAgbGF0OiBsb2NhdGlvbi5jb29yZHMubGF0aXR1ZGUsIGxuZzogbG9jYXRpb24uY29vcmRzLmxvbmdpdHVkZVxuICAgICAgICB9O1xuXG4gICAgICAgIGlmIChzZWxmLm15TG9jYXRpb24pIHtcbiAgICAgICAgICBzZWxmLm15TG9jYXRpb24uc2V0TWFwKG51bGwpO1xuICAgICAgICB9XG4gICAgICAgIHNlbGYubXlMb2NhdGlvbiA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgIHBvc2l0aW9uOiBzZWxmLm15TGF0TG5nLFxuICAgICAgICAgIGljb246IHtcbiAgICAgICAgICAgIHVybDogc2VsZi5ncmVlblBpbkljb24sIC8vIHVybFxuICAgICAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSwgLy8gc2NhbGVkIHNpemVcbiAgICAgICAgICAgIG9yaWdpbjogbmV3IGdvb2dsZS5tYXBzLlBvaW50KDAsIDApLCAvLyBvcmlnaW5cbiAgICAgICAgICAgIGFuY2hvcjogbmV3IGdvb2dsZS5tYXBzLlBvaW50KDE1LCAxNSkgLy8gYW5jaG9yXG4gICAgICAgICAgfSxcbiAgICAgICAgICB6SW5kZXg6IDEwMDAsXG4gICAgICAgICAgbWFwOiBzZWxmLm1hcCxcbiAgICAgICAgICBkcmFnZ2FibGU6IGZhbHNlLFxuICAgICAgICAgIHRpdGxlOiBzZWxmLmNvcmRpbmF0ZS50aXRsZVxuICAgICAgICB9KTtcblxuICAgICAgICBzZWxmLmdlb2NvZGVQb3NpdGlvbihzZWxmLm15TG9jYXRpb24uZ2V0UG9zaXRpb24oKSwgbWFwQ29uZmlnLm1hcEV2ZW50TmFtZXMubXlMb2NhdGlvbik7XG4gICAgICAgIC8vc2VsZi5hZGREcmFnZW5kTGlzdGVuZXIoKTtcbiAgICAgICAgc2VsZi5nZXRDdXJyZW50TG9jYXRpb25FdmVudC5lbWl0KG5ldyBNYXAobG9jYXRpb24uY29vcmRzLmxhdGl0dWRlLCBsb2NhdGlvbi5jb29yZHMubG9uZ2l0dWRlKSlcbiAgICAgICAgbGV0IGN1cnJlbnRab29tTGV2ZWwgPSBzZWxmLm1hcC5nZXRab29tKCk7XG4gICAgICAgIGxldCBib3VuZHMgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nQm91bmRzKCk7XG4gICAgICAgIHNlbGYubWFwLnNldENlbnRlcihuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHNlbGYubXlMYXRMbmcubGF0LCBzZWxmLm15TGF0TG5nLmxuZykpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnNvbGUubG9nKFwiR2VvbG9jYXRpb24gaXMgbm90IHN1cHBvcnRlZCBieSB0aGlzIGJyb3dzZXIuXCIpO1xuICAgIH1cbiAgfVxuXG4gIGdldFJhZGl1cyhkaXN0YW5jZTogbnVtYmVyID0gMTAwKSB7XG4gICAgaWYgKHRoaXMubXlMb2NhdGlvbikge1xuICAgICAgbGV0IHcgPSB0aGlzLm1hcC5nZXRCb3VuZHMoKS5nZXRTb3V0aFdlc3QoKTtcbiAgICAgIGxldCBjZW50ZXJMbmcgPSB0aGlzLm15TG9jYXRpb24uZ2V0UG9zaXRpb24oKTtcbiAgICAgIGRpc3RhbmNlID0gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4oY2VudGVyTG5nLCB3KVxuICAgIH1cbiAgICAvL3RoaXMubWFwUmFkaXVzLmVtaXQoZGlzdGFuY2UgPyAoZGlzdGFuY2UgLyAxMDAwKS50b0ZpeGVkKDEpIDogXCJDQU5OT1RfQ0FMQ1VMQVRFX0RJU1RBTkNFXCIpO1xuICB9XG5cbiAgZ2V0VG9wRGlzdGFuY2VGcm9tTWFya2VyKGxhdDogbnVtYmVyLCBsbmc6IG51bWJlciwgZXZlbnROYW1lPzogc3RyaW5nKSB7XG4gICAgaWYgKCFsYXQgfHwgIWxuZykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBsZXQgbXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKTtcbiAgICAvLyBHZXQgbWFwIGJvdW5kc1xuICAgIGxldCBib3VuZHMgPSB0aGlzLm1hcC5nZXRCb3VuZHMoKTtcblxuICAgIC8vIEZpbmQgbWFwIHRvcCBib3JkZXIgJiBkaXN0YW5jZSB0byBjZW50ZXJcbiAgICAvL2xldCBtYXBUb3AgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGJvdW5kcy5nZXROb3J0aEVhc3QoKS5sYXQoKSwgbXlMYXRMbmcubG5nKCkpO1xuICAgIC8vbGV0IGRpc3RhbmNlVG9Ub3AgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihteUxhdExuZywgbWFwVG9wKTtcblxuICAgIC8vIEZpbmQgbWFwIGxlZnQgYm9yZGVyICYgZGlzdGFuY2UgdG8gY2VudGVyXG4gICAgLy8gbGV0IG1hcExlZnQgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKG15TGF0TG5nLmxhdCgpLCBib3VuZHMuZ2V0U291dGhXZXN0KCkubG5nKCkpO1xuICAgIC8vIGxldCBkaXN0YW5jZVRvTGVmdCA9IGdvb2dsZS5tYXBzLmdlb21ldHJ5LnNwaGVyaWNhbC5jb21wdXRlRGlzdGFuY2VCZXR3ZWVuKG15TGF0TG5nLCBtYXBMZWZ0KTtcblxuICAgIC8vIEdldCBzaG9ydGVzdCBsZW5ndGhcbiAgICAvL2xldCByYWRpdXMgPSBkaXN0YW5jZVRvVG9wIDwgZGlzdGFuY2VUb0xlZnQgPyBkaXN0YW5jZVRvVG9wIDogZGlzdGFuY2VUb0xlZnQ7XG4gICAgLy9sZXQgcmFkaXVzID0gZGlzdGFuY2VUb1RvcDtcblxuICAgIHRoaXMuZ2V0VG9wQ29yZGluYXRvckV2ZW50LmVtaXQobmV3IE1hcChib3VuZHMuZ2V0Tm9ydGhFYXN0KCkubGF0KCksIG15TGF0TG5nLmxuZygpLCBcIlwiLCBldmVudE5hbWUpKTtcbiAgICAvLyBsZXQgY2lyY2xlID0gbmV3IGdvb2dsZS5tYXBzLkNpcmNsZSh7XG4gICAgLy8gICBzdHJva2VDb2xvcjogJyNGRjAwMDAnLFxuICAgIC8vICAgc3Ryb2tlT3BhY2l0eTogMC44LFxuICAgIC8vICAgc3Ryb2tlV2VpZ2h0OiAyLFxuICAgIC8vICAgZmlsbENvbG9yOiAnI0ZGMDAwMCcsXG4gICAgLy8gICBmaWxsT3BhY2l0eTogMC4zNSxcbiAgICAvLyAgIGVkaXRhYmxlOiB0cnVlLFxuICAgIC8vICAgbWFwOiB0aGlzLm1hcCxcbiAgICAvLyAgIGNlbnRlcjogbXlMYXRMbmcsXG4gICAgLy8gICByYWRpdXM6IHJhZGl1c1xuICAgIC8vIH0pO1xuICB9XG5cbiAgZHJhd0NpcmNsZShjZW50ZXJMYXQ6IG51bWJlciwgY2VudGVyTG5nOiBudW1iZXIsIHBvaW50TGF0OiBudW1iZXIsIHBvaW50TG5nOiBudW1iZXIsIGZpbGxDb2xvcjogc3RyaW5nID0gXCIjRkYwMDAwXCIpIHtcbiAgICBsZXQgbXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGNlbnRlckxhdCwgY2VudGVyTG5nKTtcbiAgICAvLyBHZXQgbWFwIGJvdW5kc1xuICAgIGxldCBib3VuZHMgPSB0aGlzLm1hcC5nZXRCb3VuZHMoKTtcbiAgICBsZXQgcG9pbnQgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHBvaW50TGF0LCBwb2ludExuZyk7XG4gICAgbGV0IGRpc3RhbmNlVG9Ub3AgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihteUxhdExuZywgcG9pbnQpO1xuICAgIGxldCByYWRpdXMgPSBkaXN0YW5jZVRvVG9wO1xuICAgIHRoaXMuY2lyY2xlLnNldE1hcChudWxsKTtcbiAgICB0aGlzLmNpcmNsZSA9IG5ldyBnb29nbGUubWFwcy5DaXJjbGUoe1xuICAgICAgc3Ryb2tlQ29sb3I6ICcjRkYwMDAwJyxcbiAgICAgIHN0cm9rZU9wYWNpdHk6IDAuOCxcbiAgICAgIHN0cm9rZVdlaWdodDogMixcbiAgICAgIGZpbGxDb2xvcjogZmlsbENvbG9yLFxuICAgICAgZmlsbE9wYWNpdHk6IDAuMzUsXG4gICAgICBlZGl0YWJsZTogdHJ1ZSxcbiAgICAgIG1hcDogdGhpcy5tYXAsXG4gICAgICBjZW50ZXI6IG15TGF0TG5nLFxuICAgICAgcmFkaXVzOiByYWRpdXNcbiAgICB9KTtcbiAgfVxufSIsIjxkaXYgY2xhc3M9XCJyb3dcIj5cbiAgICA8ZGl2ICpuZ0lmPVwiIWhpZGVTZWFyY2hUZXh0Qm94XCIgY2xhc3M9XCJjb2wtc20tMTIgbWFwLXNlYXJjaC1jb250YWluZXJcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cbiAgICAgICAgICAgIDxkaXYgaWQ9XCJ2aWEtbWFwLXNlYXJjaFwiPlxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJkdW1teS1jb2xcIj48L2Rpdj5cbiAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIGlkPVwicGFjLWlucHV0XCIgYXJpYS1kZXNjcmliZWRieT1cInRleHRIZWxwXCIgcGxhY2Vob2xkZXI9XCJcIlxuICAgICAgICAgICAgICAgICAgICBbKG5nTW9kZWwpXT1cImNvcmRpbmF0ZS50aXRsZVwiPlxuICAgICAgICAgICAgICAgIDxpbWcgY2xhc3M9XCJpbnB1dC1zZWFyY2gtaWNvblwiIHNyYz1cIi9hc3NldHMvaWNvbnMvaWNfc2VhcmNoLnN2Z1wiPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMTJcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImdvb2dsZS1tYXBzXCIgaWQ9XCJnb29nbGVNYXBcIj48L2Rpdj5cbiAgICAgICAgPGJ1dHRvbiAqbmdJZj1cInNob3dNeUxvY2F0aW9uXCIgIChjbGljayk9XCJteUN1cnJlbnRMb2NhdGlvbigpXCIgY2xhc3M9XCJteS1sb2NhdGlvbi1idG5cIj48aW1nIHNyYz1cImFzc2V0cy9pY29ucy9ncmVlbl9waW4uc3ZnXCI+PC9idXR0b24+XG4gICAgPC9kaXY+XG48L2Rpdj4iXX0=