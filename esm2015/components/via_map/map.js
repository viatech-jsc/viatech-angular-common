import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { mapConfig } from './constant';
import { CordinateDto } from "./dto/cordinate.dto";
import MarkerClusterer from '@google/markerclustererplus';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@angular/material/form-field";
import * as i3 from "@angular/material/input";
import * as i4 from "@angular/forms";
import * as i5 from "@angular/material/button";
import * as i6 from "@angular/material/icon";
import * as i7 from "@angular/material/progress-spinner";
const _c0 = ["searchInput"];
const _c1 = ["googleMap"];
function MapComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r6 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 7);
    i0.ɵɵelementStart(1, "div", 8);
    i0.ɵɵelementStart(2, "mat-form-field", 9);
    i0.ɵɵelementStart(3, "input", 10, 11);
    i0.ɵɵlistener("ngModelChange", function MapComponent_div_1_Template_input_ngModelChange_3_listener($event) { i0.ɵɵrestoreView(_r6); const ctx_r5 = i0.ɵɵnextContext(); return ctx_r5.cordinate.title = $event; });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "button", 12);
    i0.ɵɵelementStart(6, "mat-icon");
    i0.ɵɵtext(7, "search");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("placeholder", ctx_r0.searchPlaceholer)("ngModel", ctx_r0.cordinate.title);
} }
function MapComponent_button_5_Template(rf, ctx) { if (rf & 1) {
    const _r8 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function MapComponent_button_5_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r8); const ctx_r7 = i0.ɵɵnextContext(); return ctx_r7.moveToMyLocation(); });
    i0.ɵɵelement(1, "img", 14);
    i0.ɵɵelementEnd();
} }
function MapComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 15);
    i0.ɵɵelement(1, "mat-spinner", 16);
    i0.ɵɵelementEnd();
} }
export class MapComponent {
    constructor() {
        this.markerDraggable = true;
        this.showMyLocation = true;
        this.hideSearchTextBox = false;
        this.requestLocation = true;
        this.loading = false;
        this.myLocationIcon = 'assets/icons/green_pin.svg';
        this.markerIcon = 'assets/icons/red_pin.svg';
        this.shouldClusterer = false;
        this.clustererImagepath = "assets/markerclusterer/m";
        this.cordinateChange = new EventEmitter();
        this.markerClick = new EventEmitter();
        this.mapRadius = new EventEmitter();
        this.searchEvent = new EventEmitter();
        this.getTopCordinatorEvent = new EventEmitter();
        this.getCurrentLocationEvent = new EventEmitter();
        this.boundsChanged = new EventEmitter();
        this.centerChanged = new EventEmitter();
        this.clickOnMap = new EventEmitter();
        this.dblClick = new EventEmitter();
        this.drag = new EventEmitter();
        this.dragEnd = new EventEmitter();
        this.dragStart = new EventEmitter();
        this.idle = new EventEmitter();
        this.mouseMove = new EventEmitter();
        this.mouseOut = new EventEmitter();
        this.mouseOver = new EventEmitter();
        this.tilesLoaded = new EventEmitter();
        this.zoomChanged = new EventEmitter();
        this.markers = [];
        this.myLocaltionLoaded = false;
        this.mapLoaded = false;
    }
    instance() {
        return this.map;
    }
    getCenterMapLocation() {
        return this.instance().getCenter();
    }
    getMarkerCluster() {
        return this.markerCluster;
    }
    ngAfterViewInit() {
        this.geocoder = new google.maps.Geocoder();
        if (this.cordinate.latitude && this.cordinate.longtitude) {
            this.initMap();
        }
        else {
            if (this.requestLocation && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((location) => {
                    this.cordinate.latitude = location.coords.latitude;
                    this.cordinate.longtitude = location.coords.longitude;
                    this.initMap();
                    this.myLocaltionLoaded = true;
                }, () => {
                    this.cordinate.latitude = 10.762622;
                    this.cordinate.longtitude = 106.660172;
                    this.initMap();
                });
            }
            else {
                this.cordinate.latitude = 10.762622;
                this.cordinate.longtitude = 106.660172;
                this.initMap();
            }
        }
    }
    initMap() {
        this.myLatLng = new google.maps.LatLng(this.cordinate.latitude, this.cordinate.longtitude);
        let mapConfigObject = {
            zoom: 15,
            center: this.myLatLng,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false
        };
        this.map = new google.maps.Map(this.googleMap.nativeElement, mapConfigObject);
        if (!this.hideSearchTextBox) {
            this.searchBox = new google.maps.places.SearchBox(this.searchInput.nativeElement);
            this.addSearchBoxEvent();
        }
        this.bindingEvent();
        if (this.cordinate.title === mapConfig.myLocation) {
            this.myLocation = new google.maps.Marker({
                position: this.myLatLng,
                icon: {
                    url: this.myLocationIcon,
                    scaledSize: new google.maps.Size(30, 30),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0) // anchor
                },
                zIndex: 1000,
                map: this.map,
                draggable: false
            });
        }
        else {
            this.marker = new google.maps.Marker({
                position: this.myLatLng,
                map: this.map,
                draggable: this.markerDraggable,
                animation: google.maps.Animation.DROP,
                title: this.cordinate.title
            });
            this.addClickListener();
            this.addDragendListener();
        }
        this.geocodePosition(this.myLatLng);
        if (this.shouldClusterer) {
            this.markerCluster = new MarkerClusterer(this.map, [], {
                imagePath: this.clustererImagepath,
            });
        }
    }
    bindingEvent() {
        google.maps.event.addListener(this.map, 'bounds_changed', () => {
            var _a;
            let bounds = this.map.getBounds();
            if (!this.hideSearchTextBox) {
                this.searchBox.setBounds(bounds);
            }
            (_a = this.boundsChanged) === null || _a === void 0 ? void 0 : _a.emit(bounds);
        });
        google.maps.event.addListener(this.map, 'center_changed', () => {
            var _a;
            let center = this.map.getCenter();
            (_a = this.centerChanged) === null || _a === void 0 ? void 0 : _a.emit(center);
        });
        google.maps.event.addListener(this.map, 'click', (event) => {
            var _a;
            (_a = this.clickOnMap) === null || _a === void 0 ? void 0 : _a.emit(event);
        });
        google.maps.event.addListener(this.map, 'dblclick', (event) => {
            var _a;
            (_a = this.dblClick) === null || _a === void 0 ? void 0 : _a.emit(event);
        });
        google.maps.event.addListener(this.map, 'drag', () => {
            var _a;
            (_a = this.drag) === null || _a === void 0 ? void 0 : _a.emit("drag");
        });
        google.maps.event.addListener(this.map, 'dragend', () => {
            var _a;
            (_a = this.dragEnd) === null || _a === void 0 ? void 0 : _a.emit("dragend");
        });
        google.maps.event.addListener(this.map, 'dragstart', () => {
            var _a;
            (_a = this.dragStart) === null || _a === void 0 ? void 0 : _a.emit("dragstart");
        });
        google.maps.event.addListener(this.map, 'idle', () => {
            var _a;
            this.mapLoaded = true;
            (_a = this.idle) === null || _a === void 0 ? void 0 : _a.emit("idle");
        });
        google.maps.event.addListener(this.map, 'mousemove', (event) => {
            var _a;
            (_a = this.mouseMove) === null || _a === void 0 ? void 0 : _a.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseout', (event) => {
            var _a;
            (_a = this.mouseOut) === null || _a === void 0 ? void 0 : _a.emit(event);
        });
        google.maps.event.addListener(this.map, 'mouseover', (event) => {
            var _a;
            (_a = this.mouseOver) === null || _a === void 0 ? void 0 : _a.emit(event);
        });
        google.maps.event.addListener(this.map, "tilesloaded", () => {
            var _a;
            (_a = this.tilesLoaded) === null || _a === void 0 ? void 0 : _a.emit("tilesloaded");
        });
        google.maps.event.addListener(this.map, 'zoom_changed', () => {
            var _a;
            (_a = this.zoomChanged) === null || _a === void 0 ? void 0 : _a.emit(this.map.getZoom());
        });
    }
    getFurthestPointFromLocation(lat, lng, eventName) {
        var _a;
        let cordinate, cornersArray = {}, corner1, corner2, corner3, corner4, farestPoint;
        let sW = this.map.getBounds().getSouthWest();
        let nE = this.map.getBounds().getNorthEast();
        cornersArray = {}, corner1, corner2, corner3, corner4;
        corner1 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), sW.lng()));
        corner2 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), nE.lng()));
        corner3 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), nE.lng()));
        corner4 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), sW.lng()));
        cornersArray[corner1] = {
            "lat": sW.lat(),
            "lng": sW.lng()
        };
        cornersArray[corner2] = {
            "lat": nE.lat(),
            "lng": nE.lng()
        };
        cornersArray[corner3] = {
            "lat": sW.lat(),
            "lng": nE.lng()
        };
        cornersArray[corner4] = {
            "lat": nE.lat(),
            "lng": sW.lng()
        };
        farestPoint = cornersArray[Math.max(corner1, corner2, corner3, corner4)];
        cordinate = new CordinateDto(farestPoint.lat, farestPoint.lng, "", eventName);
        (_a = this.mapRadius) === null || _a === void 0 ? void 0 : _a.emit(cordinate);
    }
    calculateDistance(aLat, aLng, bLat, bLng) {
        return google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(aLat, aLng), new google.maps.LatLng(bLat, bLng));
    }
    // Deletes all markers in the array by removing references to them.
    deleteMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        if (this.shouldClusterer) {
            this.markerCluster.clearMarkers();
        }
        this.markers = [];
    }
    showMultitpleLocations(listLocation = []) {
        let self = this;
        let infowindow = new google.maps.InfoWindow;
        let marker, i;
        this.deleteMarkers();
        for (i = 0; i < listLocation.length; i++) {
            //marker CONFIG
            let markObject = {
                position: new google.maps.LatLng(listLocation[i][1], listLocation[i][2]),
                scaledSize: new google.maps.Size(30, 30),
                map: this.map
            };
            markObject["icon"] = {
                url: this.markerIcon,
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0) // anchor
            };
            marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            google.maps.event.addListener(marker, 'click', ((marker, i) => {
                return function () {
                    let latlng = new google.maps.LatLng(listLocation[i][1], listLocation[i][2]);
                    self.geocoder.geocode({ 'latLng': latlng }, (responses) => {
                        var _a;
                        let address;
                        if (!listLocation[i][4]) {
                            if (responses && responses.length > 0) {
                                address = responses[0].formatted_address;
                            }
                            else {
                                address = "Cannot determine address at this location.";
                            }
                        }
                        else {
                            address = listLocation[i][4];
                        }
                        self.activeInfoWindow && self.activeInfoWindow.close();
                        self.activeInfoWindow = infowindow;
                        infowindow.setContent(address);
                        infowindow.open(self.map, marker);
                        //marker object [title, lat, long, index]
                        if (listLocation[i][3]) {
                            (_a = self.markerClick) === null || _a === void 0 ? void 0 : _a.emit(listLocation[i][3]);
                        }
                    });
                };
            }));
        }
        if (this.shouldClusterer) {
            this.markerCluster.addMarkers(this.markers);
        }
    }
    panTo(lat, lng, zoomNumber = 12) {
        let myLatLng = new google.maps.LatLng(lat, lng);
        this.map.setZoom(zoomNumber);
        this.map.panTo(myLatLng);
    }
    geocodePosition(pos, eventName = undefined, func = null) {
        this.geocoder.geocode({ 'latLng': pos }, (responses) => {
            if (responses && responses.length > 0) {
                this.cordinate.title = responses[0].formatted_address;
                this.cordinate.latitude = responses[0].geometry.location.lat();
                this.cordinate.longtitude = responses[0].geometry.location.lng();
            }
            else {
                this.cordinate.title = "Cannot determine address at this location.";
                this.cordinate.latitude = null;
                this.cordinate.longtitude = null;
            }
            this.triggerUpdateData(eventName);
            // $("#pac-input").click();
            if (func) {
                func();
            }
        });
    }
    addSearchBoxEvent() {
        this.searchBox.addListener('places_changed', () => {
            if (this.marker) {
                this.marker.setMap(null);
            }
            let places = this.searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            let bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                var _a;
                let markerObj = {
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: place.name,
                    animation: google.maps.Animation.DROP,
                    position: place.geometry.location
                };
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                this.marker = new google.maps.Marker(markerObj);
                this.addDragendListener();
                (_a = this.searchEvent) === null || _a === void 0 ? void 0 : _a.emit(new CordinateDto(markerObj.position.lat(), markerObj.position.lng()));
                this.cordinate.latitude = place.geometry.location.lat();
                this.cordinate.longtitude = place.geometry.location.lng();
                this.cordinate.title = place.formatted_address;
                this.triggerUpdateData(mapConfig.mapEventNames.search);
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                }
                else {
                    bounds.extend(place.geometry.location);
                }
            });
            this.map.fitBounds(bounds);
            this.map.setZoom(12);
            this.map.panTo(this.marker.getPosition());
        });
    }
    addClickListener() {
        google.maps.event.addListener(this.map, 'click', (event) => {
            //event.preventDefault();
            this.geocodePosition(event.latLng, mapConfig.mapEventNames.mapClick, () => {
                this.marker.setMap(null);
                this.marker = new google.maps.Marker({
                    position: event.latLng,
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: this.cordinate.title
                });
                this.addDragendListener();
            });
        });
    }
    addDragendListener() {
        google.maps.event.addListener(this.marker, 'dragend', () => {
            // event.preventDefault();
            this.deleteMarkers();
            this.geocodePosition(this.marker.getPosition(), mapConfig.mapEventNames.markerDrag);
            this.cordinate.latitude = this.marker.getPosition().lat();
            this.cordinate.longtitude = this.marker.getPosition().lng();
        });
    }
    triggerUpdateData(eventName) {
        var _a;
        this.cordinate.eventName = eventName;
        (_a = this.cordinateChange) === null || _a === void 0 ? void 0 : _a.emit(this.cordinate);
    }
    moveToMyLocation() {
        if (!this.requestLocation) {
            return;
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((location) => {
                var _a;
                if (this.marker) {
                    this.marker.setMap(null);
                }
                // self.deleteMarkers();
                this.cordinate.latitude = location.coords.latitude;
                this.cordinate.longtitude = location.coords.longitude;
                this.myLatLng = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
                if (this.myLocation) {
                    this.myLocation.setMap(null);
                }
                this.myLocation = new google.maps.Marker({
                    position: this.myLatLng,
                    icon: {
                        url: this.myLocationIcon,
                        scaledSize: new google.maps.Size(30, 30),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(15, 15) // anchor
                    },
                    zIndex: 1000,
                    map: this.map,
                    draggable: false,
                    title: this.cordinate.title
                });
                this.geocodePosition(this.myLocation.getPosition(), mapConfig.mapEventNames.myLocation);
                (_a = this.getCurrentLocationEvent) === null || _a === void 0 ? void 0 : _a.emit(new CordinateDto(location.coords.latitude, location.coords.longitude));
                this.map.setCenter(this.myLatLng);
                this.myLocaltionLoaded = true;
            });
        }
        else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    getTopDistanceFromMarker(lat, lng, eventName) {
        var _a;
        if (!lat || !lng) {
            return;
        }
        let myLatLng = new google.maps.LatLng(lat, lng);
        let bounds = this.map.getBounds();
        (_a = this.getTopCordinatorEvent) === null || _a === void 0 ? void 0 : _a.emit(new CordinateDto(bounds.getNorthEast().lat(), myLatLng.lng(), "", eventName));
    }
    drawCircle(centerLat, centerLng, pointLat, pointLng, fillColor = "#FF0000") {
        let myLatLng = new google.maps.LatLng(centerLat, centerLng);
        // Get map bounds
        let bounds = this.map.getBounds();
        let point = new google.maps.LatLng(pointLat, pointLng);
        let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, point);
        let radius = distanceToTop;
        this.circle.setMap(null);
        this.circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: fillColor,
            fillOpacity: 0.35,
            editable: true,
            map: this.map,
            center: myLatLng,
            radius: radius
        });
    }
}
MapComponent.ɵfac = function MapComponent_Factory(t) { return new (t || MapComponent)(); };
MapComponent.ɵcmp = i0.ɵɵdefineComponent({ type: MapComponent, selectors: [["via-map"]], viewQuery: function MapComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0, true);
        i0.ɵɵviewQuery(_c1, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.searchInput = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.googleMap = _t.first);
    } }, inputs: { cordinate: "cordinate", searchPlaceholer: "searchPlaceholer", markerDraggable: "markerDraggable", showMyLocation: "showMyLocation", hideSearchTextBox: "hideSearchTextBox", requestLocation: "requestLocation", loading: "loading", myLocationIcon: "myLocationIcon", markerIcon: "markerIcon", shouldClusterer: "shouldClusterer", clustererImagepath: "clustererImagepath" }, outputs: { cordinateChange: "cordinateChange", markerClick: "markerClick", mapRadius: "mapRadius", searchEvent: "searchEvent", getTopCordinatorEvent: "getTopCordinatorEvent", getCurrentLocationEvent: "getCurrentLocationEvent", boundsChanged: "boundsChanged", centerChanged: "centerChanged", clickOnMap: "clickOnMap", dblClick: "dblClick", drag: "drag", dragEnd: "dragEnd", dragStart: "dragStart", idle: "idle", mouseMove: "mouseMove", mouseOut: "mouseOut", mouseOver: "mouseOver", tilesLoaded: "tilesLoaded", zoomChanged: "zoomChanged" }, decls: 7, vars: 3, consts: [[1, "via-map-wrapper"], ["class", "map-search-container", 4, "ngIf"], [1, ""], [1, "google-maps"], ["googleMap", ""], ["class", "my-location-btn", 3, "click", 4, "ngIf"], ["class", "spinner-wrapper", 4, "ngIf"], [1, "map-search-container"], ["id", "via-map-search"], ["appearance", "fill"], ["matInput", "", 3, "placeholder", "ngModel", "ngModelChange"], ["searchInput", ""], ["matSuffix", "", "mat-icon-button", ""], [1, "my-location-btn", 3, "click"], ["src", "assets/icons/green_pin.svg"], [1, "spinner-wrapper"], ["strokeWidth", "1", "diameter", "20"]], template: function MapComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵtemplate(1, MapComponent_div_1_Template, 8, 2, "div", 1);
        i0.ɵɵelementStart(2, "div", 2);
        i0.ɵɵelement(3, "div", 3, 4);
        i0.ɵɵtemplate(5, MapComponent_button_5_Template, 2, 0, "button", 5);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(6, MapComponent_div_6_Template, 2, 0, "div", 6);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.hideSearchTextBox);
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("ngIf", ctx.showMyLocation && ctx.mapLoaded && ctx.myLocaltionLoaded);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.loading);
    } }, directives: [i1.NgIf, i2.MatFormField, i3.MatInput, i4.DefaultValueAccessor, i4.NgControlStatus, i4.NgModel, i5.MatButton, i2.MatSuffix, i6.MatIcon, i7.MatSpinner], styles: [".via-map-wrapper[_ngcontent-%COMP%]{display:flex;flex-direction:column;position:relative}.via-map-wrapper[_ngcontent-%COMP%]   .google-maps[_ngcontent-%COMP%]{border-radius:3px;height:300px}.via-map-wrapper[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%]{width:100%}.via-map-wrapper[_ngcontent-%COMP%]   .my-location-btn[_ngcontent-%COMP%]{background:#fff;border-radius:2px;border-style:none;bottom:110px;box-shadow:0 1px 4px -1px rgba(0,0,0,.3);cursor:pointer;height:41px;padding:0!important;position:absolute;right:10px;text-align:center;width:41px}.via-map-wrapper[_ngcontent-%COMP%]   .my-location-btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:20px}.via-map-wrapper[_ngcontent-%COMP%]   .map-search-container[_ngcontent-%COMP%]   .input-search-icon[_ngcontent-%COMP%]{position:absolute;right:21px;top:13px;width:20px}.via-map-wrapper[_ngcontent-%COMP%]   .spinner-wrapper[_ngcontent-%COMP%]{bottom:20px;position:absolute;right:60px;z-index:9999}.via-map-wrapper[_ngcontent-%COMP%]   .spinner-wrapper[_ngcontent-%COMP%]   mat-spinner[_ngcontent-%COMP%]{z-index:2}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MapComponent, [{
        type: Component,
        args: [{
                selector: 'via-map',
                templateUrl: './map.html',
                styleUrls: ['./map.scss']
            }]
    }], function () { return []; }, { searchInput: [{
            type: ViewChild,
            args: ["searchInput"]
        }], googleMap: [{
            type: ViewChild,
            args: ["googleMap"]
        }], cordinate: [{
            type: Input
        }], searchPlaceholer: [{
            type: Input
        }], markerDraggable: [{
            type: Input
        }], showMyLocation: [{
            type: Input
        }], hideSearchTextBox: [{
            type: Input
        }], requestLocation: [{
            type: Input
        }], loading: [{
            type: Input
        }], myLocationIcon: [{
            type: Input
        }], markerIcon: [{
            type: Input
        }], shouldClusterer: [{
            type: Input
        }], clustererImagepath: [{
            type: Input
        }], cordinateChange: [{
            type: Output
        }], markerClick: [{
            type: Output
        }], mapRadius: [{
            type: Output
        }], searchEvent: [{
            type: Output
        }], getTopCordinatorEvent: [{
            type: Output
        }], getCurrentLocationEvent: [{
            type: Output
        }], boundsChanged: [{
            type: Output
        }], centerChanged: [{
            type: Output
        }], clickOnMap: [{
            type: Output
        }], dblClick: [{
            type: Output
        }], drag: [{
            type: Output
        }], dragEnd: [{
            type: Output
        }], dragStart: [{
            type: Output
        }], idle: [{
            type: Output
        }], mouseMove: [{
            type: Output
        }], mouseOut: [{
            type: Output
        }], mouseOver: [{
            type: Output
        }], tilesLoaded: [{
            type: Output
        }], zoomChanged: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvY29tcG9uZW50cy92aWFfbWFwL21hcC50cyIsIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjL2NvbXBvbmVudHMvdmlhX21hcC9tYXAuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDOUYsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUN2QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDbkQsT0FBTyxlQUFlLE1BQU0sNkJBQTZCLENBQUM7Ozs7Ozs7Ozs7Ozs7SUNGdEQsOEJBQ0k7SUFBQSw4QkFDSTtJQUFBLHlDQUNJO0lBQ0EscUNBS0E7SUFESSxpTkFBNkI7SUFKakMsaUJBS0E7SUFBQSxrQ0FDRTtJQUFBLGdDQUFVO0lBQUEsc0JBQU07SUFBQSxpQkFBVztJQUM3QixpQkFBUztJQUNiLGlCQUFpQjtJQUNyQixpQkFBTTtJQUNWLGlCQUFNOzs7SUFQVSxlQUFnQztJQUFoQyxxREFBZ0MsbUNBQUE7Ozs7SUFVNUMsa0NBQ0k7SUFENkQseUxBQTRCO0lBQ3pGLDBCQUNKO0lBQUEsaUJBQVM7OztJQUdiLCtCQUNJO0lBQUEsa0NBQXlEO0lBQzdELGlCQUFNOztBRGZWLE1BQU0sT0FBTyxZQUFZO0lBbUR2QjtRQTVDUyxvQkFBZSxHQUFZLElBQUksQ0FBQztRQUNoQyxtQkFBYyxHQUFZLElBQUksQ0FBQztRQUMvQixzQkFBaUIsR0FBWSxLQUFLLENBQUM7UUFDbkMsb0JBQWUsR0FBWSxJQUFJLENBQUM7UUFDaEMsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUN6QixtQkFBYyxHQUFXLDRCQUE0QixDQUFDO1FBQ3RELGVBQVUsR0FBVywwQkFBMEIsQ0FBQztRQUNoRCxvQkFBZSxHQUFZLEtBQUssQ0FBQztRQUNqQyx1QkFBa0IsR0FBVywwQkFBMEIsQ0FBQztRQUV2RCxvQkFBZSxHQUErQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2pFLGdCQUFXLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkQsY0FBUyxHQUErQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNELGdCQUFXLEdBQStCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDN0QsMEJBQXFCLEdBQStCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkUsNEJBQXVCLEdBQStCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDekUsa0JBQWEsR0FBMkMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzRSxrQkFBYSxHQUFxQyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3JFLGVBQVUsR0FBeUMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN0RSxhQUFRLEdBQXlDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDcEUsU0FBSSxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2hELFlBQU8sR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNuRCxjQUFTLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDckQsU0FBSSxHQUF5QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2hELGNBQVMsR0FBeUMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNyRSxhQUFRLEdBQXlDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDcEUsY0FBUyxHQUF5QyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3JFLGdCQUFXLEdBQXlCLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkQsZ0JBQVcsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQVNqRSxZQUFPLEdBQUcsRUFBRSxDQUFDO1FBR2Isc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLGNBQVMsR0FBWSxLQUFLLENBQUM7SUFJM0IsQ0FBQztJQUVELFFBQVE7UUFDTixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDbEIsQ0FBQztJQUVELG9CQUFvQjtRQUNsQixPQUFPLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsZ0JBQWdCO1FBQ2QsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDM0MsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRTtZQUN4RCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDaEI7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFO2dCQUNqRCxTQUFTLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7b0JBQ3BELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO29CQUNuRCxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztvQkFDdEQsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUNmLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0JBQ2hDLENBQUMsRUFBRSxHQUFHLEVBQUU7b0JBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO29CQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7b0JBQ3ZDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDakIsQ0FBQyxDQUFDLENBQUE7YUFDSDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztnQkFDdkMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ2hCO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsT0FBTztRQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzNGLElBQUksZUFBZSxHQUFHO1lBQ3BCLElBQUksRUFBRSxFQUFFO1lBQ1IsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3JCLGlCQUFpQixFQUFFLEtBQUs7WUFDeEIsU0FBUyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU87WUFDeEMsY0FBYyxFQUFFLEtBQUs7U0FDdEIsQ0FBQztRQUVGLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxlQUFlLENBQUMsQ0FBQztRQUU5RSxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQzNCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUNsRixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztTQUMxQjtRQUVELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUVwQixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQyxVQUFVLEVBQUU7WUFDakQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUN2QyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7Z0JBQ3ZCLElBQUksRUFBRTtvQkFDSixHQUFHLEVBQUUsSUFBSSxDQUFDLGNBQWM7b0JBQ3hCLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUM7b0JBQ3hDLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ25DLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTO2lCQUM5QztnQkFDRCxNQUFNLEVBQUUsSUFBSTtnQkFDWixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7Z0JBQ2IsU0FBUyxFQUFFLEtBQUs7YUFDakIsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDbkMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO2dCQUN2QixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7Z0JBQ2IsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlO2dCQUMvQixTQUFTLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSTtnQkFDckMsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSzthQUM1QixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUN4QixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUMzQjtRQUNELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3BDLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN4QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFO2dCQUNyRCxTQUFTLEVBQUUsSUFBSSxDQUFDLGtCQUFrQjthQUNuQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFRCxZQUFZO1FBQ1YsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsZ0JBQWdCLEVBQUUsR0FBRyxFQUFFOztZQUM3RCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2xDO1lBQ0QsTUFBQSxJQUFJLENBQUMsYUFBYSwwQ0FBRSxJQUFJLENBQUMsTUFBTSxFQUFFO1FBQ25DLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsZ0JBQWdCLEVBQUUsR0FBRyxFQUFFOztZQUM3RCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ2xDLE1BQUEsSUFBSSxDQUFDLGFBQWEsMENBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRTtRQUNuQyxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxDQUFDLEtBQTZCLEVBQUUsRUFBRTs7WUFDakYsTUFBQSxJQUFJLENBQUMsVUFBVSwwQ0FBRSxJQUFJLENBQUMsS0FBSyxFQUFFO1FBQy9CLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLENBQUMsS0FBNkIsRUFBRSxFQUFFOztZQUNwRixNQUFBLElBQUksQ0FBQyxRQUFRLDBDQUFFLElBQUksQ0FBQyxLQUFLLEVBQUU7UUFDN0IsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFOztZQUNuRCxNQUFBLElBQUksQ0FBQyxJQUFJLDBDQUFFLElBQUksQ0FBQyxNQUFNLEVBQUU7UUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFOztZQUN0RCxNQUFBLElBQUksQ0FBQyxPQUFPLDBDQUFFLElBQUksQ0FBQyxTQUFTLEVBQUU7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxXQUFXLEVBQUUsR0FBRyxFQUFFOztZQUN4RCxNQUFBLElBQUksQ0FBQyxTQUFTLDBDQUFFLElBQUksQ0FBQyxXQUFXLEVBQUU7UUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFOztZQUNuRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixNQUFBLElBQUksQ0FBQyxJQUFJLDBDQUFFLElBQUksQ0FBQyxNQUFNLEVBQUU7UUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxXQUFXLEVBQUUsQ0FBQyxLQUE2QixFQUFFLEVBQUU7O1lBQ3JGLE1BQUEsSUFBSSxDQUFDLFNBQVMsMENBQUUsSUFBSSxDQUFDLEtBQUssRUFBRTtRQUM5QixDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxDQUFDLEtBQTZCLEVBQUUsRUFBRTs7WUFDcEYsTUFBQSxJQUFJLENBQUMsUUFBUSwwQ0FBRSxJQUFJLENBQUMsS0FBSyxFQUFFO1FBQzdCLENBQUMsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsV0FBVyxFQUFFLENBQUMsS0FBNkIsRUFBRSxFQUFFOztZQUNyRixNQUFBLElBQUksQ0FBQyxTQUFTLDBDQUFFLElBQUksQ0FBQyxLQUFLLEVBQUU7UUFDOUIsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxhQUFhLEVBQUUsR0FBRyxFQUFFOztZQUMxRCxNQUFBLElBQUksQ0FBQyxXQUFXLDBDQUFFLElBQUksQ0FBQyxhQUFhLEVBQUU7UUFDeEMsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxjQUFjLEVBQUUsR0FBRyxFQUFFOztZQUMzRCxNQUFBLElBQUksQ0FBQyxXQUFXLDBDQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxFQUFFO1FBQzdDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDRCQUE0QixDQUFDLEdBQVcsRUFBRSxHQUFXLEVBQUUsU0FBaUI7O1FBQ3RFLElBQUksU0FBdUIsRUFBRSxZQUFZLEdBQUcsRUFBRSxFQUFFLE9BQWUsRUFBRSxPQUFlLEVBQUUsT0FBZSxFQUFFLE9BQWUsRUFBRSxXQUEwQyxDQUFDO1FBRS9KLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDN0MsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUM3QyxZQUFZLEdBQUcsRUFBRSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQztRQUV0RCxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDOUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlJLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5SSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFOUksWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHO1lBQ3RCLEtBQUssRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFO1lBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7U0FDaEIsQ0FBQTtRQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRztZQUN0QixLQUFLLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRTtZQUNmLEtBQUssRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFO1NBQ2hCLENBQUE7UUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUc7WUFDdEIsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7WUFDZixLQUFLLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRTtTQUNoQixDQUFBO1FBQ0QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHO1lBQ3RCLEtBQUssRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFO1lBQ2YsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEVBQUU7U0FDaEIsQ0FBQTtRQUVELFdBQVcsR0FBRyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQ3pFLFNBQVMsR0FBRyxJQUFJLFlBQVksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzlFLE1BQUEsSUFBSSxDQUFDLFNBQVMsMENBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRTtJQUNsQyxDQUFDO0lBRUQsaUJBQWlCLENBQUMsSUFBWSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsSUFBWTtRQUN0RSxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ3ZJLENBQUM7SUFFRCxtRUFBbUU7SUFDbkUsYUFBYTtRQUNYLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5QjtRQUNELElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN4QixJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELHNCQUFzQixDQUFDLGVBQTJCLEVBQUU7UUFDbEQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksVUFBVSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDNUMsSUFBSSxNQUEwQixFQUFFLENBQVMsQ0FBQztRQUMxQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFckIsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3hDLGVBQWU7WUFDZixJQUFJLFVBQVUsR0FBRztnQkFDZixRQUFRLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4RSxVQUFVLEVBQUUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDO2dCQUN4QyxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7YUFDZCxDQUFDO1lBRUYsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHO2dCQUNuQixHQUFHLEVBQUUsSUFBSSxDQUFDLFVBQVU7Z0JBQ3BCLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUM7Z0JBQ3hDLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ25DLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTO2FBQzlDLENBQUM7WUFDRixNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMxQixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM1RCxPQUFPO29CQUNMLElBQUksTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM1RSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLFNBQXlCLEVBQUUsRUFBRTs7d0JBQ3hFLElBQUksT0FBc0IsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTs0QkFDdkIsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0NBQ3JDLE9BQU8sR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUM7NkJBQzFDO2lDQUFNO2dDQUNMLE9BQU8sR0FBRyw0Q0FBNEMsQ0FBQzs2QkFDeEQ7eUJBQ0Y7NkJBQU07NEJBQ0wsT0FBTyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt5QkFDOUI7d0JBRUQsSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDdkQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQzt3QkFDbkMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDL0IsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDO3dCQUVsQyx5Q0FBeUM7d0JBQ3pDLElBQUksWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFOzRCQUN0QixNQUFBLElBQUksQ0FBQyxXQUFXLDBDQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7eUJBQzVDO29CQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQTtZQUNILENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDTDtRQUNELElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN4QixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDN0M7SUFDSCxDQUFDO0lBRUQsS0FBSyxDQUFDLEdBQVcsRUFBRSxHQUFXLEVBQUUsYUFBcUIsRUFBRTtRQUNyRCxJQUFJLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsZUFBZSxDQUFDLEdBQVEsRUFBRSxZQUFvQixTQUFTLEVBQUUsT0FBaUIsSUFBSTtRQUM1RSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLFNBQXlCLEVBQUUsRUFBRTtZQUNyRSxJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO2dCQUN0RCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDL0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDbEU7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsNENBQTRDLENBQUM7Z0JBQ3BFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQ2xDO1lBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2xDLDJCQUEyQjtZQUMzQixJQUFJLElBQUksRUFBRTtnQkFDUixJQUFJLEVBQUUsQ0FBQzthQUNSO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUJBQWlCO1FBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEVBQUUsR0FBRyxFQUFFO1lBQ2hELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDZixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMxQjtZQUVELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDeEMsSUFBSSxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDdEIsT0FBTzthQUNSO1lBRUQsSUFBSSxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQzVDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTs7Z0JBQ3ZCLElBQUksU0FBUyxHQUFHO29CQUNkLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztvQkFDYixTQUFTLEVBQUUsSUFBSSxDQUFDLGVBQWU7b0JBQy9CLEtBQUssRUFBRSxLQUFLLENBQUMsSUFBSTtvQkFDakIsU0FBUyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUk7b0JBQ3JDLFFBQVEsRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVE7aUJBQ2xDLENBQUM7Z0JBRUYsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7b0JBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMscUNBQXFDLENBQUMsQ0FBQztvQkFDbkQsT0FBTztpQkFDUjtnQkFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBRWhELElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUUxQixNQUFBLElBQUksQ0FBQyxXQUFXLDBDQUFFLElBQUksQ0FBQyxJQUFJLFlBQVksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRTtnQkFFN0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUMxRCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsaUJBQWlCLENBQUM7Z0JBRS9DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUV2RCxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFO29CQUMzQixNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3ZDO3FCQUFNO29CQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDeEM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUM1QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxnQkFBZ0I7UUFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUN6RCx5QkFBeUI7WUFDekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRTtnQkFDeEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDbkMsUUFBUSxFQUFFLEtBQUssQ0FBQyxNQUFNO29CQUN0QixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7b0JBQ2IsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlO29CQUUvQixLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLO2lCQUM1QixDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxrQkFBa0I7UUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRTtZQUN6RCwwQkFBMEI7WUFDMUIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsRUFBRSxTQUFTLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BGLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDMUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxTQUFpQjs7UUFDakMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3JDLE1BQUEsSUFBSSxDQUFDLGVBQWUsMENBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7SUFDN0MsQ0FBQztJQUVELGdCQUFnQjtRQUNkLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3pCLE9BQU87U0FDUjtRQUNELElBQUksU0FBUyxDQUFDLFdBQVcsRUFBRTtZQUN6QixTQUFTLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7O2dCQUNwRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7b0JBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzFCO2dCQUNELHdCQUF3QjtnQkFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO2dCQUV0RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFFNUYsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDOUI7Z0JBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO29CQUN2QyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7b0JBQ3ZCLElBQUksRUFBRTt3QkFDSixHQUFHLEVBQUUsSUFBSSxDQUFDLGNBQWM7d0JBQ3hCLFVBQVUsRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUM7d0JBQ3hDLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7d0JBQ25DLE1BQU0sRUFBRSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTO3FCQUNoRDtvQkFDRCxNQUFNLEVBQUUsSUFBSTtvQkFDWixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7b0JBQ2IsU0FBUyxFQUFFLEtBQUs7b0JBQ2hCLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUs7aUJBQzVCLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLEVBQUUsU0FBUyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDeEYsTUFBQSxJQUFJLENBQUMsdUJBQXVCLDBDQUFFLElBQUksQ0FBQyxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFDO2dCQUN6RyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO1NBQzlEO0lBQ0gsQ0FBQztJQUVELHdCQUF3QixDQUFDLEdBQVcsRUFBRSxHQUFXLEVBQUUsU0FBa0I7O1FBQ25FLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDaEIsT0FBTztTQUNSO1FBQ0QsSUFBSSxRQUFRLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDaEQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNsQyxNQUFBLElBQUksQ0FBQyxxQkFBcUIsMENBQUUsSUFBSSxDQUFDLElBQUksWUFBWSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxRQUFRLENBQUMsR0FBRyxFQUFFLEVBQUUsRUFBRSxFQUFFLFNBQVMsQ0FBQyxFQUFFO0lBRWpILENBQUM7SUFFRCxVQUFVLENBQUMsU0FBaUIsRUFBRSxTQUFpQixFQUFFLFFBQWdCLEVBQUUsUUFBZ0IsRUFBRSxZQUFvQixTQUFTO1FBQ2hILElBQUksUUFBUSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzVELGlCQUFpQjtRQUNqQixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2xDLElBQUksS0FBSyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELElBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDM0YsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDO1FBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNuQyxXQUFXLEVBQUUsU0FBUztZQUN0QixhQUFhLEVBQUUsR0FBRztZQUNsQixZQUFZLEVBQUUsQ0FBQztZQUNmLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHO1lBQ2IsTUFBTSxFQUFFLFFBQVE7WUFDaEIsTUFBTSxFQUFFLE1BQU07U0FDZixDQUFDLENBQUM7SUFDTCxDQUFDOzt3RUFuZVUsWUFBWTtpREFBWixZQUFZOzs7Ozs7OztRQ1Z6Qiw4QkFDSTtRQUFBLDZEQUNJO1FBY0osOEJBQ0k7UUFBQSw0QkFBMEM7UUFDMUMsbUVBQ0k7UUFFUixpQkFBTTtRQUVOLDZEQUNJO1FBRVIsaUJBQU07O1FBekJHLGVBQTBCO1FBQTFCLDZDQUEwQjtRQWlCbkIsZUFBd0Q7UUFBeEQsbUZBQXdEO1FBS3ZDLGVBQWU7UUFBZixrQ0FBZTs7a0REYm5DLFlBQVk7Y0FMeEIsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxTQUFTO2dCQUNuQixXQUFXLEVBQUUsWUFBWTtnQkFDekIsU0FBUyxFQUFFLENBQUMsWUFBWSxDQUFDO2FBQzFCO3NDQUcyQixXQUFXO2tCQUFwQyxTQUFTO21CQUFDLGFBQWE7WUFDQSxTQUFTO2tCQUFoQyxTQUFTO21CQUFDLFdBQVc7WUFFYixTQUFTO2tCQUFqQixLQUFLO1lBQ0csZ0JBQWdCO2tCQUF4QixLQUFLO1lBQ0csZUFBZTtrQkFBdkIsS0FBSztZQUNHLGNBQWM7a0JBQXRCLEtBQUs7WUFDRyxpQkFBaUI7a0JBQXpCLEtBQUs7WUFDRyxlQUFlO2tCQUF2QixLQUFLO1lBQ0csT0FBTztrQkFBZixLQUFLO1lBQ0csY0FBYztrQkFBdEIsS0FBSztZQUNHLFVBQVU7a0JBQWxCLEtBQUs7WUFDRyxlQUFlO2tCQUF2QixLQUFLO1lBQ0csa0JBQWtCO2tCQUExQixLQUFLO1lBRUksZUFBZTtrQkFBeEIsTUFBTTtZQUNHLFdBQVc7a0JBQXBCLE1BQU07WUFDRyxTQUFTO2tCQUFsQixNQUFNO1lBQ0csV0FBVztrQkFBcEIsTUFBTTtZQUNHLHFCQUFxQjtrQkFBOUIsTUFBTTtZQUNHLHVCQUF1QjtrQkFBaEMsTUFBTTtZQUNHLGFBQWE7a0JBQXRCLE1BQU07WUFDRyxhQUFhO2tCQUF0QixNQUFNO1lBQ0csVUFBVTtrQkFBbkIsTUFBTTtZQUNHLFFBQVE7a0JBQWpCLE1BQU07WUFDRyxJQUFJO2tCQUFiLE1BQU07WUFDRyxPQUFPO2tCQUFoQixNQUFNO1lBQ0csU0FBUztrQkFBbEIsTUFBTTtZQUNHLElBQUk7a0JBQWIsTUFBTTtZQUNHLFNBQVM7a0JBQWxCLE1BQU07WUFDRyxRQUFRO2tCQUFqQixNQUFNO1lBQ0csU0FBUztrQkFBbEIsTUFBTTtZQUNHLFdBQVc7a0JBQXBCLE1BQU07WUFDRyxXQUFXO2tCQUFwQixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgbWFwQ29uZmlnIH0gZnJvbSAnLi9jb25zdGFudCc7XG5pbXBvcnQgeyBDb3JkaW5hdGVEdG8gfSBmcm9tIFwiLi9kdG8vY29yZGluYXRlLmR0b1wiO1xuaW1wb3J0IE1hcmtlckNsdXN0ZXJlciBmcm9tICdAZ29vZ2xlL21hcmtlcmNsdXN0ZXJlcnBsdXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICd2aWEtbWFwJyxcbiAgdGVtcGxhdGVVcmw6ICcuL21hcC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbWFwLnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBNYXBDb21wb25lbnQge1xuXG4gIEBWaWV3Q2hpbGQoXCJzZWFyY2hJbnB1dFwiKSBzZWFyY2hJbnB1dDogRWxlbWVudFJlZjxIVE1MSW5wdXRFbGVtZW50PjtcbiAgQFZpZXdDaGlsZChcImdvb2dsZU1hcFwiKSBnb29nbGVNYXA6IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+O1xuXG4gIEBJbnB1dCgpIGNvcmRpbmF0ZTogQ29yZGluYXRlRHRvO1xuICBASW5wdXQoKSBzZWFyY2hQbGFjZWhvbGVyOiBzdHJpbmc7XG4gIEBJbnB1dCgpIG1hcmtlckRyYWdnYWJsZTogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpIHNob3dNeUxvY2F0aW9uOiBib29sZWFuID0gdHJ1ZTtcbiAgQElucHV0KCkgaGlkZVNlYXJjaFRleHRCb3g6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQElucHV0KCkgcmVxdWVzdExvY2F0aW9uOiBib29sZWFuID0gdHJ1ZTtcbiAgQElucHV0KCkgbG9hZGluZzogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSBteUxvY2F0aW9uSWNvbjogc3RyaW5nID0gJ2Fzc2V0cy9pY29ucy9ncmVlbl9waW4uc3ZnJztcbiAgQElucHV0KCkgbWFya2VySWNvbjogc3RyaW5nID0gJ2Fzc2V0cy9pY29ucy9yZWRfcGluLnN2Zyc7XG4gIEBJbnB1dCgpIHNob3VsZENsdXN0ZXJlcjogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSBjbHVzdGVyZXJJbWFnZXBhdGg6IHN0cmluZyA9IFwiYXNzZXRzL21hcmtlcmNsdXN0ZXJlci9tXCI7XG5cbiAgQE91dHB1dCgpIGNvcmRpbmF0ZUNoYW5nZTogRXZlbnRFbWl0dGVyPENvcmRpbmF0ZUR0bz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBtYXJrZXJDbGljazogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBtYXBSYWRpdXM6IEV2ZW50RW1pdHRlcjxDb3JkaW5hdGVEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgc2VhcmNoRXZlbnQ6IEV2ZW50RW1pdHRlcjxDb3JkaW5hdGVEdG8+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgZ2V0VG9wQ29yZGluYXRvckV2ZW50OiBFdmVudEVtaXR0ZXI8Q29yZGluYXRlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGdldEN1cnJlbnRMb2NhdGlvbkV2ZW50OiBFdmVudEVtaXR0ZXI8Q29yZGluYXRlRHRvPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGJvdW5kc0NoYW5nZWQ6IEV2ZW50RW1pdHRlcjxnb29nbGUubWFwcy5MYXRMbmdCb3VuZHM+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgY2VudGVyQ2hhbmdlZDogRXZlbnRFbWl0dGVyPGdvb2dsZS5tYXBzLkxhdExuZz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBjbGlja09uTWFwOiBFdmVudEVtaXR0ZXI8Z29vZ2xlLm1hcHMuTW91c2VFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBkYmxDbGljazogRXZlbnRFbWl0dGVyPGdvb2dsZS5tYXBzLk1vdXNlRXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgZHJhZzogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBkcmFnRW5kOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGRyYWdTdGFydDogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIEBPdXRwdXQoKSBpZGxlOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG1vdXNlTW92ZTogRXZlbnRFbWl0dGVyPGdvb2dsZS5tYXBzLk1vdXNlRXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgbW91c2VPdXQ6IEV2ZW50RW1pdHRlcjxnb29nbGUubWFwcy5Nb3VzZUV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIG1vdXNlT3ZlcjogRXZlbnRFbWl0dGVyPGdvb2dsZS5tYXBzLk1vdXNlRXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgdGlsZXNMb2FkZWQ6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgem9vbUNoYW5nZWQ6IEV2ZW50RW1pdHRlcjxudW1iZXI+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIGxhdGxuZzogZ29vZ2xlLm1hcHMuTGF0TG5nO1xuICBnZW9jb2RlcjogYW55O1xuICBteUxhdExuZzogZ29vZ2xlLm1hcHMuTGF0TG5nO1xuICBtYXA6IGdvb2dsZS5tYXBzLk1hcDtcbiAgbXlMb2NhdGlvbjogZ29vZ2xlLm1hcHMuTWFya2VyO1xuICBtYXJrZXI6IGdvb2dsZS5tYXBzLk1hcmtlcjtcbiAgc2VhcmNoQm94OiBnb29nbGUubWFwcy5wbGFjZXMuU2VhcmNoQm94O1xuICBtYXJrZXJzID0gW107XG4gIGFjdGl2ZUluZm9XaW5kb3c6IGdvb2dsZS5tYXBzLkluZm9XaW5kb3c7XG4gIGNpcmNsZTogYW55O1xuICBteUxvY2FsdGlvbkxvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuICBtYXBMb2FkZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgbWFya2VyQ2x1c3RlcjogTWFya2VyQ2x1c3RlcmVyO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgaW5zdGFuY2UoKTogZ29vZ2xlLm1hcHMuTWFwIHtcbiAgICByZXR1cm4gdGhpcy5tYXA7XG4gIH1cblxuICBnZXRDZW50ZXJNYXBMb2NhdGlvbigpIHtcbiAgICByZXR1cm4gdGhpcy5pbnN0YW5jZSgpLmdldENlbnRlcigpO1xuICB9XG5cbiAgZ2V0TWFya2VyQ2x1c3RlcigpIHtcbiAgICByZXR1cm4gdGhpcy5tYXJrZXJDbHVzdGVyO1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIHRoaXMuZ2VvY29kZXIgPSBuZXcgZ29vZ2xlLm1hcHMuR2VvY29kZXIoKTtcbiAgICBpZiAodGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgJiYgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSkge1xuICAgICAgdGhpcy5pbml0TWFwKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICh0aGlzLnJlcXVlc3RMb2NhdGlvbiAmJiBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24pIHtcbiAgICAgICAgbmF2aWdhdG9yLmdlb2xvY2F0aW9uLmdldEN1cnJlbnRQb3NpdGlvbigobG9jYXRpb24pID0+IHtcbiAgICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSA9IGxvY2F0aW9uLmNvb3Jkcy5sYXRpdHVkZTtcbiAgICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlID0gbG9jYXRpb24uY29vcmRzLmxvbmdpdHVkZTtcbiAgICAgICAgICB0aGlzLmluaXRNYXAoKTtcbiAgICAgICAgICB0aGlzLm15TG9jYWx0aW9uTG9hZGVkID0gdHJ1ZTtcbiAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgIHRoaXMuY29yZGluYXRlLmxhdGl0dWRlID0gMTAuNzYyNjIyO1xuICAgICAgICAgIHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSAxMDYuNjYwMTcyO1xuICAgICAgICAgIHRoaXMuaW5pdE1hcCgpO1xuICAgICAgICB9KVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSAxMC43NjI2MjI7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSAxMDYuNjYwMTcyO1xuICAgICAgICB0aGlzLmluaXRNYXAoKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBpbml0TWFwKCkge1xuICAgIHRoaXMubXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHRoaXMuY29yZGluYXRlLmxhdGl0dWRlLCB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlKTtcbiAgICBsZXQgbWFwQ29uZmlnT2JqZWN0ID0ge1xuICAgICAgem9vbTogMTUsXG4gICAgICBjZW50ZXI6IHRoaXMubXlMYXRMbmcsXG4gICAgICBzdHJlZXRWaWV3Q29udHJvbDogZmFsc2UsXG4gICAgICBtYXBUeXBlSWQ6IGdvb2dsZS5tYXBzLk1hcFR5cGVJZC5ST0FETUFQLFxuICAgICAgbWFwVHlwZUNvbnRyb2w6IGZhbHNlXG4gICAgfTtcblxuICAgIHRoaXMubWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcCh0aGlzLmdvb2dsZU1hcC5uYXRpdmVFbGVtZW50LCBtYXBDb25maWdPYmplY3QpO1xuXG4gICAgaWYgKCF0aGlzLmhpZGVTZWFyY2hUZXh0Qm94KSB7XG4gICAgICB0aGlzLnNlYXJjaEJveCA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuU2VhcmNoQm94KHRoaXMuc2VhcmNoSW5wdXQubmF0aXZlRWxlbWVudCk7XG4gICAgICB0aGlzLmFkZFNlYXJjaEJveEV2ZW50KCk7XG4gICAgfVxuXG4gICAgdGhpcy5iaW5kaW5nRXZlbnQoKTtcblxuICAgIGlmICh0aGlzLmNvcmRpbmF0ZS50aXRsZSA9PT0gbWFwQ29uZmlnLm15TG9jYXRpb24pIHtcbiAgICAgIHRoaXMubXlMb2NhdGlvbiA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICBwb3NpdGlvbjogdGhpcy5teUxhdExuZyxcbiAgICAgICAgaWNvbjoge1xuICAgICAgICAgIHVybDogdGhpcy5teUxvY2F0aW9uSWNvbiwgLy8gdXJsXG4gICAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSwgLy8gc2NhbGVkIHNpemVcbiAgICAgICAgICBvcmlnaW46IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSwgLy8gb3JpZ2luXG4gICAgICAgICAgYW5jaG9yOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCkgLy8gYW5jaG9yXG4gICAgICAgIH0sXG4gICAgICAgIHpJbmRleDogMTAwMCxcbiAgICAgICAgbWFwOiB0aGlzLm1hcCxcbiAgICAgICAgZHJhZ2dhYmxlOiBmYWxzZVxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMubWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgIHBvc2l0aW9uOiB0aGlzLm15TGF0TG5nLFxuICAgICAgICBtYXA6IHRoaXMubWFwLFxuICAgICAgICBkcmFnZ2FibGU6IHRoaXMubWFya2VyRHJhZ2dhYmxlLFxuICAgICAgICBhbmltYXRpb246IGdvb2dsZS5tYXBzLkFuaW1hdGlvbi5EUk9QLFxuICAgICAgICB0aXRsZTogdGhpcy5jb3JkaW5hdGUudGl0bGVcbiAgICAgIH0pO1xuICAgICAgdGhpcy5hZGRDbGlja0xpc3RlbmVyKCk7XG4gICAgICB0aGlzLmFkZERyYWdlbmRMaXN0ZW5lcigpO1xuICAgIH1cbiAgICB0aGlzLmdlb2NvZGVQb3NpdGlvbih0aGlzLm15TGF0TG5nKTtcbiAgICBpZiAodGhpcy5zaG91bGRDbHVzdGVyZXIpIHtcbiAgICAgIHRoaXMubWFya2VyQ2x1c3RlciA9IG5ldyBNYXJrZXJDbHVzdGVyZXIodGhpcy5tYXAsIFtdLCB7XG4gICAgICAgIGltYWdlUGF0aDogdGhpcy5jbHVzdGVyZXJJbWFnZXBhdGgsXG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBiaW5kaW5nRXZlbnQoKSB7XG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAsICdib3VuZHNfY2hhbmdlZCcsICgpID0+IHtcbiAgICAgIGxldCBib3VuZHMgPSB0aGlzLm1hcC5nZXRCb3VuZHMoKTtcbiAgICAgIGlmICghdGhpcy5oaWRlU2VhcmNoVGV4dEJveCkge1xuICAgICAgICB0aGlzLnNlYXJjaEJveC5zZXRCb3VuZHMoYm91bmRzKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuYm91bmRzQ2hhbmdlZD8uZW1pdChib3VuZHMpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAsICdjZW50ZXJfY2hhbmdlZCcsICgpID0+IHtcbiAgICAgIGxldCBjZW50ZXIgPSB0aGlzLm1hcC5nZXRDZW50ZXIoKTtcbiAgICAgIHRoaXMuY2VudGVyQ2hhbmdlZD8uZW1pdChjZW50ZXIpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAsICdjbGljaycsIChldmVudDogZ29vZ2xlLm1hcHMuTW91c2VFdmVudCkgPT4ge1xuICAgICAgdGhpcy5jbGlja09uTWFwPy5lbWl0KGV2ZW50KTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwLCAnZGJsY2xpY2snLCAoZXZlbnQ6IGdvb2dsZS5tYXBzLk1vdXNlRXZlbnQpID0+IHtcbiAgICAgIHRoaXMuZGJsQ2xpY2s/LmVtaXQoZXZlbnQpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAsICdkcmFnJywgKCkgPT4ge1xuICAgICAgdGhpcy5kcmFnPy5lbWl0KFwiZHJhZ1wiKTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwLCAnZHJhZ2VuZCcsICgpID0+IHtcbiAgICAgIHRoaXMuZHJhZ0VuZD8uZW1pdChcImRyYWdlbmRcIik7XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCwgJ2RyYWdzdGFydCcsICgpID0+IHtcbiAgICAgIHRoaXMuZHJhZ1N0YXJ0Py5lbWl0KFwiZHJhZ3N0YXJ0XCIpO1xuICAgIH0pO1xuXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAsICdpZGxlJywgKCkgPT4ge1xuICAgICAgdGhpcy5tYXBMb2FkZWQgPSB0cnVlO1xuICAgICAgdGhpcy5pZGxlPy5lbWl0KFwiaWRsZVwiKTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwLCAnbW91c2Vtb3ZlJywgKGV2ZW50OiBnb29nbGUubWFwcy5Nb3VzZUV2ZW50KSA9PiB7XG4gICAgICB0aGlzLm1vdXNlTW92ZT8uZW1pdChldmVudCk7XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCwgJ21vdXNlb3V0JywgKGV2ZW50OiBnb29nbGUubWFwcy5Nb3VzZUV2ZW50KSA9PiB7XG4gICAgICB0aGlzLm1vdXNlT3V0Py5lbWl0KGV2ZW50KTtcbiAgICB9KTtcblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKHRoaXMubWFwLCAnbW91c2VvdmVyJywgKGV2ZW50OiBnb29nbGUubWFwcy5Nb3VzZUV2ZW50KSA9PiB7XG4gICAgICB0aGlzLm1vdXNlT3Zlcj8uZW1pdChldmVudCk7XG4gICAgfSk7XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcCwgXCJ0aWxlc2xvYWRlZFwiLCAoKSA9PiB7XG4gICAgICB0aGlzLnRpbGVzTG9hZGVkPy5lbWl0KFwidGlsZXNsb2FkZWRcIik7XG4gICAgfSk7XG4gICAgXG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAsICd6b29tX2NoYW5nZWQnLCAoKSA9PiB7XG4gICAgICB0aGlzLnpvb21DaGFuZ2VkPy5lbWl0KHRoaXMubWFwLmdldFpvb20oKSk7XG4gICAgfSk7XG4gIH1cblxuICBnZXRGdXJ0aGVzdFBvaW50RnJvbUxvY2F0aW9uKGxhdDogbnVtYmVyLCBsbmc6IG51bWJlciwgZXZlbnROYW1lOiBzdHJpbmcpIHtcbiAgICBsZXQgY29yZGluYXRlOiBDb3JkaW5hdGVEdG8sIGNvcm5lcnNBcnJheSA9IHt9LCBjb3JuZXIxOiBudW1iZXIsIGNvcm5lcjI6IG51bWJlciwgY29ybmVyMzogbnVtYmVyLCBjb3JuZXI0OiBudW1iZXIsIGZhcmVzdFBvaW50OiB7IGxhdDogbnVtYmVyOyBsbmc6IG51bWJlcjsgfTtcblxuICAgIGxldCBzVyA9IHRoaXMubWFwLmdldEJvdW5kcygpLmdldFNvdXRoV2VzdCgpO1xuICAgIGxldCBuRSA9IHRoaXMubWFwLmdldEJvdW5kcygpLmdldE5vcnRoRWFzdCgpO1xuICAgIGNvcm5lcnNBcnJheSA9IHt9LCBjb3JuZXIxLCBjb3JuZXIyLCBjb3JuZXIzLCBjb3JuZXI0O1xuXG4gICAgY29ybmVyMSA9IGdvb2dsZS5tYXBzLmdlb21ldHJ5LnNwaGVyaWNhbC5jb21wdXRlRGlzdGFuY2VCZXR3ZWVuKG5ldyBnb29nbGUubWFwcy5MYXRMbmcobGF0LCBsbmcpLCBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHNXLmxhdCgpLCBzVy5sbmcoKSkpO1xuICAgIGNvcm5lcjIgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxhdCwgbG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhuRS5sYXQoKSwgbkUubG5nKCkpKTtcbiAgICBjb3JuZXIzID0gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXQsIGxuZyksIG5ldyBnb29nbGUubWFwcy5MYXRMbmcoc1cubGF0KCksIG5FLmxuZygpKSk7XG4gICAgY29ybmVyNCA9IGdvb2dsZS5tYXBzLmdlb21ldHJ5LnNwaGVyaWNhbC5jb21wdXRlRGlzdGFuY2VCZXR3ZWVuKG5ldyBnb29nbGUubWFwcy5MYXRMbmcobGF0LCBsbmcpLCBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKG5FLmxhdCgpLCBzVy5sbmcoKSkpO1xuXG4gICAgY29ybmVyc0FycmF5W2Nvcm5lcjFdID0ge1xuICAgICAgXCJsYXRcIjogc1cubGF0KCksXG4gICAgICBcImxuZ1wiOiBzVy5sbmcoKVxuICAgIH1cbiAgICBjb3JuZXJzQXJyYXlbY29ybmVyMl0gPSB7XG4gICAgICBcImxhdFwiOiBuRS5sYXQoKSxcbiAgICAgIFwibG5nXCI6IG5FLmxuZygpXG4gICAgfVxuICAgIGNvcm5lcnNBcnJheVtjb3JuZXIzXSA9IHtcbiAgICAgIFwibGF0XCI6IHNXLmxhdCgpLFxuICAgICAgXCJsbmdcIjogbkUubG5nKClcbiAgICB9XG4gICAgY29ybmVyc0FycmF5W2Nvcm5lcjRdID0ge1xuICAgICAgXCJsYXRcIjogbkUubGF0KCksXG4gICAgICBcImxuZ1wiOiBzVy5sbmcoKVxuICAgIH1cblxuICAgIGZhcmVzdFBvaW50ID0gY29ybmVyc0FycmF5W01hdGgubWF4KGNvcm5lcjEsIGNvcm5lcjIsIGNvcm5lcjMsIGNvcm5lcjQpXTtcbiAgICBjb3JkaW5hdGUgPSBuZXcgQ29yZGluYXRlRHRvKGZhcmVzdFBvaW50LmxhdCwgZmFyZXN0UG9pbnQubG5nLCBcIlwiLCBldmVudE5hbWUpO1xuICAgIHRoaXMubWFwUmFkaXVzPy5lbWl0KGNvcmRpbmF0ZSk7XG4gIH1cblxuICBjYWxjdWxhdGVEaXN0YW5jZShhTGF0OiBudW1iZXIsIGFMbmc6IG51bWJlciwgYkxhdDogbnVtYmVyLCBiTG5nOiBudW1iZXIpIHtcbiAgICByZXR1cm4gZ29vZ2xlLm1hcHMuZ2VvbWV0cnkuc3BoZXJpY2FsLmNvbXB1dGVEaXN0YW5jZUJldHdlZW4obmV3IGdvb2dsZS5tYXBzLkxhdExuZyhhTGF0LCBhTG5nKSwgbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhiTGF0LCBiTG5nKSk7XG4gIH1cblxuICAvLyBEZWxldGVzIGFsbCBtYXJrZXJzIGluIHRoZSBhcnJheSBieSByZW1vdmluZyByZWZlcmVuY2VzIHRvIHRoZW0uXG4gIGRlbGV0ZU1hcmtlcnMoKSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm1hcmtlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMubWFya2Vyc1tpXS5zZXRNYXAobnVsbCk7XG4gICAgfVxuICAgIGlmICh0aGlzLnNob3VsZENsdXN0ZXJlcikge1xuICAgICAgdGhpcy5tYXJrZXJDbHVzdGVyLmNsZWFyTWFya2VycygpO1xuICAgIH1cbiAgICB0aGlzLm1hcmtlcnMgPSBbXTtcbiAgfVxuXG4gIHNob3dNdWx0aXRwbGVMb2NhdGlvbnMobGlzdExvY2F0aW9uOiBBcnJheTxhbnk+ID0gW10pIHtcbiAgICBsZXQgc2VsZiA9IHRoaXM7XG4gICAgbGV0IGluZm93aW5kb3cgPSBuZXcgZ29vZ2xlLm1hcHMuSW5mb1dpbmRvdztcbiAgICBsZXQgbWFya2VyOiBnb29nbGUubWFwcy5NYXJrZXIsIGk6IG51bWJlcjtcbiAgICB0aGlzLmRlbGV0ZU1hcmtlcnMoKTtcblxuICAgIGZvciAoaSA9IDA7IGkgPCBsaXN0TG9jYXRpb24ubGVuZ3RoOyBpKyspIHtcbiAgICAgIC8vbWFya2VyIENPTkZJR1xuICAgICAgbGV0IG1hcmtPYmplY3QgPSB7XG4gICAgICAgIHBvc2l0aW9uOiBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxpc3RMb2NhdGlvbltpXVsxXSwgbGlzdExvY2F0aW9uW2ldWzJdKSxcbiAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSxcbiAgICAgICAgbWFwOiB0aGlzLm1hcFxuICAgICAgfTtcblxuICAgICAgbWFya09iamVjdFtcImljb25cIl0gPSB7XG4gICAgICAgIHVybDogdGhpcy5tYXJrZXJJY29uLCAvLyB1cmxcbiAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzAsIDMwKSwgLy8gc2NhbGVkIHNpemVcbiAgICAgICAgb3JpZ2luOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoMCwgMCksIC8vIG9yaWdpblxuICAgICAgICBhbmNob3I6IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSAvLyBhbmNob3JcbiAgICAgIH07XG4gICAgICBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKG1hcmtPYmplY3QpO1xuICAgICAgdGhpcy5tYXJrZXJzLnB1c2gobWFya2VyKTtcbiAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKG1hcmtlciwgJ2NsaWNrJywgKChtYXJrZXIsIGkpID0+IHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBsZXQgbGF0bG5nID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsaXN0TG9jYXRpb25baV1bMV0sIGxpc3RMb2NhdGlvbltpXVsyXSk7XG4gICAgICAgICAgc2VsZi5nZW9jb2Rlci5nZW9jb2RlKHsgJ2xhdExuZyc6IGxhdGxuZyB9LCAocmVzcG9uc2VzOiBzdHJpbmcgfCBhbnlbXSkgPT4ge1xuICAgICAgICAgICAgbGV0IGFkZHJlc3M6IHN0cmluZyB8IE5vZGU7XG4gICAgICAgICAgICBpZiAoIWxpc3RMb2NhdGlvbltpXVs0XSkge1xuICAgICAgICAgICAgICBpZiAocmVzcG9uc2VzICYmIHJlc3BvbnNlcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgYWRkcmVzcyA9IHJlc3BvbnNlc1swXS5mb3JtYXR0ZWRfYWRkcmVzcztcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBhZGRyZXNzID0gXCJDYW5ub3QgZGV0ZXJtaW5lIGFkZHJlc3MgYXQgdGhpcyBsb2NhdGlvbi5cIjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgYWRkcmVzcyA9IGxpc3RMb2NhdGlvbltpXVs0XTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgc2VsZi5hY3RpdmVJbmZvV2luZG93ICYmIHNlbGYuYWN0aXZlSW5mb1dpbmRvdy5jbG9zZSgpO1xuICAgICAgICAgICAgc2VsZi5hY3RpdmVJbmZvV2luZG93ID0gaW5mb3dpbmRvdztcbiAgICAgICAgICAgIGluZm93aW5kb3cuc2V0Q29udGVudChhZGRyZXNzKTtcbiAgICAgICAgICAgIGluZm93aW5kb3cub3BlbihzZWxmLm1hcCwgbWFya2VyKTtcblxuICAgICAgICAgICAgLy9tYXJrZXIgb2JqZWN0IFt0aXRsZSwgbGF0LCBsb25nLCBpbmRleF1cbiAgICAgICAgICAgIGlmIChsaXN0TG9jYXRpb25baV1bM10pIHtcbiAgICAgICAgICAgICAgc2VsZi5tYXJrZXJDbGljaz8uZW1pdChsaXN0TG9jYXRpb25baV1bM10pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9KSk7XG4gICAgfVxuICAgIGlmICh0aGlzLnNob3VsZENsdXN0ZXJlcikge1xuICAgICAgdGhpcy5tYXJrZXJDbHVzdGVyLmFkZE1hcmtlcnModGhpcy5tYXJrZXJzKTtcbiAgICB9XG4gIH1cblxuICBwYW5UbyhsYXQ6IG51bWJlciwgbG5nOiBudW1iZXIsIHpvb21OdW1iZXI6IG51bWJlciA9IDEyKSB7XG4gICAgbGV0IG15TGF0TG5nID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXQsIGxuZyk7XG4gICAgdGhpcy5tYXAuc2V0Wm9vbSh6b29tTnVtYmVyKTtcbiAgICB0aGlzLm1hcC5wYW5UbyhteUxhdExuZyk7XG4gIH1cblxuICBnZW9jb2RlUG9zaXRpb24ocG9zOiBhbnksIGV2ZW50TmFtZTogc3RyaW5nID0gdW5kZWZpbmVkLCBmdW5jOiBGdW5jdGlvbiA9IG51bGwpOiB2b2lkIHtcbiAgICB0aGlzLmdlb2NvZGVyLmdlb2NvZGUoeyAnbGF0TG5nJzogcG9zIH0sIChyZXNwb25zZXM6IHN0cmluZyB8IGFueVtdKSA9PiB7XG4gICAgICBpZiAocmVzcG9uc2VzICYmIHJlc3BvbnNlcy5sZW5ndGggPiAwKSB7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLnRpdGxlID0gcmVzcG9uc2VzWzBdLmZvcm1hdHRlZF9hZGRyZXNzO1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sYXRpdHVkZSA9IHJlc3BvbnNlc1swXS5nZW9tZXRyeS5sb2NhdGlvbi5sYXQoKTtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IHJlc3BvbnNlc1swXS5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLnRpdGxlID0gXCJDYW5ub3QgZGV0ZXJtaW5lIGFkZHJlc3MgYXQgdGhpcyBsb2NhdGlvbi5cIjtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSBudWxsO1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS5sb25ndGl0dWRlID0gbnVsbDtcbiAgICAgIH1cbiAgICAgIHRoaXMudHJpZ2dlclVwZGF0ZURhdGEoZXZlbnROYW1lKTtcbiAgICAgIC8vICQoXCIjcGFjLWlucHV0XCIpLmNsaWNrKCk7XG4gICAgICBpZiAoZnVuYykge1xuICAgICAgICBmdW5jKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBhZGRTZWFyY2hCb3hFdmVudCgpOiB2b2lkIHtcbiAgICB0aGlzLnNlYXJjaEJveC5hZGRMaXN0ZW5lcigncGxhY2VzX2NoYW5nZWQnLCAoKSA9PiB7XG4gICAgICBpZiAodGhpcy5tYXJrZXIpIHtcbiAgICAgICAgdGhpcy5tYXJrZXIuc2V0TWFwKG51bGwpO1xuICAgICAgfVxuXG4gICAgICBsZXQgcGxhY2VzID0gdGhpcy5zZWFyY2hCb3guZ2V0UGxhY2VzKCk7XG4gICAgICBpZiAocGxhY2VzLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgbGV0IGJvdW5kcyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmdCb3VuZHMoKTtcbiAgICAgIHBsYWNlcy5mb3JFYWNoKChwbGFjZSkgPT4ge1xuICAgICAgICBsZXQgbWFya2VyT2JqID0ge1xuICAgICAgICAgIG1hcDogdGhpcy5tYXAsXG4gICAgICAgICAgZHJhZ2dhYmxlOiB0aGlzLm1hcmtlckRyYWdnYWJsZSxcbiAgICAgICAgICB0aXRsZTogcGxhY2UubmFtZSxcbiAgICAgICAgICBhbmltYXRpb246IGdvb2dsZS5tYXBzLkFuaW1hdGlvbi5EUk9QLFxuICAgICAgICAgIHBvc2l0aW9uOiBwbGFjZS5nZW9tZXRyeS5sb2NhdGlvblxuICAgICAgICB9O1xuXG4gICAgICAgIGlmICghcGxhY2UuZ2VvbWV0cnkpIHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIlJldHVybmVkIHBsYWNlIGNvbnRhaW5zIG5vIGdlb21ldHJ5XCIpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcihtYXJrZXJPYmopO1xuXG4gICAgICAgIHRoaXMuYWRkRHJhZ2VuZExpc3RlbmVyKCk7XG5cbiAgICAgICAgdGhpcy5zZWFyY2hFdmVudD8uZW1pdChuZXcgQ29yZGluYXRlRHRvKG1hcmtlck9iai5wb3NpdGlvbi5sYXQoKSwgbWFya2VyT2JqLnBvc2l0aW9uLmxuZygpKSk7XG5cbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSBwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbi5sYXQoKTtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubG9uZ3RpdHVkZSA9IHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpO1xuICAgICAgICB0aGlzLmNvcmRpbmF0ZS50aXRsZSA9IHBsYWNlLmZvcm1hdHRlZF9hZGRyZXNzO1xuXG4gICAgICAgIHRoaXMudHJpZ2dlclVwZGF0ZURhdGEobWFwQ29uZmlnLm1hcEV2ZW50TmFtZXMuc2VhcmNoKTtcblxuICAgICAgICBpZiAocGxhY2UuZ2VvbWV0cnkudmlld3BvcnQpIHtcbiAgICAgICAgICBib3VuZHMudW5pb24ocGxhY2UuZ2VvbWV0cnkudmlld3BvcnQpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGJvdW5kcy5leHRlbmQocGxhY2UuZ2VvbWV0cnkubG9jYXRpb24pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHRoaXMubWFwLmZpdEJvdW5kcyhib3VuZHMpO1xuICAgICAgdGhpcy5tYXAuc2V0Wm9vbSgxMik7XG4gICAgICB0aGlzLm1hcC5wYW5Ubyh0aGlzLm1hcmtlci5nZXRQb3NpdGlvbigpKTtcbiAgICB9KTtcbiAgfVxuXG4gIGFkZENsaWNrTGlzdGVuZXIoKTogdm9pZCB7XG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIodGhpcy5tYXAsICdjbGljaycsIChldmVudCkgPT4ge1xuICAgICAgLy9ldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgdGhpcy5nZW9jb2RlUG9zaXRpb24oZXZlbnQubGF0TG5nLCBtYXBDb25maWcubWFwRXZlbnROYW1lcy5tYXBDbGljaywgKCkgPT4ge1xuICAgICAgICB0aGlzLm1hcmtlci5zZXRNYXAobnVsbCk7XG4gICAgICAgIHRoaXMubWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgcG9zaXRpb246IGV2ZW50LmxhdExuZyxcbiAgICAgICAgICBtYXA6IHRoaXMubWFwLFxuICAgICAgICAgIGRyYWdnYWJsZTogdGhpcy5tYXJrZXJEcmFnZ2FibGUsXG5cbiAgICAgICAgICB0aXRsZTogdGhpcy5jb3JkaW5hdGUudGl0bGVcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuYWRkRHJhZ2VuZExpc3RlbmVyKCk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGFkZERyYWdlbmRMaXN0ZW5lcigpOiB2b2lkIHtcbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcih0aGlzLm1hcmtlciwgJ2RyYWdlbmQnLCAoKSA9PiB7XG4gICAgICAvLyBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgdGhpcy5kZWxldGVNYXJrZXJzKCk7XG4gICAgICB0aGlzLmdlb2NvZGVQb3NpdGlvbih0aGlzLm1hcmtlci5nZXRQb3NpdGlvbigpLCBtYXBDb25maWcubWFwRXZlbnROYW1lcy5tYXJrZXJEcmFnKTtcbiAgICAgIHRoaXMuY29yZGluYXRlLmxhdGl0dWRlID0gdGhpcy5tYXJrZXIuZ2V0UG9zaXRpb24oKS5sYXQoKTtcbiAgICAgIHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSB0aGlzLm1hcmtlci5nZXRQb3NpdGlvbigpLmxuZygpO1xuICAgIH0pO1xuICB9XG5cbiAgdHJpZ2dlclVwZGF0ZURhdGEoZXZlbnROYW1lOiBzdHJpbmcpOiB2b2lkIHtcbiAgICB0aGlzLmNvcmRpbmF0ZS5ldmVudE5hbWUgPSBldmVudE5hbWU7XG4gICAgdGhpcy5jb3JkaW5hdGVDaGFuZ2U/LmVtaXQodGhpcy5jb3JkaW5hdGUpO1xuICB9XG5cbiAgbW92ZVRvTXlMb2NhdGlvbigpIHtcbiAgICBpZiAoIXRoaXMucmVxdWVzdExvY2F0aW9uKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmIChuYXZpZ2F0b3IuZ2VvbG9jYXRpb24pIHtcbiAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oKGxvY2F0aW9uKSA9PiB7XG4gICAgICAgIGlmICh0aGlzLm1hcmtlcikge1xuICAgICAgICAgIHRoaXMubWFya2VyLnNldE1hcChudWxsKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBzZWxmLmRlbGV0ZU1hcmtlcnMoKTtcbiAgICAgICAgdGhpcy5jb3JkaW5hdGUubGF0aXR1ZGUgPSBsb2NhdGlvbi5jb29yZHMubGF0aXR1ZGU7XG4gICAgICAgIHRoaXMuY29yZGluYXRlLmxvbmd0aXR1ZGUgPSBsb2NhdGlvbi5jb29yZHMubG9uZ2l0dWRlO1xuXG4gICAgICAgIHRoaXMubXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGxvY2F0aW9uLmNvb3Jkcy5sYXRpdHVkZSwgbG9jYXRpb24uY29vcmRzLmxvbmdpdHVkZSk7XG5cbiAgICAgICAgaWYgKHRoaXMubXlMb2NhdGlvbikge1xuICAgICAgICAgIHRoaXMubXlMb2NhdGlvbi5zZXRNYXAobnVsbCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5teUxvY2F0aW9uID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgcG9zaXRpb246IHRoaXMubXlMYXRMbmcsXG4gICAgICAgICAgaWNvbjoge1xuICAgICAgICAgICAgdXJsOiB0aGlzLm15TG9jYXRpb25JY29uLCAvLyB1cmxcbiAgICAgICAgICAgIHNjYWxlZFNpemU6IG5ldyBnb29nbGUubWFwcy5TaXplKDMwLCAzMCksIC8vIHNjYWxlZCBzaXplXG4gICAgICAgICAgICBvcmlnaW46IG5ldyBnb29nbGUubWFwcy5Qb2ludCgwLCAwKSwgLy8gb3JpZ2luXG4gICAgICAgICAgICBhbmNob3I6IG5ldyBnb29nbGUubWFwcy5Qb2ludCgxNSwgMTUpIC8vIGFuY2hvclxuICAgICAgICAgIH0sXG4gICAgICAgICAgekluZGV4OiAxMDAwLFxuICAgICAgICAgIG1hcDogdGhpcy5tYXAsXG4gICAgICAgICAgZHJhZ2dhYmxlOiBmYWxzZSxcbiAgICAgICAgICB0aXRsZTogdGhpcy5jb3JkaW5hdGUudGl0bGVcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy5nZW9jb2RlUG9zaXRpb24odGhpcy5teUxvY2F0aW9uLmdldFBvc2l0aW9uKCksIG1hcENvbmZpZy5tYXBFdmVudE5hbWVzLm15TG9jYXRpb24pO1xuICAgICAgICB0aGlzLmdldEN1cnJlbnRMb2NhdGlvbkV2ZW50Py5lbWl0KG5ldyBDb3JkaW5hdGVEdG8obG9jYXRpb24uY29vcmRzLmxhdGl0dWRlLCBsb2NhdGlvbi5jb29yZHMubG9uZ2l0dWRlKSlcbiAgICAgICAgdGhpcy5tYXAuc2V0Q2VudGVyKHRoaXMubXlMYXRMbmcpO1xuICAgICAgICB0aGlzLm15TG9jYWx0aW9uTG9hZGVkID0gdHJ1ZTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZyhcIkdlb2xvY2F0aW9uIGlzIG5vdCBzdXBwb3J0ZWQgYnkgdGhpcyBicm93c2VyLlwiKTtcbiAgICB9XG4gIH1cblxuICBnZXRUb3BEaXN0YW5jZUZyb21NYXJrZXIobGF0OiBudW1iZXIsIGxuZzogbnVtYmVyLCBldmVudE5hbWU/OiBzdHJpbmcpIHtcbiAgICBpZiAoIWxhdCB8fCAhbG5nKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGxldCBteUxhdExuZyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcobGF0LCBsbmcpO1xuICAgIGxldCBib3VuZHMgPSB0aGlzLm1hcC5nZXRCb3VuZHMoKTtcbiAgICB0aGlzLmdldFRvcENvcmRpbmF0b3JFdmVudD8uZW1pdChuZXcgQ29yZGluYXRlRHRvKGJvdW5kcy5nZXROb3J0aEVhc3QoKS5sYXQoKSwgbXlMYXRMbmcubG5nKCksIFwiXCIsIGV2ZW50TmFtZSkpO1xuICAgIFxuICB9XG5cbiAgZHJhd0NpcmNsZShjZW50ZXJMYXQ6IG51bWJlciwgY2VudGVyTG5nOiBudW1iZXIsIHBvaW50TGF0OiBudW1iZXIsIHBvaW50TG5nOiBudW1iZXIsIGZpbGxDb2xvcjogc3RyaW5nID0gXCIjRkYwMDAwXCIpIHtcbiAgICBsZXQgbXlMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKGNlbnRlckxhdCwgY2VudGVyTG5nKTtcbiAgICAvLyBHZXQgbWFwIGJvdW5kc1xuICAgIGxldCBib3VuZHMgPSB0aGlzLm1hcC5nZXRCb3VuZHMoKTtcbiAgICBsZXQgcG9pbnQgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHBvaW50TGF0LCBwb2ludExuZyk7XG4gICAgbGV0IGRpc3RhbmNlVG9Ub3AgPSBnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2VlbihteUxhdExuZywgcG9pbnQpO1xuICAgIGxldCByYWRpdXMgPSBkaXN0YW5jZVRvVG9wO1xuICAgIHRoaXMuY2lyY2xlLnNldE1hcChudWxsKTtcbiAgICB0aGlzLmNpcmNsZSA9IG5ldyBnb29nbGUubWFwcy5DaXJjbGUoe1xuICAgICAgc3Ryb2tlQ29sb3I6ICcjRkYwMDAwJyxcbiAgICAgIHN0cm9rZU9wYWNpdHk6IDAuOCxcbiAgICAgIHN0cm9rZVdlaWdodDogMixcbiAgICAgIGZpbGxDb2xvcjogZmlsbENvbG9yLFxuICAgICAgZmlsbE9wYWNpdHk6IDAuMzUsXG4gICAgICBlZGl0YWJsZTogdHJ1ZSxcbiAgICAgIG1hcDogdGhpcy5tYXAsXG4gICAgICBjZW50ZXI6IG15TGF0TG5nLFxuICAgICAgcmFkaXVzOiByYWRpdXNcbiAgICB9KTtcbiAgfVxufSIsIjxkaXYgY2xhc3M9XCJ2aWEtbWFwLXdyYXBwZXJcIj5cbiAgICA8ZGl2ICpuZ0lmPVwiIWhpZGVTZWFyY2hUZXh0Qm94XCIgY2xhc3M9XCJtYXAtc2VhcmNoLWNvbnRhaW5lclwiPlxuICAgICAgICA8ZGl2IGlkPVwidmlhLW1hcC1zZWFyY2hcIj5cbiAgICAgICAgICAgIDxtYXQtZm9ybS1maWVsZCBhcHBlYXJhbmNlPVwiZmlsbFwiPlxuICAgICAgICAgICAgICAgIDwhLS0gPG1hdC1sYWJlbD5DbGVhcmFibGUgaW5wdXQ8L21hdC1sYWJlbD4gLS0+XG4gICAgICAgICAgICAgICAgPGlucHV0IFxuICAgICAgICAgICAgICAgICAgICBtYXRJbnB1dCBcbiAgICAgICAgICAgICAgICAgICAgI3NlYXJjaElucHV0IFxuICAgICAgICAgICAgICAgICAgICBbcGxhY2Vob2xkZXJdPVwic2VhcmNoUGxhY2Vob2xlclwiXG4gICAgICAgICAgICAgICAgICAgIFsobmdNb2RlbCldPVwiY29yZGluYXRlLnRpdGxlXCI+XG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBtYXRTdWZmaXggbWF0LWljb24tYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgPG1hdC1pY29uPnNlYXJjaDwvbWF0LWljb24+XG4gICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxuICAgICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgICA8ZGl2IGNsYXNzPVwiXCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJnb29nbGUtbWFwc1wiICNnb29nbGVNYXA+PC9kaXY+XG4gICAgICAgIDxidXR0b24gKm5nSWY9XCJzaG93TXlMb2NhdGlvbiAmJiBtYXBMb2FkZWQgJiYgbXlMb2NhbHRpb25Mb2FkZWRcIiAoY2xpY2spPVwibW92ZVRvTXlMb2NhdGlvbigpXCIgY2xhc3M9XCJteS1sb2NhdGlvbi1idG5cIj5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiYXNzZXRzL2ljb25zL2dyZWVuX3Bpbi5zdmdcIj5cbiAgICAgICAgPC9idXR0b24+XG4gICAgPC9kaXY+XG5cbiAgICA8ZGl2IGNsYXNzPVwic3Bpbm5lci13cmFwcGVyXCIgKm5nSWY9XCJsb2FkaW5nXCI+XG4gICAgICAgIDxtYXQtc3Bpbm5lciBzdHJva2VXaWR0aD1cIjFcIiBkaWFtZXRlcj1cIjIwXCI+PC9tYXQtc3Bpbm5lcj5cbiAgICA8L2Rpdj5cbjwvZGl2PiJdfQ==