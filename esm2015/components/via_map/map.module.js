import { NgModule } from '@angular/core';
import { MapComponent } from './map.component';
import { FormsModule } from "@angular/forms";
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
export function myLibInit(apiKey) {
    var x = (config) => () => config.load(apiKey);
    return x;
}
export class MapModule {
}
MapModule.ɵmod = i0.ɵɵdefineNgModule({ type: MapModule });
MapModule.ɵinj = i0.ɵɵdefineInjector({ factory: function MapModule_Factory(t) { return new (t || MapModule)(); }, imports: [[FormsModule, CommonModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(MapModule, { declarations: [MapComponent], imports: [FormsModule, CommonModule], exports: [MapComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MapModule, [{
        type: NgModule,
        args: [{
                declarations: [MapComponent],
                imports: [FormsModule, CommonModule],
                exports: [MapComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWNyYWN5Lm5ndXllbi9Xb3Jrcy9SZXBvc2l0b3JpZXMvdmlhdGVjaC9jb21tb24vdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi1zb3VyY2UvcHJvamVjdHMvdmlhdGVjaC1hbmd1bGFyLWNvbW1vbi9zcmMvIiwic291cmNlcyI6WyJjb21wb25lbnRzL3ZpYV9tYXAvbWFwLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUF3QyxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQzs7QUFJL0MsTUFBTSxVQUFVLFNBQVMsQ0FBQyxNQUFjO0lBQ3RDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBaUIsRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN6RCxPQUFPLENBQUMsQ0FBQztBQUNYLENBQUM7QUFTRCxNQUFNLE9BQU8sU0FBUzs7NkNBQVQsU0FBUztpR0FBVCxTQUFTLGtCQUxYLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQzt3RkFLekIsU0FBUyxtQkFOTCxZQUFZLGFBQ2pCLFdBQVcsRUFBRSxZQUFZLGFBQ3pCLFlBQVk7a0RBSVgsU0FBUztjQVByQixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsWUFBWSxDQUFDO2dCQUM1QixPQUFPLEVBQUUsQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDO2dCQUNwQyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDeEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycywgQVBQX0lOSVRJQUxJWkVSIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXBDb21wb25lbnQgfSBmcm9tICcuL21hcC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBNYXBMb2FkZXIgfSBmcm9tICcuL21hcC5zZXJ2aWNlJztcbmltcG9ydCB7IExBWllfTUFQU19BUElfQ09ORklHIH0gZnJvbSAnLi9nbWFwLmNvbmZpZyc7XG5cbmV4cG9ydCBmdW5jdGlvbiBteUxpYkluaXQoYXBpS2V5OiBzdHJpbmcpIHtcbiAgdmFyIHggPSAoY29uZmlnOiBNYXBMb2FkZXIpID0+ICgpID0+IGNvbmZpZy5sb2FkKGFwaUtleSk7XG4gIHJldHVybiB4O1xufVxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtNYXBDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbRm9ybXNNb2R1bGUsIENvbW1vbk1vZHVsZV0sXG4gIGV4cG9ydHM6IFtNYXBDb21wb25lbnRdXG59KVxuXG5cbmV4cG9ydCBjbGFzcyBNYXBNb2R1bGUge1xuICAvLyBzdGF0aWMgZm9yUm9vdChsYXp5Q29uZmlnPzogTEFaWV9NQVBTX0FQSV9DT05GSUcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgLy8gICByZXR1cm4ge1xuICAvLyAgICAgbmdNb2R1bGU6IE1hcE1vZHVsZSxcbiAgLy8gICAgIHByb3ZpZGVyczogW1xuICAvLyAgICAgICBNYXBMb2FkZXIsXG4gIC8vICAgICAgIHtcbiAgLy8gICAgICAgICBwcm92aWRlOiBBUFBfSU5JVElBTElaRVIsXG4gIC8vICAgICAgICAgdXNlRmFjdG9yeTogbXlMaWJJbml0KGxhenlDb25maWcuYXBpS2V5KSxcbiAgLy8gICAgICAgICBkZXBzOiBbTWFwTG9hZGVyXSxcbiAgLy8gICAgICAgICBtdWx0aTogdHJ1ZVxuICAvLyAgICAgICB9XG4gIC8vICAgICBdXG4gIC8vICAgfVxuICAvLyB9XG59XG5cbiJdfQ==