import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
// @dynamic
// @dynamic
export class MapLoader {
    constructor() { }
    load(apikey) {
        return new Promise((resolve) => {
            let googleScriptEle = document.getElementById("via-google-map");
            if (googleScriptEle) {
                return Promise.resolve();
            }
            window['__onGapiLoaded'] = (ev) => {
                resolve(true);
            };
            const node = document.createElement('script');
            node.src = 'https://maps.googleapis.com/maps/api/js?libraries=geometry,places&key=' + apikey + '&callback=__onGapiLoaded';
            node.type = 'text/javascript';
            node.id = 'via-google-map';
            document.getElementsByTagName('head')[0].appendChild(node);
        });
    }
}
MapLoader.ɵfac = function MapLoader_Factory(t) { return new (t || MapLoader)(); };
MapLoader.ɵprov = i0.ɵɵdefineInjectable({ token: MapLoader, factory: MapLoader.ɵfac });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MapLoader, [{
        type: Injectable
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FjcmFjeS5uZ3V5ZW4vV29ya3MvUmVwb3NpdG9yaWVzL3ZpYXRlY2gvY29tbW9uL3ZpYXRlY2gtYW5ndWxhci1jb21tb24tc291cmNlL3Byb2plY3RzL3ZpYXRlY2gtYW5ndWxhci1jb21tb24vc3JjLyIsInNvdXJjZXMiOlsiY29tcG9uZW50cy92aWFfbWFwL21hcC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBQzNDLFdBQVc7QUFFWCxXQUFXO0FBQ1gsTUFBTSxPQUFPLFNBQVM7SUFHbEIsZ0JBQWdCLENBQUM7SUFFakIsSUFBSSxDQUFDLE1BQWM7UUFDZixPQUFPLElBQUksT0FBTyxDQUFVLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDcEMsSUFBSSxlQUFlLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ2hFLElBQUksZUFBZSxFQUFFO2dCQUNqQixPQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUM1QjtZQUNELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEVBQUU7Z0JBQzlCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQixDQUFDLENBQUE7WUFDRCxNQUFNLElBQUksR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxHQUFHLEdBQUcsd0VBQXdFLEdBQUcsTUFBTSxHQUFHLDBCQUEwQixDQUFDO1lBQzFILElBQUksQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7WUFDOUIsSUFBSSxDQUFDLEVBQUUsR0FBRyxnQkFBZ0IsQ0FBQztZQUMzQixRQUFRLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9ELENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQzs7a0VBckJRLFNBQVM7aURBQVQsU0FBUyxXQUFULFNBQVM7a0RBQVQsU0FBUztjQUZyQixVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuLy8gQGR5bmFtaWNcbkBJbmplY3RhYmxlKClcbi8vIEBkeW5hbWljXG5leHBvcnQgY2xhc3MgTWFwTG9hZGVyIHtcbiAgICBwdWJsaWMgYXBpa2V5OiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gICAgbG9hZChhcGlrZXk6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2U8Ym9vbGVhbj4oKHJlc29sdmUpID0+IHtcbiAgICAgICAgICAgIGxldCBnb29nbGVTY3JpcHRFbGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInZpYS1nb29nbGUtbWFwXCIpO1xuICAgICAgICAgICAgaWYgKGdvb2dsZVNjcmlwdEVsZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHdpbmRvd1snX19vbkdhcGlMb2FkZWQnXSA9IChldikgPT4ge1xuICAgICAgICAgICAgICAgIHJlc29sdmUodHJ1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBub2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICAgICAgICBub2RlLnNyYyA9ICdodHRwczovL21hcHMuZ29vZ2xlYXBpcy5jb20vbWFwcy9hcGkvanM/bGlicmFyaWVzPWdlb21ldHJ5LHBsYWNlcyZrZXk9JyArIGFwaWtleSArICcmY2FsbGJhY2s9X19vbkdhcGlMb2FkZWQnO1xuICAgICAgICAgICAgbm9kZS50eXBlID0gJ3RleHQvamF2YXNjcmlwdCc7XG4gICAgICAgICAgICBub2RlLmlkID0gJ3ZpYS1nb29nbGUtbWFwJztcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdoZWFkJylbMF0uYXBwZW5kQ2hpbGQobm9kZSk7XG4gICAgICAgIH0pO1xuXG4gICAgfVxufSJdfQ==