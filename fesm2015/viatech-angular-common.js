import { EventEmitter, ɵɵdefineComponent, ɵɵelementStart, ɵɵlistener, ɵɵelementEnd, ɵɵadvance, ɵɵproperty, ɵsetClassMetadata, Component, Input, Output, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule, ɵɵdirectiveInject, ɵɵtext, ɵɵelement, ɵɵgetCurrentView, ɵɵrestoreView, ɵɵnextContext, ɵɵtemplate, ElementRef, ɵɵdefineDirective, ɵɵresolveDocument, Directive, HostListener, ɵɵpropertyInterpolate, ɵɵtextInterpolate, ɵɵsanitizeHtml, ɵɵInheritDefinitionFeature, ɵɵsanitizeUrl, ɵɵtextInterpolate1, ɵɵdefineInjectable, Injectable, ɵɵinject, ɵɵreference, ɵɵgetInheritedFactory, ɵɵdefinePipe, Pipe, ɵɵpureFunction1, ɵɵpipe, ɵɵpipeBind3, ɵɵprojection, ɵɵattribute, ɵɵpureFunction3, ɵɵpipeBind1, ɵɵpropertyInterpolate2, ɵɵpipeBind2, ɵɵelementContainerStart, ɵɵelementContainerEnd, ɵɵtextInterpolate2, ɵɵprojectionDef, ɵɵclassMap, ɵɵclassMapInterpolate1, ɵɵviewQuery, ɵɵqueryRefresh, ɵɵloadQuery, ViewChild } from '@angular/core';
import { NgbTimepicker, NgbModule, NgbActiveModal, NgbDatepickerI18n, NgbDate, NgbDateParserFormatter, NgbCalendar, NgbInputDatepicker, NgbDateAdapter, NgbPagination, NgbDropdown, NgbDropdownToggle, NgbDropdownMenu, NgbDropdownItem, NgbPaginationPrevious, NgbPaginationNext, NgbProgressbar } from '@ng-bootstrap/ng-bootstrap';
import { NgControlStatus, NgModel, FormsModule, ReactiveFormsModule, DefaultValueAccessor, FormControl, FormControlDirective, NgSelectOption, ɵangular_packages_forms_forms_x, CheckboxControlValueAccessor } from '@angular/forms';
import $$1 from 'jquery';
import { NgIf, CommonModule, NgForOf, NgClass, NgSwitch, NgSwitchCase, NgSwitchDefault, DatePipe } from '@angular/common';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { trigger } from '@angular/animations';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import { Router, RouterModule, NavigationEnd } from '@angular/router';
import { ImageCropperComponent, ImageCropperModule } from 'ngx-image-cropper';
import { humanizeBytes, NgFileDropDirective, NgFileSelectDirective, NgxUploaderModule } from 'ngx-uploader';
import { HttpHeaders, HttpClient, HttpClientModule } from '@angular/common/http';

class TimepickerComponent {
    constructor() {
        this.showSeccond = false;
        this.showMeridian = false;
        this.timepickerModelChange = new EventEmitter();
        this.disabled = false;
        this.spinners = false;
    }
    ngOnInit() {
    }
    getDateSelected() {
        // if(this.objectUtils.isValid(this.timepickerItem)){
        //   return new Date(this.timepickerItem.year, this.timepickerItem.month - 1, this.timepickerItem.day);
        // }
        // return null;
    }
    timeSelect(time) {
        this.timepickerModelChange.emit(time);
    }
}
TimepickerComponent.ɵfac = function TimepickerComponent_Factory(t) { return new (t || TimepickerComponent)(); };
TimepickerComponent.ɵcmp = ɵɵdefineComponent({ type: TimepickerComponent, selectors: [["via-timepicker"]], inputs: { timepickerModel: "timepickerModel", showSeccond: "showSeccond", showMeridian: "showMeridian", disabled: "disabled" }, outputs: { timepickerModelChange: "timepickerModelChange" }, decls: 2, vars: 5, consts: [["id", "win-timepicker"], [3, "readonlyInputs", "ngModel", "meridian", "seconds", "spinners", "ngModelChange"]], template: function TimepickerComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "div", 0);
        ɵɵelementStart(1, "ngb-timepicker", 1);
        ɵɵlistener("ngModelChange", function TimepickerComponent_Template_ngb_timepicker_ngModelChange_1_listener($event) { return ctx.timepickerModel = $event; })("ngModelChange", function TimepickerComponent_Template_ngb_timepicker_ngModelChange_1_listener($event) { return ctx.timeSelect($event); });
        ɵɵelementEnd();
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵadvance(1);
        ɵɵproperty("readonlyInputs", ctx.disabled)("ngModel", ctx.timepickerModel)("meridian", ctx.showMeridian)("seconds", ctx.showSeccond)("spinners", ctx.spinners);
    } }, directives: [NgbTimepicker, NgControlStatus, NgModel], styles: ["#win-datepicker[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:column;width:100%}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:5px 10px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]{align-items:center;display:flex;flex:1;justify-content:baseline}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{margin-right:5px;width:20px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{flex:8}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span.dropdown-arrow[_ngcontent-%COMP%]{flex:1;font-size:24px;text-align:right}#win-datepicker[_ngcontent-%COMP%]   .input-datepicker[_ngcontent-%COMP%]{font-size:0;height:0;line-height:0;margin:0;padding:0;visibility:hidden;width:100%}"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(TimepickerComponent, [{
        type: Component,
        args: [{
                selector: "via-timepicker",
                templateUrl: "./timepicker.html",
                styleUrls: ["./timepicker.scss"]
            }]
    }], function () { return []; }, { timepickerModel: [{
            type: Input
        }], showSeccond: [{
            type: Input
        }], showMeridian: [{
            type: Input
        }], timepickerModelChange: [{
            type: Output
        }], disabled: [{
            type: Input
        }] }); })();

class TimepickerModule {
}
TimepickerModule.ɵmod = ɵɵdefineNgModule({ type: TimepickerModule });
TimepickerModule.ɵinj = ɵɵdefineInjector({ factory: function TimepickerModule_Factory(t) { return new (t || TimepickerModule)(); }, imports: [[
            NgbModule,
            FormsModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(TimepickerModule, { declarations: [TimepickerComponent], imports: [NgbModule,
        FormsModule,
        ReactiveFormsModule], exports: [TimepickerComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(TimepickerModule, [{
        type: NgModule,
        args: [{
                declarations: [TimepickerComponent],
                imports: [
                    NgbModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                exports: [TimepickerComponent]
            }]
    }], null, null); })();

class ImageSphereComponent {
    constructor(ngbActiveModal) {
        this.ngbActiveModal = ngbActiveModal;
    }
    ngOnInit() {
        pannellum.viewer('panorama', {
            "type": "equirectangular",
            "autoLoad": true,
            "title": "360 Photo",
            "compass": true,
            "panorama": this.imgPath
        });
    }
    closeModal() {
        this.ngbActiveModal.close();
    }
}
ImageSphereComponent.ɵfac = function ImageSphereComponent_Factory(t) { return new (t || ImageSphereComponent)(ɵɵdirectiveInject(NgbActiveModal)); };
ImageSphereComponent.ɵcmp = ɵɵdefineComponent({ type: ImageSphereComponent, selectors: [["via-photo-sphere"]], inputs: { imgPath: "imgPath" }, decls: 5, vars: 0, consts: [["id", "photo-sphere"], [1, "modal-360", 3, "click"], ["aria-hidden", "true"], ["id", "panorama"]], template: function ImageSphereComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "div", 0);
        ɵɵelementStart(1, "div", 1);
        ɵɵlistener("click", function ImageSphereComponent_Template_div_click_1_listener() { return ctx.closeModal(); });
        ɵɵelementStart(2, "span", 2);
        ɵɵtext(3, "\u00D7");
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelement(4, "div", 3);
        ɵɵelementEnd();
    } }, styles: ["#photo-sphere[_ngcontent-%COMP%]   #panorama[_ngcontent-%COMP%]{min-height:700px}#photo-sphere[_ngcontent-%COMP%]   .modal-360[_ngcontent-%COMP%]{background:#fff;border:1px solid rgba(0,0,0,.4);border-radius:4px;cursor:pointer;height:25px;line-height:25px;position:absolute;right:3px;text-align:center;top:3px;width:25px;z-index:1}"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(ImageSphereComponent, [{
        type: Component,
        args: [{
                selector: "via-photo-sphere",
                templateUrl: "./360.img.html",
                styleUrls: ["./360.img.scss"]
            }]
    }], function () { return [{ type: NgbActiveModal }]; }, { imgPath: [{
            type: Input
        }] }); })();

class ImageSphereModule {
}
ImageSphereModule.ɵmod = ɵɵdefineNgModule({ type: ImageSphereModule });
ImageSphereModule.ɵinj = ɵɵdefineInjector({ factory: function ImageSphereModule_Factory(t) { return new (t || ImageSphereModule)(); }, imports: [[]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(ImageSphereModule, { declarations: [ImageSphereComponent], exports: [ImageSphereComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(ImageSphereModule, [{
        type: NgModule,
        args: [{
                declarations: [ImageSphereComponent],
                imports: [],
                exports: [ImageSphereComponent]
            }]
    }], null, null); })();

const mapConfig = {
    defaultLattitude: 10.8230989,
    defaultLongtitude: 106.6296638,
    defaultTitle: "Ho CHi Minh City",
    mapEventNames: {
        search: "search",
        myLocation: "myLocation",
        mapClick: "mapClick",
        markerDrag: "markerDrag"
    },
    myLocation: "MAP_MY_LOCATION"
};
const DATE_FORMAT = "hh:mma dd/MM/yyyy";
const TIME_FORMAT = "hh:mma";
const DATE_FORMAT_WITHOUT_TIME = "dd/MM/yyyy";

class Map {
    constructor(latitude, longtitude, title, eventName, zoomLevel) {
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.title = title;
        this.eventName = eventName;
        this.zoomLevel = zoomLevel;
    }
}

function MapComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r3 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 5);
    ɵɵelementStart(1, "div", 6);
    ɵɵelementStart(2, "div", 7);
    ɵɵelement(3, "div", 8);
    ɵɵelementStart(4, "input", 9);
    ɵɵlistener("ngModelChange", function MapComponent_div_1_Template_input_ngModelChange_4_listener($event) { ɵɵrestoreView(_r3); const ctx_r2 = ɵɵnextContext(); return ctx_r2.cordinate.title = $event; });
    ɵɵelementEnd();
    ɵɵelement(5, "img", 10);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = ɵɵnextContext();
    ɵɵadvance(4);
    ɵɵproperty("ngModel", ctx_r0.cordinate.title);
} }
function MapComponent_button_4_Template(rf, ctx) { if (rf & 1) {
    const _r5 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 11);
    ɵɵlistener("click", function MapComponent_button_4_Template_button_click_0_listener() { ɵɵrestoreView(_r5); const ctx_r4 = ɵɵnextContext(); return ctx_r4.myCurrentLocation(); });
    ɵɵelement(1, "img", 12);
    ɵɵelementEnd();
} }
class MapComponent {
    constructor() {
        this.markerDraggable = true;
        this.customIcon = false;
        this.showMyLocation = true;
        this.hideSearchTextBox = false;
        this.cordinateChange = new EventEmitter();
        this.markerClick = new EventEmitter();
        this.mapRadius = new EventEmitter();
        this.mapDragged = new EventEmitter();
        this.searchEvent = new EventEmitter();
        this.getTopCordinatorEvent = new EventEmitter();
        this.getCurrentLocationEvent = new EventEmitter();
        this.greenPinIcon = 'assets/icons/green_pin.svg';
        this.locationIcon = 'assets/icons/red_pin.svg';
        this.markers = [];
    }
    ngAfterViewInit() {
        let self = this;
        self.placeholderText = self.searchPlaceholer;
        self.geocoder = new google.maps.Geocoder();
        let initMap = () => {
            self.myLatLng = {
                lat: self.cordinate.latitude, lng: self.cordinate.longtitude
            };
            let mapConfigObject = {
                zoom: 15,
                center: self.myLatLng,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            self.map = new google.maps.Map(document.getElementById('googleMap'), mapConfigObject);
            //listen to map drag event
            google.maps.event.addListener(self.map, 'dragend', function () {
                self.mapDragged.emit("");
            });
            //listen to map zoom event
            google.maps.event.addListener(self.map, 'zoom_changed', function () {
                if (self.mapRadius) {
                    let cordinate = new Map(undefined, undefined, undefined, "zoom_changed", self.map.getZoom());
                    self.mapRadius.emit(cordinate);
                }
            });
            if (!self.hideSearchTextBox) {
                self.input = document.getElementById('pac-input');
                self.searchBox = new google.maps.places.SearchBox(self.input);
                self.map.addListener('bounds_changed', () => {
                    self.searchBox.setBounds(self.map.getBounds());
                });
                self.addSearchBoxEvent();
            }
            if (self.cordinate.title === mapConfig.myLocation) {
                self.myLocation = new google.maps.Marker({
                    position: self.myLatLng,
                    icon: {
                        url: self.greenPinIcon,
                        scaledSize: new google.maps.Size(30, 30),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(0, 0) // anchor
                    },
                    zIndex: 1000,
                    map: self.map,
                    draggable: false
                });
            }
            else {
                self.marker = new google.maps.Marker({
                    position: self.myLatLng,
                    map: self.map,
                    draggable: self.markerDraggable,
                    animation: google.maps.Animation.DROP,
                    title: self.cordinate.title
                });
                self.addClickListener();
                self.addDragendListener();
            }
            self.geocodePosition(self.myLatLng);
        };
        if (self.cordinate.latitude && self.cordinate.longtitude) {
            initMap();
        }
        else {
            // ask user for current location permission 
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((location) => {
                    self.cordinate.latitude = location.coords.latitude;
                    self.cordinate.longtitude = location.coords.longitude;
                    initMap();
                }, () => {
                    // default location: 288H1 Nam Ky Khoi Nghia
                    self.cordinate.latitude = 10.7895299;
                    self.cordinate.longtitude = 106.68493590000003;
                    initMap();
                });
            }
        }
    }
    getFurthestPointFromLocation(lat, lng, eventName) {
        let cordinate, self = this, cornersArray = {}, corner1, corner2, corner3, corner4, farestPoint;
        let sW = this.map.getBounds().getSouthWest();
        let nE = this.map.getBounds().getNorthEast();
        cornersArray = {}, corner1, corner2, corner3, corner4;
        corner1 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), sW.lng()));
        corner2 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), nE.lng()));
        corner3 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(sW.lat(), nE.lng()));
        corner4 = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(lat, lng), new google.maps.LatLng(nE.lat(), sW.lng()));
        cornersArray[corner1] = {
            "lat": sW.lat(),
            "lng": sW.lng()
        };
        cornersArray[corner2] = {
            "lat": nE.lat(),
            "lng": nE.lng()
        };
        cornersArray[corner3] = {
            "lat": sW.lat(),
            "lng": nE.lng()
        };
        cornersArray[corner4] = {
            "lat": nE.lat(),
            "lng": sW.lng()
        };
        farestPoint = cornersArray[Math.max(corner1, corner2, corner3, corner4)];
        cordinate = new Map(farestPoint.lat, farestPoint.lng, "", eventName);
        this.mapRadius.emit(cordinate);
    }
    getCenterMapLocation() {
        return this.map.getCenter();
    }
    calculateDistance(aLat, aLng, bLat, bLng) {
        return google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(aLat, aLng), new google.maps.LatLng(bLat, bLng));
    }
    // Deletes all markers in the array by removing references to them.
    deleteMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers = [];
    }
    //listLocation[0]: code
    //listLocation[1]: Latitude (pointY)
    //listLocation[2]: Longtitude (pointX)
    //listLocation[3]: index
    //listLocation[4]: label
    showMultitpleLocations(listLocation = []) {
        let self = this;
        let infowindow = new google.maps.InfoWindow;
        let marker, i;
        let bounds = new google.maps.LatLngBounds();
        this.deleteMarkers();
        for (i = 0; i < listLocation.length; i++) {
            //marker CONFIG
            let markObject = {
                position: new google.maps.LatLng(listLocation[i][1], listLocation[i][2]),
                //labelOrigin: new google.maps.Point(9, 9),
                //   label: {
                //     text: listLocation[i][4]? listLocation[i][4]: "",
                //     fontSize: "12px",
                //     color: "#e74c3c",
                //     fontFamily: "montserrat"
                //  },
                scaledSize: new google.maps.Size(30, 30),
                map: this.map
            };
            markObject["icon"] = {
                url: this.locationIcon,
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0) // anchor
            };
            marker = new google.maps.Marker(markObject);
            this.markers.push(marker);
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    //get address
                    let latlng = new google.maps.LatLng(listLocation[i][1], listLocation[i][2]);
                    self.geocoder.geocode({ 'latLng': latlng }, (responses) => {
                        let address;
                        if (!listLocation[i][4]) {
                            if (responses && responses.length > 0) {
                                address = responses[0].formatted_address;
                            }
                            else {
                                address = "Cannot determine address at this location.";
                            }
                        }
                        else {
                            address = listLocation[i][4];
                        }
                        self.activeInfoWindow && self.activeInfoWindow.close();
                        self.activeInfoWindow = infowindow;
                        infowindow.setContent(address);
                        infowindow.open(self.map, marker);
                        //marker object [title, lat, long, index]
                        if (listLocation[i][3]) {
                            self.markerClick.emit(listLocation[i][3]);
                        }
                    });
                };
            })(marker, i));
        }
        // for (let index = 0; index < this.markers.length; index++) {
        //   bounds.extend(this.markers[index].getPosition());
        // }
        // if (self.marker) {
        //   bounds.extend(self.marker.getPosition());
        // }
        // if (self.myLocation) {
        //   bounds.extend(self.myLocation.getPosition());
        // }
        //this.map.fitBounds(bounds);
    }
    panTo(lat, lng, zoomNumber = 12) {
        let myLatLng = new google.maps.LatLng(lat, lng);
        this.map.setZoom(zoomNumber);
        this.map.panTo(myLatLng);
    }
    geocodePosition(pos, eventName = undefined, func = null) {
        this.geocoder.geocode({ 'latLng': pos }, (responses) => {
            if (responses && responses.length > 0) {
                this.cordinate.title = responses[0].formatted_address;
                this.cordinate.latitude = responses[0].geometry.location.lat();
                this.cordinate.longtitude = responses[0].geometry.location.lng();
            }
            else {
                this.cordinate.title = "Cannot determine address at this location.";
                this.cordinate.latitude = null;
                this.cordinate.longtitude = null;
            }
            this.triggerUpdateData(eventName);
            $$1("#pac-input").click();
            if (func) {
                func();
            }
        });
    }
    addSearchBoxEvent() {
        let self = this;
        self.searchBox.addListener('places_changed', () => {
            if (self.marker) {
                self.marker.setMap(null);
            }
            //self.deleteMarkers();
            let places = self.searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            let bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                let markerObj = {
                    map: self.map,
                    draggable: this.markerDraggable,
                    title: place.name,
                    animation: google.maps.Animation.DROP,
                    position: place.geometry.location
                };
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                self.marker = new google.maps.Marker(markerObj);
                // self.markers.push(self.marker);
                self.addDragendListener();
                self.searchEvent.emit(new Map(markerObj.position.lat(), markerObj.position.lng()));
                self.cordinate.latitude = place.geometry.location.lat();
                self.cordinate.longtitude = place.geometry.location.lng();
                self.cordinate.title = place.formatted_address;
                self.triggerUpdateData(mapConfig.mapEventNames.search);
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                }
                else {
                    bounds.extend(place.geometry.location);
                }
            });
            self.map.fitBounds(bounds);
            self.map.setZoom(12);
            self.map.panTo(self.marker.position);
        });
    }
    addClickListener() {
        google.maps.event.addListener(this.map, 'click', (event) => {
            //event.preventDefault();
            this.geocodePosition(event.latLng, mapConfig.mapEventNames.mapClick, () => {
                this.marker.setMap(null);
                this.marker = new google.maps.Marker({
                    position: event.latLng,
                    map: this.map,
                    draggable: this.markerDraggable,
                    title: this.cordinate.title
                });
                this.addDragendListener();
            });
        });
    }
    addDragendListener() {
        google.maps.event.addListener(this.marker, 'dragend', () => {
            event.preventDefault();
            this.deleteMarkers();
            this.geocodePosition(this.marker.getPosition(), mapConfig.mapEventNames.markerDrag);
            this.cordinate.latitude = this.marker.getPosition().lat();
            this.cordinate.longtitude = this.marker.getPosition().lng();
        });
    }
    triggerUpdateData(eventName) {
        this.cordinate.eventName = eventName;
        this.cordinateChange.emit(this.cordinate);
    }
    myCurrentLocation() {
        let self = this;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (location) {
                if (self.marker) {
                    self.marker.setMap(null);
                }
                // self.deleteMarkers();
                self.cordinate.latitude = location.coords.latitude;
                self.cordinate.longtitude = location.coords.longitude;
                self.myLatLng = {
                    lat: location.coords.latitude, lng: location.coords.longitude
                };
                if (self.myLocation) {
                    self.myLocation.setMap(null);
                }
                self.myLocation = new google.maps.Marker({
                    position: self.myLatLng,
                    icon: {
                        url: self.greenPinIcon,
                        scaledSize: new google.maps.Size(30, 30),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(15, 15) // anchor
                    },
                    zIndex: 1000,
                    map: self.map,
                    draggable: false,
                    title: self.cordinate.title
                });
                self.geocodePosition(self.myLocation.getPosition(), mapConfig.mapEventNames.myLocation);
                //self.addDragendListener();
                self.getCurrentLocationEvent.emit(new Map(location.coords.latitude, location.coords.longitude));
                let currentZoomLevel = self.map.getZoom();
                let bounds = new google.maps.LatLngBounds();
                self.map.setCenter(new google.maps.LatLng(self.myLatLng.lat, self.myLatLng.lng));
            });
        }
        else {
            console.log("Geolocation is not supported by this browser.");
        }
    }
    getRadius(distance = 100) {
        if (this.myLocation) {
            let w = this.map.getBounds().getSouthWest();
            let centerLng = this.myLocation.getPosition();
            distance = google.maps.geometry.spherical.computeDistanceBetween(centerLng, w);
        }
        //this.mapRadius.emit(distance ? (distance / 1000).toFixed(1) : "CANNOT_CALCULATE_DISTANCE");
    }
    getTopDistanceFromMarker(lat, lng, eventName) {
        if (!lat || !lng) {
            return;
        }
        let myLatLng = new google.maps.LatLng(lat, lng);
        // Get map bounds
        let bounds = this.map.getBounds();
        // Find map top border & distance to center
        //let mapTop = new google.maps.LatLng(bounds.getNorthEast().lat(), myLatLng.lng());
        //let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, mapTop);
        // Find map left border & distance to center
        // let mapLeft = new google.maps.LatLng(myLatLng.lat(), bounds.getSouthWest().lng());
        // let distanceToLeft = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, mapLeft);
        // Get shortest length
        //let radius = distanceToTop < distanceToLeft ? distanceToTop : distanceToLeft;
        //let radius = distanceToTop;
        this.getTopCordinatorEvent.emit(new Map(bounds.getNorthEast().lat(), myLatLng.lng(), "", eventName));
        // let circle = new google.maps.Circle({
        //   strokeColor: '#FF0000',
        //   strokeOpacity: 0.8,
        //   strokeWeight: 2,
        //   fillColor: '#FF0000',
        //   fillOpacity: 0.35,
        //   editable: true,
        //   map: this.map,
        //   center: myLatLng,
        //   radius: radius
        // });
    }
    drawCircle(centerLat, centerLng, pointLat, pointLng, fillColor = "#FF0000") {
        let myLatLng = new google.maps.LatLng(centerLat, centerLng);
        // Get map bounds
        let bounds = this.map.getBounds();
        let point = new google.maps.LatLng(pointLat, pointLng);
        let distanceToTop = google.maps.geometry.spherical.computeDistanceBetween(myLatLng, point);
        let radius = distanceToTop;
        this.circle.setMap(null);
        this.circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: fillColor,
            fillOpacity: 0.35,
            editable: true,
            map: this.map,
            center: myLatLng,
            radius: radius
        });
    }
}
MapComponent.ɵfac = function MapComponent_Factory(t) { return new (t || MapComponent)(); };
MapComponent.ɵcmp = ɵɵdefineComponent({ type: MapComponent, selectors: [["via-map"]], inputs: { cordinate: "cordinate", searchPlaceholer: "searchPlaceholer", markerDraggable: "markerDraggable", customIcon: "customIcon", showMyLocation: "showMyLocation", hideSearchTextBox: "hideSearchTextBox" }, outputs: { cordinateChange: "cordinateChange", markerClick: "markerClick", mapRadius: "mapRadius", mapDragged: "mapDragged", searchEvent: "searchEvent", getTopCordinatorEvent: "getTopCordinatorEvent", getCurrentLocationEvent: "getCurrentLocationEvent" }, decls: 5, vars: 2, consts: [[1, "row"], ["class", "col-sm-12 map-search-container", 4, "ngIf"], [1, "col-sm-12"], ["id", "googleMap", 1, "google-maps"], ["class", "my-location-btn", 3, "click", 4, "ngIf"], [1, "col-sm-12", "map-search-container"], [1, "form-group"], ["id", "via-map-search"], ["id", "dummy-col"], ["type", "text", "id", "pac-input", "aria-describedby", "textHelp", "placeholder", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["src", "/assets/icons/ic_search.svg", 1, "input-search-icon"], [1, "my-location-btn", 3, "click"], ["src", "assets/icons/green_pin.svg"]], template: function MapComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "div", 0);
        ɵɵtemplate(1, MapComponent_div_1_Template, 6, 1, "div", 1);
        ɵɵelementStart(2, "div", 2);
        ɵɵelement(3, "div", 3);
        ɵɵtemplate(4, MapComponent_button_4_Template, 2, 0, "button", 4);
        ɵɵelementEnd();
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵadvance(1);
        ɵɵproperty("ngIf", !ctx.hideSearchTextBox);
        ɵɵadvance(3);
        ɵɵproperty("ngIf", ctx.showMyLocation);
    } }, directives: [NgIf, DefaultValueAccessor, NgControlStatus, NgModel], styles: [".google-maps[_ngcontent-%COMP%]{border-radius:3px;height:300px}.my-location-btn[_ngcontent-%COMP%]{background:#fff;border-radius:2px;border-style:none;bottom:110px;box-shadow:0 1px 4px -1px rgba(0,0,0,.3);cursor:pointer;height:41px;padding:0!important;position:absolute;right:25px;text-align:center;width:41px}.my-location-btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:20px}.map-search-container[_ngcontent-%COMP%]{margin-bottom:10px}.map-search-container[_ngcontent-%COMP%]   #pac-input[_ngcontent-%COMP%]{padding:10px}.map-search-container[_ngcontent-%COMP%]   .input-search-icon[_ngcontent-%COMP%]{position:absolute;right:21px;top:13px;width:20px}"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(MapComponent, [{
        type: Component,
        args: [{
                selector: 'via-map',
                templateUrl: './map.html',
                styleUrls: ['./map.scss']
            }]
    }], function () { return []; }, { cordinate: [{
            type: Input
        }], searchPlaceholer: [{
            type: Input
        }], markerDraggable: [{
            type: Input
        }], customIcon: [{
            type: Input
        }], showMyLocation: [{
            type: Input
        }], hideSearchTextBox: [{
            type: Input
        }], cordinateChange: [{
            type: Output
        }], markerClick: [{
            type: Output
        }], mapRadius: [{
            type: Output
        }], mapDragged: [{
            type: Output
        }], searchEvent: [{
            type: Output
        }], getTopCordinatorEvent: [{
            type: Output
        }], getCurrentLocationEvent: [{
            type: Output
        }] }); })();

function myLibInit(apiKey) {
    var x = (config) => () => config.load(apiKey);
    return x;
}
class MapModule {
}
MapModule.ɵmod = ɵɵdefineNgModule({ type: MapModule });
MapModule.ɵinj = ɵɵdefineInjector({ factory: function MapModule_Factory(t) { return new (t || MapModule)(); }, imports: [[FormsModule, CommonModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(MapModule, { declarations: [MapComponent], imports: [FormsModule, CommonModule], exports: [MapComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(MapModule, [{
        type: NgModule,
        args: [{
                declarations: [MapComponent],
                imports: [FormsModule, CommonModule],
                exports: [MapComponent]
            }]
    }], null, null); })();

class TypeaheadOptions {
    constructor(url = "", multiple = true, inlineSingleSearch = false) {
        this._url = url;
        this._multiple = multiple;
        this._inlineSingleSearch = inlineSingleSearch;
        this._debounceTime = 400;
        this._itemsSelected = new Array();
        this._items = new Array();
        this._errorMessage = "";
        this._isLoading = false;
        this._minLengthToLoad = 1;
        this._noDataFound = false;
    }
    /**
     * Getter url
     * return {string}
     */
    get url() {
        return this._url;
    }
    /**
     * Getter multiple
     * return {boolean}
     */
    get multiple() {
        return this._multiple;
    }
    /**
     * Getter inlineSingleSearch
     * return {boolean}
     */
    get inlineSingleSearch() {
        return this._inlineSingleSearch;
    }
    /**
     * Getter debounceTime
     * return {number}
     */
    get debounceTime() {
        return this._debounceTime;
    }
    /**
     * Getter itemsSelected
     * return {TypeaheadItem[]}
     */
    get itemsSelected() {
        return this._itemsSelected;
    }
    /**
     * Getter items
     * return {TypeaheadItem[]}
     */
    get items() {
        return this._items;
    }
    /**
     * Getter errorMessage
     * return {string}
     */
    get errorMessage() {
        return this._errorMessage;
    }
    /**
     * Getter isLoading
     * return {boolean}
     */
    get isLoading() {
        return this._isLoading;
    }
    /**
     * Getter minLengthToLoad
     * return {number}
     */
    get minLengthToLoad() {
        return this._minLengthToLoad;
    }
    /**
     * Getter noDataFound
     * return {boolean}
     */
    get noDataFound() {
        return this._noDataFound;
    }
    /**
     * Setter url
     * param {string} value
     */
    set url(value) {
        this._url = value;
    }
    /**
     * Setter multiple
     * param {boolean} value
     */
    set multiple(value) {
        this._multiple = value;
    }
    /**
     * Setter inlineSingleSearch
     * param {boolean} value
     */
    set inlineSingleSearch(value) {
        this._inlineSingleSearch = value;
    }
    /**
     * Setter debounceTime
     * param {number} value
     */
    set debounceTime(value) {
        this._debounceTime = value;
    }
    /**
     * Setter itemsSelected
     * param {TypeaheadItem[]} value
     */
    set itemsSelected(value) {
        this._itemsSelected = value;
    }
    /**
     * Setter items
     * param {TypeaheadItem[]} value
     */
    set items(value) {
        this._items = value;
    }
    /**
     * Setter errorMessage
     * param {string} value
     */
    set errorMessage(value) {
        this._errorMessage = value;
    }
    /**
     * Setter isLoading
     * param {boolean} value
     */
    set isLoading(value) {
        this._isLoading = value;
    }
    /**
     * Setter minLengthToLoad
     * param {number} value
     */
    set minLengthToLoad(value) {
        this._minLengthToLoad = value;
    }
    /**
     * Setter noDataFound
     * param {boolean} value
     */
    set noDataFound(value) {
        this._noDataFound = value;
    }
}

const languageObj = {
    enter_text: "Please enter the text",
    no_data_found: "No items to select"
};

class TypeaheadItem {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}

class ClickOutSiteDirective {
    constructor(el) {
        this.el = el;
        this.tohClickOutSite = new EventEmitter();
        this.clickOutSite = new EventEmitter();
    }
    documentClick($event) {
        if (this.tohClickOutSite.observers.length > 0) {
            if (this.el.nativeElement.innerHTML.indexOf($event.target.innerHTML) === -1) {
                this.tohClickOutSite.emit(null);
            }
        }
        else if (this.clickOutSite.observers.length > 0) {
            if (!this.el.nativeElement.contains($event.target)) {
                this.clickOutSite.emit(null);
            }
        }
    }
}
ClickOutSiteDirective.ɵfac = function ClickOutSiteDirective_Factory(t) { return new (t || ClickOutSiteDirective)(ɵɵdirectiveInject(ElementRef)); };
ClickOutSiteDirective.ɵdir = ɵɵdefineDirective({ type: ClickOutSiteDirective, selectors: [["", "tohClickOutSite", ""]], hostBindings: function ClickOutSiteDirective_HostBindings(rf, ctx) { if (rf & 1) {
        ɵɵlistener("click", function ClickOutSiteDirective_click_HostBindingHandler($event) { return ctx.documentClick($event); }, false, ɵɵresolveDocument);
    } }, outputs: { tohClickOutSite: "tohClickOutSite", clickOutSite: "clickOutSite" } });
/*@__PURE__*/ (function () { ɵsetClassMetadata(ClickOutSiteDirective, [{
        type: Directive,
        args: [{
                selector: "[tohClickOutSite]"
            }]
    }], function () { return [{ type: ElementRef }]; }, { tohClickOutSite: [{
            type: Output
        }], clickOutSite: [{
            type: Output
        }], documentClick: [{
            type: HostListener,
            args: ["document:click", ["$event"]]
        }] }); })();

function TypeaheadComponent_span_4_li_1_span_3_Template(rf, ctx) { if (rf & 1) {
    const _r11 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span", 14);
    ɵɵlistener("click", function TypeaheadComponent_span_4_li_1_span_3_Template_span_click_0_listener() { ɵɵrestoreView(_r11); const item_r7 = ɵɵnextContext().$implicit; const ctx_r9 = ɵɵnextContext(2); return ctx_r9.remove(item_r7); });
    ɵɵelement(1, "i", 15);
    ɵɵelementEnd();
} }
function TypeaheadComponent_span_4_li_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "li", 12);
    ɵɵelementStart(1, "span", 13);
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵtemplate(3, TypeaheadComponent_span_4_li_1_span_3_Template, 2, 0, "span", 9);
    ɵɵelementEnd();
} if (rf & 2) {
    const item_r7 = ctx.$implicit;
    const ctx_r6 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵpropertyInterpolate("title", item_r7.name);
    ɵɵadvance(1);
    ɵɵtextInterpolate(item_r7.name);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r6.disabled);
} }
function TypeaheadComponent_span_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtemplate(1, TypeaheadComponent_span_4_li_1_Template, 4, 3, "li", 11);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r0.options.itemsSelected);
} }
function TypeaheadComponent_span_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 16);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r1.options.itemsSelected[0].name);
} }
function TypeaheadComponent_input_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "input", 17);
} if (rf & 2) {
    const ctx_r2 = ɵɵnextContext();
    ɵɵpropertyInterpolate("placeholder", ctx_r2.languageObj.enter_text);
    ɵɵproperty("formControl", ctx_r2.searchBox);
} }
function TypeaheadComponent_span_8_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 18);
    ɵɵelement(1, "i", 19);
    ɵɵelementEnd();
} }
function TypeaheadComponent_span_9_Template(rf, ctx) { if (rf & 1) {
    const _r13 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span", 14);
    ɵɵlistener("click", function TypeaheadComponent_span_9_Template_span_click_0_listener() { ɵɵrestoreView(_r13); const ctx_r12 = ɵɵnextContext(); return ctx_r12.removeSingleItem(); });
    ɵɵelement(1, "i", 15);
    ɵɵelementEnd();
} }
function TypeaheadComponent_ul_10_li_1_Template(rf, ctx) { if (rf & 1) {
    const _r18 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "li", 23);
    ɵɵlistener("click", function TypeaheadComponent_ul_10_li_1_Template_li_click_0_listener() { ɵɵrestoreView(_r18); const item_r16 = ctx.$implicit; const ctx_r17 = ɵɵnextContext(2); return ctx_r17.select(item_r16); });
    ɵɵelementStart(1, "span");
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const item_r16 = ctx.$implicit;
    ɵɵadvance(2);
    ɵɵtextInterpolate(item_r16.name);
} }
function TypeaheadComponent_ul_10_li_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "li", 24);
    ɵɵelementStart(1, "span");
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r15 = ɵɵnextContext(2);
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r15.languageObj.no_data_found);
} }
function TypeaheadComponent_ul_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "ul", 20);
    ɵɵtemplate(1, TypeaheadComponent_ul_10_li_1_Template, 3, 1, "li", 21);
    ɵɵtemplate(2, TypeaheadComponent_ul_10_li_2_Template, 3, 1, "li", 22);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r5.options.items);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r5.options.noDataFound);
} }
class TypeaheadComponent {
    constructor() {
        this.languageObj = languageObj;
        this.disabled = false;
        this.typeaheadLoadData = new EventEmitter();
        this.typeaheadItemSelected = new EventEmitter();
        this.searchBox = new FormControl();
        //prevent duplicate search when set value for search box
        this.isSettingValue = false;
        //super();
    }
    hide() {
        this.options.items = [];
        this.options.errorMessage = "";
        this.options.isLoading = false;
        this.options.noDataFound = false;
        if (this.options.inlineSingleSearch == false) {
            //this.isSettingValue = true;
            this.searchBox.setValue("");
        }
        else if (this.isSettingValue == false && this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            //this.isSettingValue = true;
            //this.searchBox.setValue(this.options.itemsSelected[0].name);
        }
    }
    isSingleInlineSearch() {
        return this.options.inlineSingleSearch == true && this.options.multiple == false;
    }
    select(item) {
        this.options.items = this.options.items.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        if (!this.options.multiple) {
            this.options.itemsSelected = [];
        }
        this.options.itemsSelected.push(item);
        this.options.noDataFound = this.options.items.length === 0;
        this.typeaheadItemSelected.emit(item);
        if (this.isSingleInlineSearch() == true) {
            this.isSettingValue = true;
            this.searchBox.setValue(item.name);
        }
    }
    remove(item) {
        this.options.items.push(item);
        this.options.itemsSelected = this.options.itemsSelected.filter((filterItem) => {
            return filterItem.value !== item.value;
        });
        this.options.noDataFound = this.options.items.length === 0;
    }
    removeSingleItem() {
        this.isSettingValue = true;
        this.searchBox.setValue("");
        this.options.itemsSelected = [];
        this.typeaheadItemSelected.emit(new TypeaheadItem(undefined, undefined));
    }
    ngAfterViewInit() {
        if (this.isSingleInlineSearch() == true && this.options.itemsSelected.length) {
            this.searchBox.setValue(this.options.itemsSelected[0].name);
            this.isSettingValue = true;
        }
        this.searchBox.valueChanges.pipe(debounceTime(this.options.debounceTime), distinctUntilChanged()).subscribe((searchText) => {
            if (searchText.length >= this.options.minLengthToLoad && this.isSettingValue == false) {
                this.options.isLoading = true;
                this.typeaheadLoadData.emit(searchText);
            }
            this.isSettingValue = false;
        });
    }
    setSearchBoxValue(text) {
        this.searchBox.setValue(text);
        this.isSettingValue = true;
    }
}
TypeaheadComponent.ɵfac = function TypeaheadComponent_Factory(t) { return new (t || TypeaheadComponent)(); };
TypeaheadComponent.ɵcmp = ɵɵdefineComponent({ type: TypeaheadComponent, selectors: [["via-typeahead"]], inputs: { languageObj: "languageObj", options: "options", disabled: "disabled" }, outputs: { typeaheadLoadData: "typeaheadLoadData", typeaheadItemSelected: "typeaheadItemSelected" }, decls: 11, vars: 6, consts: [["id", "typeahead", 3, "tohClickOutSite"], [1, "typeahead-container"], [1, "typeahead"], [1, "typeahead-items-selected"], [4, "ngIf"], ["class", "inline-disabled", 4, "ngIf"], [1, "typeahead-menu-item-input"], ["autocomplete", "off", "class", "typeahead-input", 3, "placeholder", "formControl", 4, "ngIf"], ["class", "ic-spinner", 4, "ngIf"], ["class", "ic-remove", 3, "click", 4, "ngIf"], ["class", "typeahead-menu-items z-depth-1", 4, "ngIf"], ["class", "typeahead-menu-item-selected", 4, "ngFor", "ngForOf"], [1, "typeahead-menu-item-selected"], [1, "item-display", 3, "title"], [1, "ic-remove", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-times"], [1, "inline-disabled"], ["autocomplete", "off", 1, "typeahead-input", 3, "placeholder", "formControl"], [1, "ic-spinner"], ["id", "circle-o-notch-spin", "aria-hidden", "true", 1, "fa", "fa-circle-o-notch", "fa-spin"], [1, "typeahead-menu-items", "z-depth-1"], ["class", "typeahead-menu-item", 3, "click", 4, "ngFor", "ngForOf"], ["class", "typeahead-menu-item", 4, "ngIf"], [1, "typeahead-menu-item", 3, "click"], [1, "typeahead-menu-item"]], template: function TypeaheadComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "div", 0);
        ɵɵlistener("tohClickOutSite", function TypeaheadComponent_Template_div_tohClickOutSite_0_listener() { return ctx.hide(); });
        ɵɵelementStart(1, "ul", 1);
        ɵɵelementStart(2, "li", 2);
        ɵɵelementStart(3, "ul", 3);
        ɵɵtemplate(4, TypeaheadComponent_span_4_Template, 2, 1, "span", 4);
        ɵɵtemplate(5, TypeaheadComponent_span_5_Template, 2, 1, "span", 5);
        ɵɵelementStart(6, "li", 6);
        ɵɵtemplate(7, TypeaheadComponent_input_7_Template, 1, 2, "input", 7);
        ɵɵtemplate(8, TypeaheadComponent_span_8_Template, 2, 0, "span", 8);
        ɵɵtemplate(9, TypeaheadComponent_span_9_Template, 2, 0, "span", 9);
        ɵɵtemplate(10, TypeaheadComponent_ul_10_Template, 3, 2, "ul", 10);
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵadvance(4);
        ɵɵproperty("ngIf", !ctx.options.inlineSingleSearch);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.options.inlineSingleSearch && ctx.disabled == true && ctx.options.itemsSelected.length);
        ɵɵadvance(2);
        ɵɵproperty("ngIf", ctx.disabled == false);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.options.isLoading);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.isSingleInlineSearch() && ctx.options.itemsSelected.length && ctx.options.isLoading == false && !ctx.disabled);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.options.items.length > 0 || ctx.options.noDataFound);
    } }, directives: [ClickOutSiteDirective, NgIf, NgForOf, DefaultValueAccessor, NgControlStatus, FormControlDirective], styles: ["#typeahead[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:row;width:100%}#typeahead[_ngcontent-%COMP%]   .inline-disabled[_ngcontent-%COMP%]{padding-left:5px;padding-top:5px}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:1px 5px;z-index:1}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]{margin:0;width:100%}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;list-style:none;margin:5px 0;padding:0}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]{background:#999;border:1px solid #ebebeb;border-radius:3px;border-radius:5px;color:#fff;display:flex;flex-direction:row;margin:0 10px 0 0;max-width:200px;min-width:120px;padding:5px}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]   .item-display[_ngcontent-%COMP%]{flex:9;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:100%}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-selected[_ngcontent-%COMP%]   .ic-remove[_ngcontent-%COMP%]{flex:1;font-size:14px;text-align:right}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]{align-items:center;border:0;color:#999;display:flex;flex:1;height:34px;margin:0;width:auto}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]   .typeahead-input[_ngcontent-%COMP%]{border:0;flex:9}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-items-selected[_ngcontent-%COMP%]   .typeahead-menu-item-input[_ngcontent-%COMP%]   .ic-spinner[_ngcontent-%COMP%]{flex:1;text-align:right}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:flex;flex-direction:column;left:15px;list-style:none;margin:0;max-height:300px;overflow:auto;padding:0;position:absolute;top:90%;width:calc(100% - 30px)}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]   .typeahead-menu-item[_ngcontent-%COMP%]{margin:0;padding:10px}#typeahead[_ngcontent-%COMP%]   .typeahead-container[_ngcontent-%COMP%]   .typeahead[_ngcontent-%COMP%]   .typeahead-menu-items[_ngcontent-%COMP%]   .typeahead-menu-item[_ngcontent-%COMP%]:hover{background:#f7f8f9}#typeahead[_ngcontent-%COMP%]   .typeahead-arrow[_ngcontent-%COMP%]{flex:1;text-align:right}#typeahead[_ngcontent-%COMP%]   .typeahead-arrow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#999;font-size:24px}"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(TypeaheadComponent, [{
        type: Component,
        args: [{
                selector: "via-typeahead",
                templateUrl: "./typeahead.html",
                styleUrls: ["./typeahead.scss"]
            }]
    }], function () { return []; }, { languageObj: [{
            type: Input
        }], options: [{
            type: Input
        }], disabled: [{
            type: Input
        }], typeaheadLoadData: [{
            type: Output
        }], typeaheadItemSelected: [{
            type: Output
        }] }); })();

class ClickOutSiteModule {
}
ClickOutSiteModule.ɵmod = ɵɵdefineNgModule({ type: ClickOutSiteModule });
ClickOutSiteModule.ɵinj = ɵɵdefineInjector({ factory: function ClickOutSiteModule_Factory(t) { return new (t || ClickOutSiteModule)(); }, imports: [[]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(ClickOutSiteModule, { declarations: [ClickOutSiteDirective], exports: [ClickOutSiteDirective] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(ClickOutSiteModule, [{
        type: NgModule,
        args: [{
                imports: [],
                declarations: [ClickOutSiteDirective],
                exports: [ClickOutSiteDirective]
            }]
    }], null, null); })();

class TypeaheadModule {
}
TypeaheadModule.ɵmod = ɵɵdefineNgModule({ type: TypeaheadModule });
TypeaheadModule.ɵinj = ɵɵdefineInjector({ factory: function TypeaheadModule_Factory(t) { return new (t || TypeaheadModule)(); }, imports: [[
            NgbModule,
            ClickOutSiteModule,
            CommonModule,
            FormsModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(TypeaheadModule, { declarations: [TypeaheadComponent], imports: [NgbModule,
        ClickOutSiteModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule], exports: [TypeaheadComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(TypeaheadModule, [{
        type: NgModule,
        args: [{
                declarations: [TypeaheadComponent],
                imports: [
                    NgbModule,
                    ClickOutSiteModule,
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                exports: [TypeaheadComponent]
            }]
    }], null, null); })();

class TypeaheadItemValue extends TypeaheadItem {
    constructor(name, value, data) {
        super(name, value);
        this.data = data;
    }
}

const _c0 = ["custom-toast-component", ""];
function CustomToast_div_6_p_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "p", 6);
} if (rf & 2) {
    const item_r1 = ɵɵnextContext().$implicit;
    ɵɵproperty("innerHtml", item_r1, ɵɵsanitizeHtml);
} }
function CustomToast_div_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div");
    ɵɵtemplate(1, CustomToast_div_6_p_1_Template, 1, 1, "p", 5);
    ɵɵelementEnd();
} if (rf & 2) {
    const item_r1 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngIf", item_r1 != "undefined");
} }
class CustomToast extends Toast {
    constructor(toastrService, toastPackage) {
        super(toastrService, toastPackage);
        this.toastrService = toastrService;
        this.toastPackage = toastPackage;
        this.listMessages = this.message.toString().split("||");
    }
}
CustomToast.ɵfac = function CustomToast_Factory(t) { return new (t || CustomToast)(ɵɵdirectiveInject(ToastrService), ɵɵdirectiveInject(ToastPackage)); };
CustomToast.ɵcmp = ɵɵdefineComponent({ type: CustomToast, selectors: [["", "custom-toast-component", ""]], features: [ɵɵInheritDefinitionFeature], attrs: _c0, decls: 7, vars: 2, consts: [[1, "win-alert"], [1, "fa", "fa-close", "action"], [1, "title"], [1, "detail"], [4, "ngFor", "ngForOf"], [3, "innerHtml", 4, "ngIf"], [3, "innerHtml"]], template: function CustomToast_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "div", 0);
        ɵɵelement(1, "i", 1);
        ɵɵelementStart(2, "div", 2);
        ɵɵelementStart(3, "span");
        ɵɵtext(4);
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(5, "div", 3);
        ɵɵtemplate(6, CustomToast_div_6_Template, 2, 1, "div", 4);
        ɵɵelementEnd();
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵadvance(4);
        ɵɵtextInterpolate(ctx.title);
        ɵɵadvance(2);
        ɵɵproperty("ngForOf", ctx.listMessages);
    } }, directives: [NgForOf, NgIf], encapsulation: 2, data: { animation: [
            trigger('flyInOut', []),
        ] } });
/*@__PURE__*/ (function () { ɵsetClassMetadata(CustomToast, [{
        type: Component,
        args: [{
                selector: '[custom-toast-component]',
                templateUrl: "toast.html",
                animations: [
                    trigger('flyInOut', []),
                ],
                preserveWhitespaces: false
            }]
    }], function () { return [{ type: ToastrService }, { type: ToastPackage }]; }, null); })();

class ToastModule {
}
ToastModule.ɵmod = ɵɵdefineNgModule({ type: ToastModule });
ToastModule.ɵinj = ɵɵdefineInjector({ factory: function ToastModule_Factory(t) { return new (t || ToastModule)(); }, imports: [[
            FormsModule,
            ReactiveFormsModule,
            CommonModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(ToastModule, { declarations: [CustomToast], imports: [FormsModule,
        ReactiveFormsModule,
        CommonModule], exports: [CustomToast] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(ToastModule, [{
        type: NgModule,
        args: [{
                declarations: [CustomToast],
                imports: [
                    FormsModule,
                    ReactiveFormsModule,
                    CommonModule
                ],
                exports: [CustomToast]
            }]
    }], null, null); })();

class Modal {
    constructor(modalTitle, yesBtnTitle, noBtnTitle = "close", type, translateParams, modalContent, modalLogo) {
        this.modalTitle = modalTitle;
        this.modalContent = modalContent;
        this.yesBtnTitle = yesBtnTitle;
        this.noBtnTitle = noBtnTitle;
        this.type = type;
        this.translateParams = translateParams;
        this.modalLogo = modalLogo;
    }
}

const modalConfigs = {
    alert: "ALERT",
    confirm: "CONFIRM"
};

function WinModalComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 8);
    ɵɵelement(1, "img", 9);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("src", ctx_r0.modalOptions.modalLogo, ɵɵsanitizeUrl);
} }
function WinModalComponent_button_9_Template(rf, ctx) { if (rf & 1) {
    const _r3 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 10);
    ɵɵlistener("click", function WinModalComponent_button_9_Template_button_click_0_listener() { ɵɵrestoreView(_r3); const ctx_r2 = ɵɵnextContext(); return ctx_r2.close(); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r1.modalOptions.noBtnTitle, " ");
} }
class WinModalComponent {
    constructor(activeModal) {
        this.activeModal = activeModal;
        this.onConfirm = new EventEmitter();
        this.modalTypes = modalConfigs;
        //super();
    }
    close() {
        this.activeModal.dismiss("close");
    }
    ok() {
        this.activeModal.close("ok");
    }
    handleCloseByIcon() {
        if (this.modalOptions.type === modalConfigs.confirm) {
            this.close();
        }
        else {
            this.ok();
        }
    }
}
WinModalComponent.ɵfac = function WinModalComponent_Factory(t) { return new (t || WinModalComponent)(ɵɵdirectiveInject(NgbActiveModal)); };
WinModalComponent.ɵcmp = ɵɵdefineComponent({ type: WinModalComponent, selectors: [["via-modal"]], inputs: { modalOptions: "modalOptions" }, outputs: { onConfirm: "onConfirm" }, decls: 10, vars: 5, consts: [["class", "center-text m-t-20", 4, "ngIf"], [1, "center-text"], [1, "modal-title"], [1, "modal-body", "modal-body-text-p"], [3, "innerHtml"], [1, "modal-footer"], ["type", "button", 1, "btn", "uppercase-text", "btn-primary", "modal-action-btn", 3, "click"], ["type", "button", "class", "btn uppercase-text btn-light modal-action-btn", 3, "click", 4, "ngIf"], [1, "center-text", "m-t-20"], [2, "margin", "0px auto", 3, "src"], ["type", "button", 1, "btn", "uppercase-text", "btn-light", "modal-action-btn", 3, "click"]], template: function WinModalComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵtemplate(0, WinModalComponent_div_0_Template, 2, 1, "div", 0);
        ɵɵelementStart(1, "div", 1);
        ɵɵelementStart(2, "h4", 2);
        ɵɵtext(3);
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(4, "div", 3);
        ɵɵelement(5, "p", 4);
        ɵɵelementEnd();
        ɵɵelementStart(6, "div", 5);
        ɵɵelementStart(7, "button", 6);
        ɵɵlistener("click", function WinModalComponent_Template_button_click_7_listener() { return ctx.ok(); });
        ɵɵtext(8);
        ɵɵelementEnd();
        ɵɵtemplate(9, WinModalComponent_button_9_Template, 2, 1, "button", 7);
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵproperty("ngIf", ctx.modalOptions.modalLogo);
        ɵɵadvance(3);
        ɵɵtextInterpolate(ctx.modalOptions.modalTitle);
        ɵɵadvance(2);
        ɵɵproperty("innerHtml", ctx.modalOptions.modalContent, ɵɵsanitizeHtml);
        ɵɵadvance(3);
        ɵɵtextInterpolate1(" ", ctx.modalOptions.yesBtnTitle, " ");
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.modalOptions.type === ctx.modalTypes.confirm);
    } }, directives: [NgIf], encapsulation: 2 });
/*@__PURE__*/ (function () { ɵsetClassMetadata(WinModalComponent, [{
        type: Component,
        args: [{
                selector: 'via-modal',
                templateUrl: './modal.html'
            }]
    }], function () { return [{ type: NgbActiveModal }]; }, { modalOptions: [{
            type: Input
        }], onConfirm: [{
            type: Output
        }] }); })();

class WinModalModule {
}
WinModalModule.ɵmod = ɵɵdefineNgModule({ type: WinModalModule });
WinModalModule.ɵinj = ɵɵdefineInjector({ factory: function WinModalModule_Factory(t) { return new (t || WinModalModule)(); }, imports: [[
            NgbModule,
            FormsModule,
            CommonModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(WinModalModule, { declarations: [WinModalComponent], imports: [NgbModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule], exports: [WinModalComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(WinModalModule, [{
        type: NgModule,
        args: [{
                declarations: [WinModalComponent],
                imports: [
                    NgbModule,
                    FormsModule,
                    CommonModule,
                    ReactiveFormsModule
                ],
                exports: [WinModalComponent]
            }]
    }], null, null); })();

const DATE_FORMAT$1 = "hh:mma dd/MM/yyyy";
const DATE_FORMAT_WITHOUT_TIME$1 = "dd/MM/yyyy";

const I18N_VALUES = {
    en: {
        weekdays: [
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa",
            "Su"
        ],
        months: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        ]
    },
    vi: {
        weekdays: [
            "T2",
            "T3",
            "T4",
            "T5",
            "T6",
            "T7",
            "CN"
        ],
        months: [
            "T1",
            "T2",
            "T3",
            "T4",
            "T5",
            "T6",
            "T7",
            "T8",
            "T9",
            "T10",
            "T11",
            "T12"
        ]
    }
};
// Define a service holding the language. You probably already have one if your app is i18ned.
class I18n {
    constructor() {
        this.language = "vi";
    }
}
I18n.ɵfac = function I18n_Factory(t) { return new (t || I18n)(); };
I18n.ɵprov = ɵɵdefineInjectable({ token: I18n, factory: I18n.ɵfac });
/*@__PURE__*/ (function () { ɵsetClassMetadata(I18n, [{
        type: Injectable
    }], null, null); })();
// Define custom service providing the months and weekdays translations
class CustomDatepickerI18n extends NgbDatepickerI18n {
    constructor(_i18n) {
        super();
        this._i18n = _i18n;
    }
    getWeekdayShortName(weekday) {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    getMonthShortName(month) {
        return I18N_VALUES[this._i18n.language].months[month - 1];
    }
    getMonthFullName(month) {
        return this.getMonthShortName(month);
    }
    getDayAriaLabel(date) {
        return `${date.day}-${date.month}-${date.year}`;
    }
}
CustomDatepickerI18n.ɵfac = function CustomDatepickerI18n_Factory(t) { return new (t || CustomDatepickerI18n)(ɵɵinject(I18n)); };
CustomDatepickerI18n.ɵprov = ɵɵdefineInjectable({ token: CustomDatepickerI18n, factory: CustomDatepickerI18n.ɵfac });
/*@__PURE__*/ (function () { ɵsetClassMetadata(CustomDatepickerI18n, [{
        type: Injectable
    }], function () { return [{ type: I18n }]; }, null); })();
class DatepickerComponent {
    constructor(_i18n, formatter, calendar) {
        this._i18n = _i18n;
        this.formatter = formatter;
        this.calendar = calendar;
        this.today = new Date();
        this.minDate = {
            day: 1,
            month: 1,
            year: this.today.getFullYear() - 100
        };
        this.disabled = false;
        this.datepickerItemChange = new EventEmitter();
        this.dateFormat = DATE_FORMAT_WITHOUT_TIME$1;
        //super();
    }
    ngOnInit() {
        this._i18n.language = "en";
    }
    setLanguage(language) {
        this._i18n.language = language;
        this.placeholder = this._i18n.language == "en" ? "dd/mm/yyyy" : "nn/tt/nnnn";
    }
    selectToday() {
        this.datepickerItem = { year: 2021, month: 6, day: 7 };
    }
    getDateSelected() {
        if (this.isValid(this.datepickerItem)) {
            return new Date(this.datepickerItem.year, this.datepickerItem.month - 1, this.datepickerItem.day);
        }
        return null;
    }
    dateSelect(date) {
        this.datepickerItemChange.emit(date);
    }
    isValid(obj) {
        return obj !== null && obj !== undefined;
    }
    validateInput(currentValue, input) {
        const parsed = this.formatter.parse(input);
        let value;
        if ((parsed && this.calendar.isValid(NgbDate.from(parsed))) || parsed == undefined || parsed == null) {
            value = NgbDate.from(parsed);
        }
        else {
            value = null;
        }
        this.dateSelect(value);
        return value;
    }
    format(datepickerItem) {
        return this.formatter.format(datepickerItem);
    }
    checkError(date) {
        const parsed = this.formatter.parse(date);
        let isValid = false;
        if (parsed && this.calendar.isValid(NgbDate.from(parsed))) {
            isValid = true;
        }
        return isValid;
    }
}
DatepickerComponent.ɵfac = function DatepickerComponent_Factory(t) { return new (t || DatepickerComponent)(ɵɵdirectiveInject(I18n), ɵɵdirectiveInject(NgbDateParserFormatter), ɵɵdirectiveInject(NgbCalendar)); };
DatepickerComponent.ɵcmp = ɵɵdefineComponent({ type: DatepickerComponent, selectors: [["via-datepicker"]], inputs: { datepickerItem: "datepickerItem", maxDate: "maxDate", minDate: "minDate", disabled: "disabled" }, outputs: { datepickerItemChange: "datepickerItemChange" }, decls: 5, vars: 5, consts: [["id", "win-datepicker", "tohClickOutSite", "", 3, "clickOutSite"], ["placeholder", "DD/MM/YYYY", "name", "datePickerIpt", "ngbDatepicker", "", 1, "form-control", 3, "disabled", "startDate", "value", "minDate", "maxDate", "input", "dateSelect"], ["datePickerIpt", "", "d", "ngbDatepicker"], ["src", "./assets/icons/ic_calendar.svg", 1, "date-icon", 3, "click"]], template: function DatepickerComponent_Template(rf, ctx) { if (rf & 1) {
        const _r2 = ɵɵgetCurrentView();
        ɵɵelementStart(0, "div", 0);
        ɵɵlistener("clickOutSite", function DatepickerComponent_Template_div_clickOutSite_0_listener() { ɵɵrestoreView(_r2); const _r1 = ɵɵreference(3); return _r1.close(); });
        ɵɵelementStart(1, "input", 1, 2);
        ɵɵlistener("input", function DatepickerComponent_Template_input_input_1_listener() { ɵɵrestoreView(_r2); const _r0 = ɵɵreference(2); return ctx.datepickerItem = ctx.validateInput(ctx.datepickerItem, _r0.value); })("dateSelect", function DatepickerComponent_Template_input_dateSelect_1_listener($event) { return ctx.dateSelect($event); });
        ɵɵelementEnd();
        ɵɵelementStart(4, "img", 3);
        ɵɵlistener("click", function DatepickerComponent_Template_img_click_4_listener() { ɵɵrestoreView(_r2); const _r1 = ɵɵreference(3); return _r1.toggle(); });
        ɵɵelementEnd();
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵadvance(1);
        ɵɵproperty("disabled", ctx.disabled)("startDate", ctx.datepickerItem)("value", ctx.format(ctx.datepickerItem))("minDate", ctx.minDate)("maxDate", ctx.maxDate);
    } }, directives: [NgbInputDatepicker], styles: ["#win-datepicker[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:column;position:relative;width:100%}#win-datepicker[_ngcontent-%COMP%]   .date-icon[_ngcontent-%COMP%]{position:absolute;right:10px;top:13px;width:20px}#win-datepicker[_ngcontent-%COMP%]   .disabled-background[_ngcontent-%COMP%]{background:#f7f8f9}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:1;list-style:none;margin:0;padding:5px 10px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]{align-items:center;display:flex;flex:1;justify-content:baseline}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{margin-right:5px;width:20px}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{flex:8}#win-datepicker[_ngcontent-%COMP%]   .datepicker-container[_ngcontent-%COMP%]   .datepicker[_ngcontent-%COMP%]   span.dropdown-arrow[_ngcontent-%COMP%]{flex:1;font-size:24px;text-align:right}#win-datepicker[_ngcontent-%COMP%]   .input-datepicker[_ngcontent-%COMP%]{border:0;font-size:0;height:0;line-height:0;margin:0;padding:0;visibility:hidden;width:100%}"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(DatepickerComponent, [{
        type: Component,
        args: [{
                selector: "via-datepicker",
                templateUrl: "./datepicker.html",
                styleUrls: ["./datepicker.scss"]
            }]
    }], function () { return [{ type: I18n }, { type: NgbDateParserFormatter }, { type: NgbCalendar }]; }, { datepickerItem: [{
            type: Input
        }], maxDate: [{
            type: Input
        }], minDate: [{
            type: Input
        }], disabled: [{
            type: Input
        }], datepickerItemChange: [{
            type: Output
        }] }); })();

class DatepickerModule {
}
DatepickerModule.ɵmod = ɵɵdefineNgModule({ type: DatepickerModule });
DatepickerModule.ɵinj = ɵɵdefineInjector({ factory: function DatepickerModule_Factory(t) { return new (t || DatepickerModule)(); }, providers: [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }], imports: [[
            NgbModule,
            FormsModule,
            CommonModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(DatepickerModule, { declarations: [DatepickerComponent], imports: [NgbModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule], exports: [DatepickerComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(DatepickerModule, [{
        type: NgModule,
        args: [{
                declarations: [DatepickerComponent],
                imports: [
                    NgbModule,
                    FormsModule,
                    CommonModule,
                    ReactiveFormsModule
                ],
                exports: [DatepickerComponent],
                providers: [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }]
            }]
    }], null, null); })();

/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
class CustomAdapter extends NgbDateAdapter {
    constructor() {
        super(...arguments);
        this.DELIMITER = '-';
    }
    fromModel(value) {
        if (value) {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }
    toModel(date) {
        return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
    }
}
CustomAdapter.ɵfac = function CustomAdapter_Factory(t) { return ɵCustomAdapter_BaseFactory(t || CustomAdapter); };
CustomAdapter.ɵprov = ɵɵdefineInjectable({ token: CustomAdapter, factory: CustomAdapter.ɵfac });
const ɵCustomAdapter_BaseFactory = /*@__PURE__*/ ɵɵgetInheritedFactory(CustomAdapter);
/*@__PURE__*/ (function () { ɵsetClassMetadata(CustomAdapter, [{
        type: Injectable
    }], null, null); })();
/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
class CustomDateParserFormatter extends NgbDateParserFormatter {
    constructor() {
        super(...arguments);
        this.DELIMITER = '/';
    }
    parse(value) {
        if (value) {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }
    format(date) {
        return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
    }
}
CustomDateParserFormatter.ɵfac = function CustomDateParserFormatter_Factory(t) { return ɵCustomDateParserFormatter_BaseFactory(t || CustomDateParserFormatter); };
CustomDateParserFormatter.ɵprov = ɵɵdefineInjectable({ token: CustomDateParserFormatter, factory: CustomDateParserFormatter.ɵfac });
const ɵCustomDateParserFormatter_BaseFactory = /*@__PURE__*/ ɵɵgetInheritedFactory(CustomDateParserFormatter);
/*@__PURE__*/ (function () { ɵsetClassMetadata(CustomDateParserFormatter, [{
        type: Injectable
    }], null, null); })();

class FullTextSearchPipe {
    constructor() { }
    transform(value, keys, term) {
        if (!term)
            return value;
        return (value || []).filter(item => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
    }
}
FullTextSearchPipe.ɵfac = function FullTextSearchPipe_Factory(t) { return new (t || FullTextSearchPipe)(); };
FullTextSearchPipe.ɵpipe = ɵɵdefinePipe({ name: "fullTextSearch", type: FullTextSearchPipe, pure: false });
/*@__PURE__*/ (function () { ɵsetClassMetadata(FullTextSearchPipe, [{
        type: Pipe,
        args: [{
                name: 'fullTextSearch',
                pure: false
            }]
    }], function () { return []; }, null); })();

function DropdownComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 12);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = ɵɵnextContext();
    ɵɵpropertyInterpolate("title", ctx_r0.getSelectedStr(ctx_r0.itemSelected));
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r0.getSelectedStr(ctx_r0.itemSelected), " ");
} }
function DropdownComponent_span_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r1.placeholder);
} }
function DropdownComponent_div_9_i_2_Template(rf, ctx) { if (rf & 1) {
    const _r7 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "i", 17);
    ɵɵlistener("click", function DropdownComponent_div_9_i_2_Template_i_click_0_listener() { ɵɵrestoreView(_r7); const ctx_r6 = ɵɵnextContext(2); return ctx_r6.clearSearchText(); });
    ɵɵelementEnd();
} }
function DropdownComponent_div_9_img_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "img", 18);
} }
function DropdownComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    const _r9 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 13);
    ɵɵelementStart(1, "input", 14);
    ɵɵlistener("ngModelChange", function DropdownComponent_div_9_Template_input_ngModelChange_1_listener($event) { ɵɵrestoreView(_r9); const ctx_r8 = ɵɵnextContext(); return ctx_r8.searchText = $event; });
    ɵɵelementEnd();
    ɵɵtemplate(2, DropdownComponent_div_9_i_2_Template, 1, 0, "i", 15);
    ɵɵtemplate(3, DropdownComponent_div_9_img_3_Template, 1, 0, "img", 16);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("ngModel", ctx_r2.searchText);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r2.searchText);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r2.searchText);
} }
const _c0$1 = function (a0) { return { "selected": a0 }; };
function DropdownComponent_li_10_Template(rf, ctx) { if (rf & 1) {
    const _r12 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "li", 19);
    ɵɵlistener("click", function DropdownComponent_li_10_Template_li_click_0_listener() { ɵɵrestoreView(_r12); const item_r10 = ctx.$implicit; const ctx_r11 = ɵɵnextContext(); return ctx_r11.selected(item_r10); });
    ɵɵelementStart(1, "span", 20);
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementStart(3, "span", 21);
    ɵɵelement(4, "i", 22);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const item_r10 = ctx.$implicit;
    const ctx_r3 = ɵɵnextContext();
    ɵɵproperty("ngClass", ɵɵpureFunction1(3, _c0$1, ctx_r3.hasSelected(item_r10)));
    ɵɵadvance(1);
    ɵɵpropertyInterpolate("title", item_r10.name);
    ɵɵadvance(1);
    ɵɵtextInterpolate(item_r10.name);
} }
const _c1 = function (a0) { return { "disabled-background": a0 }; };
const _c2 = function (a0) { return { "open": a0 }; };
const _c3 = function (a0) { return { "arrow-rotate": a0 }; };
class DropdownComponent {
    constructor() {
        this.items = new Array();
        this.itemSelected = new Array();
        this.multiple = false;
        this.disabled = false;
        this.placeholder = "";
        this.itemSelectedChange = new EventEmitter();
        this.isShow = false;
        //super();
    }
    isItemSelectedValid() {
        return this.itemSelected !== null && this.itemSelected !== undefined && this.itemSelected.length > 0;
    }
    toggle() {
        if (!this.disabled) {
            this.isShow = !this.isShow;
        }
    }
    hide() {
        this.isShow = false;
    }
    show() {
        this.isShow = true;
    }
    hasSelected(item) {
        let itemsFound = this.itemSelected.filter((i) => { return i.value === item.value; });
        return itemsFound.length > 0;
    }
    selected(item) {
        this.searchText = "";
        if (this.multiple) {
            let itemsFound = this.itemSelected.filter((i) => { return i.value === item.value; });
            if (itemsFound.length === 0) {
                this.itemSelected.push(item);
            }
            else {
                this.itemSelected = this.itemSelected.filter((i) => { return i.value !== item.value; });
            }
        }
        else {
            this.hide();
            this.itemSelected = [];
            this.itemSelected.push(item);
        }
        this.itemSelectedChange.emit(this.itemSelected);
    }
    clearSearchText() {
        this.searchText = "";
    }
    getSelectedStr(itemSelected) {
        let str = "";
        if (itemSelected) {
            let i = 0;
            itemSelected.forEach(item => {
                i = i + 1;
                str = str + item.name + (i < itemSelected.length ? ", " : "");
            });
        }
        return str;
    }
}
DropdownComponent.ɵfac = function DropdownComponent_Factory(t) { return new (t || DropdownComponent)(); };
DropdownComponent.ɵcmp = ɵɵdefineComponent({ type: DropdownComponent, selectors: [["via-dropdown"]], inputs: { items: "items", itemSelected: "itemSelected", multiple: "multiple", disabled: "disabled", placeholder: "placeholder" }, outputs: { itemSelectedChange: "itemSelectedChange" }, decls: 13, vars: 20, consts: [["id", "dropdown"], [1, "dropdown-container", 3, "ngClass"], [1, "dropdown", 3, "ngClass"], [1, "dropdown-selectedBox", 3, "click"], ["class", "text-box", 3, "title", 4, "ngIf"], [4, "ngIf"], [1, "dropdown-arrow"], ["aria-hidden", "true", 1, "fa", "fa-angle-down", "arrow", 3, "ngClass"], [1, "dropdown-menu-items", "z-depth-1"], ["class", "dropdown-search-bar", 4, "ngIf"], ["class", "dropdown-menu-item", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [1, "dropdown-backdrop", 3, "ngClass", "click"], [1, "text-box", 3, "title"], [1, "dropdown-search-bar"], [1, "dropdown-search-input", 3, "ngModel", "ngModelChange"], ["class", "fa fa-times clear-search-input", "aria-hidden", "true", 3, "click", 4, "ngIf"], ["class", "input-search-icon dropdown-search-icon", "src", "/assets/icons/ic_search.svg", 4, "ngIf"], ["aria-hidden", "true", 1, "fa", "fa-times", "clear-search-input", 3, "click"], ["src", "/assets/icons/ic_search.svg", 1, "input-search-icon", "dropdown-search-icon"], [1, "dropdown-menu-item", 3, "ngClass", "click"], [1, "text-no-wrap", 3, "title"], [1, "item-check"], ["aria-hidden", "true", 1, "fa", "fa-check"]], template: function DropdownComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "div", 0);
        ɵɵelementStart(1, "ul", 1);
        ɵɵelementStart(2, "li", 2);
        ɵɵelementStart(3, "div", 3);
        ɵɵlistener("click", function DropdownComponent_Template_div_click_3_listener() { return ctx.toggle(); });
        ɵɵtemplate(4, DropdownComponent_div_4_Template, 2, 2, "div", 4);
        ɵɵtemplate(5, DropdownComponent_span_5_Template, 2, 1, "span", 5);
        ɵɵelementStart(6, "span", 6);
        ɵɵelement(7, "i", 7);
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(8, "ul", 8);
        ɵɵtemplate(9, DropdownComponent_div_9_Template, 4, 3, "div", 9);
        ɵɵtemplate(10, DropdownComponent_li_10_Template, 5, 5, "li", 10);
        ɵɵpipe(11, "fullTextSearch");
        ɵɵelementEnd();
        ɵɵelementStart(12, "div", 11);
        ɵɵlistener("click", function DropdownComponent_Template_div_click_12_listener() { return ctx.hide(); });
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵadvance(1);
        ɵɵproperty("ngClass", ɵɵpureFunction1(12, _c1, ctx.disabled));
        ɵɵadvance(1);
        ɵɵproperty("ngClass", ɵɵpureFunction1(14, _c2, ctx.isShow));
        ɵɵadvance(2);
        ɵɵproperty("ngIf", ctx.isItemSelectedValid());
        ɵɵadvance(1);
        ɵɵproperty("ngIf", !ctx.isItemSelectedValid());
        ɵɵadvance(2);
        ɵɵproperty("ngClass", ɵɵpureFunction1(16, _c3, ctx.isShow));
        ɵɵadvance(2);
        ɵɵproperty("ngIf", ctx.items.length > 15);
        ɵɵadvance(1);
        ɵɵproperty("ngForOf", ɵɵpipeBind3(11, 8, ctx.items, "name", ctx.searchText));
        ɵɵadvance(2);
        ɵɵproperty("ngClass", ɵɵpureFunction1(18, _c2, ctx.isShow));
    } }, directives: [NgClass, NgIf, NgForOf, DefaultValueAccessor, NgControlStatus, NgModel], pipes: [FullTextSearchPipe], styles: ["#dropdown[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;cursor:pointer;display:flex;flex-direction:row;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]{position:-webkit-sticky;position:sticky;top:0}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]   .dropdown-search-input[_ngcontent-%COMP%]{border:.5px solid #dedede;border-left:0;border-right:0;border-top:0;font-size:13px;height:45px;padding-left:10px;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]   .clear-search-input[_ngcontent-%COMP%]{color:#d9e0e5;position:absolute;right:13px;top:13px}#dropdown[_ngcontent-%COMP%]   .dropdown-search-bar[_ngcontent-%COMP%]   .dropdown-search-icon[_ngcontent-%COMP%]{position:absolute;right:13px;top:13px;width:20px}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:9;list-style:none;margin:0;padding:0;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]{display:flex;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown.open[_ngcontent-%COMP%]   .dropdown-backdrop[_ngcontent-%COMP%], #dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown.open[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{display:block}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:none;list-style:none;margin:5px 0 38px;max-height:400px;overflow:auto;padding:0;position:absolute;top:100%;width:100%;z-index:4}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]{align-items:center;display:flex;flex:9;justify-content:flex-start;padding:10px}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]:hover{background:#f7f8f9}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item.selected[_ngcontent-%COMP%]   .item-check[_ngcontent-%COMP%]{color:#2a7af6;display:block}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]   .text-no-wrap[_ngcontent-%COMP%]{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}#dropdown[_ngcontent-%COMP%]   .dropdown-container[_ngcontent-%COMP%]   li.dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   .dropdown-menu-item[_ngcontent-%COMP%]   .item-check[_ngcontent-%COMP%]{color:#bcbec0;display:none;flex:1;text-align:right}#dropdown[_ngcontent-%COMP%]   .arrow-rotate[_ngcontent-%COMP%]{transform:rotate(-180deg)}#dropdown[_ngcontent-%COMP%]   .arrow[_ngcontent-%COMP%]{transition:.2s ease}#dropdown[_ngcontent-%COMP%]   .dropdown-selectedBox[_ngcontent-%COMP%]{padding:10px;width:100%}#dropdown[_ngcontent-%COMP%]   .dropdown-selectedBox[_ngcontent-%COMP%]   .text-box[_ngcontent-%COMP%]{overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:90%}#dropdown[_ngcontent-%COMP%]   .dropdown-arrow[_ngcontent-%COMP%]{flex:1;height:46px;line-height:46px;position:absolute;right:10px;text-align:center;top:2px}#dropdown[_ngcontent-%COMP%]   .dropdown-arrow[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#999;font-size:24px}#dropdown[_ngcontent-%COMP%]   .dropdown-backdrop[_ngcontent-%COMP%]{display:none;height:100%;left:0;position:fixed;top:0;width:100%;z-index:2}"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(DropdownComponent, [{
        type: Component,
        args: [{
                selector: "via-dropdown",
                templateUrl: "./dropdown.html",
                styleUrls: ["./dropdown.scss"]
            }]
    }], function () { return []; }, { items: [{
            type: Input
        }], itemSelected: [{
            type: Input
        }], multiple: [{
            type: Input
        }], disabled: [{
            type: Input
        }], placeholder: [{
            type: Input
        }], itemSelectedChange: [{
            type: Output
        }] }); })();

class DropdownItem {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}

class DropdownModule {
}
DropdownModule.ɵmod = ɵɵdefineNgModule({ type: DropdownModule });
DropdownModule.ɵinj = ɵɵdefineInjector({ factory: function DropdownModule_Factory(t) { return new (t || DropdownModule)(); }, imports: [[
            NgbModule,
            FormsModule,
            CommonModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(DropdownModule, { declarations: [DropdownComponent, FullTextSearchPipe], imports: [NgbModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule], exports: [DropdownComponent, FullTextSearchPipe] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(DropdownModule, [{
        type: NgModule,
        args: [{
                declarations: [DropdownComponent, FullTextSearchPipe],
                imports: [
                    NgbModule,
                    FormsModule,
                    CommonModule,
                    ReactiveFormsModule
                ],
                exports: [DropdownComponent, FullTextSearchPipe]
            }]
    }], null, null); })();

class FilterPipe {
    transform(columns) {
        return columns.filter((column) => {
            return !column.hidden;
        });
    }
}
FilterPipe.ɵfac = function FilterPipe_Factory(t) { return new (t || FilterPipe)(); };
FilterPipe.ɵpipe = ɵɵdefinePipe({ name: "filterColumn", type: FilterPipe, pure: true });
/*@__PURE__*/ (function () { ɵsetClassMetadata(FilterPipe, [{
        type: Pipe,
        args: [{ name: 'filterColumn' }]
    }], null, null); })();

const SORT = {
    ASC: "ASC",
    DESC: "DESC",
    NONE: ""
};

class TableCell {
    constructor(value = "", css = "", type = CELL_TYPE.STRING, routeLink = "", buttonActions = [], buttonDropdownActions = []) {
        this._value = value;
        this._css = css;
        this._type = type;
        this._routeLink = routeLink;
        this._buttonActions = buttonActions;
        this._buttonDropdownActions = buttonDropdownActions;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter type
     * return {string}
     */
    get type() {
        return this._type;
    }
    /**
     * Getter routeLink
     * return {string}
     */
    get routeLink() {
        return this._routeLink;
    }
    /**
     * Getter buttonActions
     * return {ButtonAction[]}
     */
    get buttonActions() {
        return this._buttonActions;
    }
    /**
     * Getter buttonDropdownActions
     * return {ButtonAction[]}
     */
    get buttonDropdownActions() {
        return this._buttonDropdownActions;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter type
     * param {string} value
     */
    set type(value) {
        this._type = value;
    }
    /**
     * Setter routeLink
     * param {string} value
     */
    set routeLink(value) {
        this._routeLink = value;
    }
    /**
     * Setter buttonActions
     * param {ButtonAction[]} value
     */
    set buttonActions(value) {
        this._buttonActions = value;
    }
    /**
     * Setter buttonDropdownActions
     * param {ButtonAction[]} value
     */
    set buttonDropdownActions(value) {
        this._buttonDropdownActions = value;
    }
}
const CELL_TYPE = {
    STRING: "STRING",
    DATE_TIME: "DATE_TIME",
    STATUS: "STATUS",
    ACTION: "ACTION",
    IMAGE_THUMBNAIL: "IMAGE_THUMBNAIL",
    INPUT: "INPUT"
};

const TABLE_LANGUAGE_CONFIG = {
    table_entries_per_page: "Entries per page",
    table_footer_showing: "Showing",
    table_page: "Page",
    filter_by: "Filter by",
    table_to: "to",
    table_of: "of",
    no_data_found: "No data found",
    table_elements: "elements",
    rts_dropdown_filter: "Filter",
    rts_reset: "Reset",
    rts_dropdown_search: "Search"
};

class TableFilter {
    constructor(hasFilter = true) {
        this._hasFilter = hasFilter;
    }
    get hasFilter() {
        return this._hasFilter;
    }
    set hasFilter(value) {
        this._hasFilter = value;
    }
}

class TableDefaultSort {
    constructor(sortColumnName = "", sortType = SORT.ASC) {
        this._sortColumnName = sortColumnName;
        this._sortType = sortType;
    }
    /**
     * Getter sortColumnName
     * return {string}
     */
    get sortColumnName() {
        return this._sortColumnName;
    }
    /**
     * Getter sortType
     * return {string}
     */
    get sortType() {
        return this._sortType;
    }
    /**
     * Setter sortColumnName
     * param {string} value
     */
    set sortColumnName(value) {
        this._sortColumnName = value;
    }
    /**
     * Setter sortType
     * param {string} value
     */
    set sortType(value) {
        this._sortType = value;
    }
}

class TableOption {
    constructor(tableColumns = [], tableRows = [], tableType = "RTS") {
        this._tableType = "RTS";
        this._tableColumns = tableColumns;
        this._tableRows = tableRows;
        this._totalItems = 0;
        this._itemsPerPage = 10;
        this._currentPage = 0;
        this._tableFilter = new TableFilter();
        this._tableActions = new Array();
        this._defaultSort = new TableDefaultSort();
        this._tableType = tableType;
    }
    /**
     * Getter tableColumns
     * return {TableColumn[]}
     */
    get tableColumns() {
        return this._tableColumns;
    }
    /**
     * Getter tableRows
     * return {TableRow[]}
     */
    get tableRows() {
        return this._tableRows;
    }
    /**
     * Getter totalItems
     * return {number}
     */
    get totalItems() {
        return this._totalItems;
    }
    /**
     * Getter itemsPerPage
     * return {number}
     */
    get itemsPerPage() {
        return this._itemsPerPage;
    }
    /**
     * Getter currentPage
     * return {number}
     */
    get currentPage() {
        return this._currentPage;
    }
    /**
     * Getter tableFilter
     * return {TableFilter}
     */
    get tableFilter() {
        return this._tableFilter;
    }
    /**
     * Getter tableActions
     * return {TableAction[]}
     */
    get tableActions() {
        return this._tableActions;
    }
    /**
     * Getter defaultSort
     * return {TableDefaultSort}
     */
    get defaultSort() {
        return this._defaultSort;
    }
    /**
     * Getter tableType
     * return {string }
     */
    get tableType() {
        return this._tableType;
    }
    /**
     * Setter tableColumns
     * param {TableColumn[]} value
     */
    set tableColumns(value) {
        this._tableColumns = value;
    }
    /**
     * Setter tableRows
     * param {TableRow[]} value
     */
    set tableRows(value) {
        this._tableRows = value;
    }
    /**
     * Setter totalItems
     * param {number} value
     */
    set totalItems(value) {
        this._totalItems = value;
    }
    /**
     * Setter itemsPerPage
     * param {number} value
     */
    set itemsPerPage(value) {
        this._itemsPerPage = value;
    }
    /**
     * Setter currentPage
     * param {number} value
     */
    set currentPage(value) {
        this._currentPage = value;
    }
    /**
     * Setter tableFilter
     * param {TableFilter} value
     */
    set tableFilter(value) {
        this._tableFilter = value;
    }
    /**
     * Setter tableActions
     * param {TableAction[]} value
     */
    set tableActions(value) {
        this._tableActions = value;
    }
    /**
     * Setter defaultSort
     * param {TableDefaultSort} value
     */
    set defaultSort(value) {
        this._defaultSort = value;
    }
    /**
     * Setter tableType
     * param {string } value
     */
    set tableType(value) {
        this._tableType = value;
    }
}
class EntryPerpage {
    constructor(name = "5", value = 5, selected = false) {
        this._name = name;
        this._value = value;
        this._selected = selected;
    }
    get selected() {
        return this._selected;
    }
    set selected(value) {
        this._selected = value;
    }
    get value() {
        return this._value;
    }
    set value(value) {
        this._value = value;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
}

function TableComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r11 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 19);
    ɵɵelementStart(1, "ul", 20);
    ɵɵelementStart(2, "li", 21);
    ɵɵelementStart(3, "span", 22);
    ɵɵelementStart(4, "img", 23);
    ɵɵlistener("click", function TableComponent_div_5_Template_img_click_4_listener() { ɵɵrestoreView(_r11); const ctx_r10 = ɵɵnextContext(); return ctx_r10.toggleFilter(); });
    ɵɵelementEnd();
    ɵɵelementStart(5, "span", 24);
    ɵɵlistener("click", function TableComponent_div_5_Template_span_click_5_listener() { ɵɵrestoreView(_r11); const ctx_r12 = ɵɵnextContext(); return ctx_r12.toggleFilter(); });
    ɵɵtext(6);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(7, "div", 25);
    ɵɵlistener("click", function TableComponent_div_5_Template_div_click_7_listener() { ɵɵrestoreView(_r11); const ctx_r13 = ɵɵnextContext(); return ctx_r13.hideFilter(); });
    ɵɵelementEnd();
    ɵɵelementStart(8, "ul", 26);
    ɵɵelementStart(9, "li");
    ɵɵprojection(10);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = ɵɵnextContext();
    ɵɵadvance(2);
    ɵɵproperty("ngClass", ctx_r0.isShowFilter ? "open" : "");
    ɵɵadvance(4);
    ɵɵtextInterpolate(ctx_r0.TablelanguageObj.filter_by);
} }
function TableComponent_div_7_span_1_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r19 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 28);
    ɵɵlistener("click", function TableComponent_div_7_span_1_button_1_Template_button_click_0_listener() { ɵɵrestoreView(_r19); const btn_r15 = ɵɵnextContext().$implicit; const ctx_r17 = ɵɵnextContext(2); return ctx_r17.doAction(btn_r15); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const btn_r15 = ɵɵnextContext().$implicit;
    ɵɵproperty("ngClass", btn_r15.css);
    ɵɵadvance(1);
    ɵɵtextInterpolate(btn_r15.name);
} }
function TableComponent_div_7_span_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtemplate(1, TableComponent_div_7_span_1_button_1_Template, 2, 2, "button", 27);
    ɵɵelementEnd();
} if (rf & 2) {
    const btn_r15 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !btn_r15.hided);
} }
function TableComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div");
    ɵɵtemplate(1, TableComponent_div_7_span_1_Template, 2, 1, "span", 16);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r1.tableOption.tableActions);
} }
function TableComponent_option_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "option", 29);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const entry_r21 = ctx.$implicit;
    ɵɵproperty("selected", entry_r21.selected);
    ɵɵattribute("value", entry_r21.value);
    ɵɵadvance(1);
    ɵɵtextInterpolate(entry_r21.name);
} }
function TableComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    const _r23 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 30);
    ɵɵelementStart(1, "span", 31);
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementStart(3, "ngb-pagination", 32);
    ɵɵlistener("pageChange", function TableComponent_div_16_Template_ngb_pagination_pageChange_3_listener($event) { ɵɵrestoreView(_r23); const ctx_r22 = ɵɵnextContext(); return ctx_r22.tableOption.currentPage = $event; })("pageChange", function TableComponent_div_16_Template_ngb_pagination_pageChange_3_listener() { ɵɵrestoreView(_r23); const ctx_r24 = ɵɵnextContext(); return ctx_r24.doPageChange(); });
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = ɵɵnextContext();
    ɵɵadvance(2);
    ɵɵtextInterpolate1("", ctx_r4.TablelanguageObj.table_page, " ");
    ɵɵadvance(1);
    ɵɵproperty("directionLinks", false)("collectionSize", ctx_r4.tableOption.totalItems)("rotate", true)("maxSize", 5)("pageSize", ctx_r4.tableOption.itemsPerPage)("page", ctx_r4.tableOption.currentPage);
} }
function TableComponent_thead_18_tr_1_th_1_i_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "i", 40);
} }
function TableComponent_thead_18_tr_1_th_1_i_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "i", 41);
} }
const _c0$2 = function (a0, a1, a2) { return [a0, a1, a2]; };
function TableComponent_thead_18_tr_1_th_1_Template(rf, ctx) { if (rf & 1) {
    const _r33 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "th", 36);
    ɵɵlistener("click", function TableComponent_thead_18_tr_1_th_1_Template_th_click_0_listener() { ɵɵrestoreView(_r33); const column_r29 = ctx.$implicit; const ctx_r32 = ɵɵnextContext(3); return ctx_r32.doSort(column_r29); });
    ɵɵelementStart(1, "div", 37);
    ɵɵelementStart(2, "span");
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵtemplate(4, TableComponent_thead_18_tr_1_th_1_i_4_Template, 1, 0, "i", 38);
    ɵɵtemplate(5, TableComponent_thead_18_tr_1_th_1_i_5_Template, 1, 0, "i", 39);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r29 = ctx.$implicit;
    const ctx_r27 = ɵɵnextContext(3);
    ɵɵproperty("ngClass", ɵɵpureFunction3(4, _c0$2, column_r29.sortable ? "sortable" : "", column_r29.sort, column_r29.css));
    ɵɵadvance(3);
    ɵɵtextInterpolate(column_r29.value);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r27.isSortAsc(column_r29));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r27.isSortDesc(column_r29));
} }
function TableComponent_thead_18_tr_1_th_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "th");
} }
function TableComponent_thead_18_tr_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "tr", 34);
    ɵɵtemplate(1, TableComponent_thead_18_tr_1_th_1_Template, 6, 8, "th", 35);
    ɵɵpipe(2, "filterColumn");
    ɵɵtemplate(3, TableComponent_thead_18_tr_1_th_3_Template, 1, 0, "th", 7);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r25 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ɵɵpipeBind1(2, 2, ctx_r25.tableOption.tableColumns));
    ɵɵadvance(2);
    ɵɵproperty("ngIf", ctx_r25.rowSortable);
} }
function TableComponent_thead_18_tr_2_th_1_i_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "i", 40);
} }
function TableComponent_thead_18_tr_2_th_1_i_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "i", 41);
} }
function TableComponent_thead_18_tr_2_th_1_div_6_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r45 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 51);
    ɵɵelementStart(1, "input", 52);
    ɵɵlistener("ngModelChange", function TableComponent_thead_18_tr_2_th_1_div_6_div_3_Template_input_ngModelChange_1_listener($event) { ɵɵrestoreView(_r45); const header_r42 = ctx.$implicit; return header_r42.checked = $event; });
    ɵɵelementEnd();
    ɵɵelementStart(2, "label", 53);
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const header_r42 = ctx.$implicit;
    const i_r43 = ctx.index;
    const columnIndex_r36 = ɵɵnextContext(2).index;
    ɵɵadvance(1);
    ɵɵpropertyInterpolate2("id", "styled-checkbox-", i_r43, "-", columnIndex_r36, "");
    ɵɵpropertyInterpolate("value", header_r42.value);
    ɵɵproperty("ngModel", header_r42.checked);
    ɵɵadvance(1);
    ɵɵpropertyInterpolate2("for", "styled-checkbox-", i_r43, "-", columnIndex_r36, "");
    ɵɵadvance(1);
    ɵɵtextInterpolate(header_r42.title);
} }
function TableComponent_thead_18_tr_2_th_1_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r49 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 45);
    ɵɵelement(1, "img", 46);
    ɵɵelementStart(2, "div", 47);
    ɵɵtemplate(3, TableComponent_thead_18_tr_2_th_1_div_6_div_3_Template, 4, 7, "div", 48);
    ɵɵelementStart(4, "div", 49);
    ɵɵelementStart(5, "button", 50);
    ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_6_Template_button_click_5_listener() { ɵɵrestoreView(_r49); const column_r35 = ɵɵnextContext().$implicit; const ctx_r47 = ɵɵnextContext(3); return ctx_r47.filterOnTableColumn(column_r35.filterList, column_r35.name); });
    ɵɵtext(6);
    ɵɵelementEnd();
    ɵɵelementStart(7, "button", 50);
    ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_6_Template_button_click_7_listener() { ɵɵrestoreView(_r49); const column_r35 = ɵɵnextContext().$implicit; const ctx_r50 = ɵɵnextContext(3); return ctx_r50.filterOnTableColumnReset(column_r35.filterList); });
    ɵɵtext(8);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r35 = ɵɵnextContext().$implicit;
    const ctx_r39 = ɵɵnextContext(3);
    ɵɵadvance(3);
    ɵɵproperty("ngForOf", column_r35.filterList);
    ɵɵadvance(3);
    ɵɵtextInterpolate(ctx_r39.TablelanguageObj.rts_dropdown_filter);
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r39.TablelanguageObj.rts_reset);
} }
function TableComponent_thead_18_tr_2_th_1_div_7_Template(rf, ctx) { if (rf & 1) {
    const _r55 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 45);
    ɵɵelement(1, "img", 54);
    ɵɵelementStart(2, "div", 55);
    ɵɵelementStart(3, "input", 56);
    ɵɵlistener("ngModelChange", function TableComponent_thead_18_tr_2_th_1_div_7_Template_input_ngModelChange_3_listener($event) { ɵɵrestoreView(_r55); const column_r35 = ɵɵnextContext().$implicit; return column_r35.searchStr = $event; });
    ɵɵelementEnd();
    ɵɵelementStart(4, "div", 49);
    ɵɵelementStart(5, "button", 50);
    ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_7_Template_button_click_5_listener() { ɵɵrestoreView(_r55); const column_r35 = ɵɵnextContext().$implicit; const ctx_r56 = ɵɵnextContext(3); return ctx_r56.tableDropdownSearch(column_r35.searchStr, column_r35.name); });
    ɵɵtext(6);
    ɵɵelementEnd();
    ɵɵelementStart(7, "button", 50);
    ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_div_7_Template_button_click_7_listener() { ɵɵrestoreView(_r55); const column_r35 = ɵɵnextContext().$implicit; const ctx_r58 = ɵɵnextContext(3); return ctx_r58.tableDropdownSearchReset(column_r35.searchStr); });
    ɵɵtext(8);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r35 = ɵɵnextContext().$implicit;
    const ctx_r40 = ɵɵnextContext(3);
    ɵɵadvance(3);
    ɵɵproperty("ngModel", column_r35.searchStr);
    ɵɵadvance(3);
    ɵɵtextInterpolate(ctx_r40.TablelanguageObj.rts_dropdown_search);
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r40.TablelanguageObj.rts_reset);
} }
function TableComponent_thead_18_tr_2_th_1_Template(rf, ctx) { if (rf & 1) {
    const _r62 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "th", 43);
    ɵɵlistener("click", function TableComponent_thead_18_tr_2_th_1_Template_th_click_0_listener() { ɵɵrestoreView(_r62); const column_r35 = ctx.$implicit; const ctx_r61 = ɵɵnextContext(3); return ctx_r61.doSort(column_r35); });
    ɵɵelementStart(1, "div", 37);
    ɵɵelementStart(2, "span");
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵtemplate(4, TableComponent_thead_18_tr_2_th_1_i_4_Template, 1, 0, "i", 38);
    ɵɵtemplate(5, TableComponent_thead_18_tr_2_th_1_i_5_Template, 1, 0, "i", 39);
    ɵɵtemplate(6, TableComponent_thead_18_tr_2_th_1_div_6_Template, 9, 3, "div", 44);
    ɵɵtemplate(7, TableComponent_thead_18_tr_2_th_1_div_7_Template, 9, 3, "div", 44);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const column_r35 = ctx.$implicit;
    const ctx_r34 = ɵɵnextContext(3);
    ɵɵadvance(3);
    ɵɵtextInterpolate(column_r35.value);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r34.isSortAsc(column_r35));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r34.isSortDesc(column_r35));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", column_r35.filterList.length);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", column_r35.searchable);
} }
function TableComponent_thead_18_tr_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "tr", 34);
    ɵɵtemplate(1, TableComponent_thead_18_tr_2_th_1_Template, 8, 5, "th", 42);
    ɵɵpipe(2, "filterColumn");
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r26 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ɵɵpipeBind1(2, 1, ctx_r26.tableOption.tableColumns));
} }
function TableComponent_thead_18_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "thead");
    ɵɵtemplate(1, TableComponent_thead_18_tr_1_Template, 4, 4, "tr", 33);
    ɵɵtemplate(2, TableComponent_thead_18_tr_2_Template, 3, 3, "tr", 33);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r5 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r5.tableType === "WIN");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r5.tableType === "RTS");
} }
function TableComponent_ng_container_20_td_2_span_1_Template(rf, ctx) { if (rf & 1) {
    const _r77 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span", 68);
    ɵɵelementStart(1, "img", 69);
    ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_1_Template_img_click_1_listener() { ɵɵrestoreView(_r77); const row_r63 = ɵɵnextContext(2).$implicit; const ctx_r75 = ɵɵnextContext(); return ctx_r75.doOpenPreview(row_r63.tableCells[1].value); });
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = ɵɵnextContext().$implicit;
    const ctx_r69 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("src", ctx_r69.downloadFilePath + cell_r68.value + "_thumb", ɵɵsanitizeUrl);
} }
function TableComponent_ng_container_20_td_2_span_2_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r83 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 74);
    ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_2_button_3_Template_button_click_0_listener() { ɵɵrestoreView(_r83); const cell_r68 = ɵɵnextContext(2).$implicit; const i_r64 = ɵɵnextContext().index; const ctx_r81 = ɵɵnextContext(); cell_r68.enabled = true; return ctx_r81.focusInput("input-" + i_r64); });
    ɵɵelement(1, "i", 75);
    ɵɵelementEnd();
} }
function TableComponent_ng_container_20_td_2_span_2_div_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 76);
    ɵɵtext(1, "maxlength ");
    ɵɵelementEnd();
} }
function TableComponent_ng_container_20_td_2_span_2_Template(rf, ctx) { if (rf & 1) {
    const _r87 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span", 70);
    ɵɵtext(1);
    ɵɵelementStart(2, "input", 71);
    ɵɵlistener("ngModelChange", function TableComponent_ng_container_20_td_2_span_2_Template_input_ngModelChange_2_listener($event) { ɵɵrestoreView(_r87); const cell_r68 = ɵɵnextContext().$implicit; return cell_r68.value = $event; })("focusin", function TableComponent_ng_container_20_td_2_span_2_Template_input_focusin_2_listener() { ɵɵrestoreView(_r87); const cell_r68 = ɵɵnextContext().$implicit; return cell_r68.oldValue = cell_r68.value; })("focusout", function TableComponent_ng_container_20_td_2_span_2_Template_input_focusout_2_listener() { ɵɵrestoreView(_r87); const cell_r68 = ɵɵnextContext().$implicit; const row_r63 = ɵɵnextContext().$implicit; const ctx_r90 = ɵɵnextContext(); ctx_r90.doUpdateInputEvent(row_r63, cell_r68.enabled, cell_r68.value, cell_r68.oldValue); return cell_r68.enabled = false; })("keyup.enter", function TableComponent_ng_container_20_td_2_span_2_Template_input_keyup_enter_2_listener() { ɵɵrestoreView(_r87); const cell_r68 = ɵɵnextContext().$implicit; const row_r63 = ɵɵnextContext().$implicit; const ctx_r93 = ɵɵnextContext(); cell_r68.enabled = false; return ctx_r93.doUpdateInputEvent(row_r63, true, cell_r68, cell_r68.oldValue); });
    ɵɵelementEnd();
    ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_2_button_3_Template, 2, 0, "button", 72);
    ɵɵtemplate(4, TableComponent_ng_container_20_td_2_span_2_div_4_Template, 2, 0, "div", 73);
    ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = ɵɵnextContext().$implicit;
    const i_r64 = ɵɵnextContext().index;
    const ctx_r70 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", cell_r68.originalValue, " ");
    ɵɵadvance(1);
    ɵɵpropertyInterpolate("id", "input-" + i_r64);
    ɵɵproperty("ngModel", cell_r68.value)("disabled", !cell_r68.enabled || !ctx_r70.rowSortable);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !cell_r68.enabled && ctx_r70.rowSortable);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", cell_r68.value.length > ctx_r70.inputMaxLenght);
} }
function TableComponent_ng_container_20_td_2_span_3_Template(rf, ctx) { if (rf & 1) {
    const _r100 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span", 77);
    ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_3_Template_span_click_0_listener() { ɵɵrestoreView(_r100); const cell_r68 = ɵɵnextContext().$implicit; const ctx_r98 = ɵɵnextContext(2); return ctx_r98.cellClicked(cell_r68); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = ɵɵnextContext().$implicit;
    ɵɵpropertyInterpolate("title", cell_r68.value);
    ɵɵproperty("ngClass", cell_r68.routeLink.trim().length > 0 ? "hand" : "");
    ɵɵadvance(1);
    ɵɵtextInterpolate(cell_r68.value);
} }
function TableComponent_ng_container_20_td_2_span_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 78);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = ɵɵnextContext().$implicit;
    ɵɵpropertyInterpolate("title", cell_r68.value);
    ɵɵproperty("ngClass", cell_r68.value);
    ɵɵadvance(1);
    ɵɵtextInterpolate(cell_r68.value);
} }
function TableComponent_ng_container_20_td_2_span_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 79);
    ɵɵpipe(1, "date");
    ɵɵtext(2);
    ɵɵpipe(3, "date");
    ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = ɵɵnextContext().$implicit;
    const ctx_r73 = ɵɵnextContext(2);
    ɵɵpropertyInterpolate("title", ɵɵpipeBind2(1, 2, cell_r68.value, ctx_r73.dateFormat));
    ɵɵadvance(2);
    ɵɵtextInterpolate(ɵɵpipeBind2(3, 5, cell_r68.value, ctx_r73.dateFormat));
} }
function TableComponent_ng_container_20_td_2_span_6_span_1_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r112 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 84);
    ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_span_1_button_2_Template_button_click_0_listener() { ɵɵrestoreView(_r112); const buttonAction_r107 = ɵɵnextContext().$implicit; const row_r63 = ɵɵnextContext(3).$implicit; return buttonAction_r107.buttonAction(row_r63); });
    ɵɵelement(1, "img", 85);
    ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r107 = ɵɵnextContext().$implicit;
    ɵɵpropertyInterpolate("title", buttonAction_r107.name);
    ɵɵproperty("disabled", buttonAction_r107.disabled);
    ɵɵadvance(1);
    ɵɵproperty("src", buttonAction_r107.iconUrl, ɵɵsanitizeUrl);
} }
function TableComponent_ng_container_20_td_2_span_6_span_1_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r117 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 86);
    ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_span_1_button_3_Template_button_click_0_listener() { ɵɵrestoreView(_r117); const buttonAction_r107 = ɵɵnextContext().$implicit; const row_r63 = ɵɵnextContext(3).$implicit; return buttonAction_r107.buttonAction(row_r63); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r107 = ɵɵnextContext().$implicit;
    ɵɵproperty("disabled", buttonAction_r107.disabled)("ngClass", "btn-" + buttonAction_r107.name);
    ɵɵadvance(1);
    ɵɵtextInterpolate(buttonAction_r107.name);
} }
function TableComponent_ng_container_20_td_2_span_6_span_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "span");
    ɵɵtemplate(2, TableComponent_ng_container_20_td_2_span_6_span_1_button_2_Template, 2, 3, "button", 82);
    ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_6_span_1_button_3_Template, 2, 3, "button", 83);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r107 = ctx.$implicit;
    ɵɵadvance(2);
    ɵɵproperty("ngIf", buttonAction_r107.iconUrl);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !buttonAction_r107.iconUrl);
} }
function TableComponent_ng_container_20_td_2_span_6_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r122 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 87);
    ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_button_2_Template_button_click_0_listener($event) { ɵɵrestoreView(_r122); const i_r64 = ɵɵnextContext(3).index; const ctx_r120 = ɵɵnextContext(); return ctx_r120.expand("table-row-extra" + i_r64, $event); });
    ɵɵelement(1, "i", 88);
    ɵɵelementEnd();
} if (rf & 2) {
    const i_r64 = ɵɵnextContext(3).index;
    const ctx_r105 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("ngClass", "table-row-extra" + i_r64 == ctx_r105.expandedRowId ? "arrow-rotate" : "");
} }
function TableComponent_ng_container_20_td_2_span_6_div_3_span_5_Template(rf, ctx) { if (rf & 1) {
    const _r128 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "button", 94);
    ɵɵlistener("click", function TableComponent_ng_container_20_td_2_span_6_div_3_span_5_Template_button_click_1_listener() { ɵɵrestoreView(_r128); const buttonAction_r125 = ctx.$implicit; const row_r63 = ɵɵnextContext(4).$implicit; return buttonAction_r125.disabled != true ? buttonAction_r125.buttonAction(row_r63) : ""; });
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const buttonAction_r125 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("disabled", buttonAction_r125.disabled);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", buttonAction_r125.name, "");
} }
function TableComponent_ng_container_20_td_2_span_6_div_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 89);
    ɵɵelementStart(1, "div", 90);
    ɵɵelementStart(2, "button", 91);
    ɵɵelement(3, "i", 92);
    ɵɵelementEnd();
    ɵɵelementStart(4, "div", 93);
    ɵɵtemplate(5, TableComponent_ng_container_20_td_2_span_6_div_3_span_5_Template, 3, 2, "span", 16);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = ɵɵnextContext(2).$implicit;
    ɵɵadvance(5);
    ɵɵproperty("ngForOf", cell_r68.buttonDropdownActions);
} }
function TableComponent_ng_container_20_td_2_span_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵtemplate(1, TableComponent_ng_container_20_td_2_span_6_span_1_Template, 4, 2, "span", 16);
    ɵɵtemplate(2, TableComponent_ng_container_20_td_2_span_6_button_2_Template, 2, 1, "button", 80);
    ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_6_div_3_Template, 6, 1, "div", 81);
    ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = ɵɵnextContext().$implicit;
    const row_r63 = ɵɵnextContext().$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", cell_r68.buttonActions);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", row_r63.expandedText);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", cell_r68.buttonDropdownActions.length);
} }
function TableComponent_ng_container_20_td_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 61);
    ɵɵtemplate(1, TableComponent_ng_container_20_td_2_span_1_Template, 2, 1, "span", 62);
    ɵɵtemplate(2, TableComponent_ng_container_20_td_2_span_2_Template, 5, 6, "span", 63);
    ɵɵtemplate(3, TableComponent_ng_container_20_td_2_span_3_Template, 2, 3, "span", 64);
    ɵɵtemplate(4, TableComponent_ng_container_20_td_2_span_4_Template, 2, 3, "span", 65);
    ɵɵtemplate(5, TableComponent_ng_container_20_td_2_span_5_Template, 4, 8, "span", 66);
    ɵɵtemplate(6, TableComponent_ng_container_20_td_2_span_6_Template, 4, 3, "span", 67);
    ɵɵelementEnd();
} if (rf & 2) {
    const cell_r68 = ctx.$implicit;
    const ctx_r65 = ɵɵnextContext(2);
    ɵɵproperty("ngClass", cell_r68.type + " " + cell_r68.css)("ngSwitch", cell_r68.type);
    ɵɵadvance(1);
    ɵɵproperty("ngSwitchCase", ctx_r65.cellType.IMAGE_THUMBNAIL);
    ɵɵadvance(1);
    ɵɵproperty("ngSwitchCase", ctx_r65.cellType.INPUT);
    ɵɵadvance(2);
    ɵɵproperty("ngSwitchCase", ctx_r65.cellType.STATUS);
    ɵɵadvance(1);
    ɵɵproperty("ngSwitchCase", ctx_r65.cellType.DATE_TIME);
    ɵɵadvance(1);
    ɵɵproperty("ngSwitchCase", ctx_r65.cellType.ACTION);
} }
function TableComponent_ng_container_20_td_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 95);
    ɵɵelement(1, "a", 96);
    ɵɵelementEnd();
} }
function TableComponent_ng_container_20_tr_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "tr", 97);
    ɵɵelementStart(1, "td");
    ɵɵelementStart(2, "div", 98);
    ɵɵelement(3, "div", 99);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r132 = ɵɵnextContext();
    const row_r63 = ctx_r132.$implicit;
    const i_r64 = ctx_r132.index;
    ɵɵadvance(1);
    ɵɵattribute("colspan", row_r63.tableCells.length);
    ɵɵadvance(1);
    ɵɵpropertyInterpolate("id", "table-row-extra" + i_r64);
    ɵɵadvance(1);
    ɵɵproperty("innerHTML", row_r63.expandedText, ɵɵsanitizeHtml);
} }
function TableComponent_ng_container_20_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵelementStart(1, "tr", 57);
    ɵɵtemplate(2, TableComponent_ng_container_20_td_2_Template, 7, 7, "td", 58);
    ɵɵtemplate(3, TableComponent_ng_container_20_td_3_Template, 2, 0, "td", 59);
    ɵɵelementEnd();
    ɵɵtemplate(4, TableComponent_ng_container_20_tr_4_Template, 4, 3, "tr", 60);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    const row_r63 = ctx.$implicit;
    const i_r64 = ctx.index;
    const ctx_r6 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵpropertyInterpolate("id", "table-row-" + i_r64);
    ɵɵproperty("ngClass", ctx_r6.rowSortable ? row_r63.css + "sortable-row" : row_r63.css);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", row_r63.tableCells);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r6.rowSortable);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r6.rowSortable);
} }
function TableComponent_tfoot_21_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "tfoot");
    ɵɵelementStart(1, "tr");
    ɵɵelementStart(2, "td");
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r7 = ɵɵnextContext();
    ɵɵadvance(2);
    ɵɵattribute("colspan", ctx_r7.tableOption.tableColumns.length);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r7.TablelanguageObj.no_data_found);
} }
function TableComponent_div_22_div_2_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "img", 103);
} }
function TableComponent_div_22_div_2_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "img", 104);
} }
function TableComponent_div_22_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r137 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 30);
    ɵɵelementStart(1, "ngb-pagination", 100);
    ɵɵlistener("pageChange", function TableComponent_div_22_div_2_Template_ngb_pagination_pageChange_1_listener($event) { ɵɵrestoreView(_r137); const ctx_r136 = ɵɵnextContext(2); return ctx_r136.tableOption.currentPage = $event; })("pageChange", function TableComponent_div_22_div_2_Template_ngb_pagination_pageChange_1_listener() { ɵɵrestoreView(_r137); const ctx_r138 = ɵɵnextContext(2); return ctx_r138.doPageChange(); });
    ɵɵtemplate(2, TableComponent_div_22_div_2_ng_template_2_Template, 1, 0, "ng-template", 101);
    ɵɵtemplate(3, TableComponent_div_22_div_2_ng_template_3_Template, 1, 0, "ng-template", 102);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r133 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("collectionSize", ctx_r133.tableOption.totalItems)("rotate", true)("maxSize", 5)("pageSize", ctx_r133.tableOption.itemsPerPage)("page", ctx_r133.tableOption.currentPage);
} }
function TableComponent_div_22_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 8);
    ɵɵelementStart(1, "div", 9);
    ɵɵtemplate(2, TableComponent_div_22_div_2_Template, 4, 5, "div", 14);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r8 = ɵɵnextContext();
    ɵɵadvance(2);
    ɵɵproperty("ngIf", !ctx_r8.noDataFound());
} }
function TableComponent_div_23_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 105);
    ɵɵtext(1);
    ɵɵelementStart(2, "span", 106);
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵtext(4);
    ɵɵelementStart(5, "span", 106);
    ɵɵtext(6);
    ɵɵelementEnd();
    ɵɵtext(7);
    ɵɵelementStart(8, "span", 107);
    ɵɵtext(9);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r9 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r9.TablelanguageObj.table_footer_showing, " ");
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r9.startItemNumber());
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r9.TablelanguageObj.table_to, " ");
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r9.endItemNumber());
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r9.TablelanguageObj.table_of, " ");
    ɵɵadvance(2);
    ɵɵtextInterpolate2("", ctx_r9.tableOption.totalItems, " ", ctx_r9.TablelanguageObj.table_elements, "");
} }
const _c1$1 = [[["", "filter", ""]]];
const _c2$1 = ["[filter]"];
class TableComponent {
    constructor(router) {
        this.router = router;
        this.rowSortable = false;
        this.canShowActionButtons = true;
        this.downloadFilePath = '';
        this.actionSort = new EventEmitter();
        this.pageChange = new EventEmitter();
        this.entryPageChange = new EventEmitter();
        this.actionButton = new EventEmitter();
        //RTS
        this.dropdownSearch = new EventEmitter();
        this.dropdownFilter = new EventEmitter();
        // gallery
        this.openGalleryImagePreview = new EventEmitter();
        this.doGalleryUpdateIndexCallback = new EventEmitter();
        this.doGalleryUpdateInput = new EventEmitter();
        this.cellType = CELL_TYPE;
        this.dateFormat = DATE_FORMAT;
        this.entryPerpages = new Array();
        this.isShowFilter = false;
        this.isShowActionHeaderBtn = true;
        this.inputMaxLenght = 255;
        this.TablelanguageObj = TABLE_LANGUAGE_CONFIG;
    }
    ngOnInit() {
        this.tableType = this.type ? this.type : "RTS";
        $(document).on('click', '.dropdown-menu', function (e) {
            e.stopPropagation();
        });
        this.entryPerpages.push(new EntryPerpage("5", 5));
        this.entryPerpages.push(new EntryPerpage("10", 10));
        this.entryPerpages.push(new EntryPerpage("20", 20));
        for (let i = 0; i < this.entryPerpages.length; i++) {
            let entry = this.entryPerpages[i];
            entry.selected = false;
            if (entry.value === this.tableOption.itemsPerPage) {
                entry.selected = true;
            }
        }
        if (this.rowSortable) {
            let self = this;
            setTimeout(() => {
                $('#sortable').sortable({
                    placeholder: "sortable-placeholder",
                    helper: 'clone',
                    axis: "y",
                    cursor: "move",
                    deactivate: function (event, ui) {
                        let dragPostionObj = { fromIndex: ui.item[0].cells[1].innerText, toIndex: self.getSibling(ui.item[0]) };
                        self.doUpdateIndex(dragPostionObj);
                    }
                });
                $("#sortable").disableSelection();
            }, 0);
        }
    }
    getSibling(ui) {
        let text = "";
        if (ui.nextElementSibling && ui.nextElementSibling.cells) {
            text = ui.nextElementSibling.cells[1].innerText;
        }
        else if (ui.previousElementSibling && ui.previousElementSibling.cells) {
            text = ui.previousElementSibling.cells[1].innerText;
        }
        return text;
    }
    startItemNumber() {
        return (this.tableOption.currentPage * this.tableOption.itemsPerPage) - this.tableOption.itemsPerPage + 1;
    }
    endItemNumber() {
        return (this.tableOption.currentPage * this.tableOption.itemsPerPage) < this.tableOption.totalItems ? (this.tableOption.currentPage * this.tableOption.itemsPerPage) : this.tableOption.totalItems;
    }
    noDataFound() {
        return this.tableOption.tableRows.length === 0 && this.tableOption.currentPage <= 1;
    }
    hideFilter() {
        this.isShowFilter = false;
    }
    cellClicked(cell) {
        if (cell.routeLink.trim().length > 0) {
            this.router.navigate([cell.routeLink]);
        }
    }
    showFilter() {
        this.isShowFilter = true;
    }
    toggleFilter() {
        this.isShowFilter = !this.isShowFilter;
    }
    doAction(actionBtn) {
        this.actionButton.emit(actionBtn);
    }
    doPageChange() {
        let currentOffset = (this.tableOption.currentPage - 1) * this.tableOption.itemsPerPage;
        this.pageChange.emit(currentOffset);
    }
    doEntryPage(value) {
        this.tableOption.itemsPerPage = value;
        this.tableOption.currentPage = 1;
        this.entryPageChange.emit(value);
    }
    doSort(column) {
        if (!column.sortable) {
            return;
        }
        let columns = this.tableOption.tableColumns;
        this.currentSortColumnName = undefined;
        this.currentSortType = undefined;
        for (let i = 0; i < this.tableOption.tableColumns.length; i++) {
            let col = this.tableOption.tableColumns[i];
            if (col.name === column.name) {
                if (col.sort === SORT.ASC) {
                    col.sort = SORT.DESC;
                }
                else if (col.sort === SORT.DESC) {
                    col.sort = SORT.NONE;
                    col.sort = this.tableOption.defaultSort.sortColumnName === col.name ? SORT.ASC : SORT.NONE;
                    this.currentSortColumnName = this.tableOption.defaultSort.sortColumnName;
                    this.currentSortType = this.tableOption.defaultSort.sortType;
                }
                else if (col.sort === SORT.NONE) {
                    col.sort = SORT.ASC;
                }
            }
            else {
                col.sort = SORT.NONE;
            }
        }
        this.actionSort.emit(column);
    }
    isSortAsc(column) {
        return column.sort === SORT.ASC || (this.currentSortColumnName && this.currentSortColumnName === column.name && this.currentSortType === SORT.ASC);
    }
    isSortDesc(column) {
        return column.sort === SORT.DESC || (this.currentSortColumnName && this.currentSortColumnName === column.name && this.currentSortType === SORT.DESC);
    }
    //RTS
    //dropdown filter
    filterOnTableColumn(data, name) {
        this.dropdownFilter.emit({ filterData: data, columnName: name });
    }
    filterOnTableColumnReset(data) {
        data.forEach(element => {
            element.checked = false;
        });
    }
    //dropdown search
    tableDropdownSearch(searchString, name) {
        this.dropdownSearch.emit({ searchStr: searchString, columnName: name });
    }
    tableDropdownSearchReset(searchString) {
        searchString = "";
    }
    // gallery
    doOpenPreview(imgCode) {
        this.openGalleryImagePreview.emit(imgCode);
    }
    doUpdateIndex(dragPostionObj) {
        this.doGalleryUpdateIndexCallback.emit(dragPostionObj);
    }
    doUpdateInputEvent(row, canEdit, newValue, originalValue) {
        if (canEdit && newValue.value !== originalValue) {
            if (newValue.value.length <= this.inputMaxLenght) {
                this.doGalleryUpdateInput.emit(row);
            }
            else {
                newValue.value = originalValue;
            }
        }
    }
    focusInput(id) {
        setTimeout(() => {
            document.getElementById(id).focus();
        }, 0);
    }
    expand(id, event) {
        if (this.expandedRowId === id) {
            $("#" + this.expandedRowId).slideUp("fast");
            this.expandedRowId = undefined;
        }
        else {
            if (this.expandedRowId && this.expandedRowId !== id) {
                $("#" + this.expandedRowId).slideUp("fast");
            }
            $("#" + id).slideToggle("fast");
            this.expandedRowId = id;
        }
        event.preventDefault();
    }
}
TableComponent.ɵfac = function TableComponent_Factory(t) { return new (t || TableComponent)(ɵɵdirectiveInject(Router)); };
TableComponent.ɵcmp = ɵɵdefineComponent({ type: TableComponent, selectors: [["via-table"]], inputs: { type: "type", tableOption: "tableOption", rowSortable: "rowSortable", canShowActionButtons: "canShowActionButtons", downloadFilePath: "downloadFilePath", TablelanguageObj: "TablelanguageObj" }, outputs: { actionSort: "actionSort", pageChange: "pageChange", entryPageChange: "entryPageChange", actionButton: "actionButton", dropdownSearch: "dropdownSearch", dropdownFilter: "dropdownFilter", openGalleryImagePreview: "openGalleryImagePreview", doGalleryUpdateIndexCallback: "doGalleryUpdateIndexCallback", doGalleryUpdateInput: "doGalleryUpdateInput" }, ngContentSelectors: _c2$1, decls: 24, vars: 13, consts: [["id", "win-table"], [1, "win-table-wrapper"], [1, "table-top"], [1, "action-wrapper"], [1, "action-container"], ["class", "filter", 4, "ngIf"], [1, "action-button"], [4, "ngIf"], [1, "paging"], [1, "paging-container"], [1, "number-per-page"], [3, "change"], ["entry", ""], [3, "selected", 4, "ngFor", "ngForOf"], ["class", "paging-wrapper", 4, "ngIf"], ["id", "sortable"], [4, "ngFor", "ngForOf"], ["class", "paging", 4, "ngIf"], ["class", "win-table-footer", 4, "ngIf"], [1, "filter"], [1, "filter-container"], [1, "dropdown", 3, "ngClass"], [1, "dropdown-content"], ["src", "./assets/icons/ic_filter.svg", 3, "click"], [1, "filter-text", 3, "click"], [1, "filter-mask", 3, "click"], [1, "dropdown-menu-items", "z-depth-1"], ["type", "button", "class", "btn uppercase-text m-l-10", 3, "ngClass", "click", 4, "ngIf"], ["type", "button", 1, "btn", "uppercase-text", "m-l-10", 3, "ngClass", "click"], [3, "selected"], [1, "paging-wrapper"], [1, "paging-title"], ["size", "sm", 3, "directionLinks", "collectionSize", "rotate", "maxSize", "pageSize", "page", "pageChange"], ["class", "heading", 4, "ngIf"], [1, "heading"], ["class", "handle", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [1, "handle", 3, "ngClass", "click"], [1, "header-container"], ["class", "fa fa-sort-amount-asc", "aria-hidden", "true", 4, "ngIf"], ["class", "fa fa-sort-amount-desc", "aria-hidden", "true", 4, "ngIf"], ["aria-hidden", "true", 1, "fa", "fa-sort-amount-asc"], ["aria-hidden", "true", 1, "fa", "fa-sort-amount-desc"], ["style", "border-radius: 50px;", "class", "handle", 3, "click", 4, "ngFor", "ngForOf"], [1, "handle", 2, "border-radius", "50px", 3, "click"], ["ngbDropdown", "", "display", "dynamic", 4, "ngIf"], ["ngbDropdown", "", "display", "dynamic"], ["ngbDropdownToggle", "", "src", "./assets/icons/filter.svg", 1, "filter-icon"], ["ngbDropdownMenu", "", 1, "dropdown-menu", "dropdown-menu-right"], ["class", "table-dropdown-item", 4, "ngFor", "ngForOf"], [1, "table-dropdown-btm-wrapper"], [1, "table-dropdown-btn", 3, "click"], [1, "table-dropdown-item"], ["type", "checkbox", 1, "styled-checkbox", 3, "id", "value", "ngModel", "ngModelChange"], [3, "for"], ["id", "dropdownForm1", "ngbDropdownToggle", "", "src", "./assets/icons/search-filter.svg", 1, "search-icon"], ["ngbDropdownMenu", "", "aria-labelledby", "dropdownForm1", 1, "dropdown-menu", "dropdown-menu-right"], [1, "table-dropdown-search-input", 3, "ngModel", "ngModelChange"], [3, "id", "ngClass"], ["class", "tablerow", 3, "ngClass", "ngSwitch", 4, "ngFor", "ngForOf"], ["class", "handle", 4, "ngIf"], ["class", "extra-row", 4, "ngIf"], [1, "tablerow", 3, "ngClass", "ngSwitch"], ["class", "thumbnail-wrapper", 4, "ngSwitchCase"], ["class", "cell-input-wrapper", 4, "ngSwitchCase"], [3, "title", "ngClass", "click", 4, "ngSwitchDefault"], ["class", "status", 3, "title", "ngClass", 4, "ngSwitchCase"], [3, "title", 4, "ngSwitchCase"], [4, "ngSwitchCase"], [1, "thumbnail-wrapper"], ["width", "60px", 3, "src", "click"], [1, "cell-input-wrapper"], [1, "cell-input", 3, "ngModel", "id", "disabled", "ngModelChange", "focusin", "focusout", "keyup.enter"], ["class", "btn btn-link btn-action cell-edit-icon", 3, "click", 4, "ngIf"], ["class", "danger-text", "translate", "", 4, "ngIf"], [1, "btn", "btn-link", "btn-action", "cell-edit-icon", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-pencil"], ["translate", "", 1, "danger-text"], [3, "title", "ngClass", "click"], [1, "status", 3, "title", "ngClass"], [3, "title"], ["type", "button", "class", "btn expand-btn", 3, "click", 4, "ngIf"], ["class", "btn-group mr-3", 4, "ngIf"], ["class", "btn btn-link btn-action icon-btn", 3, "disabled", "title", "click", 4, "ngIf"], ["type", "button", "class", "btn btn-light btn-action", 3, "disabled", "ngClass", "click", 4, "ngIf"], [1, "btn", "btn-link", "btn-action", "icon-btn", 3, "disabled", "title", "click"], [3, "src"], ["type", "button", 1, "btn", "btn-light", "btn-action", 3, "disabled", "ngClass", "click"], ["type", "button", 1, "btn", "expand-btn", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-caret-down", "arrow", 3, "ngClass"], [1, "btn-group", "mr-3"], ["ngbDropdown", "", "placement", "bottom-right", "role", "group", "aria-label", "Button group with nested dropdown", 1, "btn-group", "table-dropdown"], ["ngbDropdownToggle", "", 1, "btn"], ["aria-hidden", "true", 1, "fa", "fa-ellipsis-v"], ["ngbDropdownMenu", "", 1, "dropdown-menu"], ["ngbDropdownItem", "", 3, "disabled", "click"], [1, "handle"], [1, "fa", "fa-sort"], [1, "extra-row"], [1, "extra-wrapper", 2, "display", "none", 3, "id"], [3, "innerHTML"], ["size", "md", 1, "d-flex", "justify-content-end", 3, "collectionSize", "rotate", "maxSize", "pageSize", "page", "pageChange"], ["ngbPaginationPrevious", ""], ["ngbPaginationNext", ""], ["src", "./assets/icons/back.svg", 1, "handle-icon"], ["src", "./assets/icons/next.svg", 1, "handle-icon"], [1, "win-table-footer"], [1, "number"], [1, "total"]], template: function TableComponent_Template(rf, ctx) { if (rf & 1) {
        const _r139 = ɵɵgetCurrentView();
        ɵɵprojectionDef(_c1$1);
        ɵɵelementStart(0, "div", 0);
        ɵɵelementStart(1, "div", 1);
        ɵɵelementStart(2, "div", 2);
        ɵɵelementStart(3, "div", 3);
        ɵɵelementStart(4, "div", 4);
        ɵɵtemplate(5, TableComponent_div_5_Template, 11, 2, "div", 5);
        ɵɵelementStart(6, "div", 6);
        ɵɵtemplate(7, TableComponent_div_7_Template, 2, 1, "div", 7);
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(8, "div", 8);
        ɵɵelementStart(9, "div", 9);
        ɵɵelementStart(10, "div", 10);
        ɵɵelementStart(11, "select", 11, 12);
        ɵɵlistener("change", function TableComponent_Template_select_change_11_listener() { ɵɵrestoreView(_r139); const _r2 = ɵɵreference(12); return ctx.doEntryPage(_r2.value); });
        ɵɵtemplate(13, TableComponent_option_13_Template, 2, 3, "option", 13);
        ɵɵelementEnd();
        ɵɵelementStart(14, "span");
        ɵɵtext(15);
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵtemplate(16, TableComponent_div_16_Template, 4, 7, "div", 14);
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(17, "table");
        ɵɵtemplate(18, TableComponent_thead_18_Template, 3, 2, "thead", 7);
        ɵɵelementStart(19, "tbody", 15);
        ɵɵtemplate(20, TableComponent_ng_container_20_Template, 5, 5, "ng-container", 16);
        ɵɵelementEnd();
        ɵɵtemplate(21, TableComponent_tfoot_21_Template, 4, 2, "tfoot", 7);
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵtemplate(22, TableComponent_div_22_Template, 3, 1, "div", 17);
        ɵɵtemplate(23, TableComponent_div_23_Template, 10, 7, "div", 18);
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵclassMap(ctx.tableType);
        ɵɵadvance(5);
        ɵɵproperty("ngIf", ctx.tableOption.tableFilter.hasFilter);
        ɵɵadvance(2);
        ɵɵproperty("ngIf", ctx.tableOption.tableActions.length > 0 && ctx.canShowActionButtons);
        ɵɵadvance(6);
        ɵɵproperty("ngForOf", ctx.entryPerpages);
        ɵɵadvance(2);
        ɵɵtextInterpolate1(" ", ctx.TablelanguageObj.table_entries_per_page, "");
        ɵɵadvance(1);
        ɵɵproperty("ngIf", !ctx.noDataFound());
        ɵɵadvance(2);
        ɵɵproperty("ngIf", !ctx.noDataFound());
        ɵɵadvance(2);
        ɵɵproperty("ngForOf", ctx.tableOption.tableRows);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.noDataFound() && ctx.tableType === "WIN");
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.tableType === "RTS");
        ɵɵadvance(1);
        ɵɵproperty("ngIf", !ctx.noDataFound() && ctx.tableType === "WIN");
    } }, directives: [NgIf, NgForOf, NgClass, NgSelectOption, ɵangular_packages_forms_forms_x, NgbPagination, NgbDropdown, NgbDropdownToggle, NgbDropdownMenu, CheckboxControlValueAccessor, NgControlStatus, NgModel, DefaultValueAccessor, NgSwitch, NgSwitchCase, NgSwitchDefault, NgbDropdownItem, NgbPaginationPrevious, NgbPaginationNext], pipes: [FilterPipe, DatePipe], styles: ["#win-table[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:column}#win-table[_ngcontent-%COMP%]   .extra-row[_ngcontent-%COMP%]{border-bottom:0;height:auto;min-height:0}#win-table[_ngcontent-%COMP%]   .extra-row[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{padding-bottom:0!important;padding-top:0!important}#win-table[_ngcontent-%COMP%]   .expand-btn[_ngcontent-%COMP%]{background:none;margin-top:5px;text-align:center}#win-table[_ngcontent-%COMP%]   .expand-btn[_ngcontent-%COMP%]   .arrow-rotate[_ngcontent-%COMP%]{transform:rotate(-180deg)}#win-table[_ngcontent-%COMP%]   .expand-btn[_ngcontent-%COMP%]   .arrow[_ngcontent-%COMP%]{transition:.2s ease}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]:after{display:none}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-item[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]{background:none;margin-top:4px}#win-table[_ngcontent-%COMP%]   .table-dropdown[_ngcontent-%COMP%]   .dropdown-toggle[_ngcontent-%COMP%]:hover{background:#f7f8f9}#win-table[_ngcontent-%COMP%]   .ui-sortable-helper[_ngcontent-%COMP%]{display:inline-table}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]{align-items:center;background:#fff;border:1px solid #ebedf8;border-radius:3px;display:flex;flex-direction:column;font-size:14px;padding-bottom:40px;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]{align-items:center;display:flex;justify-content:center;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]{align-items:baseline;display:flex;margin-top:20px;width:95%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]{align-items:baseline;flex:3}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]{color:#999;display:flex;flex:9;list-style:none;margin:0;padding:0;z-index:1}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{margin:0;padding:0}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]{width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]{align-items:baseline;display:flex;flex-direction:row;justify-content:flex-start}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{cursor:pointer;margin-right:5px;width:18px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-content[_ngcontent-%COMP%]   .filter-text[_ngcontent-%COMP%]{color:#0800d1;font-weight:700}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{background:#fff;border:1px solid #ebebeb;border-radius:3px;display:none;list-style:none;padding:10px 10px 30px;position:absolute;top:100%;width:250%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{width:100%;z-index:1}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .filter[_ngcontent-%COMP%]   .filter-container[_ngcontent-%COMP%]   .dropdown.open[_ngcontent-%COMP%]   .dropdown-menu-items[_ngcontent-%COMP%]{display:flex}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .action-wrapper[_ngcontent-%COMP%]   .action-container[_ngcontent-%COMP%]   .action-button[_ngcontent-%COMP%]{align-items:baseline;display:flex;flex:7;justify-content:flex-end}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]{align-items:center;border-bottom:1px solid #f0f0f0;display:flex;justify-content:center;padding:10px 0;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]{align-items:baseline;display:flex;width:95%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .number-per-page[_ngcontent-%COMP%]{align-items:baseline;flex:1}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .number-per-page[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{background:#fff;border:1px solid #f0f0f0;border-radius:5px;max-width:120px;padding:5px;width:100px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .paging-wrapper[_ngcontent-%COMP%]{align-items:baseline;color:#aab2c0;display:flex;flex:1;justify-content:flex-end}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .paging-container[_ngcontent-%COMP%]   .paging-wrapper[_ngcontent-%COMP%]   .paging-title[_ngcontent-%COMP%]{margin-right:10px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]{width:95%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]{color:#b4bac6;font-size:12px;padding:30px 15px 10px 0;text-align:left;text-transform:uppercase}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th.sortable[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:row;justify-content:center;width:100%}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{flex:9}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   th[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#000;flex:1;text-align:right}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]{min-height:40px;vertical-align:text-top}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]{color:#b4bac6;font-size:14px;padding:10px 0}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td.bold[_ngcontent-%COMP%]{color:#354052;font-weight:700}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]   .hand[_ngcontent-%COMP%]{cursor:pointer}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td[_ngcontent-%COMP%]   button.btn-action[_ngcontent-%COMP%]{margin-right:5px;margin-top:5px;padding:10px 0!important;text-transform:uppercase;width:80px}#win-table[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tfoot[_ngcontent-%COMP%]{font-size:24px;font-weight:700;text-align:center}#win-table[_ngcontent-%COMP%]   .win-table-footer[_ngcontent-%COMP%]{color:#aab2c0;font-size:15px;margin-top:20px;width:95%}#win-table[_ngcontent-%COMP%]   .win-table-footer[_ngcontent-%COMP%]   .number[_ngcontent-%COMP%]{color:#000;font-weight:700}#win-table[_ngcontent-%COMP%]   .win-table-footer[_ngcontent-%COMP%]   .total[_ngcontent-%COMP%]{font-weight:700}#win-table[_ngcontent-%COMP%]   .ellipsis-td[_ngcontent-%COMP%]{max-width:110px;overflow:hidden;padding:0 10px!important;text-overflow:ellipsis;white-space:nowrap}#win-table[_ngcontent-%COMP%]   .wordwrap-td[_ngcontent-%COMP%]{display:block;max-width:110px}#win-table[_ngcontent-%COMP%]   .ACTION[_ngcontent-%COMP%]{text-align:center!important}#win-table[_ngcontent-%COMP%]   .icon-btn[_ngcontent-%COMP%]{width:50px!important}#win-table[_ngcontent-%COMP%]   .icon-btn[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:20px}#win-table[_ngcontent-%COMP%]   .text-align-right[_ngcontent-%COMP%]{padding-right:10px!important;text-align:right}#win-table[_ngcontent-%COMP%]   .header-text-align-right[_ngcontent-%COMP%]{padding-right:10px!important;text-align:right!important}#win-table[_ngcontent-%COMP%]   .filter-mask[_ngcontent-%COMP%]{bottom:0;display:none;left:0;position:fixed;right:0;top:0}#win-table[_ngcontent-%COMP%]   .open[_ngcontent-%COMP%]   .filter-mask[_ngcontent-%COMP%]{display:block}#win-table[_ngcontent-%COMP%]   .thumbnail-wrapper[_ngcontent-%COMP%]{cursor:pointer;height:30px;overflow:hidden;width:60px}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-edit-icon[_ngcontent-%COMP%]{color:#5a5a5a;cursor:pointer;margin-right:0;margin-top:0;width:35px!important}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-edit-icon[_ngcontent-%COMP%]:hover{color:#9a0000}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-input[_ngcontent-%COMP%]{background:#fff;border:0}#win-table[_ngcontent-%COMP%]   .cell-input-wrapper[_ngcontent-%COMP%]   .cell-input[_ngcontent-%COMP%]:focus{border-bottom:1px solid #07f}#win-table[_ngcontent-%COMP%]   .sortable-row[_ngcontent-%COMP%]{cursor:move}#win-table[_ngcontent-%COMP%]   .sortable-row[_ngcontent-%COMP%]:hover{background:hsla(0,0%,50.2%,.03)}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]{opacity:0;position:absolute}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%] + label[_ngcontent-%COMP%]{cursor:pointer;font-weight:200;padding:0;position:relative;text-transform:none}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%] + label[_ngcontent-%COMP%]:before{background:#fff;border:1px solid #a6b4bf;border-radius:2px;content:\"\";display:inline-block;height:20px;margin-right:10px;vertical-align:text-top;width:20px}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:checked + label[_ngcontent-%COMP%]:before{border:1px solid #4ca0fd}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:disabled + label[_ngcontent-%COMP%]{color:#b8b8b8;cursor:auto}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:disabled + label[_ngcontent-%COMP%]:before{background:#ddd;box-shadow:none}.RTS[_ngcontent-%COMP%]   .styled-checkbox[_ngcontent-%COMP%]:checked + label[_ngcontent-%COMP%]:after{background:#4ca0fd;box-shadow:2px 0 0 #4ca0fd,4px 0 0 #4ca0fd,4px -2px 0 #4ca0fd,4px -4px 0 #4ca0fd,4px -6px 0 #4ca0fd,4px -8px 0 #4ca0fd;content:\"\";height:2px;left:5px;position:absolute;top:10px;transform:rotate(45deg);width:2px}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]:before{border-bottom:7px solid rgba(0,0,0,.03);border-left:7px solid transparent;border-right:7px solid transparent;content:\"\";display:inline-block;position:absolute;right:2px;top:-7px}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]{top:25px!important}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]:after{border-bottom:6px solid #fff;border-left:6px solid transparent;border-right:6px solid transparent;content:\"\";display:inline-block;position:absolute;right:3px;top:-6px}.RTS[_ngcontent-%COMP%]   .dropdown-menu-right[_ngcontent-%COMP%]{left:auto!important;right:0!important}.RTS[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]{border:none;box-shadow:0 3px 11px -1px #d6d6d6;padding:10px 10px 0}.RTS[_ngcontent-%COMP%]   .table-dropdown-btn[_ngcontent-%COMP%]{background:#4ca0fd;border:none;border-radius:5px;color:#fff;cursor:pointer;height:40px;margin:5px;min-width:100px;text-align:center}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]{border:1px solid #e2e6ea!important;border-radius:5px!important;padding-bottom:0!important}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]{width:100%!important}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]{border-bottom:1px solid #e2e6ea}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]{height:50px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]{background:#fdfefe;padding-top:6px!important}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]{color:#577286;padding-left:15px;text-transform:uppercase}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .filter-icon[_ngcontent-%COMP%]{cursor:pointer;width:17px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .search-icon[_ngcontent-%COMP%]{cursor:pointer;margin-top:3px;width:25px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .table-dropdown-item[_ngcontent-%COMP%]{padding-left:5px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .table-dropdown-btm-wrapper[_ngcontent-%COMP%]{display:inline-flex;text-align:center}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   thead[_ngcontent-%COMP%]   tr.heading[_ngcontent-%COMP%]   th.handle[_ngcontent-%COMP%]   .table-dropdown-search-input[_ngcontent-%COMP%]{border:1px solid #d8d8d8;border-radius:5px;height:40px;margin-bottom:10px;margin-left:5px;margin-top:10px;padding-left:10px;width:96%}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]{border-bottom:1px solid #e2e6ea;height:50px}.RTS[_ngcontent-%COMP%]   .win-table-wrapper[_ngcontent-%COMP%]   table[_ngcontent-%COMP%]   tbody[_ngcontent-%COMP%]   tr[_ngcontent-%COMP%]   td.tablerow[_ngcontent-%COMP%]{padding-bottom:0!important;padding-left:15px!important;padding-top:0}.RTS[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]{margin-top:35px;width:100%}.RTS[_ngcontent-%COMP%]   .paging[_ngcontent-%COMP%]   .handle-icon[_ngcontent-%COMP%]{position:relative;top:-2px}"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(TableComponent, [{
        type: Component,
        args: [{
                selector: "via-table",
                templateUrl: "./table.html",
                styleUrls: ["./table.scss"]
            }]
    }], function () { return [{ type: Router }]; }, { type: [{
            type: Input
        }], tableOption: [{
            type: Input
        }], rowSortable: [{
            type: Input
        }], canShowActionButtons: [{
            type: Input
        }], downloadFilePath: [{
            type: Input
        }], actionSort: [{
            type: Output
        }], pageChange: [{
            type: Output
        }], entryPageChange: [{
            type: Output
        }], actionButton: [{
            type: Output
        }], dropdownSearch: [{
            type: Output
        }], dropdownFilter: [{
            type: Output
        }], openGalleryImagePreview: [{
            type: Output
        }], doGalleryUpdateIndexCallback: [{
            type: Output
        }], doGalleryUpdateInput: [{
            type: Output
        }], TablelanguageObj: [{
            type: Input
        }] }); })();

class TableModule {
}
TableModule.ɵmod = ɵɵdefineNgModule({ type: TableModule });
TableModule.ɵinj = ɵɵdefineInjector({ factory: function TableModule_Factory(t) { return new (t || TableModule)(); }, imports: [[
            RouterModule.forChild([]),
            NgbModule,
            FormsModule,
            CommonModule,
            ReactiveFormsModule
        ], RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(TableModule, { declarations: [TableComponent, FilterPipe], imports: [RouterModule, NgbModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule], exports: [TableComponent, FilterPipe, RouterModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(TableModule, [{
        type: NgModule,
        args: [{
                imports: [
                    RouterModule.forChild([]),
                    NgbModule,
                    FormsModule,
                    CommonModule,
                    ReactiveFormsModule
                ],
                declarations: [TableComponent, FilterPipe],
                exports: [TableComponent, FilterPipe, RouterModule]
            }]
    }], null, null); })();

class TableAction {
    constructor(name = "", routeLink = "", css = "btn-primary", type = ACTION_TYPE.BUTTON, hided = false) {
        this._name = name;
        this._type = type;
        this._routeLink = routeLink;
        this._css = css;
        this._hided = hided;
    }
    /**
     * Getter hided
     * return {boolean}
     */
    get hided() {
        return this._hided;
    }
    /**
     * Setter hided
     * param {boolean} value
     */
    set hided(value) {
        this._hided = value;
    }
    get css() {
        return this._css;
    }
    set css(value) {
        this._css = value;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get type() {
        return this._type;
    }
    set type(value) {
        this._type = value;
    }
    get routeLink() {
        return this._routeLink;
    }
    set routeLink(value) {
        this._routeLink = value;
    }
}
const ACTION_TYPE = {
    BUTTON: "BUTTON",
    LINK: "LINK",
    MODAL: "MODAL"
};

class ButtonAction {
    constructor(name = "", callbackFunc, disabled = false, iconUrl = undefined, isDropdownItem = false) {
        this._isDropdownItem = false;
        this._name = name;
        this._buttonAction = callbackFunc;
        this._disabled = disabled;
        this._iconUrl = iconUrl;
        this._isDropdownItem = isDropdownItem;
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter buttonAction
     * return {Function}
     */
    get buttonAction() {
        return this._buttonAction;
    }
    /**
     * Getter disabled
     * return {boolean}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * Getter iconUrl
     * return {string}
     */
    get iconUrl() {
        return this._iconUrl;
    }
    /**
     * Getter isDropdownItem
     * return {boolean }
     */
    get isDropdownItem() {
        return this._isDropdownItem;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter buttonAction
     * param {Function} value
     */
    set buttonAction(value) {
        this._buttonAction = value;
    }
    /**
     * Setter disabled
     * param {boolean} value
     */
    set disabled(value) {
        this._disabled = value;
    }
    /**
     * Setter iconUrl
     * param {string} value
     */
    set iconUrl(value) {
        this._iconUrl = value;
    }
    /**
     * Setter isDropdownItem
     * param {boolean } value
     */
    set isDropdownItem(value) {
        this._isDropdownItem = value;
    }
}

class TableColumn {
    constructor(value = "", name = "", sortable = false, sort = SORT.NONE, css = "", hidden = false, filterList = [], searchable = false, searchString = "") {
        this._name = name;
        this._value = value;
        this._sort = sort;
        this._sortable = sortable;
        this._css = css;
        this._hidden = hidden;
        this._filterList = filterList;
        this._searchStr = searchString;
        this._searchable = searchable;
    }
    /**
     * Getter name
     * return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter sortable
     * return {boolean}
     */
    get sortable() {
        return this._sortable;
    }
    /**
     * Getter sort
     * return {string}
     */
    get sort() {
        return this._sort;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter hidden
     * return {boolean}
     */
    get hidden() {
        return this._hidden;
    }
    /**
     * Getter filterList
     * return {TableFilterDropdownItem[]}
     */
    get filterList() {
        return this._filterList;
    }
    /**
     * Getter searchStr
     * return {string}
     */
    get searchStr() {
        return this._searchStr;
    }
    /**
     * Getter searchable
     * return {boolean}
     */
    get searchable() {
        return this._searchable;
    }
    /**
     * Setter name
     * param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter sortable
     * param {boolean} value
     */
    set sortable(value) {
        this._sortable = value;
    }
    /**
     * Setter sort
     * param {string} value
     */
    set sort(value) {
        this._sort = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter hidden
     * param {boolean} value
     */
    set hidden(value) {
        this._hidden = value;
    }
    /**
     * Setter filterList
     * param {TableFilterDropdownItem[]} value
     */
    set filterList(value) {
        this._filterList = value;
    }
    /**
     * Setter searchStr
     * param {string} value
     */
    set searchStr(value) {
        this._searchStr = value;
    }
    /**
     * Setter searchable
     * param {boolean} value
     */
    set searchable(value) {
        this._searchable = value;
    }
}

class TableFilterDropdownItem {
    constructor(title, value, checked = false) {
        this._title = title;
        this._value = value;
        this._checked = checked;
    }
    /**
     * Getter title
     * return {string}
     */
    get title() {
        return this._title;
    }
    /**
     * Getter value
     * return {string}
     */
    get value() {
        return this._value;
    }
    /**
     * Getter checked
     * return {boolean}
     */
    get checked() {
        return this._checked;
    }
    /**
     * Setter title
     * param {string} value
     */
    set title(value) {
        this._title = value;
    }
    /**
     * Setter value
     * param {string} value
     */
    set value(value) {
        this._value = value;
    }
    /**
     * Setter checked
     * param {boolean} value
     */
    set checked(value) {
        this._checked = value;
    }
}

class TableRow {
    constructor(tableCells = [], css = "", expandedText = "") {
        this._tableCells = tableCells;
        this._css = css;
        this._expandedText = expandedText;
        this._editAble = false;
        this._activeAble = false;
        this._deactiveAble = false;
        this._deleteAble = false;
    }
    /**
     * Getter tableCells
     * return {TableCell[]}
     */
    get tableCells() {
        return this._tableCells;
    }
    /**
     * Getter css
     * return {string}
     */
    get css() {
        return this._css;
    }
    /**
     * Getter editAble
     * return {boolean}
     */
    get editAble() {
        return this._editAble;
    }
    /**
     * Getter deleteAble
     * return {boolean}
     */
    get deleteAble() {
        return this._deleteAble;
    }
    /**
     * Getter deactiveAble
     * return {boolean}
     */
    get deactiveAble() {
        return this._deactiveAble;
    }
    /**
     * Getter activeAble
     * return {boolean}
     */
    get activeAble() {
        return this._activeAble;
    }
    /**
     * Getter expandedText
     * return {string}
     */
    get expandedText() {
        return this._expandedText;
    }
    /**
     * Setter tableCells
     * param {TableCell[]} value
     */
    set tableCells(value) {
        this._tableCells = value;
    }
    /**
     * Setter css
     * param {string} value
     */
    set css(value) {
        this._css = value;
    }
    /**
     * Setter editAble
     * param {boolean} value
     */
    set editAble(value) {
        this._editAble = value;
    }
    /**
     * Setter deleteAble
     * param {boolean} value
     */
    set deleteAble(value) {
        this._deleteAble = value;
    }
    /**
     * Setter deactiveAble
     * param {boolean} value
     */
    set deactiveAble(value) {
        this._deactiveAble = value;
    }
    /**
     * Setter activeAble
     * param {boolean} value
     */
    set activeAble(value) {
        this._activeAble = value;
    }
    /**
     * Setter expandedText
     * param {string} value
     */
    set expandedText(value) {
        this._expandedText = value;
    }
}

class TableRowValue extends TableRow {
    constructor(tableCells = [], css = "", data = null) {
        super(tableCells, css);
        this._data = data;
    }
    get data() {
        return this._data;
    }
    set data(value) {
        this._data = value;
    }
}

class UploadOption {
    constructor(acceptFileType = [], uploadApi, downloadFileApi, language, authorization, placeHolderImg) {
        this._acceptFileType = acceptFileType;
        this._uploadApi = uploadApi;
        this._language = language;
        this._authorization = authorization;
        this._downloadFileApi = downloadFileApi;
        this._placeHolderImg = placeHolderImg;
    }
    /**
     * Getter acceptFileType
     * return {string[]}
     */
    get acceptFileType() {
        return this._acceptFileType;
    }
    /**
     * Getter uploadApi
     * return {string}
     */
    get uploadApi() {
        return this._uploadApi;
    }
    /**
     * Getter downloadFileApi
     * return {string}
     */
    get downloadFileApi() {
        return this._downloadFileApi;
    }
    /**
     * Getter language
     * return {string}
     */
    get language() {
        return this._language;
    }
    /**
     * Getter authorization
     * return {string}
     */
    get authorization() {
        return this._authorization;
    }
    /**
     * Getter placeHolderImg
     * return {string}
     */
    get placeHolderImg() {
        return this._placeHolderImg;
    }
    /**
     * Setter acceptFileType
     * param {string[]} value
     */
    set acceptFileType(value) {
        this._acceptFileType = value;
    }
    /**
     * Setter uploadApi
     * param {string} value
     */
    set uploadApi(value) {
        this._uploadApi = value;
    }
    /**
     * Setter downloadFileApi
     * param {string} value
     */
    set downloadFileApi(value) {
        this._downloadFileApi = value;
    }
    /**
     * Setter language
     * param {string} value
     */
    set language(value) {
        this._language = value;
    }
    /**
     * Setter authorization
     * param {string} value
     */
    set authorization(value) {
        this._authorization = value;
    }
    /**
     * Setter placeHolderImg
     * param {string} value
     */
    set placeHolderImg(value) {
        this._placeHolderImg = value;
    }
}

const UPLOAD_FILE_EVENT = {
    START_UPLOAD: "START_UPLOAD",
    UPLOAD_ERROR: "UPLOAD_ERROR",
    UPLOAD_SUCCESSED: "UPLOAD_SUCCESSED",
    FINISH_UPLOAD: "FINISH_UPLOAD"
};
const UPLOAD_FILE_LANGUAGE_CONFIG = {
    crop_image_yes: "Yes",
    crop_image_cancel: "Cancel",
    upload_file_drag_and_drop: "Drag and drop file here",
    upload_file_or: "or",
    upload_file_change: "Change",
    upload_file_browse_file: "Browser file",
    upload_file_browse_files: "Browser files"
};
const uploadType = {
    IMAGE: "IMAGE",
    FILE: "FILE"
};
const allowedContentTypesUploadTxt = ["text/plain"];
const allowedContentTypesUploadImg = ["image/jpg", "image/jpeg", "image/png"];
const allowedContentTypesUploadExel = [".xlsx", ".csv", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"];
const uploadRatio = {
    SIXTEEN_NINE: {
        text: "SIXTEEN_NINE",
        ratioStr: "16:9",
        ratio: 16 / 9,
        calculateHeightRatio: 9 / 16,
        resizeToWidth: 1024
    },
    ONE_ONE: {
        text: "ONE_ONE",
        ratioStr: "1:1",
        ratio: 1 / 1,
        calculateHeightRatio: 1,
        resizeToWidth: 300
    },
    ONE_THREE: {
        text: "ONE_THREE",
        ratioStr: "1:3",
        ratio: 1 / 3,
        calculateHeightRatio: 3,
        resizeToWidth: 150
    },
    FOUR_ONE: {
        text: "FOUR_ONE",
        ratioStr: "4:1",
        ratio: 4 / 1,
        calculateHeightRatio: 1 / 4,
        resizeToWidth: 1024
    }
};
class ErrorObj {
    constructor(field = "", message = "") {
        this.field = field;
        this.message = message;
    }
}

class UploadResponse {
    constructor(responseType = "", message = "", code = "") {
        this._message = message;
        this._responseType = responseType;
        this._code = code;
    }
    /**
     * Getter responseType
     * return {string}
     */
    get responseType() {
        return this._responseType;
    }
    /**
     * Getter message
     * return {string}
     */
    get message() {
        return this._message;
    }
    /**
     * Getter code
     * return {string}
     */
    get code() {
        return this._code;
    }
    /**
     * Setter responseType
     * param {string} value
     */
    set responseType(value) {
        this._responseType = value;
    }
    /**
     * Setter message
     * param {string} value
     */
    set message(value) {
        this._message = value;
    }
    /**
     * Setter code
     * param {string} value
     */
    set code(value) {
        this._code = value;
    }
}

function UploadFileComponent_div_0_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r6 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 6);
    ɵɵelementStart(1, "image-cropper", 7);
    ɵɵlistener("imageCropped", function UploadFileComponent_div_0_div_1_Template_image_cropper_imageCropped_1_listener($event) { ɵɵrestoreView(_r6); const ctx_r5 = ɵɵnextContext(2); return ctx_r5.imageCropped($event); })("imageLoaded", function UploadFileComponent_div_0_div_1_Template_image_cropper_imageLoaded_1_listener() { ɵɵrestoreView(_r6); const ctx_r7 = ɵɵnextContext(2); return ctx_r7.imageLoaded(); })("loadImageFailed", function UploadFileComponent_div_0_div_1_Template_image_cropper_loadImageFailed_1_listener() { ɵɵrestoreView(_r6); const ctx_r8 = ɵɵnextContext(2); return ctx_r8.loadImageFailed(); });
    ɵɵelementEnd();
    ɵɵelementStart(2, "div");
    ɵɵelementStart(3, "button", 8);
    ɵɵlistener("click", function UploadFileComponent_div_0_div_1_Template_button_click_3_listener() { ɵɵrestoreView(_r6); const ctx_r9 = ɵɵnextContext(2); return ctx_r9.uploadCroppedImage(); });
    ɵɵtext(4);
    ɵɵelementEnd();
    ɵɵelementStart(5, "button", 9);
    ɵɵlistener("click", function UploadFileComponent_div_0_div_1_Template_button_click_5_listener() { ɵɵrestoreView(_r6); const ctx_r10 = ɵɵnextContext(2); return ctx_r10.clearUploadCroppedImage(); });
    ɵɵtext(6);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("maintainAspectRatio", true)("imageQuality", 100)("imageChangedEvent", ctx_r2.imageChangedEvent)("autoCrop", true)("maintainAspectRatio", true)("aspectRatio", ctx_r2.getRatio());
    ɵɵadvance(3);
    ɵɵtextInterpolate1(" ", ctx_r2.uploadlanguageObj.crop_image_yes, " ");
    ɵɵadvance(2);
    ɵɵtextInterpolate1(" ", ctx_r2.uploadlanguageObj.crop_image_cancel, " ");
} }
function UploadFileComponent_div_0_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r13 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 10);
    ɵɵelementStart(1, "div", 11);
    ɵɵelementStart(2, "div", 12);
    ɵɵelementStart(3, "img", 13, 14);
    ɵɵlistener("error", function UploadFileComponent_div_0_div_2_Template_img_error_3_listener() { ɵɵrestoreView(_r13); const _r11 = ɵɵreference(4); const ctx_r12 = ɵɵnextContext(2); _r11.error = null; return _r11.src = ctx_r12.uploadOption.placeHolderImg; });
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = ɵɵnextContext(2);
    ɵɵadvance(3);
    ɵɵproperty("src", ctx_r3.uploadOption.downloadFileApi + ctx_r3.fileName, ɵɵsanitizeUrl);
} }
function UploadFileComponent_div_0_div_3_div_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 22);
    ɵɵelement(1, "i", 23);
    ɵɵelementEnd();
} }
function UploadFileComponent_div_0_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r16 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 15);
    ɵɵelementStart(1, "label", 16);
    ɵɵtemplate(2, UploadFileComponent_div_0_div_3_div_2_Template, 2, 0, "div", 17);
    ɵɵelementStart(3, "div", 18);
    ɵɵtext(4);
    ɵɵelementEnd();
    ɵɵelementStart(5, "div", 19);
    ɵɵtext(6);
    ɵɵelementEnd();
    ɵɵelement(7, "br");
    ɵɵelementStart(8, "span", 20);
    ɵɵtext(9);
    ɵɵelementEnd();
    ɵɵelement(10, "br");
    ɵɵelementStart(11, "input", 21);
    ɵɵlistener("change", function UploadFileComponent_div_0_div_3_Template_input_change_11_listener($event) { ɵɵrestoreView(_r16); const ctx_r15 = ɵɵnextContext(2); return ctx_r15.fileChangeEvent($event); });
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = ɵɵnextContext(2);
    ɵɵadvance(2);
    ɵɵproperty("ngIf", !ctx_r4.fileName);
    ɵɵadvance(2);
    ɵɵtextInterpolate1("", ctx_r4.uploadlanguageObj.upload_file_drag_and_drop, " ");
    ɵɵadvance(2);
    ɵɵtextInterpolate(ctx_r4.uploadlanguageObj.upload_file_or);
    ɵɵadvance(3);
    ɵɵtextInterpolate1(" ", ctx_r4.uploadlanguageObj.upload_file_browse_file, " ");
    ɵɵadvance(2);
    ɵɵpropertyInterpolate("accept", ctx_r4.options.allowedContentTypes);
} }
function UploadFileComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 2);
    ɵɵtemplate(1, UploadFileComponent_div_0_div_1_Template, 7, 8, "div", 3);
    ɵɵtemplate(2, UploadFileComponent_div_0_div_2_Template, 5, 1, "div", 4);
    ɵɵtemplate(3, UploadFileComponent_div_0_div_3_Template, 12, 5, "div", 5);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = ɵɵnextContext();
    ɵɵclassMapInterpolate1("ratio-", ctx_r0.ratio, " cropper-upload");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r0.imageChangedEvent && ctx_r0.options);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r0.imageChangedEvent && ctx_r0.fileName);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r0.imageChangedEvent && ctx_r0.options);
} }
function UploadFileComponent_div_1_div_1_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 29);
    ɵɵelement(1, "ngb-progressbar", 30);
    ɵɵelementEnd();
} if (rf & 2) {
    const f_r22 = ɵɵnextContext().$implicit;
    ɵɵadvance(1);
    ɵɵproperty("value", f_r22 == null ? null : f_r22.progress == null ? null : f_r22.progress.data == null ? null : f_r22.progress.data.percentage);
} }
function UploadFileComponent_div_1_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 10);
    ɵɵelementStart(1, "div", 11);
    ɵɵtemplate(2, UploadFileComponent_div_1_div_1_div_1_div_2_Template, 2, 1, "div", 28);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r20 = ɵɵnextContext(3);
    ɵɵadvance(2);
    ɵɵproperty("ngIf", !ctx_r20.isUploadFilesDone());
} }
function UploadFileComponent_div_1_div_1_div_2_img_3_Template(rf, ctx) { if (rf & 1) {
    const _r29 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "img", 13, 32);
    ɵɵlistener("error", function UploadFileComponent_div_1_div_1_div_2_img_3_Template_img_error_0_listener() { ɵɵrestoreView(_r29); const _r27 = ɵɵreference(1); const ctx_r28 = ɵɵnextContext(4); _r27.error = null; return _r27.src = ctx_r28.uploadOption.placeHolderImg; });
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r26 = ɵɵnextContext(4);
    ɵɵproperty("src", ctx_r26.uploadOption.downloadFileApi + ctx_r26.getImgUrl(), ɵɵsanitizeUrl);
} }
function UploadFileComponent_div_1_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 10);
    ɵɵelementStart(1, "div", 11);
    ɵɵelementStart(2, "div", 12);
    ɵɵtemplate(3, UploadFileComponent_div_1_div_1_div_2_img_3_Template, 2, 1, "img", 31);
    ɵɵelementEnd();
    ɵɵtext(4);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r21 = ɵɵnextContext(3);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", !ctx_r21.hidePreview);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r21.fileId, " ");
} }
function UploadFileComponent_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div");
    ɵɵtemplate(1, UploadFileComponent_div_1_div_1_div_1_Template, 3, 1, "div", 27);
    ɵɵtemplate(2, UploadFileComponent_div_1_div_1_div_2_Template, 5, 2, "div", 4);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r17 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", ctx_r17.files);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r17.multiple && (ctx_r17.fileId || ctx_r17.fileName));
} }
function UploadFileComponent_div_1_div_2_span_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 18);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r30 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", ctx_r30.uploadlanguageObj.upload_file_drag_and_drop, " ");
} }
function UploadFileComponent_div_1_div_2_span_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 19);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r31 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r31.uploadlanguageObj.upload_file_or);
} }
function UploadFileComponent_div_1_div_2_span_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 38);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r32 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r32.uploadlanguageObj.upload_file_browse_files, " ");
} }
function UploadFileComponent_div_1_div_2_span_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 38);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r33 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r33.uploadlanguageObj.upload_file_change, " ");
} }
function UploadFileComponent_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r35 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div");
    ɵɵtemplate(1, UploadFileComponent_div_1_div_2_span_1_Template, 2, 1, "span", 33);
    ɵɵtemplate(2, UploadFileComponent_div_1_div_2_span_2_Template, 2, 1, "span", 34);
    ɵɵelementStart(3, "label", 35);
    ɵɵelementStart(4, "input", 36);
    ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_div_2_Template_input_uploadOutput_4_listener($event) { ɵɵrestoreView(_r35); const ctx_r34 = ɵɵnextContext(2); return ctx_r34.onUploadOutput($event); });
    ɵɵelementEnd();
    ɵɵtemplate(5, UploadFileComponent_div_1_div_2_span_5_Template, 2, 1, "span", 37);
    ɵɵtemplate(6, UploadFileComponent_div_1_div_2_span_6_Template, 2, 1, "span", 37);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r18 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r18.isUploadFilesDone());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r18.isUploadFilesDone());
    ɵɵadvance(2);
    ɵɵpropertyInterpolate("accept", ctx_r18.options.allowedContentTypes);
    ɵɵproperty("options", ctx_r18.options)("uploadInput", ctx_r18.uploadInput);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r18.isUploadFilesDone());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r18.isUploadFilesDone());
} }
function UploadFileComponent_div_1_div_3_div_1_span_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 18);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r38 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", ctx_r38.uploadlanguageObj.upload_file_drag_and_drop, " ");
} }
function UploadFileComponent_div_1_div_3_div_1_span_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 19);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r39 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r39.uploadlanguageObj.upload_file_or);
} }
function UploadFileComponent_div_1_div_3_div_1_span_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 38);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r40 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r40.uploadlanguageObj.upload_file_browse_file, " ");
} }
function UploadFileComponent_div_1_div_3_div_1_span_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span", 38);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r41 = ɵɵnextContext(4);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r41.uploadlanguageObj.upload_file_change, " ");
} }
function UploadFileComponent_div_1_div_3_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r43 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div");
    ɵɵtemplate(1, UploadFileComponent_div_1_div_3_div_1_span_1_Template, 2, 1, "span", 33);
    ɵɵtemplate(2, UploadFileComponent_div_1_div_3_div_1_span_2_Template, 2, 1, "span", 34);
    ɵɵelementStart(3, "label", 35);
    ɵɵelementStart(4, "input", 40);
    ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_div_3_div_1_Template_input_uploadOutput_4_listener($event) { ɵɵrestoreView(_r43); const ctx_r42 = ɵɵnextContext(3); return ctx_r42.onUploadOutput($event); });
    ɵɵelementEnd();
    ɵɵtemplate(5, UploadFileComponent_div_1_div_3_div_1_span_5_Template, 2, 1, "span", 37);
    ɵɵtemplate(6, UploadFileComponent_div_1_div_3_div_1_span_6_Template, 2, 1, "span", 37);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r36 = ɵɵnextContext(3);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r36.isUploadFilesDone());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r36.isUploadFilesDone());
    ɵɵadvance(2);
    ɵɵpropertyInterpolate("accept", ctx_r36.options.allowedContentTypes);
    ɵɵproperty("options", ctx_r36.options)("uploadInput", ctx_r36.uploadInput);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r36.isUploadFilesDone());
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r36.isUploadFilesDone());
} }
function UploadFileComponent_div_1_div_3_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r45 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 15);
    ɵɵelementStart(1, "label", 16);
    ɵɵelementStart(2, "span", 20);
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵtext(4);
    ɵɵelementStart(5, "input", 40);
    ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_div_3_div_2_Template_input_uploadOutput_5_listener($event) { ɵɵrestoreView(_r45); const ctx_r44 = ɵɵnextContext(3); return ctx_r44.onUploadOutput($event); });
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r37 = ɵɵnextContext(3);
    ɵɵadvance(3);
    ɵɵtextInterpolate1(" ", ctx_r37.uploadlanguageObj.upload_file_browse_file, " ");
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", ctx_r37.fileId, " ");
    ɵɵadvance(1);
    ɵɵpropertyInterpolate("accept", ctx_r37.options.allowedContentTypes);
    ɵɵproperty("options", ctx_r37.options)("uploadInput", ctx_r37.uploadInput);
} }
function UploadFileComponent_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 39);
    ɵɵtemplate(1, UploadFileComponent_div_1_div_3_div_1_Template, 7, 7, "div", 25);
    ɵɵtemplate(2, UploadFileComponent_div_1_div_3_div_2_Template, 6, 5, "div", 5);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r19 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r19.uploadType === "IMAGE");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r19.uploadType === "ALL");
} }
const _c0$3 = function (a0) { return { "is-drop-over": a0 }; };
function UploadFileComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r47 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 24);
    ɵɵlistener("uploadOutput", function UploadFileComponent_div_1_Template_div_uploadOutput_0_listener($event) { ɵɵrestoreView(_r47); const ctx_r46 = ɵɵnextContext(); return ctx_r46.onUploadOutput($event); });
    ɵɵtemplate(1, UploadFileComponent_div_1_div_1_Template, 3, 2, "div", 25);
    ɵɵtemplate(2, UploadFileComponent_div_1_div_2_Template, 7, 7, "div", 25);
    ɵɵtemplate(3, UploadFileComponent_div_1_div_3_Template, 3, 2, "div", 26);
    ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r1 = ɵɵnextContext();
    ɵɵproperty("options", ctx_r1.options)("uploadInput", ctx_r1.uploadInput)("ngClass", ɵɵpureFunction1(6, _c0$3, ctx_r1.dragOver));
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r1.uploadType === "IMAGE");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r1.multiple);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !ctx_r1.multiple);
} }
class UploadFileComponent {
    constructor(http) {
        //super();
        this.http = http;
        this.uploadQueue = new Array();
        this.totalDoneUploaded = 0;
        //crop img
        this.imageChangedEvent = '';
        this.originalImage = '';
        this.uploadRatioConfig = uploadRatio;
        this.multiple = false;
        this.hidePreview = false;
        this.uploadType = uploadType.IMAGE;
        this.fileSize = 1024 * 1024 * 10; //5MB
        this.uploadlanguageObj = UPLOAD_FILE_LANGUAGE_CONFIG;
        this.response = new EventEmitter();
        this.uploadFileEVent = new EventEmitter();
        this.files = [];
        this.uploadInput = new EventEmitter();
        this.humanizeBytes = humanizeBytes;
        this.errorMessage = "";
    }
    ngAfterViewInit() {
        setTimeout(() => {
            this.options = {
                concurrency: 1,
                maxUploads: 9999,
                allowedContentTypes: this.uploadOption.acceptFileType.length ? this.uploadOption.acceptFileType : allowedContentTypesUploadImg
            };
        }, 0);
    }
    isValid(obj) {
        return obj !== null && obj !== undefined;
    }
    parseErrorResponse(response) {
        let errors = new Array();
        if (this.isValid(response.error) && this.isValid(response.error.error) && response.error.error instanceof Array) {
            let arr = response.error.error;
            for (let i = 0; i < arr.length; i++) {
                errors.push(new ErrorObj(arr[i].field, arr[i].message));
            }
        }
        else if (response.status !== 401) {
            errors.push(new ErrorObj(null, "serverError"));
        }
        return errors;
    }
    getRatio() {
        return uploadRatio[this.ratio].ratio;
    }
    getImgUrl() {
        return this.fileName ? this.fileName + "&" + new Date().getTime() : undefined;
    }
    onUploadOutput(output) {
        if (output.type === "allAddedToQueue" && this.uploadQueue.length) {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.START_UPLOAD));
            this.uploadQueue.forEach((element, index) => {
                this.startUpload(element);
            });
        }
        else if (output.type === "addedToQueue" && typeof output.file !== "undefined") {
            if (output.file.size > this.fileSize) {
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
            }
            else {
                if (this.multiple) {
                    this.files.push(output.file);
                }
                else {
                    this.files = [output.file];
                }
                this.uploadQueue.push(output.file);
            }
        }
        else if (output.type === "uploading" && typeof output.file !== "undefined") {
            if (this.multiple) {
                const index = this.files.findIndex(file => typeof output.file !== "undefined" && file.id === output.file.id);
                this.files[index] = output.file;
            }
        }
        else if (output.type === "removed") {
            if (this.multiple) {
                this.files = this.files.filter((file) => file !== output.file);
            }
            else {
                this.files = [];
            }
        }
        else if (output.type === "dragOver") {
            this.dragOver = true;
        }
        else if (output.type === "dragOut") {
            this.dragOver = false;
        }
        else if (output.type === "drop") {
            this.dragOver = false;
        }
        else if (output.type === "done") {
            this.checkResponse(output);
        }
        else if (output.type === "rejected") {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.type"));
        }
    }
    checkResponse(output) {
        this.totalDoneUploaded = this.totalDoneUploaded + 1;
        if (this.isValid(output.file.response.error) && output.file.response.error instanceof Array && output.file.responseStatus !== 200) {
            let arr = output.file.response.error;
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, arr[0].code, output.file.responseStatus));
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
            this.uploadQueue = [];
        }
        else if (output.file.response.result || output.file.responseStatus === 200) {
            if (!this.multiple) {
                this.uploadQueue = [];
                this.fileId = output.file.response.result;
                this.fileName = output.file.response.result;
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, output.file.response.result, output.file.responseStatus));
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
            }
            else if (this.totalDoneUploaded === this.uploadQueue.length) {
                this.uploadQueue = [];
                this.totalDoneUploaded = 0;
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
            }
        }
        else {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error", output.file.responseStatus));
            this.files = [];
            this.uploadQueue = [];
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD, "", output.file.responseStatus));
        }
    }
    isUploadFilesDone() {
        for (let i = 0; i < this.files.length; i++) {
            return this.files[i].progress.data.percentage === 100;
        }
        return false;
    }
    getFilesUploaded() {
        return this.files;
    }
    startUpload(file) {
        this.fileId = "";
        const event = {
            type: "uploadFile",
            url: this.uploadOption.uploadApi.replace(":fileType", this.uploadType),
            method: "POST",
            file: file,
            headers: {
                language: this.uploadOption.language,
                Authorization: this.uploadOption.authorization
            }
        };
        this.uploadInput.emit(event);
    }
    cancelUpload(id) {
        this.uploadInput.emit({ type: "cancel", id: id });
    }
    removeFile(id) {
        this.uploadInput.emit({ type: "remove", id: id });
    }
    removeAllFiles() {
        this.uploadInput.emit({ type: "removeAll" });
    }
    // IMAGE CROPPER
    fileChangeEvent(event) {
        if (event.srcElement.files[0].size > this.fileSize) {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, "upload.file.error.message.file.maxSize"));
        }
        else {
            this.imageChangedEvent = event;
            //this.originalImage = event.srcElement.files;
        }
    }
    imageCropped(event) {
        //this.croppedImage = event.base64;
        this.cropImgObject = event;
        // crop image again if current cropped image's width is greater than resizeToWidth config
        if (event.width > uploadRatio[this.ratio].resizeToWidth) {
            this.imageCropper.resizeToWidth = uploadRatio[this.ratio].resizeToWidth;
            this.imageCropper.crop();
            this.imageCropper.resizeToWidth = 0;
        }
    }
    imageLoaded() {
        // show cropper
    }
    loadImageFailed() {
        // show message
    }
    clearUploadCroppedImage() {
        //this.originalImage = new Array();
        this.cropImgObject = undefined;
        this.imageChangedEvent = undefined;
    }
    uploadCroppedImage() {
        //upload cropped image
        if (this.cropImgObject) {
            this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.START_UPLOAD));
            let formData = new FormData();
            formData.append('file', this.cropImgObject.file);
            let headers = new HttpHeaders();
            headers.append('language', this.uploadOption.language);
            headers.append('Authorization', this.uploadOption.authorization);
            this.http.post(this.uploadOption.uploadApi.replace(":fileType", this.uploadType), formData, { headers: headers }).subscribe((result) => {
                let res = result;
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_SUCCESSED, res.result));
                this.clearUploadCroppedImage();
                this.fileName = result.message;
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
            }, (errorRes) => {
                let errors = this.parseErrorResponse(errorRes);
                if (errors.length > 0 && errors[0].message.trim().length > 0) {
                    this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.UPLOAD_ERROR, errors[0].message.trim()));
                }
                this.response.emit(new UploadResponse(UPLOAD_FILE_EVENT.FINISH_UPLOAD));
            });
        }
    }
}
UploadFileComponent.ɵfac = function UploadFileComponent_Factory(t) { return new (t || UploadFileComponent)(ɵɵdirectiveInject(HttpClient)); };
UploadFileComponent.ɵcmp = ɵɵdefineComponent({ type: UploadFileComponent, selectors: [["via-upload-file"]], viewQuery: function UploadFileComponent_Query(rf, ctx) { if (rf & 1) {
        ɵɵviewQuery(ImageCropperComponent, true);
    } if (rf & 2) {
        var _t;
        ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.imageCropper = _t.first);
    } }, inputs: { multiple: "multiple", hidePreview: "hidePreview", uploadType: "uploadType", fileName: "fileName", fileSize: "fileSize", uploadOption: "uploadOption", uploadlanguageObj: "uploadlanguageObj", ratio: "ratio" }, outputs: { response: "response", uploadFileEVent: "uploadFileEVent" }, decls: 2, vars: 2, consts: [["id", "upload-file", 3, "class", 4, "ngIf"], ["id", "upload-file", "class", "drop-container", "ngFileDrop", "", 3, "options", "uploadInput", "ngClass", "uploadOutput", 4, "ngIf"], ["id", "upload-file"], ["class", "img-cropper-wrapper", "style", "width: 100%;", 4, "ngIf"], ["class", "upload-item", 4, "ngIf"], ["class", "upload-file-container", 4, "ngIf"], [1, "img-cropper-wrapper", 2, "width", "100%"], ["outputType", "file", 3, "maintainAspectRatio", "imageQuality", "imageChangedEvent", "autoCrop", "aspectRatio", "imageCropped", "imageLoaded", "loadImageFailed"], ["type", "button", 1, "btn", "uppercase-text", "btn-primary", "modal-action-btn", 3, "click"], ["type", "button", 1, "btn", "uppercase-text", "btn-light", "modal-action-btn", 3, "click"], [1, "upload-item"], [1, "upload-item-content"], [1, "image-uploaded"], [3, "src", "error"], ["img", ""], [1, "upload-file-container"], [1, "upload-button-file"], ["id", "upload-icon", 4, "ngIf"], [1, "message-drag-and-drop"], [1, "message-or"], [1, "btn-button-file"], ["onclick", "this.value=null", "type", "file", 1, "input-upload", 3, "accept", "change"], ["id", "upload-icon"], ["aria-hidden", "true", 1, "fa", "fa-cloud-upload", "fa-5x"], ["id", "upload-file", "ngFileDrop", "", 1, "drop-container", 3, "options", "uploadInput", "ngClass", "uploadOutput"], [4, "ngIf"], ["style", "width: 100%;", 4, "ngIf"], ["class", "upload-item", 4, "ngFor", "ngForOf"], ["class", "progress-content", 4, "ngIf"], [1, "progress-content"], ["showValue", "true", "type", "success", 3, "value"], [3, "src", "error", 4, "ngIf"], ["image", ""], ["class", "message-drag-and-drop", 4, "ngIf"], ["class", "message-or", 4, "ngIf"], [1, "upload-button"], ["onclick", "this.value=null", "type", "file", "ngFileSelect", "", "multiple", "", 1, "input-upload", 3, "accept", "options", "uploadInput", "uploadOutput"], ["class", "btn-button", 4, "ngIf"], [1, "btn-button"], [2, "width", "100%"], ["onclick", "this.value=null", "type", "file", "ngFileSelect", "", 1, "input-upload", 3, "accept", "options", "uploadInput", "uploadOutput"]], template: function UploadFileComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵtemplate(0, UploadFileComponent_div_0_Template, 4, 6, "div", 0);
        ɵɵtemplate(1, UploadFileComponent_div_1_Template, 4, 8, "div", 1);
    } if (rf & 2) {
        ɵɵproperty("ngIf", ctx.ratio);
        ɵɵadvance(1);
        ɵɵproperty("ngIf", ctx.options && !ctx.ratio);
    } }, directives: [NgIf, ImageCropperComponent, NgFileDropDirective, NgClass, NgForOf, NgbProgressbar, NgFileSelectDirective], styles: ["upload-file[_ngcontent-%COMP%]{width:100%}#upload-file[_ngcontent-%COMP%]{align-items:center;border:2px dashed #c4cdd9;border-radius:3px;display:flex;flex-direction:column;overflow:hidden;text-align:center;width:100%}#upload-file[_ngcontent-%COMP%]   .drop-container[_ngcontent-%COMP%]{padding:30px 10px}#upload-file[_ngcontent-%COMP%]   .message-drag-and-drop[_ngcontent-%COMP%]{font-weight:700}#upload-file[_ngcontent-%COMP%]   .message-or[_ngcontent-%COMP%]{color:#999;display:block;margin-top:20px}#upload-file[_ngcontent-%COMP%]   .upload-button[_ngcontent-%COMP%]{align-items:center;display:flex;flex-direction:column;width:100%}#upload-file[_ngcontent-%COMP%]   .upload-button[_ngcontent-%COMP%]   .input-upload[_ngcontent-%COMP%]{height:0;visibility:hidden;width:0}#upload-file[_ngcontent-%COMP%]   .upload-button[_ngcontent-%COMP%]   .btn-button[_ngcontent-%COMP%]{background:#9a0000;border-radius:.25rem;color:#fff;cursor:pointer;font-size:12px;font-weight:700;max-width:200px;padding:10px}#upload-file[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]{display:flex;display:inline-block;flex-direction:column;margin:10px;text-align:center;width:100%}#upload-file[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]   .input-upload[_ngcontent-%COMP%]{height:0;visibility:hidden;width:0}#upload-file[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]   .btn-button-file[_ngcontent-%COMP%]{background:#9a0000;border-radius:.25rem;color:#fff;cursor:pointer;font-size:12px;font-weight:700;max-width:200px;padding:10px}#upload-file[_ngcontent-%COMP%]   .upload-file-container[_ngcontent-%COMP%]{padding-bottom:10px}#upload-file[_ngcontent-%COMP%]   .message-error[_ngcontent-%COMP%]{margin-bottom:5px;text-align:left;width:80%}#upload-file[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]{margin:20px auto;width:100%}#upload-file[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]   .upload-item-content[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]{padding:15px;text-align:center}#upload-file[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]   .upload-item-content[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:100%}#upload-file[_ngcontent-%COMP%]   .multiple-upload-thumbnail[_ngcontent-%COMP%]{display:inline-block;width:100px}#upload-file.cropper-upload[_ngcontent-%COMP%]{min-height:250px}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]{margin-bottom:0!important}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]{margin:0;margin-bottom:0!important}#upload-file.cropper-upload[_ngcontent-%COMP%]   .message-or[_ngcontent-%COMP%]{margin-top:0!important}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-item[_ngcontent-%COMP%]{margin-top:0}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-file-container[_ngcontent-%COMP%]{padding:0}#upload-file.cropper-upload[_ngcontent-%COMP%]   .upload-file-container[_ngcontent-%COMP%]   .upload-button-file[_ngcontent-%COMP%]{margin-bottom:10px}#upload-file.cropper-upload[_ngcontent-%COMP%]   #upload-icon[_ngcontent-%COMP%]{color:hsla(0,0%,50.2%,.1803921568627451);margin-top:100px}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]{max-height:515px;width:100%}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]{height:100%;padding-bottom:10px}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]   image-cropper[_ngcontent-%COMP%]{max-height:440px}#upload-file.ratio-SIXTEEN_NINE[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-height:390px;max-width:100%;width:auto!important}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]{max-height:730px;width:100%}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]{height:100%;padding-bottom:10px}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]   image-cropper[_ngcontent-%COMP%]{max-height:550px}#upload-file.ratio-ONE_ONE[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-height:600px;max-width:100%;width:auto!important}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]{max-height:700px;width:100%}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]{height:100%;padding-bottom:10px}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]   .img-cropper-wrapper[_ngcontent-%COMP%]   image-cropper[_ngcontent-%COMP%]{max-height:650px}#upload-file.ratio-ONE_THREE[_ngcontent-%COMP%]   .image-uploaded[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-height:550px;max-width:100%;width:auto!important}"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(UploadFileComponent, [{
        type: Component,
        args: [{
                selector: "via-upload-file",
                templateUrl: "./upload.file.html",
                styleUrls: ["./upload.file.scss"]
            }]
    }], function () { return [{ type: HttpClient }]; }, { imageCropper: [{
            type: ViewChild,
            args: [ImageCropperComponent]
        }], multiple: [{
            type: Input
        }], hidePreview: [{
            type: Input
        }], uploadType: [{
            type: Input
        }], fileName: [{
            type: Input
        }], fileSize: [{
            type: Input
        }], uploadOption: [{
            type: Input
        }], uploadlanguageObj: [{
            type: Input
        }], ratio: [{
            type: Input
        }], response: [{
            type: Output
        }], uploadFileEVent: [{
            type: Output
        }] }); })();

class UploadFileModule {
}
UploadFileModule.ɵmod = ɵɵdefineNgModule({ type: UploadFileModule });
UploadFileModule.ɵinj = ɵɵdefineInjector({ factory: function UploadFileModule_Factory(t) { return new (t || UploadFileModule)(); }, imports: [[
            HttpClientModule,
            NgbModule,
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            ImageCropperModule,
            NgxUploaderModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(UploadFileModule, { declarations: [UploadFileComponent], imports: [HttpClientModule,
        NgbModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ImageCropperModule,
        NgxUploaderModule], exports: [UploadFileComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(UploadFileModule, [{
        type: NgModule,
        args: [{
                declarations: [UploadFileComponent],
                imports: [
                    HttpClientModule,
                    NgbModule,
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    ImageCropperModule,
                    NgxUploaderModule
                ],
                exports: [UploadFileComponent]
            }]
    }], null, null); })();

class GoogleAnalyticsService {
    //************************
    // Import this to app.component.ts
    // subscribe to router events and send page views to Google Analytics
    // this.router.events.subscribe(event => {
    //     GoogleAnalyticsService.subscribeRouterEvents(event);
    //   });
    //************************
    constructor() { }
    // /**
    //   * Emit google analytics event
    //   * Fire event example:
    //   * this.emitEvent("testCategory", "testAction", "testLabel", 10);
    //   * param {string} eventCategory
    //   * param {string} eventAction
    //   * param {string} eventLabel
    //   * param {number} eventValue
    //   */
    eventEmitter(eventCategory, eventAction, eventLabel = null, eventValue = null) {
        ga('send', 'event', {
            eventCategory: eventCategory,
            eventLabel: eventLabel,
            eventAction: eventAction,
            eventValue: eventValue
        });
    }
    subscribeRouterEvents(event) {
        if (event instanceof NavigationEnd) {
            ga('set', 'page', event.urlAfterRedirects);
            ga('send', 'pageview');
        }
    }
}
GoogleAnalyticsService.ɵfac = function GoogleAnalyticsService_Factory(t) { return new (t || GoogleAnalyticsService)(); };
GoogleAnalyticsService.ɵprov = ɵɵdefineInjectable({ token: GoogleAnalyticsService, factory: GoogleAnalyticsService.ɵfac });
/*@__PURE__*/ (function () { ɵsetClassMetadata(GoogleAnalyticsService, [{
        type: Injectable
    }], function () { return []; }, null); })();

class GoogleAnalyticsServiceModule {
}
GoogleAnalyticsServiceModule.ɵmod = ɵɵdefineNgModule({ type: GoogleAnalyticsServiceModule });
GoogleAnalyticsServiceModule.ɵinj = ɵɵdefineInjector({ factory: function GoogleAnalyticsServiceModule_Factory(t) { return new (t || GoogleAnalyticsServiceModule)(); }, providers: [GoogleAnalyticsService], imports: [[]] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(GoogleAnalyticsServiceModule, [{
        type: NgModule,
        args: [{
                imports: [],
                providers: [GoogleAnalyticsService],
                exports: []
            }]
    }], null, null); })();

/*
 * Public API Surface of viatech-angular-common
 */

/**
 * Generated bundle index. Do not edit.
 */

export { ACTION_TYPE, ButtonAction, CELL_TYPE, ClickOutSiteDirective, ClickOutSiteModule, CustomAdapter, CustomDateParserFormatter, CustomDatepickerI18n, CustomToast, DATE_FORMAT$1 as DATE_FORMAT, DATE_FORMAT_WITHOUT_TIME$1 as DATE_FORMAT_WITHOUT_TIME, DatepickerComponent, DatepickerModule, DropdownComponent, DropdownItem, DropdownModule, EntryPerpage, ErrorObj, FilterPipe, FullTextSearchPipe, GoogleAnalyticsService, GoogleAnalyticsServiceModule, I18n, ImageSphereComponent, ImageSphereModule, Map, MapComponent, MapModule, Modal, SORT, TABLE_LANGUAGE_CONFIG, TableAction, TableCell, TableColumn, TableComponent, TableDefaultSort, TableFilter, TableFilterDropdownItem, TableModule, TableOption, TableRow, TableRowValue, TimepickerComponent, TimepickerModule, ToastModule, TypeaheadComponent, TypeaheadItem, TypeaheadItemValue, TypeaheadModule, TypeaheadOptions, UPLOAD_FILE_EVENT, UPLOAD_FILE_LANGUAGE_CONFIG, UploadFileComponent, UploadFileModule, UploadOption, UploadResponse, WinModalComponent, WinModalModule, allowedContentTypesUploadExel, allowedContentTypesUploadImg, allowedContentTypesUploadTxt, languageObj, modalConfigs, myLibInit, uploadRatio, uploadType };
//# sourceMappingURL=viatech-angular-common.js.map
